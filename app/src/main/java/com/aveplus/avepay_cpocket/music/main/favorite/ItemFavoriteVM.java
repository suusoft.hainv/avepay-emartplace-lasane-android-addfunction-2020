package com.aveplus.avepay_cpocket.music.main.favorite;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.IOnMenuItemClick;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.main.home.ItemSongsVM;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ItemFavoriteVM extends ItemSongsVM {

    public ItemFavoriteVM(Context self, Object object, int position) {
        super(self, object, position);
    }

    public void onClickAction(View view) {
        Global.showPopupMenu(self, view, R.menu.menu_action_item_favorite, new IOnMenuItemClick() {
            @Override
            public void onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_play:
                        ((ItemMenuActionSongListener) listener).onClickWatch(position);
                        break;

                    case R.id.action_delete:
                        ((ItemMenuActionSongListener) listener).onClickDelete(position);
                        break;
                }
            }
        });
    }
}
