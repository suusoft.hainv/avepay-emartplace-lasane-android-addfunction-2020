package com.aveplus.avepay_cpocket.taxi.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.interfaces.IOnItemClickListener;

import com.aveplus.avepay_cpocket.view.fragments.BaseFragment;

public class TaxiFragment   extends BaseFragment implements IOnItemClickListener, View.OnClickListener {
    private static final String TAG = TaxiFragment.class.getSimpleName();

    public static TaxiFragment newInstance(int type) {

        Bundle args = new Bundle();
        args.putInt(Args.TYPE_OF_CATEGORY, type);
        TaxiFragment fragment = new TaxiFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initData();
        return view;
    }

    private void initData() {
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    protected View inflateLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_taxi, container, false);

    }

    @Override
    protected void initUI(View view) {

    }

    @Override
    protected void initControl() {

    }
}
