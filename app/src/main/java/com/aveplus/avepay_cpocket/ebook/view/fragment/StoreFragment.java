package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseFragment;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity;
import com.google.android.material.tabs.TabLayout;


/**
 * Created by trangpham on 3/5/18.
 */

public class StoreFragment extends BaseFragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TextView tvTitle;
    private int curPosition;

    public static StoreFragment newInstance() {

        Bundle args = new Bundle();

        StoreFragment fragment = new StoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_store;
    }

    @Override
    protected void init() {
    }


    @Override
    protected void initView(View view) {
        //tvTitle = (TextView) view.findViewById(R.id.c_tv_title);
        viewPager = (ViewPager) view.findViewById(R.id.view_pagger);
        viewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager()));
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(curPosition);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    ((MainActivity) self).showIconToolbar(/*R.id.action_clear_history*/);
                } else {
                    ((MainActivity) self).showIconToolbar();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void getData() {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private String[] tabs = getResources().getStringArray(R.array.tab_store);

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK, Constant.MENU_LEFT_HISTORY);
                bundle.putString(Constant.KEY_TITLE_TOOLBAR, getString(R.string.download));
                Fragment fragment = FragmentListChapterBookmark.newInstance(true, true);
                fragment.setArguments(bundle);
                return fragment;
            }
            if (position == 1) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK, Constant.MENU_LEFT_BOOKMARKS);
                bundle.putString(Constant.KEY_TITLE_TOOLBAR, getString(R.string.bookmarks));
                Fragment fragment = FragmentListBook.newInstance(true);
                fragment.setArguments(bundle);
                return fragment;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public int getCount() {
            return tabs.length;
        }


    }
}
