package com.aveplus.avepay_cpocket.ebook.base.model.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Created by Trang on 7/12/2017.
 */
public abstract class BaseAdapter extends RecyclerView.Adapter<BaseAdapter.BaseViewHolder> {

    protected Context context;
    public abstract void setDatas(List<?> data);

    public BaseAdapter(Context context) {
        this.context = context;
    }

    public void setItems(List<?> data) {
        setDatas(data);
//        notifyDataSetChanged();
    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {

        public BaseViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
        }
    }

    protected ViewDataBinding getViewBinding(ViewGroup parent, int layout){
        return DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layout, parent, false);

    }

}
