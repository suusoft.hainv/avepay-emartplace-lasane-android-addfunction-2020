package com.aveplus.avepay_cpocket.movie.movie.main.movies.impl;



import com.aveplus.avepay_cpocket.movie.movie.main.movies.model.BaseYTParser;

import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface YoutubeCallback {
    public void onCompleted(List<BaseYTParser.VideoStream> videoStreams);
    public void onFailed(boolean b);
}
