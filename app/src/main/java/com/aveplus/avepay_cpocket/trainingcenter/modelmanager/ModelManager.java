package com.aveplus.avepay_cpocket.trainingcenter.modelmanager;



import com.aveplus.avepay_cpocket.trainingcenter.config.WebserviceConfig;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;

import java.util.HashMap;

public class ModelManager extends BaseRequest {

    public static void getListProductByCateGory(int categoryId,
                                                final CompleteListener listener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("categoryId", categoryId + "");
        get(WebserviceConfig.URL_GETLISTPRODUCTBYCATEGORY, params, listener);
    }

    public static void getListCategoryProduct(final CompleteListener listener) {
        final String url = WebserviceConfig.URL_GETCATEGORY;
        HashMap<String, String> params = new HashMap<>();
        get(url, params, listener);

    }


    public static void getListAbout(final CompleteListener listener) {
        final String url = WebserviceConfig.URL_ABOUT;
        HashMap<String, String> params = new HashMap<>();
        get(url, params, listener);
    }

    public static void getListPEOPLE(final CompleteListener listener) {
        final String url = WebserviceConfig.URL_PEOPLE;
        HashMap<String, String> params = new HashMap<>();
        get(url, params, listener);
    }

    public static void getListContact(final CompleteListener listener) {
        final String url = WebserviceConfig.URL_CONTACT;
        HashMap<String, String> params = new HashMap<>();
        get(url, params, listener);
    }

    public static void getListNewsRecent(int page,
                                         final CompleteListener listener) {
        final String url = WebserviceConfig.URL_NEWS;
        HashMap<String, String> params = new HashMap<>();
        params.put("page", page + "");
        params.put("type", "recent");
        get(url, params, listener);
    }

    public static void getListNewsPopular(int page, final CompleteListener listener) {
        final String url = WebserviceConfig.URL_NEWS;
        HashMap<String, String> params = new HashMap<>();
        params.put("page", page + "");
        params.put("type", "popular");
        get(url, params, listener);
    }

    public static void getListNewsFeature(int page, final CompleteListener listener) {
        final String url = WebserviceConfig.URL_NEWS;
        HashMap<String, String> params = new HashMap<>();
        params.put("page", page + "");
        params.put("type", "featured");
        get(url, params, listener);
    }

    public static void getListProducts(int page, final CompleteListener listener) {
        final String url = WebserviceConfig.URL_PRODUCTS;
        HashMap<String, String> params = new HashMap<>();
        params.put("page", page + "");
        get(url, params, listener);
    }

    public static void getListProByCateAndKey(String cate,
                                              String key, final CompleteListener listener) {
        final String url = WebserviceConfig.URL_GETLISTPRODUCTBYCATEGORY;
        HashMap<String, String> params = new HashMap<>();
        if (!cate.equals("0"))
            params.put("categoryId", cate);
        params.put("keyword", key);
        get(url, params, listener);
    }

    public static void getListNewsByCateAndKey(String cate,
                                               String key, String page, final CompleteListener listener) {
        final String url = WebserviceConfig.URL_GETLISTNEWBYCATEGORY;
        HashMap<String, String> params = new HashMap<>();

        if (!cate.equals("0"))
            params.put("categoryId", cate);
        params.put("keyword", key);
        params.put("page", page + "");
        get(url, params, listener);
    }

    public static void getListPortfolios(int page, String key,
                                         final CompleteListener listener) {
        final String url = WebserviceConfig.URL_PORTFOLIOS;

        HashMap<String, String> params = new HashMap<>();
        params.put("keyword", key);
        if (page != 0)
            params.put("page", page + "");
        get(url, params, listener);
    }

    public static void getListService(int page,
                                      final CompleteListener listener) {
        final String url = WebserviceConfig.URL_SERVICE;
        HashMap<String, String> params = new HashMap<>();
        params.put("page", page + "");
        get(url, params, listener);
    }

    public static void getListCustomer(
            final CompleteListener listener) {
        final String url = WebserviceConfig.URL_TESTIMONIAL;
        HashMap<String, String> params = new HashMap<>();
        get(url, params, listener);
    }

    public static void getListSaleRecent(
            final CompleteListener listener) {
        final String url = WebserviceConfig.URL_SALE;
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "recent");
        get(url, params, listener);
    }

    public static void getListSalePopular(
            final CompleteListener listener) {
        final String url = WebserviceConfig.URL_SALE;
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "popular");
        get(url, params, listener);
    }

    public static void getListSaleFeature(
            final CompleteListener listener) {
        final String url = WebserviceConfig.URL_SALE;
        HashMap<String, String> params = new HashMap<>();
        params.put("type", "featured");
        get(url, params, listener);
    }

    public static void getSetting(
            final CompleteListener listener) {
        final String url = WebserviceConfig.URL_SETTING;
        HashMap<String, String> params = new HashMap<>();
        get(url, params, listener);
    }

    public static void putDataEmail(String fullname,
                                    String phone, String email, String time, String topics,
                                    String content,
                                    final CompleteListener listener) {

        final String url = WebserviceConfig.HOST_URL_EMAIL;
        HashMap<String, String> params = new HashMap<>();
        params.put("fullName", fullname);
        params.put("phone", phone);
        params.put("email", email);
        params.put("topics", topics);
        params.put("content", content);
        params.put("subject", time);
        get(url, params, listener);
    }


    public static void putDataCart(String json,
                                   String fullname, String phone, String email, String payment,
                                   String content, double vat, double ship, double totalprice,
                                   final CompleteListener listener) {

        final String url = WebserviceConfig.URL_ORDER;
        HashMap<String, String> params = new HashMap<>();
        params.put("fullName", fullname);
        params.put("phoneNumber", phone);
        params.put("email", email);
        params.put("content", content);
        params.put("paymentMethod", payment);
        params.put("product", json);
        params.put("vat", vat + "");
        params.put("transportFee", ship + "");
        params.put("total", totalprice + "");
        get(url, params, listener);
    }

    public static void getHistory(String ids,
                                  final CompleteListener listener) {
        final String url = WebserviceConfig.HOST_URL_HISTORY;

        HashMap<String, String> params = new HashMap<>();
        params.put("ids", ids);
        get(url, params, listener);
    }

    public static void getDetailHistory(String orderId,
                                        final CompleteListener listener) {
        final String url = WebserviceConfig.HOST_URL_DETAIL_HISTORY;

        HashMap<String, String> params = new HashMap<>();
        params.put("orderId", orderId);
        get(url, params, listener);
    }

    public static void sendComment(String name, String email, String address, String comment, final CompleteListener listener) {
        final String url = WebserviceConfig.URL_COMMENT;

        HashMap<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("email", email);
        params.put("content", comment);
        params.put("location", address);
        get(url, params, listener);


    }

    public static void order(String userName, String userPhone, String userAddress, String userEmail,
                             String userPostCode, String receiverName, String receiverPhone,
                             String receiverAddress, String receiverEmail, String receiverPostCode,
                             String content, String vat, String transportFee, String total, String paymentMethod,
                             String items, final CompleteListener listener) {
        final String url = WebserviceConfig.URL_ORDER;

        HashMap<String, String> params = new HashMap<>();
        params.put("billingName", userName);
        params.put("billingPhone", userPhone);
        params.put("billingAddress", userAddress);
        params.put("billingEmail", userEmail);
        params.put("billingPostcode", userPostCode);

        params.put("shippingName", receiverName);
        params.put("shippingPhone", receiverPhone);
        params.put("shippingAddress", receiverAddress);
        params.put("shippingEmail", receiverEmail);
        params.put("shippingPostcode", receiverPostCode);

        params.put("content", content);
        params.put("paymentMethod", paymentMethod);
        params.put("items", items);

        params.put("vat", vat + "");
        params.put("transportFee", transportFee);
        params.put("total", total);
        get(url, params, listener);

    }
}