package com.aveplus.avepay_cpocket.trainingcenter.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ScheduleAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.cart.CartManager;
import com.aveplus.avepay_cpocket.trainingcenter.object.Class;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.aveplus.avepay_cpocket.trainingcenter.object.Schedule;
import com.aveplus.avepay_cpocket.view.activities.BaseActivity;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

/**
 * Created by SuuSoft on 05/09/2017.
 */

public class DetailScheduleActivity extends BaseActivity {

    private TextView txttitle, tvWeek;
    private TextView tvPrice;
    private ImageView imgservice;
    private LinearLayout llParent;
    private TextView tvExpired;
    private RelativeLayout btnAddToCart;
    private ArrayList<Schedule> mListSchedule;
    private ListView mLvListClass;
    private ScheduleAdapter mScheduleAdapter;
    private Product product;
    private Class aClass;
    private ImageView btnBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_schedule);
        intiView();
        intiData();
        initControl();
    }


    @Override
    protected void inflateLayout() {

    }

    @Override
    protected void initUI() {

    }

    @Override
    protected void initControl() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!CartManager.getInstance().isExist(product)) {
                    product.setNumberCart(1);
                    product.setIdClassChose(aClass.getIdClass());
                    //CartManager.getInstance().getYourCart().add(product);
                    CartManager.getInstance().addCart(product);
                    Toast.makeText(getApplicationContext(),
                            "Add " + product.getName() + " To Cart Successfully",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "This item have already existed !",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void intiView() {
        txttitle = (TextView) findViewById(R.id.txtTitleServicesDetail);
        tvWeek = (TextView) findViewById(R.id.tvWeek);
        imgservice = (ImageView) findViewById(R.id.imgServiceDetail);
        mLvListClass = (ListView) findViewById(R.id.lvListSchedules);
        btnAddToCart = (RelativeLayout) findViewById(R.id.btnAddToCart);
        llParent = (LinearLayout) findViewById(R.id.llParent);
        tvExpired = (TextView) findViewById(R.id.tvExpired);
        txttitle.setSelected(true);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        btnBack = (ImageView) findViewById(R.id.imgBack);


    }


    public void intiData() {
        Bundle bundle = getIntent().getBundleExtra("bundle");
        product = bundle.getParcelable("item");
        aClass = bundle.getParcelable("Class");
        if (aClass.isExpired()) {
            llParent.setVisibility(View.GONE);
            tvExpired.setVisibility(View.VISIBLE);
            btnAddToCart.setEnabled(false);
        } else {
            llParent.setVisibility(View.VISIBLE);
            tvExpired.setVisibility(View.GONE);
            btnAddToCart.setEnabled(true);
        }

        mListSchedule = aClass.getmListSchedule();
        mScheduleAdapter = new ScheduleAdapter(mListSchedule);
        mLvListClass.setAdapter(mScheduleAdapter);
        txttitle.setText(product.getName());
        tvWeek.setText(aClass.getmStartDate() + " - " + aClass.getmEndDate());
        tvPrice.setText(product.getPrice());
        Picasso.get().load(product.getImage().get(0)).into(imgservice);
    }

}
