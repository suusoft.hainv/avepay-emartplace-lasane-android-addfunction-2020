package com.aveplus.avepay_cpocket.movie.movie.datastore;

import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.movie.movie.model.User;
import com.aveplus.avepay_cpocket.objects.SettingsObj;
import com.aveplus.avepay_cpocket.utils.StringUtil;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright © 2019 SUUSOFT
 */

public class DataStoreManager extends BaseDataStore {

    // ============== User ============================
    private static final String PREF_USER = "PREF_USER";
    private static final String PREF_TOKEN_USER = "PREF_TOKEN_USER";
    private static final String PREF_SETTINGS_NOTIFY = "PREF_SETTINGS_NOTIFY";
    private static final String PREF_SETTING_UTILITY ="SETTING_UTILITY" ;

    /**
     * save and get user
     *
     */
    public static void saveUser(User user) {
        if (user != null) {
            String jsonUser = user.toJSon();
            getInstance().sharedPreferences.putStringValue(PREF_USER, jsonUser);
        }
    }

    public static void removeUser() {
        getInstance().sharedPreferences.putStringValue(PREF_USER, null);
    }

    public static User getUser() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_USER);
        User user = new Gson().fromJson(jsonUser, User.class);
        return user;
    }

    /**
     * save and get user's token
     *
     */
    public static void saveToken(String token) {
        getInstance().sharedPreferences.putStringValue(PREF_TOKEN_USER, token);
    }

    public static String getToken() {
        return getInstance().sharedPreferences.getStringValue(PREF_TOKEN_USER);
    }

    /**
     * save and get caching time
     *
     */
    public static String getCaching(String request){
        return getInstance().dbConnection.getCaching(StringUtil.getAction(request));
    }

    public static void saveCaching(String url, String objectRoot, String timeCaching) {
        getInstance().dbConnection.saveCaching(StringUtil.getAction(url),objectRoot, timeCaching);
    }

    //================== settings notify ======================
    public static void saveSettingNotify(boolean state){
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY, state);
    }

    public static boolean getSettingNotifyState(){
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY);
    }
    public static SettingsObj getSettingUtility() {
        String setting = getInstance().sharedPreferences.getStringValue(PREF_SETTING_UTILITY);
        if (setting.isEmpty()) {
            return null;
        } else {
            try {
                JSONObject jsonObject = new JSONObject(setting);
                ApiResponse apiResponse = new ApiResponse(jsonObject);
                if (!apiResponse.isError()) {
                    SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
                    return utitlityObj;

                } else {
                    return null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
