package com.aveplus.avepay_cpocket.music.model;

import android.os.Parcel;
import android.os.Parcelable;


import com.aveplus.avepay_cpocket.music.base.model.BaseModel;

import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Video extends BaseModel implements Parcelable {
    public static final int TYPE_VIDEO = 0;
    public static final int TYPE_PLAYLIST = 1;
    public String id;
    public String video_id;
    public String title;
    public String description;
    public String image, imageHD;
    public String file, dateTime;
    public List<String> gallery;
    public int like;
    public int views;
    public int status, type; // 0 is video, 1 is playlist
    public float rate;
    public String duration;
    public int count;

    public Video(){}

    protected Video(Parcel in) {
        id = in.readString();
        video_id = in.readString();
        title = in.readString();
        description = in.readString();
        image = in.readString();
        imageHD = in.readString();
        file = in.readString();
        dateTime = in.readString();
        gallery = in.createStringArrayList();
        like = in.readInt();
        views = in.readInt();
        status = in.readInt();
        type = in.readInt();
        rate = in.readFloat();
        duration = in.readString();
        count = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(video_id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(imageHD);
        dest.writeString(file);
        dest.writeString(dateTime);
        dest.writeStringList(gallery);
        dest.writeInt(like);
        dest.writeInt(views);
        dest.writeInt(status);
        dest.writeInt(type);
        dest.writeFloat(rate);
        dest.writeString(duration);
        dest.writeInt(count);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    public Playlist convertToPlaylist(){
        Playlist item = new Playlist();
        item.id = id;
        item.title = title;
        item.description = description;
        item.image = image;
        item.views = views;
        item.dateTime = dateTime;
        item.duration = duration;
        item.count = count;
        return item;
    }

}
