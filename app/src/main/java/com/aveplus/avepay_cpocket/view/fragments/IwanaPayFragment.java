package com.aveplus.avepay_cpocket.view.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.view.activities.BuyCreditsActivity;
import com.aveplus.avepay_cpocket.view.activities.HistoryActivity;
import com.aveplus.avepay_cpocket.view.activities.RedeemActivity;
import com.aveplus.avepay_cpocket.view.activities.TransferActivity;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;

/**
 * Created by Suusoft on 30/11/2016.
 */

public class IwanaPayFragment extends BaseFragment implements View.OnClickListener {
    private TextViewRegular btnBuyCredits, btnRedeem, btnTranfer, btnHistories;

    public static IwanaPayFragment newInstance() {

        Bundle args = new Bundle();

        IwanaPayFragment fragment = new IwanaPayFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_iwana_pay, container, false);
    }

    @Override
    protected void initUI(View view) {

        btnBuyCredits = (TextViewRegular) view.findViewById(R.id.btn_buy_credits);
        btnHistories = (TextViewRegular) view.findViewById(R.id.btn_history);
        btnRedeem = (TextViewRegular) view.findViewById(R.id.btn_functions);
        btnTranfer = (TextViewRegular) view.findViewById(R.id.btn_transfer);
    }

    @Override
    protected void initControl() {
        btnTranfer.setOnClickListener(this);
        btnRedeem.setOnClickListener(this);
        btnHistories.setOnClickListener(this);
        btnBuyCredits.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnBuyCredits) {
            AppUtil.startActivity(getActivity(), BuyCreditsActivity.class);
        } else if (view == btnHistories) {
            AppUtil.startActivity(getActivity(), HistoryActivity.class);
        } else if (view == btnRedeem) {
            AppUtil.startActivity(getActivity(), RedeemActivity.class);
        } else if (view == btnTranfer) {
            AppUtil.startActivity(getActivity(), TransferActivity.class);
        }
    }
}
