package com.aveplus.avepay_cpocket.music.main.video2;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObjVideo implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("is_free")
    @Expose
    private Integer isFree;
    @SerializedName("source_id")
    @Expose
    private Integer sourceId;

    @SerializedName("video_title_id")
    @Expose
    private Integer video_title_id;
    @SerializedName("url")
    @Expose
    private String url;


    protected ObjVideo(Parcel in) {
        if (in.readByte () == 0) {
            id = null;
        } else {
            id = in.readInt ();
        }
        name = in.readString ();
        image = in.readString ();
        video = in.readString ();
        description = in.readString ();
        if (in.readByte () == 0) {
            price = null;
        } else {
            price = in.readDouble ();
        }
        if (in.readByte () == 0) {
            isFree = null;
        } else {
            isFree = in.readInt ();
        }
        if (in.readByte () == 0) {
            sourceId = null;
        } else {
            sourceId = in.readInt ();
        }
        if (in.readByte () == 0) {
            video_title_id = null;
        } else {
            video_title_id = in.readInt ();
        }
        url = in.readString ();
    }

    public static final Creator<ObjVideo> CREATOR = new Creator<ObjVideo>() {
        @Override
        public ObjVideo createFromParcel(Parcel in) {
            return new ObjVideo (in);
        }

        @Override
        public ObjVideo[] newArray(int size) {
            return new ObjVideo[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getIsFree() {
        return isFree;
    }

    public void setIsFree(Integer isFree) {
        this.isFree = isFree;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getVideo_title_id() {
        return video_title_id;
    }

    public void setVideo_title_id(Integer video_title_id) {
        this.video_title_id = video_title_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte ((byte) 0);
        } else {
            parcel.writeByte ((byte) 1);
            parcel.writeInt (id);
        }
        parcel.writeString (name);
        parcel.writeString (image);
        parcel.writeString (video);
        parcel.writeString (description);
        if (price == null) {
            parcel.writeByte ((byte) 0);
        } else {
            parcel.writeByte ((byte) 1);
            parcel.writeDouble (price);
        }
        if (isFree == null) {
            parcel.writeByte ((byte) 0);
        } else {
            parcel.writeByte ((byte) 1);
            parcel.writeInt (isFree);
        }
        if (sourceId == null) {
            parcel.writeByte ((byte) 0);
        } else {
            parcel.writeByte ((byte) 1);
            parcel.writeInt (sourceId);
        }
        if (video_title_id == null) {
            parcel.writeByte ((byte) 0);
        } else {
            parcel.writeByte ((byte) 1);
            parcel.writeInt (video_title_id);
        }
        parcel.writeString (url);
    }
}
