package com.aveplus.avepay_cpocket.ebook.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Dashboard {

    @SerializedName("hot")
    private ArrayList<Book> mListBookHot;
    @SerializedName("new")
    private ArrayList<Book> mListBookNew;
    @SerializedName("feature")
    private ArrayList<Book> mListBookFeature;
    private ArrayList<Book> listTop;
    private ArrayList<Category> categories;

    public ArrayList<Book> getmListBookHot() {
        if (mListBookHot == null)
            mListBookHot = new ArrayList<>();
        return mListBookHot;
    }

    public void setmListBookHot(ArrayList<Book> mListBookHot) {
        this.mListBookHot = mListBookHot;
    }

    public ArrayList<Book> getmListBookNew() {
        if (mListBookNew == null)
            mListBookNew = new ArrayList<>();
        return mListBookNew;
    }

    public void setmListBookNew(ArrayList<Book> mListBookNew) {
        this.mListBookNew = mListBookNew;
    }

    public ArrayList<Book> getmListBookFeature() {
        if (mListBookFeature == null)
            mListBookFeature = new ArrayList<>();
        return mListBookFeature;
    }

    public ArrayList<Book> getListTop() {
        if (listTop == null)
            listTop = new ArrayList<>();

        if (getmListBookFeature().size() > 0) {
            listTop.add(mListBookFeature.get(0));
            if (mListBookFeature.size()> 1)
                if (mListBookFeature.get(1) != null)
                    listTop.add(mListBookFeature.get(1));

            if (mListBookFeature.size()> 2)
                if (mListBookFeature.get(2) != null)
                    listTop.add(mListBookFeature.get(2));

        }
        return listTop;
    }

    public void setListTop(ArrayList<Book> listTop) {
        this.listTop = listTop;
    }

    public void setmListBookFeature(ArrayList<Book> mListBookFeature) {
        this.mListBookFeature = mListBookFeature;
    }

    public ArrayList<Category> getCategories() {
        if (categories == null)
            categories = new ArrayList<>();
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }
}
