package com.aveplus.avepay_cpocket.trainingcenter.object;

public class Customers {
	private String id;
	private String name;
	private String email;
	private String skype;
	private String image;
	private String phoneNumber;
	private String description;
	private String website;
	private String information;
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Customers(String id, String name, String email, String skype,
                     String image, String phoneNumber, String description,
                     String website, String information) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.skype = skype;
		this.image = image;
		this.phoneNumber = phoneNumber;
		this.description = description;
		this.website = website;
		this.information = information;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Customers() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
