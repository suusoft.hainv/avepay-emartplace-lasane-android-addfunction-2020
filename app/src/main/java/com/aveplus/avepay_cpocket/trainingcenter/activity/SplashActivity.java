package com.aveplus.avepay_cpocket.trainingcenter.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.config.FruitySharedPreferences;
import com.aveplus.avepay_cpocket.trainingcenter.config.GlobalValue;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.utils.AppUtil;


public class SplashActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        if (!FruitySharedPreferences.getInstance(getApplicationContext()).getBooleanValue("first")) {
            addShortcut(getApplicationContext(), SplashActivity.class);
            FruitySharedPreferences.getInstance(getApplicationContext()).putBooleanValue("first", true);
        }

        if (GlobalValue.pref == null) {
            GlobalValue.pref = new FruitySharedPreferences(this);
        }
        slideImg();
    }

    private void startMainActivity() {
        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        finish();
    }

    private void slideImg() {
        ModelManager.getListAbout(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                GlobalValue.pref.setArrPicture(ParseUtility.parseListGalleries(response.getRootStr()));
                startMainActivity();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(SplashActivity.this, message);
            }
        });

    }

    public static void addShortcut(Context context, Class tClass) {
        //on Home screen
        Intent shortcutIntent = new Intent(context, tClass);

        shortcutIntent.setAction(Intent.ACTION_MAIN);

        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, context.getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(context, R.mipmap.ic_launcher));
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        context.sendBroadcast(addIntent);
    }


}
