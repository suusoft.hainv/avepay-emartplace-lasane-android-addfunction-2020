/*
 * Name: $RCSfile: NetworkUtility.java,v $
 * Version: $Revision: 1.1 $
 * Date: $Date: Oct 31, 2011 3:57:18 PM $
 *
 * Copyright (C) 2011 COMPANY_NAME, Inc. All rights reserved.
 */

package com.aveplus.avepay_cpocket.restaurant.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.aveplus.avepay_cpocket.AppController;


/**
 * NetworkUtility checks available network
 *
 * @author Lemon
 */
public class NetworkUtility {

    private Context context = null;

    private static NetworkUtility instance = null;

    /**
     * Constructor
     *
     * @param context
     */
    private NetworkUtility(Context context) {
        this.context = context;
    }

    /**
     * Get class instance
     *
     * @param context
     * @return
     */
    public static NetworkUtility getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkUtility(context);
        }
        return instance;
    }

    /**
     * Check network connection
     *
     * @return
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager conMgr = (ConnectivityManager) AppController.getInstance()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null) {
            return false;
        }
        if (!i.isConnected()) {
            return false;
        }
        if (!i.isAvailable()) {
            return false;
        }
        return true;
    }


}
