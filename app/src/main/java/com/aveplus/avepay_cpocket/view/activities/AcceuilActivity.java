package com.aveplus.avepay_cpocket.view.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.aveplus.avepay_cpocket.movie.movie.main.CategoryActivity;
import com.aveplus.avepay_cpocket.music.MusicActivity;
import com.aveplus.avepay_cpocket.music.main.main2.MainActivity2;
import com.aveplus.avepay_cpocket.restaurant.RestaurantActivity;
import com.aveplus.avepay_cpocket.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.retrofit.response.BaseReponse;
import com.aveplus.avepay_cpocket.retrofit.response.ResponseUser;
import com.aveplus.avepay_cpocket.taxi.TaxiActivity;
import com.aveplus.avepay_cpocket.taxi.view.activities.SplashActivity;
import com.aveplus.avepay_cpocket.trainingcenter.TrainingCenterActivity;
import com.aveplus.avepay_cpocket.trainingcenter.activity.HomeActivity;
import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.PacketUtility;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.Constants;
import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.interfaces.IOnItemClickListener;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.network1.MyProgressDialog;
import com.aveplus.avepay_cpocket.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.objects.Acceuil;
import com.aveplus.avepay_cpocket.objects.ContactObj;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.objects.MenuLeft;
import com.aveplus.avepay_cpocket.objects.SettingsObj;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.aveplus.avepay_cpocket.parsers.JSONParser;
import com.aveplus.avepay_cpocket.quickblox.QbAuthUtils;
import com.aveplus.avepay_cpocket.quickblox.QbDialogHolder;
import com.aveplus.avepay_cpocket.quickblox.SharedPreferencesUtil;
import com.aveplus.avepay_cpocket.quickblox.chat.ChatHelper;
import com.aveplus.avepay_cpocket.quickblox.conversation.utils.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.aveplus.avepay_cpocket.utils.map.IMaps;
import com.aveplus.avepay_cpocket.utils.map.LocationService;
import com.aveplus.avepay_cpocket.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.view.adapters.AcceuilAdapter;
import com.aveplus.avepay_cpocket.view.adapters.MenuLeftAdapte;
import com.aveplus.avepay_cpocket.view.fragments.AllDealsFragment;
import com.aveplus.avepay_cpocket.view.fragments.ContactFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealListFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealManagerFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealsFragment;
import com.aveplus.avepay_cpocket.view.fragments.FragmentFavorite;
import com.aveplus.avepay_cpocket.view.fragments.IwanaPayFragment;
import com.aveplus.avepay_cpocket.view.fragments.MyAccountMyInfoFragment;
import com.aveplus.avepay_cpocket.view.fragments.MyDealFragment;
import com.aveplus.avepay_cpocket.view.fragments.NewsFragment;
import com.aveplus.avepay_cpocket.view.fragments.ProducerManagerFragment;
import com.aveplus.avepay_cpocket.view.fragments.SellerFragment;
import com.aveplus.avepay_cpocket.view.fragments.SettingsFragment;
import com.aveplus.avepay_cpocket.view.fragments.WebViewFragment;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.SubscribeService;

import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcceuilActivity extends BaseActivity implements IOnItemClickListener,
        NavigationView.OnNavigationItemSelectedListener,
        DealListFragment.IListenerDealsChange, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = AcceuilActivity.class.getSimpleName();
    private RecyclerView rcv_list;
    private ArrayList<Acceuil> listAcceuil;
    private Toolbar toolbar;
    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private CoordinatorLayout bgMain;
    private FragmentTransaction fragmentTransaction;
    private AllDealsFragment mAllDealsFragment;
    private FragmentFavorite mFavoriteFragment;
    private ContactFragment mContactFragment;
    private DealsFragment mFrgDeal;
    private ProducerManagerFragment mProManaFragment;
    private IwanaPayFragment mIwanaPayFragment;
    private MyAccountMyInfoFragment myAccountFragment;
    private WebViewFragment mWebViewFragment;
    private ArrayList<MenuLeft> menuLefts;
    private RecyclerView lvMenu;
    private CircleImageView imgAvatar;
    private TextViewBold tvName, tvEmail;
    private MenuLeftAdapte adapter;
    private static final int RC_LOCATION_PERMISSION_TO_UPDATE_DRIVER_LOCATION = 1;
    private static final int RC_TURN_ON_LOCATION_TO_UPDATE_DRIVER_LOCATION = 2;
    private static final int RC_LOCATION_PERMISSION_TO_UPDATE_USER_LOCATION = 3;
    private static final int RC_TURN_ON_LOCATION_TO_UPDATE_USER_LOCATION = 4;
    private static final String FRAG_HOME = "Home";
    private static final String FRAG_DEAL_MANAGER = "dealManager";
    private static final String FRAG_SELLER_MANAGER = "sellerManager";
    private static final String FRAG_BUYER_MANAGER = "buyerManager";
    private static final String FRAG_ALL_DEALS = "allDeals";
    private static final String FRAG_NEWS = "newsAndEvents";
    private static final String FRAG_SETTINGS = "settings";
    private static final String FRAG_IWANA_PAY = "iwanapay";
    private static final String FRAG_MY_ACCOUNT = "myaccount";
    private static final String FRAG_MY_DEAL = "mydeal";
    private static final String FRAG_IWANA_CHAT = "FRAG_IWANA_CHAT";
    private static final String FRAG_ABOUT = "FRAG_ABOUT";
    private static final String FRAG_FAQ = "FRAG_FAQ";
    private static final String FRAG_WEBVIEW = "FRAG_WEBVIEW";
    private DealManagerFragment mFrgDealManager;
    private SellerFragment mSellerFragment;
    private NewsFragment mNewsFragment;
    private SettingsFragment mSettingsFragment;
    private MyDealFragment mFrgMyDeal;
    private GoogleApiClient mGoogleApiClient;
    private boolean userLocationIsUpdated;
    private int indexMenu = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "Token: " + DataStoreManager.getToken());
        initGoogleApiClient();
        // Get saved instances
        if (savedInstanceState != null) {
            mFavoriteFragment = (FragmentFavorite) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_HOME);
            mFrgDealManager = (DealManagerFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_DEAL_MANAGER);
            mSellerFragment = (SellerFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_SELLER_MANAGER);
            mFrgDeal = (DealsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_BUYER_MANAGER);
            // mAllDealsFragment = (AllDealsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_ALL_DEALS);
            mNewsFragment = (NewsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_NEWS);
//            mSettingsFragment = (SettingsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_SETTINGS);
            mIwanaPayFragment = (IwanaPayFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_IWANA_PAY);
            myAccountFragment = (MyAccountMyInfoFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_MY_ACCOUNT);
            mFrgMyDeal = (MyDealFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_MY_DEAL);
            mContactFragment = (ContactFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_IWANA_CHAT);
            mWebViewFragment = (WebViewFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_WEBVIEW);
        }


        // Init quickblox
        if (DataStoreManager.getUser() != null && (DataStoreManager.getToken() != null
                && !DataStoreManager.getToken().equals(""))) {
            initSession(savedInstanceState);
            initDialogsListener();
            initPushManager();
        }

        // Start location service in some cases(driver closes app without deactivating)
        updateDriverLocation();

        // Get contacts from scratch
        getContacts();

        AppUtil.logSizeMultiScreen(this);
    }

    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(self)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private void updateDriverLocation() {
        if (DataStoreManager.getUser() != null && DataStoreManager.getUser().getDriverData() != null) {
            if (DataStoreManager.getUser().getDriverData().isAvailable()) {
                LocationService.start(self, LocationService.REQUEST_LOCATION);
            }
        }
    }

    private void getContacts() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            ModelManager.getContacts(self, false, new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    Log.e(TAG, "contacts " + new Gson().toJson(object));
                    JSONObject jsonObject = (JSONObject) object;
                    if (JSONParser.responseIsSuccess(jsonObject)) {
                        ArrayList<ContactObj> contactObjs = JSONParser.parseContacts(jsonObject);
                        if (contactObjs.size() > 0) {
                            // Save contacts into preference
                            DataStoreManager.saveContactsList(contactObjs);
                        }
                    }
                }

                @Override
                public void onError() {
                }
            });
        }
    }

    @Override
    public void onChanged(ArrayList<DealObj> mDealObj) {

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (GlobalFunctions.locationIsGranted(self, Args.RC_LOCATION, null)) {
            MapsUtil.getMyLocation(mGoogleApiClient, new IMaps() {
                @Override
                public void processFinished(Object obj) {
                    Location location = (Location) obj;
                    AppController.getInstance().setMyLocation(location);
                }
            });

            updateUserLocation();
        }
    }

    private void updateUserLocation() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            if (!userLocationIsUpdated) {
                if (DataStoreManager.getUser() != null) {
                    if ((DataStoreManager.getUser().getDriverData() != null
                            && !DataStoreManager.getUser().getDriverData().isAvailable())
                            || DataStoreManager.getUser().getDriverData() == null) {
                        if (GlobalFunctions.locationIsGranted(self, RC_LOCATION_PERMISSION_TO_UPDATE_USER_LOCATION, null)) {
                            if (MapsUtil.locationIsEnable(self)) {
                                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                                    MapsUtil.getMyLocation(mGoogleApiClient, new IMaps() {
                                        @Override
                                        public void processFinished(Object obj) {
                                            Location location = (Location) obj;

                                            ModelManager.updateLocation(self, new LatLng(location.getLatitude(), location.getLongitude()));
                                            userLocationIsUpdated = true;
                                        }
                                    });
                                }
                            } else {
                                MapsUtil.displayLocationSettingsRequest(self, RC_TURN_ON_LOCATION_TO_UPDATE_USER_LOCATION);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void inflateLayout() {
        setContentView(R.layout.activity_acceuil);
    }

    @Override
    protected void initUI() {

        rcv_list = findViewById(R.id.rcv_list);
        bgMain = (CoordinatorLayout) findViewById(R.id.bg_main);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Acceuil");
        setSupportActionBar(toolbar);

        initNavigationView();
        initMenuLeft();
        mNavigationView.setNavigationItemSelectedListener(this);
        switchMenu(Constants.MENU_ALL_DEAL);
        listAcceuil = new ArrayList<>();
        listAcceuil.add(new Acceuil("movie", R.drawable.transfert));
        listAcceuil.add(new Acceuil("shopping", R.drawable.panier));
        listAcceuil.add(new Acceuil("music", R.drawable.pay_bills));
        listAcceuil.add(new Acceuil("ebook", R.drawable.remittance));
        listAcceuil.add(new Acceuil("restaurant", R.drawable.parking));
        listAcceuil.add(new Acceuil("training center", R.drawable.my_account));
        listAcceuil.add(new Acceuil("Taxi", R.drawable.future));

        AcceuilAdapter acceuilAdapter = new AcceuilAdapter(listAcceuil, self);
        rcv_list.setLayoutManager(new GridLayoutManager(self, 3, LinearLayoutManager.VERTICAL, false));
        acceuilAdapter.setListener(this);
        rcv_list.setAdapter(acceuilAdapter);
    }

    @Override
    protected void initControl() {

    }

    @Override
    public void onItemClick(int position) {

        Bundle bundle = getIntent().getExtras();
        if (position == 1) {
            if (bundle != null) {
                GlobalFunctions.startActivityWithoutAnimation(self, CategoryActivity.class, bundle);
            } else {
                GlobalFunctions.startActivityWithoutAnimation(self, CategoryActivity.class);
            }
        } else if (position == 2) {
            if (bundle != null) {
                GlobalFunctions.startActivityWithoutAnimation(self, MainActivity.class, bundle);
            } else {
                GlobalFunctions.startActivityWithoutAnimation(self, MainActivity.class);
            }

//            Music
        } else if (position == 3) {
            if (bundle != null) {
                GlobalFunctions.startActivityWithoutAnimation(self, MainActivity2.class, bundle);
            } else {
                GlobalFunctions.startActivityWithoutAnimation(self, MainActivity2.class);
            }
        } else if (position == 4) {
            if (bundle != null) {
                GlobalFunctions.startActivityWithoutAnimation(self, com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity.class, bundle);
            } else {
                GlobalFunctions.startActivityWithoutAnimation(self, com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity.class);
            }
        } else if (position == 5) {
            if (bundle != null) {
                GlobalFunctions.startActivityWithoutAnimation(self, com.aveplus.avepay_cpocket.restaurant.main.mainAct.MainActivity.class, bundle);
            } else {
                GlobalFunctions.startActivityWithoutAnimation(self, com.aveplus.avepay_cpocket.restaurant.main.mainAct.MainActivity.class);
            }
        } else if (position == 6) {
            if (bundle != null) {
                GlobalFunctions.startActivityWithoutAnimation(self, HomeActivity.class, bundle);
            } else {
                GlobalFunctions.startActivityWithoutAnimation(self, HomeActivity.class);
            }
        } else if (position == 7) {
            Log.d(TAG, "onItemClick: "+position);
            if (bundle != null) {
                GlobalFunctions.startActivityWithoutAnimation(self, com.aveplus.avepay_cpocket.taxi.view.activities.SplashLoginActivity.class, bundle);
            } else {
                GlobalFunctions.startActivityWithoutAnimation(self, com.aveplus.avepay_cpocket.taxi.view.activities.SplashLoginActivity.class);
            }
        }

    }

    private void initNavigationView() {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AppUtil.hideSoftKeyboard(AcceuilActivity.this);

//                updateMenuHeader();
            }
        };
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        return super.onCreateOptionsMenu(menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            GlobalFunctions.startActivityWithoutAnimation(self, SearchActivity.class);
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void switchMenu(int idMenuLeft) {
        // setbackGroundMainHaveData();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (idMenuLeft) {
            case Constants.MENU_ALL_DEAL:
                getSupportFragmentManager().popBackStack();
                break;


            case Constants.MENU_CHAT:
                if (mContactFragment == null) {
                    mContactFragment = ContactFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main1, mContactFragment).addToBackStack("chat").commit();

                setTitle(R.string.chat);
                break;


            case Constants.MENU_PROFILE:
                if (myAccountFragment == null) {
                    myAccountFragment = MyAccountMyInfoFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main1, myAccountFragment).addToBackStack("profile").commit();

                setTitle(R.string.profile);
                break;


            case Constants.MENU_SHARE:
                AppUtil.share(this, "http://play.google.com/store/apps/details?id=" + new PacketUtility().getPacketName());

                setTitle(R.string.share);

                break;

            case Constants.MENU_FAQ:
                showScreenFaq();
                break;

//            case Constants.MENU_SETTING:
//                fragmentTransaction.replace(R.id.frl_main1, SettingsFragment.newInstance()).addToBackStack("setting").commit();
//                setTitle(R.string.settings);
//                break;

            case Constants.MENU_HELP:
                showScreenHelp();
                break;

            case Constants.MENU_ABOUT_US:
                showScreenAboutUs();
                break;

            case Constants.MENU_LOGOUT:
                logout();
                break;
        }

        mDrawer.closeDrawer(GravityCompat.START);

    }

    public void setbackGroundMainHaveData() {
        bgMain.setBackgroundResource(R.drawable.bg_deal);
    }

    public void setbackGroundMainNoData() {
        bgMain.setBackgroundColor(getResources().getColor(R.color.colorBackgroundNodata));
    }

    private void showScreenFaq() {
        SettingsObj setting = DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                ModelManager.getSettingUtility(this, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        JSONObject jsonObject = (JSONObject) object;
                        ApiResponse apiResponse = new ApiResponse(jsonObject);
                        if (!apiResponse.isError()) {
                            DataStoreManager.saveSettingUtility(jsonObject.toString());
                            SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.faq), utitlityObj.getFaq());

                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.KEY_URL, utitlityObj.getFaq());
                            mWebViewFragment = WebViewFragment.newInstance(bundle);

                            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                            setTitle(getString(R.string.faq));

//                                    openWebView(getString(R.string.about_us), DataStoreManager.getSettingUtility().getAbout());
                        }

                    }

                    @Override
                    public void onError() {
                    }
                });
            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getFaq());
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

            setTitle(getString(R.string.faq));

//                    openWebView(getString(R.string.faq), DataStoreManager.getSettingUtility().getFaq());
        }
    }

    private void showScreenHelp() {
        SettingsObj setting = DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                ModelManager.getSettingUtility(this, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        JSONObject jsonObject = (JSONObject) object;
                        ApiResponse apiResponse = new ApiResponse(jsonObject);
                        if (!apiResponse.isError()) {
                            DataStoreManager.saveSettingUtility(jsonObject.toString());
                            SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.help), utitlityObj.getHelp());

                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.KEY_URL, utitlityObj.getHelp());
                            mWebViewFragment = WebViewFragment.newInstance(bundle);

                            fragmentTransaction.replace(R.id.frl_main1, mWebViewFragment).addToBackStack("help").commit();

                            setTitle(getString(R.string.help));

                        } else {
                            AppUtil.showToast(getApplicationContext(), apiResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError() {
                        AppUtil.showToast(getApplicationContext(), "Error!");
                    }
                });
            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getHelp());
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main1, mWebViewFragment).commit();

            setTitle(getString(R.string.help));

//                    openWebView(getString(R.string.help), DataStoreManager.getSettingUtility().getHelp());
        }
    }

    private void showScreenAboutUs() {

        SettingsObj setting = DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                ModelManager.getSettingUtility(this, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        JSONObject jsonObject = (JSONObject) object;
                        ApiResponse apiResponse = new ApiResponse(jsonObject);
                        if (!apiResponse.isError()) {
                            DataStoreManager.saveSettingUtility(jsonObject.toString());
                            SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.about_us), utitlityObj.getAbout());

                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.KEY_URL, utitlityObj.getAbout());
                            mWebViewFragment = WebViewFragment.newInstance(bundle);

                            fragmentTransaction.replace(R.id.frl_main1, mWebViewFragment).addToBackStack("about").commit();

                            setTitle(getString(R.string.about_us));
                        }
                    }

                    @Override
                    public void onError() {
                    }
                });
            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }
        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getAbout());
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main1, mWebViewFragment).commit();

            setTitle(getString(R.string.about_us));
//                    openWebView(getString(R.string.about_us), DataStoreManager.getSettingUtility().getAbout());
        }
    }

    private void logout() {
        showDialogLogout();

    }

    private void showDialogLogout() {
        //Tạo đối tượng
        AlertDialog.Builder b = new AlertDialog.Builder(this);
//Thiết lập tiêu đề
        b.setTitle("You need logout?");
// Nút Ok
        b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ApiUtils.getAPIService().logout(DataStoreManager.getToken()).enqueue(new Callback<BaseReponse>() {
                    @Override
                    public void onResponse(Call<BaseReponse> call, Response<BaseReponse> response) {
                        if (response.body() != null) {
                            if (response.body().isSuccess(self)) {
//                    AppUtil.showToast(self, "logout");
                                AppUtil.startActivity(self, SplashLoginActivity.class);
                                DataStoreManager.removeUser();
                                finish();
                            } else {
                                Log.e(TAG, "eror1: ");
                            }
                        } else {
                            Log.e(TAG, "eror2: ");
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseReponse> call, Throwable t) {
                        Log.e(TAG, "onFailure: " + t.getMessage());
                    }
                });

            }
        });
//Nút Cancel
        b.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
//Tạo dialog
        AlertDialog al = b.create();

        al.show();

    }

    private void requestLogout() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            final MyProgressDialog progressDialog = new MyProgressDialog(self);
            progressDialog.show();

            if (QbAuthUtils.isSessionActive()) {
                ChatHelper.getInstance().logout(new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        processBeforeLoggingOut(progressDialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Log.e(TAG, "Log out - onError: " + e.getMessage());
                    }
                });
            } else {
                ChatHelper.getInstance().login(self, SharedPreferencesUtil.getQbUser(), new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        processBeforeLoggingOut(progressDialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Toast.makeText(self, "Fail to logout: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }

    private void processBeforeLoggingOut(MyProgressDialog progressDialog) {
        if (DataStoreManager.getUser() != null && DataStoreManager.getUser().getDriverData() != null) {
            if (DataStoreManager.getUser().getDriverData().isAvailable()) {
                LocationService.start(self, LocationService.STOP_REQUESTING_LOCATION);

                // Deactivate driver's mode before logging out
                ModelManager.activateDriverMode(self, Constants.OFF, 0, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                    }

                    @Override
                    public void onError() {
                    }
                });
            }
        }

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        SubscribeService.unSubscribeFromPushes(self);
        QBChatService.getInstance().destroy();
        SharedPreferencesUtil.removeQbUser();
        QbDialogHolder.getInstance().clear();

        DataStoreManager.clearUserToken();
        AppController.getInstance().setUserUpdated(true);
        AppUtil.startActivity(self, SplashLoginActivity.class);
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initListMenu() {
        menuLefts = new ArrayList<>();

        menuLefts.add(new MenuLeft(Constants.MENU_ALL_DEAL, R.drawable.ic_all_deals_white,
                getResources().getString(R.string.home), false));
//        menuLefts.add(new MenuLeft(Constants.MENU_FAVORITE, R.drawable.ic_favorite_border_white_24dp,
//                getResources().getString(R.string.favorite), false ));
        menuLefts.add(new MenuLeft(Constants.MENU_CHAT, R.drawable.ic_chatbubble_,
                getResources().getString(R.string.chat), false));
//        menuLefts.add(new MenuLeft(Constants.MENU_BUYER_NAMAGER, R.drawable.ic_buyer_white,
//                getResources().getString(R.string.buy_deals), false));
//        menuLefts.add(new MenuLeft(Constants.MENU_SELLER, R.drawable.ic_seller_white,
//                getResources().getString(R.string.sell_deals), false));
//        menuLefts.add(new MenuLeft(Constants.MENU_PAYMENT, R.drawable.ic_payment_white,
//                getResources().getString(R.string.payment), false));
        menuLefts.add(new MenuLeft(Constants.MENU_PROFILE, R.drawable.ic_profiles_white,
                getResources().getString(R.string.profile), false));
//        menuLefts.add(new MenuLeft(Constants.MENU_NEW_EVENT, R.drawable.ic_new_event,
//                getResources().getString(R.string.news_and_events), false));
        menuLefts.add(new MenuLeft(Constants.MENU_SHARE, R.drawable.ic_share_white,
                getResources().getString(R.string.share), false));
        menuLefts.add(new MenuLeft(Constants.MENU_FAQ, R.drawable.ic_faq,
                getResources().getString(R.string.faq), false));
//        menuLefts.add(new MenuLeft(Constants.MENU_SETTING, R.drawable.ic_setting_white,
//                getResources().getString(R.string.settings), false));
        menuLefts.add(new MenuLeft(Constants.MENU_HELP, R.drawable.ic_help_white,
                getResources().getString(R.string.help), false));
        menuLefts.add(new MenuLeft(Constants.MENU_ABOUT_US, R.drawable.ic_aboutus_white,
                getResources().getString(R.string.about_us), false));
        menuLefts.add(new MenuLeft(Constants.MENU_LOGOUT, R.drawable.ic_signout_white,
                getResources().getString(R.string.log_out), false));


    }

    private void initMenuLeft() {
        imgAvatar = (CircleImageView) findViewById(R.id.img_avatar);
        tvName = (TextViewBold) findViewById(R.id.lbl_deal_name);
        tvEmail = (TextViewBold) findViewById(R.id.tv_email);
        lvMenu = (RecyclerView) findViewById(R.id.lv_menu);

        initMenuLeftHeader();

        initListMenu();

        adapter = new MenuLeftAdapte(menuLefts, self, new IOnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (indexMenu != position) {
                    menuLefts.get(indexMenu).setSelected(false);
                    indexMenu = position;
                    switchMenu(menuLefts.get(position).getId());
                    menuLefts.get(position).setSelected(true);
                } else mDrawer.closeDrawer(GravityCompat.START);

                adapter.notifyDataSetChanged();
            }
        }
        );
        LinearLayoutManager manager = new LinearLayoutManager(self);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        lvMenu.setAdapter(adapter);
        lvMenu.setLayoutManager(manager);
        lvMenu.setHasFixedSize(true);
    }

    private void initMenuLeftHeader() {
        Log.e(TAG, "User: " + DataStoreManager.getUser());

        UserObj obj = DataStoreManager.getUser();
        if (obj != null) {
            tvName.setText(obj.getName());
            tvEmail.setText(obj.getEmail());
            ImageUtil.setImage(self, imgAvatar, obj.getAvatar());
        }

//        UserObj userObj = DataStoreManager.getUser();
//        if (userObj != null) {
//            tvName.setText(userObj.getName());
//            tvEmail.setText(userObj.getEmail());
//            ImageUtil.setImage(self, imgAvatar, userObj.getAvatar());
//        }
    }

    public void updateMenuLeftHeader() {
        if (AppController.getInstance().isUserUpdated()) {
            UserObj userObj = DataStoreManager.getUser();
            if (userObj != null) {
                tvName.setText(userObj.getName());
                tvEmail.setText(userObj.getEmail());
                ImageUtil.setImage(self, imgAvatar, userObj.getAvatar());
            }

            AppController.getInstance().setUserUpdated(false);
        }
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        if(indexMenu !=0){
//
//        }
//    }
}
