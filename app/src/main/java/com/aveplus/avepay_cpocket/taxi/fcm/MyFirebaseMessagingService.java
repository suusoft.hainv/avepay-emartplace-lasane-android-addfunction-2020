package com.aveplus.avepay_cpocket.taxi.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.configs.Action;
import com.aveplus.avepay_cpocket.taxi.configs.ChatConfigs;
import com.aveplus.avepay_cpocket.taxi.configs.Config;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.globals.Constants;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.objects.InfoReceive;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.BaseModel;
import com.aveplus.avepay_cpocket.taxi.view.activities.MainActivity;
import com.aveplus.avepay_cpocket.taxi.view.activities.SplashLoginActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    private static int REQUEST_CODE = 0;
    private static int NOTIFICATION_ID = 0;
    private static String notifi_chanel = "notify_001";

    public static final String KEY_MSG = "message";
    public static final String KEY_TITLE = "title";
    private String keyData = "additionalData";
    private String message = "";

    @Override
    public void onNewToken(String refreshedToken) {
        Log.e(TAG, "Refreshed token: " + refreshedToken);

        // Save fcm token into preference
        GlobalFunctions.saveFCMToken(this, refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);

    }

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {

        if (Config.isShowToast)
            EventBus.getDefault().post(new Global.NotifiShowToast(token));

        if (DataStoreManager.getUser() != null) {
            ApiUtils.getAPIService().updateGcmId(DataStoreManager.getTokenUser(), token, "1")
                    .enqueue(new Callback<BaseModel>() {
                        @Override
                        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {

                        }

                        @Override
                        public void onFailure(Call<BaseModel> call, Throwable t) {

                        }
                    });
        }


    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom() + ", message: " + remoteMessage.getData().get("message")
                + " content " + remoteMessage.getData().get("content"));


        // Check if message contains a data payload.
        String messagebody = "", type = "";
        if (remoteMessage.getData().size() > 0) {
            message = remoteMessage.getData().get(Args.MESSAGE);
            type = remoteMessage.getData().get(Args.NOTIFICATION_TYPE);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        if (type.equals(ChatConfigs.TYPE_DEAL)) {
            String id = remoteMessage.getData().get(Args.BALANCE);
            sendNewDeal(message, ChatConfigs.TYPE_DEAL, id);
        } else if (type.equals(Args.TYPE_BALANCE)) {
            UserObj user = DataStoreManager.getUser();
            String balance = remoteMessage.getData().get(Args.BALANCE);
            user.setBalance(Float.parseFloat(balance));
            DataStoreManager.saveUser(user);
            if (AppController.getInstance().getiRedeem() != null) {
                AppController.getInstance().getiRedeem().updateBalace(user.getBalance());
            }

            sendNotification(message);
        } else if (type.equals("trip")) {

            processingDataTrip(remoteMessage);

        } else {
            sendNotification(message);
        }
    }

    // [END receive_message]

    /**
     * Xử lý trip instant booking
     */
    private void processingDataTrip(RemoteMessage remoteMessage) {

        Log.e(TAG, "processingDataTrip: " + remoteMessage.toString());
        //app đang được mở
        if (remoteMessage.getData().containsKey(keyData)) {
            message = remoteMessage.getData().get(Args.MESSAGE);

            String data = remoteMessage.getData().get(keyData);
            InfoReceive infoReceive = new Gson().fromJson(data, InfoReceive.class);

            Log.e("trip", "infoReceive: " + data);
            if (infoReceive != null) {
                if (infoReceive.action.equals(Action.ACTION_ACCEPT)) {
                    message = getString(R.string.the_driver_has_accepted_your_trip);
                    //sendNotification("The trip has accecpt.");
                    Intent intent = new Intent(Action.ACTION_ACCEPT);
                    intent.putExtra(Args.KEY_INFO_RECEIVE, infoReceive);
                    sendBroadcast(intent);
                    EventBus.getDefault().post(new Global.PushDriverAccept());


                } else if (infoReceive.action.equals(Action.ACTION_DRIVE_PUT_TIME)) {

                    message = getString(R.string.the_driver_has_confirmed_the_time_to_pick_you_up);
                    EventBus.getDefault().post(new Global.PushDriverEstimateTime());

                } else if (infoReceive.action.equals(Action.ACTION_ACCEPT_DEAL)) {

                    message = getString(R.string.there_is_a_driver_who_has_agreed_to_negociate_the_price_of_the_trip_with_you);
                    EventBus.getDefault().post(new Global.PushDriverAcceptDeal());

                } else if (infoReceive.action.equals(Action.ACTION_DRIVER_DEAL)) {

                    message = getString(R.string.there_is_a_driver_who_has_agreed_to_negociate_the_price_of_the_trip_with_you);
                    EventBus.getDefault().post(new Global.PushDriverAcceptDeal());

                } else if (infoReceive.action.equals(Action.ACTION_CANCEL)) {
                    message = getString(R.string.your_trip_has_been_canceled);
                    sendBroadcast(new Intent(Action.ACTION_CANCEL));
                    EventBus.getDefault().post(new Global.PushCancelTrip());

                } else if (infoReceive.action.equals(Action.ACTION_FINISHED)) {
                    message = getString(R.string.your_trip_is_complete);
                    //sendNotification("The trip has completed.");
                    sendBroadcast(new Intent(Action.ACTION_FINISHED));
                }
                Log.e(TAG, "message : " + message);
                sendNotification(message);

            } else
                sendNotification(remoteMessage.getData());
        }
    }


    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }

        Log.e("notif", "sendNotification");
        Intent intent = new Intent(this, SplashLoginActivity.class);
        REQUEST_CODE++;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, notifi_chanel)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NOTIFICATION_ID++;
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }


    private void sendNewDeal(String messageBody, String notificationType, String idDeal) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }

        Log.e("notif", "sendNewDeal");
        Intent intent = new Intent(this, SplashLoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        bundle.putString(Args.KEY_ID_DEAL, idDeal);
        bundle.putString(Args.NOTIFICATION_TYPE, notificationType);
        intent.putExtras(bundle);

        REQUEST_CODE++;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(Integer.parseInt(idDeal), notificationBuilder.build());
    }

    private void sendMessage(boolean negotiated) {
        Log.e("notif", "sendMessage");
        // The string "my-integer" will be used to filer the intent
        Intent intent = new Intent(Constants.INTENT_ACTION_UPDATE_MENU);
        // Adding some data
        intent.putExtra(Args.NEGOTIATED, negotiated);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    // xu ly trip instant booking

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */

    private void sendNotification(Map<String, String> messageBody) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }


        Bundle bundle = new Bundle();
        if (messageBody.containsKey(keyData)) {
            bundle.putString(Args.KEY_EXTRA_DATA, messageBody.get(keyData));
            Log.e("send data ", "send data : " + messageBody.get(keyData));
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, notifi_chanel);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent openUI = new Intent(this, MainActivity.class);
        openUI.putExtras(bundle);

        openUI.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE, openUI,
                PendingIntent.FLAG_CANCEL_CURRENT);

        {
            notificationBuilder
                    .setOnlyAlertOnce(true)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setSound(defaultSoundUri)
                    .setColor(AppController.getInstance().getResources().getColor(R.color.grey))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true).setOngoing(true).setDeleteIntent(pendingIntent)
                    .setContentTitle(getString(R.string.app_name))
//                    .setContentTitle(messageBody.get(KEY_TITLE))
                    .setContentText(messageBody.get(KEY_MSG))
                    .setSmallIcon(R.mipmap.ic_launcher);
        }

        notificationManager.notify(NOTIFICATION_ID /* ID of notification */, notificationBuilder.build());

    }


    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        NotificationManager
                mNotificationManager =
                (NotificationManager)
                        getSystemService(Context.NOTIFICATION_SERVICE);
        // The id of the channel.
        String id = notifi_chanel;
        // The user-visible name of the channel.
        CharSequence name = "Media playback";
        // The user-visible description of the channel.
        String description = "Media playback controls";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        // Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        mChannel.enableLights(true);
        mChannel.enableVibration(true);

        mNotificationManager.createNotificationChannel(mChannel);
    }
}
