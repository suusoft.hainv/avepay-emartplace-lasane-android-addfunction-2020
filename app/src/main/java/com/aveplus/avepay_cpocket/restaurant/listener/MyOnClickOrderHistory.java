package com.aveplus.avepay_cpocket.restaurant.listener;


import com.aveplus.avepay_cpocket.restaurant.main.history.DetailOrder;

public interface MyOnClickOrderHistory {
    void onClick(DetailOrder orderObj, int position);
}
