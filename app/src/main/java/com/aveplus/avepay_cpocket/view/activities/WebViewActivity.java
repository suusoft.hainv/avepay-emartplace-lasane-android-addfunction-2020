package com.aveplus.avepay_cpocket.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.globals.Constants;

/**
 * Created by Suusoft on 24/12/2016.
 */

public class WebViewActivity extends com.aveplus.avepay_cpocket.base.BaseActivity {
    private WebView webView;
    private String url;
    String title;
    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_web_view;
    }

    @Override
    protected void getExtraData(Intent intent) {
        Bundle bundle = getIntent().getExtras();
        title = bundle.getString(Constants.KEY_TITLE);
        Log.e("PP",title);

        url = bundle.getString(Constants.KEY_URL);
        Log.e("PP",url);
    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {
        setToolbarTitle(title);
        webView = (WebView) findViewById(R.id.web_view);
    }

    @Override
    protected void onViewCreated() {
        setUpWebView(webView);
    }

    private void setUpWebView(WebView webView) {
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                return super.shouldOverrideUrlLoading(view, request);
            }
        });

    }
}
