package com.aveplus.avepay_cpocket.restaurant.main.save;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.RestaurantAct;
import com.aveplus.avepay_cpocket.restaurant.main.save.savemodels.RestaurantInfo;
import com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeFavourite;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeListSave;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.util.DialogUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.ViewDialog;
import com.aveplus.avepay_cpocket.restaurant.widgets.recyclerview.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.ID_RESTAURANT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LAT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LONG;


public class SaveFragment extends BaseFragment implements EndlessRecyclerViewScrollListener.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = SaveFragment.class.getSimpleName();
    private RecyclerView rvListSave;
    private ArrayList<RestaurantInfo> list;
    private SaveAdapter saveAdapter;
    private double longitude = 105.777013;
    private double latitude = 21.037464;
    private LinearLayout llnoData, llnoConnect;
    private int page = 1;
    private EndlessRecyclerViewScrollListener scrollListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayoutManager manager;

    @Override
    protected int getLayoutInflate() {
        return R.layout.save_fragment;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        rvListSave = view.findViewById(R.id.rv_list_save);
        llnoData = view.findViewById(R.id.ll_no_data);
        llnoConnect = view.findViewById(R.id.ll_no_connection);
        checkNetworkshow();
        manager = new LinearLayoutManager(self);
        rvListSave.setLayoutManager(manager);
        scrollListener = new EndlessRecyclerViewScrollListener(this, manager);
        rvListSave.addOnScrollListener(scrollListener);
        scrollListener.setEnded(false);
        swipeRefreshLayout.setOnRefreshListener(this);
        setAdapter();


    }

    private void setAdapter() {
        list = new ArrayList<>();
        saveAdapter = new SaveAdapter(getActivity(), list, new SaveAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RestaurantInfo restaurantInfo) {
                Intent intent = new Intent(self, RestaurantAct.class);
                intent.putExtra(USER_LONG, longitude);
                intent.putExtra(USER_LAT, latitude);
                intent.putExtra(ID_RESTAURANT, restaurantInfo.getId());
                startActivity(intent);
            }
        });

        saveAdapter.setUnsaveListener(new SaveAdapter.ItemimgUnsaveListener() {
            @Override
            public void onUnsaveclick(View view, int position) {
                int id = list.get(position).getId();
                Log.e(TAG, "onUnsaveclick: " + id);
                DialogUtil.showAlertDialog(self, R.string.msg_remove_save, new DialogUtil.IDialogConfirm() {
                    @Override
                    public void onClickOk() {
                        list.remove(position);
                        saveAdapter.notifyDataSetChanged();
                        ApiUtils.getAPIService().Favourite(DataStoreManager.getToken(), id, "restaurant").enqueue(new Callback<ResponeFavourite>() {
                            @Override
                            public void onResponse(Call<ResponeFavourite> call, Response<ResponeFavourite> response) {
                                Log.e(TAG, "onResponse: success");
                            }

                            @Override
                            public void onFailure(Call<ResponeFavourite> call, Throwable t) {
                                Log.e(TAG, "onFailure: " + t.getMessage());
                            }
                        });
                    }
                });

            }
        });
        rvListSave.setAdapter(saveAdapter);
        rvListSave.hasFixedSize();
    }

    @Override
    protected void getData() {
        if (NetworkUtility.isNetworkAvailable()) {
            ViewDialog dialog = new ViewDialog(getActivity());
            dialog.showDialog();
            APIService apiService = ApiUtils.getAPIService();
            apiService.FavouriteList(DataStoreManager.getToken(), "restaurant", page, 5).enqueue(new Callback<ResponeListSave>() {
                @Override
                public void onResponse(Call<ResponeListSave> call, Response<ResponeListSave> response) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (response.body() != null) {
                        if (response.body().isSuccess()) {
                            for (int i = 0; i < response.body().getData().size(); i++) {
                                list.add(response.body().getData().get(i).getRestaurantInfo());
                                for (int j = 0; j < i; j++) {
                                    list.add(response.body().getData().get(j).getRestaurantInfo());
                                    scrollListener.onLoadMoreComplete();
                                }
                                //                        AppUtil.showToast(self, response.body().getData().get(i).getId()+"");
                            }

                            saveAdapter.notifyDataSetChanged();
                            dialog.hideDialog();
                            Log.e("size: ", response.body().getData().size() + "");
                        } else {
                            AppUtil.showToast(self, "OnFail");
                        }
                    } else {
                        dialog.hideDialog();
                        AppUtil.showToast(self, "Have some errror in getiting data");
                    }


                }

                @Override
                public void onFailure(Call<ResponeListSave> call, Throwable t) {
                    Toast.makeText(self, "not data", Toast.LENGTH_SHORT).show();
                    Log.e("OnFail: ", t.getMessage());
                    dialog.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        } else {
            AppUtil.showToast(self, getString(R.string.msg_no_network));
        }
    }

    private void checkNetworkshow() {
        if (NetworkUtility.isNetworkAvailable()) {
            llnoConnect.setVisibility(View.GONE);
        } else {
            llnoConnect.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onRefresh() {
        page = 1;
        list.clear();
        scrollListener.setEnded(false);
        scrollListener.setCurrentPage(page);
        getData();
    }

    @Override
    public void onLoadMore(int page) {
        this.page = page;
        getData();
    }

    private void checkViewHideShow() {
        if (list.size() == 0) {
            llnoData.setVisibility(View.VISIBLE);
        } else {
            llnoData.setVisibility(View.GONE);
        }
    }


}
