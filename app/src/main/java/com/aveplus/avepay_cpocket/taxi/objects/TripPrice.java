package com.aveplus.avepay_cpocket.taxi.objects;

public class TripPrice {

    public double distance;
    public double duration;
    public double estimate_fare;

}
