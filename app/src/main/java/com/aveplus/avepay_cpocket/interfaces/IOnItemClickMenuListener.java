package com.aveplus.avepay_cpocket.interfaces;

/**
 * Created by Suusoft on 12/16/2016.
 */

public interface IOnItemClickMenuListener  {
    void onClickPay(int position);
    void onClickRate(int position);

}


