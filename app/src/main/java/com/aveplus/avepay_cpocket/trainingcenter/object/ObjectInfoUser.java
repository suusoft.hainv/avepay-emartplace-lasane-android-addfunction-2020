package com.aveplus.avepay_cpocket.trainingcenter.object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SuuSoft on 01/09/2017.
 */

public class ObjectInfoUser implements Parcelable {
    private String nameUser;
    private String addressUser;
    private String mailUser;
    private String postCodeUser;
    private String phoneUser;
    private String nameReceiver;
    private String addressReceiver;
    private String phoneReceiver;
    private String postCodeReceiver;
    private String mailReceiver;

    public ObjectInfoUser() {
    }

    protected ObjectInfoUser(Parcel in) {
        nameUser = in.readString();
        addressUser = in.readString();
        mailUser = in.readString();
        postCodeUser = in.readString();
        phoneUser = in.readString();
        nameReceiver = in.readString();
        addressReceiver = in.readString();
        phoneReceiver = in.readString();
        postCodeReceiver = in.readString();
        mailReceiver = in.readString();
    }

    public static final Creator<ObjectInfoUser> CREATOR = new Creator<ObjectInfoUser>() {
        @Override
        public ObjectInfoUser createFromParcel(Parcel in) {
            return new ObjectInfoUser(in);
        }

        @Override
        public ObjectInfoUser[] newArray(int size) {
            return new ObjectInfoUser[size];
        }
    };

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getAddressUser() {
        return addressUser;
    }

    public void setAddressUser(String addressUser) {
        this.addressUser = addressUser;
    }

    public String getPostCodeUser() {
        return postCodeUser;
    }

    public void setPostCodeUser(String postCodeUser) {
        this.postCodeUser = postCodeUser;
    }

    public String getMailUser() {
        return mailUser;
    }

    public void setMailUser(String mailUser) {
        this.mailUser = mailUser;
    }

    public String getPhoneUser() {
        return phoneUser;
    }

    public void setPhoneUser(String phoneUser) {
        this.phoneUser = phoneUser;
    }

    public String getNameReceiver() {
        return nameReceiver;
    }

    public void setNameReceiver(String nameReceiver) {
        this.nameReceiver = nameReceiver;
    }

    public String getAddressReceiver() {
        return addressReceiver;
    }

    public void setAddressReceiver(String addressReceiver) {
        this.addressReceiver = addressReceiver;
    }

    public String getPhoneReceiver() {
        return phoneReceiver;
    }

    public void setPhoneReceiver(String phoneReceiver) {
        this.phoneReceiver = phoneReceiver;
    }

    public String getPostCodeReceiver() {
        return postCodeReceiver;
    }

    public void setPostCodeReceiver(String postCodeReceiver) {
        this.postCodeReceiver = postCodeReceiver;
    }

    public String getMailReceiver() {
        return mailReceiver;
    }

    public void setMailReceiver(String mailReceiver) {
        this.mailReceiver = mailReceiver;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameUser);
        dest.writeString(addressUser);
        dest.writeString(mailUser);
        dest.writeString(postCodeUser);
        dest.writeString(phoneUser);
        dest.writeString(nameReceiver);
        dest.writeString(addressReceiver);
        dest.writeString(phoneReceiver);
        dest.writeString(postCodeReceiver);
        dest.writeString(mailReceiver);
    }
}
