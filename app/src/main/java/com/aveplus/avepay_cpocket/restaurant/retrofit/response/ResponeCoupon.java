package com.aveplus.avepay_cpocket.restaurant.retrofit.response;

import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.coupon.Couponmodels;

/**
 * Created by suusoft.com on 11/12/19.
 */

public class ResponeCoupon extends BaseModel {

    public Couponmodels data;

    public Couponmodels getData() {
        return data;
    }

    public void setData(Couponmodels data) {
        this.data = data;
    }
}
