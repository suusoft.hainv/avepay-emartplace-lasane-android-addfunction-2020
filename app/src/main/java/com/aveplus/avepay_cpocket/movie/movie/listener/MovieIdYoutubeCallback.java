package com.aveplus.avepay_cpocket.movie.movie.listener;

public interface MovieIdYoutubeCallback {
    public  void onCompleted(String id);
    public void onFailed(Exception exception);
}
