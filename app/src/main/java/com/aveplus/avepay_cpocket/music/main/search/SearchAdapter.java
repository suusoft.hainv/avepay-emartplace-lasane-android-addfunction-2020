package com.aveplus.avepay_cpocket.music.main.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.main.home.ItemAlbumVM;
import com.aveplus.avepay_cpocket.music.main.home.ItemCategoryVM;
import com.aveplus.avepay_cpocket.music.main.home.ItemSongsVM;
import com.aveplus.avepay_cpocket.music.main.video2.vm.VideoVM;
import com.aveplus.avepay_cpocket.music.player.QueueManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.BaseViewHolder> implements ItemMenuActionSongListener {
    public static final int ITEM_NORMAL = 1;
    public static final int ITEM_FEATURE = 2;
    private ArrayList<SearchCategory> listData;
    private Context context;
    private int posSongItem = -1;

    public SearchAdapter(Context context, ArrayList<SearchCategory> mListCategory) {
        this.listData = mListCategory;
        this.context = context;
    }


    @Override
    public SearchAdapter.BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new SearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchAdapter.BaseViewHolder holder, int position) {
        SearchAdapter.ViewHolder viewHolder = (SearchAdapter.ViewHolder) holder;
        SearchCategory item = listData.get(position);
        if (item != null) {
            viewHolder.tvName.setText(item.getName());
            if (SearchCategory.SONGS.equals(item.getName())) {
                viewHolder.tvMore.setVisibility(View.GONE);
                posSongItem = position;
                viewHolder.rcvData.setPadding(0, 0, 0, 90);
                viewHolder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_song, item.getListSongs(), ItemSongsVM.class, this));
            } else if (SearchCategory.ARTIST.equals(item.getName())) {
                viewHolder.tvMore.setVisibility(View.GONE);
                viewHolder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_artist, item.getListArtist(), ItemCategoryVM.class));
                setOnClickViewMore(viewHolder.tvMore, item.getName(), position);
            } else if (SearchCategory.VIDEOS.equals(item.getName())) {
                viewHolder.tvMore.setVisibility(View.GONE);
                viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_video_list, item.getListVideo(), VideoVM.class));
                viewHolder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            } else {
                viewHolder.tvMore.setVisibility(View.GONE);
                viewHolder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_album_home, item.getListAlbum(), ItemAlbumVM.class));
                setOnClickViewMore(viewHolder.tvMore, item.getName(), position);

            }
        }
    }


    private void setOnClickViewMore(TextView tvMore, final String object, final int type) {
        tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new Global.ViewMoreClicked(object, type));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public void onClickWatch(int position) {
        if (posSongItem != -1) {
            SearchCategory item = listData.get(posSongItem);
            QueueManager.getInstance().addNewQueue(item.getListSongs(), position);
            EventBus.getDefault().post(new Global.HomeSongItemClicked());
        }
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {

    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ViewHolder extends SearchAdapter.BaseViewHolder {
        TextView tvName, tvMore;
        RecyclerView rcvData;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMore = itemView.findViewById(R.id.tv_more);
            tvName = itemView.findViewById(R.id.tv_name);
            rcvData = itemView.findViewById(R.id.rcv_data);
            rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        }
    }
}
