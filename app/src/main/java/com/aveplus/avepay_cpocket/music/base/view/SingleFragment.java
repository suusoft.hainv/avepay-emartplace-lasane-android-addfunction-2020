package com.aveplus.avepay_cpocket.music.base.view;

import android.content.Context;
import android.os.Bundle;
import android.view.View;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class SingleFragment extends BaseFragmentBinding {

    public int layout;
    private Bundle bundle;
    public BaseViewModel viewModel;
    private Constructor<?> cons;

    public static SingleFragment newInstance(int layout, Class viewModelClass) {

        SingleFragment fragment = new SingleFragment();
        try {
            fragment.cons = viewModelClass.getConstructor(Context.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        fragment.layout = layout;
        return fragment;
    }

    public static SingleFragment newInstance(int layout, Class viewModelClass, Bundle bundle) {

        SingleFragment fragment = new SingleFragment();
        try {
            fragment.cons = viewModelClass.getConstructor(Context.class, Bundle.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        fragment.layout = layout;
        fragment.bundle = bundle;
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return layout;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        try {
            if (bundle == null) {
                viewModel = (BaseViewModel) cons.newInstance(self);
            } else {
                viewModel = (BaseViewModel) cons.newInstance(self, bundle);
            }
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return viewModel;
    }

    @Override
    protected void initView(View view) {

    }
}
