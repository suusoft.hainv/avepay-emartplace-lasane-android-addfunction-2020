package com.aveplus.avepay_cpocket.movie.movie.main.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.listener.IMyOnClickSub;
import com.aveplus.avepay_cpocket.movie.movie.model.listSub;

import java.util.ArrayList;

public class ListSubAdapter extends RecyclerView.Adapter<ListSubAdapter.MyViewHolder> {
    private static final String TAG = ListSubAdapter.class.getSimpleName();
    ArrayList<com.aveplus.avepay_cpocket.movie.movie.model.listSub> listSub;
    Context context;
    private IMyOnClickSub onClick;

    private int lastSelectedPosition = -1;
    RelativeLayout relativeLayout;

    public ListSubAdapter(ArrayList<listSub> listSub, Context context, IMyOnClickSub onClick) {
        this.listSub = this.listSub;
        this.context = context;
        this.onClick = onClick;
    }

    public void addList(ArrayList<listSub> listSub) {
        this.listSub = listSub;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ListSubAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_list_sub, viewGroup, false);

        return new ListSubAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListSubAdapter.MyViewHolder holder, int position) {
        final listSub listSubs = listSub.get(position);

        holder.tvListSub.setText(listSubs.getLanguage());
        holder.llListSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClick != null) {
                    onClick.myOnClick(position, listSubs);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return listSub == null ? 0 : listSub.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvListSub;
        LinearLayout llListSub;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvListSub = itemView.findViewById(R.id.tv_sub);
            llListSub = itemView.findViewById(R.id.ll_sub);
        }
    }
}
