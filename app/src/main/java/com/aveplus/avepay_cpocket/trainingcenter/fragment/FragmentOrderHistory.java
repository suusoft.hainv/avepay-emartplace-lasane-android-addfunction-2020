package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.OrderHistoryAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.config.GlobalValue;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.OrderHistory;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class FragmentOrderHistory extends BaseFragment {

    private static final String TAG = "FragmentOrderHistory";

    private View v;
    private ListView mLvOrderHistory;
    private ArrayList<OrderHistory> mArrHistory;
    private OrderHistoryAdapter mAdapter;

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("orderHistory");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("orderHistory");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_order_history, container, false);

//		init(view);
//		initControl();
//		initData();
        initUI();
        getHistory();
        return v;
    }

    private void initUI() {
        mLvOrderHistory = (ListView) v.findViewById(R.id.lvOrderHistory);
        initControl();
    }

    private void initControl() {
        mLvOrderHistory.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DetailOrderHistory history = new DetailOrderHistory();
                Bundle bundle = new Bundle();
                String str_orderID = mArrHistory.get(position).getOrderID();
                bundle.putString("orderID", str_orderID);
                history.setArguments(bundle);

                getChildFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right)
                        .replace(R.id.showOrderHistory, history).addToBackStack(null).commit();
//				getFragmentManager()
//				.beginTransaction()
//				.setCustomAnimations(R.anim.slide_in_left,
//						R.anim.slide_out_left)
//				.replace(R.id.showOrderHistory, history)
//				.addToBackStack(null).commit();
            }
        });
    }

    private void getHistory() {
        ModelManager.getHistory(GlobalValue.pref.getStringValue(GlobalValue.KEY_ORDER_IDS), new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                mArrHistory = ParseUtility.parseHistory(response.getRootStr());
                if (mArrHistory.size() > 0) {
                    mAdapter = new OrderHistoryAdapter(getActivity(), R.layout.item_order_history,
                            mArrHistory);
                    mLvOrderHistory.setAdapter(mAdapter);
                } else {
                    Toast.makeText(getActivity(), "Not found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

}
