package com.aveplus.avepay_cpocket.movie.movie.main.impl;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface onFocusRow {
    public void onFocus(int position);
}
