package com.aveplus.avepay_cpocket.music.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.music.base.model.BaseModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Artist extends BaseModel implements Parcelable {
    public static final String KEY_ARTIST = Artist.class.getName();
    @SerializedName("songs")
    public ArrayList<Song> listSongs;
    public String id;
    @SerializedName("img_thumb")
    public String thumbnail;
    public String name, description, birth_date,location,real_name;
    public int songs_count,count_views;

    public Artist() {

    }


    protected Artist(Parcel in) {
        listSongs = in.createTypedArrayList(Song.CREATOR);
        id = in.readString();
        thumbnail = in.readString();
        name = in.readString();
        description = in.readString();
        birth_date = in.readString();
        location = in.readString();
        real_name = in.readString();
        songs_count = in.readInt();
        count_views = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(listSongs);
        dest.writeString(id);
        dest.writeString(thumbnail);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(birth_date);
        dest.writeString(location);
        dest.writeString(real_name);
        dest.writeInt(songs_count);
        dest.writeInt(count_views);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Artist> CREATOR = new Creator<Artist>() {
        @Override
        public Artist createFromParcel(Parcel in) {
            return new Artist(in);
        }

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    public ArrayList<Song> getListSongs() {
        if (listSongs == null) {
            listSongs = new ArrayList<>();
        }
        return listSongs;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public int getSongs_count() {
        return songs_count;
    }

    public void setSongs_count(int songs_count) {
        this.songs_count = songs_count;
    }

    public int getCount_views() {
        return count_views;
    }

    public void setCount_views(int count_views) {
        this.count_views = count_views;
    }
}
