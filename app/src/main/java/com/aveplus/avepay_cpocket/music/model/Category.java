package com.aveplus.avepay_cpocket.music.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Category implements Parcelable {
    @SerializedName("img_thumb")
    public String image;
    public String id, title, name, description,content, release_date;

    protected Category(Parcel in) {
        image = in.readString();
        id = in.readString();
        title = in.readString();
        name = in.readString();
        description = in.readString();
        content = in.readString();
        release_date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(content);
        dest.writeString(release_date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
