package com.aveplus.avepay_cpocket.movie.movie.main;

import android.app.ActivityManager;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.aveplus.avepay_cpocket.movie.movie.configs.Apis;
import com.aveplus.avepay_cpocket.movie.movie.retrofit.base.BaseRequest;
import com.aveplus.avepay_cpocket.utils.toolbox.BitmapLruCache;


import java.util.HashMap;

public class RequestManager extends BaseRequest {
    private static RequestQueue mRequestQueue;
    private static ImageLoader mImageLoader;
    public static final String VOLLEY_TAG = "volley";
    private static final String PARAM_GCM_ID = "gcm_id";
    private static final String PARAM_STATUS = "status_hot";
    private static final String PARAM_IMEI = "ime";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_FULLNAME = "fullname";
    private static final String PARAM_IMAGE = "image";
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_USER_ID = "user_id";
    private static final String PARAM_MESSAGE = "message";
    private static final String PARAM_ID = "id";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_GENDER = "gender";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_TASK_ID = "task_id";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_LOGIN_TYPE = "login_type";
    private static final String PARAM_BOOK_ID = "book_id";
    private static final String PARAM_CATEGORY_ID = "category_id";
    private static final String PARAM_CHAPTER_ID = "chapter_id";
    private static final String PARAM_KEYWORD = "keyword";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_PHONE = "phone";
    private static final String PARAM_ADDRESS = "address";
    private static final String PARAM_AVATAR = "avatar";
    private static final String PARAM_FILTER = "filter";
    private static final String PARAM_TOKEN = "token";

    private static final String PARAM_COUPON = "coupon";
    private static final String PARAM_IS_CHECK = "is_check";

    private RequestManager() {
        // no instances
    }

    public static void init(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);

        int memClass = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE))
                .getMemoryClass();
        // Use 1/6th of the available memory for this memory cache.
        int cacheSize = 1024 * 1024 * memClass / 6;
        mImageLoader = new ImageLoader(mRequestQueue, new BitmapLruCache(cacheSize));
    }

    public static RequestQueue getRequestQueue() {
        if (mRequestQueue != null) {
            return mRequestQueue;
        } else {

            throw new IllegalStateException("RequestQueue not initialized");
        }
    }

    /**
     * Returns instance of ImageLoader initialized with {@see FakeImageCache} which effectively means
     * that no memory caching is used. This is useful for images that you know that will be show
     * only once.
     *
     * @return
     */
    public static ImageLoader getImageLoader() {
        if (mImageLoader != null) {
            return mImageLoader;
        } else {
            throw new IllegalStateException("ImageLoader not initialized");
        }
    }

    public static void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static void login(Context context, String username, String password, final BaseRequest.CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USERNAME, username);
        params.put(PARAM_PASSWORD, password);
//        params.put(PARAM_NAME, "");
//        params.put(PARAM_TYPE, "2");
        params.put(PARAM_LOGIN_TYPE, "n");
//        params.put(PARAM_AVATAR, "");
//        params.put("ime","0");

        get(Apis.URL_LOGIN, params, true, completeListener);
    }

    public static void loginSocial(Context context, String username, String name, String avatar, final CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USERNAME, username);
        params.put(PARAM_NAME, name);
        params.put(PARAM_IMEI, "0");
        // params.put(PARAM_GCM_ID, DataStoreManager.getTokenFCM());
        params.put(PARAM_TYPE, "2");
        params.put(PARAM_AVATAR, avatar);
        params.put(PARAM_LOGIN_TYPE, "s");
        //params.put(PARAM_STATUS, "1");

        get(Apis.URL_LOGIN, params, true, completeListener);
    }
}
