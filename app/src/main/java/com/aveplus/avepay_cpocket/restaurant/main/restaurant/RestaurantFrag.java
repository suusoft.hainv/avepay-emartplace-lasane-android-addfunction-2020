package com.aveplus.avepay_cpocket.restaurant.main.restaurant;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.main.home.RestaurantAdapter;
import com.aveplus.avepay_cpocket.restaurant.main.home.RestaurantModel;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.MycartActivity;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.photo.AdapterPhoto;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.photo.PhotoActivity;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.FoodActivity;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.ImageVideo;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.RandomFood;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.Rate;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.RestaurantDetail;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.Review;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.review.AdapteReview;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.toporder.AdapterTopOrder;
import com.aveplus.avepay_cpocket.restaurant.main.save.savemodels.RestaurantInfo;
import com.aveplus.avepay_cpocket.restaurant.main.save.savemodels.SaveMdoels;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeFavourite;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponseListRestaurant;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponseRestaurantDetail;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponseReview;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.MyProgressDialog;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.ViewDialog;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.ID_RESTAURANT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_SELLLER_ID;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LAT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LONG;


public class RestaurantFrag extends BaseFragment implements View.OnClickListener, AdapterTopOrder.HomeCallBack {
    private final static String TAG = RestaurantFrag.class.getSimpleName();
    private MapView mapView;
    private GoogleMap map;
    private RecyclerView rcPhoto, rcTopOrder, rcPlaceNearby, rcReview, rcCart, rcNearby;
    private TextView tvreview, tvPhonContact, tvAbout;
    public static RelativeLayout rlBottom;
    private ImageView imgHeart, imgBanner, imgCart, imgClose, imgInstagram, imgFacebook, imgVideo, imgFanpage;
    private double LatLng, LongLng;
    private double user_lat;
    private double user_long;
    public static ArrayList<RandomFood> randomFoods;
    public static int restaurant_id;
    public static int cart_count = 0;
    private List<ImageVideo> imageVideoList;
    private List<RestaurantModel> restaurantList;
    private List<Review> reviewList;
    private ArrayList<RestaurantInfo> infos;
    AdapterPhoto adapterPhoto;
    public static ArrayList<RestaurantDetail> arrDetai;
    private AdapteReview adapteReview;
    AdapterTopOrder adapterTopOrder;
    RestaurantAdapter restaurantAdapter;
    private int id_restaurant;
    private String restaurantname;
    private ProgressDialog progressDialog;
    private ImageView imgFavourite;
    private Button btnOrder, btnCallDialog,
            btnRate;
    private TextView btnContact;
    private RelativeLayout webvideo, rlComment, rlPhoto, rlShare, rlWhatsapp;
    private ToggleButton toggleButton;
    private int seller_id;
    private SaveMdoels save;
    private BottomSheetDialog bottomSheetDialog;
    private String contact, urlFacebook, urlInsta, urlFanpage;
    private FusedLocationProviderClient client;

    private String videoId;
    private TextView txtRating5;
    private TextView txtRating4;
    private TextView txtRating3;
    private TextView txtRating2;
    private TextView txtRating1;
    private RelativeLayout rlPhotoVideo;
    private RestaurantDetail restaurantDetail;
    private TextView categoryRestaurant, timeOpen, timeClose, tvAddressDetail, tvDistance, tvNameRestaurant, tvMinPrice, tvMaxPrice;

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_restaurantdetail;
    }

    @Override
    protected void init() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            id_restaurant = bundle.getInt(ID_RESTAURANT, 0);
            seller_id = bundle.getInt(PARAM_SELLLER_ID, 0);
        } else {
            Toast.makeText(self, "Null", Toast.LENGTH_SHORT).show();
        }
        reQuest();
        setHasOptionsMenu(true);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        client = LocationServices.getFusedLocationProviderClient(self);
        if (ActivityCompat.checkSelfPermission(
                self, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
        client.getLastLocation().addOnSuccessListener((Activity) self, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    user_long = location.getLongitude();
                    user_lat = location.getLatitude();
                    getSearch(user_long, user_lat);
//                    getDataNearby(user_lat, user_long);
                }
            }
        });
        //getDataRestaurant ();
//        getDataNearby ();
        Log.e("TAG", "onViewCreated: " + seller_id);
        mapView = view.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);


    }

    @Override
    protected void initView(View view) {
        txtRating5 = view.findViewById(R.id.txt_rating5);
        txtRating4 = view.findViewById(R.id.txt_rating4);
        txtRating3 = view.findViewById(R.id.txt_rating3);
        txtRating2 = view.findViewById(R.id.txt_rating2);
        txtRating1 = view.findViewById(R.id.txt_rating1);


        rlPhotoVideo = view.findViewById(R.id.rl_photo_video);


        imgHeart = view.findViewById(R.id.img_heart);
        imgBanner = view.findViewById(R.id.img_banner);
        btnOrder = view.findViewById(R.id.btn_order);
        btnContact = view.findViewById(R.id.btn_contact);

        btnRate = view.findViewById(R.id.btn_rate);
        btnRate.setOnClickListener(this);

//        imgFavourite     = view.findViewById(R.id.img_favourite);
        timeOpen = view.findViewById(R.id.time_open);
        timeClose = view.findViewById(R.id.time_close);
        tvNameRestaurant = view.findViewById(R.id.name_resraurant);
        tvAddressDetail = view.findViewById(R.id.tv_address_detail);
        tvDistance = view.findViewById(R.id.tv_distance);
        categoryRestaurant = view.findViewById(R.id.tv_category_restaurant);
        rcReview = view.findViewById(R.id.rc_review);
        tvreview = view.findViewById(R.id.tv_review);
        imgFacebook = view.findViewById(R.id.img_facebook);
        imgInstagram = view.findViewById(R.id.img_instagram);
        tvAbout = view.findViewById(R.id.tv_about_us);
        imgVideo = view.findViewById(R.id.img_video);
        webvideo = view.findViewById(R.id.webvideo_layout);
        imgFanpage = view.findViewById(R.id.img_fanpage);

        rcPhoto = view.findViewById(R.id.rc_photo);
        rcTopOrder = view.findViewById(R.id.rc_top_order);
        rcPlaceNearby = view.findViewById(R.id.rc_place_nearby);
        rlWhatsapp = view.findViewById(R.id.rl_whatsapp);
        TextView tv = view.findViewById(R.id.tv_place);
        SpannableString content = new SpannableString("Places nearby");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tv.setText(content);

        btnContact.setOnClickListener(this);
        tvreview.setOnClickListener(this);
        imgFacebook.setOnClickListener(this);
        imgInstagram.setOnClickListener(this);
        rlWhatsapp.setOnClickListener(this);
        webvideo.setOnClickListener(this);
        imgFanpage.setOnClickListener(this);
        //tvOrder.setOnClickListener(this);
        imgHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewDialog dialog = new ViewDialog(getActivity());
                dialog.showDialog();
                APIService apiService = ApiUtils.getAPIService();
                apiService.Favourite(DataStoreManager.getToken(), id_restaurant, "restaurant").enqueue(new Callback<ResponeFavourite>() {
                    @Override
                    public void onResponse(Call<ResponeFavourite> call, Response<ResponeFavourite> response) {
                        if (response.body().getData().isFavourite()) {
                            imgHeart.setImageResource(R.drawable.ic_bookmark_1);
                            dialog.hideDialog();
                            AppUtil.showToast(self, R.string.add_save);
                        } else {
                            imgHeart.setImageResource(R.drawable.ic_bookmark);
                            AppUtil.showToast(self, R.string.un_save);
                            dialog.hideDialog();
                        }


                    }

                    @Override
                    public void onFailure(Call<ResponeFavourite> call, Throwable t) {
                        Toast.makeText(self, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                        Log.e("OnFailure :", t.getMessage());
                        dialog.hideDialog();
                    }
                });
            }
        });
        // list
        imageVideoList = new ArrayList<>();
        randomFoods = new ArrayList<>();
        topOrder();
        setAdapterReview();
        photo();
        //review ();
        getNearby();


    }

    private void setAdapterReview() {
        reviewList = new ArrayList<>();
        adapteReview = new AdapteReview(reviewList, self);
        rcReview.setLayoutManager(new LinearLayoutManager(self));
        rcReview.setAdapter(adapteReview);
    }


    @Override
    public void getData() {
    }

    private void getLocation1() {
        client = LocationServices.getFusedLocationProviderClient(self);
        if (ActivityCompat.checkSelfPermission(
                self, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
        client.getLastLocation().addOnSuccessListener((Activity) self, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    user_long = location.getLongitude();
                    user_lat = location.getLatitude();
                    Log.e(TAG, "initViewlong: " + user_long);
                    Log.e(TAG, "initViewlat: " + user_lat);
                }
            }
        });
    }

    private void reQuest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    private void getSearch(double longitude, double latitude) {
        if (seller_id != 0) {
            ViewDialog dialog = new ViewDialog(getActivity());
            dialog.showDialog();
            APIService apiService = ApiUtils.getAPIService();
            apiService.getRestaurantDetail(DataStoreManager.getToken(), seller_id, latitude, longitude)
                    .enqueue(new Callback<ResponseRestaurantDetail>() {
                        @Override
                        public void onResponse(Call<ResponseRestaurantDetail> call, Response<ResponseRestaurantDetail> response) {
                            LatLng = Double.parseDouble(response.body().getData().getLat());
                            LongLng = Double.parseDouble(response.body().getData().getLong());
                            Log.e(TAG, "onResponse: " + LatLng);
                            Log.e(TAG, "onResponse: " + LongLng);
                            if (response.body().getData().getLat() != null && response.body().getData().getLong() != null) {
                                mapView.getMapAsync(new OnMapReadyCallback() {
                                    @Override
                                    public void onMapReady(GoogleMap googleMap) {
                                        map = googleMap;
                                        map.setMinZoomPreference(11);
                                        com.google.android.gms.maps.model.LatLng latLng = new LatLng(LatLng, LongLng);
                                        MarkerOptions markerOptions = new MarkerOptions();
                                        markerOptions.position(latLng);
                                        map.addMarker(markerOptions);
                                        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                    }
                                });
                            }

                            setTextRestaurant(response.body().getData());
                            restaurant_id = response.body().getData().getId();
                            contact = response.body().getData().getPhone();
                            Log.e("CONTACT", "onResponse: " + contact);


//                add Toporder
                            randomFoods.addAll(response.body().getData().getRandomFood());
                            Collections.sort(randomFoods, new Comparator<RandomFood>() {
                                @Override
                                public int compare(RandomFood randomFood, RandomFood t1) {
                                    return randomFood.getName().compareTo(t1.getName());

                                }
                            });
                            adapterTopOrder.notifyDataSetChanged();
//                add photo
                            Log.e(TAG, "getDataRestaurant: " + new Gson().toJson(response.body().getData().getReview()));
                            reviewList.addAll(response.body().getData().getReview());
                            adapteReview.notifyDataSetChanged();
                            //add review
                            Log.e("API", "onResponse: " + response.body().getData().getName());

                            //add review
                            reviewList.addAll(response.body().getData().getReview());
                            adapteReview.notifyDataSetChanged();

                            dialog.hideDialog();
                            Log.e("Image", "onResponse: " + imageVideoList.size());
                        }

                        @Override
                        public void onFailure(Call<ResponseRestaurantDetail> call, Throwable t) {
                            dialog.hideDialog();
                        }
                    });
        } else {
            getDataRestaurant(longitude, latitude);
        }
    }

    private void saveState(boolean isFavourite) {
        SharedPreferences aSharedPreferences = getActivity().getSharedPreferences("Favourite", Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences
                .edit();
        aSharedPreferencesEdit.putBoolean("State", isFavourite);
        aSharedPreferencesEdit.commit();
    }

    private boolean readState() {
        SharedPreferences aSharedPreferences = getActivity().getSharedPreferences(
                "Favourite", Context.MODE_PRIVATE);
        return aSharedPreferences.getBoolean("State", true);
    }

    private void getDataRestaurant(double longitude, double latitude) {
        ViewDialog dialog = new ViewDialog(getActivity());
        dialog.showDialog();
        APIService apiService = ApiUtils.getAPIService();
        apiService.getRestaurantDetail(DataStoreManager.getToken(), id_restaurant, latitude, longitude)
                .enqueue(new Callback<ResponseRestaurantDetail>() {
                    @Override
                    public void onResponse(Call<ResponseRestaurantDetail> call, Response<ResponseRestaurantDetail> response) {
                        dialog.hideDialog();
                        if (response.body() != null) {
                            if (response.body().isSuccess((RestaurantAct) self)) {
                                if (response.body().getData() != null) {
                                    restaurantDetail = response.body().getData();
                                    LatLng = Double.parseDouble(restaurantDetail.getLat());
                                    LongLng = Double.parseDouble(restaurantDetail.getLong());
                                    if (restaurantDetail.getLat() != null && restaurantDetail.getLong() != null) {
                                        mapView.getMapAsync(new OnMapReadyCallback() {
                                            @Override
                                            public void onMapReady(GoogleMap googleMap) {
                                                map = googleMap;
                                                map.setMinZoomPreference(11);
                                                com.google.android.gms.maps.model.LatLng latLng = new LatLng(LatLng, LongLng);
                                                MarkerOptions markerOptions = new MarkerOptions();
                                                markerOptions.position(latLng);
                                                map.addMarker(markerOptions);
                                                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                            }
                                        });
                                    }
                                    setTextRestaurant(restaurantDetail);
                                    contact = response.body().getData().getPhone();
                                    urlFacebook = response.body().getData().getFacebookLink();
                                    urlInsta = response.body().getData().getIgLink();


//                add Toporder

                                    restaurant_id = restaurantDetail.getId();
                                    randomFoods.addAll(restaurantDetail.getRandomFood());
                                    Collections.sort(randomFoods, new Comparator<RandomFood>() {
                                        @Override
                                        public int compare(RandomFood randomFood, RandomFood t1) {
                                            return randomFood.getName().compareTo(t1.getName());

                                        }
                                    });
                                    adapterTopOrder.notifyDataSetChanged();
//                add photo
                                    imageVideoList.addAll(response.body().getData().getImageVideo());
                                    adapterPhoto.notifyDataSetChanged();
                                    //add review
                                    Log.e(TAG, "getDataRestaurant: " + new Gson().toJson(response.body().getData().getReview()));
                                    reviewList.addAll(response.body().getData().getReview());
                                    adapteReview.notifyDataSetChanged();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRestaurantDetail> call, Throwable t) {
                        dialog.hideDialog();
                    }
                });
    }

    private void setTextRestaurant(RestaurantDetail restaurant) {

        getDataNearby(Double.parseDouble(restaurant.getLat()), Double.parseDouble(restaurant.getLong()));
        Glide.with(self).load(restaurant.getImage()).into(imgBanner);
        restaurantname = restaurant.getName();
        String distance = restaurant.getDistance() + " km";
        String min_price = restaurant.getMinPrice() + " - ";
        tvNameRestaurant.setText(restaurant.getName());
        tvAddressDetail.setText(restaurant.getAddress());
        tvDistance.setText(distance);
        if (restaurant.getAbout() != null && !restaurant.getAbout().isEmpty()) {
            tvAbout.setText(android.text.Html.fromHtml(restaurant.getAbout()));
        } else {
            tvAbout.setText("");
        }


        if (restaurant.getReview_count() != null) {
            setReviewCount(restaurant.getReview_count());
        }


        Log.e(TAG, "setTextRestaurant: " + min_price);
        Log.e(TAG, "setTextRestaurant: " + restaurant.getMaxPrice());
        String open = restaurant.getOpenHour();
        timeOpen.setText(open);
        timeClose.setText(restaurant.getCloseHour());
        categoryRestaurant.setText(restaurant.getDescription());
        if (restaurant.getYoutubeLink() != null && !restaurant.getYoutubeLink().isEmpty() && !restaurant.getYoutubeLink().equals("null")) {
            videoId = restaurant.getYoutubeLink();
            String img_url = "http://img.youtube.com/vi/" + restaurant.getYoutubeLink() + "/0.jpg"; // this is link which will give u thumnail image of that video}
            Log.e(TAG, "setTextRestaurant: " + img_url);
            Glide.with(self).load(img_url).into(imgVideo);
        } else {
            rlPhotoVideo.setVisibility(View.GONE);
        }
        urlFanpage = restaurant.getFanpageLink();


        // picasso jar file download image for u and set image in imagview


    }

    private void setReviewCount(Rate review_count) {
        if (review_count.getOne() != null) {
            txtRating1.setText(review_count.getOne().toString());
        }
        if (review_count.getTwo() != null) {
            txtRating2.setText(review_count.getTwo().toString());
        }
        if (review_count.getThree() != null) {
            txtRating3.setText(review_count.getThree().toString());
        }
        if (review_count.getFour() != null) {
            txtRating4.setText(review_count.getFour().toString());
        }
        if (review_count.getFive() != null) {
            txtRating5.setText(review_count.getFive().toString());
        }
    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = null;
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }

    private void photo() {
        int ScreenWidth = AppUtil.getScreenWidth(getActivity());
        adapterPhoto = new AdapterPhoto(self, imageVideoList, ScreenWidth, new AdapterPhoto.OnItemClickListener() {
            @Override
            public void onItemClick(List<ImageVideo> imageVideo, int position) {

                PhotoActivity photoActivity = PhotoActivity.newInstance(imageVideo, position, restaurantname);
                AppUtil.startActivity(getActivity(), photoActivity.getClass());

            }
        });
        //rcPhoto.setLayoutManager (new GridLayoutManager (self, 3, GridLayout.VERTICAL, false));
        GridLayoutManager layoutManager = new GridLayoutManager(self, 3);
        rcPhoto.setLayoutManager(layoutManager);
        rcPhoto.setAdapter(adapterPhoto);
        adapterPhoto.notifyDataSetChanged();
    }

//    private void review() {
//
//        adapteReview = new AdapteReview (reviewList, self);
//        rcReview.setLayoutManager (new LinearLayoutManager (self, LinearLayout.VERTICAL, false));
//        rcReview.setAdapter (adapteReview);
//    }

    private void topOrder() {
        if (randomFoods.size() > 2) {
            rcTopOrder.getLayoutParams().height = AppUtil.getScreenWidth(getActivity());
        }

        adapterTopOrder = new AdapterTopOrder(self, randomFoods, this);
        adapterTopOrder.setListener(new AdapterTopOrder.ItemimgAddListener() {
            @Override
            public void onImageclick(int position) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("product", randomFoods.get(position));
                AppUtil.startActivity(self, ProductDetailAct.class, bundle);
            }

            @Override
            public void onDetailclick(int position) {
                Bundle bundle = new Bundle();
                bundle.putString("img", randomFoods.get(position).getAttachment());
                AppUtil.startActivity(self, FoodActivity.class, bundle);
            }
        });

//        rcTopOrder.setLayoutManager (new LinearLayoutManager (self, LinearLayout.VERTICAL, false));
        LinearLayoutManager layoutManager = new LinearLayoutManager(self);
        rcTopOrder.setLayoutManager(layoutManager);
        rcTopOrder.setAdapter(adapterTopOrder);
        adapterTopOrder.notifyDataSetChanged();

    }

    private void getNearby() {
        restaurantList = new ArrayList<>();
        restaurantAdapter = new RestaurantAdapter(self, restaurantList, new RestaurantAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RestaurantModel restaurantModel) {
                Toast.makeText(self, "" + restaurantModel.getName(), Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putInt(ID_RESTAURANT, restaurantModel.getId());
                bundle.putDouble(USER_LAT, Double.parseDouble(restaurantModel.getLat()));
                bundle.putDouble(USER_LONG, Double.parseDouble(restaurantModel.getLong()));
                RestaurantFrag restaurantFrag = new RestaurantFrag();
                restaurantFrag.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.frame_restaurant, restaurantFrag).commit();
            }
        });
        //rcPlaceNearby.setLayoutManager (new GridLayoutManager (getActivity (), 2, GridLayout.VERTICAL, false));
        GridLayoutManager layoutManager = new GridLayoutManager(self, 2);
        rcPlaceNearby.setLayoutManager(layoutManager);
        rcPlaceNearby.setAdapter(restaurantAdapter);
    }

    private void getDataNearby(double latitude, double longtitude) {
        APIService apiService = ApiUtils.getAPIService();
        apiService.getNearbyRestaurant(latitude, longtitude, DataStoreManager.getToken(), 1, 20).enqueue(new Callback<ResponseListRestaurant>() {
            @Override
            public void onResponse(Call<ResponseListRestaurant> call, Response<ResponseListRestaurant> response) {
                restaurantList.addAll(response.body().getData());
                restaurantAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponseListRestaurant> call, Throwable t) {
                Log.e("getDataNearby", "onFailure: " + t.getMessage());
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_order:
                if (cart_count < 1) {
                    AppUtil.showToast(getActivity(), R.string.not_item_in_cart);
                } else {
                    Intent intent = new Intent(getActivity(), MycartActivity.class);
                    intent.putExtra(ID_RESTAURANT, id_restaurant);
                    startActivity(intent);
                }
                break;

            case R.id.btn_rate:
                if (restaurantDetail != null) {
                    showRatingDialog();
                }
                break;
            case R.id.btn_contact:
                if (contact != null && !contact.isEmpty()) {
                    View view = LayoutInflater.from(getActivity()).inflate(R.layout.contact_dialog, null);
                    TextView tvphone = view.findViewById(R.id.tv_contact);
                    imgClose = view.findViewById(R.id.img_close);
                    btnCallDialog = view.findViewById(R.id.btn_call_contact);
                    bottomSheetDialog = new BottomSheetDialog(getActivity());
                    bottomSheetDialog.setContentView(view);
                    bottomSheetDialog.show();
                    tvphone.setText(contact);

                    imgClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            bottomSheetDialog.dismiss();
                        }
                    });

                    btnCallDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent phone = new Intent(Intent.ACTION_DIAL);
                            phone.setData(Uri.parse("tel:" + tvphone.getText().toString()));
                            startActivity(phone);
                        }
                    });
                } else {
                    Toast.makeText(self, "not found", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.img_facebook:
                if (urlFacebook == null || urlFacebook.isEmpty()) {
                    Toast.makeText(self, "not found", Toast.LENGTH_SHORT).show();
                } else {
                    gotoFacebook(urlFacebook);
                }
                break;
            case R.id.img_instagram:
                if (urlInsta == null || urlInsta.isEmpty()) {
                    Toast.makeText(self, "not found", Toast.LENGTH_SHORT).show();
                } else {
                    gotoInsta(urlInsta);
                }
                break;
            case R.id.rl_whatsapp:
                gotoWhasapp();
                break;
            case R.id.webvideo_layout:
                watchYoutubeVideo(self, videoId);
                break;
            case R.id.img_fanpage:
                if (urlFanpage == null || urlFanpage.isEmpty()) {
                    Toast.makeText(self, "not found", Toast.LENGTH_SHORT).show();
                } else {
                    gotoFacebook(urlFanpage);
                }
                break;
            default:
                break;


        }
    }

    private void showRatingDialog() {
        final Dialog dialog = new Dialog(self);
        dialog.setContentView(R.layout.dialog_rating);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView lblSubmit = dialog.findViewById(R.id.lbl_submit);
        final RatingBar ratingBar = dialog.findViewById(R.id.rating_bar);
        final EditText edtContent = dialog.findViewById(R.id.txt_comment);

        lblSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float rating = ratingBar.getRating();
                String content = edtContent.getText().toString();
                if (rating == 0) {
                    AppUtil.showToast(getActivity(), R.string.msg_rating_require);
                    return;
                } else if (content.isEmpty()) {
                    AppUtil.showToast(getActivity(), R.string.msg_content_require);
                    return;
                } else {
                    sendReponse(rating, content);
                }
                dialog.dismiss();
            }
        });
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void sendReponse(float rating, String content) {
        MyProgressDialog myProgressDialog = new MyProgressDialog(self);
        myProgressDialog.show();
        if (DataStoreManager.getUser() != null) {
            ApiUtils.getAPIService().reviewRestaurant(DataStoreManager.getToken(), rating * 2, content, "shop", restaurant_id).enqueue(new Callback<ResponseReview>() {
                @Override
                public void onResponse(Call<ResponseReview> call, Response<ResponseReview> response) {
                    myProgressDialog.cancel();
                    if (response.body() != null) {
                        if (response.body().isSuccess(getActivity())) {
                            if (response.body().getReview_count() != null) {
                                setReviewCount(response.body().getReview_count());
                            }
                            if (response.body().getReviews() != null) {
                                Log.e(TAG, "onResponse: " + new Gson().toJson(response.body().getReviews()));
                                if (reviewList != null && !reviewList.isEmpty()) {
                                    reviewList.clear();
                                }
                                reviewList.addAll(response.body().getReviews());
                                adapteReview.notifyDataSetChanged();

                            }
                        }
                    } else {
                        Toast.makeText(self, R.string.msg_have_some_error, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseReview> call, Throwable t) {

                    myProgressDialog.cancel();
                    Toast.makeText(self, R.string.msg_have_some_error, Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    private void gotoInsta(String url) {
        Uri uri = Uri.parse(url);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url)));
        }
    }

    private void gotoFacebook(String url) {
        Uri uri = Uri.parse(url);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
        likeIng.setPackage("com.facebook.katana");
        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url)));
        }
    }

    public static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + id));
        Log.e(TAG, "watchYoutubeVideo: " + id);
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    private void gotoWhasapp() {
        if (isAppExist()) {
            Uri mUri = Uri.parse("https://api.whatsapp.com/send?phone=" + contact + "&text='Hello'");
            Intent intent = new Intent("android.intent.action.VIEW", mUri);
            intent.setPackage("com.whatsapp");
            startActivity(intent);

        } else if (isAppExist2()) {
            Uri mUri = Uri.parse("https://api.whatsapp.com/send?phone=" + contact + "&text='Hello'");
            Intent intent = new Intent("android.intent.action.VIEW", mUri);
            intent.setPackage("com.whatsapp.w4b");
            startActivity(intent);
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp")));
        }
    }


    private boolean isAppExist() {
        PackageManager pm = self.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    private boolean isAppExist2() {
        PackageManager pm = self.getPackageManager();
        try {
            PackageInfo info2 = pm.getPackageInfo("com.whatsapp.w4b", PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }


    public void onResume() {
        super.onResume();
        mapView.onResume();
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity != null) {
            activity.getSupportActionBar().show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_cart, menu);
//        MenuItem menuItem = menu.findItem(R.id.cart_action);
//        menuItem.setIcon(Convert.convertLayoutToImage(getContext(), cart_count, R.drawable.ic_shopping_cart_24dp));
//        //imgCart.setImageDrawable(Convert.convertLayoutToImage(getContext(), cart_count, R.drawable.ic_shopping_cart_gray_24dp));
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.cart_action:
//                if (cart_count < 1) {
//                    AppUtil.showToast(getActivity(), R.string.not_item_in_cart);
//                } else {
//                    Intent intent = new Intent(getActivity(), MycartActivity.class);
//                    intent.putExtra(ID_RESTAURANT, id_restaurant);
//                    startActivity(intent);
//                }
//                break;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//
//        return true;
//    }


    @Override
    public void updateCartCount(Context context) {
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().invalidateOptionsMenu();
    }
}
