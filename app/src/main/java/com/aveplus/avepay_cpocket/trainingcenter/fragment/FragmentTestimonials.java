package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.CustomerAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.Customers;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class FragmentTestimonials extends BaseFragment {

    private ArrayList<Customers> listCustomer = new ArrayList<Customers>();
    private ListView lstCustom;
    private CustomerAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("customer");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onPause();
//        if (onTitleChanged != null)
//            onTitleChanged.onTitleChanged("home");
//        else
//            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_customer, container, false);
        initUI(view);
        getData();
        return view;
    }

    private void getData() {
        ModelManager.getListCustomer(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                listCustomer = ParseUtility.parseListCustomer(response.getRootStr());
                mAdapter = new CustomerAdapter(getActivity(),
                        listCustomer);
                lstCustom.setAdapter(mAdapter);
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });

    }

    private void initUI(View v) {
        lstCustom = (ListView) v.findViewById(R.id.lstCustomer);
    }
}
