package com.aveplus.avepay_cpocket.taxi.retrofit.base;


import com.aveplus.avepay_cpocket.taxi.retrofit.APIService;

public class BaseRequestApi {

    protected static final String TAG = BaseRequestApi.class.getSimpleName();

    private static BaseRequestApi mInstance;

    protected static APIService mAPIService;

    public static BaseRequestApi getInstance() {
        if (mInstance == null) {
            mInstance = new BaseRequestApi();
        }

        return mInstance;
    }

    //********************************************************************************
    public interface ListenerLoading {
        void onLoadingIsProcessing();

        void onLoadingIsCompleted();
    }

    /**
     * interface for listening progress.
     * purpose is show and hide progressbar
     */
    private ListenerLoading listenerLoading;

    public ListenerLoading getListenerLoading() {
        return listenerLoading;
    }

    public void setListenerLoading(ListenerLoading listenerLoading) {
        this.listenerLoading = listenerLoading;
    }

    /**
     * show hide progress bar
     */
    protected void showProgress(boolean isOpen) {
        if (isOpen && listenerLoading != null) {
            listenerLoading.onLoadingIsProcessing();
        }
    }

    protected void hideProgress(boolean isOpen) {
        if (isOpen && listenerLoading != null) {
            listenerLoading.onLoadingIsCompleted();
        }
    }

    //*******************************************************************************
}
