package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.objects.PaymentMethodObj;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.kyleduo.switchbutton.SwitchButton;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class EarnBonusActivity extends BaseActivity {


    private SwitchButton switchOnline;
    private TextView btnSend;
    private boolean isGetEarnBonus = false;
    private TextView tvNotif;

    private TextView btnYes, btnNo;


    private String mSelectedPayment = PaymentMethodObj.CASH;
    private RecyclerView mRcl;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_earn_bonus;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {

        EventBus.getDefault().post(new Global.FinishAllScreenTrip());

        setToolbarTitle(R.string.earn_bonus);

        tvNotif = findViewById(R.id.tv_notif);
        btnSend = findViewById(R.id.btn_send);
        switchOnline = findViewById(R.id.sb_online);

        btnYes = findViewById(R.id.btn_yes);
        btnNo = findViewById(R.id.btn_no);

        switchOnline.setOnCheckedChangeListener(onCheckedChangeListener);

        mRcl = (RecyclerView) findViewById(R.id.rcl_payment_methods);
        mRcl.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));
        mRcl.setHasFixedSize(true);
        setYes(false);


        initPaymentMethod();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendToserver();
            }
        });

        tvNotif.setText(String.format(getString(R.string.hello_1_s_thank_you_for_confirming), DataStoreManager.getUser().getName()) + "% " + getString(R.string.bonus_on_the_amount_paid_for_each_trip));

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setYes(true);

            }
        });


        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setYes(false);
            }
        });
    }

    private void setYes(boolean isYes) {
        if (isYes) {
            btnYes.setTextColor(getResources().getColor(R.color.white));
            btnNo.setTextColor(getResources().getColor(R.color.textColorSecondary));
            btnYes.setBackgroundResource(R.drawable.bg_border_green_radius_left);
            btnNo.setBackgroundColor(getResources().getColor(R.color.transparent));


            AppUtil.showToast(self, R.string.thank_you_you_have_just_received_10_on_your_loyalty_account);
        } else {
            btnYes.setTextColor(getResources().getColor(R.color.textColorSecondary));
            btnNo.setTextColor(getResources().getColor(R.color.white));

            btnNo.setBackgroundResource(R.drawable.bg_border_green_radius_right);
            btnYes.setBackgroundColor(getResources().getColor(R.color.transparent));


            AppUtil.showToast(self, R.string.if_you_click_no_you_will_not_receive_a_bonus);
            //AppUtil.showToast(self, R.string.you_will_not_be_eligible_for_any_bonuses);
        }

    }

    private void sendToserver() {

        finish();

    }

    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, final boolean isChecked) {

            isGetEarnBonus = isChecked;

            if (!isChecked) {
                AppUtil.showToast(self, R.string.you_will_not_be_eligible_for_any_bonuses);
            }

        }
    };

    @Override
    protected void onViewCreated() {

    }

    private void initPaymentMethod() {
        PaymentMethodAdapter adapter = new PaymentMethodAdapter(new IPayment() {
            @Override
            public void onPaymentMethodSelected(PaymentMethodObj obj) {
                mSelectedPayment = obj.getId();

            }
        });

        mRcl.setAdapter(adapter);
    }

    class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder> {

        private ArrayList<PaymentMethodObj> paymentMethodObjs;
        private IPayment iPayment;

        public PaymentMethodAdapter(IPayment iPayment) {
            this.iPayment = iPayment;

            if (paymentMethodObjs == null) {
                paymentMethodObjs = new ArrayList<>();
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.CASH, getString(R.string.cash), true));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.CREDITS, getString(R.string.credits), false));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.BONUS, getString(R.string.bonus_pay), false));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.MNT_MONEY, getString(R.string.mnt_money), false));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.ORANGE_MONEY, getString(R.string.orange_money), false));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.MOOV_MONEY, getString(R.string.moov_money), false));
            }
        }

        @Override
        public PaymentMethodAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_method, parent, false);

            return new PaymentMethodAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final PaymentMethodAdapter.ViewHolder holder, final int position) {
            if (getItemCount() > 0) {
                final PaymentMethodObj obj = paymentMethodObjs.get(position);
                if (obj != null) {
                    holder.lblMethod.setText(obj.getName());
                    if (obj.isSelected()) {
                        holder.lblMethod.setTextColor(ContextCompat.getColor(self, R.color.white));
                        holder.lblMethod.setBackgroundResource(R.drawable.bg_accent_shadow);
                    } else {
                        holder.lblMethod.setTextColor(ContextCompat.getColor(self, R.color.textColorPrimary));
                        holder.lblMethod.setBackgroundResource(R.drawable.bg_pressed_grey);
                    }

                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (/*!obj.getId().equals(PaymentMethodObj.MNT_MONEY)
                                    && */!obj.getId().equals(PaymentMethodObj.MOOV_MONEY)
                                    && !obj.getId().equals(PaymentMethodObj.ORANGE_MONEY)) {

                                if (!mSelectedPayment.equals(obj.getId())) {
                                    for (int i = 0; i < getItemCount(); i++) {
                                        PaymentMethodObj paymentMethodObj = paymentMethodObjs.get(i);
                                        if (paymentMethodObj.isSelected()) {
                                            paymentMethodObj.setSelected(false);
                                            break;
                                        }
                                    }

                                    obj.setSelected(true);
                                    notifyDataSetChanged();
                                    mSelectedPayment = obj.getId();
                                    iPayment.onPaymentMethodSelected(obj);
                                }
                            } else {
                                AppUtil.showToast(self, R.string.currently_unavailable);
                            }


                        }
                    });
                }
            }
        }

        @Override
        public int getItemCount() {
            try {
                return paymentMethodObjs.size();
            } catch (NullPointerException ex) {
                return 0;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView lblMethod;

            public ViewHolder(View view) {
                super(view);

                lblMethod = view.findViewById(R.id.lbl_payment_method);
            }
        }
    }

    interface IPayment {
        void onPaymentMethodSelected(PaymentMethodObj obj);
    }


}
