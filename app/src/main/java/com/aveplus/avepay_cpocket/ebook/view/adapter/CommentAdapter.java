package com.aveplus.avepay_cpocket.ebook.view.adapter;

import android.content.Context;
import android.view.ViewGroup;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseAdapterBinding;
import com.aveplus.avepay_cpocket.ebook.model.Comment;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemCommentVM;

import java.util.List;


public class CommentAdapter extends BaseAdapterBinding {

    private List<Comment> listData;

    public CommentAdapter(Context context, List<?> datas) {
        super(context);
        this.listData = (List<Comment>) datas;

    }


    public void create(Comment comment) {
        listData.add(0, comment);
        notifyItemInserted(0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getViewBinding(parent, R.layout.item_comment));
    }

    @Override
    public void onBindViewHolder(BaseAdapterBinding.ViewHolder holder, int position) {
         Comment item = listData.get(position);
         if (item != null ) {
             holder.bind(new ItemCommentVM(context, item, position ));
         }
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    @Override
    public void addDatas(List<?> data) {
        int positionStart = listData.size();
        notifyItemRangeInserted(positionStart, data.size());
    }

    @Override
    public void setDatas(List<?> data) {
        notifyDataSetChanged();
    }



}
