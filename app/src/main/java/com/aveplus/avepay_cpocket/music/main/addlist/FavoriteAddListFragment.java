package com.aveplus.avepay_cpocket.music.main.addlist;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentNavBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.main.home.ItemSongsVM;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class FavoriteAddListFragment extends BaseListFragmentNavBinding {
    private static final String TAG = FavoriteAddListFragment.class.getSimpleName();
    private FavoriteAddListVM viewModel;
    private AddListObj addListObj;
    CollapsingToolbarLayout collapsingToolbarLayout;
    private FloatingActionButton buttonPlay;

    private static final String KEY_OBJECT = "OBJECT";


    public static FavoriteAddListFragment newInstance(AddListObj addListObj) {
        //Log.e(TAG, "newInstance: "+new Gson().toJson(addListObj));
        Bundle args = new Bundle();
        args.putParcelable(KEY_OBJECT, addListObj);
        FavoriteAddListFragment fragment = new FavoriteAddListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        showHideActionMenu(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(new SingleAdapter(self, R.layout.item_song_album, viewModel.getListData(), ItemSongsVM.class, viewModel.getActionListener()));

    }

    @Override
    protected void initialize() {
        addListObj = getArguments().getParcelable(KEY_OBJECT);
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new FavoriteAddListVM(self, addListObj);
        return viewModel;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_favoriteaddlist;
    }

    @Override
    protected void initToolbar(View view) {
        super.initToolbar(view);
        AppUtil.hideSoftKeyboard((Activity) self);
        collapsingToolbarLayout = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(addListObj.getAddListName());
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(self, android.R.color.transparent));
        buttonPlay = (FloatingActionButton) view.findViewById(R.id.buttonPlay);
        AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    buttonPlay.hide();
                } else {
                    buttonPlay.show();
                }
            }
        });
    }
}
