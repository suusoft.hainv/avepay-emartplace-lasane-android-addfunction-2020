package com.aveplus.avepay_cpocket.restaurant.main.restaurant.order;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.aveplus.avepay_cpocket.restaurant.main.restaurant.RestaurantFrag.randomFoods;

public class ConfirmAdapter extends RecyclerView.Adapter<ConfirmAdapter.ViewHolder> {
    private ArrayList<CartModel> cartModels;
    private Context context;

    public ConfirmAdapter(ArrayList<CartModel> cartModels, Context context) {
        this.cartModels = cartModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart_confirm,
                viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.tvPrice.setText("$" + cartModels.get(i).getPrice());
        viewHolder.tvName.setText(cartModels.get(i).getName());
        viewHolder.tvRestaurant.setText(randomFoods.get(i).getDescription());
        viewHolder.tvQtt.setText(String.valueOf(cartModels.get(i).getQuantity()));
        Glide.with(context).load(cartModels.get(i).getImage()).into(viewHolder.imgFood);

        //for remove single item in cart and update the total value and list


    }

    @Override
    public int getItemCount() {
        return cartModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvRestaurant, tvQuantity, tvPrice;
        private ImageView imgFood, imgDelete;
        private TextView tvQtt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_namefood_cart);
            tvRestaurant = itemView.findViewById(R.id.tv_cart_restaurant);
            tvPrice = itemView.findViewById(R.id.tv_cart_price);
            imgFood = itemView.findViewById(R.id.img_cart_item);
            tvQtt = itemView.findViewById(R.id.tv_quantity);
        }


    }
}
