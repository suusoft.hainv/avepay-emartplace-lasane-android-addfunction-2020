package com.aveplus.avepay_cpocket.restaurant.main.login;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.restaurant.configs.Global;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.listener.IConfirmation;
import com.aveplus.avepay_cpocket.restaurant.listener.ModelManagerListener;
import com.aveplus.avepay_cpocket.restaurant.main.login.models.UserModels;
import com.aveplus.avepay_cpocket.restaurant.main.mainAct.MainActivity;
import com.aveplus.avepay_cpocket.restaurant.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeEmail;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeUser;
import com.aveplus.avepay_cpocket.restaurant.social.facebook.FaceBookManager;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.util.StringUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.ViewDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_PASSWORD;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_SAVELOGIN;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_USERNAME;


public class FragmentLogin extends BaseFragment implements View.OnClickListener, View.OnTouchListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = FragmentLogin.class.getSimpleName();
    private static final int RC_GOOGLE_SIGN_IN = 9001;
    private static final int RC_PERMISSIONS = 1;

    private EditText edUsername, edPassword;
    private TextView tvLogin, tvRegister, tvForgotpassword, tvFacebook, tvGoogle;
    private CheckBox cb_save;
    private ArrayList<UserModels> dataUser;
    private SharedPreferences loginPrefernces;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;
    private InputMethodManager imm;
    private ProgressDialog progressDialog;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager mCallbackManager;
    private RequestQueue mRequestQueue;
    private boolean isFromSingup = false;
    private Bundle bundle;
    private String text;
    private FaceBookManager manager;
    private LinearLayout llnoConnect;
    private View mClickedView; // Keep button which was just clicked(google, facebook or login)


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_login;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        reQuest();
        llnoConnect = view.findViewById(R.id.ll_no_connection);
        edUsername = view.findViewById(R.id.ed_username_lg);
        edPassword = view.findViewById(R.id.ed_password_lg);
        tvRegister = view.findViewById(R.id.tv_register);
        tvForgotpassword = view.findViewById(R.id.tv_forgot_password);
        cb_save = view.findViewById(R.id.cb_saveLoginCheckBox);
        tvLogin = view.findViewById(R.id.tv_login);
        tvFacebook = view.findViewById(R.id.tv_facebook);
        tvGoogle = view.findViewById(R.id.tv_google);
        tvGoogle.setOnClickListener(this);
        tvFacebook.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvForgotpassword.setOnClickListener(this);
        checkNetworkshow();
        LoginActivity login = (LoginActivity) getActivity();
        loginPrefernces = login.getLoginPreferences();
        loginPrefsEditor = loginPrefernces.edit();

        saveLogin = loginPrefernces.getBoolean(PARAM_SAVELOGIN, false);
        if (saveLogin == true) {
            edUsername.setText(loginPrefernces.getString(PARAM_USERNAME, ""));
            edPassword.setText(loginPrefernces.getString(PARAM_PASSWORD, ""));
            cb_save.setChecked(true);
        }

        tvLogin.setOnTouchListener(this);
        tvFacebook.setOnTouchListener(this);
        tvGoogle.setOnTouchListener(this);
        mRequestQueue = Volley.newRequestQueue(self);
        initGoogleApiclien();


    }

    @Override
    protected void getData() {

    }

    private void checkNetworkshow() {
        if (NetworkUtility.isNetworkAvailable()) {
            llnoConnect.setVisibility(View.GONE);
        } else {
            llnoConnect.setVisibility(View.VISIBLE);
        }
    }

    private void initGoogleApiclien() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        mGoogleApiClient = new GoogleApiClient.Builder(self).addApi(Auth.GOOGLE_SIGN_IN_API, gso).
                enableAutoManage(getActivity(), this)
                .build();
    }


    @Override
    public void onClick(View view) {
        String username = edUsername.getText().toString().trim();
        String password = edPassword.getText().toString().trim();

        if (view == tvLogin) {
            mClickedView = tvLogin;
            if (StringUtil.isValidEmail(username)) {
                if (Global.isGranted(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, RC_PERMISSIONS, "")) {
                    loginUser();
                }
            }
        } else if (view == tvForgotpassword) {
            ShowConfirmPass();
        } else if (view == tvRegister) {
            LoginActivity loginActivity = (LoginActivity) getActivity();
            loginActivity.showFragment(loginActivity.getFrmRegister());
        } else if (view == tvGoogle) {
            mClickedView = tvGoogle;
            if (Global.isGranted(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSIONS, "")) {
                signInGoogle();
            }
        } else if (view == tvFacebook) {
            mClickedView = tvFacebook;
            if (Global.isGranted(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSIONS, "")) {
                signInFacebook();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSIONS: {
                if (grantResults.length > 0) {
                    if (grantResults.length == 1) {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            showPermissionsReminder(RC_PERMISSIONS, true);
                        } else {
                            if (mClickedView == tvGoogle) {
                                signInGoogle();
                                AppUtil.showToast(self, R.string.google);
                            } else if (mClickedView == tvFacebook) {
                                signInFacebook();
                                AppUtil.showToast(self, R.string.facebook);
                            } else if (mClickedView == tvLogin) {
                                loginUser();
                            }
                        }
                    } else if (grantResults.length == 2) {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED
                                || grantResults[1] == PackageManager.PERMISSION_DENIED) {
                            showPermissionsReminder(RC_PERMISSIONS, true);
                        } else {
                            if (mClickedView == tvGoogle) {
                                signInGoogle();
                                AppUtil.showToast(self, R.string.google);
                            } else if (mClickedView == tvFacebook) {
                                signInFacebook();
                                AppUtil.showToast(self, R.string.facebook);
                            } else if (mClickedView == tvLogin) {
                                loginUser();
                            }
                        }
                    }

                }
                break;
            }
            default:
                break;
        }
    }

    private void reQuest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    private void getLocation1() {
        if (ActivityCompat.checkSelfPermission(
                self, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
    }

    public void loginUser() {
        ViewDialog dialog = new ViewDialog(getActivity());
        dialog.showDialog();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                dialog.hideDialog();
            }
        };
        String username = edUsername.getText().toString().trim();
        String password = edPassword.getText().toString().trim();
        Handler handler = new Handler();
        handler.postDelayed(runnable, 2000);
        APIService apiService = ApiUtils.getAPIService();
        apiService.Login(username, password, UserModels.NORMAL).enqueue(new Callback<ResponeUser>() {
            @Override
            public void onResponse(Call<ResponeUser> call, Response<ResponeUser> response) {

                String token = response.body().getToken();

                if (response.body() != null) {


                    if (response.body().isSuccess()) {
                        DataStoreManager.saveUser(response.body().getData());
                    }
                    if (token != null) {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        DataStoreManager.saveToken(response.body().getToken());
                        startActivity(intent);

                        LoginActivity act = (LoginActivity) getActivity();
                        act.finish();

                    } else {
                        AppUtil.showToast(self, R.string.password_is_incorrect);

                    }
                }
                dialog.hideDialog();
            }

            @Override
            public void onFailure(Call<ResponeUser> call, Throwable t) {
                AppUtil.showToast(self, R.string.login_errors);
                Log.e("Error: ", t.getMessage());
                dialog.hideDialog();
                return;
            }
        });


    }

    private void login(final String name, String email, String password, final String avatar, final String loginMethod) {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            ViewDialog dialog = new ViewDialog(getActivity());
            dialog.showDialog();
            APIService apiService = ApiUtils.getAPIService();
            apiService.Login(email, password, name, avatar, UserModels.SOCIAL).enqueue(new Callback<ResponeUser>() {
                @Override
                public void onResponse(Call<ResponeUser> call, Response<ResponeUser> response) {
                    if (response.body().isSuccess((LoginActivity) self)) {
                        UserModels userModels = response.body().getData();
                        DataStoreManager.saveToken(response.body().getToken());
                        userModels.setName(name);
                        userModels.setAvatar(avatar);
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        DataStoreManager.saveUser(userModels);
                        startActivity(intent);
                        AppUtil.showToast(self, R.string.success);
                        LoginActivity act = (LoginActivity) getActivity();
                        act.finish();
                        dialog.hideDialog();
                    }
                }

                @Override
                public void onFailure(Call<ResponeUser> call, Throwable t) {
                    Toast.makeText(self, getString(R.string.msg_have_some_error), Toast.LENGTH_SHORT).show();
                    dialog.hideDialog();
                }
            });
        } else {
            Toast.makeText(self, getString(R.string.msg_no_network), Toast.LENGTH_SHORT).show();
        }

    }

    private void ShowConfirmPass() {
        AlertDialog.Builder alert = new AlertDialog.Builder(self);
        alert.setTitle("Alert");
        alert.setMessage(getString(R.string.enter_your_email_address));

        final EditText edEmail = new EditText(self);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        edEmail.setLayoutParams(lp);
        alert.setView(edEmail);
        alert.setIcon(R.drawable.logo);
        alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String email = edEmail.getText().toString();
                if (!StringUtil.isValidEmail(email) || StringUtil.isEmpty(email)) {
                    AppUtil.showToast(self, R.string.email_error);
                    return;
                }

                APIService apiService = ApiUtils.getAPIService();
                apiService.Forgotpassword(email).enqueue(new Callback<ResponeEmail>() {
                    @Override
                    public void onResponse(Call<ResponeEmail> call, Response<ResponeEmail> response) {
                        String status = response.body().getStatus();
                        AppUtil.showToast(self, R.string.please_check_mail);
                    }

                    @Override
                    public void onFailure(Call<ResponeEmail> call, Throwable t) {
                        Toast.makeText(self, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ", t.getMessage());
                    }
                });

            }
        });
        alert.setPositiveButton("Cansel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alert.show();
    }

    public EditText getEdUsername() {
        return edUsername;
    }

    public EditText getEdPassword() {
        return edPassword;
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.tv_login:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tvLogin.setBackgroundResource(R.drawable.shap_hover);
                    tvLogin.setTextColor(Color.BLACK);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    tvLogin.setBackgroundResource(R.drawable.shap_border_white);
                    tvLogin.setTextColor(Color.WHITE);
                }
                break;
            case R.id.tv_google:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tvGoogle.setBackgroundResource(R.drawable.shap_google_press);
                    tvGoogle.setTextColor(Color.WHITE);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    tvGoogle.setBackgroundResource(R.drawable.shap_google);
                    tvGoogle.setTextColor(Color.WHITE);
                }
                break;
            case R.id.tv_facebook:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tvFacebook.setBackgroundResource(R.drawable.shap_facebook_press);
                    tvFacebook.setTextColor(Color.WHITE);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    tvFacebook.setBackgroundResource(R.drawable.shap_facebook);
                    tvFacebook.setTextColor(Color.WHITE);
                }
                break;
        }
        return false;
    }

    private void signInGoogle() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            // Sign out before signing in again
            if (mGoogleApiClient.isConnected()) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {

                            }
                        });

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN);
            } else {
                mGoogleApiClient.connect();
            }
        } else {
            Toast.makeText(self, getString(R.string.network_is_not_connect), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCallbackManager != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        }
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String email = "", name = "", avatar = "";
            if (acct.getEmail() != null) {
                email = acct.getEmail();
            }
            if (acct.getDisplayName() != null) {
                name = acct.getDisplayName();
            }
            if (acct.getPhotoUrl() != null) {
                avatar = acct.getPhotoUrl().toString();
            }

            if (!email.equals("")) {
                login(name, email, "", avatar, UserModels.SOCIAL);
            } else {
                Toast.makeText(self, getString(R.string.msg_can_not_get_email), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void gotoHome() {

        Global.startActivityWithoutAnimation(self, MainActivity.class, bundle);
        // Close this activity
        getActivity().finish();
    }

    protected void showPermissionsReminder(final int reqCode, final boolean flag) {
        Global.showConfirmationDialog(self, getString(R.string.msg_remind_user_grants_permissions),
                getString(R.string.allow), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        Global.isGranted(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.ACCESS_FINE_LOCATION}, reqCode, null);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            getActivity().finish();
                        }
                    }
                });
    }

    // Login with Facebook
    private void signInFacebook() {
        if (NetworkUtility.isNetworkAvailable()) {
            mCallbackManager = CallbackManager.Factory.create();

            if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut();
            }

            LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    String accessToken = loginResult.getAccessToken().getToken();
                    ModelManager.getFacebookInfo(self, mRequestQueue, accessToken, new ModelManagerListener() {
                        @Override
                        public void onSuccess(Object object) {
                            JSONObject jsonObject = (JSONObject) object;
                            String email = jsonObject.optString("email");
                            String name = jsonObject.optString("name");
                            String avatar = "https://graph.facebook.com/" + jsonObject.optString("id") + "/picture?type=large";

//                            try {
//                                avatar = jsonObject.getJSONObject("picture").getJSONObject("data").optString("url");
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
                            if (!email.equals("")) {
                                Log.e("Splash", "onSuccess login fb2");
                                login(name, email, "", avatar, UserModels.SOCIAL);
                            } else {
                                Toast.makeText(self, R.string.msg_can_not_get_email, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError() {
                            Log.e("Splash", "onError login fb");
                            Toast.makeText(self, R.string.msg_have_some_errors, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onCancel() {
                    Log.e("onCancel", "Facebook Login cancel");
                }

                @Override
                public void onError(FacebookException error) {
                    Log.e("onError", "Facebook Login error");
                }
            });
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        } else {
            Toast.makeText(self, getString(R.string.msg_no_network), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
