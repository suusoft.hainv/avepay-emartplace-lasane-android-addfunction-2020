package com.aveplus.avepay_cpocket.music.main.payment;

import android.content.Context;
import android.util.Log;

import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.google.gson.Gson;


import java.util.List;

public class SubscribeVM extends BaseViewModelList {
    private static final String TAG = SubscribeVM.class.getSimpleName();

    public SubscribeVM(Context context) {
        super(context);
        getData(1);
    }

    @Override
    public void getData(int page) {

        RequestManager.getListSubscribe(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                List<PackageSubscribe> dataList = response.getDataList(PackageSubscribe.class);
                Log.e(TAG, "onSuccess: " + new Gson().toJson(dataList));
                addListData(dataList);
            }

            @Override
            public void onError(String message) {

            }
        });
    }
}
