package com.aveplus.avepay_cpocket.restaurant.main.home.Banner;


import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;

public class BannerModel extends BaseModel {
    private String image;

    public BannerModel(String image) {
        this.image = image;

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
