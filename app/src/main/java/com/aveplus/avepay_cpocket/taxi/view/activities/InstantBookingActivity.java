package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.Intent;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.view.instant.BookingFragment;


public class InstantBookingActivity extends BaseActivity {


    private BookingFragment fragment;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NORMAL;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_contain_fragment;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {
        setToolbarTitle(R.string.instant_booking);

        fragment = BookingFragment.newInstance();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fr_content, fragment).commit();

    }

    @Override
    protected void onViewCreated() {

    }
}
