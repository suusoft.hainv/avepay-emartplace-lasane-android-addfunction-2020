package com.aveplus.avepay_cpocket.music.configs;

import android.os.Environment;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

import java.io.File;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Config {
    public static final String APP_DOMAIN = "https://www.googleapis.com/youtube/v3/";
    public static final String URL_API = AppController.getInstance().getString(R.string.URL_API_MUSIC) + "backend/web/index.php/api/";
    public static final int MAX_RESULT = 20;
    public static final int MAX_RESULT_PLAYLIST = 100;


    public static class Api {
        // Domain
        public static final String PLAYLIST = Config.APP_DOMAIN + "playlists";
        public static final String PLAYLIST_ITEMS = Config.APP_DOMAIN + "playlistItems";
        public static final String VIDEO = Config.APP_DOMAIN + "videos";
        public static final String CHANNELS = Config.APP_DOMAIN + "channels";


        public static final String HOME = URL_API+ "home/statistics";
        public static final String ITEM_BY_CATEGORY = URL_API+ "list-song/category";
        public static final String ITEM_BY_PLAYLIST = URL_API+ "list-song/playlist";
        public static final String ALL_CATEGORIES = URL_API+ "category/list";
        public static final String ALL_ALBUM_BY= URL_API+ "playlist/view-more";
        public static final String SEARCH = URL_API+ "search/index?";
        public static final String REGISTER_DEVICE = URL_API+ "device/index";

        // Urls
        public static final String URL_LOGIN = Config.APP_DOMAIN + "login";
        public static final String URL_LOGOUT = Config.APP_DOMAIN + "logout";
        public static final String URL_REGISTER = Config.APP_DOMAIN + "register";
        public static final String URL_RESET_PASSWORD = Config.APP_DOMAIN + "resetPassword";
        public static final String URL_GET_ALL = Config.APP_DOMAIN + "browseTask";
        public static final String URL_GET_VIDEO = URL_API+"video/list-feature";
        public static final String URL_GET_CATEGORY= URL_API+"category/list";
        public static final String URL_GET_VIDEO_CATEGORY= URL_API+"video/category";
        public static final String URL_GET_LIST_SUBSCRIBE= URL_API+"subscribe/list-package";
        public static final String URL_GET_PAYMENT_KHALTI= URL_API+"payment/khalti";
        public static final String URL_GET_PAYMENT_ESEWA= URL_API+"payment/esewa";
        public static final String URL_CHECK_PAYMENT= URL_API+"subscribe/check";

    }

//    public static String getPathDir() {
//        File file = Environment.getExternalStorageDirectory();
//        return file.getPath() + "/" + getNameDownloadFolder();
//
//    }
    public static String getPathDir() {
//old
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString ().trim ();



        File file =  new File(root + "/" + getNameDownloadFolder());
        return file.getPath ();
    }
    public static String getExternalFolder() {
        File file = Environment.getExternalStorageDirectory();
        return file.getPath();

    }

    public static String getNameDownloadFolder(){
        return  "suusoft.com/downloads";
    }

}
