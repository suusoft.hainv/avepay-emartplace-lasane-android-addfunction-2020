package com.aveplus.avepay_cpocket.movie.movie.main.detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;


import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public class MoviesAdapter extends ArrayAdapter {

    private int resId;
    private List<Movie> mMovies;
    private LayoutInflater mInflater;
    private ImageLoader mImageLoader;

    public MoviesAdapter(Context context, List<Movie> movies) {
        super(context, R.layout.adapter_movies_item, movies);

        resId = R.layout.adapter_movies_item;
        mMovies = movies;
        mInflater = LayoutInflater.from(context);
        mImageLoader = RequestManager.getImageLoader();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(resId, null);
        }

        NetworkImageView imageView = (NetworkImageView)view.findViewById(R.id.imageView);
        Movie movie = mMovies.get(position);
        imageView.setImageUrl(movie.getMovie_thumb(), mImageLoader);
        imageView.setDefaultImageResId(R.drawable.poster);
        imageView.setErrorImageResId(R.drawable.poster);
        return view;
    }
}
