package com.aveplus.avepay_cpocket.trainingcenter.object;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Product implements Parcelable {
    private int numberCart;
    private String id, name, description, content, categoryId, price;
    private ArrayList<String> image;
    private ArrayList<Class> mListClass;
    private String idClassChose;

    public Product() {
    }

    protected Product(Parcel in) {
        numberCart = in.readInt();
        id = in.readString();
        name = in.readString();
        description = in.readString();
        content = in.readString();
        categoryId = in.readString();
        price = in.readString();
        image = in.createStringArrayList();
        mListClass = in.createTypedArrayList(Class.CREATOR);
        idClassChose = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getNumberCart() {
        return numberCart;
    }

    public void setNumberCart(int total) {
        this.numberCart = total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getPrice() {
        return price;
    }

    public ArrayList<Class> getmListClass() {
        return mListClass;
    }

    public void setmListClass(ArrayList<Class> mListClass) {
        this.mListClass = mListClass;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIdClassChose() {
        return idClassChose;
    }

    public void setIdClassChose(String idClassChose) {
        this.idClassChose = idClassChose;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(numberCart);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(content);
        dest.writeString(categoryId);
        dest.writeString(price);
        dest.writeStringList(image);
        dest.writeTypedList(mListClass);
        dest.writeString(idClassChose);
    }
}
