package com.aveplus.avepay_cpocket.interfaces;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface onPageChangeAdapter {
    public void onClickItemSubcate(int subcatePosition);
    public void onNextPage(int position);
    public void onPrevPage(int position);
}
