package com.aveplus.avepay_cpocket.restaurant.main.save;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.main.save.savemodels.RestaurantInfo;
import com.bumptech.glide.Glide;


import java.util.List;

public class SaveAdapter extends RecyclerView.Adapter<SaveAdapter.viewHoler> {
    private Context context;
    private List<RestaurantInfo> restaurantInfos;
    private final OnItemClickListener listener;
    private ItemimgUnsaveListener unsaveListener;


    public SaveAdapter(Context context, List<RestaurantInfo> restaurantInfos, OnItemClickListener listener) {
        this.context = context;
        this.restaurantInfos = restaurantInfos;
        this.listener = listener;
    }

    public void setUnsaveListener(ItemimgUnsaveListener unsaveListener) {
        this.unsaveListener = unsaveListener;
    }

    public interface OnItemClickListener {
        void onItemClick(RestaurantInfo restaurantInfo);
    }

    @NonNull
    @Override
    public SaveAdapter.viewHoler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from (context).inflate (R.layout.row_save, viewGroup, false);
        return new viewHoler (view, unsaveListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SaveAdapter.viewHoler holer, int i) {
        holer.Container(restaurantInfos.get(i));

    }

    @Override
    public int getItemCount() {
        return restaurantInfos == null ? 0 : restaurantInfos.size();
    }

    public class viewHoler extends RecyclerView.ViewHolder {
        private ImageView imgFood, btnUnsave;
        private TextView tvNameFood;
        private TextView tvAddress, tvOpen, tvClose;
        private ItemimgUnsaveListener unsaveListener;

        public viewHoler(@NonNull View itemView, ItemimgUnsaveListener mUnsaveListener) {
            super (itemView);
            unsaveListener = mUnsaveListener;
            imgFood = itemView.findViewById (R.id.img_foodd);
            tvOpen = itemView.findViewById (R.id.tv_open);
            tvClose = itemView.findViewById(R.id.tv_close);
            btnUnsave = itemView.findViewById(R.id.btn_unsave);
            tvNameFood = itemView.findViewById (R.id.tv_name_food);
            tvAddress = itemView.findViewById (R.id.tv_address);
            btnUnsave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view == btnUnsave) {
                        unsaveListener.onUnsaveclick(btnUnsave, getAdapterPosition());
                    }
                }
            });

        }

        public void Container(final RestaurantInfo restaurantInfos) {
            tvAddress.setText (restaurantInfos.getAddress());
            tvNameFood.setText (restaurantInfos.getName());
            Glide.with(context).load(restaurantInfos.getImage()).into(imgFood);
            String open = restaurantInfos.getOpenHour();
            tvOpen.setText(open);
            tvClose.setText(open);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(restaurantInfos);
                }
            });


        }
    }

    public interface ItemimgUnsaveListener{
        void onUnsaveclick(View view, int position);
    }
}
