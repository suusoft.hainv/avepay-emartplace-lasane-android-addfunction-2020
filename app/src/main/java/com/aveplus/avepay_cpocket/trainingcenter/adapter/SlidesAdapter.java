package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.activity.BlogDetailActivity;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class SlidesAdapter extends PagerAdapter {
    private ImageView imgflag;
    private TextView tvTittleBlog;
    private Activity mContext;
    private LayoutInflater inflaterl;
    private ArrayList<NewEvent> arrImage = new ArrayList<NewEvent>();

    public SlidesAdapter(Activity con, ArrayList<NewEvent> arrImage) {
        mContext = con;
        this.arrImage = arrImage;
        inflaterl = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrImage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = (int) v.getTag();
                NewEvent item = arrImage.get(i);
                Bundle bundle = new Bundle();
                bundle.putString("id", item.getId());
                bundle.putString("title", item.getTitle());
                bundle.putString("description", item.getDescription());
                bundle.putString("content", item.getContent());
                bundle.putString("image", item.getImage());
                bundle.putString("time", item.getTime());
                bundle.putString("link", item.getLinkNews());
                Intent intent = new Intent(mContext, BlogDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("bundle", bundle);
                mContext.startActivity(intent);
            }
        });
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = inflaterl.inflate(R.layout.item_product_detail,
                container, false);
        itemView.setTag(position);
        imgflag = (ImageView) itemView.findViewById(R.id.imgProDetail);
        tvTittleBlog = (TextView) itemView.findViewById(R.id.tvTitleBlog);
       // tvTittleBlog.setText(arrImage.get(position).getTitle());
        Picasso.get().load(arrImage.get(position).getImage()).into(imgflag);
        ((ViewPager) container).addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}
