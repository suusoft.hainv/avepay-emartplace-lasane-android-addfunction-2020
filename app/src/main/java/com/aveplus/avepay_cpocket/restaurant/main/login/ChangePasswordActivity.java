package com.aveplus.avepay_cpocket.restaurant.main.login;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.main.login.models.UserModels;
import com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeUser;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.util.StringUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {
    public static final String PASS = "pass";
    private static final String TAG = ChangePasswordActivity.class.getName();
    public static final int RC_CREATE_PASS = 123;
    private EditText edtOldPass, edtPass, edtConfirmPass;
    private TextView btnChangePassWord;
    private EditText edtEmail;

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_change_pass_word_restaurant;
    }

    @Override
    protected void initView() {
//        setToolbarTitle("Change Password");
        edtOldPass = findViewById(R.id.edt_current_pass);
        edtPass = findViewById(R.id.edt_password);
        edtConfirmPass = findViewById(R.id.edt_password_confirm);
        btnChangePassWord = findViewById(R.id.btn_change_password);
        edtEmail = findViewById(R.id.edt_email);


    }

    @Override
    protected void onViewCreated() {
        btnChangePassWord.setOnClickListener(this);
        UserModels userObj = DataStoreManager.getUserModels();
        edtEmail.setTag(edtEmail.getKeyListener());
        edtEmail.setKeyListener(null);

        edtEmail.setVisibility(View.GONE);
        edtOldPass.setVisibility(View.VISIBLE);
        btnChangePassWord.setText(getString(R.string.button_change_pass));

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    public void onClick(View view) {
        if (view == btnChangePassWord) {
            if (NetworkUtility.isNetworkAvailable()) {
//                String oldpass = edtOldPass.getText().toString().trim();
                final String pass = edtPass.getText().toString().trim();
                String confirmPass = edtConfirmPass.getText().toString().trim();

                if (StringUtil.isEmpty(pass)) {
                    AppUtil.showToast(this, R.string.msg_password_required);
                    return;
                } else {
                    if (!StringUtil.isValidatePassword(pass)) {
                        AppUtil.showToast(this, R.string.msg_password_required);
                        return;
                    }
                }
                if (StringUtil.isEmpty(confirmPass)) {
                    AppUtil.showToast(this, R.string.msg_confirm_pass_is_required);
                    return;
                } else if (!confirmPass.equals(pass)) {
                    AppUtil.showToast(this, R.string.msg_password_is_not_match);
                    return;
                }

                APIService apiService = ApiUtils.getAPIService();
                apiService.updatePassword(DataStoreManager.getToken(), pass).enqueue(new Callback<ResponeUser>() {
                    @Override
                    public void onResponse(Call<ResponeUser> call, Response<ResponeUser> response) {
                        if (response.body().isSuccess()) {
                            Intent intent = new Intent();
                            intent.putExtra(PASS, pass);
                            setResult(Activity.RESULT_OK, intent);
                            AppUtil.showToast(getApplicationContext(), R.string.msg_success);
                            finish();
                        } else {
                            AppUtil.showToast(self, response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponeUser> call, Throwable t) {
                        AppUtil.showToast(self, t.getMessage());
                        Log.e("Error", t.getMessage());
                    }
                });
            }
        }
    }

    private boolean isValid() {
        String oldpass = edtOldPass.getText().toString().trim();
        String pass = edtPass.getText().toString().trim();
        String confirmPass = edtConfirmPass.getText().toString().trim();
        if (DataStoreManager.getUserModels().isSecured()) {
            if (StringUtil.isEmpty(oldpass)) {
                AppUtil.showToast(this, R.string.msg_fill_current_password);
                return false;
            }
        }

        if (StringUtil.isEmpty(pass)) {
            AppUtil.showToast(this, R.string.msg_password_required);
            return false;
        } else {
            if (!StringUtil.isValidatePassword(pass)) {
                AppUtil.showToast(this, R.string.msg_password_required);
                return false;
            }
        }
        if (StringUtil.isEmpty(confirmPass)) {
            AppUtil.showToast(this, R.string.msg_confirm_pass_is_required);
            return false;
        } else if (!confirmPass.equals(pass)) {
            AppUtil.showToast(this, R.string.msg_password_is_not_match);
            return false;
        }
        return true;
    }
}
