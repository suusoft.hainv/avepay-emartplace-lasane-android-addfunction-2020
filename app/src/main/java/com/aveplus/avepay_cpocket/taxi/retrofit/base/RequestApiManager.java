package com.aveplus.avepay_cpocket.taxi.retrofit.base;

import android.util.Log;


import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseTripObj;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestApiManager extends BaseRequestApi {


    public static void getTripStatus(final ICompleteListener iCompleteListener) {
        getInstance().showProgress(true);
        mAPIService = ApiUtils.getAPIService();
        mAPIService.getTripStatus(DataStoreManager.getUser().getId(), "0", DataStoreManager.getTokenUser())
                .enqueue(new Callback<ResponseTripObj>() {
                    @Override
                    public void onResponse(Call<ResponseTripObj> call, Response<ResponseTripObj> response) {
                        getInstance().showProgress(false);
                        Log.e(TAG, "respone: " + response.body().toJson());


                    }

                    @Override
                    public void onFailure(Call<ResponseTripObj> call, Throwable t) {
                        getInstance().showProgress(false);
                        Log.e(TAG, "onFailure: " + t.getMessage());
                    }
                });
    }


}
