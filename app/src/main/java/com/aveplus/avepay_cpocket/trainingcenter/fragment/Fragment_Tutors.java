package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.OurPeopleAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.OurPeople;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class Fragment_Tutors extends BaseFragment {

    private GridView gridpeople;
    private OurPeopleAdapter peopleadapter;
    private ArrayList<OurPeople> listpeople;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("ourpeople");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onPause();
//		if (onTitleChanged != null)
//			onTitleChanged.onTitleChanged("about");
//		else
//			Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater
                .inflate(R.layout.fragment_people, container, false);

        initView(view);
        initData();

        return view;

    }

    private void initView(View view) {
        gridpeople = (GridView) view.findViewById(R.id.gridPeople);
    }

    private void initData() {
        ModelManager.getListPEOPLE(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                listpeople = ParseUtility.parseListOurPeople(response.getRootStr());
                peopleadapter = new OurPeopleAdapter(getActivity(),
                        listpeople);
                gridpeople.setAdapter(peopleadapter);

                gridpeople
                        .setOnItemClickListener(new OnItemClickListener() {

                            @Override
                            public void onItemClick(
                                    AdapterView<?> parent, View view,
                                    int position, long id) {
                                FragmentDetailTutors fragment = new FragmentDetailTutors();

                                OurPeople item = listpeople
                                        .get(position);
                                Bundle bundle = new Bundle();
                                bundle.putString("id", item.getId());
                                bundle.putString("name", item.getName());
                                bundle.putString("position",
                                        item.getPosition());
                                bundle.putString("email",
                                        item.getEmail());
                                bundle.putString("image",
                                        item.getImage());
                                bundle.putString("skype",
                                        item.getSkype());
                                bundle.putString("description",
                                        item.getDescription());
                                fragment.setArguments(bundle);

                                getFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(
                                                R.anim.slide_in_left,
                                                R.anim.slide_out_left)
                                        .replace(R.id.showPeople,
                                                fragment,
                                                "detailtutor")
                                        .addToBackStack("detailtutor").commit();
                            }
                        });
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

}
