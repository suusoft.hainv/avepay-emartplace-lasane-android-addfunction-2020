package com.aveplus.avepay_cpocket.restaurant.listener;

public interface IConfirmation {

    void onPositive();

    void onNegative();
}
