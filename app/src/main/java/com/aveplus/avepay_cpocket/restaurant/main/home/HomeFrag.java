package com.aveplus.avepay_cpocket.restaurant.main.home;
/**
 * Created by suusoft.com on 11/12/17.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.main.home.Banner.AdapterHomeBanner;
import com.aveplus.avepay_cpocket.restaurant.main.home.Banner.BannerModel;
import com.aveplus.avepay_cpocket.restaurant.main.home.Banner.ViewPagerAdapter;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.RestaurantAct;
import com.aveplus.avepay_cpocket.restaurant.main.search.SearchActivity;
import com.aveplus.avepay_cpocket.restaurant.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.restaurant.network.ApiResponse;
import com.aveplus.avepay_cpocket.restaurant.network.BaseRequest;
import com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponseListRestaurant;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.recyclerview.EndlessRecyclerOnScrollListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;


import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static androidx.core.content.PermissionChecker.checkSelfPermission;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.ID_RESTAURANT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LAT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LONG;


public class HomeFrag extends BaseFragment implements View.OnClickListener, EndlessRecyclerOnScrollListener.OnLoadMoreListener {
    //Define a request code to send to Google Play services

    private FusedLocationProviderClient client;
    private static final int REQUEST_LOCATION = 1;
    private RecyclerView rc_restaurant, rcNearby;
    private RestaurantAdapter restaurantAdapter;
    private ArrayList<RestaurantModel> restauranList;
    private ArrayList<RestaurantModel> restauranListNearBy;
    private Button btnRecent, btnNearby;
    private LinearLayout searchView;
    private LinearLayout llnoData, llnoConnect, indicator;
    private double longitude;
    private double latitude;
    private int page;
    private TextView tvRestaurant;
    String TAG = "TAG";
    private List<BannerModel> bannerList;
    private BannerModel bannerModel;
    private EndlessRecyclerOnScrollListener onScrollListener;

    int currentPage = 0;
    private Timer timer;
    final long DELAY_MS = 2000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.
    static final int COLOR_INACTIVE = Color.WHITE;
    static final int COLOR_ACTIVE = Color.RED;

    private ViewPager mPager;
    private AdapterHomeBanner mAdapter;
    private ViewPagerAdapter mPagerAdapter;
    private LocationManager locationManager;

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_home_restaurant;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        reQuest();
        mPager = view.findViewById(R.id.home_banner);
        llnoData = view.findViewById(R.id.ll_no_data);
        llnoConnect = view.findViewById(R.id.ll_no_connection);
        searchView = view.findViewById(R.id.searchView);
        btnRecent = view.findViewById(R.id.btn_recent);
        btnNearby = view.findViewById(R.id.btn_nearby);
        rc_restaurant = view.findViewById(R.id.rc_restaurant);
        tvRestaurant = view.findViewById(R.id.tv_number_restaurant);

        rcNearby = view.findViewById(R.id.rc_restaurant_nearby);
        restauranListNearBy = new ArrayList<>();
        restauranList = new ArrayList<>();


        initPermission();

        Log.e(TAG, "initView: " + longitude);

        searchView.setOnClickListener(this);
        btnRecent.setOnClickListener(this);
        btnNearby.setOnClickListener(this);
        rc_restaurant.setHasFixedSize(true);
        rc_restaurant.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rcNearby.setLayoutManager(new GridLayoutManager(getContext(), 2));


        ActivityCompat.requestPermissions(getActivity(),
                new String[]{ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

//        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            OnGPS();
//        }


        checkNetworkshow();
//        restaurantAdapter = new RestaurantAdapter(self, restauranList, new RestaurantAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(RestaurantModel restaurantModel) {
//                Intent intent = new Intent(self, RestaurantAct.class);
//                intent.putExtra(USER_LONG, longitude);
//                intent.putExtra(USER_LAT, latitude);
//                intent.putExtra(ID_RESTAURANT, restaurantModel.getId());
//                startActivity(intent);
//            }
//        });

//
        // Indicator:
        getBannerList();
        indicator = view.findViewById(R.id.indicator);
        openLocation();
        getRecentRestaurant();
        getLocation1();

        onScrollListener = new EndlessRecyclerOnScrollListener(this);
        //rcNearby.addOnScrollListener(onScrollListener);
        //rc_restaurant.addOnScrollListener(onScrollListener);
        onScrollListener.setEnded(false);


    }

    @Override
    protected void getData() {
        Bundle bundle = getArguments();
    }

    private void getLocation1() {
        client = LocationServices.getFusedLocationProviderClient(self);
        if (ActivityCompat.checkSelfPermission(
                self, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;

        }
        client.getLastLocation().addOnSuccessListener((Activity) self, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    longitude = location.getLongitude();
                    latitude = location.getLatitude();
                    getNearbyRestaurant(latitude, longitude);
                }
            }
        });
    }

    private void reQuest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, 1);
    }


    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(self);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                self, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                self, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                latitude = locationGPS.getLatitude();
                longitude = locationGPS.getLongitude();
                Log.e(TAG, "Your Location: " + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude);
            } else {
                Toast.makeText(self, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void isLocationEnabled() {

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(self);
            alertDialog.setTitle("Enable Location");
            alertDialog.setMessage("Your locations setting is not enabled. Please enabled it in settings menu.");
            alertDialog.setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(self);
            alertDialog.setTitle("Confirm Location");
            alertDialog.setMessage("Your Location is enabled, please enjoy");
            alertDialog.setNegativeButton("Back to interface", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
    }


    View createDot(Context context, @ColorInt int color) {
        View dot = new View(context);
        LinearLayout.MarginLayoutParams dotParams = new LinearLayout.MarginLayoutParams(20, 20);
        dotParams.setMargins(20, 10, 20, 10);
        dot.setLayoutParams(dotParams);
        ShapeDrawable drawable = new ShapeDrawable(new OvalShape());
        drawable.setTint(color);
        dot.setBackground(drawable);
        return dot;
    }


    private void getRecentRestaurant() {
        RequestManager.getRestaurant(self, 1, 20, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (!response.isError()) {
                    restauranList.addAll(response.getDataList(RestaurantModel.class));
                    restaurantAdapter = new RestaurantAdapter(self, restauranList, new RestaurantAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(RestaurantModel restaurantModel) {
                            Intent intent = new Intent(self, RestaurantAct.class);
                            intent.putExtra(USER_LONG, longitude);
                            intent.putExtra(USER_LAT, latitude);
                            intent.putExtra(ID_RESTAURANT, restaurantModel.getId());
                            startActivity(intent);
                        }
                    });
                    rc_restaurant.setAdapter(restaurantAdapter);
                    restaurantAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onError(String message) {
                Log.e(TAG, "onError: " + message);
            }
        });

    }

    private void getBannerList() {
        if (NetworkUtility.isNetworkAvailable()) {
            APIService retrofitClient = ApiUtils.getAPIService();
            retrofitClient.getRecentRestaurant().enqueue(new Callback<ResponseListRestaurant>() {
                @Override
                public void onResponse(Call<ResponseListRestaurant> call, Response<ResponseListRestaurant> response) {
                    if (response.isSuccessful()) {

                        if (response.body() != null) {

                            if (response.body().isSuccess()) {
                                bannerList = new ArrayList<>();
                                if (bannerList.size() == 0) {
                                    bannerList.addAll(response.body().getBanner());
                                    mAdapter = new AdapterHomeBanner(self, bannerList);
                                    mPager.setAdapter(mAdapter);
                                    indicator.removeAllViews();
                                    getBanner();
                                }
                                Log.e(TAG, "onResponse: " + bannerList.size());

                            } else {
                                AppUtil.showToast(self, response.body().getMessage());
                            }

                        }
                    }

                }

                @Override
                public void onFailure(Call<ResponseListRestaurant> call, Throwable t) {
                }
            });
        } else {
            AppUtil.showToast(self, getString(R.string.msg_no_network));
        }
    }

    private void getBanner() {
        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == bannerList.size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        timer = new Timer();  // This will create a new Thread
        timer.schedule(new TimerTask() {  // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);


        // changeColorDot
        for (int i = 0; i < bannerList.size(); i++) {
            // COLOR_ACTIVE ứng với chấm ứng với vị trí hiện tại của ViewPager,
            // COLOR_INACTIVE ứng với các chấm còn lại
            // ViewPager có vị trí mặc định là 0, vì vậy color ở vị trí i == 0 sẽ là COLOR_ACTIVE
            View dot = createDot(indicator.getContext(), i == bannerList.size() ? COLOR_ACTIVE : COLOR_INACTIVE);
            indicator.addView(dot);
        }

        // Thay đổi màu các chấm khi ViewPager thay đổi vị trí:
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for (int i = 0; i < bannerList.size(); i++) {
                    // Duyệt qua từng "chấm" trong indicator
                    // Nếu i == position, tức i đang là vị trí hiện tại của ViewPager,
                    // ta sẽ đổi màu "chấm" thành COLOR_ACTIVE, nếu không
                    // thì sẽ đổi thành màu COLOR_INACTIVE
                    indicator.getChildAt(i).getBackground().mutate().setTint(i == position ? COLOR_ACTIVE : COLOR_INACTIVE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void getNearbyRestaurant(double lat, double longitude) {
        Log.d(TAG, "getNearbyRestaurant: ");
        if (NetworkUtility.isNetworkAvailable()) {
            RequestManager.getRestaurantNearBy(self, lat, longitude, DataStoreManager.getToken(), 1, 500, new BaseRequest.CompleteListener() {
                @Override
                public void onSuccess(ApiResponse response) {
                    if (!response.isError()) {
                        restauranListNearBy.addAll(response.getDataList(RestaurantModel.class));
                        tvRestaurant.setText("(" + restauranListNearBy.size() + ")");
                        restaurantAdapter = new RestaurantAdapter(self, restauranListNearBy, new RestaurantAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(RestaurantModel restaurantModel) {
                                Intent intent = new Intent(self, RestaurantAct.class);
                                intent.putExtra(USER_LONG, longitude);
                                intent.putExtra(USER_LAT, latitude);
                                intent.putExtra(ID_RESTAURANT, restaurantModel.getId());
                                startActivity(intent);
                            }
                        });
                        rcNearby.setAdapter(restaurantAdapter);
                        restaurantAdapter.notifyDataSetChanged();
                    }

//                    checkViewHideShow2();
                }

                @Override
                public void onError(String message) {

                }
            });
        } else {
            AppUtil.showToast(self, getString(R.string.msg_no_network));
        }
    }

    private void checkViewHideShow() {
        if (restauranList.isEmpty()) {
            llnoData.setVisibility(View.VISIBLE);
        } else {
            llnoData.setVisibility(View.GONE);
        }
    }

    private void checkViewHideShow2() {
        if (restauranListNearBy.isEmpty()) {
            llnoData.setVisibility(View.VISIBLE);
        } else {
            llnoData.setVisibility(View.GONE);
        }
    }

    private void checkNetworkshow() {
        if (NetworkUtility.isNetworkAvailable()) {
            llnoConnect.setVisibility(View.GONE);
        } else {
            llnoConnect.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_recent:
                btnNearby.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL), Typeface.NORMAL);
                btnRecent.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL), Typeface.BOLD);
//                restauranList.clear ();
//                getRecentRestaurant ();
                rcNearby.setVisibility(View.GONE);
                rc_restaurant.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_nearby:
                btnNearby.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL), Typeface.BOLD);
                btnRecent.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL), Typeface.NORMAL);
//                openLocation ();
//                getLocation1 ();
//                restauranList.clear ();
                rc_restaurant.setVisibility(View.GONE);
                rcNearby.setVisibility(View.VISIBLE);
//                getNearbyRestaurant ();
                break;

            case R.id.searchView:
                Intent search = new Intent(self, SearchActivity.class);
                startActivity(search);
                break;
        }

    }


    private void openLocation() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(self)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(getActivity(), 1);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                getActivity().finish();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @SuppressLint("WrongConstant")
    public void initPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(self, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(self, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION + Manifest.permission.WRITE_EXTERNAL_STORAGE);
                //Register permission
                requestPermissions(new String[]{ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            } else if (checkSelfPermission(self, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION);
                //Register permission
                requestPermissions(new String[]{ACCESS_FINE_LOCATION}, 1);
            }
        } else if (checkSelfPermission(self, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            //Register permission
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override
    public void onLoadMore(int page) {
        this.page = page;
    }


    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            Toast.makeText(getContext(), "OnLocationChanged", Toast.LENGTH_SHORT).show();   // Toast not showing
            String longitude = "Longitude: " + loc.getLongitude();
            String latitude = "Latitude: " + loc.getLatitude();
            String s = longitude + "\n" + latitude;
            Log.e(TAG, "onLocationChanged: " + s);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getContext(), "onProviderDisabled", Toast.LENGTH_SHORT).show();  // Toast not showing
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(getContext(), "on ProviderEnabled", Toast.LENGTH_SHORT).show();  // Toast not showing
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Toast.makeText(getContext(), "onStatusChanged", Toast.LENGTH_SHORT).show();  // Toast not showing
        }
    }
}
