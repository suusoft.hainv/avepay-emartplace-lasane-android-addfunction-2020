package com.aveplus.avepay_cpocket.view.fragments;

import android.view.View;

import com.aveplus.avepay_cpocket.R;

/**
 * Created by Suusoft on 10/16/2017.
 */

public class PageChatFragment extends com.aveplus.avepay_cpocket.base.BaseFragment {


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_chat;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {

    }

    @Override
    protected void getData() {

    }
}
