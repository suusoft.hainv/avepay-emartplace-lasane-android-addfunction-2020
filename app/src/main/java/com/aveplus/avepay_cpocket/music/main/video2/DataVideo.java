package com.aveplus.avepay_cpocket.music.main.video2;

import com.aveplus.avepay_cpocket.music.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataVideo extends BaseModel {
    public static String CATEGORY_VIDEO = "Video Categories";
    public static String PLAYLIST_VIDEO = "PLAYLIST_VIDEO";
    public static String VIDEO = "Videos";
    @SerializedName("data")
    @Expose
    private Video data;



    public Video getData() {
        return data;
    }

    public void setData(Video data) {
        this.data = data;
    }

}
