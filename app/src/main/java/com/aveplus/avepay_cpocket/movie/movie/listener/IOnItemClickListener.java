package com.aveplus.avepay_cpocket.movie.movie.listener;

/**
 * Copyright © 2019 SUUSOFT
 */

public interface IOnItemClickListener {
    void onItemClickMenu(int position);
}
