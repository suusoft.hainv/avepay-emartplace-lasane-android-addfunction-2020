package com.aveplus.avepay_cpocket.ebook.view.adapter;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.view.SingleAdapter;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.model.HomeCategory;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.DetailsBookActivity;
import com.aveplus.avepay_cpocket.ebook.view.activity.ListBookViewMoreActivity;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemBookVM;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemCategoryVM;
import com.aveplus.avepay_cpocket.movie.movie.util.DateTimeUtility;
import com.google.gson.Gson;


import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by pham on 12/09/2017.
 */

public class ItemHomeAdapter extends RecyclerView.Adapter<ItemHomeAdapter.BaseViewHolder> {
    public static final int ITEM_NORMAL = 1;
    public static final int ITEM_FEATURE = 2;
    private ArrayList<HomeCategory> listData;
    private Context context;
    private int posSongItem = -1;

    public ItemHomeAdapter(Context context, ArrayList<HomeCategory> mListCategory) {
        this.listData = mListCategory;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position != 0)
            return ITEM_NORMAL;
        return ITEM_FEATURE;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_NORMAL) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_row, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_row_feature, parent, false);
            return new ViewHolderHeader(view);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            HomeCategory item = listData.get(position);
            if (item != null) {
                viewHolder.tvName.setText(item.name);
                if (HomeCategory.CATEGORY.equals(item.type)) {
                    viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_category_type_1, item.getListCategory(), ItemCategoryVM.class));
                    viewHolder.tvMore.setVisibility(View.GONE);
                    setOnClickViewMore(viewHolder.tvMore, item.name, position);

                } else {
                    viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_book_grid_home, item.getListBooks(), ItemBookVM.class));
                    setOnClickViewMore(viewHolder.tvMore, item.name, position);

                }
            }
        } else {
            HomeCategory item = listData.get(position);
            if (item != null) {
                ViewHolderHeader viewHolder = (ViewHolderHeader) holder;
                viewHolder.setAdapter(item.getListBooks());
            }
        }
    }

    private void setOnClickViewMore(TextView tvMore, final String object, final int type) {
        tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt(ListBookViewMoreActivity.KEY_TYPE, type);
                bundle.putString(ListBookViewMoreActivity.KEY_NAME, object);
                AppUtil.startActivity(context, ListBookViewMoreActivity.class, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class BaseViewHolder extends RecyclerView.ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvName, tvMore;
        RecyclerView rcvData;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMore = (TextView) itemView.findViewById(R.id.tv_more);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            rcvData = (RecyclerView) itemView.findViewById(R.id.rcv_data);
            rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        }
    }

    public class ViewHolderHeader extends BaseViewHolder {
        ViewPager viewPager;
        CircleIndicator circleIndicator;
        HeaderItemAdapter adapter;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            viewPager = (ViewPager) itemView.findViewById(R.id.viewPagger);
            circleIndicator = (CircleIndicator) itemView.findViewById(R.id.circleIndicator);
        }

        public void setAdapter(ArrayList<Book> listData) {
            adapter = new HeaderItemAdapter(listData);
            viewPager.setAdapter(adapter);
            circleIndicator.setViewPager(viewPager);
            adapter.registerDataSetObserver(circleIndicator.getDataSetObserver());
            adapter.setAutoSlide();
        }

        public class HeaderItemAdapter extends PagerAdapter {
            private static final long DELAY = 5000;
            private ArrayList<Book> mListData;
            private Handler handler = new Handler();
            public ImageView imageView;
            private Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    int item = viewPager.getCurrentItem();
                    int totalItem = mListData.size() - 1;
                    if (item < totalItem) {
                        item++;
                        viewPager.setCurrentItem(item);
                    } else if (item == totalItem) {
                        viewPager.setCurrentItem(0);
                    }
                    handler.postDelayed(this, DELAY);
                }
            };

            public HeaderItemAdapter(ArrayList<Book> mListData) {
                this.mListData = mListData;
            }

            @Override
            public int getCount() {
                return mListData.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, final int position) {
                final Book book = mListData.get(position);

                View view = LayoutInflater.from(context).inflate(R.layout.item_pager_header, container, false);
                imageView = (ImageView) view.findViewById(R.id.imageView);
                AppUtil.setImage(imageView, book.getImage_banner());
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Bundle bundle = new Bundle();
                        bundle.putString(Constant.KEY_BOOK, new Gson().toJson(book));
                        bundle.putBoolean(Constant.KEY_OPEN_LAST_CHAPTER, true);
                        AppUtil.startActivityClearTop(context, DetailsBookActivity.class, bundle);
                        book.setDateHistory(DateTimeUtility.getCurrentTimeStamp());
                        DataStoreManager.addBook(book, Constant.LIST_BOOK_HISTORY);
                    }
                });
                container.addView(view);

                return view;

            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                handler.removeCallbacks(runnable);
                container.removeView((RelativeLayout) object);
            }

            public void setAutoSlide() {
                handler.postDelayed(runnable, DELAY);

            }

        }
    }
}
