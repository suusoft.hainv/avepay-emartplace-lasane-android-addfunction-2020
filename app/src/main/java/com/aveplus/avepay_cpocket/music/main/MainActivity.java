package com.aveplus.avepay_cpocket.music.main;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.SingleFragment;
import com.aveplus.avepay_cpocket.music.base.view.SingleListFragment;
import com.aveplus.avepay_cpocket.music.base.view.ToolbarNavActivity;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.listener.IOnFragmentNavListener;
import com.aveplus.avepay_cpocket.music.listener.IOnMenuItemClick;
import com.aveplus.avepay_cpocket.music.main.album.AllAlbumByFragment;
import com.aveplus.avepay_cpocket.music.main.category.AllCategoryFragment;
import com.aveplus.avepay_cpocket.music.main.category.MusicByCategoryFragment;
import com.aveplus.avepay_cpocket.music.main.detail.AlbumFragment;
import com.aveplus.avepay_cpocket.music.main.downloaded.DownloadedFileFragment;
import com.aveplus.avepay_cpocket.music.main.favorite.FavoriteSongFragment;
import com.aveplus.avepay_cpocket.music.main.home.MainFragment;
import com.aveplus.avepay_cpocket.music.main.localmusic.LocalMusicFragment;
import com.aveplus.avepay_cpocket.music.main.login.SplashLoginActivity;
import com.aveplus.avepay_cpocket.music.main.player.ItemSongQueueVM;
import com.aveplus.avepay_cpocket.music.main.player.NowPlayingFragment;
import com.aveplus.avepay_cpocket.music.main.player.QueueSongVM;
import com.aveplus.avepay_cpocket.music.main.profile.MyAccountMyInfoFragment;
import com.aveplus.avepay_cpocket.music.main.search.SearchFragment;
import com.aveplus.avepay_cpocket.music.main.settings.SettingsVM;
import com.aveplus.avepay_cpocket.music.model.Category;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.aveplus.avepay_cpocket.music.player.MusicService;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.util.AdsUtil;
import com.aveplus.avepay_cpocket.music.util.BindingUtil;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.music.widgets.slidinguplo.SlidingUpPanelLayout;
import com.aveplus.avepay_cpocket.network1.MyProgressDialog;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.navigation.NavigationView;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends ToolbarNavActivity implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener {

    private Fragment mCurrenFragment;
    private Menu mMenu;

    private FrameLayout contentSecond, contentSlideUp;

    /**
     * Position of fragment selected. set when item of left menu clicked
     */
    private FragmentPosition mCurFragSeleted = FragmentPosition.FRAGMENT_HOME;
    private LinearLayout llPlayerBottom;
    protected Animation animSlideIn, animSlideOut, animSlideUp, animSlideDown;

    private String tagFragment2Current = "", tagFragment3Current = "";
    private int countClicked;
    private boolean isWaitingShownAdmob;
    private AdView adsView;
    public static final String LOG_OUT = "LOG_OUT";

    /**
     * List of fragment by position
     */
    private enum FragmentPosition {
        FRAGMENT_HOME,
        FRAGMENT_FAVORITE,
        FRAGMENT_HISTORY,
        FRAGMENT_CATEGORY,
        FRAGMENT_PROFILE,
        FRAGMENT_SETTINGS,
        FRAGMENT_ABOUTUS,
        FRAGMENT_MYLIST,
        FRAGMENT_FEEDBACK
    }


    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_main_music;
    }

    @Override
    protected void initView() {
        RequestManager.registerDevice(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                Log.e("Register device", response.getMessage());
            }

            @Override
            public void onError(String message) {
                Log.e("Register device", message);

            }
        });
        MobileAds.initialize(this, getString(R.string.admob_app_unit_id));
        adsView = findViewById(R.id.adView);
        adsView.setVisibility(View.GONE);
        AdsUtil.loadBanner(adsView);
        initViewPlayer();

    }

    @Override
    protected void onViewCreated() {

        switchScreen();
        initPagerPlayer();
        initAnimation();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (slidingUpPanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            showNowPlayingScreen(false);
        } else {
            checkAndBackScreen();
        }

    }

    private void showDialogRate() {
        DialogUtil.showAlertDialog(self, R.string.msg_rating_app, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                AppUtil.goToAppOnStore(self);
                finish();
            }

            @Override
            public void onClickCancel() {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu;
        mMenu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        if (mCurFragSeleted == FragmentPosition.FRAGMENT_HOME) {

            MenuItem searchItem = menu.findItem(R.id.action_seach);
            searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    SearchFragment fragment = SearchFragment.newInstance();
                    fragment.setListener(new IOnFragmentNavListener() {
                        @Override
                        public void onFragmentBack() {
                            removeScreen2(TAG_FRAG_SEARCH);
                        }
                    });
                    switchFragment2(fragment, TAG_FRAG_SEARCH);
                    return false;
                }
            });


        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }


    private void initAnimation() {
        animSlideIn = AnimationUtils.loadAnimation(self, R.anim.slide_in_left);
        animSlideOut = AnimationUtils.loadAnimation(self, R.anim.slide_out_left);
        animSlideUp = AnimationUtils.loadAnimation(self, R.anim.slide_up);
        animSlideDown = AnimationUtils.loadAnimation(self, R.anim.slide_down);
    }


    /**
     * Menu left onClick and switch fragment
     */
    @Override
    public void switchScreen() {
        String tag = "frag_" + mCurFragSeleted;
        mCurrenFragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (mCurrenFragment == null) {
            if (mCurFragSeleted == FragmentPosition.FRAGMENT_HOME) {
                mCurrenFragment = MainFragment.newInstance();
            } else if (mCurFragSeleted == FragmentPosition.FRAGMENT_SETTINGS) {
                mCurrenFragment = SingleFragment.newInstance(R.layout.fragment_settings, SettingsVM.class);
            }
        }
        switchFragment(tag, mCurrenFragment);
    }

    /**
     * Set menu for screen
     */
    @Override
    public void switchMenu() {
        if (mMenu != null) {
            mMenu.clear();
            if (mCurFragSeleted == FragmentPosition.FRAGMENT_HOME) {
                getMenuInflater().inflate(R.menu.main, mMenu);
            } else if (mCurFragSeleted == FragmentPosition.FRAGMENT_HISTORY) {
                getMenuInflater().inflate(R.menu.menu_top_video_history, mMenu);
            } else if (mCurFragSeleted == FragmentPosition.FRAGMENT_MYLIST) {
                getMenuInflater().inflate(R.menu.menu_action_mylist, mMenu);
            } else if (mCurFragSeleted == FragmentPosition.FRAGMENT_FAVORITE) {
                getMenuInflater().inflate(R.menu.menu_top_video_history, mMenu);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_policy) {
            rateApp();
        } else if (id == R.id.nav_aboutus) {
            AppUtil.goToWeb(self, getString(R.string.about_web));
        } else if (id == R.id.nav_term) {
            AppUtil.goToWeb(self, getString(R.string.about_more_app));
        } else if (id == R.id.nav_logout) {
            if (DataStoreManager.getUser() != null) {
                logout();
            } else {
                AppUtil.showToast(self, "No Account");
            }
        } else if (id == R.id.nav_profile) {
            MyAccountMyInfoFragment fragment = MyAccountMyInfoFragment.newInstance();
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen3(TAG_PROFILE);
                    showHideContentSlideUp(false);
                }
            });
            switchFragment3(fragment, TAG_PROFILE);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void rateApp() {
        AppUtil.goToAppOnStore(this);
    }


    @Override
    public void onClick(View view) {
        if (view == btnBackPlayer) {
            showNowPlayingScreen(false);
        } else if (view == btnRepeat) {
            actionRepeat();
        } else if (view == btnShuffer) {
            actionShuffer();
        } else if (view == btnActionPlayer) {
            showMenuActionPlayer();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_UP: {
                countClicked++;
                if (countClicked >= Constant.NUMBER_OF_CLICKED) {
                    if (!isWaitingShownAdmob) {
                        //loadAdsInterstitial();
                    }
                }
                break;
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private void showMenuActionPlayer() {
        Global.showPopupMenu(self, btnActionPlayer, R.menu.menu_action_player, new IOnMenuItemClick() {
            @Override
            public void onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_volumn:
                        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_SAME, AudioManager.FLAG_SHOW_UI);
                        break;

                }
            }
        });
    }

    private void actionRepeat() {
        QueueManager queueManager = QueueManager.getInstance();
        if (queueManager.isRepeat) {
            queueManager.setRepeat(false);
            btnRepeat.setImageResource(R.drawable.ic_player_repeat_all);
            AppUtil.showToast(this, R.string.no_repeat);
        } else {
            queueManager.setRepeat(true);
            btnRepeat.setImageResource(R.drawable.ic_player_repeat_all_pressed);
            AppUtil.showToast(this, R.string.repeat);
        }
    }

    private void actionShuffer() {
        QueueManager queueManager = QueueManager.getInstance();
        if (queueManager.isShuffle) {
            queueManager.setShuffle(false);
            btnShuffer.setImageResource(R.drawable.ic_player_shuffle);
            AppUtil.showToast(this, R.string.no_shuffe);
        } else {
            queueManager.setShuffle(true);
            btnShuffer.setImageResource(R.drawable.ic_player_shuffle_selected);
            AppUtil.showToast(this, R.string.shuffer);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.HomeSongItemClicked event) {
        MediaControllerCompat.getMediaController(MainActivity.this).getTransportControls().playFromMediaId("t", null);
        showNowPlayingScreen(true);
    }

    private static final String TAG_LOCAL_MUSIC = "TAG_LOCAL_MUSIC";
    private static final String TAG_DOWNLOADED_FILE = "TAG_DOWNLOADED_FILE";
    private static final String TAG_FAVORITE = "TAG_FAVORITE";
    private static final String TAG_PROFILE = "TAG_PROFILE";
    private static final String TAG_AlBUM = "TAG_AlBUM";
    private static final String TAG_MUSIC_BY_CATEGORY = "TAG_MUSIC_BY_CATEGORY";
    private static final String TAG_MUSIC_ALL_CATEGORY = "TAG_MUSIC_ALL_CATEGORY";
    private static final String TAG_MUSIC_ALL_ALBUM = "TAG_MUSIC_ALL_ALBUM";
    private static final String TAG_FRAG_SEARCH = "TAG_FRAG_SEARCH";


    // receive from MyMusicVM class
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.MyMusicItemClicked event) {

        if (event.type == Global.LocalItemTypeClick.TYPE_DOWNLOADED) {
            DownloadedFileFragment fragment = DownloadedFileFragment.newInstance();
//            fragment.setListener (new IOnFragmentNavListener () {
//                @Override
//                public void onFragmentBack() {
//                    removeScreen3 (TAG_DOWNLOADED_FILE);
//                    showHideContentSlideUp (false);
//                }
//            });
            switchFragment3(fragment, TAG_DOWNLOADED_FILE);
        } else if (event.type == Global.LocalItemTypeClick.TYPE_SONG) {
            LocalMusicFragment fragment = LocalMusicFragment.newInstance(0);
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen3(TAG_LOCAL_MUSIC);
                    showHideContentSlideUp(false);
                }
            });
            switchFragment3(fragment, TAG_LOCAL_MUSIC);
        } else if (event.type == Global.LocalItemTypeClick.TYPE_ARITST) {
            LocalMusicFragment fragment = LocalMusicFragment.newInstance(1);
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen3(TAG_LOCAL_MUSIC);
                    showHideContentSlideUp(false);
                }
            });
            switchFragment3(fragment, TAG_LOCAL_MUSIC);
        } else if (event.type == Global.LocalItemTypeClick.TYPE_FAVORITE) {
            FavoriteSongFragment fragment = FavoriteSongFragment.newInstance();
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen3(TAG_FAVORITE);
                    showHideContentSlideUp(false);
                }
            });
            switchFragment3(fragment, TAG_FAVORITE);
        }
    }

    // click on album home
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ItemAlbumClicked event) {
        AlbumFragment fragment = AlbumFragment.newInstance(event.object);
        fragment.setListener(new IOnFragmentNavListener() {
            @Override
            public void onFragmentBack() {
                removeScreen3(TAG_AlBUM);
                showHideContentSlideUp(false);
            }
        });
        switchFragment3(fragment, TAG_AlBUM);
    }

    //
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ItemObjectClicked event) {
        MusicByCategoryFragment fragment = MusicByCategoryFragment.newInstance((Category) event.object);
        fragment.setListener(new IOnFragmentNavListener() {
            @Override
            public void onFragmentBack() {
                removeScreen3(TAG_MUSIC_BY_CATEGORY);
                showHideContentSlideUp(false);
            }
        });
        switchFragment3(fragment, TAG_MUSIC_BY_CATEGORY);
    }

    // event click more button
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ViewMoreClicked event) {
        if (event.position == 1) {
            AllCategoryFragment fragment = AllCategoryFragment.newInstance();
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen2(TAG_MUSIC_ALL_CATEGORY);
                    showHideContentSlideUp(false);
                }
            });
            switchFragment2(fragment, TAG_MUSIC_ALL_CATEGORY);
        } else {
            AllAlbumByFragment fragment = AllAlbumByFragment.newInstance(event.object, event.position);
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen2(TAG_MUSIC_ALL_ALBUM);
                    showHideContentSlideUp(false);
                }
            });
            switchFragment2(fragment, TAG_MUSIC_ALL_ALBUM);
        }

    }

    private void checkAndBackScreen() {
        if (!tagFragment3Current.isEmpty()) {
            removeScreen3(tagFragment3Current);
        } else if (!tagFragment2Current.isEmpty()) {
            removeScreen2(tagFragment2Current);
        } else if (mCurFragSeleted == FragmentPosition.FRAGMENT_HOME) {
            showDialogRate();
        }
    }

    // switch screen from right to left
    private void switchFragment2(Fragment fragment, String tag) {
        showHideContentSecont(true);
        tagFragment2Current = tag;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_second, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();


    }

    // switch screen from bottom to top
    private void switchFragment3(Fragment fragment, String tag) {
        showHideContentSlideUp(true);
        tagFragment3Current = tag;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_slide_up, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    private void removeScreen2(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            tagFragment2Current = "";
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }

    }

    private void removeScreen3(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            tagFragment3Current = "";
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }

    }

    private void showHideContentSecont(boolean isShown) {
        if (isShown) {
            contentSecond.startAnimation(animSlideIn);
            contentSecond.setVisibility(View.VISIBLE);
        } else {
            contentSecond.startAnimation(animSlideOut);
            contentSecond.setVisibility(View.GONE);
        }

    }

    private void showHideContentSlideUp(boolean isShown) {
        if (isShown) {
            contentSlideUp.startAnimation(animSlideIn);
            contentSlideUp.setVisibility(View.VISIBLE);
        } else {
            contentSlideUp.startAnimation(animSlideOut);
            contentSlideUp.setVisibility(View.GONE);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        mMediaBrowser.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        // (see "stay in sync with the MediaSession")
        MediaControllerCompat controllerCompat = MediaControllerCompat.getMediaController(this);
        if (controllerCompat != null) {
            controllerCompat.unregisterCallback(controllerCallback);
        }
        mMediaBrowser.disconnect();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSeekbarUpdate();
        mExecutorService.shutdown();
    }


    //================ player ==========================
    private static final long PROGRESS_UPDATE_INTERNAL = 1000;
    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;
    // player

    private SlidingUpPanelLayout slidingUpPanel;
    private LinearLayout dragView;
    private ImageView btnPlayFooter, pImgBg;
    private ImageView imgSongFooter;
    private TextView tvNameSongFooter, tvNameSingerFooter, tvStartTime, tvEndTime, tvTitlePlayer, tvArtists;
    private ImageButton btnPlay, btnNext, btnPrev, btnShuffer, btnRepeat, btnBackPlayer, btnActionPlayer, btnNextBottom, btnPrevBottom;
    private SeekBar seekBarPlayMusic;
    private ViewPager vpgMusicPlayer;
    private CircleIndicator circleIndicator;

    private final ScheduledExecutorService mExecutorService =
            Executors.newSingleThreadScheduledExecutor();

    private ScheduledFuture<?> mScheduleFuture;
    private final Handler mHandler = new Handler();
    private final Runnable mUpdateProgressTask = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    private MediaBrowserCompat mMediaBrowser;
    private PlaybackStateCompat playbackStateCompat;

    private final MediaBrowserCompat.ConnectionCallback mConnectionCallbacks =
            new MediaBrowserCompat.ConnectionCallback() {
                @Override
                public void onConnected() {
                    connectToSession();
                }

                @Override
                public void onConnectionFailed() {
                    Log.e("MediaBrowserCompat", "onConnectionFailed");
                }

                @Override
                public void onConnectionSuspended() {
                    Log.e("MediaBrowserCompat", "onConnectionSuspended");
                }
            };

    private void connectToSession() {
        MediaSessionCompat.Token token = mMediaBrowser.getSessionToken();
        MediaControllerCompat mediaController = null;
        try {
            mediaController = new MediaControllerCompat(MainActivity.this, // Context
                    token);
            MediaControllerCompat.setMediaController(MainActivity.this, mediaController);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
        buildTransportControls();
    }

    MediaControllerCompat.Callback controllerCallback =
            new MediaControllerCompat.Callback() {
                @Override
                public void onMetadataChanged(MediaMetadataCompat metadata) {
                    if (metadata != null) {
                        updateMediaDescription(metadata.getDescription());
                        //updateMediaSong();
                        updateDuration(metadata);
                    }
                }

                @Override
                public void onPlaybackStateChanged(PlaybackStateCompat state) {
                    updatePlaybackState(state);
                }
            };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ItemChanged event) {
        updateMediaSong();
    }

    private void buildTransportControls() {
        MediaControllerCompat mediaController = MediaControllerCompat.getMediaController(MainActivity.this);
        mediaController.registerCallback(controllerCallback);
        PlaybackStateCompat state = mediaController.getPlaybackState();
        updatePlaybackState(state);
        MediaMetadataCompat metadata = mediaController.getMetadata();
        if (metadata != null) {
//            updateMediaDescription(metadata.getDescription());
            updateDuration(metadata);
        }
        updateProgress();
        if (state != null && (state.getState() == PlaybackStateCompat.STATE_PLAYING ||
                state.getState() == PlaybackStateCompat.STATE_BUFFERING)) {
            scheduleSeekbarUpdate();
        }
    }

    private void showNowPlayingScreen(boolean isShow) {
        if (isShow) {
            slidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            vpgMusicPlayer.setCurrentItem(1);
        } else {
            slidingUpPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        }

    }


    private void updateMediaDescription(MediaDescriptionCompat description) {
        if (description == null) {
            return;
        }
        tvArtists.setText(description.getSubtitle());
        tvTitlePlayer.setText(description.getTitle());
        tvNameSongFooter.setText(description.getTitle());
        tvNameSingerFooter.setText(description.getSubtitle());

        Song song = QueueManager.getInstance().getCurrentItem();
        if (song != null) {
            BindingUtil.setImage(imgSongFooter, song.image);
            if (song.image == null || song.image.isEmpty()) {
                setDefaultBgNowPlaying();
            } else {
                BindingUtil.setImageResizeQuare(pImgBg, song.image);
            }
        }
    }

    private void updateMediaSong() {
        Song song = QueueManager.getInstance().getCurrentItem();
        if (song != null) {
            tvArtists.setText(song.getMusicArtists().toString());
            tvTitlePlayer.setText(song.getTitle());
            tvNameSongFooter.setText(song.getTitle());
            tvNameSingerFooter.setText(song.nameSinger);
            BindingUtil.setImage(imgSongFooter, song.image);
            if (song.image == null || song.image.isEmpty()) {
                setDefaultBgNowPlaying();
            } else {
                BindingUtil.setImageResizeQuare(pImgBg, song.image);
            }
        }
    }

    private void setDefaultBgNowPlaying() {
        pImgBg.setImageBitmap(ImageUtil.blur(this, BitmapFactory.decodeResource(getResources(), R.drawable.bg_player_default)));
    }

    private void updateDuration(MediaMetadataCompat metadata) {
        if (metadata == null) {
            return;
        }
        int duration = (int) metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
        seekBarPlayMusic.setMax(duration);
        tvEndTime.setText(DateUtils.formatElapsedTime(duration / 1000));
    }

    private void initViewPlayer() {
        imgSongFooter = findViewById(R.id.img_song_footer);
        tvNameSongFooter = findViewById(R.id.tv_songname_footer);
        tvNameSingerFooter = findViewById(R.id.tv_artist_name_footer);
        btnPlayFooter = findViewById(R.id.btn_player_footer);

        // content player
        dragView = findViewById(R.id.dragView);
        tvTitlePlayer = findViewById(R.id.c_tv_title);
        tvArtists = findViewById(R.id.c_tv_artists);
        tvArtists.setVisibility(View.VISIBLE);
        vpgMusicPlayer = findViewById(R.id.viewPager);
        seekBarPlayMusic = findViewById(R.id.srb_progress);
        tvStartTime = findViewById(R.id.txt_start_time);
        tvEndTime = findViewById(R.id.txt_total_time);
        btnShuffer = findViewById(R.id.btn_shuffer);
        btnPlay = findViewById(R.id.btn_play);
        btnRepeat = findViewById(R.id.btn_repeat);
        btnNext = findViewById(R.id.btn_next);
        btnPrevBottom = findViewById(R.id.btn_prev_bottom);
        btnNextBottom = findViewById(R.id.btn_next_bottom);
        btnPrev = findViewById(R.id.btn_prev);
        btnBackPlayer = findViewById(R.id.c_btn_back);
        btnActionPlayer = findViewById(R.id.c_btn_action);
        circleIndicator = findViewById(R.id.circleIndicator);
        pImgBg = findViewById(R.id.player_img_bg);
        llPlayerBottom = findViewById(R.id.llPlayerBottom);
        slidingUpPanel = findViewById(R.id.sliding_up_pannel);
        contentSecond = findViewById(R.id.content_second);
        contentSlideUp = findViewById(R.id.content_slide_up);

        btnPlayFooter.setOnClickListener(this);
        btnShuffer.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnRepeat.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnBackPlayer.setOnClickListener(this);
        btnActionPlayer.setOnClickListener(this);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(MainActivity.this).getTransportControls();
                controls.skipToNext();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(MainActivity.this).getTransportControls();
                controls.skipToPrevious();
            }
        });
        btnNextBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(MainActivity.this).getTransportControls();
                controls.skipToNext();
            }
        });

        btnPrevBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(MainActivity.this).getTransportControls();
                controls.skipToPrevious();
            }
        });
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionPlayPause();
            }
        });

        btnPlayFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionPlayPause();
            }
        });

        seekBarPlayMusic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvStartTime.setText(DateUtils.formatElapsedTime(progress / 1000));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                stopSeekbarUpdate();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MediaControllerCompat.getMediaController(MainActivity.this).getTransportControls().seekTo(seekBar.getProgress());
                scheduleSeekbarUpdate();
            }
        });

        mMediaBrowser = new MediaBrowserCompat(this,
                new ComponentName(this, MusicService.class),
                mConnectionCallbacks, null); // optional Bundle

    }

    private void actionPlayPause() {
        PlaybackStateCompat state = MediaControllerCompat.getMediaController(MainActivity.this).getPlaybackState();
        if (state != null) {
            MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(MainActivity.this).getTransportControls();
            switch (state.getState()) {
                case PlaybackStateCompat.STATE_PLAYING: // fall through
                case PlaybackStateCompat.STATE_BUFFERING:
                    controls.pause();

                    stopSeekbarUpdate();
                    break;
                case PlaybackStateCompat.STATE_PAUSED:
                case PlaybackStateCompat.STATE_STOPPED:
                    controls.play();
                    scheduleSeekbarUpdate();
                    break;
                default:
                    Log.d("onClick with state ", " " + state.getState());
            }
        }
    }

    private void updateUIState(boolean isPlaying) {
        if (isPlaying) {
            btnPlay.setImageResource(R.drawable.ic_pause_white_24dp);
            btnPlayFooter.setImageResource(R.drawable.ic_pause_white_24dp);
        } else {
            btnPlay.setImageResource(R.drawable.ic_play_white);
            btnPlayFooter.setImageResource(R.drawable.ic_play_white);
        }
    }


    private void scheduleSeekbarUpdate() {
        stopSeekbarUpdate();
        if (!mExecutorService.isShutdown()) {
            mScheduleFuture = mExecutorService.scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            mHandler.post(mUpdateProgressTask);
                        }
                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
        }
    }

    private void stopSeekbarUpdate() {
        if (mScheduleFuture != null) {
            mScheduleFuture.cancel(false);
        }
    }

    private void updateProgress() {
        if (playbackStateCompat == null) {
            return;
        }
        long currentPosition = playbackStateCompat.getPosition();
        if (playbackStateCompat.getState() == PlaybackStateCompat.STATE_PLAYING) {
            long timeDelta = SystemClock.elapsedRealtime() -
                    playbackStateCompat.getLastPositionUpdateTime();
            currentPosition += (int) timeDelta * playbackStateCompat.getPlaybackSpeed();
        }
        seekBarPlayMusic.setProgress((int) currentPosition);
    }

    private void updatePlaybackState(PlaybackStateCompat state) {
        if (state == null) {
            return;
        }
        playbackStateCompat = state;
        MediaControllerCompat controllerCompat = MediaControllerCompat.getMediaController(MainActivity.this);
        if (controllerCompat != null && controllerCompat.getExtras() != null) {
        }

        switch (state.getState()) {
            case PlaybackStateCompat.STATE_PLAYING:
                updateUIState(true);
                scheduleSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_PAUSED:
                updateUIState(false);
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_NONE:
            case PlaybackStateCompat.STATE_STOPPED:
                updateUIState(false);
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_BUFFERING:
                stopSeekbarUpdate();
                break;
            default:
                Log.d("updatePlaybackState", "Unhandled state " + state.getState());
        }

    }

    private void initPagerPlayer() {
        tvArtists.setText("");
        tvTitlePlayer.setText("");
        setDefaultBgNowPlaying();
        vpgMusicPlayer.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        circleIndicator.setViewPager(vpgMusicPlayer);
        vpgMusicPlayer.setCurrentItem(1);
        slidingUpPanel.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    llPlayerBottom.setVisibility(View.GONE);
                    startAnimCircleImagePlaying(true);
                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    llPlayerBottom.setVisibility(View.VISIBLE);
                    startAnimCircleImagePlaying(false);
                } else if (newState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                    llPlayerBottom.setVisibility(View.GONE);
                }
            }
        });
    }

    public void startAnimCircleImagePlaying(boolean isStart) {
        if (fragmentNowPlaying != null) {
            if (isStart) fragmentNowPlaying.startAnimation();
            else fragmentNowPlaying.stopAnimation();
        }
    }

    NowPlayingFragment fragmentNowPlaying;

    class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                SingleListFragment fragment = SingleListFragment.newInstance(QueueSongVM.class, R.layout.layout_list_playing, R.layout.item_song_queue, ItemSongQueueVM.class);
                //fragment.setBaseListener(this);
                return fragment;
            }
            if (position == 1) {
                fragmentNowPlaying = NowPlayingFragment.newInstance();
                return fragmentNowPlaying;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "";
            } else {
                return "";
            }
        }

        @Override
        public int getCount() {
            return 2;
        }


    }

    private void logout() {
        showDialogLogout();


    }

    private void showDialogLogout() {
        DialogUtil.showAlertDialog(self, R.string.log_out, R.string.you_wanto_logout, new DialogUtil.IDialogConfirm2() {
            @Override
            public void onClickOk() {
                requestLogout();
            }
        });
    }

    private void requestLogout() {
        DataStoreManager.removeUser();
        //  AppController.getInstance().setUserUpdated(true);

        AppUtil.startActivity(self, SplashLoginActivity.class);
        finish();

    }

    private void processBeforeLoggingOut(MyProgressDialog progressDialog) {
        if (DataStoreManager.getUser() != null) {

        }

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        DataStoreManager.removeUser();

        AppController.getInstance().setUserUpdated(true);

        AppUtil.startActivity(self, SplashLoginActivity.class);
        finish();


    }

}
