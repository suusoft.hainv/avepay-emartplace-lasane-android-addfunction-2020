package com.aveplus.avepay_cpocket.music.main.mymusic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.main.login.SplashLoginActivity;
import com.aveplus.avepay_cpocket.music.main.main2.MainActivity2;
import com.aveplus.avepay_cpocket.music.main.payment.BottomPaymentFragment;
import com.aveplus.avepay_cpocket.music.main.payment.IPayement;
import com.aveplus.avepay_cpocket.music.main.payment.ListSubscribeActivity;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class MyMusicVM extends BaseViewModel {
    private boolean push;

    public boolean isPush() {
        if (!DataStoreManager.getCheckPush()) {
            return false;
        }
        return DataStoreManager.getCheckPush();
    }

    public void setPush(boolean push) {
        if (push) {
            DataStoreManager.saveCheckPush(true);
        } else {
            DataStoreManager.saveCheckPush(false);
        }

        this.push = push;
    }

    public MyMusicVM(Context self) {
        super(self);
    }

    public void onClickAlbum(View view) {
        EventBus.getDefault().post(new Global.MyMusicItemClicked(Global.LocalItemTypeClick.TYPE_ARITST));

    }

    public void onClickSong(View view) {
        EventBus.getDefault().post(new Global.MyMusicItemClicked(Global.LocalItemTypeClick.TYPE_SONG));

    }

    public void onClickFavorite(View view) {
        EventBus.getDefault().post(new Global.MyMusicItemClicked(Global.LocalItemTypeClick.TYPE_FAVORITE));

    }

    public void onClickLogout(View view) {
        showDialogLogout();
    }


    private void showDialogLogout() {
        DialogUtil.showAlertDialog(self, R.string.log_out, R.string.you_wanto_logout, new DialogUtil.IDialogConfirm2() {
            @Override
            public void onClickOk() {
                requestLogout();
            }
        });
    }

    private void requestLogout() {
        DataStoreManager.removeUser();


        AppUtil.startActivity(self, MainActivity2.class);
        ((Activity) self).finish();

    }

    // send to MainActivity
    public void onClickDownload(View view) {
        EventBus.getDefault().post(new Global.MyMusicItemClicked(Global.LocalItemTypeClick.TYPE_DOWNLOADED));
    }
    public void onClickLogin(View view){
        AppUtil.startActivity(self, SplashLoginActivity.class);//MainAct
    }
    public int isUser(){
        notifyChange();
        if (DataStoreManager.getUser()==null) {
            return View.GONE;
        }
        return View.VISIBLE;
    }
    public int isUserLogin(){
        notifyChange();
        if (DataStoreManager.getUser()==null) {
            return View.VISIBLE;
        }
        return View.GONE;
    }

    public String getName() {
        notifyChange();
        if (DataStoreManager.getUser() == null) {
            return "No account";
        }
        return DataStoreManager.getUser().getName();
    }


    @SuppressLint("ResourceType")
    public String getImage() {
        notifyChange();
        if (DataStoreManager.getUser() == null) {
            return "http://suusoft-demo.com/products/music-v2/backend/web/index.php/api/file?f=1581499409756_thumb_image.jpeg";
        }
        return DataStoreManager.getUser().getAvatar();
    }

    public String getEmail() {
        notifyChange();
        if (DataStoreManager.getUser() == null) {
            return "";
        }
        return DataStoreManager.getUser().getEmail();
    }

    public String getPhone() {
        notifyChange();
        if (DataStoreManager.getUser().getPhone() == null) {
            return "Phone number has not been updated";
        }
        return DataStoreManager.getUser().getPhone();
    }

    public String getSubcribe() {
        AppUtil.checkPayment(DataStoreManager.getUser().getToken());
        if (DataStoreManager.getCheckPayment()) {
            return "VIP";
        }
        return "FREE";
    }

    public int isVisible() {
        notifyChange();
        AppUtil.checkPayment(DataStoreManager.getUser().getToken());
        if (DataStoreManager.getCheckPayment()) {
            return View.GONE;
        }
        return View.VISIBLE;
    }

    public void onClickBuy(View view) {
        BottomPaymentFragment bottomPaymentFragment = new BottomPaymentFragment(new IPayement() {
            @Override
            public void onPayment(String payment) {
                Intent intent = new Intent(self, ListSubscribeActivity.class);

                self.startActivity(intent);
//                            if (payment.equals("Esewa")) {
//                                // ivPayment.setImageDrawable(getResources().getDrawable(R.drawable.payment_cash));
//                            }
//                            else {
//                                // ivPayment.setImageDrawable(getResources().getDrawable(R.drawable.payment_credits));
//                            }
            }
        });
        bottomPaymentFragment.show(((MainActivity2) self).getSupportFragmentManager(), bottomPaymentFragment.getTag());
    }
    public void onClickAbout(View view){
        AppUtil.goToWeb(self, AppController.getInstance().getString(R.string.about_web));
    }
    public void onClickPolicy(View view){
        AppUtil.goToWeb(self,AppController.getInstance().getString(R.string.url_policy));
    }
    public void onClickTerm(View view){
        AppUtil.goToWeb(self, AppController.getInstance().getString(R.string.url_term));
    }
}
