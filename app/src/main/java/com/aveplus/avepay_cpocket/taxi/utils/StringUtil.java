package com.aveplus.avepay_cpocket.taxi.utils;

import android.content.Context;
import android.util.Log;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by SuuSoft.com on 12/06/2016.
 */

public class StringUtil {

    private static final String TAG = StringUtil.class.getSimpleName();

    public static String convertNumberToString(float number, int numberAfterDecimal) {
        return String.format(Locale.US, "%,.0" + numberAfterDecimal + "f", number);
    }

    public static String convertNumberToString(float number) {
        return String.format(Locale.US, "%.0f", number);
    }

    public static String convertNumberToString(double number, int numberAfterDecimal) {
        return String.format(Locale.US, "%,.0" + numberAfterDecimal + "f", number);
    }

    public static String convertNumberToString(Context context, double number, int numberAfterDecimal) {
        if (number < 0) {
            number = 0 - number;
            return "-" +  String.format(Locale.US, "%,.0" + numberAfterDecimal + "f", number);
        } else
            return String.format(Locale.US, "%,.0" + numberAfterDecimal + "f", number);
    }

    public static float convertStringToNumber(String strNumber) {
        NumberFormat format = NumberFormat.getInstance(Locale.US);

        if (strNumber.contains(",")) {
            strNumber = strNumber.replace(",", "");
        }

        try {
            Number number = format.parse(strNumber);
            return number.floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static double convertStringToNumberDouble(String strNumber) {
        NumberFormat format = NumberFormat.getInstance(Locale.US);
        if (strNumber.isEmpty()) {
            return 0;
        }

        if (strNumber.contains(",")) {
            strNumber = strNumber.replace(",", "");
        }

        try {
            Number number = format.parse(strNumber);
            return number.doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    /**
     * Check format of email
     *
     * @param target is a email need checking
     */
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Check field not empty and 6 word
     *
     * @param input text input
     */
    public static boolean isGoodField(String input) {
        if (input == null || input.isEmpty() || input.length() < 6)
            return false;
        return true;
    }

    /**
     * Check field not empty
     *
     * @param input text input
     */
    public static boolean isEmpty(String input) {
        if (input == null || input.isEmpty())
            return true;
        return false;
    }

    /**
     * get action of url request
     *
     * @param url
     */
    public static String getAction(String url) {
        return url.substring(url.lastIndexOf("/") + 1, url.length());
    }

    /**
     * Password is minimum 8 characters and 1 Number and not contain special character
     * exp: trang123
     */
    public static boolean isValidatePassword(String password) {
        Pattern p = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(password);
        return m.find();
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 6 || phone.length() > 13) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }

    public static String convertNumberToPrice(double price){
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(price);
        String str1 = String.format(AppController.getInstance().getString(R.string.dollar_value), String.valueOf(yourFormattedString))
                .replace(",", " ");
        Log.e(TAG, "convertNumberToPrice " + str1);
        return str1;
    }

    public static String convertNumberToPrice(int price){
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(price);
        String str1 = String.format(AppController.getInstance().getString(R.string.dollar_value), String.valueOf(yourFormattedString))
                .replace(",", " ");
        Log.e(TAG, "convertNumberToPrice " + str1);
        return str1;
    }

    public static String convertNumberToPrice(String price){
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format( Double.parseDouble(price) );
        String str1 = String.format(AppController.getInstance().getString(R.string.dollar_value), String.valueOf(yourFormattedString))
                .replace(",", " ");
        Log.e(TAG, "convertNumberToPrice " + str1);
        return str1;
    }

    public static String convertNumberPrice(String price){
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format( Double.parseDouble(price) );
        return yourFormattedString.replace(",", " ");
    }

    public static String convertNumberPrice(double price){
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(price);
        return yourFormattedString.replace(",", " ");
    }

    public static String convertNumberPrice(int price){
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(price);
        return yourFormattedString.replace(",", " ");
    }

    public static String converTimeToString(int totalSecs){
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;

        if (hours>0){
            return String.format("%02d hours %02d mins", hours, minutes/*, seconds*/);
        }else {
            return String.format("%02d mins %02d seconds", minutes, seconds);
        }
//        int min = 60;
//        int hour = 3600;
//        int day = 86400;
//
//        if (totalSecs % day > 0){
//            //ngày
//
//        }else {
//            if (totalSecs % hour > 0){
//
//
//            }else {
//                if (totalSecs % min > 0){
//
//
//                }else {
//
//
//
//                }
//
//
//            }
//
//        }

    }

    public static String bindCountDown(long millisUntilFinished){

        String string = ""+String.format(AppController.getInstance().getString(R.string.bind_minutes_count_down),
                TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

        return string;
    }
}
