package com.aveplus.avepay_cpocket.movie.movie.retrofit.response;

import android.app.Activity;

import android.app.Dialog;
import android.content.Context;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.movie.movie.util.AppUtil;
import com.aveplus.avepay_cpocket.movie.movie.util.DialogUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Suusoft 2020.
 */

public class ApiResponse {


    public static final String KEY_NAME_ERROR = "error";
    public static final String KEY_NAME_CODE = "code";
    public static final String KEY_NAME_MESSAGE = "message";
    public static final String KEY_NAME_STATUS = "status";
    public static final String KEY_NAME_SUCCESS = "SUCCESS";
    public static final String KEY_NAME_DATA = "data";

    public static final int ERROR_CODE_NOERROR = 0;
    public static final int ERROR_CODE_UNKOWN = -9000;
    public static final int ERROR_CODE_JSON_ERROR = -9001;
    public static final int ERROR_CODE_INVALID_RESPONSE_FORMAT = -9002;
    public static final int ERROR_CODE_REQUEST_TIMEOUT = -9003;
    public static final int ERROR_CODE_NOT_ENOUGH_MONEY = 412;

    private boolean isError;

    public JsonObject data;
    public String status;
    public int code;
    public String total_page ;
    public String message;
    public String login_token;

    private static Dialog dialog;



    public ApiResponse(JsonObject json) {
        if (json == null) {
            this.isError = true;
            this.message = "Empty json";
            this.code = ERROR_CODE_JSON_ERROR;
        } else {
            this.code = json.get("code").getAsInt();
            this.message =  json.get("message").getAsString();

            if (json.get(KEY_NAME_STATUS).getAsString().equalsIgnoreCase(KEY_NAME_SUCCESS)) {
                this.data = json;
            } else {
                this.isError = true;
            }


        }
    }



    /**
     *  Method get object from data if data responde is a object
     *  @param tClass
     *  @return Object
     *
     */

    public <T> T getDataObject(Class<T> tClass) {
        try {
            JsonObject object = getDataObject().getAsJsonObject();
            T obj = new GsonBuilder().create().fromJson(object.toString(),tClass);
            return obj;
        }catch (Exception e){
            return null;
        }

    }

    /**
     *  This get list object from data if responde is array
     *  @param tClass
     *  @return List<Object>
     *
     */
    public <T> List<T> getDataList(Class<T> tClass) {
        List<T> listObj = new ArrayList<>();
        JsonArray arr = getDataArray();
        if (data != null) {
            JsonObject jo;
            T object;
            int size = arr.size();
            Gson gson = new GsonBuilder().create();
            for (int i = 0; i < size; i++) {
                jo = arr.get(i).getAsJsonObject();
                if (jo != null) {
                    object = gson.fromJson(jo.toString(),tClass);
                    listObj.add(object);
                }

            }
        }
        return listObj;
    }


    public JsonObject getDataObject() {
        return this.data != null ? this.data.getAsJsonObject(KEY_NAME_DATA) : null;
    }


    public JsonArray getDataArray() {
        return this.data != null ? this.data.getAsJsonArray(KEY_NAME_DATA) : null;
    }

    public String getValueFromRoot(String key){
        try {
            return this.data.get(key).getAsString();
        }catch (Exception e){
            return "";
        }

    }

    public boolean isError() {
        return isError;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    /*
     *  check response body success
     *
     */
    public boolean isSuccess(final Activity mContext){

        return  checkCode(mContext);

    }

    public boolean isSuccess(final Context mContext){

        return  checkCode((Activity) mContext);

    }


    public boolean isSuccess(){

        if (getCode() == 200 || getCode() == 202)
            return true;
        else if (getCode()==401){

            // dang nhap het han



        }else if (getCode()==221){

            // khong tim thay user



        }

        return false;
    }

    private boolean checkCode(final Activity mContext){

        if (getCode() == 200 || getCode() == 202)
            return true;
        else if (getCode()==401){

            if (mContext!=null) {

                if (dialog!=null)
                    if (dialog.isShowing())
                        return false;


                dialog =  DialogUtil.createAlertDialog(mContext, R.string.lbl_error_token, (R.string.lbl_het_han), new DialogUtil.IDialogConfirmCancel() {
                    @Override
                    public void onClickOk() {
                        checkShowDialog();
                        //AppUtil.startActivity(mContext, LoginActivity.class);
                        DataStoreManager.removeUser();
                        ((Activity) mContext).finish();
                    }

                    @Override
                    public void onCancel() {
                        checkShowDialog();
                        //AppUtil.startActivity(mContext, LoginActivity.class);
                        DataStoreManager.removeUser();
                        ((Activity) mContext).finish();

                    }
                });
            }


        }else if (getCode()==221){

            if (mContext!=null){

                if (dialog!=null)
                    if (dialog.isShowing())
                        return false;


                dialog =  DialogUtil.createAlertDialog(mContext, R.string.lbl_error_token,(R.string.lbl_user_not_found), new DialogUtil.IDialogConfirmCancel() {
                    @Override
                    public void onClickOk() {
                        checkShowDialog();
                        //AppUtil.startActivity(mContext, LoginActivity.class);
                        DataStoreManager.removeUser();
                        ((Activity)mContext).finish();
                    }

                    @Override
                    public void onCancel() {
                        checkShowDialog();
                        //AppUtil.startActivity(mContext, LoginActivity.class);
                        DataStoreManager.removeUser();
                        ((Activity)mContext).finish();

                    }
                });
            }

        }else {
            AppUtil.showToast(mContext, getMessage());
        }

        return false;
    }

    private void checkShowDialog(){
        if (dialog!=null){
            if (dialog.isShowing())
                dialog.dismiss();

            dialog=null;
            System.gc();
        }
    }

    public String getLogin_token() {
        return login_token;
    }
}
