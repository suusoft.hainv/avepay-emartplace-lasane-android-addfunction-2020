package com.aveplus.avepay_cpocket.music.main.video2.vm;

import android.content.Context;
import android.view.View;


import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.main.video2.ObjVideo;

import org.greenrobot.eventbus.EventBus;

public class VideoVM extends BaseAdapterVM {
    private ObjVideo video;
    private int mPosition;

    public VideoVM(Context self, Object object, int position) {
        super (self, position);
        this.video = (ObjVideo) object;
        this.mPosition = position;
    }

    public String getName() {
        return video.getName ();
    }

    public String getDescription() {
        return video.getDescription ();
    }

    public String getImage() {
        return video.getImage ();
    }

    public void onItemClick(View view) {
    EventBus.getDefault().post(new Global.VideoItemClicked(video));
    }

    @Override
    public void setData(Object object) {
        video = (ObjVideo) object;
        notifyChange ();
    }

    public int isVisible() {
        if (video.getIsFree () == 0) {
            return View.VISIBLE;
        }
        return View.GONE;
    }

}
