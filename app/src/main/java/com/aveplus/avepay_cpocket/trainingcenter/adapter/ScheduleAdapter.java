package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.Schedule;

import java.util.ArrayList;

/**
 * Created by suusoft on 29/08/2017.
 */

public class ScheduleAdapter extends BaseAdapter {
    private ArrayList<Schedule> mListSchedule;

    public ScheduleAdapter(ArrayList<Schedule> mListClass) {
        this.mListSchedule = mListClass;
    }

    @Override
    public int getCount() {
        return mListSchedule.size();
    }

    @Override
    public Object getItem(int position) {
        return mListSchedule.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_schedule, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Schedule aClass = mListSchedule.get(position);
        viewHolder.tvWeek.setText(aClass.getWeek());
        viewHolder.tvDuration.setText(aClass.getStartTime() + " - " + aClass.getEndTime());
        return convertView;
    }

    public class ViewHolder {
        private TextView tvWeek;
        private TextView tvDuration;
        private View view;

        public ViewHolder(View view) {
            this.view = view;
            initUI();
        }

        private void initUI() {
            tvWeek = (TextView) findViewById(R.id.tvWeek);
            tvDuration = (TextView) findViewById(R.id.tvDuration);
        }

        private View findViewById(int idRes) {
            return view.findViewById(idRes);
        }
    }
}
