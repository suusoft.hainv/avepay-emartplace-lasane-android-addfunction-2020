package com.aveplus.avepay_cpocket.taxi.retrofit.response;


import com.aveplus.avepay_cpocket.taxi.objects.UserObj;

public class ResponseUser extends BaseModel {

    public UserObj data;

    public UserObj getData() {
        return data;
    }

    public void setData(UserObj data) {
        this.data = data;
    }
}
