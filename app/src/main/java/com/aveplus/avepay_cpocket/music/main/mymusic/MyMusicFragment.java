package com.aveplus.avepay_cpocket.music.main.mymusic;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.utils.AppUtil;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class MyMusicFragment extends BaseFragmentBinding {

    private MyMusicVM viewModel;
    private TextView tvTitle;

    public static MyMusicFragment newInstance() {

        Bundle args = new Bundle();

        MyMusicFragment fragment = new MyMusicFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_mymusic;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new MyMusicVM(self);
        return viewModel;
    }

    @Override
    protected void initView(View view) {
        AppUtil.hideSoftKeyboard((Activity) self);
        tvTitle = view.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.more_music);
    }
}
