package com.aveplus.avepay_cpocket.restaurant.main.restaurant.toporder;
/**
 * Created by suusoft.com on 11/12/19.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.RandomFood;
import com.bumptech.glide.Glide;


import java.util.List;

public class SearchTopOrderAdapter extends RecyclerView.Adapter<SearchTopOrderAdapter.ViewHolder> {
    private Context context;
    private List<RandomFood> arrayList;

    public SearchTopOrderAdapter(Context context, List<RandomFood> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rowtopolder, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binData(arrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgAdd;
        private TextView tvNameFood;
        private TextView tvPrice;
        private ImageView imgFood;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgAdd = itemView.findViewById (R.id.img_add);
            tvNameFood = itemView.findViewById (R.id.tv_name_food);
            tvPrice = itemView.findViewById (R.id.tv_price);
            imgFood = itemView.findViewById (R.id.img_foodd);
        }

        private void binData(RandomFood randomFood){
            tvNameFood.setText(randomFood.getName());
            tvPrice.setText(randomFood.getPrice()+"RM");
            Glide.with(context).load(randomFood.getAttachment()).into(imgFood);

        }
    }
}
