package com.aveplus.avepay_cpocket.music.main.video;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.music.base.model.BaseModel;


public class Artist  extends BaseModel implements Parcelable {
    private int id,count_views,count_likes,count_rates,rates,is_active,is_new,is_top,is_hot,type,status;
    private String name, img_thumb,img_banner,real_name,description,content,birth_date,location,lang,created_user,updated_user;

    public Artist() {
    }

    public Artist(int id, int count_views, int count_likes, int count_rates, int rates, int is_active, int is_new, int is_top, int is_hot, int type, int status, String name, String img_thumb, String img_banner, String real_name, String description, String content, String birth_date, String location, String lang, String created_user, String updated_user) {
        this.id = id;
        this.count_views = count_views;
        this.count_likes = count_likes;
        this.count_rates = count_rates;
        this.rates = rates;
        this.is_active = is_active;
        this.is_new = is_new;
        this.is_top = is_top;
        this.is_hot = is_hot;
        this.type = type;
        this.status = status;
        this.name = name;
        this.img_thumb = img_thumb;
        this.img_banner = img_banner;
        this.real_name = real_name;
        this.description = description;
        this.content = content;
        this.birth_date = birth_date;
        this.location = location;
        this.lang = lang;
        this.created_user = created_user;
        this.updated_user = updated_user;
    }

    protected Artist(Parcel in) {
        id = in.readInt();
        count_views = in.readInt();
        count_likes = in.readInt();
        count_rates = in.readInt();
        rates = in.readInt();
        is_active = in.readInt();
        is_new = in.readInt();
        is_top = in.readInt();
        is_hot = in.readInt();
        type = in.readInt();
        status = in.readInt();
        name = in.readString();
        img_thumb = in.readString();
        img_banner = in.readString();
        real_name = in.readString();
        description = in.readString();
        content = in.readString();
        birth_date = in.readString();
        location = in.readString();
        lang = in.readString();
        created_user = in.readString();
        updated_user = in.readString();
    }

    public static final Creator<Artist> CREATOR = new Creator<Artist>() {
        @Override
        public Artist createFromParcel(Parcel in) {
            return new Artist(in);
        }

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount_views() {
        return count_views;
    }

    public void setCount_views(int count_views) {
        this.count_views = count_views;
    }

    public int getCount_likes() {
        return count_likes;
    }

    public void setCount_likes(int count_likes) {
        this.count_likes = count_likes;
    }

    public int getCount_rates() {
        return count_rates;
    }

    public void setCount_rates(int count_rates) {
        this.count_rates = count_rates;
    }

    public int getRates() {
        return rates;
    }

    public void setRates(int rates) {
        this.rates = rates;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public int getIs_new() {
        return is_new;
    }

    public void setIs_new(int is_new) {
        this.is_new = is_new;
    }

    public int getIs_top() {
        return is_top;
    }

    public void setIs_top(int is_top) {
        this.is_top = is_top;
    }

    public int getIs_hot() {
        return is_hot;
    }

    public void setIs_hot(int is_hot) {
        this.is_hot = is_hot;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_thumb() {
        return img_thumb;
    }

    public void setImg_thumb(String img_thumb) {
        this.img_thumb = img_thumb;
    }

    public String getImg_banner() {
        return img_banner;
    }

    public void setImg_banner(String img_banner) {
        this.img_banner = img_banner;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public String getUpdated_user() {
        return updated_user;
    }

    public void setUpdated_user(String updated_user) {
        this.updated_user = updated_user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(count_views);
        parcel.writeInt(count_likes);
        parcel.writeInt(count_rates);
        parcel.writeInt(rates);
        parcel.writeInt(is_active);
        parcel.writeInt(is_new);
        parcel.writeInt(is_top);
        parcel.writeInt(is_hot);
        parcel.writeInt(type);
        parcel.writeInt(status);
        parcel.writeString(name);
        parcel.writeString(img_thumb);
        parcel.writeString(img_banner);
        parcel.writeString(real_name);
        parcel.writeString(description);
        parcel.writeString(content);
        parcel.writeString(birth_date);
        parcel.writeString(location);
        parcel.writeString(lang);
        parcel.writeString(created_user);
        parcel.writeString(updated_user);
    }
}
