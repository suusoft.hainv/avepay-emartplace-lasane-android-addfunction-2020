package com.aveplus.avepay_cpocket.music.main.downloaded;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.IOnMenuItemClick;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.main.home.ItemSongsVM;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ItemDownloadedFileVM extends ItemSongsVM {

    public ItemDownloadedFileVM(Context self, Object song, int position) {
        super(self, song, position);
    }


    public void onClickAction(View view) {
        showPopupMenu(view, R.menu.menu_action_file_downloaded);
    }

    public void showPopupMenu(View view, int menu_action_song) {
        Global.showPopupMenu(self, view, menu_action_song, new IOnMenuItemClick() {
            @Override
            public void onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_delete:
                        ((ItemMenuActionSongListener) listener).onClickDelete(position);
                        break;

                    case R.id.action_add_to_queue:
                        actionAddToQueue();
                        break;


                }
            }
        });
    }


}
