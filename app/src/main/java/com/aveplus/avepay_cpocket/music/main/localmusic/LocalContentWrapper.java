package com.aveplus.avepay_cpocket.music.main.localmusic;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;


import com.aveplus.avepay_cpocket.music.model.Artist;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.utils.FileUtil;

import java.io.File;
import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class LocalContentWrapper {

    public static ArrayList<Artist> getArtist(Context context) {
        ArrayList<Artist> list = new ArrayList<>();
        Cursor cursor = FileUtil.getAllUniqueArtists(context);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Artist item = new Artist();
                String id = cursor.getString(0);
                String artist = cursor.getString(1);
                int numOfTrack = cursor.getInt(2);

                item.setId(id);
                item.setName(artist);
                item.setSongs_count(numOfTrack);
                list.add(item);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return list;
    }

    public static ArrayList<Song> getSongsByArtist(Context context, String artistId) {
        return getSongFromCursor(FileUtil.getSongsByArtist(context, artistId));
    }

    public static ArrayList<Song> getAllSongs(Context context) {
        return getSongFromCursor(FileUtil.getAllSongs(context));
    }

    private static ArrayList<Song> getSongFromCursor(Cursor cursor) {
        ArrayList<Song> mListSong = new ArrayList<>();
        if (cursor != null) {
            cursor.moveToFirst();
            File file;
            while (!cursor.isAfterLast()) {
                Song song = new Song();
                String title = cursor.getString(0);
                String artist = cursor.getString(1);
                String path = cursor.getString(2);
                String displayName = cursor.getString(3);
                String songDuration = cursor.getString(4);
                String album = cursor.getString(5);
                String composer = cursor.getString(6);
                String idAlbum = cursor.getString(7);
                String id = cursor.getString(8);
                String size = cursor.getString(9);
                String artistId = cursor.getString(10);

                    /*Cursor cursorImage = context.getContentResolver().query(
                            MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                            new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART},
                            MediaStore.Audio.Albums._ID + "=?",
                            new String[]{idAlbum},
                            null);

                    if (cursorImage.moveToFirst()) {
                        String image = cursorImage.getString(cursorImage.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                        song.setImage(image);
                        song.setThumbnail(image);
                    }*/
                song.setId(id);
                song.setTitle(title);
                song.setNameSinger(artist);
                song.setNameAlbum(album);
                song.setSongLocal(true);
                song.setSong_file(path);
                //song.setSize(String.format("%.2f", getSizeSong(size, song)));
                file = new File(path);
                if (file.exists())
                    mListSong.add(song);
                cursor.moveToNext();
            }
        }


        cursor.close();
        return mListSong;
    }

    //--------------

    public static ArrayList<Song> scanFileMp3ByPaths(Context context, String paths) {
        ArrayList<Song> mListSong = new ArrayList<>();
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0" + " AND " + MediaStore.Audio.Media.DATA + " LIKE '" + paths + "%'";
        String[] projection = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.COMPOSER,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.SIZE
        };
        final String sortOrder = MediaStore.Audio.AudioColumns.TITLE + " COLLATE LOCALIZED ASC";

        Cursor cursor = null;
        try {
            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            cursor = context.getContentResolver().query(uri, projection, selection, null, sortOrder);
            if (cursor != null) {
                File file;
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Song song = new Song();
                    String title = cursor.getString(0);
                    String artist = cursor.getString(1);
                    String path = cursor.getString(2);

                    String displayName = cursor.getString(3);
                    String songDuration = cursor.getString(4);
                    String album = cursor.getString(5);
                    String composer = cursor.getString(6);
                    String idAlbum = cursor.getString(7);
                    String id = cursor.getString(8);
                    String size = cursor.getString(9);

                   /* Cursor cursorImage = context.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART},
                            MediaStore.Audio.Albums._ID + "=?",
                            new String[]{idAlbum},
                            null);

                    if (cursorImage.moveToFirst()) {
                        String image = cursorImage.getString(cursorImage.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                        song.setImage(image);
                        song.setThumbnail(image);
                    }*/
                    song.setId(id);
                    song.setTitle(title);
                    song.setNameSinger(artist);
                    song.setNameAlbum(album);
                    song.setSongLocal(true);
                    song.setSong_file(path);
                    //song.setSize(String.format("%.2f", getSizeSong(size, song)));
                    file = new File(path);
                    if (file.exists())
                        mListSong.add(song);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return mListSong;
    }

    private static float getSizeSong(String size, Song song) {
        if (size == null || size.equals("")) {
            song.setUnits(Song.KB);
            return 0.00f;
        }
        float s = Float.valueOf(size);
        float kb = s / 1024;
        if (kb >= 1024) {
            float mb = kb / 1024;
            song.setUnits(Song.MB);
            return mb;
        } else {
            song.setUnits(Song.KB);
            return kb;
        }
    }

}
