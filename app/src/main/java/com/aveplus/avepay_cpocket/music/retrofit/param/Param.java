package com.aveplus.avepay_cpocket.music.retrofit.param;

public interface Param {

    String PARAM_IMEI = "ime";

    // Params
    String PARAM_USERNAME = "username";
    String PARAM_DOB = "dob";
    String PARAM_STATE = "state";
    String PARAM_CITY = "city";
    String PARAM_CMT_ID = "cmt_id";
    String PARAM_PHONE = "phone";
    String PARAM_EMAIL = "email";
    String PARAM_IS_DRIVER = "is_driver";
    String PARAM_IME = "ime";
    String PARAM_GCM_ID = "gcm_id";
    String PARAM_TYPE = "type";
    String PARAM_TYPE_PRODUCT = "type_product";
    String PARAM_STATUS = "status";
    String PARAM_CODE = "code";
    String PARAM_PASSWORD = "password";
    public static final String PARAM_LOGIN_TYPE = "login_type";
    String PARAM_ADDRESS = "address";
    String PARAM_ZIPCODE = "zipcode";
    String PARAM_GENDER = "gender";
    String PARAM_BIRTHDAY = "birthday";
    String PARAM_DISCOUNT = "discount";
    String PARAM_VIDEO_TITLE_ID = "video_title_id";
    String PARAM_VIDEO_CATEGORY_ID = "category_id";
    String PARAM_NAME = "name";
    String PARAM_AVATAR = "avatar";
    String PARAM_IMAGE4 = "image4";
    String PARAM_IMAGE1 = "image1";
    String PARAM_IMAGE2 = "image2";
    String PARAM_IMAGE3 = "image3";
    String PARAM_DESCRIPTION = "description";
    String PARAM_FIELDS = "fields";
    String PARAM_ACCESS_TOKEN = "access_token";
    String PARAM_LOGIN_METHOD = "login_type";
    // params update profile pro
    String PARAM_USER_ID = "user_id";
    String PARAM_QUANTITY = "quantity";

    String PARAM_BUSINESS_NAME = "business_name";
    String PARAM_BUSINESS_EMAIL = "business_email";
    String PARAM_BUSINESS_PHONE = "business_phone";
    String PARAM_BUSINESS_ADDRESS = "business_address";
    String PARAM_BRAND = "brand";
    String PARAM_MODEL = "model";
    String PARAM_YEAR = "year";
    String PARAM_COST_YOU_CHARGE_PER_KM = "fare";
    String PARAM_CAR_COLOR = "color";
    String PARAM_PLATE = "plate";
    String PARAM_FARE_TYPE = "fare_type";

    String PARAM_TYPE_OF_TRANSPORT = "type";
    String PARAM_FUEL = "fuel_type";
    String PARAM_YEAR_KM = "yearly_km";
    String PARAM_YEAR_INTEND = "year_intend";
    String PARAM_YEAR_TAX = "yearly_tax";
    String PARAM_YEAR_GARA = "yearly_gara";
    String PARAM_AVERAGE_CONSUMPTION = "average_consumption";
    String PARAM_FUEL_UNIT_PRICE = "fuel_unit_price";
    String PARAM_YEAR_INSURANCE = "yearly_insurance";
    String PARAM_YEAR_MAINTENANCE = "yearly_maintenance";
    String PARAM_PRICE_4_NEW_TYRES = "price_4_new_tyres";
    String PARAM_YEAR_UNEXPECTED = "yearly_unexpected";
    String PARAM_SOLD_VALUE = "sold_value";
    String PARAM_BOUGHT_VALUE = "bought_value";
    String PARAM_CERTIFICATION = "driver_license";
    String PARAM_CAR = "image";
    String PARAM_DELIVERY = "is_delivery";

    String PARAM_TOKEN = "token";
    String PARAM_INVENTORY = "inventory";
    String PARAM_SEARCH_TYPE = "search_type";
    String PARAM_IS_ONLINE = "is_online";
    String PARAM_CATEGORY_ID = "category_id";
    String PARAM_IS_CHECKED = "is_checked";
    String PARAM_PAGE = "page";
    String PARAM_LAT = "lat";
    String PARAM_LONG = "long";
    String PARAM_IS_PREMIUM = "is_premium";
    String PARAM_ACTION = "action";
    String PARAM_TIME_TO_PASSENGER = "time_to_passenger";
    String PARAM_ACTION_CREATE = "create";
    String PARAM_ACTION_UPDATE = "update";
    String PARAM_ACTION_ONLINE = "online";
    String PARAM_DURATION = "duration";
    String PARAM_ESTIMATE_DURATION = "estimate_duration";
    String PARAM_PRICE = "price";
    String PARAM_DISCOUNT_TYPE = "discount_type";
    String PARAM_DISCOUNT_PRICE = "discount";
    String PARAM_DISCOUNT_PERCENT = "discount_rate";
    String PARAM_AUTO_RENEW = "is_renew";
    String PARAM_KEY_WORD = "keyword";
    String PARAM_DISTANCE = "distance";
    String PARAM_ESTIMATE_DISTANCE = "estimate_distance";
    String PARAM_NUM_PER_PAGE = "number_per_page";
    String PARAM_OBJECT_ID = "object_id";
    String PARAM_OBJECT_TYPE = "object_type";
    String FAVORITE_TYPE_DEAL = "deal";
    String PARAM_SORT_BY = "sort_by";
    String PARAM_SORT_TYPE = "sort_type";

    String PARAM_TRANSPORT_FEE = "transportFee";
    String PARAM_TRANSPORT_TYPE = "transportType";
    String PARAM_DESTINATION_ROLE = "destination_role";
    String PARAM_CONTENT = "content";
    String PARAM_RATE = "rate";
    String PARAM_ATTACHED = "attachment";


    String PARAM_NEW_PASS = "new_password";
    String PARAM_CURRENT_PASS = "current_password";
    String PARAM_START_LATITUDE = "start_lat";
    String PARAM_END_LATITUDE = "end_lat";
    String PARAM_START_LONGITUDE = "start_long";
    String PARAM_END_LONGITUDE = "end_long";
    String PARAM_DEAL_ID = "deal_id";
    String PARAM_DEAL_NAME = "deal_name";
    String PARAM_BUYER_ID = "buyer_id";
    String PARAM_BUYER_NAME = "buyer_name";
    String PARAM_RESERVATION_ID = "reservation_id";
    //    settings
    String PARAM_NOTIFI = "notify";
    String PARAM_NOTIFI_FAVOURITE = "notify_favourite";
    String PARAM_NOTIFI_TRANSPORT = "notify_transport";
    String PARAM_NOTIFI_FOOD = "notify_food";
    String PARAM_NOTIFI_LABOR = "notify_labor";
    String PARAM_NOTIFI_TRAVEL = "notify_travel";
    String PARAM_NOTIFI_SHOPPING = "notify_shopping";
    String PARAM_NOTIFI_NEW_AND_EVENT = "notify_news";
    String PARAM_NOTIFI_NEARBY = "notify_nearby";
    //    Transaction
    String PARAM_AMOUNT = "amount";
    String PARAM_NOTE = "note";
    String PARAM_METHOD_PAYMENT = "payment_method";
    String PARAM_DESTINATION_EMAIL = "destination_email";
    String PARAM_ID_TRANSACTION = "transaction_id";

    String PARAM_TRIP_ID = "trip_id";
    String PARAM_PASSENGER_ID = "passenger_id";
    String PARAM_DRIVER_ID = "driver_id";
    String PARAM_SEAT_COUNT = "seat_count";
    String PARAM_START_LOCATION = "start_location";
    String PARAM_END_LOCATION = "end_location";
    String PARAM_TRANSACTION_ID = "transaction_id";
    String PARAM_ACTUAL_FARE = "actual_fare";
    String PARAM_ESTIMATE_FARE = "estimate_fare";
    String PARAM_MODE = "mode";
    String PARAM_FRIEND_ID = "friend_id";
    String PARAM_LATLNG = "latlng";
    String PARAM_SENSOR = "sensor";
    String PARAM_RESTAURANT_ID = "restaurant_id";
    String PARAM_BILLING_NAME = "billingName";
    String PARAM_BILLING_ADDRESS = "billingAddress";
    String PARAM_BILLING_PHONE = "billingPhone";
    String PARAM_BILLING_EMAIL = "billingEmail";
    String PARAM_VAT = "vat";
    String PARAM_ID = "id";


    String PARAM_SUB_ID = "sub_id";
    String PARAM_SELLER_NAME = "seller_name";
    String PARAM_PRODUCT_NAME = "keyword";
    String PARAM_SELLER_ID = "seller_id";


    String PARAM_BILLNG_NAME = "billingName";
    String PARAM_BILLNG_PHONE = "billingPhone";
    String PARAM_BILLNG_ADDRESS = "billingAddress";
    String PARAM_TOTAL = "total";
    String PARAM_ORDER_ID = "order_id";
    String PARAM_ITEMS = "items";
    String PARAM_PAYMENT_METHOD = "paymentMethod";


    String PARAM_SUBCRIBE_ID = "subscriber_id";
    String PARAM_SUBCRIBED_ID = "subscribed_id";

}
