package com.aveplus.avepay_cpocket.taxi.datastore;

import com.aveplus.avepay_cpocket.taxi.base.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.objects.SettingsObj;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.objects.favorite.RecentPlace;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SuuSoft.com on 9/13/2016.
 */

public class DataStoreManager extends BaseDataStore {


    // ============== User ============================
    private static final String PREF_USER = "PREF_USER";
    private static final String PREF_TOKEN_USER = "PREF_TOKEN_USER";
    private static final String PREF_RECENT_PLACE = "PREF_RECENT_PLACE";

    // ============== Settings ============================
    private static final String PREF_SETTINGS_NOTIFY = "PREF_SETTINGS_NOTIFY";
    private static final String PREF_SETTINGS_NOTIFY_MESSAGE = "PREF_SETTINGS_NOTIFY_MESSAGE";
    private static final String PREF_SETTINGS_MY_FAVOURITE = "PREF_SETTING_MY_FAVOURITE";
    private static final String PREF_SETTINGS_TRANSPORT_DELIVERIES = "PREF_SETTINGS_TRANSPORT_DELIVERIES";
    private static final String PREF_SETTINGS_LABOR = "PREF_SETTINGS_LABOR";
    private static final String PREF_SETTINGS_FOOD_BERVERAGES = "PREF_SETTINGS_FOOD_BERVERAGES";
    private static final String PREF_SETTINGS_TRAVELLING = "PREF_SETTINGS_TRAVELLING";
    private static final String PREF_SETTINGS_SHOPPING = "PREF_SETTINGS_SHOPPING";
    private static final String PREF_SETTINGS_NEW_AND_EVENTS = "PREF_SETTINGS_NEW_AND_EVENTS";
    private static final String PREF_SETTINGS_NEAR_BY_DEALS = "PREF_SETTINGS_NEAR_BY_DEALS";
    private static final String PREF_LOGIN_CHECKED = "PREF_LOGIN_CHECKED";
    private static final String PREF_RECENT_CHAT = "PREF_RECENT_CHAT";
    private static final String PREF_CURRENT_CHAT = "PREF_CURRENT_CHAT";
    private static final String PREF_BOOK_LATER = "PREF_BOOK_LATER";
    // setting utitlity
    private static final String PREF_SETTING_UTILITY = "SETTING_UTILITY";
    private static final String PREF_TRANSPORT_DEAL = "transportDeal";
    private static final String PREF_CONVERSATION_STATUS = "conversationStatus";
    private static final String PREF_TRACKING_TRIP = "PREF_TRACKING_TRIP";
    private static final String PREF_SCREEN_MAIN = "PREF_SCREEN_MAIN";
    private static final String PREF_DEAL_IS_NEGOTIATED = "dealIsNegotiated";
    private static final String PREF_CONTACTS_LIST = "contactsList";
    private static final String PREF_NEGOTIATED_RESERVATION = "reservationNegotiatedObj";
    private static final String PREF_SETTINGS_ONLINE = "PREF_SETTINGS_ONLINE";
    private static final String PREF_SETTINGS_BOOK_LATER = "PREF_SETTINGS_BOOK_LATER";
    private static final String PREF_ID_TRIP_OLD = "PREF_ID_TRIP_OLD";

    /**
     * save and get user
     */
    public static void saveUser(UserObj user) {
        if (user != null) {
            String jsonUser = new Gson().toJson(user);
            getInstance().sharedPreferences.putStringValue(PREF_USER, jsonUser);
        }
    }

    public static void removeUser() {
        getInstance().sharedPreferences.putStringValue(PREF_USER, null);
    }

    public static UserObj getUser() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_USER);
        UserObj user = new Gson().fromJson(jsonUser, UserObj.class);
        return user;
    }

    public static void clearUserToken() {
        UserObj userObj = getUser();
        userObj.setToken(null);
        saveUser(userObj);
    }


    public static void clearRecentChat() {
        BaseDataStore.getInstance().sharedPreferences.putStringValue(PREF_RECENT_CHAT, "");
    }


    public static void saveBookLater(TripObj obj) {
        if (obj != null) {
            String jsonUser = new Gson().toJson(obj);
            getInstance().sharedPreferences.putStringValue(PREF_BOOK_LATER, jsonUser);
        }
    }

    public static void removeBookLater() {
        getInstance().sharedPreferences.putStringValue(PREF_BOOK_LATER, null);
    }

    public static TripObj getBookLater() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_BOOK_LATER);

        return new Gson().fromJson(jsonUser, TripObj.class);
    }


    /**
     * save status live conversation
     **/
    public static void saveConversationStatus(boolean status) {
        BaseDataStore.getInstance().sharedPreferences.putBooleanValue(PREF_CONVERSATION_STATUS, status);
    }

    public static boolean conversationIsLive() {
        return BaseDataStore.getInstance().sharedPreferences.getBooleanValue(PREF_CONVERSATION_STATUS);
    }


    /**
     * save status trip tracking
     **/
    public static boolean isTrackingTrip() {
        return BaseDataStore.getInstance().sharedPreferences.getBooleanValue(PREF_TRACKING_TRIP);
    }

    public static void saveTrackingTrip(boolean status) {
        BaseDataStore.getInstance().sharedPreferences.putBooleanValue(PREF_TRACKING_TRIP, status);
    }


    /**
     * save status app in screen main
     **/
    public static boolean isScreenMain() {
        return BaseDataStore.getInstance().sharedPreferences.getBooleanValue(PREF_SCREEN_MAIN);
    }

    public static void saveScreenMain(boolean status) {
        BaseDataStore.getInstance().sharedPreferences.putBooleanValue(PREF_SCREEN_MAIN, status);
    }


    /**
     * /**
     * =======================================================
     * Settings screen
     */
    public static void saveSettingsNotify(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY, isChecked);
    }

    public static boolean getSettingsNotify() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY);
    }

    public static void saveSettingsMyFavourite(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_MY_FAVOURITE, isChecked);
    }

    public static boolean getSettingsMyFavourite() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_MY_FAVOURITE);
    }

    public static void saveSettingsTransportDeliveries(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_TRANSPORT_DELIVERIES, isChecked);
    }

    public static boolean getSettingsTransportDeliveries() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_TRANSPORT_DELIVERIES);
    }

    public static void saveSettingsFoodBeverages(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_FOOD_BERVERAGES, isChecked);
    }

    public static boolean getSettingsFoodBeverages() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_FOOD_BERVERAGES);
    }

    public static void saveSettingsLabor(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_LABOR, isChecked);
    }

    public static boolean getSettingsLabor() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_LABOR);
    }

    public static void saveSettingsTravelling(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_TRAVELLING, isChecked);
    }

    public static boolean getSettingsTravelling() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_TRAVELLING);
    }

    public static void saveSettingsShopping(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_SHOPPING, isChecked);
    }

    public static boolean getSettingsShopping() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_SHOPPING);
    }

    public static void saveSettingsNewsAndEvents(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NEW_AND_EVENTS, isChecked);
    }

    public static boolean getSettingsNewsAndEvents() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NEW_AND_EVENTS);
    }

    public static void saveSettingsNearByDeals(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NEAR_BY_DEALS, isChecked);
    }

    public static boolean getSettingsNearByDeals() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NEAR_BY_DEALS);
    }

    public static void saveOnline(boolean isOnline) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_ONLINE, isOnline);
    }

    public static boolean isOnline() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_ONLINE);
    }

    public static void saveBookLater(boolean isOnline) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_BOOK_LATER, isOnline);
    }

    public static boolean isBookLater() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_BOOK_LATER);
    }

    public static void saveSettingsNotifyMessage(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY_MESSAGE, isChecked);
    }

    public static boolean getSettingsNotifyMessage() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY_MESSAGE);
    }

    public static void saveSettingUtility(String setting) {
        getInstance().sharedPreferences.putStringValue(PREF_SETTING_UTILITY, setting);
    }

    public static SettingsObj getSettingUtility() {
        String setting = getInstance().sharedPreferences.getStringValue(PREF_SETTING_UTILITY);
        if (setting.isEmpty()) {
            return null;
        } else {
            try {
                JSONObject jsonObject = new JSONObject(setting);
                ApiResponse apiResponse = new ApiResponse(jsonObject);
                if (!apiResponse.isError()) {
                    SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
                    return utitlityObj;

                } else {
                    return null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }


    public static TripObj getTransport(String driverId) {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_TRANSPORT_DEAL + driverId);

        return new Gson().fromJson(jsonUser, TripObj.class);
    }


    public static RecentPlace getRecentPlace() {
        String json = getInstance().sharedPreferences.getStringValue(PREF_RECENT_PLACE);
        return new Gson().fromJson(json, RecentPlace.class);
    }

    public static void initRecentPlace() {
        RecentPlace recentPlace = new RecentPlace();
        String json = new Gson().toJson(recentPlace);
        getInstance().sharedPreferences.putStringValue(PREF_RECENT_PLACE, json);
    }

    public static String getTripOld() {
        return getInstance().sharedPreferences.getStringValue(PREF_ID_TRIP_OLD);
    }

    public static void saveTripOld(String id) {
        getInstance().sharedPreferences.putStringValue(PREF_ID_TRIP_OLD, id);
    }

    /**
     * save and get user's token
     *
     */
    public static void saveToken(String token) {
        getInstance().sharedPreferences.putStringValue(PREF_TOKEN_USER, token);
    }
    public static String getToken() {
        return getInstance().sharedPreferences.getStringValue(PREF_TOKEN_USER);
    }

}
