
package com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail;
/**
 * Created by suusoft.com on 11/12/19.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RestaurantDetail {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("facebook_link")
    @Expose
    private String facebookLink;
    @SerializedName("fanpage_link")
    @Expose
    private String fanpageLink;
    @SerializedName("ig_link")
    @Expose
    private String igLink;
    @SerializedName("youtube_link")
    @Expose
    private String youtubeLink;
    @SerializedName("open_hour")
    @Expose
    private String openHour;
    @SerializedName("close_hour")
    @Expose
    private String closeHour;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String Long;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("card_number")
    @Expose
    private int cardNumber;
    @SerializedName("card_cvv")
    @Expose
    private int cardCvv;
    @SerializedName("card_exp")
    @Expose
    private String cardExp;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("rate_count")
    @Expose
    private String rateCount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("max_price")
    @Expose
    private String maxPrice;
    @SerializedName("min_price")
    @Expose
    private String minPrice;
    @SerializedName("is_favourite")
    @Expose
    private Boolean isFavourite;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("random_food")
    @Expose
    private ArrayList<RandomFood> randomFood = null;
    @SerializedName("image_video")
    @Expose
    private ArrayList<ImageVideo> imageVideo = null;
    @SerializedName("review")
    @Expose
    private ArrayList<Review> review = null;
    @SerializedName("response_type")
    @Expose
    private String responseType;

    @SerializedName("review_count")
    @Expose
    private Rate review_count;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getFanpageLink() {
        return fanpageLink;
    }

    public void setFanpageLink(String fanpageLink) {
        this.fanpageLink = fanpageLink;
    }

    public String getIgLink() {
        return igLink;
    }

    public void setIgLink(String igLink) {
        this.igLink = igLink;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public String getOpenHour() {
        return openHour;
    }

    public void setOpenHour(String openHour) {
        this.openHour = openHour;
    }

    public String getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(String closeHour) {
        this.closeHour = closeHour;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLong() {
        return Long;
    }

    public void setLong(String aLong) {
        Long = aLong;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(int cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getCardExp() {
        return cardExp;
    }

    public void setCardExp(String cardExp) {
        this.cardExp = cardExp;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRateCount() {
        return rateCount;
    }

    public void setRateCount(String rateCount) {
        this.rateCount = rateCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public Boolean getFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean favourite) {
        isFavourite = favourite;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public ArrayList<RandomFood> getRandomFood() {
        return randomFood;
    }

    public void setRandomFood(ArrayList<RandomFood> randomFood) {
        this.randomFood = randomFood;
    }

    public ArrayList<ImageVideo> getImageVideo() {
        return imageVideo;
    }

    public void setImageVideo(ArrayList<ImageVideo> imageVideo) {
        this.imageVideo = imageVideo;
    }

    public ArrayList<Review> getReview() {
        return review;
    }

    public void setReview(ArrayList<Review> review) {
        this.review = review;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public Rate getReview_count() {
        return review_count;
    }

    public void setReview_count(Rate review_count) {
        this.review_count = review_count;
    }
}
