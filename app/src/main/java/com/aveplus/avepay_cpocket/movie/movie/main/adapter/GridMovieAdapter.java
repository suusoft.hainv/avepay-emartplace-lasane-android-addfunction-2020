package com.aveplus.avepay_cpocket.movie.movie.main.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onPageChangeAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;


import java.lang.ref.WeakReference;
import java.util.List;


/**
 * Copyright © 2019 SUUSOFT
 */
public class GridMovieAdapter extends ArrayAdapter<Movie> {
    private int mResource;
    //    private List<Movie> mMovies;
    private List<Movie> mMovies;
    private LayoutInflater inflater;
    private ImageLoader imageLoader;
    private onPageChangeAdapter callback;
    private int mPosition;
    private Context mContext;
    private GridView mGridview;
    private WeakReference<AdapterView> mWeakAdapterView;

    private Typeface typeface;

    public GridMovieAdapter(Context context, int resource, List<Movie> objects,
                            AdapterView adapterView, onPageChangeAdapter callback) {
        super(context, resource, objects);
        this.mResource = resource;
        this.mMovies = objects;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        imageLoader = RequestManager.getImageLoader();
        this.callback = callback;
        mContext = context;
        mGridview = (GridView) adapterView;
        mWeakAdapterView = new WeakReference<>(adapterView);
        //
        AssetManager assetManager = mContext.getAssets();
        typeface = Typeface.createFromAsset(assetManager, "fonts/Roboto-Bold.ttf");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        HolderView holderView;


        if (view == null) {

            view = inflater.inflate(mResource, null);
            holderView = new HolderView();
            holderView.imgMovie = (NetworkImageView) view.findViewById(R.id.imv_movie);
            holderView.lblName = (TextView) view.findViewById(R.id.tv_name);
            view.setTag(holderView);
            //set font
            holderView.lblName.setTypeface(typeface);
        } else
            holderView = (HolderView) view.getTag();
        if (mMovies != null) {
            //mMovies.get(position).getMovie_thumb()
            holderView.lblName.setText(mMovies.get(position).getMovie_name());
           // Picasso.with(mContext).load(mMovies.get(position).getMovie_thumb()).into(holderView.imgMovie);
           // ImageUtil.setImage(holderView.imgMovie,mMovies.get(position).getMovie_thumb());
            Log.e("IMAGE", mMovies.get(position).getMovie_thumb());

            holderView.imgMovie.setImageUrl(mMovies.get(position).getMovie_thumb(), imageLoader);
        }
        holderView.position = position;
        mPosition = position;
        // Add keyup listener for view of current item
        view.setOnKeyListener(mKeyListener);
        return view;
    }

    private View.OnKeyListener mKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            if ((keyCode != KeyEvent.KEYCODE_ENTER && keyCode != KeyEvent.KEYCODE_DPAD_CENTER)
                    || mWeakAdapterView.get() == null
                    || mWeakAdapterView.get().getOnItemClickListener() == null)
                return false;

            HolderView holderView = (HolderView) v.getTag();
            if (holderView == null)
                return false;

            int position = holderView.position;
            AdapterView adapterView = mWeakAdapterView.get();
            adapterView.getOnItemClickListener().onItemClick(adapterView, v, position, position);
            return true;
        }
    };

    private int pxFromDp(int dp) {
        return (int) (dp * mContext.getResources().getDisplayMetrics().density);
    }

    class HolderView {
        public NetworkImageView imgMovie;
        public TextView lblName;

        public int position;
    }

}
