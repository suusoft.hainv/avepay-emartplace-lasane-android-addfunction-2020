package com.aveplus.avepay_cpocket.taxi.modelmanager;

public interface ModelManagerListener {

    void onSuccess(Object object);

    void onError();

}
