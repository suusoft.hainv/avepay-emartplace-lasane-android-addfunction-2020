package com.aveplus.avepay_cpocket.music.main.home;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.music.main.MainActivity;
import com.aveplus.avepay_cpocket.music.main.mymusic.MyMusicFragment;
import com.aveplus.avepay_cpocket.music.main.video2.VideoFrag;
import com.google.android.material.tabs.TabLayout;


/**
 * "Copyright © 2019 SUUSOFT"
 */
public class MainFragment extends BaseFragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_main;
    }

    @Override
    protected void init() {
        setHasOptionsMenu(false);
    }

    @Override
    protected void initView(View view) {
        tabLayout = (TabLayout) ((MainActivity) getActivity()).getToolbar().findViewById(R.id.tab_layout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        viewPager = (ViewPager) view.findViewById(R.id.view_pagger);
        viewPager.setAdapter(new TabViewPaggerAdapter(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);
    }

    @Override
    protected void getData() {

    }

    private class TabViewPaggerAdapter extends FragmentPagerAdapter {

        String[] titles = getResources().getStringArray(R.array.tabs);

        public TabViewPaggerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return HomeFragment.newInstance();
                case 1:
                    return VideoFrag.newInstance();
//                    return VideoFragment.newInstance();
                case 2:
                    return MyMusicFragment.newInstance();

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }

}
