package com.aveplus.avepay_cpocket.music.modelmanager;

import android.content.Context;


import com.aveplus.avepay_cpocket.music.configs.Config;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.aveplus.avepay_cpocket.music.util.AppUtil;

import java.util.HashMap;

/**
 *
 */
public class RequestManager extends BaseRequest {

    private static final String TAG = RequestManager.class.getSimpleName();

    // Params
    private static final String PARAM_GCM_ID = "gcm_id";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_STATUS = "status";
    private static final String PARAM_IMEI = "ime";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_FULLNAME = "fullname";
    private static final String PARAM_IMAGE = "image";
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_USER_ID = "user_id";
    private static final String PARAM_MESSAGE = "message";

    private static final String PARAM_PAGE = "page";
    private static final String PARAM_ID = "id";
    private static final String PARAM_NAME = "query";
    private static final String PARAM_DESCRIPTION = "description";

    // youtube api
    private static final String PARAM_PART = "part";
    private static final String PARAM_CONTENT_DETAIL = "contentDetails";
    private static final String PARAM_SNIPPET = "snippet";

    private static final String PARAM_CHANNEL_ID = "channelId";
    private static final String PARAM_PLAYLIST_ID = "playlist_id";
    private static final String PARAM_MAX_RESULT = "maxResults";
    private static final String PARAM_KEY = "key";
    public static final String PARAM_VIDEO = "video";
    public static final String PARAM_PAGE_TOKEN = "pageToken";
    public static final String PARAM_ORDER = "order";
    public static final String PARAM_DATE = "date";
    private static final String PARAM_RELEVANT = "relevance";
    private static final String PARAM_RELATED_VIDEO = "relatedToVideoId";
    private static final String PARAM_VIEW_COUNT = "viewCount";
    private static final String PARAM_QUERY = "q";
    private static final String PARAM_PAGE_SIZE = "per-page";
    private static final String PARAM_NUMBERPAGE_SIZE = "number_per_page";
    private static final String PARAM_IS_NEW = "is_new";
    private static final String PARAM_IS_TRENDING = "is_trending";
    private static final String PARAM_IS_POPULAR = "is_popular";
    private static final String PARAM_IS_VIDEO = "is_video";
    private static final String PARAM_CATEGORY_ID = "category_id";
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_KHALTI_TOKEN = "khalti_token";
    private static final String PARAM_AMOUNT = "amount";
    private static final String PARAM_ATM = "atm";
    private static final String PARAM_RID = "rid";
    private static final String PARAM_PID = "pid";
    private static final String PARAM_SDC = "sdc";


    ///////////////////////////////////////////////////////////////////////////
    // Add functions connecting to server
    ///////////////////////////////////////////////////////////////////////////

    public static void registerDevice(String gcm, String ime, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_IMEI, ime);
        params.put(PARAM_GCM_ID, gcm);
        params.put(PARAM_TYPE, "1");// 1 is android

        get(Config.Api.REGISTER_DEVICE, params, false, completeListener);
    }

    public static void register(Context context, String email, String password, String type, final CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_EMAIL, email);
        params.put(PARAM_PASSWORD, password);
        params.put(PARAM_TYPE, type);

        get(Config.Api.URL_REGISTER, params, completeListener);

    }

    public static void login(Context context, String username, String password, final CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USERNAME, username);
        params.put(PARAM_PASSWORD, password);
        params.put(PARAM_IMEI, AppUtil.getIMEI(context));
        params.put(PARAM_GCM_ID, DataStoreManager.getTokenFCM());
        params.put(PARAM_TYPE, "1");
        params.put(PARAM_STATUS, "1");

        get(Config.Api.URL_LOGIN, params, completeListener);
    }

    public static void resetPassword(Context context, String email, final CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_EMAIL, email);
        get(Config.Api.URL_RESET_PASSWORD, params, completeListener);
    }


    public static void getHome(Context context, CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        get(Config.Api.HOME, params, true, false, completeListener);
    }

    public static void getItemByCategory(int page, String categoryId, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE, page + "");
        params.put(PARAM_CATEGORY_ID, categoryId);
        params.put(PARAM_NUMBERPAGE_SIZE, Config.MAX_RESULT + "");
        get(Config.Api.ITEM_BY_CATEGORY, params, true, false, completeListener);
    }

    public static void getItemByPlaylist(int page, String playlistId, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE, page + "");
        params.put(PARAM_PLAYLIST_ID, playlistId);
        params.put(PARAM_NUMBERPAGE_SIZE, Config.MAX_RESULT_PLAYLIST + "");
        get(Config.Api.ITEM_BY_PLAYLIST, params, true, false, completeListener);
    }

    public static void getAllCategories(final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE_SIZE, "50");
        get(Config.Api.ALL_CATEGORIES, params, true, false, completeListener);
    }

    public static void getAllAlbumBy(int position, final CompleteListener completeListener) {
       //old params int page, int isTop, int isHot, int isNew
        HashMap<String, String> params = new HashMap<>();
//        params.put(PARAM_PAGE_SIZE, Config.MAX_RESULT + "");
//        params.put(PARAM_PAGE, page + "");
//        params.put("top", isTop + "");
//        params.put("hot", isHot + "");
//        params.put("new", isNew + "");
        params.put("playlist_title_id",position+"" );
        get(Config.Api.ALL_ALBUM_BY, params, true, false, completeListener);
    }

    public static void searchSongs(int page, String keywork, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_NUMBERPAGE_SIZE, Config.MAX_RESULT + "");
        params.put(PARAM_NAME, keywork);
        params.put(PARAM_PAGE, page + "");
        get(Config.Api.SEARCH, params, true, false, completeListener);
    }

    public static void registerDevice( final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
       // params.put(PARAM_TOKEN,DataStoreManager.getToken());
        params.put(PARAM_GCM_ID, DataStoreManager.getTokenFCM());
        params.put("type", "1");
        params.put("status", "1");
        get(Config.Api.REGISTER_DEVICE, params, false, completeListener);
    }

    public static void getVideo(String is_new, String is_trending, String is_popular, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_IS_NEW, is_new);
        params.put(PARAM_IS_TRENDING, is_trending);
        params.put(PARAM_IS_POPULAR, is_popular);
        get(Config.Api.URL_GET_VIDEO, params, false, completeListener);
    }

    public static void getCategory(String is_video, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_IS_VIDEO, is_video);
        get(Config.Api.URL_GET_CATEGORY, params, false, completeListener);
    }

    public static void getVideoCategory(String category_id, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_CATEGORY_ID, category_id);
        get(Config.Api.URL_GET_VIDEO_CATEGORY, params, false, completeListener);
    }

    public static void getListSubscribe(final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();

        get(Config.Api.URL_GET_LIST_SUBSCRIBE, params, false, completeListener);
    }

    public static void getPaymentKhalti(String token, String khalti_token, String amount, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_TOKEN, token);
        params.put(PARAM_KHALTI_TOKEN, khalti_token);
        params.put(PARAM_AMOUNT, amount);
        get(Config.Api.URL_GET_PAYMENT_KHALTI, params, false, completeListener);
    }

    public static void getPaymentEsewa(String token, String atm, String rid, String pid, String sdc, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_TOKEN, token);
        params.put(PARAM_ATM, atm);
        params.put(PARAM_RID, rid);
        params.put(PARAM_PID, pid);
        params.put(PARAM_SDC, sdc);
        get(Config.Api.URL_GET_PAYMENT_ESEWA, params, false, completeListener);
    }

    public static void checkPayment(String token, final CompleteListener completeListener){
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_TOKEN, token);
        get(Config.Api.URL_CHECK_PAYMENT, params, false, completeListener);
    }

}
