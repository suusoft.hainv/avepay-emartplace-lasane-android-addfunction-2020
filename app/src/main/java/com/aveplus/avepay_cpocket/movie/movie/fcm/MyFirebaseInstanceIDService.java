package com.aveplus.avepay_cpocket.movie.movie.fcm;

import android.util.Log;

import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.movie.movie.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.movie.movie.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.movie.movie.modelmanager.ModelManagerListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;



public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);

        // Save fcm token into preference
        GlobalFunctions.saveFCMToken(this, refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        sendRegistrationToServer(refreshedToken);


    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */

    private void sendRegistrationToServer(String token) {

        DataStoreManager.saveTokenFCM(token);

        ModelManager.registerDevice(getBaseContext(),  token, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {

            }

            @Override
            public void onError() {

            }
        } );

    }




}
