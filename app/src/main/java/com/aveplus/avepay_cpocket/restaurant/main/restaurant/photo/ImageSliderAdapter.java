package com.aveplus.avepay_cpocket.restaurant.main.restaurant.photo;
/**
 * Created by suusoft.com on 11/12/19.
 */
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.PagerAdapter;

import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.ImageVideo;
import com.bumptech.glide.Glide;

import java.util.List;

public class ImageSliderAdapter extends PagerAdapter {
    private Context mContex;
    private List<ImageVideo> imageVideos;

    public ImageSliderAdapter(Context mContex, List<ImageVideo> imageVideos) {
        this.mContex = mContex;
        this.imageVideos = imageVideos;
    }

    @Override
    public int getCount() {
        return imageVideos.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        // Tạo ImageView
        final AppCompatImageView imageView = new AppCompatImageView(mContex);
        container.addView(imageView);
        // Load ảnh vào ImageView bằng Glide
        Glide.with(mContex).load(imageVideos.get(position).getImage()).into(imageView);
        // Return
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        // container chính là ViewPager, còn Object chính là return của instantiateItem ứng với position
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
