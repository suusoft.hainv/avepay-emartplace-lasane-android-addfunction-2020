package com.aveplus.avepay_cpocket.restaurant.main.history;

public class HistoryModel {
    private  String Datetime;
    private  String Adress;
    private  int pay;

    public String getDatetime() {
        return Datetime;
    }

    public void setDatetime(String datetime) {
        Datetime = datetime;
    }

    public String getAdress() {
        return Adress;
    }

    public void setAdress(String adress) {
        Adress = adress;
    }

    public int getPay() {
        return pay;
    }

    public void setPay(int pay) {
        this.pay = pay;
    }

    public HistoryModel(String datetime, String adress, int pay) {
        Datetime = datetime;
        Adress = adress;
        this.pay = pay;
    }
}
