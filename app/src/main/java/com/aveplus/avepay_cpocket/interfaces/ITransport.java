package com.aveplus.avepay_cpocket.interfaces;

import com.aveplus.avepay_cpocket.objects.TransportObj;

/**
 * Created by Suusoft on 11/29/2016.
 */

public interface ITransport {
    void onTransportSelected(TransportObj transportObj);
}
