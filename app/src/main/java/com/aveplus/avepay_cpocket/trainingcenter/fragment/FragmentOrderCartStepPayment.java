package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;


import org.json.JSONObject;

public class FragmentOrderCartStepPayment extends BaseFragment {
    public static final String MESSAGE_SUCCESS = "success";
    private Button btnSendPayment;
    LinearLayout llPaypal, llCreditCard, llDeliver;

    View view;
    private String selected_payment_method = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_order_step_payment, container, false);
        initUI(view);


        return view;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("ordercart");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("cart");
        else
            Log.i(TAG, "onTitleChanged is null");
    }


    @Override
    public void onDestroy() {
        self.stopService(new Intent(self, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data
                    .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("paymentExample",
                            "Payment OK. Response :" + confirm.toJSONObject());
                    JSONObject json = confirm.toJSONObject();


                } catch (Exception e) {
                    Log.e("paymentExample",
                            "an extremely unlikely failure occurred: ", e);
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample",
                    "An invalid payment was submitted. Please see the docs.");
        }
    }

    private void initUI(View v) {
        btnSendPayment = (Button) v.findViewById(R.id.btn_send_payment);
        llPaypal = (LinearLayout) v.findViewById(R.id.llPaypal);
        llCreditCard = (LinearLayout) v.findViewById(R.id.llCreditCard);
        llDeliver = (LinearLayout) v.findViewById(R.id.llDeliver);
        setoOnclick();
    }

    private void setoOnclick() {
        btnSendPayment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentOrderCartStepConfirm payment = new FragmentOrderCartStepConfirm();
                Bundle bundle = new Bundle();
                bundle.putDouble("payment", 0);
                payment.setArguments(bundle);
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left,
                                R.anim.slide_out_left)
                        .replace(R.id.layout_replace, payment, "showConfirm")
                        .addToBackStack(null).commit();
            }
        });
        final String[] items = self.getResources().getStringArray(
                R.array.payment_method);
        llPaypal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                llPaypal.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_payment_selected));
                llDeliver.setBackgroundColor(getActivity().getResources().getColor(R.color.payment_item));
                llCreditCard.setBackgroundColor(getActivity().getResources().getColor(R.color.payment_item));
            }
        });
        llDeliver.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                llPaypal.setBackgroundColor(getActivity().getResources().getColor(R.color.payment_item));
                llDeliver.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_payment_selected));
                llCreditCard.setBackgroundColor(getActivity().getResources().getColor(R.color.payment_item));
            }
        });
        llCreditCard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                llPaypal.setBackgroundColor(getActivity().getResources().getColor(R.color.payment_item));
                llDeliver.setBackgroundColor(getActivity().getResources().getColor(R.color.payment_item));
                llCreditCard.setBackgroundColor(getActivity().getResources().getColor(R.color.bg_payment_selected));
            }
        });


    }


}
