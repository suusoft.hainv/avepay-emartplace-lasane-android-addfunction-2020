package com.aveplus.avepay_cpocket.ebook.model;


public class Category {

    private String id;
    private String name;
    private String image;
    private String order;
    private String status;
    private String description;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public String getOrder() {
        return order;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", order='" + order + '\'' +
                ", status_hot='" + status + '\'' +
                '}';
    }
}
