package com.aveplus.avepay_cpocket.music.main.detail;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListActivityBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.main.home.ItemSongsVM;
import com.aveplus.avepay_cpocket.music.model.Album;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class AlbumActivity extends BaseListActivityBinding {

    private FloatingActionButton buttonPlay;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AlbumVM viewModel;
    private Album album;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {
        Bundle bundle = getIntent().getExtras();
        album = bundle.getParcelable(Constant.PREF_KEY_OBJECT);
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new AlbumVM(self, album);
        return viewModel;
    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_album;
    }

    @Override
    protected void onViewCreated() {
        setUpView();
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(new SingleAdapter(self, R.layout.item_song_album, viewModel.getListData(), ItemSongsVM.class));
    }

    private void setUpView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_toolbar_gradient_transparent));
        initToolBarNav();

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(album.title);
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent));
        buttonPlay = (FloatingActionButton) findViewById(R.id.buttonPlay);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    buttonPlay.hide();
                } else {
                    buttonPlay.show();
                }
            }
        });
    }


}
