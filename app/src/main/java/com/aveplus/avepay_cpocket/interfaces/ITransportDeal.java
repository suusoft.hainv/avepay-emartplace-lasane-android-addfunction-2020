package com.aveplus.avepay_cpocket.interfaces;

import com.aveplus.avepay_cpocket.objects.TransportDealObj;

/**
 * Created by Suusoft on 12/05/2016.
 */

public interface ITransportDeal {

    void onCancel(TransportDealObj obj);

    void onTracking(TransportDealObj obj);

    void onFinish(TransportDealObj obj);
}
