package com.aveplus.avepay_cpocket.ebook.widgets.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;


public class ProgressBarDialog extends Dialog {


    private SeekBar seekBar;
    private TextView tvPercent, btnCancel;

    public ProgressBarDialog(Context context, IOnClickCancel iOnClickCancel ) {
        super(context);
        this.iOnClickCancel = iOnClickCancel;
    }


    public ProgressBarDialog(Context context ) {
        super(context);
    }


    private IOnClickCancel iOnClickCancel;

    public interface IOnClickCancel{
        void onCancel();
    }


    public void setiOnClickCancel(IOnClickCancel iOnClickCancel) {
        this.iOnClickCancel = iOnClickCancel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(
                R.drawable.bg_dialog_progress);
        setContentView(R.layout.layout_progress_bar_dialog);


        seekBar = findViewById(R.id.progress);
        tvPercent = findViewById(R.id.tv_percent);
        btnCancel = findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iOnClickCancel.onCancel();
            }
        });
    }


    public void setProgress(int progress){
        seekBar.setProgress(progress);
        tvPercent.setText(progress + " %");
    }

    public void setPercent(int progress){
        tvPercent.setText(progress + " %");
    }




}
