package com.aveplus.avepay_cpocket.music.main.home;

import android.content.Context;
import android.view.View;


import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.model.Category;
import com.aveplus.avepay_cpocket.music.util.AdsUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ItemCategoryVM extends BaseAdapterVM {
    public static final String TAG = "Playlist";
    private static int count = 0;
    private static long timeold = 0;
    private Category item;

    public ItemCategoryVM(Context self, Object album, int position) {
        super(self, position);
        this.item = (Category) album;
    }

    public String getBackground() {
        return item.image;
    }

    public String getName() {
        return item.name;
    }

    public String getTitle() {
        return item.title;
    }


    @Override
    public void setData(Object object) {
        item = (Category) object;
        notifyChange();
    }

    public void onItemClicked(View view) {
        EventBus.getDefault().post(new Global.ItemObjectClicked(item, 0));
        //  AdsUtil.checkShowAds(self);
        //  checkShowAds(self);
    }

    public static void checkShowAds(Context context) {
        count = count + 1;

        if (count == 5) {
            AdsUtil.loadInterstitial(context);
            count = 0;
        }
//        else if (count == 10) {
//            AdsUtil.loadInterstitial(context);
//            count = 5;
//
//        }
    }


}
