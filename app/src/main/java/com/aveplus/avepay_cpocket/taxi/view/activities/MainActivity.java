package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.PacketUtility;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.configs.ChatConfigs;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.globals.Constants;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.interfaces.IChangeTitle;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.network1.MyProgressDialog;
import com.aveplus.avepay_cpocket.taxi.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.taxi.objects.SettingsObj;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.BaseModel;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseTripObj;
import com.aveplus.avepay_cpocket.taxi.trip.TripManager;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.ImageUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.IMaps;
import com.aveplus.avepay_cpocket.taxi.utils.map.LocationService;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.taxi.view.fragment.BecomeDriverFragment;
import com.aveplus.avepay_cpocket.taxi.view.fragment.BonusFragment;
import com.aveplus.avepay_cpocket.taxi.view.fragment.InfoUserFragment;
import com.aveplus.avepay_cpocket.taxi.view.fragment.PayFragment;
import com.aveplus.avepay_cpocket.taxi.view.fragment.SettingsFragment;
import com.aveplus.avepay_cpocket.taxi.view.fragment.WebViewFragment;
import com.aveplus.avepay_cpocket.taxi.view.instant.BookingFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_about_us;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_become_driver;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_bonus;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_booking_later;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_faqs;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_instant_booking;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_negotiated_booking;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_pay;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_profile;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_settings;
import static com.aveplus.avepay_cpocket.taxi.configs.Menu.nav_share;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        IChangeTitle {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int RC_LOCATION_PERMISSION_TO_UPDATE_DRIVER_LOCATION = 1;
    private static final int RC_TURN_ON_LOCATION_TO_UPDATE_DRIVER_LOCATION = 2;
    private static final int RC_LOCATION_PERMISSION_TO_UPDATE_USER_LOCATION = 3;
    private static final int RC_TURN_ON_LOCATION_TO_UPDATE_USER_LOCATION = 4;

    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;

    private SettingsFragment mSettingsFragment;
    private InfoUserFragment profileFragment;
    private BecomeDriverFragment infoUserProFragment;
    private BookingFragment bookingFragment;
    private PayFragment mPayFragment;
    private WebViewFragment mWebViewFragment;

    private GoogleApiClient mGoogleApiClient;
    private int mSelectedNav;
    private boolean userLocationIsUpdated;
    private TextView btnBookLater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGoogleApiClient();
        Log.e(TAG, "Token: " + DataStoreManager.getToken());
        // Start location service in some cases(driver closes app without deactivating)
        updateDriverLocation();

        // Get contacts from scratch
        setSelectedOnNavigation(0);

        sendRegistrationToServer(GlobalFunctions.getFCMToken(this));

    }


    private void sendRegistrationToServer(String token) {
        if (DataStoreManager.getUser() != null) {
            ApiUtils.getAPIService().updateGcmId(DataStoreManager.getTokenUser(), token, "1")
                    .enqueue(new Callback<BaseModel>() {
                        @Override
                        public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {

                        }

                        @Override
                        public void onFailure(Call<BaseModel> call, Throwable t) {

                        }
                    });
        }


    }

    @Override
    void inflateLayout() {
        setContentView(R.layout.activity_main_taxi);
    }

    @Override
    void initUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AppUtil.hideSoftKeyboard(MainActivity.this);
                updateMenuHeader();
            }
        };
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        btnBookLater = findViewById(R.id.btn_book_later);

        btnBookLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavigationItemSelected(mNavigationView.getMenu().getItem(nav_booking_later));
            }
        });

        onNavigationItemSelected(mNavigationView.getMenu().getItem(nav_instant_booking));
        setSelectedOnNavigation(nav_instant_booking);


        AppController.getInstance().setUserUpdated(true);
        updateMenuHeader();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_LOCATION_PERMISSION_TO_UPDATE_USER_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        updateUserLocation();
                    } else {
                        showPermissionsReminder(RC_LOCATION_PERMISSION_TO_UPDATE_USER_LOCATION, true);
                    }
                }
                break;
            case RC_LOCATION_PERMISSION_TO_UPDATE_DRIVER_LOCATION: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (MapsUtil.locationIsEnable(self)) {
                            updateDriverLocation();
                        } else {
                            turnOnLocationReminder(RC_TURN_ON_LOCATION_TO_UPDATE_DRIVER_LOCATION, false);
                        }
                    } else {
                        showPermissionsReminder(RC_LOCATION_PERMISSION_TO_UPDATE_DRIVER_LOCATION, true);
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    private void updateMenuHeader() {
        if (AppController.getInstance().isUserUpdated()) {
            View view = mNavigationView.getHeaderView(0);
            ImageView imageView = (ImageView) view.findViewById(R.id.civ_avatar);
            TextView tvName = (TextView) view.findViewById(R.id.lbl_name);
            TextView tvEmail = (TextView) view.findViewById(R.id.lbl_email);
            UserObj userObj = DataStoreManager.getUser();
            if (userObj != null) {
                tvName.setText(userObj.getName());
                tvEmail.setText(userObj.getPhoneNumber());
                ImageUtil.setImage(self, imageView, userObj.getAvatar());
            }

            AppController.getInstance().setUserUpdated(false);
        }
    }

    private void checkTripStatus() {
        Log.e(TAG, " checkTripStatus ");
        Log.e(TAG, "test: " + DataStoreManager.getUser().getId() + "hihi" + DataStoreManager.getTokenUser());
        Call<ResponseTripObj> call = ApiUtils.getAPIService().getTripStatus(DataStoreManager.getUser().getId(), "0", DataStoreManager.getTokenUser());
        call.enqueue(new Callback<ResponseTripObj>() {
            @Override
            public void onResponse(Call<ResponseTripObj> call, Response<ResponseTripObj> response) {
                if (response.isSuccessful())
                    if (response.body().isSuccess()) {
                        if (response.body().getData() != null) {
                            TripObj tripObj;
                            Log.e(TAG, "respone: " + response.body().toJson());
                            tripObj = response.body().getData();

                            TripManager.directionTrip(self, tripObj, TAG);


                        } else {
                            Log.e(TAG, "onResponse1: " + response.message());
                        }
                    } else {
                        Log.e(TAG, "onResponse2: " + response.message());
                    }

            }

            @Override
            public void onFailure(Call<ResponseTripObj> call, Throwable t) {
                Log.e(TAG, "onFailure3: " + t.getMessage());
            }
        });
    }

    @Override
    void initControl() {
        mNavigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    protected void getExtraValues() {
        super.getExtraValues();
        Log.e("EE", "EE: MAIN");
        getExtra(getIntent());
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("EE", "EE: NEWINTENT MAIN");
        getExtra(intent);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        setFragment(id);

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.onResume();
        DataStoreManager.saveScreenMain(true);
        checkTripStatus();
    }


    public void setFragment(final int navId) {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (navId == R.id.nav_instant_booking) {

            //AppUtil.startActivity(self, InstantBookingActivity.class);
            setSelectedOnNavigation(nav_instant_booking);

            if (mSelectedNav != navId) {

                if (bookingFragment == null) {
                    bookingFragment = BookingFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, bookingFragment).commit();

                mSelectedNav = navId;
            }

            setTitle(R.string.instant_booking);


        } else if (navId == R.id.nav_bonus) {
            setSelectedOnNavigation(nav_bonus);
            if (mSelectedNav != navId) {

                fragmentTransaction.replace(R.id.frl_main, BonusFragment.newInstance()).commit();

                mSelectedNav = navId;
            }

            setTitle(R.string.bonus);

        } else if (navId == R.id.nav_pay) {
            setSelectedOnNavigation(nav_pay);
            if (mSelectedNav != navId) {
                if (mPayFragment == null) {
                    mPayFragment = PayFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, mPayFragment).commit();

                setTitle(R.string.wallet);
                mSelectedNav = navId;
            }
        } else if (navId == R.id.nav_profile) {
            setSelectedOnNavigation(nav_profile);
            if (mSelectedNav != navId) {
                if (profileFragment == null) {
                    profileFragment = InfoUserFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, profileFragment).commit();

                setTitle(R.string.my_account);

                mSelectedNav = navId;
            }
        } else if (navId == R.id.nav_become_driver) {
            setSelectedOnNavigation(nav_become_driver);

            if (mSelectedNav != navId) {
                if (infoUserProFragment == null) {
                    infoUserProFragment = BecomeDriverFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, infoUserProFragment).commit();

                setTitle(R.string.become_driver);

                mSelectedNav = navId;
            }
        } else if (navId == R.id.nav_share) {
            setSelectedOnNavigation(nav_share);
            AppUtil.share(this, "http://play.google.com/store/apps/details?id=" + new PacketUtility().getPacketName());
        } else if (navId == R.id.nav_help) {
            onOpenScreenHelp(navId, fragmentTransaction);

        } else if (navId == R.id.nav_settings) {
            if (mSelectedNav != navId) {
                if (mSettingsFragment == null) {
                    mSettingsFragment = SettingsFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, mSettingsFragment).commit();

                setTitle(R.string.settings);

                mSelectedNav = navId;
            }
            setSelectedOnNavigation(nav_settings);
        } else if (navId == R.id.nav_about_us) {

            onOpenScreenAboutUs(navId, fragmentTransaction);
            setSelectedOnNavigation(nav_about_us);

        } else if (navId == R.id.nav_log_out) {
            logout();
        } else if (navId == R.id.nav_faqs) {

            onOpenScreenFAQs(navId, fragmentTransaction);
            setSelectedOnNavigation(nav_faqs);

        }

    }

    private void onOpenScreenFAQs(final int navId, final FragmentTransaction fragmentTransaction) {
        if (mSelectedNav != navId) {
            SettingsObj setting = DataStoreManager.getSettingUtility();
            if (setting == null) {
                if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                    ModelManager.getSettingUtility(this, new ModelManagerListener() {
                        @Override
                        public void onSuccess(Object object) {
                            JSONObject jsonObject = (JSONObject) object;
                            ApiResponse apiResponse = new ApiResponse(jsonObject);
                            if (!apiResponse.isError()) {
                                DataStoreManager.saveSettingUtility(jsonObject.toString());
                                SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.faq), utitlityObj.getFaq());

                                Bundle bundle = new Bundle();
                                bundle.putString(Constants.KEY_URL, utitlityObj.getFaq());
                                mWebViewFragment = WebViewFragment.newInstance(bundle);

                                fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                                setTitle(getString(R.string.faq));

                                mSelectedNav = navId;
//                                    openWebView(getString(R.string.about_us), DataStoreManager.getSettingUtility().getAbout());
                            }

                        }

                        @Override
                        public void onError() {
                        }
                    });
                } else {
                    AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
                }
            } else {

                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getFaq());
                mWebViewFragment = WebViewFragment.newInstance(bundle);

                fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                setTitle(getString(R.string.faq));

                mSelectedNav = navId;
//                    openWebView(getString(R.string.faq), DataStoreManager.getSettingUtility().getFaq());
            }
        }

    }

    private void onOpenScreenAboutUs(final int navId, final FragmentTransaction fragmentTransaction) {
        if (mSelectedNav != navId) {
            SettingsObj setting = DataStoreManager.getSettingUtility();
            if (setting == null) {
                if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                    ModelManager.getSettingUtility(this, new ModelManagerListener() {
                        @Override
                        public void onSuccess(Object object) {
                            JSONObject jsonObject = (JSONObject) object;
                            ApiResponse apiResponse = new ApiResponse(jsonObject);
                            if (!apiResponse.isError()) {
                                DataStoreManager.saveSettingUtility(jsonObject.toString());
                                SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.about_us), utitlityObj.getAbout());

                                Bundle bundle = new Bundle();
                                bundle.putString(Constants.KEY_URL, utitlityObj.getAbout());
                                mWebViewFragment = WebViewFragment.newInstance(bundle);

                                fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                                setTitle(getString(R.string.about_us));

                                mSelectedNav = navId;
                            }
                        }

                        @Override
                        public void onError() {
                        }
                    });
                } else {
                    AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
                }
            } else {

                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getAbout());
                mWebViewFragment = WebViewFragment.newInstance(bundle);

                fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                setTitle(getString(R.string.about_us));
                mSelectedNav = navId;
//                    openWebView(getString(R.string.about_us), DataStoreManager.getSettingUtility().getAbout());
            }

        }

    }

    private void onOpenScreenHelp(final int navId, final FragmentTransaction fragmentTransaction) {
        if (mSelectedNav != navId) {
            SettingsObj setting = DataStoreManager.getSettingUtility();
            if (setting == null) {
                if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                    ModelManager.getSettingUtility(this, new ModelManagerListener() {
                        @Override
                        public void onSuccess(Object object) {
                            JSONObject jsonObject = (JSONObject) object;
                            ApiResponse apiResponse = new ApiResponse(jsonObject);
                            if (!apiResponse.isError()) {
                                DataStoreManager.saveSettingUtility(jsonObject.toString());
                                SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
                                //openWebView(getString(R.string.help), utitlityObj.getHelp());


                                Bundle bundle = new Bundle();
                                bundle.putString(Constants.KEY_URL, utitlityObj.getHelp());
                                mWebViewFragment = WebViewFragment.newInstance(bundle);

                                fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                                setTitle(getString(R.string.help));

                                mSelectedNav = navId;
                            } else {
                                AppUtil.showToast(getApplicationContext(), apiResponse.getMessage());
                            }
                        }

                        @Override
                        public void onError() {
                            AppUtil.showToast(getApplicationContext(), "Error!");
                        }
                    });
                } else {
                    AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
                }

            } else {

                Bundle bundle = new Bundle();
                bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getHelp());
                mWebViewFragment = WebViewFragment.newInstance(bundle);

                fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                setTitle(getString(R.string.help));

                mSelectedNav = navId;
                //openWebView(getString(R.string.help), DataStoreManager.getSettingUtility().getHelp());
            }
        }
        setSelectedOnNavigation(8);
    }


    private void setSelectedOnNavigation(int i) {
        mNavigationView.getMenu().getItem(i).setCheckable(true);
    }


    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Args.RC_TURN_ON_LOCATION) {
            if (MapsUtil.locationIsEnable(self)) {
                getMyLocation();
            } else {
                turnOnLocationReminder(Args.RC_TURN_ON_LOCATION, true);
            }
        } else if (requestCode == RC_TURN_ON_LOCATION_TO_UPDATE_DRIVER_LOCATION) {
            if (MapsUtil.locationIsEnable(self)) {
                updateDriverLocation();
            } else {
                turnOnLocationReminder(RC_TURN_ON_LOCATION_TO_UPDATE_DRIVER_LOCATION, false);
            }
        } else if (requestCode == RC_TURN_ON_LOCATION_TO_UPDATE_USER_LOCATION) {
            if (MapsUtil.locationIsEnable(self)) {
                updateUserLocation();
            } else {
                turnOnLocationReminder(RC_TURN_ON_LOCATION_TO_UPDATE_USER_LOCATION, true);
            }
        }
    }

    private void getMyLocation() {
        if (MapsUtil.locationIsEnable(self)) {
            MapsUtil.getMyLocation(mGoogleApiClient, new IMaps() {
                @Override
                public void processFinished(Object obj) {
                    AppController.getInstance().setMyLocation((Location) obj);
                }
            });
        } else {
            MapsUtil.displayLocationSettingsRequest(this, Args.RC_TURN_ON_LOCATION);
        }
    }

    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(self)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        MapsUtil.getMyLocation(mGoogleApiClient, new IMaps() {
            @Override
            public void processFinished(Object obj) {
                Location location = (Location) obj;
                AppController.getInstance().setMyLocation(location);
            }
        });

        updateUserLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private void getExtra(Intent intent) {
        Bundle bundle = intent.getExtras();
        //processNotificationNewDeal(bundle);
        if (bundle != null) {
            Log.e(TAG, "bundle != null");
            String notificationType = "";
            if (bundle.containsKey(Args.NOTIFICATION_TYPE)) {
                notificationType = bundle.getString(Args.NOTIFICATION_TYPE);
                if (notificationType != null) {
                    Log.e(TAG, "type is " + notificationType);
                    if (notificationType.equalsIgnoreCase(ChatConfigs.QUICKBLOX_MESSAGE)) {
                        if (bundle.containsKey(Args.RECENT_CHAT_OBJ)) {

                        } else {
                            Log.e(TAG, "have no recentChatObj");
                        }
                    } else if (notificationType.equalsIgnoreCase(ChatConfigs.NOTIFICATION_LOCATION_PERMISSION)) {
                        if (GlobalFunctions.locationIsGranted(self, RC_LOCATION_PERMISSION_TO_UPDATE_DRIVER_LOCATION, "")) {
                            updateDriverLocation();
                        }
                    } else if (notificationType.equalsIgnoreCase(ChatConfigs.NOTIFICATION_TURN_ON_LOCATION)) {
                        if (MapsUtil.locationIsEnable(self)) {
                            updateDriverLocation();
                        } else {
                            MapsUtil.displayLocationSettingsRequest(self, RC_TURN_ON_LOCATION_TO_UPDATE_DRIVER_LOCATION);
                        }
                    }
                }
            }
        } else {
            Log.e(TAG, "bundle is null");
        }
    }

    private void openWebView(String title, String url) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_TITLE, title);
        bundle.putString(Constants.KEY_URL, url);
        AppUtil.startActivity(MainActivity.this, WebViewActivity.class, bundle);
    }

    private void logout() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            final MyProgressDialog progressDialog = new MyProgressDialog(self);
            progressDialog.show();

            RequestManager.logout(self, new BaseRequest.CompleteListener() {

                @Override
                public void onSuccess(com.aveplus.avepay_cpocket.taxi.network.ApiResponse response) {
                    processBeforeLoggingOut(progressDialog);
                }

                @Override
                public void onError(String message) {
                    AppUtil.showToast(self, message);
                }
            });

        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }

    private void processBeforeLoggingOut(MyProgressDialog progressDialog) {
        if (DataStoreManager.getUser() != null && DataStoreManager.getUser().getDriverData() != null) {
            if (DataStoreManager.getUser().getDriverData().isAvailable()) {
                LocationService.start(self, LocationService.STOP_REQUESTING_LOCATION);

                // Deactivate driver's mode before logging out
                ModelManager.activateDriverMode(self, Constants.OFF, 0, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                    }

                    @Override
                    public void onError() {
                    }
                });
            }
        }

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        DataStoreManager.clearUserToken();
        DataStoreManager.removeUser();
        AppController.getInstance().setUserUpdated(true);
        AppUtil.startActivity(self, SplashLoginActivity.class);
        finish();
    }

    public void updateDriverLocation() {
        if (DataStoreManager.getUser() != null && DataStoreManager.getUser().getDriverData() != null) {
            if (DataStoreManager.getUser().getDriverData().isAvailable()) {
                LocationService.start(self, LocationService.REQUEST_LOCATION);
            }
        }
    }

    private void updateUserLocation() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            if (!userLocationIsUpdated) {
                if (DataStoreManager.getUser() != null) {
                    if ((DataStoreManager.getUser().getDriverData() != null
                            && !DataStoreManager.getUser().getDriverData().isAvailable())
                            || DataStoreManager.getUser().getDriverData() == null) {
                        if (GlobalFunctions.locationIsGranted(self, RC_LOCATION_PERMISSION_TO_UPDATE_USER_LOCATION, null)) {
                            if (MapsUtil.locationIsEnable(self)) {
                                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                                    MapsUtil.getMyLocation(mGoogleApiClient, new IMaps() {
                                        @Override
                                        public void processFinished(Object obj) {
                                            Location location = (Location) obj;

                                            ModelManager.updateLocation(self, new LatLng(location.getLatitude(), location.getLongitude()));
                                            userLocationIsUpdated = true;
                                        }
                                    });
                                }
                            } else {
                                MapsUtil.displayLocationSettingsRequest(self, RC_TURN_ON_LOCATION_TO_UPDATE_USER_LOCATION);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onChangTitle(String title) {
        setTitle(title);

    }

    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver(receiverDeals);
        AppController.onPause();
        DataStoreManager.saveScreenMain(false);
    }

    /**
     * show dialog thông báo cho user biết người đối thoại đã hủy deal
     **/


    private void goToChat(TripObj data) {

    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        mGoogleApiClient.connect();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        // Reset recent chat list
        DataStoreManager.clearRecentChat();

        // Keep conversation status
        DataStoreManager.saveConversationStatus(false);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.PushDriverAccept event) {
        if (bookingFragment != null)
            bookingFragment.hideProgress();


        checkTripStatus();
        EventBus.getDefault().post(new Global.FinishAllChatAct());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.PushBookLater event) {
        Log.e(TAG, "PushBookLater");

        if (DataStoreManager.getBookLater() != null)
            showDialogConfirmBookLater();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final Global.PushBookNegociatePrice event) {
        Log.e(TAG, "PushBookNegociatePrice tripObj : " + new Gson().toJson(event.tripObj));

        onNavigationItemSelected(mNavigationView.getMenu().getItem(nav_negotiated_booking));


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.FinishAllCurrentAct event) {/* Do something */
        finish();
    }

    ;


    private void showDialogConfirmBookLater() {


        final AlertDialog.Builder builder = new AlertDialog.Builder(self/*, R.style.AlertDialog*/);
        builder.setTitle(DataStoreManager.getBookLater().getTimeBookLater());
        builder.setMessage(R.string.do_you_want_to_book);
        builder.setCancelable(false);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                // nếu app đang show màn hình bookingFragment
                TripObj tripObj = DataStoreManager.getBookLater();
                if (tripObj.isInstantBooking()) {
                    // nếu app đang ko show fragment bookingFragment thì gọi hàm naỳ
                    if (mSelectedNav == R.id.nav_instant_booking) {
                        if (bookingFragment != null)
                            bookingFragment.checkBookLater();
                    } else {
                        onNavigationItemSelected(mNavigationView.getMenu().getItem(nav_instant_booking));
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                bookingFragment.checkBookLater();
                            }
                        }, 1000);
                    }
                }

                dialog.dismiss();

            }
        });

        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DataStoreManager.removeBookLater();
                DataStoreManager.saveBookLater(false);
                dialog.dismiss();

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.PushDriverAcceptDeal event) {

        checkTripStatus();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.GoToChat event) {/* Do something */
        goToChat(event.tripObj);

    }

    ;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.PushCancelTrip event) {/* Do something */

        EventBus.getDefault().post(new Global.FinishAllChatAct());
        AppUtil.showToast(self, R.string.your_trip_has_been_canceled);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final Global.NotifiShowRequestTrip event) {/* Do something */

        onNavigationItemSelected(mNavigationView.getMenu().getItem(nav_instant_booking));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                bookingFragment.showProgress(event.tripObj);
            }
        }, 1000);

    }

    ;


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.NotifiShowToast event) {/* Do something */
        AppUtil.showToast(self, event.gcm);

    }

}
