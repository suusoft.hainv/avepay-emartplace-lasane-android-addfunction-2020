package com.aveplus.avepay_cpocket.trainingcenter.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.listener.IOnItemClickedListener;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.CategoryProductAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ProductAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.DetailCourse;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.CategoryProduct;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.aveplus.avepay_cpocket.trainingcenter.widget.EndlessRecyclerOnScrollListener;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.view.activities.BaseActivity;

import java.util.ArrayList;

/**
 * Created by suusoft on 05/09/2017.
 */

public class ResultSearchActivity extends BaseActivity {
    private ProductAdapter mAdapter = null;
    private View view;
    private CategoryProductAdapter categoryAdapter;
    private ArrayList<Product> listPro = new ArrayList<Product>();
    private ArrayList<CategoryProduct> arrCategories = new ArrayList<CategoryProduct>();
    private EditText edtSearch;
    private Spinner spnCategories;
    private int currentPage, totalPage;
    private int categoryIndex = 0;
    private boolean isPullRefresh = true;
    private HomeActivity mEducationActivity;
    private ImageView btnClose;
    private LinearLayout llParent;
    private RecyclerView rcvData;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_search);
        initUI();

        initControl();
        initGridView();
        initData();
    }


    @Override
    protected void inflateLayout() {

    }

    public void initUI() {
        rcvData = (RecyclerView) view.findViewById(R.id.rcv_data);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        spnCategories = (Spinner) findViewById(R.id.spinerPro);
        btnClose = (ImageView) findViewById(R.id.btnClose);
        llParent = (LinearLayout) findViewById(R.id.productFragment);
        closeKeyboardWhenTouchOutside(this, llParent);
        edtSearch.setSelected(true);
        edtSearch.setFocusable(true);
        edtSearch.requestFocus();
        showKeyboard(this, edtSearch);
    }

    @Override
    protected void initControl() {
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        rcvData.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        spnCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                categoryIndex = position;
                refreshGridView(false);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchProductByKey(edtSearch.getText().toString());
                }
                return false;
            }
        });
    }


    private void initGridView() {
        mAdapter = new ProductAdapter(self, listPro);
        mAdapter.setListener(new IOnItemClickedListener() {
            @Override
            public void onItemClicked(View view, int position) {
                onClickItem(position);
            }
        });
        rcvData.setAdapter(mAdapter);
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(new EndlessRecyclerOnScrollListener.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                getAllProduct(currentPage, false);

            }
        });
        rcvData.addOnScrollListener(endlessRecyclerOnScrollListener);

    }

    private void onClickItem(int position) {
        DetailCourse fragment = new DetailCourse();
        Product item = listPro.get(position);
        Bundle bundle = new Bundle();
        bundle.putParcelable("item", item);
        Intent intent = new Intent(getApplicationContext(), DetailCouresActivity.class);
        intent.putExtra("bundle", bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void searchProductByKey(String key) {
        currentPage = 1;
        int categoryID = 0;
        if (categoryIndex != 0) {
            CategoryProduct category = arrCategories.get(categoryIndex);
            categoryID = category.getId();
        }
        ModelManager.getListProByCateAndKey(String.valueOf(categoryID), key,
                new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        listPro.clear();
                        listPro.addAll(ParseUtility.parseListPro(response.getRootStr()));
                        totalPage = ParseUtility.getAllPage(response.getRootStr());
                        if (listPro.size() == 0) {
                            Toast.makeText(self, "No data is found !",
                                    Toast.LENGTH_SHORT).show();
                        }
                        mAdapter.notifyDataSetChanged();
                        endlessRecyclerOnScrollListener.setEnded(endlessRecyclerOnScrollListener.getCurrentPage() >= totalPage);

                    }

                    @Override
                    public void onError(String message) {
                        AppUtil.showToast(self, message);
                    }
                });


    }

    @SuppressLint("NewApi")
    private void refreshGridView(boolean isPull) {
        String searchKey = edtSearch.getText().toString();
        currentPage = 1;
//        getAllProduct(currentPage, isPull);
        if (searchKey.isEmpty() && categoryIndex == 0) {
            getAllProduct(currentPage, isPull);
            return;
        }
        if (searchKey.isEmpty() && categoryIndex != 0) {
            CategoryProduct category = arrCategories.get(categoryIndex);
            getProductByCategory(category.getId(), currentPage);
            return;
        }
        if (!searchKey.isEmpty() && categoryIndex == 0) {
            searchProductByKey(searchKey);
            return;
        }
        if (!searchKey.isEmpty() && categoryIndex != 0) {
            searchProductByKey(searchKey);
            return;
        }
    }


    private void getProductByCategory(int id, final int currentPage2) {
        ModelManager.getListProductByCateGory(id, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                listPro.clear();
                listPro.addAll(ParseUtility.parseListPro(response.getRootStr()));
                if (listPro.size() == 0) {
                    Toast.makeText(self, "No data is found !",
                            Toast.LENGTH_SHORT).show();
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

    private void getAllProduct(int currentPage2, boolean isPull) {
        loadGrid(currentPage2, isPull);
    }

    private void initData() {
        getCategories();
        initSearch();
        getAllProduct(currentPage, false);
    }

    private void getCategories() {
        ModelManager.getListCategoryProduct(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                arrCategories.clear();
                CategoryProduct item = new CategoryProduct();
                item.setName(self.getString(R.string.all_categories));
                arrCategories.add(item);
                ArrayList<CategoryProduct> arr = ParseUtility
                        .parseListProductCategories(response.getRootStr());
                arrCategories.addAll(arr);
                setDataCategoryToList(arrCategories);
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });

    }

    private void setDataCategoryToList(ArrayList<CategoryProduct> arrCategorys) {
        categoryAdapter = new CategoryProductAdapter(self,
                R.layout.layout_text_spinner, arrCategorys);
        categoryAdapter
                .setDropDownViewResource(R.layout.layout_text_row_spinner);
        spnCategories.setAdapter(categoryAdapter);
        spnCategories.setSelection(0);
    }

    private void initSearch() {
    }

    private void loadGrid(final int page, boolean isPull) {
        ModelManager.getListProducts(page,
                new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        if (page <= 1)
                            listPro.clear();
                        listPro.addAll(ParseUtility.parseListPro(response.getRootStr()));
                        totalPage = ParseUtility.getAllPage(response.getRootStr());
                        mAdapter.notifyDataSetChanged();
                        endlessRecyclerOnScrollListener.setEnded(endlessRecyclerOnScrollListener.getCurrentPage() >= totalPage);

                    }

                    @Override
                    public void onError(String message) {
                        AppUtil.showToast(self, message);
                    }
                });
    }

    public void closeKeyboardWhenTouchOutside(final Activity act, View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(act);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                closeKeyboardWhenTouchOutside(act, innerView);
            }
        }
    }

    public void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public static void showKeyboard(Context ctx, EditText editText) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }
}
