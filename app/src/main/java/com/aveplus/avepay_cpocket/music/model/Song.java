package com.aveplus.avepay_cpocket.music.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.music.base.model.BaseModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Song extends BaseModel implements Parcelable {
    public static final String KEY_SONG = "song";
    public static final String HEADER = "header";
    public static final String FOOTER = "footer";
    public static final String NORMAL = "normal";
    public static final String KB = "kb";
    public static final String MB = "mb";
    public String nameSinger;
    public String nameAlbum;
    public String content;
    public String song_file, song_url;
    public String size;
    public String units;
    @SerializedName("img_thumb")
    public String image;
    @SerializedName("name")
    public String title;
    public String album_id;
    public boolean isSongLocal;
    public boolean isPlaying;
    public int count_download, count_views, count_likes;
    public long duration;
    public String id, description, release_date, song_artist;

    protected Song(Parcel in) {
        nameSinger = in.readString();
        nameAlbum = in.readString();
        content = in.readString();
        song_file = in.readString();
        song_url = in.readString();
        size = in.readString();
        units = in.readString();
        image = in.readString();
        title = in.readString();
        album_id = in.readString();
        isSongLocal = in.readByte() != 0;
        isPlaying = in.readByte() != 0;
        count_download = in.readInt();
        count_views = in.readInt();
        count_likes = in.readInt();
        duration = in.readLong();
        id = in.readString();
        description = in.readString();
        release_date = in.readString();
        song_artist = in.readString();
        musicArtists = in.createTypedArrayList(Artist.CREATOR);
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    public String getSong_artist() {
        return song_artist;
    }

    public void setSong_artist(String song_artist) {
        this.song_artist = song_artist;
    }

    List<Artist> musicArtists;

    public Song() {
    }


    public String getSong_url() {
        return song_url;
    }

    public void setSong_url(String song_url) {
        this.song_url = song_url;
    }

    public String getNameAlbum() {
        return nameAlbum;
    }

    public void setNameAlbum(String nameAlbum) {
        this.nameAlbum = nameAlbum;
    }

    public void setSong_file(String song_file) {
        this.song_file = song_file;
    }

    public boolean isSongLocal() {
        return isSongLocal;
    }

    public void setSongLocal(boolean songLocal) {
        isSongLocal = songLocal;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public int getCount_download() {
        return count_download;
    }

    public void setCount_download(int count_download) {
        this.count_download = count_download;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public String getNameSinger() {
        String name = "";
        if (getMusicArtists().size() > 0) {
            for (Artist item : musicArtists) {
                name += item.name + ", ";
            }
            return name.substring(0, name.lastIndexOf(","));
        }
        return nameSinger;
    }

    public void setNameSinger(String nameSinger) {

        this.nameSinger = nameSinger;
    }

    public String getSong_file() {
        if (song_url != null && !song_url.isEmpty())
            return song_url;
        return song_file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Artist> getMusicArtists() {
        if (musicArtists == null)
            musicArtists = new ArrayList<>();
        return musicArtists;
    }

    public void setMusicArtists(List<Artist> musicArtists) {
        this.musicArtists = musicArtists;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameSinger);
        dest.writeString(nameAlbum);
        dest.writeString(content);
        dest.writeString(song_file);
        dest.writeString(song_url);
        dest.writeString(size);
        dest.writeString(units);
        dest.writeString(image);
        dest.writeString(title);
        dest.writeString(album_id);
        dest.writeByte((byte) (isSongLocal ? 1 : 0));
        dest.writeByte((byte) (isPlaying ? 1 : 0));
        dest.writeInt(count_download);
        dest.writeInt(count_views);
        dest.writeInt(count_likes);
        dest.writeLong(duration);
        dest.writeString(id);
        dest.writeString(description);
        dest.writeString(release_date);
        dest.writeString(song_artist);
        dest.writeTypedList(musicArtists);
    }
}
