package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer;

import android.os.Bundle;
import android.util.Log;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.configs.Constant;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class QuickPlayActivity extends YouTubeBaseActivity {

    private static final String TAG = QuickPlayActivity.class.getSimpleName();
    private String curVideo = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_play);

        curVideo = getIntent().getStringExtra(Constant.KEY_VIDEO);
        Log.e(TAG, "curVideo:" + curVideo);

        YouTubePlayerView youTubePlayerView =
                (YouTubePlayerView) findViewById(R.id.player);


        youTubePlayerView.initialize(AppController.getInstance().getString(R.string.GOOGLE_API_KEY),
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {

                        youTubePlayer.setShowFullscreenButton(false);
                        // do any work here to cue video, play video, etc.
                        youTubePlayer.loadVideo(curVideo);

                        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                            @Override
                            public void onLoading() {

                                Log.e(TAG, "onLoading:");
                            }

                            @Override
                            public void onLoaded(String s) {
                                Log.e(TAG, "onLoaded:");

                            }

                            @Override
                            public void onAdStarted() {
                                Log.e(TAG, "onAdStarted:");

                            }

                            @Override
                            public void onVideoStarted() {
                                Log.e(TAG, "onVideoStarted:");
                                youTubePlayer.play();

                            }

                            @Override
                            public void onVideoEnded() {
                                Log.e(TAG, "onVideoEnded:");

                            }

                            @Override
                            public void onError(YouTubePlayer.ErrorReason errorReason) {
                                Log.e(TAG, "onError:");

                            }
                        });


                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });


    }

}
