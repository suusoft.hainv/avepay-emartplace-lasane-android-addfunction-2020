package com.aveplus.avepay_cpocket.ebook.network;

import com.aveplus.avepay_cpocket.ebook.listener.IBaseListener;

/**
 * Created by suusoft.com on 1/26/18.
 */

public interface ListenerLoading extends IBaseListener {
    void onLoadingIsProcessing();
    void onLoadingIsCompleted();
}
