package com.aveplus.avepay_cpocket.movie.movie.main.movies.model;

import java.net.URL;
import java.util.Comparator;

public abstract class BaseYTParser {

	static public class VideoStream {
		public YTParser.VideoQuality vq;
		public URL url;

		public VideoStream(YTParser.VideoQuality vq, URL u) {
			this.vq = vq;
			this.url = u;
		}
	}

	static public class VideoContentFirst implements Comparator<VideoStream> {

		@Override
		public int compare(VideoStream o1, VideoStream o2) {
			Integer i1 = o1.vq.ordinal();
			Integer i2 = o2.vq.ordinal();
			Integer ic = i1.compareTo(i2);

			return ic;
		}

	}

}
