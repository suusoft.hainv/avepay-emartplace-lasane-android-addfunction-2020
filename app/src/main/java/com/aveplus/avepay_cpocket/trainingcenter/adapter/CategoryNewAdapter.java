package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.CategoryNewEvent;

import java.util.ArrayList;

public class CategoryNewAdapter extends ArrayAdapter<CategoryNewEvent> {

	Activity act;
	private ArrayList<CategoryNewEvent> lstCategoryNew;
	private LayoutInflater inflate;
	private int selectRow = -1;

	public CategoryNewAdapter(Activity context, int resource,
                              ArrayList<CategoryNewEvent> objects) {
		super(context, resource, objects);
		act = context;
		lstCategoryNew = objects;
		inflate = (LayoutInflater) act
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = inflate.inflate(R.layout.layout_text_spinner, parent, false);

		selectRow = position;
		return row;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflate.inflate(R.layout.layout_text_row_spinner,
					null);
			holder.lblTextcontent = (TextView) convertView
					.findViewById(R.id.lblTextContent);
			holder.layoutRowSpinner = (RelativeLayout) convertView
					.findViewById(R.id.layoutRowSpinner);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		CategoryNewEvent o = lstCategoryNew.get(position);

		if (o != null) {
			if (selectRow == position) {
				holder.layoutRowSpinner.setBackgroundColor(act.getResources()
						.getColor(R.color.cl_tabmain));
				holder.lblTextcontent.setTextColor(Color.WHITE);

			} else {
				// holder.lblTextcontent.setTextColor(Color.BLACK);
				holder.layoutRowSpinner.setBackgroundColor(Color.WHITE);
			}
			holder.lblTextcontent.setText(o.getName());
		}
		return convertView;
	}

	class ViewHolder {
		public TextView lblTextcontent;
		public RelativeLayout layoutRowSpinner;
	}

}
