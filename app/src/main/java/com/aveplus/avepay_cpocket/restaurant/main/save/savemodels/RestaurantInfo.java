package com.aveplus.avepay_cpocket.restaurant.main.save.savemodels;
/**
 * Created by suusoft.com on 11/12/19.
 */
import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestaurantInfo extends BaseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("open_hour")
    @Expose
    private String openHour;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("card_number")
    @Expose
    private Object cardNumber;
    @SerializedName("card_cvv")
    @Expose
    private Object cardCvv;
    @SerializedName("card_exp")
    @Expose
    private String cardExp;
    @SerializedName("rate")
    @Expose
    private Object rate;
    @SerializedName("rate_count")
    @Expose
    private Object rateCount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("max_price")
    @Expose
    private Object maxPrice;
    @SerializedName("min_price")
    @Expose
    private Object minPrice;
    @SerializedName("distance")
    @Expose
    private Object distance;
    @SerializedName("random_food")
    @Expose
    private Object randomFood;
    @SerializedName("image_video")
    @Expose
    private Object imageVideo;
    @SerializedName("review")
    @Expose
    private Object review;


//    public RestaurantInfo(Integer id, String name, String description, String image, String phone, String email, String openHour, Object maxPrice, Object minPrice) {
//        this.id = id;
//        this.name = name;
//        this.description = description;
//        this.image = image;
//        this.phone = phone;
//        this.email = email;
//        this.openHour = openHour;
//        this.maxPrice = maxPrice;
//        this.minPrice = minPrice;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOpenHour() {
        return openHour;
    }

    public void setOpenHour(String openHour) {
        this.openHour = openHour;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String get_long() {
        return _long;
    }

    public void set_long(String _long) {
        this._long = _long;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Object cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Object getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(Object cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getCardExp() {
        return cardExp;
    }

    public void setCardExp(String cardExp) {
        this.cardExp = cardExp;
    }

    public Object getRate() {
        return rate;
    }

    public void setRate(Object rate) {
        this.rate = rate;
    }

    public Object getRateCount() {
        return rateCount;
    }

    public void setRateCount(Object rateCount) {
        this.rateCount = rateCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Object getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Object maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Object getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Object minPrice) {
        this.minPrice = minPrice;
    }

    public Object getDistance() {
        return distance;
    }

    public void setDistance(Object distance) {
        this.distance = distance;
    }

    public Object getRandomFood() {
        return randomFood;
    }

    public void setRandomFood(Object randomFood) {
        this.randomFood = randomFood;
    }

    public Object getImageVideo() {
        return imageVideo;
    }

    public void setImageVideo(Object imageVideo) {
        this.imageVideo = imageVideo;
    }

    public Object getReview() {
        return review;
    }

    public void setReview(Object review) {
        this.review = review;
    }
}
