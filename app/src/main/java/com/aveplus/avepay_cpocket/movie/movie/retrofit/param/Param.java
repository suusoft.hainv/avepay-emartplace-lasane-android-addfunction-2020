package com.aveplus.avepay_cpocket.movie.movie.retrofit.param;


/**
 * Created by Suusoft 2020.
 */

public interface Param {

    public static final String PARAM_IMEI = "ime";

    // Params
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_IS_DRIVER = "is_driver";
    public static final String PARAM_FUEL_CONSUMPTION = "fuel_consumption";
    public static final String PARAM_FEATURE = "feature";

    public static final String PARAM_IME = "ime";
    public static final String PARAM_GCM_ID = "gcm_id";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_CODE_NAME = "code_name";
    public static final String PARAM_MAC_ADDRESS = "mac_address";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_CREATED_DATE = " created_date";

    public static final String PARAM_ZIPCODE = "zipcode";
    public static final String PARAM_GENDER = "gender";
    public static final String PARAM_BIRTHDAY = "birthday";
    public static final String PARAM_AVATAR = "avatar";
    public static final String PARAM_INVITE_CODE = "invite_code";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_LEVEL = "level";
    public static final String PARAM_ORTHER_SKILL = "other_skill";
    public static final String PARAM_IMAGE = "image";
    public static final String PARAM_IMAGE_1 = "image_1";
    public static final String PARAM_IMAGE_2 = "image_2";
    public static final String PARAM_IMAGE_3 = "image_3";
    public static final String PARAM_IMAGE_4 = "image_4";
    public static final String PARAM_PHONE = "phone";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_FIELDS = "fields";
    public static final String PARAM_ACCESS_TOKEN = "access_token";
    public static final String PARAM_LOGIN_METHOD = "login_type";
    // params update profile pro
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_QB_USER_ID = "qb_id";

    public static final String PARAM_FEATURE_SUNROOF = "feature_sunroof";
    public static final String PARAM_FEATURE_GPS = "feature_gps";
    public static final String PARAM_FEATURE_KID_CHAIR = "feature_kidchair";
    public static final String PARAM_FEATURE_CAMERA = "feature_reservecamera";
    public static final String PARAM_FEATURE_BLUETOOTH = "feature_bluetooth";
    public static final String PARAM_FEATURE_USB = "feature_usb";
    public static final String PARAM_FEATURE_MAP = "feature_map";




    public static final String PARAM_PAGE_SIZE = "per-page";
    public static final String PARAM_IS_SELLER = "is_seller";
    public static final String PARAM_DOB = "dob";
    public static final String PARAM_CMT_ID = "cmt_id";
    public static final String PARAM_CMT_DATE = "cmt_date";
    public static final String PARAM_CMT_PLACE = "cmt_place";
    public static final String PARAM_SERVICE_ID = "service_id";
    public static final String PARAM_PRICE = "price";
    public static final String PARAM_NUMBER_CERTIFICATE = "numberCertificates";
    public static final String PARAM_NAME_CERTIFICATE = "cer_name";
    public static final String PARAM_IS_PROVIDER = "is_provider";
    public static final String PARAM_KEYWORD = "keyword";
    public static final String PARAM_NUMBER_PER_PAGE = "number_per_page";

    public static final String PARAM_BUSINESS_NAME = "business_name";
    public static final String PARAM_BUSINESS_EMAIL = "business_email";
    public static final String PARAM_BUSINESS_PHONE = "business_phone";
    public static final String PARAM_BUSINESS_ADDRESS = "business_address";
    public static final String PARAM_BRAND = "brand";
    public static final String PARAM_MODEL = "model";
    public static final String PARAM_YEAR = "year";
    public static final String PARAM_COST_YOU_CHARGE_PER_KM = "fare";
    public static final String PARAM_CAR_COLOR = "color";
    public static final String PARAM_PLATE = "plate";
    public static final String PARAM_FARE_TYPE = "fare_type";
    public static final String PARAM_COLOR = "color";

    public static final String PARAM_YEARS = "year";
    public static final String PARAM_VEHICLE_TRANSMISSION = "vehicle_transmission";
    public static final String PARAM_PRICE_PER_DAY = "price_per_day";
    public static final String PARAM_PRICE_PER_KM = "price_per_km";
    public static final String PARAM_PRICE_OVER_LIMIT = "price_over_limt";

    public static final String PARAM_TYPE_OF_TRANSPORT = "type";
    public static final String PARAM_FUEL = "fuel_type";
    public static final String PARAM_YEAR_KM = "yearly_km";
    public static final String PARAM_YEAR_INTEND = "year_intend";
    public static final String PARAM_YEAR_TAX = "yearly_tax";
    public static final String PARAM_YEAR_GARA = "yearly_gara";
    public static final String PARAM_AVERAGE_CONSUMPTION = "average_consumption";
    public static final String PARAM_FUEL_UNIT_PRICE = "fuel_unit_price";
    public static final String PARAM_YEAR_INSURANCE = "yearly_insurance";
    public static final String PARAM_YEAR_MAINTENANCE = "yearly_maintenance";
    public static final String PARAM_PRICE_4_NEW_TYRES = "price_4_new_tyres";
    public static final String PARAM_YEAR_UNEXPECTED = "yearly_unexpected";
    public static final String PARAM_SOLD_VALUE = "sold_value";
    public static final String PARAM_BOUGHT_VALUE = "bought_value";
    public static final String PARAM_CERTIFICATION = "driver_license";
    public static final String PARAM_CAR = "image";
    public static final String PARAM_DELIVERY = "is_delivery";

    public static final String PARAM_TOKEN = "token";
    public static final String PARAM_ID_VEHICLE_BRAND = "id_vehicle_brand";
    public static final String PARAM_SEAT = "seat";
    public static final String PARAM_BRAND_NAME = "brand_name";


    public static final String PARAM_FUEL_TYPE = "fuel_type";


    public static final String PARAM_INVENTORY = "inventory";
    public static final String PARAM_SEARCH_TYPE = "search_type";
    public static final String PARAM_IS_ONLINE = "is_online";
    public static final String PARAM_CATEGORY_ID = "category_id";
    public static final String PARAM_PAGE = "page";
    public static final String PARAM_LAT = "lat";
    public static final String PARAM_LONG = "long";
    public static final String PARAM_IS_PREMIUM = "is_premium";
    public static final String PARAM_ACTION = "action";
    public static final String PARAM_TIME_TO_PASSENGER = "time_to_passenger";
    public static final String PARAM_ACTION_CREATE = "create";
    public static final String PARAM_ACTION_UPDATE = "update";
    public static final String PARAM_ACTION_ONLINE = "online";
    public static final String PARAM_DURATION = "duration";
    public static final String PARAM_ESTIMATE_DURATION = "estimate_duration";
    public static final String PARAM_DISCOUNT_TYPE = "discount_type";
    public static final String PARAM_DISCOUNT_PRICE = "discount";
    public static final String PARAM_DISCOUNT_PERCENT = "discount_rate";
    public static final String PARAM_AUTO_RENEW = "is_renew";
    public static final String PARAM_KEY_WORD = "keyword";
    public static final String PARAM_DISTANCE = "distance";
    public static final String PARAM_ESTIMATE_DISTANCE = "estimate_distance";
    public static final String PARAM_NUM_PER_PAGE = "number_per_page";
    public static final String PARAM_OBJECT_ID = "object_id";
    public static final String PARAM_OBJECT_TYPE = "object_type";
    public static final String FAVORITE_TYPE_DEAL = "deal";
    public static final String PARAM_SORT_BY = "sort_by";
    public static final String PARAM_SORT_TYPE = "sort_type";
    public static final String PARAM_HOME_DELIVERY_AROUND = "home_delivery_around";
    public static final String PARAM_HOME_DELIVERY_PRICE = "home_delivery_price";
    public static final String PARAM_LIMITED_KMPERDAY = "limited_kmperday";
    public static final String PARAM_LIMITED_FEEPERDAY = "limited_feeperday";

    public static final String PARAM_DESTINATION_ID = "destination_id";
    public static final String PARAM_AUTHOR_ROLE = "author_role";
    public static final String PARAM_DESTINATION_ROLE = "destination_role";
    public static final String PARAM_CONTENT = "content";
    public static final String PARAM_RATE = "rate";
    public static final String PARAM_TERMS_CAR = "terms_car";
    public static final String PARAM_ATTACHED = "attachment";


    public static final String PARAM_NEW_PASS = "new_password";
    public static final String PARAM_CURRENT_PASS = "current_password";
    public static final String PARAM_START_LATITUDE = "start_lat";
    public static final String PARAM_END_LATITUDE = "end_lat";
    public static final String PARAM_START_LONGITUDE = "start_long";
    public static final String PARAM_END_LONGITUDE = "end_long";
    public static final String PARAM_DEAL_ID = "deal_id";
    public static final String PARAM_DEAL_NAME = "deal_name";
    public static final String PARAM_BUYER_ID = "buyer_id";
    public static final String PARAM_BUYER_NAME = "buyer_name";
    public static final String PARAM_RESERVATION_ID = "reservation_id";
    //    settings
    public static final String PARAM_NOTIFI = "notify";
    public static final String PARAM_NOTIFI_FAVOURITE = "notify_favourite";
    public static final String PARAM_NOTIFI_TRANSPORT = "notify_transport";
    public static final String PARAM_NOTIFI_FOOD = "notify_food";
    public static final String PARAM_NOTIFI_LABOR = "notify_labor";
    public static final String PARAM_NOTIFI_TRAVEL = "notify_travel";
    public static final String PARAM_NOTIFI_SHOPPING = "notify_shopping";
    public static final String PARAM_NOTIFI_NEW_AND_EVENT = "notify_news";
    public static final String PARAM_NOTIFI_NEARBY = "notify_nearby";
    //    Transaction
    public static final String PARAM_AMOUNT = "amount";
    public static final String PARAM_NOTE = "note";
    public static final String PARAM_METHOD_PAYMENT = "payment_method";
    public static final String PARAM_DESTINATION_EMAIL = "destination_email";
    public static final String PARAM_ID_TRANSACTION = "transaction_id";

    public static final String PARAM_TRIP_ID = "trip_id";
    public static final String PARAM_PASSENGER_ID = "passenger_id";
    public static final String PARAM_DRIVER_ID = "driver_id";
    public static final String PARAM_SEAT_COUNT = "seat_count";
    public static final String PARAM_START_LOCATION = "start_location";
    public static final String PARAM_END_LOCATION = "end_location";
    public static final String PARAM_TRANSACTION_ID = "transaction_id";
    public static final String PARAM_ACTUAL_FARE = "actual_fare";
    public static final String PARAM_ESTIMATE_FARE = "estimate_fare";
    public static final String PARAM_MODE = "mode";
    public static final String PARAM_FRIEND_ID = "friend_id";
    public static final String PARAM_LATLNG = "latlng";
    public static final String PARAM_SENSOR = "sensor";
    public static final String PARAM_RESTAURANT_ID = "restaurant_id";
    public static final String PARAM_BILLING_NAME = "billingName";
    public static final String PARAM_BILLING_ADDRESS = "billingAddress";
    public static final String PARAM_BILLING_PHONE = "billingPhone";
    public static final String PARAM_BILLING_EMAIL = "billingEmail";
    public static final String PARAM_VAT = "vat";
    public static final String PARAM_ID = "id";
    public static final String PARAM_FBCLID = "fbclid";



    public static final String PARAM_VAHICLE_ID = "vehicle_id";
    public static final String PARAM_VAHICLE_MODEL_ID = "vehicle_model_id";


    public static final String PARAM_SUB_ID = "sub_id";
    public static final String PARAM_SELLER_NAME = "seller_name";
    public static final String PARAM_SELLER_ID = "seller_id";
    public static final String PARAM_CREATE_RENT = "create_rent";
    public static final String PARAM_EXPIRE_RENT = "expire_rent";



    public static final String PARAM_BILLNG_NAME = "billingName";
    public static final String PARAM_BILLNG_PHONE = "billingPhone";
    public static final String PARAM_BILLNG_ADDRESS = "billingAddress";
    public static final String PARAM_TOTAL = "total";
    public static final String PARAM_ORDER_ID = "trip_id";
    public static final String PARAM_ITEMS = "items";
    public static final String PARAM_PAYMENT_METHOD = "paymentMethod";


    public static final String PARAM_SUBCRIBE_ID = "subscriber_id";
    public static final String PARAM_SUBCRIBED_ID = "subscribed_id";


    public static final String PARAM_FULLNAME = "fullname";
    public static final String PARAM_MESSAGE = "message";

    public static final String NUMBER_PER_PAGE = "number_per_page";
    public static final String PARAM_PROVIDER_ID = "provider_id";
    public static final String PARAM_TIME = "time";
    public static final String PARAM_AUTHOR_ID = "author_id";
    public static final String PARAM_CURRENT_PASSWORD = "current_password";
    public static final String PARAM_NEW_PASSWORD = "new_password";
    public static final String PARAM_ALBUM_SIZE = "albums_size";

}
