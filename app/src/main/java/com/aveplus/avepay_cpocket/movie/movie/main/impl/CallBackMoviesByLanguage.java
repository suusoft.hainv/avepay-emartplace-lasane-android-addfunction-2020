package com.aveplus.avepay_cpocket.movie.movie.main.impl;


import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;

import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface CallBackMoviesByLanguage {
    public void success(List<Movie> movies);
}
