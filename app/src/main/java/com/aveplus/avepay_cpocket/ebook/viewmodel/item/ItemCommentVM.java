package com.aveplus.avepay_cpocket.ebook.viewmodel.item;

import android.content.Context;
import android.view.View;


import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.ebook.listener.IOnItemClickedListener1;
import com.aveplus.avepay_cpocket.ebook.model.Comment;
import com.aveplus.avepay_cpocket.movie.movie.listener.ISetDataListener;

import java.util.Date;


public class ItemCommentVM extends BaseAdapterVM implements IOnItemClickedListener1,
        ISetDataListener {

    private Comment comment;
    private Date specDate;

    public ItemCommentVM(Context context, Object comment, int position) {
        super(context);
        this.comment = (Comment) comment;
    }

    public String getName() {
        return comment.getName();
    }

    public String getContent() {
        return comment.getContent();
    }


    public String getTime() {
        return comment.getDatetime();
    }

    @Override
    public void onItemClicked(View view) {

    }

    @Override
    public void setData(Object object) {
        this.comment = (Comment) object;
        notifyChange();
    }
}
