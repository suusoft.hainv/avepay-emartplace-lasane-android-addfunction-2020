package com.aveplus.avepay_cpocket.ebook.viewmodel.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;


/**
 * Created by Suusoft on 9/12/2017.
 */
public class AboutVM extends BaseViewModel {


    @Override
    public void getData(int page) {

    }

    public AboutVM(Context self) {
        super(self);
    }

    public String getImage() {
        return "";
    }

    public String getTitle() {
        return self.getString(R.string.about_title);
    }

    public String getDescription() {
        return self.getString(R.string.about_content1) + "\n" + self.getString(R.string.about_content2);
    }

    public void onClickFace(View view) {
        Intent link = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com"));
        self.startActivity(link);
    }

    public void onClickGoogle(View view) {
        Intent link = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com"));
        self.startActivity(link);
    }
}
