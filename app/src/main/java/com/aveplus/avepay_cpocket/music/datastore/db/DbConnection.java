package com.aveplus.avepay_cpocket.music.datastore.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.model.Playlist;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.model.Video;
import com.aveplus.avepay_cpocket.music.util.DateTimeUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Class connects to Database, get all data here
 */
public class DbConnection implements IDatabaseConfig {

	private Context mContext;
	private DataBaseHelper mDBHelper;

	public DbConnection(Context mContext) {
		this.mContext = mContext;
		this.mDBHelper = new DataBaseHelper(this.mContext, DATABASE_NAME);
	}

	/**
	 * close database
	 */
	private void close() {
		mDBHelper.close();
	}

	/**
	 * open and connect to database
	 */
	private void openAndConnectDB() {
		try {
			mDBHelper.createDataBase();
			mDBHelper.openDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void closeDB() {
		this.mDBHelper.close();
	}

	private void execSQL(String sqlQuery) {
		mDBHelper.getmDatabase().execSQL(sqlQuery);
	}

	/**
	 * **************************************************************** Caching
	 */
	public String getCaching(String request){
		this.openAndConnectDB();
		String response = "";
		String query = "SELECT * FROM " + TABLE_CACHING + " WHERE "
				+ Constant.Caching.KEY_REQUEST + " = '%s' " ;
		Cursor mCursor = this.mDBHelper.rawQuery(String.format(query,request));
		if (mCursor.moveToFirst()){
			response = mCursor.getString(2);
		}

		return response;
	}

	public boolean saveCaching(String request, String response, String timeUpdated){
		synchronized (this) {
			openAndConnectDB();

			ContentValues contentValues = new ContentValues();
			contentValues.put(Constant.Caching.KEY_REQUEST, request);
			contentValues.put(Constant.Caching.KEY_RESPONSE, response);
			contentValues.put(Constant.Caching.KEY_TIME_UPDATED, timeUpdated);

			long result = mDBHelper.getmDatabase().insertWithOnConflict(TABLE_CACHING, null,
					contentValues, SQLiteDatabase.CONFLICT_REPLACE);
			closeDB();
			return (result >= 0) ? true : false;
		}
	}

	public HashMap<String, String> getAllCachingTime(){
		this.openAndConnectDB();
		HashMap<String, String> cachingTimes = new HashMap<>();
		String response = "";
		String query = "SELECT * FROM " + TABLE_CACHING ;
		Cursor mCursor = this.mDBHelper.rawQuery(query);
		if (mCursor.moveToFirst()){
			do {
				cachingTimes.put(mCursor.getString(1),mCursor.getString(3));
			}while (mCursor.moveToNext());
		}

		return cachingTimes;
	}

	/**
	 *
	 * set time updated = 0
	 * @param request is url
	 *
	 */
	public synchronized boolean resetCachingTime(String request){
		openAndConnectDB();
		String whereClause = Constant.Caching.KEY_REQUEST + " = '"+ request +"'";
		ContentValues values = new ContentValues();
		values.put(Constant.Caching.KEY_TIME_UPDATED,"0");
		long result = mDBHelper.update(TABLE_CACHING,values,whereClause,null);
		closeDB();
		return (result >= 0) ? true : false;
	}

	/**
	 *
	 * Favorite
	 *
	 */
	public ArrayList<Song> getFavourites(){
		this.openAndConnectDB();
		ArrayList<Song> list = new ArrayList<>();
		String query = "SELECT * FROM " + TABLE_FAVORITE ;
		Cursor mCursor = this.mDBHelper.rawQuery(query);
		if (mCursor.moveToFirst()){
			Song video;
			do {
				video = new Song();
				video.title = mCursor.getString(1)+"";
				video.description = mCursor.getString(2)+"";
				video.image = mCursor.getString(3)+"";
				video.nameSinger = mCursor.getString(6)+"";
				video.id = mCursor.getString(8)+"";
				video.song_file = mCursor.getString(9)+"";
				list.add(video);

			}while (mCursor.moveToNext());
		}
		mCursor.close();
		return list;
	}

	public boolean saveFavorited(Song item){
		synchronized (this) {
			openAndConnectDB();

			ContentValues contentValues = new ContentValues();
			contentValues.put("item_id", item.id);
			contentValues.put("title", item.title);
			contentValues.put("description", item.description);
			contentValues.put("image", item.image);
			contentValues.put("singer", item.getNameSinger());
			if(item.song_file.endsWith(".mp3") || item.song_file.endsWith(".wav")){
				contentValues.put("src", item.song_file);
			}else {
				contentValues.put("src", item.song_url);
			}


			long result = mDBHelper.getmDatabase().insertWithOnConflict(TABLE_FAVORITE, null,
					contentValues, SQLiteDatabase.CONFLICT_REPLACE);
			closeDB();
			return (result >= 0) ? true : false;
		}
	}

	public boolean isFavorited(String id){
		synchronized (this) {
			this.openAndConnectDB();
			String query = "SELECT * FROM " + TABLE_FAVORITE + " WHERE item_id = '" + id + "'";
			Cursor mCursor = this.mDBHelper.rawQuery(query);
			if (mCursor.getCount() <= 0) {
				mCursor.close();
				closeDB();
				return false;
			}
			mCursor.close();
			closeDB();
			return true;
		}
	}
	public void unFavorite(String id){
		synchronized (this) {
			this.openAndConnectDB();
			String query = "DELETE FROM " + TABLE_FAVORITE + " WHERE item_id = '" + id + "'";
			Cursor mCursor = this.mDBHelper.rawQuery(query);
			if (mCursor.getCount() == 1) {
				Log.e("deleteHistory", "id: " + id);
			}

			mCursor.close();
			closeDB();
		}

	}

	public void clearFavorites(){
		synchronized (this) {
			this.openAndConnectDB();
			String query = "DELETE FROM " + TABLE_FAVORITE;
			Cursor mCursor = this.mDBHelper.rawQuery(query);
			if (mCursor.getCount() <= 0) {
				Log.d("deleteHistory", "fail ");
			}

			mCursor.close();
			closeDB();
		}

	}


	// my list
	/**
	 *
	 * History
	 *
	 */
	public ArrayList<Playlist> getMyList(){
		synchronized (this) {
			this.openAndConnectDB();
			ArrayList<Playlist> list = new ArrayList<>();
			String query = "SELECT * FROM " + TABLE_MYLIST;
			Cursor mCursor = this.mDBHelper.rawQuery(query);
			if (mCursor.moveToFirst()) {
				Playlist video;
				do {
					video = new Playlist();
					video.id = mCursor.getString(1) + "";
					video.title = mCursor.getString(2) + "";
					video.description = mCursor.getString(3) + "";
					video.image = mCursor.getString(4) + "";
					video.count = mCursor.getInt(5);
					video.dateTime = mCursor.getString(6);
					video.type = mCursor.getInt(7);
					video.duration = mCursor.getString(8);

					list.add(video);

				} while (mCursor.moveToNext());
			}
			mCursor.close();
			closeDB();
			return list;
		}
	}

	public boolean saveMyList(Playlist item){
		synchronized (this) {
			this.openAndConnectDB();
			ContentValues contentValues = new ContentValues();
			contentValues.put("tid", item.id);
			contentValues.put("title", item.title);
			contentValues.put("description", item.description);
			contentValues.put("image", item.image);
			contentValues.put("count", item.count);
			contentValues.put("type", 1);
			contentValues.put("time", DateTimeUtility.getTimeStamp());
			contentValues.put("duration", item.duration);
			long result = mDBHelper.getmDatabase().insertWithOnConflict(TABLE_MYLIST, null,
					contentValues, SQLiteDatabase.CONFLICT_REPLACE);
			closeDB();
			return (result >= 0) ? true : false;
		}
	}

	public boolean saveMyList(Video item){
		synchronized (this) {
			this.openAndConnectDB();
			ContentValues contentValues = new ContentValues();
			contentValues.put("tid", item.id);
			contentValues.put("title", item.title);
			contentValues.put("description", item.description);
			contentValues.put("image", item.image);
			contentValues.put("count", 1);
			contentValues.put("type", 0);
			contentValues.put("time", DateTimeUtility.getTimeStamp());
			contentValues.put("duration", item.duration);
			long result = mDBHelper.getmDatabase().insertWithOnConflict(TABLE_MYLIST, null,
					contentValues, SQLiteDatabase.CONFLICT_REPLACE);

			closeDB();
			return (result >= 0) ? true : false;
		}
	}

	public boolean isSavedMyList(String id){
		synchronized (this) {
			this.openAndConnectDB();
			String query = "SELECT * FROM " + TABLE_MYLIST + " WHERE tid = '" + id + "'";
			Cursor mCursor = this.mDBHelper.rawQuery(query);
			if (mCursor.getCount() <= 0) {
				mCursor.close();
				closeDB();
				return false;
			}
			mCursor.close();
			closeDB();
			return true;
		}
	}

	public void deleteMyList(String id){
		synchronized (this) {
			this.openAndConnectDB();
			String query = "DELETE FROM " + TABLE_MYLIST + " WHERE tid = '" + id + "'";
			Cursor mCursor = this.mDBHelper.rawQuery(query);
			if (mCursor.getCount() == 1) {
				Log.d("deleteHistory", "id: " + id);
			}

			mCursor.close();
			closeDB();
		}

	}

	public void deleteAllMyList(){
		synchronized (this) {
			this.openAndConnectDB();
			String query = "DELETE FROM " + TABLE_MYLIST;
			Cursor mCursor = this.mDBHelper.rawQuery(query);
			if (mCursor.getCount() <= 0) {
				Log.e("deleteHistory", "fail ");
			}

			mCursor.close();
			closeDB();
		}

	}

}
