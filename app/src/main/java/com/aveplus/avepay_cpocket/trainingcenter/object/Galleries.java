package com.aveplus.avepay_cpocket.trainingcenter.object;

public class Galleries {
	private String url;

	public Galleries(String url) {
		super();
		this.url = url;
	}

	public Galleries() {
		// TODO Auto-generated constructor stub
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
