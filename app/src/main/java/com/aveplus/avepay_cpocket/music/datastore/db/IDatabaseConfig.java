package com.aveplus.avepay_cpocket.music.datastore.db;

public interface IDatabaseConfig {

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "db.sqlite";
	public static final String TABLE_CACHING = "caching";
	public static final String TABLE_FAVORITE = "favorite";
	public static final String TABLE_HISTORY = "history";
	public static final String TABLE_MYLIST = "mylist";



}
