package com.aveplus.avepay_cpocket.taxi.view.instant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.interfaces.ITransport;
import com.aveplus.avepay_cpocket.taxi.interfaces.IVehicle;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.book.ServiceObj;
import com.aveplus.avepay_cpocket.taxi.view.adapters.Transport1Adapter;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class BottomSheetFragment extends BottomSheetDialogFragment {

    private Transport1Adapter transport1Adapter;
    private RecyclerView rcyVehicle;
    private View view;
    private TripObj tripObj;
    private IVehicle iVehicle;

    public BottomSheetFragment(IVehicle iVehicle) {
        this.iVehicle = iVehicle;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bootom_sheet, container, false);
        dishplayListService();
        return view;
    }

    private void dishplayListService() {
        rcyVehicle = view.findViewById(R.id.rcy_vehicle1);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rcyVehicle.setLayoutManager(layoutManager);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rcyVehicle.getContext(),
                layoutManager.getOrientation());
        rcyVehicle.addItemDecoration(mDividerItemDecoration);
        rcyVehicle.setHasFixedSize(true);

        transport1Adapter = new Transport1Adapter(getContext(), ServiceObj.getListService(), new ITransport() {
            @Override
            public void onTransportSelected(ServiceObj serviceObj) {
                iVehicle.onService(serviceObj);
                dismiss();
            }
        });
        rcyVehicle.setAdapter(transport1Adapter);
    }
}
