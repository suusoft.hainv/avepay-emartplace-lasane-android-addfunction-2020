package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.configs.SizeScreen;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.utils.DialogUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;



/**
 * Created by Suusoft on 01/03/2018.
 */

public class SplashActivity extends Activity {

    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_taxi);
        SizeScreen.init(this);

        onNext();


    }

    private void onNext() {
        // Get token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "getInstanceId failed", task.getException());
                            showDialogMessage();
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        //String msg = getString(R.string.msg_token_fmt, token);
                        Log.e(TAG, token);
                        //Toast.makeText(SplashActivity.this, token, Toast.LENGTH_SHORT).show();
                        GlobalFunctions.saveFCMToken(SplashActivity.this, token);
                        onContinue();
                    }
                });


    }

    private void showDialogMessage() {
        DialogUtil.showAlertDialog(this, R.string.error, R.string.error_receive_token, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                finish();
            }
        });
    }

    private void onContinue() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;

                if (DataStoreManager.getUser()!=null){
                    intent = new Intent(SplashActivity.this, SplashLoginActivity.class);

                }else {
                    intent = new Intent(SplashActivity.this, StartScreenActivity.class);
                }
                startActivity(intent);
                finish();

            }
        }, 2000);
    }



}
