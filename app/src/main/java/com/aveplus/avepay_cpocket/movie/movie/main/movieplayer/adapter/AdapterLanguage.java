package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public class AdapterLanguage extends ArrayAdapter<String> {

    private int mResId;

    public AdapterLanguage(Context context, int resource) {
        super(context, resource);

        mResId = resource;
    }

    public AdapterLanguage(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);

        mResId = resource;
    }

    public AdapterLanguage(Context context, int resource, String[] objects) {
        super(context, resource, objects);

        mResId = resource;
    }

    public AdapterLanguage(Context context, int resource, int textViewResourceId, String[] objects) {
        super(context, resource, textViewResourceId, objects);

        mResId = resource;
    }

    public AdapterLanguage(Context context, int resource, List<String> objects) {
        super(context, resource, objects);

        mResId = resource;
    }

    public AdapterLanguage(Context context, int resource, int textViewResourceId, List<String> objects) {
        super(context, resource, textViewResourceId, objects);

        mResId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            Context context = getContext();
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(mResId, null);
            if (view instanceof TextView) {
                TextView txtView = (TextView) view;
                txtView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/CaviarDreams_Bold.ttf"));
            }
        }
        return super.getView(position, view, parent);
    }
}
