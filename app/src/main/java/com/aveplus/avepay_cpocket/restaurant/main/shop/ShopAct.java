package com.aveplus.avepay_cpocket.restaurant.main.shop;

import android.view.View;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;


public class ShopAct extends BaseActivity implements View.OnClickListener {
    private TextView btnCancel;
    private TextView btnOk;

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_shop;
    }

    @Override
    protected void initView() {
//        setToolbarTitle(R.string.open_my_shop);

        btnCancel = findViewById(R.id.btn_cancel);
        btnOk = findViewById(R.id.btn_ok);

        btnCancel.setOnClickListener(this);
        btnOk.setOnClickListener(this);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_ok:
                AppUtil.goToWeb(self, "https://bazaaramadan.com/shop/backend/web/index.php");

//                startActivity(new Intent(self, RegisterShopAct.class));
                break;
        }
    }
}
