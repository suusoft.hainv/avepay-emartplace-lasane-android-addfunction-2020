package com.aveplus.avepay_cpocket.taxi.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.aveplus.avepay_cpocket.taxi.retrofit.APIService;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment {

    protected AppCompatActivity self;
    protected APIService mAPIService;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        self = (AppCompatActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflateLayout(inflater, container, savedInstanceState);
        mAPIService = ApiUtils.getAPIService();
        initUI(view);
        initControl();

        return view;
    }

    abstract View inflateLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    abstract void initUI(View view);

    abstract void initControl();


}
