package com.aveplus.avepay_cpocket.ebook.model;

import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.google.gson.annotations.SerializedName;


import java.io.File;
import java.util.List;

/**
 * Created by Suusoft on 9/13/2017.
 */
public class Chapter implements Parcelable {

    public static final String TYPE_PDF = "pdf";
    public static final String TYPE_EPUB = "epub";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_MP3 = "audio";
    public static final String TYPE_NORMAL = "normal";

    private String id;
    @SerializedName("book_id")
    private String bookId;
    @SerializedName("content")
    private String description;
    private String title;
    private String bookMarkIndex;
    private String image;
    private String type;
    private String time;
    private String chapterIndex;
    private String attachment;
    private String directURL;
    private List<Actor> actor;

    private String sdcardURL;
    private String sdcardFolder;

    private int pageBookmark;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBookMarkIndex() {
        return bookMarkIndex;
    }

    public void setBookMarkIndex(String bookMarkIndex) {
        this.bookMarkIndex = bookMarkIndex;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getChapterIndex() {
        return chapterIndex;
    }

    public void setChapterIndex(String chapterIndex) {
        this.chapterIndex = chapterIndex;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getDirectURL() {
        return directURL;
    }

    public void setDirectURL(String directURL) {
        this.directURL = directURL;
    }

    public List<Actor> getActor() {
        return actor;
    }

    public void setActor(List<Actor> actor) {
        this.actor = actor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPageBookmark() {
        return pageBookmark;
    }

    public void setPageBookmark(int pageBookmark) {
        this.pageBookmark = pageBookmark;
    }

    private File folderFile;

    public String getSdcardFolder() {
        File ebookFolder = new File(Environment.getExternalStorageDirectory() + Constant.FOLDER_EBOOK);
       if (getType().equals(TYPE_PDF)) {
            folderFile = new File(Environment.getExternalStorageDirectory() + Constant.FOLDER_PDF);
        } else if (getType().equals(TYPE_EPUB + "")) {
            folderFile = new File(Environment.getExternalStorageDirectory() + Constant.FOLDER_EPUB);
        } else {
            return "";
        }

        if (!folderFile.exists()) {
            ebookFolder.mkdir();
            folderFile.mkdir();
        }
        Log.e("rrr", "" + folderFile.toString());
        return folderFile.toString();
    }

    public String getSdcardURL() {
        String duoi = "";
        if (getType().equals(TYPE_PDF)) {
            duoi = ".pdf";
        } else if (getType().equals(TYPE_EPUB)) {
            duoi = ".epub";
        }
        String filePath = getSdcardFolder() + "/" + getBookId() + getId() + (duoi);
        Log.e("Files", "save:" + filePath);
        DataStoreManager.saveInfoChapterInPath(this,filePath );
        return filePath;
     }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.bookId);
        dest.writeString(this.description);
        dest.writeString(this.title);
        dest.writeString(this.bookMarkIndex);
        dest.writeString(this.image);
        dest.writeString(this.type);
        dest.writeString(this.time);
        dest.writeString(this.chapterIndex);
        dest.writeString(this.attachment);
        dest.writeString(this.directURL);
        dest.writeTypedList(this.actor);
        dest.writeString(this.sdcardURL);
        dest.writeString(this.sdcardFolder);
        dest.writeSerializable(this.folderFile);
    }

    public Chapter() {
    }

    protected Chapter(Parcel in) {
        this.id = in.readString();
        this.bookId = in.readString();
        this.description = in.readString();
        this.title = in.readString();
        this.bookMarkIndex = in.readString();
        this.image = in.readString();
        this.type = in.readString();
        this.time = in.readString();
        this.chapterIndex = in.readString();
        this.attachment = in.readString();
        this.directURL = in.readString();
        this.actor = in.createTypedArrayList(Actor.CREATOR);
        this.sdcardURL = in.readString();
        this.sdcardFolder = in.readString();
        this.folderFile = (File) in.readSerializable();
    }

    public static final Creator<Chapter> CREATOR = new Creator<Chapter>() {
        @Override
        public Chapter createFromParcel(Parcel source) {
            return new Chapter(source);
        }

        @Override
        public Chapter[] newArray(int size) {
            return new Chapter[size];
        }
    };
}
