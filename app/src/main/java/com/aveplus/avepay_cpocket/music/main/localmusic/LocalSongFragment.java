package com.aveplus.avepay_cpocket.music.main.localmusic;

import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.main.downloaded.ItemDownloadedFileVM;
import com.aveplus.avepay_cpocket.music.network.ListenerLoading;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class LocalSongFragment extends BaseListFragmentBinding implements ListenerLoading {

    LocalSongVM viewModel;

    public static LocalSongFragment newInstance() {

        Bundle args = new Bundle();

        LocalSongFragment fragment = new LocalSongFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new LocalSongVM(self,this);
        return viewModel;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        SingleAdapter singleAdapter = new SingleAdapter(self, R.layout.item_song, viewModel.getListData() , ItemDownloadedFileVM.class, viewModel.getActionListener());
        recyclerView.setAdapter(singleAdapter);

        viewModel.getData(1);

    }

    @Override
    protected void initialize() {

    }


    @Override
    public void onLoadingIsProcessing() {
        showProgress(true);
    }

    @Override
    public void onLoadingIsCompleted() {
        showProgress(false);

    }
}
