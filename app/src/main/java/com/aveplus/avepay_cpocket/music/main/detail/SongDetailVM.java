package com.aveplus.avepay_cpocket.music.main.detail;

import android.content.Context;
import android.view.View;

import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.model.Song;


/**
 * Created by phamv on 8/20/2017.
 */

public class SongDetailVM extends BaseViewModel {

    private Song item;


    public String getTitle(){
        return item.title;
    }

    public String getImage(){
        return item.image;
    }

    public String getDescription(){
        return item.description;
    }
    public String getReleaseDate(){
        return item.release_date;
    }

    public String getSinger(){
        return item.getNameSinger();
    }

    public String getAlbum(){
        return item.nameAlbum;
    }

    public String getThumbBlur(){
        return "";
    }

    public String getThumb(){
        return item.image;
    }

    public int isVisibleAvatar(){
        return View.GONE;
    }


    public SongDetailVM(Context self, Song item) {
        super(self);
        this.item = item;
    }

}
