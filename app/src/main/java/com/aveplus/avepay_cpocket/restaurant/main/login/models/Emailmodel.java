package com.aveplus.avepay_cpocket.restaurant.main.login.models;

import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;

/**
 * Created by suusoft.com on 11/12/19.
 */

public class Emailmodel extends BaseModel {
    private String email;

    public Emailmodel(String email) {
        this.email = email;
    }

    public Emailmodel() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
