package com.aveplus.avepay_cpocket.movie.movie.model;

import com.aveplus.avepay_cpocket.movie.movie.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MovieNewModel extends BaseModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("sub_title")
    @Expose
    private String subTitle;
    @SerializedName("release_year")
    @Expose
    private Object releaseYear;
    @SerializedName("rating")
    @Expose
    private Object rating;
    @SerializedName("runtime")
    @Expose
    private Object runtime;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("source_id")
    @Expose
    private Integer sourceId;
    @SerializedName("play_with")
    @Expose
    private Integer playWith;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("modified_at")
    @Expose
    private Object modifiedAt;
    @SerializedName("listSub")
    @Expose
    private ArrayList<listSub> listSub = null;

    public MovieNewModel() {

    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public Object getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Object releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Object getRuntime() {
        return runtime;
    }

    public void setRuntime(Object runtime) {
        this.runtime = runtime;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getPlayWith() {
        return playWith;
    }

    public void setPlayWith(Integer playWith) {
        this.playWith = playWith;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Object modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public ArrayList<listSub> getListSub() {
        return listSub;
    }
}
