package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.Intent;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.view.fragment.UpdateInfoUserFragment;


public class UpdateInfoUserActivity extends BaseActivity implements UpdateInfoUserFragment.IUpdateSuccess {

    private UpdateInfoUserFragment fragment;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NORMAL;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_contain_fragment;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void onViewCreated() {
        setToolbarTitle(R.string.update_info);

        fragment = UpdateInfoUserFragment.newInstance(this);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fr_content, fragment).commit();

    }

    @Override
    public void onUpdated() {
        // send post close screen login

        finish();
    }
}
