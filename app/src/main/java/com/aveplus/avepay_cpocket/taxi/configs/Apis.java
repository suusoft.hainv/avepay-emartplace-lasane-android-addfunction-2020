package com.aveplus.avepay_cpocket.taxi.configs;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

/**
 * Created by SuuSoft.com on 06/22/2016.
 */
public class Apis {
    public static final int REQUEST_TIME_OUT = 15000;
    public static final String URL_GET_FACEBOOK_INFO = "https://graph.facebook.com/me";
    public static final String URL_GET_ADDRESS_BY_LATLNG = "http://maps.googleapis.com/maps/api/geocode/json";
    private static final String APP_DOMAIN = AppController.getInstance().getString(R.string.URL_API_TAXI_TYPE) + "backend/web/index.php/api/";
    public static final String URL_REGISTER_DEVICE = APP_DOMAIN + "device";
    public static final String URL_FAVORITE = APP_DOMAIN + "favourite";
    //review
    public static final String URL_POST_REVIEW = APP_DOMAIN + "review";
    public static final String URL_GET_REVIEW = APP_DOMAIN + "review/list";
    private static final String USER = "user/";
    public static final String URL_REGISTER_NORMAL_ACCOUNT = APP_DOMAIN + USER + "register";
    public static final String URL_LOGIN = APP_DOMAIN + USER + "login";
    public static final String URL_LOGOUT = APP_DOMAIN + USER + "logout";
    // profile
    public static final String URL_UPDATE_PROFILE_NORMAL = APP_DOMAIN + USER + "update-profile";
    public static final String URL_UPDATE_PROFILE_PRO = APP_DOMAIN + USER + "update-pro-profile";
    public static final String URL_PROFILE = APP_DOMAIN + USER + "profile";
    public static final String URL_CHANGE_PASS_WORD = APP_DOMAIN + USER + "change-password";
    public static final String URL_GET_PROFILE_DRIVER = APP_DOMAIN + USER + "profile";
    public static final String URL_FORGET_PASSWORD = APP_DOMAIN + USER + "forget-password";
    //    settings
    public static final String URL_SETTINGS = APP_DOMAIN + USER + "setting";
    private static final String DEAL = "deal/";
    // deal
    public static final String URL_DEAL_ACTION = APP_DOMAIN + DEAL;
    public static final String URL_GET_DEAL_LIST = APP_DOMAIN + "shopping/" + DEAL + "list";
    public static final String URL_GET_DEAL_DETAIL = APP_DOMAIN + DEAL + "detail";
    public static final String URL_ACTIVATE_DEAL = APP_DOMAIN + DEAL + "switch";
    public static final String URL_UPDATE_DEAL_DURATION = APP_DOMAIN + DEAL + "update";
    private static final String TRANSPORT = "transport/";
    // Transport
    public static final String URL_GET_NEARBY_DRIVERS = APP_DOMAIN + TRANSPORT + "driver-list";
    public static final String URL_FIND_DRIVER = APP_DOMAIN + "taxi/"+TRANSPORT + "find-driver";
    public static final String URL_TRIP_PRICE = APP_DOMAIN + "taxi/" + TRANSPORT + "trip-price";
    public static final String URL_ACTIVATE_DRIVER_MODE = APP_DOMAIN + TRANSPORT + "online";
    public static final String URL_GET_TRIP_HISTORY = APP_DOMAIN + TRANSPORT + "trip-list";
    public static final String URL_TRIP_ACTION = APP_DOMAIN + TRANSPORT + "trip";
    public static final String URL_DELETE_ALL_TRIP = APP_DOMAIN + TRANSPORT + "trip-bulk-delete";
    public static final String URL_TRIP_STATUS = APP_DOMAIN + TRANSPORT + "trip-status";
    public static final String URL_TRIP_HISTORY = APP_DOMAIN + TRANSPORT + "trip-history";
    //parter restaurant
    public static final String URL_FIND_RESTAURANT = APP_DOMAIN + TRANSPORT + "find-restaurant";
    public static final String URL_RESTAURANT_PRODUCT = APP_DOMAIN + TRANSPORT + "restaurant-products";
    public static final String URL_RESTAURANT_ORDER = APP_DOMAIN + TRANSPORT + "order";
    public static final String URL_ORDER_BY_TRIP = APP_DOMAIN + TRANSPORT + "order-by-trip";
    public static final String URL_ORDER_HISTORY = APP_DOMAIN + TRANSPORT + "order-history";
    public static final String URL_ORDER_HISTORY_DETAIL = APP_DOMAIN + TRANSPORT + "order-history-detail";
    // instant booking
    public static final String URL_TRIP_DETAIL = APP_DOMAIN + TRANSPORT + "trip-detail";
    public static final String URL_TRIP_INSTANT_ACTION = APP_DOMAIN + TRANSPORT + "trip-instant";
    //Deal trip
    public static final String URL_TRIP_DEAL = APP_DOMAIN + TRANSPORT + "trip-deal";
    public static final String URL_GET_DRIVER_LOCATION = APP_DOMAIN + TRANSPORT + "track-driver";
    private static final String RESERVATION = "reservation";
    public static final String URL_RESERVATION_ACTION = APP_DOMAIN + RESERVATION;
    public static final String URL_GET_RESERVATION = APP_DOMAIN + RESERVATION + "/list";
    private static final String TRANSACTION = "transaction";
    //    transaction
    public static final String URL_TRANSACTION = APP_DOMAIN + TRANSACTION;
    public static final String URL_HISTORY = APP_DOMAIN + TRANSACTION + "/list";
    public static final String URL_DELETE_HISTORY = APP_DOMAIN + TRANSACTION + "/delete";
    public static final String URL_GET_ACCOUNTING = APP_DOMAIN + TRANSACTION + "/accounting";
    private static final String UTILITY = "utility";
    //    utitlity
    public static final String URL_SETTING_UTILITY = APP_DOMAIN + UTILITY + "/setting";
    public static final String URL_UPDATE_LOCATION = APP_DOMAIN + UTILITY + "/update-location";
    private static final String CONTACT = "friend";
    public static final String URL_GET_CONTACTS = APP_DOMAIN + CONTACT + "/list";
    public static final String URL_CONTACTS_ACTION = APP_DOMAIN + CONTACT;
}
