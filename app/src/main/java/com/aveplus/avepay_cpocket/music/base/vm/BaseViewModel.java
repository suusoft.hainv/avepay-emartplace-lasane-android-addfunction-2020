package com.aveplus.avepay_cpocket.music.base.vm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.databinding.BaseObservable;

import com.aveplus.avepay_cpocket.music.network.ListenerLoading;


/**
 * "Copyright © 2019 SUUSOFT"
 */
public abstract class BaseViewModel extends BaseObservable {

    public Context self;
    protected Bundle bundle;
    private boolean isVisibleToUser;

    protected ListenerLoading listenerLoading;

    public BaseViewModel(Context self) {
        this.self = self;
    }

    public BaseViewModel(Context self, Bundle bundle) {
        this(self);
        this.bundle = bundle;
    }

    public void getData(int page){}
    public void onActivityResult(int requestCode, int resultCode, Intent data) {}
    public void onOptionsItemSelected(MenuItem item){}
    public void onResume(){}
    public void onPause(){}
    public void onStop(){}
    public void onDestroyView(){}
    public void onDestroy(){}

    // for menu action
    public boolean isHasMenuOption(){ return false;}
    public int getMenuLayout(){return  0;}


    public boolean isVisibleToUser() {
        return isVisibleToUser;
    }

    public void setVisibleToUser(boolean visibleToUser) {
        isVisibleToUser = visibleToUser;
    }


    public ListenerLoading getListenerLoading() {
        return listenerLoading;
    }

    public void setListenerLoading(ListenerLoading listenerLoading) {
        this.listenerLoading = listenerLoading;
    }
}
