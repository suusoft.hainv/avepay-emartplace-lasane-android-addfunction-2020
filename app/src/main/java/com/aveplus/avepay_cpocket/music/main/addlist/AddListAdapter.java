package com.aveplus.avepay_cpocket.music.main.addlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.player.QueueManager;

import java.util.ArrayList;

public class AddListAdapter extends RecyclerView.Adapter<AddListAdapter.AddListVH> {
    private static final String TAG = AddListAdapter.class.getSimpleName();
    private ArrayList<AddListObj> listObjs;
    private Context context;

    public AddListAdapter(Context context, ArrayList<AddListObj> listObjs) {
        this.listObjs = listObjs;
        this.context = context;
    }

    @NonNull
    @Override
    public AddListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_playlist, parent, false);
        return new AddListVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddListVH holder, int position) {
        AddListObj addListObj = listObjs.get(position);
        holder.tvName.setText(addListObj.getAddListName());
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listObjs.get(position).getListVideos().add(QueueManager.getInstance().getCurrentItem());
                Myplaylist myplaylist = new Myplaylist(listObjs);
                DataStoreManager.saveMyPlaylist(myplaylist);
                Toast.makeText(context, "Added to " + addListObj.getAddListName(), Toast.LENGTH_SHORT).show();
                //Log.e(TAG, "onClick: " + new Gson().toJson(addListObj.getListVideos()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listObjs != null ? listObjs.size() : 0;
    }

    public class AddListVH extends RecyclerView.ViewHolder {
        TextView tvName;

        public AddListVH(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_playlist_name);
        }
    }
}
