package com.aveplus.avepay_cpocket.interfaces;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface CallBackNewMovies {
    public void LoadSuccess(boolean success);
}
