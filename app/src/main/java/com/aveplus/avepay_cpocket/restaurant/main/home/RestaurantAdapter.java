package com.aveplus.avepay_cpocket.restaurant.main.home;
/**
 * Created by suusoft.com on 11/12/17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class RestaurantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<RestaurantModel> restaurantList;
    private final OnItemClickListener listener;

    public RestaurantAdapter(Context context, List<RestaurantModel> restaurantList, OnItemClickListener listener) {
        this.context = context;
        this.restaurantList = restaurantList;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(RestaurantModel restaurantModel);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from (context).inflate (R.layout.row_product, parent, false);
        return new ItemRestaurant (view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        RestaurantModel rs = restaurantList.get (i);
        ((ItemRestaurant) viewHolder).Container (rs);
    }

    @Override
    public int getItemCount() {
        if (restaurantList != null)
            return restaurantList.size ();
        else return 0;
    }

    private class ItemRestaurant extends RecyclerView.ViewHolder {
        private ImageView imgRestaurant;
        private TextView tvAddress;
        private TextView tvNameRestaurant;



        private ItemRestaurant(View itemView) {
            super (itemView);imgRestaurant = itemView.findViewById(R.id.img_restaurant);
            tvAddress = itemView.findViewById(R.id.tv_address);
            tvNameRestaurant = itemView.findViewById(R.id.tv_name_restaurant);
        }

        void Container(final RestaurantModel restaurantModel) {
            Glide.with (context)
                    .load (restaurantModel.getImage ())
                    .into (imgRestaurant);
            tvNameRestaurant.setText (restaurantModel.getName ());
            tvAddress.setText (restaurantModel.getAddress ());
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    listener.onItemClick (restaurantModel);
                }
            });

        }
    }
}
