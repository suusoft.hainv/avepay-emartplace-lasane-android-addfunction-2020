package com.aveplus.avepay_cpocket.music.model;

import com.aveplus.avepay_cpocket.music.util.DateTimeUtility;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Youtube {

    public enum TYPE_ITEM {
        VIDEO,
        PLAYLIST,
    }

    public PageInfo pageInfo;
    public String nextPageToken = "";
    private List<Item> items;

    public List<Item> getItems() {
        if (items == null)
            items = new ArrayList<>();
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public class PageInfo {
        public int totalResults, resultsPerPage;

    }

    public class Item {
        public Snippet snippet;
        public Object id;
        public ContentDetails contentDetails;


        public String getIdItem(TYPE_ITEM type) {
            if (type == TYPE_ITEM.VIDEO) { // is video
                LinkedTreeMap idType = (LinkedTreeMap) id;
                return idType.get("videoId").toString();
            } else if (type == TYPE_ITEM.PLAYLIST) {
                return id.toString();
            }
            return "";
        }

        public class Snippet {
            public String publishedAt, title, description, channelTitle, liveBroadcastContent;
            public Image thumbnails;
            public String channelId;

            public class Image {
                @SerializedName("high")
                public Image defaultImg;
                @SerializedName("medium")
                public Image imgMedium;
                public String url;
            }
        }

        public class Id {
            public String kind, videoId;
        }

        public class ContentDetails {
            public String duration;
            public int itemCount;
            public String videoPublishedAt, videoId;
        }

    }

    // for get channel information
    public Video getChannelInfor() {
        Video channel = new Video();
        Item item = items.get(0);
        if (item != null) {
            channel.title = item.snippet.title;
            channel.description = item.snippet.description;
            channel.image = item.snippet.thumbnails.imgMedium.url;
        }

        return channel;

    }

    // get list playlist from responde
    public List<Playlist> getPlaylists() {
        List<Playlist> list = new ArrayList<>();
        Playlist playlist;
        for (Item item : items) {
            playlist = new Playlist();
            playlist.id = item.getIdItem(TYPE_ITEM.PLAYLIST);
            playlist.title = item.snippet.title;
            playlist.dateTime = item.snippet.publishedAt;
            playlist.description = item.snippet.description;
            playlist.image = item.snippet.thumbnails.imgMedium.url;
            playlist.count = item.contentDetails.itemCount;

            list.add(playlist);
        }

        return list;
    }

    public ArrayList<Video> getVideos() {
        ArrayList<Video> list = new ArrayList<>();
        Video video;
        for (Item item : items) {
            if (item.snippet.thumbnails == null) {
                continue;
            }
            video = new Video();
            if (item.contentDetails != null) {
                video.id = item.contentDetails.videoId;
                video.dateTime = item.contentDetails.videoPublishedAt;
                video.duration = item.contentDetails.duration;
            } else {
                video.id = item.getIdItem(TYPE_ITEM.VIDEO);
                video.dateTime = item.snippet.publishedAt;
            }
            video.title = item.snippet.title;
            video.description = item.snippet.description;
            video.image = item.snippet.thumbnails.imgMedium.url;
            video.imageHD = item.snippet.thumbnails.defaultImg.url;

            list.add(video);
        }

        return list;
    }

    // get duration from request
    public ArrayList<Video> getVideosForDuration() {
        ArrayList<Video> list = new ArrayList<>();
        Video video;
        for (Item item : items) {
            video = new Video();
            video.id = item.getIdItem(TYPE_ITEM.PLAYLIST);
            video.duration = getDuration(item.contentDetails.duration) + "";

            list.add(video);
        }

        return list;
    }

    // get duration from request
    public String getDuration(String yDuration) {
        String time = yDuration.substring(2);
        long duration = 0L;
        Object[][] indexs = new Object[][]{{"H", 3600}, {"M", 60}, {"S", 1}};
        for (int i = 0; i < indexs.length; i++) {
            int index = time.indexOf((String) indexs[i][0]);
            if (index != -1) {
                String value = time.substring(0, index);
                duration += Integer.parseInt(value) * (int) indexs[i][1] * 1000;
                time = time.substring(value.length() + 1);
            }
        }
        return DateTimeUtility.convertSecondsToHMmSs(duration);
    }

    //get total pages
    public int getTotalPages() {
        return pageInfo == null ? 1 : pageInfo.totalResults;
    }

}
