package com.aveplus.avepay_cpocket.trainingcenter.object;

public class Info {

	String id;
	String introduction;

	public Info(String id, String introduction) {
		super();
		this.id = id;
		this.introduction = introduction;
	}

	public Info() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

}
