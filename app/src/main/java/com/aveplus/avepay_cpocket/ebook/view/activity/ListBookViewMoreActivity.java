package com.aveplus.avepay_cpocket.ebook.view.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseListActivityBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.SingleAdapter;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.viewmodel.activity.ListBookViewMoreVM;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemBookVM;


/**
 * Created by trangpham on 3/4/18.
 */

public class ListBookViewMoreActivity extends BaseListActivityBinding {

    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_TYPE = "KEY_TYPE";

    private ListBookViewMoreVM viewModel;
    private Bundle bundle;


    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void getExtraData(Intent intent) {
        bundle = intent.getExtras();
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new ListBookViewMoreVM(self, bundle);
        return viewModel;
    }

    @Override
    protected void onViewCreated() {
        setToolbarTitle(bundle.getString(KEY_NAME));
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(self));
        recyclerView.setAdapter(new SingleAdapter(self, R.layout.item_book_grid_all, viewModel.getListData(), ItemBookVM.class));

    }


}
