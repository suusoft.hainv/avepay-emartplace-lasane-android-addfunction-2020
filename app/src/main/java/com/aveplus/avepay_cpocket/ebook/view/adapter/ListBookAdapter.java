package com.aveplus.avepay_cpocket.ebook.view.adapter;

import android.content.Context;
import android.view.ViewGroup;


import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseAdapter;
import com.aveplus.avepay_cpocket.ebook.model.Book;

import java.util.List;


public class ListBookAdapter extends BaseAdapter {

    private List<Book> listData;
    private int type;

    public ListBookAdapter(Context context, List<?> datas) {
        super(context);
        this.listData = (List<Book>) datas;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void removeAll() {
        listData.clear();
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    @Override
    public void setDatas(List<?> data) {
        int size = listData.size();
        listData.addAll((List<Book>) data);
        notifyItemRangeInserted(size,listData.size());
    }



}
