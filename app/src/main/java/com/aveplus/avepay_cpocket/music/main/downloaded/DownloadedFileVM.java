package com.aveplus.avepay_cpocket.music.main.downloaded;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Config;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.main.localmusic.LocalContentWrapper;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.FileUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class DownloadedFileVM extends BaseViewModelList implements ItemMenuActionSongListener {

    ArrayList<Song> listData;
    private boolean check = false;


    public DownloadedFileVM(Context context) {
        super(context);
        this.listData = new ArrayList<>();

        getData(1);
    }

    @Override
    public void getData(int page) {
        new AsyncTask<String, String, ArrayList<Song>>() {
            @Override
            protected ArrayList<Song> doInBackground(String... strings) {
                listData = LocalContentWrapper.scanFileMp3ByPaths(self, Config.getPathDir());
                if (listData.size() > 0) {
                    check = true;
                    notifyChange();
                } else {
                    check = false;
                    notifyChange();
                }
                return listData;
            }

            @Override
            protected void onPostExecute(ArrayList<Song> list) {
                setListData(list);
                if (list.size() > 0) {
                    check = true;
                    notifyChange();
                } else {
                    check = false;
                    notifyChange();
                }
                checkLoadingMoreComplete(1);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().addNewQueue(listData);
        QueueManager.getInstance().setPositionCurrentItem(position);
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {
        final int pos = position;
        DialogUtil.showAlertDialog(self, R.string.confirm_delete, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                if (FileUtil.deleteFile(listData.get(pos).getSong_file())) {
                    listData.remove(pos);
                    mDatas.remove(pos);
                    notifyDataChanged(false);
                    AppUtil.showToast(self, R.string.deleted);
                } else {
                    AppUtil.showToast(self, R.string.cannot_remove_this_song);
                }
            }

            @Override
            public void onClickCancel() {

            }
        });

    }

    public int isVisibility() {
        notifyChange();
        if (check) {

            return View.GONE;
        }
        return View.VISIBLE;
    }
}
