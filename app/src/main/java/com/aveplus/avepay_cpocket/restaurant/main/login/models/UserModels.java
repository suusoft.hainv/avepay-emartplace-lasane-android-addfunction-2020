package com.aveplus.avepay_cpocket.restaurant.main.login.models;
/**
 * Created by suusoft.com on 11/12/19.
 */

import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserModels extends BaseModel {

    public static final String NORMAL = "n";
    public static final String SOCIAL = "s";
    public static final String DATA_USER = "data user";

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("qb_id")
    @Expose
    private int qbId;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("long")
    @Expose
    private double _long;
    @SerializedName("is_active")
    @Expose
    private int isActive;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("rate")
    @Expose
    private int rate;
    @SerializedName("rate_count")
    @Expose
    private int rateCount;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("modified_date")
    @Expose
    private String modifiedDate;
    @SerializedName("is_secured")
    @Expose
    private int is_secured;
    @SerializedName("pro_data")
    @Expose
    private String proData;
    @SerializedName("driver_data")
    @Expose
    private String driverData;
    @SerializedName("vehicle_data")
    @Expose
    private String vehicleData;
    @SerializedName("avg_rate")
    @Expose
    private String avgRate;
    @SerializedName("total_rate_count")
    @Expose
    private String totalRateCount;
    private String password;
    @SerializedName("listProducts")
    @Expose
    private List<Object> listProducts = null;

    public boolean isSecured() {
        return is_secured == 1;
    }

    public void setIs_secured(int is_secured) {
        this.is_secured = is_secured;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQbId() {
        return qbId;
    }

    public void setQbId(int qbId) {
        this.qbId = qbId;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double get_long() {
        return _long;
    }

    public void set_long(double _long) {
        this._long = _long;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getRateCount() {
        return rateCount;
    }

    public void setRateCount(int rateCount) {
        this.rateCount = rateCount;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public int getIs_secured() {
        return is_secured;
    }

    public String getProData() {
        return proData;
    }

    public void setProData(String proData) {
        this.proData = proData;
    }

    public String getDriverData() {
        return driverData;
    }

    public void setDriverData(String driverData) {
        this.driverData = driverData;
    }

    public String getVehicleData() {
        return vehicleData;
    }

    public void setVehicleData(String vehicleData) {
        this.vehicleData = vehicleData;
    }

    public String getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(String avgRate) {
        this.avgRate = avgRate;
    }

    public String getTotalRateCount() {
        return totalRateCount;
    }

    public void setTotalRateCount(String totalRateCount) {
        this.totalRateCount = totalRateCount;
    }

    public List<Object> getListProducts() {
        return listProducts;
    }

    public void setListProducts(List<Object> listProducts) {
        this.listProducts = listProducts;
    }
}
