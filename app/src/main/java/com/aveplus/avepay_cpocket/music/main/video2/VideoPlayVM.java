package com.aveplus.avepay_cpocket.music.main.video2;

import android.content.Context;
import android.util.Log;

import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.List;

public class VideoPlayVM extends BaseViewModelList {
    private static final String TAG = VideoPlayVM.class.getSimpleName();
    private ArrayList<ObjVideo> list;
    private ObjVideo item;

    public VideoPlayVM(Context context) {
        super(context);
        getData(1);

    }

    public VideoPlayVM(Context context, int position) {
        super(context);
        getData(position);

    }

    public VideoPlayVM(Context context, ObjVideo video) {
        super(context);
        this.item = video;
        getData(1);

    }

    @Override
    public void getData(int page) {
        Log.e(TAG, "getData: "+page );
        List<Video> list = new ArrayList<>();
        if (page == 10000) {
            getList("1", "0", "0");
        } else if (page == 20000) {
            getList("0", "1", "0");
        } else if (page == 30000) {
            getList("0", "0", "1");
        }
        else if(page ==40000) {
           getList();
        }
        else {
            getList(page);
        }

    }

    public void getList(String is_new, String is_trending, String is_popular) {
        RequestManager.getVideo(is_new, is_trending, is_popular, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                // addListData(response.getDataList(Video.class));
                List<Video> dataList = response.getDataList(Video.class);
                Log.e(TAG, "onSuccess: " + new Gson().toJson(dataList));
                addListData(dataList);
            }

            @Override
            public void onError(String message) {

            }

        });
        //addListData(list);
    }
    public void getList() {
        RequestManager.getCategory("1",  new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                List<ObjVideoCategory> dataList = response.getDataList(ObjVideoCategory.class);
                Log.e(TAG, "onSuccess: " + new Gson().toJson(dataList));
                addListData(dataList);
            }

            @Override
            public void onError(String message) {

            }

        });
        //addListData(list);
    }
    public void getList(int id_category) {
        RequestManager.getVideoCategory(String.valueOf(id_category),  new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                // addListData(response.getDataList(Video.class));
                List<Video> dataList = response.getDataList(Video.class);
                Log.e(TAG, "onSuccess: " + new Gson().toJson(dataList));
//                addListData(dataList);

            }

            @Override
            public void onError(String message) {

            }

        });
    }

}
