package com.aveplus.avepay_cpocket.taxi.interfaces;

/**
 * Created by SuuSoft.com on 10/06/2015.
 */
public interface IConfirmation {

    void onPositive();

    void onNegative();
}
