package com.aveplus.avepay_cpocket.movie.movie.main.movies.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieBuilder;

import java.util.ArrayList;
import java.util.List;

public class MyMoviesDatabase {
    private static final String DB_NAME = "myMoviesDB";
    private static final int DB_VERSION = 1;
    private static final String TB_NAME = "myMovies";
    private static final String COL_ID = "id";
    private static final String COL_NAME = "movie_name";
    private static final String COL_THUMB = "video_thumb";
    private static final String COL_VIDEO = "video_video";
    private static final String COL_TIME_ADDED = "added_at";
    private static final String COL_MY_MOVIE = "myMovie";
    private static final String COL_IMDB_NAME = "video_imdb_name";
    private static final String COL_IMDB = "video_imdb";
    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDB;

    public MyMoviesDatabase(Context context) {
        this.mDBHelper = new DatabaseHelper(context);
    }

    class DatabaseHelper extends SQLiteOpenHelper {

        private static final String QR_CREATE_TABLE = "CREATE TABLE " + TB_NAME
                + " ("
                + COL_ID + " INTEGER PRIMARY KEY, "
                + COL_NAME + " TEXT NOT NULL, "
                + COL_VIDEO + " TEXT NOT NULL, "
                + COL_THUMB + " TEXT NOT NULL, "
                + COL_MY_MOVIE + " BOOL, "
                + COL_TIME_ADDED + " DATETIME DEFAULT CURRENT_TIMESTAMP, "
                + COL_IMDB + " FLOAT, "
                + COL_IMDB_NAME + " TEXT " + ")";
        public DatabaseHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            db.execSQL(QR_CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub

        }

    }

    public SQLiteDatabase open() {
        mDB = mDBHelper.getWritableDatabase();
        return mDB;
    }

    public void close() {
        if (mDB != null)
            mDB.close();

    }

    public long addVideo(Movie movie) {
        ContentValues values = new ContentValues();
        values.put(COL_ID, movie.getMovie_id());
        values.put(COL_NAME, movie.getMovie_name());
        values.put(COL_VIDEO, movie.getMovie_video());
        values.put(COL_MY_MOVIE, movie.isMovie_isFavo());
        values.put(COL_THUMB, movie.getMovie_thumb());
        values.put(COL_IMDB, movie.getMovie_rating());
        values.put(COL_IMDB_NAME, movie.getMovie_imdb());
        return mDB.insert(TB_NAME, null, values);
    }

    public void removeVideo(Movie movie) {
        int remove = removeVideo(Integer.parseInt(movie.getMovie_id()));
        Log.i("removeVideo removeVideo", "removeVideo: " + remove);
    }

    public int removeVideo(int movieId) {
        return mDB.delete(TB_NAME, COL_ID + " = ?",
                new String[]{String.valueOf(movieId)});

    }

    public boolean isExist(Movie movie) {
        Cursor cursor = mDB.query(TB_NAME, new String[]{COL_ID, COL_NAME},
                COL_ID + "= ?", new String[]{movie.getMovie_id()}, null,
                null, null);
        try {
            cursor.moveToFirst();
            if (cursor.getCount() > 0)
                return true;
            return false;
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    public boolean isExist(String movieId) {
        String whereClause = COL_ID + " LIKE " + movieId;
        Cursor cursor = mDB.query(TB_NAME, new String[]{COL_ID},
                whereClause, null, null, null, null);
        try {
            cursor.moveToFirst();
            if (cursor.getCount() > 0)
                return true;

            return false;
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    public List<Movie> getAllMyMovies() {
        List<Movie> listMovies = new ArrayList<Movie>();
        String selectQuery = "SELECT * FROM " + TB_NAME + " ORDER BY "
                + COL_TIME_ADDED + " DESC";

        Cursor cursor = mDB.rawQuery(selectQuery, null);
        try {
            while (cursor.moveToNext()) {
                String id = cursor.getString(0);
                String name = cursor.getString(1);
                String video = cursor.getString(2);
                String thumb = cursor.getString(3);
                float imdb = cursor.getFloat(6);
                String imdb_name = cursor.getString(7);
                boolean isMyMovie = cursor.getInt(4) > 0 ? true : false;

                Movie movie = new MovieBuilder().setMovie_id(id).setMovie_name(name)
                        .setMovie_thumb(thumb).setMovie_isFavo(isMyMovie).setMovie_video(video)
                        .setMovie_rating(imdb).setMovie_imdb(imdb_name)
                        .createMovie();
                listMovies.add(movie);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return listMovies;
    }
}
