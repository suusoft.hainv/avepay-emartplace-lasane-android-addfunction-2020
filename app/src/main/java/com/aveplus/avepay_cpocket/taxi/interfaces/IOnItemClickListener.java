package com.aveplus.avepay_cpocket.taxi.interfaces;

/**
 * Created by SuuSoft.com on 12/9/2016.
 */

public interface IOnItemClickListener {
    void onItemClick(int position);
}
