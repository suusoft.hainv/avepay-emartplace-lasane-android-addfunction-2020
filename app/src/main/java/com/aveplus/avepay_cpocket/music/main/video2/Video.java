package com.aveplus.avepay_cpocket.music.main.video2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Video {
    private int id_playlist;
    private String name;

    @SerializedName("total_list")
    @Expose
    private Integer total_list;
    @SerializedName("videos")
    @Expose
    private ArrayList<ObjVideo> arrVideo;
    @SerializedName("video_categories")
    @Expose
    private ArrayList<ObjVideoCategory> arrVideoCate;
    @SerializedName("video_playlist_title")
    @Expose
    private ArrayList<ObjVideoTitle> arrVideoTitle;

    public Integer getTotal_list() {
        return total_list;
    }

    public void setTotal_list(Integer total_list) {
        this.total_list = total_list;
    }

    public ArrayList<ObjVideo> getArrVideo() {
        if (arrVideo == null){
            arrVideo = new ArrayList<>();
        }
        return arrVideo;
    }

    public void setArrVideo(ArrayList<ObjVideo> arrVideo) {
        this.arrVideo = arrVideo;
    }

    public ArrayList<ObjVideoCategory> getArrVideoCate() {
        if (arrVideoCate == null){
            arrVideoCate = new ArrayList<>();
        }
        return arrVideoCate;
    }

    public void setArrVideoCate(ArrayList<ObjVideoCategory> arrVideoCate) {
        this.arrVideoCate = arrVideoCate;
    }

    public ArrayList<ObjVideoTitle> getArrVideoTitle() {if (arrVideoTitle == null){
        arrVideoTitle = new ArrayList<>();
    }
        return arrVideoTitle;
    }

    public void setArrVideoTitle(ArrayList<ObjVideoTitle> arrVideoTitle) {
        this.arrVideoTitle = arrVideoTitle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_playlist() {
        return id_playlist;
    }

    public void setId_playlist(int id_playlist) {
        this.id_playlist = id_playlist;
    }
}
