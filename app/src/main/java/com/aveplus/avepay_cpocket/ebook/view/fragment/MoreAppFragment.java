package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseFragment;
import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.ebook.model.OurApp;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity;
import com.aveplus.avepay_cpocket.ebook.view.adapter.MoreAppAdapter;

import java.util.ArrayList;
import java.util.List;

public class MoreAppFragment extends BaseFragment {

    private RecyclerView rcvData;
    private MoreAppAdapter mAdapter;
    private List<OurApp> listData;
    private TextView tvViewMore;

    public static MoreAppFragment newInstance() {
        Bundle args = new Bundle();
        MoreAppFragment fragment = new MoreAppFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_more_apps;
    }

    @Override
    protected void init() {
        listData = new ArrayList<>();
    }

    @Override
    protected void initView(View view) {
        tvViewMore = (TextView) view.findViewById(R.id.tvViewMore);
        tvViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.goToResultByKeySearchOnGooglePlay(getActivity(), Config.KEY_SEARCH_MORE_APP_GOOGLE_PLAY);
            }
        });
        RelativeLayout layoutAll = (RelativeLayout) view.findViewById(R.id.layoutAll);

        rcvData = (RecyclerView) view.findViewById(R.id.rcv_data);
        rcvData.setLayoutManager(new LinearLayoutManager(self));
    }

    @Override
    protected void getData() {
        if (listData == null) listData = new ArrayList<>();
        else listData.clear();
        listData = Config.getListOurApp(self);
        Log.e("Our app size", listData.size() + "");
        mAdapter = new MoreAppAdapter(self, listData);
        rcvData.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) self).setToolbarTitle(R.string.more_apps);
        ((MainActivity) self).showIconToolbar();
    }
}
