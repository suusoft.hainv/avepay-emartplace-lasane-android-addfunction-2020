package com.aveplus.avepay_cpocket.taxi.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.interfaces.IObserver;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.taxi.view.activities.MainActivity;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;


import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by SuuSoft.com on 01/12/2016.
 */

public class UpdateInfoUserFragment extends BaseFragment implements View.OnClickListener, IObserver {
    private static final String TAG = UpdateInfoUserFragment.class.getName();
    private static final int RC_ADDRESS = 134;
    private static final int RQ_CHANGE_PASS = 234;

    private EditText edtBusinessName, edtAddress;
    private TextViewBold btnSave;
    public IUpdateSuccess iUpdateSuccess;

    public interface IUpdateSuccess {
        void onUpdated();
    }


    public static UpdateInfoUserFragment newInstance(IUpdateSuccess iUpdateSuccess) {
        Bundle args = new Bundle();
        UpdateInfoUserFragment fragment = new UpdateInfoUserFragment();
        fragment.setArguments(args);
        fragment.iUpdateSuccess = iUpdateSuccess;
        return fragment;
    }

    @Override
    View inflateLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_info_user, container, false);
    }

    @Override
    void initUI(View view) {

        edtBusinessName = (EditText) view.findViewById(R.id.edt_bussiness_name);
        edtAddress = (EditText) view.findViewById(R.id.edt_address);
        btnSave = (TextViewBold) view.findViewById(R.id.btn_save);


        edtAddress.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    AppUtil.hideSoftKeyboard(self);

                }
                return false;
            }
        });

    }

    @Override
    void initControl() {
        btnSave.setOnClickListener(this);

        getProfile();
    }

    @Override
    public void onClick(View view) {
        if (view == btnSave) {
            if (com.aveplus.avepay_cpocket.taxi.network1.NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
                if (isValid()) {
                    updateProfile();
                }
            } else {
                AppUtil.showToast(getActivity(), R.string.msg_no_network);
            }

        } else if (view == edtAddress) {
            MapsUtil.getAutoCompletePlaces(this, RC_ADDRESS);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_ADDRESS) {
            if (resultCode == -1) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                AppUtil.fillAddress(getActivity(), edtAddress, place);
            }
        }

    }

    private boolean isValid() {
        String bussinessName = edtBusinessName.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        if (StringUtil.isEmpty(bussinessName)) {
            AppUtil.showToast(getActivity(), R.string.msg_fill_full_name);
            edtBusinessName.requestFocus();
            return false;
        }

        if (StringUtil.isEmpty(address)) {
            AppUtil.showToast(getActivity(), R.string.msg_address_is_required);
            edtAddress.requestFocus();
            return false;
        }
        return true;
    }


    private void setData(UserObj userObj) {
        edtBusinessName.setText(userObj.getName());
        edtAddress.setText(userObj.getAddress());
    }


    private void updateProfile() {
        String bussinessName = edtBusinessName.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();

        ModelManager.updateProfileFirst(getActivity(), bussinessName,  address, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                try {
                    JSONObject jsonObject = new JSONObject(object.toString());
                    ApiResponse response = new ApiResponse(jsonObject);
                    if (!response.isError()) {
                        UserObj userObj = response.getDataObject(UserObj.class);
                        userObj.setToken( DataStoreManager.getTokenUser());
                        userObj.setRememberMe(DataStoreManager.getUser().isRememberMe());
                        DataStoreManager.saveUser(userObj);
                        AppUtil.showToast(getActivity(), R.string.msg_update_success);
                        AppUtil.startActivity(self, MainActivity.class);

                        if (iUpdateSuccess!=null)
                            iUpdateSuccess.onUpdated();



                    } else {
                        Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError() {
                Log.e(TAG, "ERROR: update profile!");
            }
        });
    }

    private void getProfile() {
        setData(DataStoreManager.getUser());

    }


    @Override
    public void update() {
        setData(DataStoreManager.getUser());
    }
}
