package com.aveplus.avepay_cpocket.taxi.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.interfaces.ITransport;
import com.aveplus.avepay_cpocket.taxi.objects.book.ServiceObj;
import com.aveplus.avepay_cpocket.taxi.utils.ImageUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;

import java.util.ArrayList;

public class Transport1Adapter extends RecyclerView.Adapter<Transport1Adapter.ViewHolder> {

    private ArrayList<ServiceObj> serviceObjs;
    private ITransport iTransport;
    private Context mContext;
    private int posCurrent = -1;

    public Transport1Adapter(Context context, ArrayList<ServiceObj> serviceObjs, ITransport iTransport) {
        this.serviceObjs = serviceObjs;
        this.iTransport = iTransport;
        this.mContext = context;
    }

    @Override
    public Transport1Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transport_type1, parent, false);
        return new Transport1Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final Transport1Adapter.ViewHolder holder, final int position) {
        if (getItemCount() > 0) {

            if (posCurrent==position)
                holder.loItem.setBackground(mContext.getResources().getDrawable(R.drawable.bg_radius_gray));
            else
                holder.loItem .setBackgroundColor(mContext.getResources().getColor(R.color.transparent));

            final ServiceObj serviceObj = serviceObjs.get(position);
            if (serviceObj != null) {
                ImageUtil.setImage(holder.imgTransport, serviceObj.getImage() );
                holder.lblTransportType.setText(serviceObj.getTitle());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        posCurrent =position;
                        iTransport.onTransportSelected(serviceObj);
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        try {
            return serviceObjs.size();
        } catch (NullPointerException ex) {
            return 0;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgTransport;
        private TextView lblTransportType;
        private View loItem;

        public ViewHolder(View view) {
            super(view);

            loItem =  view.findViewById(R.id.lo_item1);
            imgTransport = (ImageView) view.findViewById(R.id.img_type1);
            lblTransportType = (TextViewRegular) view.findViewById(R.id.lbl_type1);
        }
    }
}
