package com.aveplus.avepay_cpocket.music.base.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;

import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;


/**
 * "Copyright © 2019 SUUSOFT"
 */
public abstract class BaseActivityBinding extends AbstractActivity {

    protected BaseViewModel viewModel;
    protected ViewDataBinding binding;

    protected abstract BaseViewModel getViewModel();
    protected abstract int getLayoutInflate();
    protected abstract void onViewCreated();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initChildView();
        onViewCreated();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (viewModel != null)
            viewModel.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * inflate child view. get from getLayoutInflate()
     */
    private void initChildView(){
        viewModel = getViewModel();
        setViewDataBinding(inflateView());
    }

    protected void initView(){
        // overide for child class
    }

    /**
     * get ViewDatabinding is referenced to #getLayoutInflate()
     */
    public final ViewDataBinding inflateView(){
        return DataBindingUtil.inflate(getLayoutInflater(),getLayoutInflate(),super.contentLayout,true);
    }

    protected void setViewDataBinding(ViewDataBinding binding){
        this.binding = binding;
        this.binding.setVariable(BR.viewModel,viewModel);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.onDestroy();
    }

}
