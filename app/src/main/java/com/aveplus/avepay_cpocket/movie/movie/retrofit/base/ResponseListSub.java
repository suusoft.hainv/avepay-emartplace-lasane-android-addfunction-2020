package com.aveplus.avepay_cpocket.movie.movie.retrofit.base;


import com.aveplus.avepay_cpocket.movie.movie.model.getListSubModel;

public class ResponseListSub extends BaseReponse {
    private getListSubModel data;

    public getListSubModel getData() {
        return data;
    }

    public void setData(getListSubModel data) {
        this.data = data;
    }
}
