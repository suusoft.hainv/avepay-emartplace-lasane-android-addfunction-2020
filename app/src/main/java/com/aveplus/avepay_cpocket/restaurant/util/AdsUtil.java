package com.aveplus.avepay_cpocket.restaurant.util;//package com.pinsetbeauty.bro.util;
//
//import android.content.Context;
//
//import com.google.android.gms.ads.AdListener;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.InterstitialAd;
//import com.pinsetbeauty.bro.R;
//
///**
// * Created by suusoft.com on 11/30/17.
// */
//
//public class AdsUtil {
//
//    private static final String TEST_DEVICE = "E4F547339E2F2AA8A3C840384EADA42F";
//
//    // for ads
//    public static void loadBanner(AdView adView){
//        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(TEST_DEVICE)
//                .build();
//        adView.loadAd(adRequest);
//    }
//
//    public static void loadInterstitial(Context context){
//        final InterstitialAd mInterstitialAd = new InterstitialAd(context);
//        mInterstitialAd.setAdUnitId(context.getString(R.string.banner_ad_unit_id));
//        mInterstitialAd.loadAd(new AdRequest.Builder()
//                .addTestDevice(TEST_DEVICE)
//                .build());
//        mInterstitialAd.setAdListener(new AdListener(){
//            @Override
//            public void onAdLoaded() {
//                mInterstitialAd.show();
//
//            }
//        });
//
//    }
//
//}
