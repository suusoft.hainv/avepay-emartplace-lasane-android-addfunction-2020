package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.ProductHistory;

import java.util.ArrayList;

public class ProductHistoryAdapter extends ArrayAdapter<ProductHistory> {
    private Context context;
    private ArrayList<ProductHistory> list;

    public ProductHistoryAdapter(Context context, int layoutID, ArrayList<ProductHistory> list) {
        super(context, layoutID);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHoler vh;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_product_history, null);
            vh = new ViewHoler();
            vh.tvNameProductHistory = (TextView) rowView.findViewById(R.id.tvNameProductHistory);
            vh.tvQuantityProductHistory = (TextView) rowView.findViewById(R.id.tvQuantityProductHistory);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        ProductHistory ph = list.get(position);
        if (ph != null) {
            vh.tvNameProductHistory.setText(ph.getNameProductHistory().toUpperCase());
            vh.tvQuantityProductHistory.setText(ph.getQuantity());
        }
        return rowView;
    }

    class ViewHoler {
        TextView tvNameProductHistory, tvQuantityProductHistory;
    }
}

