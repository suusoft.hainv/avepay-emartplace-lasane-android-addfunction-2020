package com.aveplus.avepay_cpocket.trainingcenter.object;

import android.os.Parcel;
import android.os.Parcelable;

public class NewEvent {

	private String id;
	private String title;
	private String description;
	private String content;
	private String time;
	private String image;
	private String linkNews;

	public NewEvent(String id, String title, String description,
                    String content, String time, String image, String linkNews) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.content = content;
		this.time = time;
		this.image = image;
		this.linkNews = linkNews;
	}

	public String getLinkNews() {
		return linkNews;
	}

	public void setLinkNews(String linkNews) {
		this.linkNews = linkNews;
	}

	public NewEvent() {
		// TODO Auto-generated constructor stub
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}


}
