package com.aveplus.avepay_cpocket.music.main.downloaded;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentNavBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class DownloadedFileFragment extends BaseListFragmentNavBinding {
    TextView tvNodata;
    ImageButton imgDowload;
    DownloadedFileVM viewModel;

    public static DownloadedFileFragment newInstance() {

        Bundle args = new Bundle();

        DownloadedFileFragment fragment = new DownloadedFileFragment();
        fragment.setArguments(args);
        return fragment;
    }

//    @Override
//    protected int getLayoutInflate() {
//        return R.layout.fragment_download;
//    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new DownloadedFileVM(self);
        return viewModel;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {

        setTitle(R.string.download);
//        showHideBack(false);

        SingleAdapter adapter = new SingleAdapter(self, R.layout.item_song, viewModel.getListData(), ItemDownloadedFileVM.class, viewModel.getActionListener());
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        showHideActionMenu(false);
//        imgDowload = view.findViewById(R.id.c_btn_action);
//        imgDowload.setVisibility(View.GONE);
    }
}

