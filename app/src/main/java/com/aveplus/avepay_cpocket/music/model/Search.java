package com.aveplus.avepay_cpocket.music.model;


import com.aveplus.avepay_cpocket.music.main.video2.ObjVideo;

import java.util.ArrayList;

public class Search {
    private ArrayList<Song> songs;
    private ArrayList<Album> playlists;
    private ArrayList<Artist> artists;
    private ArrayList<ObjVideo> videos;
    private int count;

    public ArrayList<Song> getSongs() {
        if (songs==null){
            songs = new ArrayList<>();
        }
        return songs;
    }

    public ArrayList<Album> getPlaylists() {
        if (playlists==null){
            playlists = new ArrayList<>();
        }
        return playlists;
    }

    public ArrayList<Artist> getArtists() {
        if (artists==null){
            artists = new ArrayList<>();
        }
        return artists;
    }

    public ArrayList<ObjVideo> getVideos() {
        return videos;
    }

    public int getCount() {
        return count;
    }
}
