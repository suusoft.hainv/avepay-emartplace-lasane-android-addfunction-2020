package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.interfaces.IConfirmation;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.taxi.objects.HistoryObj;
import com.aveplus.avepay_cpocket.taxi.parsers.JSONParser;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.view.adapters.HistoryAdapter;
import com.aveplus.avepay_cpocket.taxi.widgets.recyclerview.EndlessRecyclerOnScrollListener;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SuuSoft.com on 15/12/2016.
 */

public class HistoryActivity extends com.aveplus.avepay_cpocket.base.BaseActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView rcvData;
    private ArrayList<HistoryObj> listHistory;
    private HistoryAdapter adapter;
    private TextViewBold btnDeleteAll;
    private LinearLayout llNodata, llNoConection;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int page = 1;
    private EndlessRecyclerOnScrollListener onScrollListener;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_history_taxi;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
        AppController.onPause();
    }

    @Override
    protected void initilize() {
        listHistory = new ArrayList<>();
        adapter = new HistoryAdapter(this, listHistory);
    }

    @Override
    protected void initView() {
        setToolbarTitle(R.string.history);
        rcvData = (RecyclerView) findViewById(R.id.rcv_data);
        btnDeleteAll = (TextViewBold) findViewById(R.id.btn_delete_all);
        llNodata = (LinearLayout) findViewById(R.id.ll_no_data);
        llNoConection = (LinearLayout) findViewById(R.id.ll_no_connection);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
    }

    private void setUpRecyclerView() {
        rcvData.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcvData.setLayoutManager(linearLayoutManager);
        rcvData.setAdapter(adapter);
        onScrollListener = new EndlessRecyclerOnScrollListener(new EndlessRecyclerOnScrollListener.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                getHistory(page);
            }
        }, linearLayoutManager);
        rcvData.addOnScrollListener(onScrollListener);
    }

    @Override
    protected void onViewCreated() {
        swipeRefreshLayout.setOnRefreshListener(this);
        setUpRecyclerView();
        getHistory(page);
        btnDeleteAll.setOnClickListener(this);
    }

    private void getHistory(final int page) {
        swipeRefreshLayout.setRefreshing(true);
        if (com.aveplus.avepay_cpocket.utils.NetworkUtility.isNetworkAvailable()) {
            ModelManager.getHistory(this, String.valueOf(page), new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    JSONObject jsonObject = (JSONObject) object;
                    ApiResponse apiResponse = new ApiResponse(jsonObject);
                    if (!apiResponse.isError()) {
                        listHistory.addAll(apiResponse.getDataList(HistoryObj.class));
                        adapter.notifyDataSetChanged();
                        onScrollListener.onLoadMoreComplete();
                        onScrollListener.setEnded(JSONParser.isEnded(apiResponse, page));
                        swipeRefreshLayout.setRefreshing(false);
                    } else {
                        AppUtil.showToast(getApplicationContext(), R.string.msg_have_some_errors);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    if (listHistory.isEmpty()) {
                        llNodata.setVisibility(View.VISIBLE);
                    } else {
                        llNodata.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError() {
                    swipeRefreshLayout.setRefreshing(false);
                    if (listHistory.isEmpty()) {
                        llNodata.setVisibility(View.VISIBLE);
                    } else {
                        llNodata.setVisibility(View.GONE);
                    }
                    AppUtil.showToast(getApplicationContext(), R.string.msg_have_some_errors);
                }
            });
        } else {
            llNoConection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnDeleteAll) {
            if (NetworkUtility.getInstance(getApplicationContext()).isNetworkAvailable()) {
                if (!listHistory.isEmpty()) {
                    GlobalFunctions.showConfirmationDialog(this, getString(R.string.msg_confirm_delete_history),
                            getString(R.string.yes), getString(R.string.no), true, new IConfirmation() {
                                @Override
                                public void onPositive() {
                                    deleteAllHistory();
                                }

                                @Override
                                public void onNegative() {

                                }
                            });

                } else {
                    AppUtil.showToast(getApplicationContext(), R.string.msg_no_history);
                }


            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        }
    }

    private void deleteAllHistory() {
        ModelManager.deleteHistory(this, "", new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                ApiResponse apiResponse = new ApiResponse(jsonObject);
                if (!apiResponse.isError()) {
                    listHistory.clear();
                    adapter.notifyDataSetChanged();
                    AppUtil.showToast(getApplicationContext(), R.string.msg_delete_history_success);
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    public void onRefresh() {
        listHistory.clear();
        adapter.notifyDataSetChanged();
        onScrollListener.setEnded(false);
        onScrollListener.setCurrentPage(page);
        getHistory(page);

    }
}
