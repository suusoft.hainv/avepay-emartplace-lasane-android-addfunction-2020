package com.aveplus.avepay_cpocket.music.configs;

import android.content.Context;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.model.Playlist;
import com.aveplus.avepay_cpocket.music.model.Video;
import com.aveplus.avepay_cpocket.utils.AppUtil;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class WrapperFuntion {


    // for save a video to my list
    public static void saveToMyList(Context context, Video item){
        if (item != null) {
            if (!DataStoreManager.isSavedMyList(item.id)){
                DataStoreManager.saveMyList(item);
                AppUtil.showToast(context, R.string.added_to_mylist);
            }else {
                AppUtil.showToast(context, R.string.item_already);
            }
        }
    }

    // for save a playlist to my list
    public static void saveToMyList(Context context, Playlist item){
        if (item != null) {
            if (!DataStoreManager.isSavedMyList(item.id)){
                DataStoreManager.saveMyList(item);
                AppUtil.showToast(context, R.string.added_to_mylist);
            }else {
                AppUtil.showToast(context, R.string.item_already);
            }
        }
    }

    @BindingAdapter("margin_top")
    public static void setMarginTopItemPlaylist(View view, int margin ) {
        GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) view.getLayoutParams();
        layoutParams.setMargins(0,margin,0,0);
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("margin_bottom")
    public static void setMarginBottonItemPlaylist(View view, int margin ) {
        GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) view.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, margin);
        view.setLayoutParams(layoutParams);
    }

}
