package com.aveplus.avepay_cpocket.music.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Playlist implements Parcelable {
    public String id;
    public String title;
    public String description;
    public String dateTime;
    public String image;
    public int views, count;
    public int type;
    public String duration;

    private ArrayList<Video> videos;

    public Playlist(){}


    protected Playlist(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        dateTime = in.readString();
        image = in.readString();
        views = in.readInt();
        count = in.readInt();
        type = in.readInt();
        videos = in.createTypedArrayList(Video.CREATOR);
        duration = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(dateTime);
        dest.writeString(image);
        dest.writeInt(views);
        dest.writeInt(count);
        dest.writeInt(type);
        dest.writeTypedList(videos);
        dest.writeString(duration);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Playlist> CREATOR = new Creator<Playlist>() {
        @Override
        public Playlist createFromParcel(Parcel in) {
            return new Playlist(in);
        }

        @Override
        public Playlist[] newArray(int size) {
            return new Playlist[size];
        }
    };

    public Video convertToVideoTypePlaylist(){
        Video video = new Video();
        video.id = id;
        video.title = title;
        video.description = description;
        video.image = image;
        video.type = Video.TYPE_PLAYLIST;
        video.duration = duration;
        return video;
    }


    public Video convertToVideo(){
        Video video = new Video();
        video.id = id;
        video.title = title;
        video.description = description;
        video.image = image;
        video.type = Video.TYPE_VIDEO;
        video.duration = duration;
        return video;
    }


    public ArrayList<Video> getVideos() {
        if (videos == null)
            videos = new ArrayList<>();
        return videos;
    }

    public void setVideos(ArrayList<Video> videos) {
        this.videos = videos;
    }



}
