package com.aveplus.avepay_cpocket.taxi.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.taxi.objects.book.DriverObj;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SuuSoft.com on 11/24/2016.
 */

public class UserObj implements Parcelable {

    public static final String NORMAL = "n";
    public static final String SOCIAL = "s";
    public static final String DATA_USER = "data user";
    private String id, name, email, phone, address, avatar, dob, status, latitude, longitude;
    private float rate;
    private float avg_rate;
    private int total_rate_count;
    private int rate_count, is_active;
    private String token;

    @SerializedName("created_date")
    private String created_date;

    private boolean rememberMe;
    @SerializedName("pro_data")
    private ProObj proData;
    @SerializedName("driver_data")
    private DriverObj driverData;
    @SerializedName("vehicle_data")
    private VehicleObject vehicleObject;
    private int is_secured;

    private float balance;

    private int qb_id = 0;

    private String bonus;

    public UserObj() {
    }


    protected UserObj(Parcel in) {
        id = in.readString();
        name = in.readString();
        email = in.readString();
        phone = in.readString();
        address = in.readString();
        avatar = in.readString();
        dob = in.readString();
        status = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        rate = in.readFloat();
        avg_rate = in.readFloat();
        total_rate_count = in.readInt();
        rate_count = in.readInt();
        is_active = in.readInt();
        token = in.readString();
        created_date = in.readString();
        rememberMe = in.readByte() != 0;
        proData = in.readParcelable(ProObj.class.getClassLoader());
        driverData = in.readParcelable(DriverObj.class.getClassLoader());
        is_secured = in.readInt();
        balance = in.readFloat();
        qb_id = in.readInt();
        bonus = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(avatar);
        dest.writeString(dob);
        dest.writeString(status);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeFloat(rate);
        dest.writeFloat(avg_rate);
        dest.writeInt(total_rate_count);
        dest.writeInt(rate_count);
        dest.writeInt(is_active);
        dest.writeString(token);
        dest.writeString(created_date);
        dest.writeByte((byte) (rememberMe ? 1 : 0));
        dest.writeParcelable(proData, flags);
        dest.writeParcelable(driverData, flags);
        dest.writeInt(is_secured);
        dest.writeFloat(balance);
        dest.writeInt(qb_id);
        dest.writeString(bonus);
    }

    public static final Creator<UserObj> CREATOR = new Creator<UserObj>() {
        @Override
        public UserObj createFromParcel(Parcel in) {
            return new UserObj(in);
        }

        @Override
        public UserObj[] newArray(int size) {
            return new UserObj[size];
        }
    };

    public DriverObj getDriverData() {
        return driverData;
    }

    public void setDriverData(DriverObj driverData) {
        this.driverData = driverData;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public int getRate_count() {
        return rate_count;
    }

    public void setRate_count(int rate_count) {
        this.rate_count = rate_count;
    }

    public ProObj getProData() {
        return proData;
    }

    public void setProData(ProObj proData) {
        this.proData = proData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public float getRate() {
        return (rate / 2);
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public boolean isOnline() {
        return !getToken().isEmpty();
    }

    public int getQb_id() {
        return qb_id;
    }

    public void setQb_id(int qb_id) {
        this.qb_id = qb_id;
    }

    public VehicleObject getVehicleObject() {
        return vehicleObject;
    }

    public void setVehicleObject(VehicleObject vehicleObject) {
        this.vehicleObject = vehicleObject;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float getAvg_rate() {
        return avg_rate / 2;
    }

    public void setAvg_rate(float avg_rate) {
        this.avg_rate = avg_rate;
    }

    public int getTotal_rate_count() {
        return total_rate_count;
    }

    public void setTotal_rate_count(int total_rate_count) {
        this.total_rate_count = total_rate_count;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public boolean isSecured() {
        return is_secured == 1;
    }

    public void setIs_secured(int is_secured) {
        this.is_secured = is_secured;
    }

    public String getPhoneCode() {
        if (phone == null) {
            return "";
        } else {
            String[] code = phone.split(" ");
            int length = code.length;
            if (length >= 2) {
                String phoneCode = "";
                for (int i = 0; i < length - 1; i++) {
                    phoneCode = code[i] + " " + phoneCode;
                }
                return phoneCode;
            } else {
                return "";
            }

        }

    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {

        // 2019-06-28 21:37:33


        this.created_date = created_date;
    }

    public int getIs_secured() {
        return is_secured;
    }

    public String getPhoneNumber() {
        if (phone == null) {
            return "";
        } else {
            String[] code = phone.split(" ");
            int length = code.length;
            if (length >= 2) {
                return code[length - 1];
            } else {
                return phone;
            }

        }
    }

    public String getBonus() {
        if (bonus == null)
            return "0";

        if (bonus.equals(""))
            return "0";

        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }
}
