package com.aveplus.avepay_cpocket.ebook.viewmodel.item;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.listener.IOnItemClickedListener1;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.OpenChapterActivity;
import com.aveplus.avepay_cpocket.movie.movie.listener.ISetDataListener;


public class ItemChapterDownloadVM extends BaseAdapterVM implements IOnItemClickedListener1,
        ISetDataListener {

    private Chapter chapter;

    public ItemChapterDownloadVM(Context context, Object chapter, int position) {
        super(context, position);
        this.chapter = (Chapter) chapter;
    }


    public boolean isSelected() {
        return true;
    }

    public String getTitle() {
        return chapter.getTitle();
    }

    public String getImage() {
        return chapter.getImage();
    }

    public String getType() {
        return chapter.getType();
    }

    public int getBgStatus() {
        /*if (chapter.getStatus().equals(Constant.STATUS_BOOK_HOT)) {
            return R.drawable.status_hot;
        }
        if (chapter.getStatus().equals(Constant.STATUS_BOOK_NEW)) {
            return R.drawable.status_new;
        }
        if (chapter.getStatus().equals(Constant.STATUS_BOOK_MOST)) {
            return R.drawable.status_most;
        }*/
        return 0;
    }

    public String getStatus() {
        /*if (chapter.getStatus().equals(Constant.STATUS_BOOK_HOT)) {
            return self.getString(R.string.hot);
        }
        if (chapter.getStatus().equals(Constant.STATUS_BOOK_NEW)) {
            return self.getString(R.string.status_new);
        }
        if (chapter.getStatus().equals(Constant.STATUS_BOOK_MOST)) {
            return self.getString(R.string.most);
        }*/
        return "";
    }


    public String getAuthor() {
        return "";
    }

    public String getPublisher() {
        return "";
    }

    public String getPageCurrent() {
        return "Page current: " + (chapter.getPageBookmark() + 1);
    }

    public String getDescription() {
        return  (chapter.getDescription() );
    }

//    public Object getDescription() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            return Html.fromHtml(chapter.getDescription(), Html.FROM_HTML_OPTION_USE_CSS_COLORS );
//        }else
//            return chapter.getDescription();
//    }

    @Override
    public void setData(Object object) {
        this.chapter = (Chapter) object;
        notifyChange();
    }


    @Override
    public void onItemClicked(View view) {

        Log.e("download", "onItemClicked");
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.KEY_CHAPTER, chapter);
        bundle.putBoolean(Constant.KEY_IS_DOWNLOAD, true);
        AppUtil.startActivityClearTop(self, OpenChapterActivity.class, bundle);

        //((MainActivity) self).startActivityForResult(intent,69);

//        chapter.setDateHistory(DateTimeUtility.getCurrentTimeStamp());
//        DataStoreManager.addBook(chapter, Constant.LIST_BOOK_HISTORY);
    }
}

