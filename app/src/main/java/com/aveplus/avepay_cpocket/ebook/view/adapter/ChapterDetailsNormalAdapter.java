package com.aveplus.avepay_cpocket.ebook.view.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.aveplus.avepay_cpocket.ebook.model.ChapterNomal;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentDetailsChapterNormalItem;

import java.util.List;

/**
 * Created by Suusoft on 11/14/2017.
 */
public class ChapterDetailsNormalAdapter extends FragmentStatePagerAdapter {
    private List<ChapterNomal> list;

    public ChapterDetailsNormalAdapter(FragmentManager fm, List<ChapterNomal> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentDetailsChapterNormalItem.newInstance(list.get(position));
    }

    @Override
    public int getCount() {
        return list.size();
    }
}
