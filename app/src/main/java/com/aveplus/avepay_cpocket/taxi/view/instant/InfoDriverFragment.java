package com.aveplus.avepay_cpocket.taxi.view.instant;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseFragment;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.utils.ImageUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.taxi.view.activities.TripTrackingActivity;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;

import de.hdodenhof.circleimageview.CircleImageView;


public class InfoDriverFragment extends BaseFragment implements View.OnClickListener {

    public static final int RC_PHONE_CALL_DRIVER = 100;

    private static final String TAG = InfoDriverFragment.class.getSimpleName();
    private TextView tvTime, tvEstimate, tvNote, tvAddressPickup, tvAddressDropOff,
            tvAddressFullPickup, tvAddressFullDropOff, tvName, tvPhone, tvInfo2,tvInfo3, tvInfo4;
    private Button btnCancel;
    private ViewGroup layoutEstimate;
    private LinearLayout loParent;
    private CircleImageView imgAvatarPassenger;
    private RelativeLayout progressbar;
    private View btnBack;
    private View loTime;

    private TextView btnFinish;

    private TripObj data;

    public static InfoDriverFragment newInstance(TripObj obj) {

        Bundle args = new Bundle();

        InfoDriverFragment fragment = new InfoDriverFragment();
        fragment.setArguments(args);
        fragment.data = obj;
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_detail_driver;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {

        btnBack = view.findViewById(R.id.btn_back);
        tvTime = view.findViewById(R.id.tv_time);
        tvEstimate = view.findViewById(R.id.time_estimate);
        tvName = view.findViewById(R.id.name1);
        tvPhone = view.findViewById(R.id.phone1);
        tvInfo2 = view.findViewById(R.id.tv_info2);
        tvInfo3 = view.findViewById(R.id.tv_info3);
        tvInfo4 = view.findViewById(R.id.tv_info4);
        tvAddressPickup = view.findViewById(R.id.lbl_start_point);
        tvAddressFullPickup = view.findViewById(R.id.lbl_end_point);
        tvAddressDropOff = view.findViewById(R.id.lbl_start_point1);
        tvAddressFullDropOff = view.findViewById(R.id.lbl_end_point1);
        layoutEstimate = view.findViewById(R.id.layout_time_estimate);
        progressbar = view.findViewById(R.id.lo_loading);
        imgAvatarPassenger=view.findViewById(R.id.img_avatar_passenger);

        btnCancel = view.findViewById(R.id.btn_cancel);
        btnFinish = view.findViewById(R.id.lbl_completed);
        loTime = view.findViewById(R.id.lo_time);


        btnCancel.setOnClickListener(this);
        btnFinish.setOnClickListener(this);
        layoutEstimate.setOnClickListener(this);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TripTrackingActivity)self).hideDetailTrip();
            }
        });


        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new Global.PushClickBtnFinishTrip());
            }
        });

        setData();

    }


    private void setData() {
        Log.e(TAG, "obj " + new Gson().toJson(data));

        if (tvName!=null){
            tvName.setText(String.format(getString(R.string.bind_name), data.getDriverName()));
            tvPhone.setText(String.format(getString(R.string.bind_phone),data.getDriverPhone()) );
            //tvInfo2.setText(StringUtil.convertNumberToPrice(data.getEstimateFare()));
            tvInfo2.setText(String.format(getString(R.string.bind_brand),data.getDriverBrand()) );
            tvInfo3.setText(String.format(getString(R.string.bind_model),data.getDriverModel()) );
            tvInfo4.setText(String.format(getString(R.string.bind_number_plate),data.getDriverPlate()) );
            tvEstimate.setText(data.getEstimateTime() + "");
            tvAddressPickup.setText(MapsUtil.getAddressFromLatLng(self, data.getLatLngPickup()));
            tvAddressFullPickup.setText(MapsUtil.getCityFromLatLng(self, data.getLatLngPickup()));

            tvAddressDropOff.setText(MapsUtil.getAddressFromLatLng(self, data.getLatLngDestination()) );
            tvAddressFullDropOff.setText(MapsUtil.getCityFromLatLng(self, data.getLatLngDestination()));
            ImageUtil.setImage(self, imgAvatarPassenger, data.getDriverAvatar());

            if (data.getTime_to_passenger() > 0)
                beginTimeCountDown();
            else {
                data.setTime_to_passenger(4);
                beginTimeCountDown();
//            loTime.setVisibility(View.GONE);
            }
        }


    }

    public void setData(TripObj obj) {
        data = obj;

        if (tvName!=null){
            tvName.setText(String.format(getString(R.string.bind_name), data.getDriverName()));
            tvPhone.setText(String.format(getString(R.string.bind_phone),data.getDriverPhone()) );
            //tvInfo2.setText(StringUtil.convertNumberToPrice(data.getEstimateFare()));
            tvInfo2.setText(String.format(getString(R.string.bind_brand),data.getDriverBrand()) );
            tvInfo3.setText(String.format(getString(R.string.bind_model),data.getDriverModel()) );
            tvInfo4.setText(String.format(getString(R.string.bind_number_plate),data.getDriverPlate()) );
            tvEstimate.setText(data.getEstimateTime() + "");
            tvAddressPickup.setText(MapsUtil.getAddressFromLatLng(self, data.getLatLngPickup()));
            tvAddressFullPickup.setText(MapsUtil.getCityFromLatLng(self, data.getLatLngPickup()));

            tvAddressDropOff.setText(MapsUtil.getAddressFromLatLng(self, data.getLatLngDestination()) );
            tvAddressFullDropOff.setText(MapsUtil.getCityFromLatLng(self, data.getLatLngDestination()));
            ImageUtil.setImage(self, imgAvatarPassenger, data.getDriverAvatar());

            if (data.getTime_to_passenger() > 0)
                beginTimeCountDown();
            else {
                data.setTime_to_passenger(4);
                beginTimeCountDown();
                //loTime.setVisibility(View.GONE);
            }
        }


    }


    @Override
    protected void getData() {

    }

    @Override
    public void onClick(View view) {

    }

    public void callPhoneDriver() {
        GlobalFunctions.call((Activity) self, data.getDriverPhone());
    }

    private CountDownTimer countDownTimer;
    private void beginTimeCountDown() {

        long timeTotal = data.getTime_to_passenger()   * 1000 * 60 ; // minutes

        if (countDownTimer==null)
            countDownTimer =  new CountDownTimer(timeTotal , 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                tvTime.setText(StringUtil.bindCountDown(millisUntilFinished));
            }

            public void onFinish() {

                if (self!=null)
                {
                    tvTime.setText("00:00");
                    // tvTime.setTextColor(self.getResources().getColor(R.color.red));
                }

            }
        };

        countDownTimer.start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        countDownTimer .cancel();
    }
}
