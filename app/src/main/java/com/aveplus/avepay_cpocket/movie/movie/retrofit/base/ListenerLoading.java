package com.aveplus.avepay_cpocket.movie.movie.retrofit.base;


import com.aveplus.avepay_cpocket.movie.movie.network.IBaseListener;

/**
 * Created by Suusoft 2020.
 */

public interface ListenerLoading extends IBaseListener {
    void onLoadingIsProcessing();
    void onLoadingIsCompleted();
}
