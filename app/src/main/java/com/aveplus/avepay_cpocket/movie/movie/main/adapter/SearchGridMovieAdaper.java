package com.aveplus.avepay_cpocket.movie.movie.main.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.customwidget.FadeInNetworkImageView;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;


import java.util.List;

/**
 *Copyright © 2019 SUUSOFT
 */
public class SearchGridMovieAdaper extends ArrayAdapter<Movie> {
    private int mResource;
    private List<Movie> mMovies;
    private LayoutInflater inflater;
    private ImageLoader imageLoader;
    private int mPosition;
    private Context mContext;
    private GridView mGridview;

    public SearchGridMovieAdaper(Context context, int resource, List<Movie> objects,
                            AdapterView adapterView) {
        super(context, resource, objects);
        this.mResource = resource;
        this.mMovies = objects;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        imageLoader = RequestManager.getImageLoader();
        mContext = context;
        mGridview = (GridView) adapterView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        HolderView holderView;
        if(view == null){
            view = inflater.inflate(mResource, null);
            holderView = new HolderView();
            holderView.imgMovie = (FadeInNetworkImageView) view.findViewById(R.id.imv_movie);
            holderView.lblName = (TextView) view.findViewById(R.id.tv_name);
            holderView.lblLang = (TextView) view.findViewById(R.id.tvLanguage);
            //set font
            AssetManager assetManager = mContext.getAssets();
            holderView.lblName.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));
            holderView.lblLang.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));

            view.setTag(holderView);
        }else
            holderView = (HolderView) view.getTag();
        if(mMovies!=null){
            holderView.lblName.setText(mMovies.get(position).getMovie_name());
            holderView.lblLang.setText(mMovies.get(position).getMovie_rating()+"");
            holderView.imgMovie.setImageUrl(mMovies.get(position).getMovie_thumb(), imageLoader);
        }
        holderView.position = position;
        return view;
    }

    class HolderView{
        public FadeInNetworkImageView imgMovie;
        public TextView lblName;
        public TextView lblLang;
        public int position;
    }
}
