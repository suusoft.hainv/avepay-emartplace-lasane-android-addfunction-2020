package com.aveplus.avepay_cpocket.movie.movie.globals;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;

import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.aveplus.avepay_cpocket.MySharedPreferences;
import com.aveplus.avepay_cpocket.R;


/**
 * Copyright © 2019 SUUSOFT
 */
public class GlobalFunctions {

    private static final String GCM_TOKEN = "gcmToken";
    private static final String USER_OBJ = "userObj";



    public static void enableStrictMode() {
        // Allow strict mode
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static void startActivityLTR(Activity act, Class<?> clz, Bundle bundle) {
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtras(bundle);
        act.startActivity(intent);
        act.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public static void startActivityLTR(Activity act, Class<?> clz) {
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        act.startActivity(intent);
        act.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public static void startActivityRTL(Activity act, Class<?> clz, Bundle bundle) {
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtras(bundle);
        act.startActivity(intent);
        act.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public static void startActivityRTL(Activity act, Class<?> clz) {
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        act.startActivity(intent);
        act.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public static void startActivityWithoutAnimation(Context act, Class<?> clz) {
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        act.startActivity(intent);
    }
    public static  void startActivityCart (Context act, Class<?> clz){
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        act.startActivity(intent);
    }

    public static void startActivityWithoutAnimation(Context act, Class<?> clz, Bundle bundle) {
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtras(bundle);
        act.startActivity(intent);
    }

    public static void startActivityWithoutAnimation1(Context act, Class<?> clz, Bundle bundle) {
        Intent intent = new Intent(act, clz);
        intent.putExtras(bundle);
        act.startActivity(intent);
    }

    public static void startActivityForResult(Activity act, Class<?> clz, int reqCode, Bundle bundle) {
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtras(bundle);
        act.startActivityForResult(intent, reqCode);
    }

    public static void startActivityForResult(Activity act, Class<?> clz, int reqCode) {
        Intent intent = new Intent(act, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        act.startActivityForResult(intent, reqCode);
    }

    public static void startActivityForResult(Fragment act, Class<?> clz, int reqCode, Bundle bundle) {
        Intent intent = new Intent(act.getContext(), clz);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        act.startActivityForResult(intent, reqCode);
    }

    /**
     * Close activity with right-to-left animation
     *
     * @param act
     */
    public static void finishActivity(Activity act) {
        act.finish();
        act.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    /**
     * Force close keyboard of the given editText
     *
     * @param activity
     */
    public static void closeKeyboard(AppCompatActivity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public static void showKeyboard(Context ctx, EditText editText) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * @param email
     * @return True if the given email is valid
     */
    public static boolean validateEmail(String email) {
        return !email.trim().isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * @param act
     * @return Width of screen by pixel
     */
    public static int getScreenWidthAsPixel(Activity act) {
        DisplayMetrics dm = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    /**
     * @param act
     * @return Width of screen by inch
     */
    public static double getScreenWidthAsInch(Activity act) {
        DisplayMetrics dm = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int dens = dm.densityDpi;
        double wi = (double) width / (double) dens;
        double hi = (double) height / (double) dens;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        return Math.sqrt(x + y);
    }

    /**
     * Send an email to the (one)given email, you can customize to send to multiple
     *
     * @param ctx
     * @param email
     */
    public static void sendEmail(Context ctx, String email) {
        if (!email.isEmpty() && !email.equals("null")) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            i.putExtra(Intent.EXTRA_SUBJECT, "");
            i.putExtra(Intent.EXTRA_TEXT, "");
            try {
                ctx.startActivity(Intent.createChooser(i, ctx.getString(R.string.send_email)));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(ctx, ctx.getString(R.string.no_email_client), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(ctx, ctx.getString(R.string.no_email), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * If you want to call this method on api 23 or higher then you need to check permission in runtime
     *
     * @param act
     * @param phoneNumber
     */
    public static void call(Activity act, String phoneNumber) {
        if (phoneNumber != null && !phoneNumber.equals("") && !phoneNumber.equalsIgnoreCase("null")) {
            String number = "tel:" + phoneNumber;
            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
            act.startActivity(callIntent);
        } else {
            Toast.makeText(act, act.getString(R.string.no_phone_number), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Generating random string
     *
     * @param length Length of code
     * @return Random string
     */
    public static String generateCode(int length) {
        String str = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789";
        String code = "";
        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * str.length());
            code += str.charAt(index);
        }

        return code;
    }

    public static void saveFCMToken(Context context, String token) {
        MySharedPreferences.getInstance(context).putStringValue(GCM_TOKEN, token);
    }

    public static String getFCMToken(Context context) {
        return MySharedPreferences.getInstance(context).getStringValue(GCM_TOKEN);
    }

    /**
     * Check permissions in runtime
     *
     * @param activity
     * @param permissions
     * @param reqCode
     * @param notification
     * @return
     */
    public static boolean isGranted(Activity activity, String[] permissions, int reqCode, String notification) {
        boolean granted = true;

        if (isMarshmallow()) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];

                granted = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
                if (!granted) {
                    if (notification != null && notification.length() > 0) {
                        Toast.makeText(activity, notification, Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

            // Ask permissions
            if (!granted) {
                ActivityCompat.requestPermissions(activity, permissions, reqCode);
            }
        }

        return granted;
    }

    /**
     * Check location is granted or not in run time
     *
     * @param activity
     * @param reqCode
     * @param notification [optional] can be null/empty if you don't want to notify user
     * @return true if location is granted
     */
    public static boolean locationIsGranted(Activity activity, int reqCode, String notification) {
        boolean granted = true;

        if (isMarshmallow()) {
            String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];

                granted = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
                if (granted) {
                    break;
                }
            }

            // Ask permissions
            if (!granted) {
                if (notification != null && notification.length() > 0) {
                    Toast.makeText(activity, notification, Toast.LENGTH_SHORT).show();
                }

                ActivityCompat.requestPermissions(activity, permissions, reqCode);
            }
        }

        return granted;
    }

    /**
     * Check CALL_PHONE is granted or not in run time
     *
     * @param activity
     * @param reqCode
     * @param notification [optional] can be null/empty if you don't want to notify user
     * @return true if CALL_PHONE is granted
     */
    public static boolean callPhoneIsGranted(Activity activity, int reqCode, String notification) {
        boolean granted = true;

        if (isMarshmallow()) {
            String[] permissions = new String[]{Manifest.permission.CALL_PHONE};
            String permission = permissions[0];

            granted = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
            if (!granted) {
                if (notification != null && notification.length() > 0) {
                    Toast.makeText(activity, notification, Toast.LENGTH_SHORT).show();
                }
            }

            // Ask permissions
            if (!granted) {
                ActivityCompat.requestPermissions(activity, permissions, reqCode);
            }
        }

        return granted;
    }

    public static boolean isMarshmallow() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M;
    }


    /*public static UserObj getUserInfo(Context context) {
        String strUser = MySharedPreferences.getInstance(context).getStringValue(USER_OBJ);
        return new Gson().fromJson(strUser, UserObj.class);
    }*/




}
