package com.aveplus.avepay_cpocket.music.main.player;

import android.content.Context;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.QueueManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LyricsVM extends BaseViewModel {
    Song item;

    public LyricsVM(Context self) {
        super(self);
        checkItem();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.NewSongSelected event) {
        checkItem();
        getContent();
        notifyChange();
    }


    private void checkItem() {
        item = QueueManager.getInstance().getCurrentItem();
    }

    public String getContent() {
        notifyChange();
        return item != null ? item.getContent() : "";
    }
}
