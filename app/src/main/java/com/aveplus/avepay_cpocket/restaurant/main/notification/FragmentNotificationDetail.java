package com.aveplus.avepay_cpocket.restaurant.main.notification;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.restaurant.util.ImageUtil;

import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_CONTENT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_IMAGE;


public class FragmentNotificationDetail extends BaseFragment {
    private ImageView imgNotifi;
    private TextView tvContent;
    private Toolbar mToolbar;
    private ImageView imgBack;

    @Override
    protected int getLayoutInflate() {
        return R.layout.notifi_detail;
    }


    @Override
    protected void init() {

    }



    @Override
    protected void initView(View view) {
        mToolbar    = view.findViewById(R.id.toolbar_notification);
        imgBack     = view.findViewById(R.id.img_back);
        imgBack.setVisibility(View.VISIBLE);

        tvContent   = view.findViewById(R.id.tv_content);
        imgNotifi   = view.findViewById(R.id.img_notifi);

        Bundle bundle = getArguments();
        if (bundle != null) {
            tvContent.setText(bundle.getString(PARAM_CONTENT));
            ImageUtil.setImage(imgNotifi, bundle.getString(PARAM_IMAGE));
        }

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationFragment fragment = new NotificationFragment();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().replace(R.id.root_layout, fragment);
                fragmentTransaction.commit();
                fragmentTransaction.addToBackStack("");
            }
        });

    }

    @Override
    protected void getData() {

    }


}
