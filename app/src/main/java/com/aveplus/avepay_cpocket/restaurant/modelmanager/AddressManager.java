package com.aveplus.avepay_cpocket.restaurant.modelmanager;


import com.aveplus.avepay_cpocket.restaurant.main.login.models.UserModels;

import java.util.ArrayList;

public class AddressManager {
    public ArrayList<UserModels> userList = new ArrayList<>();
    public static AddressManager mInstance;

    public AddressManager(){

    }
    public static AddressManager getInstance() {
        if (mInstance == null) {
            {
                mInstance = new AddressManager();
            }
        }
        return mInstance;
    }

    public void addItem (UserModels userModels){
        userList.add(userModels);
    }
    public void removeItem (int position){
        userList.remove(position);
    }
    public ArrayList<UserModels> getArray(){
        return this.userList;
    }
}
