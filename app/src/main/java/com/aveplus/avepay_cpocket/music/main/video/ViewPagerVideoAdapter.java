package com.aveplus.avepay_cpocket.music.main.video;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class  ViewPagerVideoAdapter extends FragmentPagerAdapter {
    private static final String TAG = ViewPagerVideoAdapter.class.getSimpleName() ;
    private List<Genre> listGenre;
    private Context mContext;

    public ViewPagerVideoAdapter(Context context, FragmentManager fm) {

        super(fm);
        mContext = context;
    }
    public void setListGenre(List<Genre> listGenre) {
        this.listGenre = listGenre;
    }
    @Override
    public Fragment getItem(int position) {
        Log.e(TAG, "getItem: "+position );

        return ListVideoFragment.newInstance(mContext,listGenre.get(position));
    }
    @Override
    public int getCount() {
        return listGenre.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
       // return listGenre.size() != 0 ? listGenre.get(position).getName() : "";
        return listGenre.get(position).getName();
    }



}
