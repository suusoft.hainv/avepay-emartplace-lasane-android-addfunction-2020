package com.aveplus.avepay_cpocket.restaurant.retrofit.response;
/**
 * Created by suusoft.com on 11/12/19.
 */
import com.aveplus.avepay_cpocket.restaurant.main.login.models.UserModels;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponeUser extends BaseModel {

    public UserModels data;

    @SerializedName("login_token")
    @Expose
    private String token;

    public UserModels getData() {
        return data;
    }

    public void setData(UserModels data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
