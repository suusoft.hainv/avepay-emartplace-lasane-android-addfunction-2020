package com.aveplus.avepay_cpocket.music.main.search;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragmentNav;
import com.aveplus.avepay_cpocket.music.model.Search;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class SearchFragment extends BaseFragmentNav {
    public static final String KEY_SEARCH = "KEY_SEARCH";
    private SearchAdapter searchAdapter;
    private ArrayList<SearchCategory> searchArrayList = new ArrayList<>();
    private RecyclerView rcvSearch;
    private static String keyword;
    private EditText edtSearch;
    private TextView tvTitle;
    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment ();

        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_search_music;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        edtSearch = view.findViewById (R.id.edt_search);
        rcvSearch = view.findViewById (R.id.rcv_search);
        tvTitle = view.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.action_search);
        searchArrayList = new ArrayList<>();
        getSuggest();
        setAdapterS ();
        getDataSearch ();
    }

    private void getDataSearch() {
        edtSearch.addTextChangedListener (new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchKey ();
            }
        });

//        RequestManager.searchSongs (1, keyword, new BaseRequest.CompleteListener () {
//            @Override
//            public void onSuccess(ApiResponse response) {
//
//                if (!response.isError ()) {
//                    if (!searchArrayList.isEmpty ()) {
//                        searchArrayList.clear ();
//                    }
//                    SearchCategory searchCategory;
//                    Search search = response.getDataObject (Search.class);
//                    if (search.getCount ()==0){
//                        AppUtil.showToast (self,R.string.empty);
//                    }else {
//                        for (int i = 0; i < search.getCount (); i++) {
//                        searchCategory = new SearchCategory ();
////                        if (i == 0 && search.getArtists ().size () > 0) {
////                            searchCategory.setName (SearchCategory.ARTIST);
////                            searchCategory.getListArtist ().addAll (search.getArtists ());
////                        } else
//                        if (i == 0 && search.getPlaylists ().size () > 0) {
//                            searchCategory.setName (SearchCategory.PLAYLIST);
//                            searchCategory.getListAlbum ().addAll (search.getPlaylists ());
//                        } else if (search.getSongs ().size () > 0) {
//                            searchCategory.setName (SearchCategory.SONGS);
//                            searchCategory.getListSongs ().addAll (search.getSongs ());
//                        }
//                        searchArrayList.add (searchCategory);
//                    }}
//
//                     searchAdapter.notifyDataSetChanged ();
//                }
//            }
//
//            @Override
//            public void onError(String message) {
//                AppUtil.showToast (self, message);
//            }
//        });
    }

    private void searchKey() {
        if (edtSearch.getText ().toString ().trim ().length () == 0) {
            searchArrayList.clear ();
            searchAdapter.notifyDataSetChanged ();
        } else {
            RequestManager.searchSongs (1, edtSearch.getText ().toString ().trim (), new BaseRequest.CompleteListener () {
                @Override
                public void onSuccess(ApiResponse response) {

                    if (!response.isError ()) {
                        if (!searchArrayList.isEmpty ()) {
                            searchArrayList.clear ();
                        }
                        SearchCategory searchCategory;
                        Search search = response.getDataObject (Search.class);
                        if (search.getCount () == 0) {
                            AppUtil.showToast (self, R.string.msg_empty);
                        } else {
                            for (int i = 0; i < search.getCount (); i++) {
                                searchCategory = new SearchCategory ();
//                        if (i == 0 && search.getArtists ().size () > 0) {
//                            searchCategory.setName (SearchCategory.ARTIST);
//                            searchCategory.getListArtist ().addAll (search.getArtists ());
//                        } else
                                if (i == 0 && search.getPlaylists ().size () > 0) {
                                    searchCategory.setName (SearchCategory.PLAYLIST);
                                    searchCategory.getListAlbum ().addAll (search.getPlaylists ());
                                } else if (i == 1&&search.getSongs ().size () > 0) {
                                    searchCategory.setName (SearchCategory.SONGS);
                                    searchCategory.getListSongs ().addAll (search.getSongs ());
                                }
//                                else if (search.getVideos ().size () > 0) {
//                                    Log.e ("TAG", "search.getVideos ().size (): "+ search.getVideos ().size ());
//                                    searchCategory.setName (SearchCategory.VIDEOS);
//                                    searchCategory.getListVideo ().addAll (search.getVideos ());
//                                }
                                searchArrayList.add (searchCategory);
                            }
                        }

                        searchAdapter.notifyDataSetChanged ();
                    }
                }

                @Override
                public void onError(String message) {
                    AppUtil.showToast (self, message);
                }
            });
        }
    }

    private void setAdapterS() {
        searchAdapter = new SearchAdapter (self, searchArrayList);
        rcvSearch.setLayoutManager (new LinearLayoutManager(self, RecyclerView.VERTICAL, false));
        rcvSearch.setAdapter (searchAdapter);
    }

    @Override
    protected void getData() {
    }
    private void getSuggest(){
        RequestManager.searchSongs (1,"a", new BaseRequest.CompleteListener () {
            @Override
            public void onSuccess(ApiResponse response) {

                if (!response.isError ()) {
                    if (!searchArrayList.isEmpty ()) {
                        searchArrayList.clear ();
                    }
                    SearchCategory searchCategory;
                    Search search = response.getDataObject (Search.class);
                    if (search.getCount () == 0) {
                        AppUtil.showToast (self, R.string.empty);
                    } else {
                        for (int i = 0; i < search.getCount (); i++) {
                            searchCategory = new SearchCategory ();
//                        if (i == 0 && search.getArtists ().size () > 0) {
//                            searchCategory.setName (SearchCategory.ARTIST);
//                            searchCategory.getListArtist ().addAll (search.getArtists ());
//                        } else
                            if (i == 0 && search.getPlaylists ().size () > 0) {
                                searchCategory.setName (SearchCategory.PLAYLIST);
                                searchCategory.getListAlbum ().addAll (search.getPlaylists ());
                            } else if (i == 1&&search.getSongs ().size () > 0) {
                                searchCategory.setName (SearchCategory.SONGS);
                                searchCategory.getListSongs ().addAll (search.getSongs ());
                            }
//                            else if (search.getVideos ().size () > 0) {
//                                Log.e ("TAG", "search.getVideos ().size (): "+ search.getVideos ().size ());
//                                searchCategory.setName (SearchCategory.VIDEOS);
//                                searchCategory.getListVideo ().addAll (search.getVideos ());
//                            }
                            searchArrayList.add (searchCategory);
                        }
                    }

                    searchAdapter.notifyDataSetChanged ();
                }
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast (self, message);
            }
        });
    }
}
