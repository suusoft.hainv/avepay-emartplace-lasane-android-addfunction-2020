package com.aveplus.avepay_cpocket.music.main.video2;

import com.aveplus.avepay_cpocket.music.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponeMoreVideo extends BaseModel {
    @SerializedName("data")
    @Expose
    private ArrayList<ObjVideo> data;

    public ArrayList<ObjVideo> getData() {
        return data;
    }

    public void setData(ArrayList<ObjVideo> data) {
        this.data = data;
    }
}
