package com.aveplus.avepay_cpocket.restaurant.model;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.PagerAdapter;

import com.aveplus.avepay_cpocket.restaurant.listener.IOnClickListener;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.ProductImage;
import com.bumptech.glide.Glide;


import java.util.List;

public class AdapterViewPageImage extends PagerAdapter {
    private Context context;
    private List<ProductImage>  banner;
    private IOnClickListener iOnClickListener;

    public AdapterViewPageImage(Context context, List<ProductImage>  banner, IOnClickListener onClickListener) {
        this.context = context;
        this.banner = banner;
        this.iOnClickListener = onClickListener;
    }

    @Override
    public int getCount() {
        return banner.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final AppCompatImageView imageView = new AppCompatImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        container.addView(imageView);
        // Load ảnh vào ImageView
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iOnClickListener.onClick(position);
            }
        });
//        ImageUtil.setImage(imageView,banner.get(position).getImage());
        Glide.with(context).load(banner.get(position).getImage()).into(imageView);
        // Return
        return imageView;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        // container chính là ViewPager, còn Object chính là return của instantiateItem ứng với position
        container.removeView((View) object);
    }
}
