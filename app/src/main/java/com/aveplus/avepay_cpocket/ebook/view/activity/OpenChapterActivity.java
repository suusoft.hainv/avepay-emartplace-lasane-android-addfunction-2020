package com.aveplus.avepay_cpocket.ebook.view.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseActivity;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.view.adapter.ChapterAdapter;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentDetailsChapterPDF;
import com.aveplus.avepay_cpocket.ebook.widgets.dialog.ProgressBarDialog;
import com.folioreader.FolioReader;
import com.folioreader.model.HighLight;
import com.folioreader.model.locators.ReadLocator;
import com.folioreader.util.OnHighlightListener;
import com.folioreader.util.ReadLocatorListener;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class OpenChapterActivity extends BaseActivity implements OnHighlightListener, ReadLocatorListener {


    private Bundle bundle;
    public Chapter mChapter;
    private TextView tvFooterChapter;
    private boolean isLoadEpub;
    private boolean isDownload;

    public int curPage = 1;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_details_book;
    }

    @Override
    protected void getExtraData(Intent intent) {
        bundle = intent.getExtras();
        if (null != bundle) {
            mChapter = bundle.getParcelable(Constant.KEY_CHAPTER);
            isDownload = bundle.getBoolean(Constant.KEY_IS_DOWNLOAD);
            onRead();
        }
    }


    private void onRead() {
        Fragment fragment = null;

        switch (mChapter.getType()) {
            case Chapter.TYPE_PDF:
                fragment = FragmentDetailsChapterPDF.newInstance(mChapter, isDownload);
                break;

            case Chapter.TYPE_EPUB:
                isLoadEpub = true;
                loadFileEpub(mChapter, flag);
                break;

        }

        if (!mChapter.getType().equals(Chapter.TYPE_EPUB))
            ((FragmentActivity) self).getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.content_details, fragment, Constant.FRAGMENT_DETAILS_CHAPTER)
                    .addToBackStack(null)
                    .commit();
    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLoadEpub)
            finish();
    }

    @Override
    protected void onViewCreated() {
        setToolbarTitle(mChapter.getTitle());
    }

    @Override
    public void onBackPressed() {
        // String tag = getSupportFragmentManager().findFragmentById(R.id.content_details).getTag();
        Log.e("EEE", "BACK");
        if (toolbar.getVisibility() == View.GONE) {
            showFullScreen(false);
            ((Activity) self).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            ((Activity) self).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } else {
            finish();
//            if (tag.equals(Constant.FRAGMENT_CHAPTER)) {
//                finish();
//            } else {
//                super.onBackPressed();
//            }
            System.gc();
            Runtime.getRuntime().gc();
        }

    }


    public void showFullScreen(boolean values) {
        TextView tvIndicator = (TextView) this.findViewById(R.id.tv_indicator);

        if (values) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            toolbar.setVisibility(View.GONE);
            if (tvFooterChapter != null)
                tvFooterChapter.setVisibility(View.GONE);
            tvIndicator.setVisibility(View.GONE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            toolbar.setVisibility(View.VISIBLE);
            if (tvFooterChapter != null)
                tvFooterChapter.setVisibility(View.VISIBLE);
            tvIndicator.setVisibility(View.VISIBLE);

        }
    }

    public void addBookrMarks() {
        setResult(RESULT_CANCELED);
        DataStoreManager.addPageBookmark(mChapter, curPage);
        //DataStoreManager.addBook(mBook, Constant.LIST_BOOK_MARKS);
        showSnackBar(R.string.message_success_add_bookmarks);
    }

    public void deleteBookrMarks() {
        setResult(RESULT_OK);
        DataStoreManager.deletePageBookmark(mChapter, curPage);
        //DataStoreManager.deleteBook(mBook, Constant.LIST_BOOK_MARKS);
        showSnackBar(R.string.message_success_delete_bookmarks);
    }

    public void setFooterChapter(TextView footerChapter) {
        tvFooterChapter = footerChapter;
    }


    private void loadFileEpub(Chapter chapter, int flag) {
        this.flag = flag;
        this.chapter = chapter;
        File file = new File(chapter.getSdcardURL());
        if (file.exists()) {
            openEpub();
        } else {
            download();
        }
        Log.e("readPosition", "flag " + flag);
    }


    //////////////////////////////////
    //download with file Epub
    private int flag;
    private Chapter chapter;
    private ProgressBarDialog mProgressDialog;
    private AsyncTask<Void, Integer, Boolean> downloadAsyntask;

    public void download() {
        downloadAsyntask = new AsyncTask<Void, Integer, Boolean>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressBarDialog(self, new ProgressBarDialog.IOnClickCancel() {
                    @Override
                    public void onCancel() {
                        ((FragmentActivity) self).onBackPressed();
                    }
                });
//                mProgressDialog.setMessage(self.getString(R.string.downloading));
//                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
//                mProgressDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        ((FragmentActivity) self).onBackPressed();
//                    }
//                });
                Log.e("EEE", "" + chapter.getAttachment());
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    String downloadUrl = chapter.getAttachment();
                    String downloadPath = chapter.getSdcardURL();

                    long downloadedLength = 0;

                    File file = new File(downloadPath);
                    URL url = new URL(downloadUrl);

                    BufferedInputStream inputStream = null;
                    BufferedOutputStream outputStream = null;

                    URLConnection connection = url.openConnection();

                    if (file.exists()) {
                        Log.e("XX", "XXXXX");
                        downloadedLength = file.length();
                        connection.setRequestProperty("Range", "bytes=" + downloadedLength + "-");
                        outputStream = new BufferedOutputStream(new FileOutputStream(file, true));

                    } else {
                        outputStream = new BufferedOutputStream(new FileOutputStream(file));

                    }

                    connection.connect();

                    if (connection.getContentLength() <= 0 && file.length() > 0) {
                        Log.e("XX", "Co nghĩa gì đâu");
                        return true;
                    } else {
                        ((Activity) self).runOnUiThread(new Runnable() {
                            public void run() {
                                if (mProgressDialog.isShowing()) {
                                    mProgressDialog.dismiss();
                                    mProgressDialog.show();
                                } else {
                                    mProgressDialog.show();
                                }

                            }
                        });
                    }
                    inputStream = new BufferedInputStream(connection.getInputStream());

                    byte[] buffer = new byte[1024 * 8];
                    int byteCount;

                    long total = 0;

                    int fileLength = connection.getContentLength();
                    Log.e("eee", "File Length: " + fileLength);
                    Log.e("XX", "Làm gì kệ tao");
                    while ((byteCount = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, byteCount);
                        Log.e("XX", "Má ơi nó lại ghi lại à..!");
                        total += byteCount;
                        if (fileLength > 0) {
                            Log.e("eee", "progres: " + (int) (total * 100 / fileLength));
                            publishProgress((int) (total * 100 / fileLength));
                        }
                    }

                    inputStream.close();
                    outputStream.flush();
                    outputStream.close();

                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onProgressUpdate(Integer... values) {

                mProgressDialog.setProgress(values[0]);
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                mProgressDialog.dismiss();
                if (aVoid) {
                    if (flag == ChapterAdapter.START) {
                        //startRead();
                        openEpub();
                    } else if (flag == ChapterAdapter.CONTINUES) {
                        openEpubContinue();
                        //readContinues();
                    }
                    Log.e("XX", "ghi xong rồi...");
//                    showDialogContinue();
                    // openEpub();
                } else {
                    showDialodDownloadFailed();
                }
                Log.e("eee", "End");
                System.gc();
                Runtime.getRuntime().gc();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private AlertDialog alertDialogReload;

    public void showDialodDownloadFailed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(self);
        builder.setTitle(R.string.app_name)
                .setCancelable(false)
                .setMessage(R.string.error_LoadBook)
                .setNegativeButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        downloadEpub();
                        download();
                    }
                })
                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((FragmentActivity) self).onBackPressed();
                    }
                });
        alertDialogReload = builder.create();
        //if (this.isVisible()) {
        if (!alertDialogReload.isShowing()) {
            alertDialogReload.show();
        }
//        } else {
//            alertDialogReload.dismiss();
//        }
    }


    private void openEpubContinue() {
//        ObjectReader objectReader = ObjectMapperSingleton.getObjectMapper().reader();
        ReadLocator readLocator = DataStoreManager.getLastReadLocator(chapter);
        if (folioReader == null) {
            folioReader = FolioReader.get().setOnHighlightListener(this).setReadLocatorListener(this);

        }
        setConfig();
        folioReader.openBook(chapter.getSdcardURL());
        folioReader.setReadLocator(readLocator);
    }


    private FolioReader folioReader;

    private void openEpub() {

        if (folioReader == null) {
            folioReader = FolioReader.get().setOnHighlightListener(this).setReadLocatorListener(this);
        }
        setConfig();
        folioReader.openBook(chapter.getSdcardURL());
        Log.e("epub_path", chapter.getSdcardURL());
    }

    private void setConfig() {
//        Config config = new Config()
//                .setAllowedDirection(Config.AllowedDirection.ONLY_VERTICAL)
//                .setDirection(Config.Direction.VERTICAL)
//                .setFont(Constants.FONT_LORA)
//                .setFontSize(2)
//                .setNightMode(true)
//                .setThemeColorRes(R.color.red)
//                .setShowTts(true);
//
//        folioReader.setConfig(config, true);
    }

    @Override
    public void onHighlight(HighLight highlight, HighLight.HighLightAction type) {

    }


//    @Override
//    public void saveReadPosition(ReadPosition readPosition) {
//        // DataStoreManager.saveReadPosition(chapter, readPosition.toJson());
//
//    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        FolioReader.clear();
    }

    @Override
    public void saveReadLocator(ReadLocator readLocator) {

    }
}
