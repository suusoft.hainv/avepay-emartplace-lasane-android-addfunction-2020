package com.aveplus.avepay_cpocket.music.base.view;

import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.listener.IBaseListener;
import com.aveplus.avepay_cpocket.music.listener.IDataChangedListener;

import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public abstract class BaseListActivityBinding extends BaseActivityBinding implements IDataChangedListener, IBaseListener {

    private BaseViewModelList mViewModel;
    public RecyclerView recyclerView;

    protected abstract void setUpRecyclerView(RecyclerView recyclerView);

    @Override
    protected int getLayoutInflate() {
        return R.layout._base_list_music;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        // set viewmodel
        mViewModel = (BaseViewModelList) viewModel;
        mViewModel.setDataListener(this);

        // set bindding
        this.binding = binding;
        this.binding.setVariable(BR.viewModel, viewModel);

        // get recyclerView and set
        recyclerView = (RecyclerView) binding.getRoot().findViewById(R.id.rcv_data);
        recyclerView.setLayoutManager(mViewModel.getLayoutManager());
        setUpRecyclerView(recyclerView);
    }

    /**
     * listener #BaseViewmodel.notifyDataChanged()
     * and refresh adapter
     */
    @Override
    public void onListDataChanged(List<?> data, boolean isAppend) {
        BaseAdapterBinding adapter = (BaseAdapterBinding) recyclerView.getAdapter();
        adapter.setItems(data, isAppend);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.onDestroy();
    }
}
