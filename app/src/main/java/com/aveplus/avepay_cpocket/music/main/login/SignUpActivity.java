package com.aveplus.avepay_cpocket.music.main.login;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.Args;
import com.aveplus.avepay_cpocket.music.configs.GlobalFunctions;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.model.DataPart;
import com.aveplus.avepay_cpocket.music.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.music.retrofit.response.ResponseLogin;
import com.aveplus.avepay_cpocket.music.util.NetworkUtility;
import com.aveplus.avepay_cpocket.music.util.StringUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewCondensedItalic;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private static final int RC_ADDRESS = 134;
    private final String TAG = SignUpActivity.class.getSimpleName();
    private LinearLayout llSignUp;
    private RelativeLayout rlSignUp;

    private LinearLayout llStep1;

    private CircleImageView imgAvatar;
    private ImageView imgSignUp;
    private EditText mTxtFullName, mTxtEmail, mTxtPassword, mTxtRetypePassword;


    private String mFullName, mEmail, mPassword, mRetypePassword;

    private DataPart avatar = null;

    private TextView mLblCreateAccount;
    private TextViewCondensedItalic mLblAlreadyAMember;
    private CheckBox mChkRememberMe;
    private TextView tvPhoneCode;
    private boolean mIsRegistered;
    private Dialog dialogGender;
    private RadioButton rbFemale, rbMale;
    private ProgressBar progress;


    private String countryCodeSelected = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    void inflateLayout() {
        setContentView(R.layout.activity_sign_up_reskin_scroll_music);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    void initUI() {
        // Hide actionbar
        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        llSignUp = findViewById(R.id.ll_sign_up);
        rlSignUp = findViewById(R.id.rl_sign_up);
        imgAvatar = findViewById(R.id.img_avatar);

        mLblCreateAccount = findViewById(R.id.lbl_create_account);
        mLblAlreadyAMember = findViewById(R.id.lbl_already_a_member);
        mChkRememberMe = findViewById(R.id.chk_remember_me);
        tvPhoneCode = findViewById(R.id.tv_phone_code);
        llSignUp.getLayoutParams().height = AppUtil.getScreenHeight(this) - AppUtil.getStatusBarHeight(this);

        llStep1 = findViewById(R.id.llStep1);

        progress = findViewById(R.id.progress);


        mTxtFullName = findViewById(R.id.txt_full_name);
        mTxtEmail = findViewById(R.id.txt_email);
        mTxtPassword = findViewById(R.id.txt_password);
        mTxtRetypePassword = findViewById(R.id.txt_retype_password);
        showScreenStep1();
        getCountryCode();
    }


    private void showScreenStep1() {
        mLblCreateAccount.setText(R.string.start_now);

        llStep1.setVisibility(View.VISIBLE);

    }

    @Override
    void initControl() {
        mLblCreateAccount.setOnClickListener(this);
        mLblAlreadyAMember.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == imgAvatar) {
            chooseImage();
        } else if (v == mLblCreateAccount) {
            //Check processing in steps?
            if (isValidStep1()) {
                progress.setVisibility(View.VISIBLE);
                createAccount();
            }
//            if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS1) {
//                if (isValidStep1())
//                    showScreenStep2();
//            } else if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS2) {
//                if (isValidStep2()){
//                    showScreenStep3();
//
//                }
//
//
//            } else if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS3) {
//                if (isValidStep3()) {
//                    createAccount();
//                }
//            }
        } else if (v == mLblAlreadyAMember) {
            GlobalFunctions.startActivityWithoutAnimation(self, LoginActivity.class);
            finish();
        }
//        else if (v == tvPhoneCode) {
////            tvPhoneCode.setBackgroundResource(R.drawable.bg_border_grey);
//            GlobalFunctions.startActivityForResult(this, PhoneCountryListActivity.class, Args.RQ_GET_PHONE_CODE);
//        }
    }

    private void chooseImage() {
        AppUtil.pickImage(this, AppUtil.PICK_IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == AppUtil.PICK_IMAGE_REQUEST_CODE) {
                AppUtil.setImageFromUri(imgAvatar, data.getData());
            } else if (requestCode == Args.RQ_GET_PHONE_CODE) {
                countryCodeSelected = data.getExtras().getString(Args.KEY_PHONE_CODE);
                tvPhoneCode.setText(countryCodeSelected);
            } else if (requestCode == RC_ADDRESS) {
//                if (resultCode == -1) {
//                    Place place = PlaceAutocomplete.getPlace(self, data);
//                    AppUtil.fillAddress(self, mTxtAddress, place);
//                    mTxtZipcode.requestFocus();
//                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
//                    Status status = PlaceAutocomplete.getStatus(self, data);
//                    Log.e(TAG, status.getStatusMessage());
//                    mTxtAddress.requestFocus();
//                } else if (resultCode == 0) {
//                    mTxtAddress.requestFocus();
//                    // The user canceled the operation.
//                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        GlobalFunctions.startActivityWithoutAnimation(self, SplashLoginActivity.class);
        finish();
    }

    private void getCountryCode() {
        String[] rl = getResources().getStringArray(R.array.CountryCodes);
        //int curPosition = AppUtil.getCurentPositionCountryCode(this);
        int curPosition = 0;
        String phoneCode = rl[curPosition].split(",")[0];

    }

    private void gotoLoginPage() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Args.IS_FROM_SIGNUP, true);
        if (mIsRegistered) {
            String email = mTxtEmail.getText().toString().trim();
            String password = mTxtPassword.getText().toString().trim();

            if (!email.isEmpty()) {
                bundle.putString(Args.EMAIL, email);
            }
            if (!password.isEmpty()) {
                bundle.putString(Args.PASSWORD, password);
            }

        }

        GlobalFunctions.startActivityWithoutAnimation(self, LoginActivity.class, bundle);
        finish();
    }


    private void createAccount() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            ApiUtils.getAPIService().register(mFullName, mEmail, mPassword).enqueue(new Callback<ResponseLogin>() {
                @Override
                public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                    //  showProgress (false);
                    progress.setVisibility(View.GONE);
                    if (response.body() != null) {
                        if (response.body().isSuccess(self)) {
                            UserObj userObj = response.body().getData();
                            mIsRegistered = true;
                            userObj.setName(mFullName);
                            userObj.setEmail(mEmail);
                            userObj.setPassWord(mPassword);
                            userObj.setRememberMe(true);
                            DataStoreManager.saveUser(userObj);
//                                        AddressManager.getInstance().getArray().clear();
//                                        AddressManager.getInstance().addItem(new Person(mFullName, phone, mAddress));
                            Toast.makeText(self, R.string.msg_register_success, Toast.LENGTH_LONG).show();
                            gotoLoginPage();
                        } else {
                            Toast.makeText(self, R.string.some_thing_err, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(self, R.string.some_thing_err, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseLogin> call, Throwable t) {
                    //   showProgress (false);
                    progress.setVisibility(View.GONE);
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(self, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(self, getString(R.string.msg_no_network), Toast.LENGTH_SHORT).show();
        }

    }

    private boolean isValidStep1() {
        mFullName = mTxtFullName.getText().toString().trim();
        mEmail = mTxtEmail.getText().toString().trim();
        mPassword = mTxtPassword.getText().toString().trim();
        mRetypePassword = mTxtRetypePassword.getText().toString().trim();

        if (mFullName.isEmpty()) {
            AppUtil.showToast(self, R.string.msg_name_is_required);
            mTxtFullName.requestFocus();

            return false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            AppUtil.showToast(self, R.string.msg_email_is_required);
            mTxtEmail.requestFocus();

            return false;
        }

        if (mPassword.length() < 6) {
            AppUtil.showToast(self, R.string.msg_password_not_valid);
            mTxtPassword.requestFocus();

            return false;
        }
        if (!mRetypePassword.equals(mPassword)) {
            AppUtil.showToast(self, R.string.msg_password_is_not_match);
            mTxtRetypePassword.requestFocus();

            return false;
        }

        return true;
    }

    private boolean isValidStep2() {


//        if (mZipcode.isEmpty()) {
//            AppUtil.showToast(self, R.string.msg_zipcode_is_required);
//            mTxtZipcode.requestFocus();
//
//            return false;
//        }
        return true;
    }

    private boolean isValidStep3() {
        if (imgAvatar.getDrawable() != getResources().getDrawable(R.drawable.ic_cam)) {
            avatar = new DataPart("avatar.png", AppUtil.getFileDataFromDrawable(self, imgAvatar.getDrawable()), DataPart.TYPE_IMAGE);
        }


        if (imgAvatar == null) {
            AppUtil.showToast(self, R.string.msg_image_is_required);

            return false;
        }


        return true;
    }

    private boolean isValid() {
        String fullName = mTxtFullName.getText().toString().trim();
        String email = mTxtEmail.getText().toString().trim();

        String password = mTxtPassword.getText().toString().trim();
        String retypePassword = mTxtRetypePassword.getText().toString().trim();

        if (fullName.isEmpty()) {
            Toast.makeText(self, R.string.msg_name_is_required, Toast.LENGTH_SHORT).show();
            mTxtFullName.requestFocus();

            return false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(self, R.string.msg_email_is_required, Toast.LENGTH_SHORT).show();
            mTxtEmail.requestFocus();

            return false;
        }

        if (!StringUtil.isValidatePassword(password)) {
            Toast.makeText(self, R.string.msg_password_is_required, Toast.LENGTH_LONG).show();
            mTxtPassword.requestFocus();

            return false;
        }
        if (!retypePassword.equals(password)) {
            Toast.makeText(self, R.string.msg_password_is_not_match, Toast.LENGTH_SHORT).show();
            mTxtRetypePassword.requestFocus();

            return false;
        }

        return true;
    }

    private void getListCode() {

        Map<String, String> languagesMap = new TreeMap<>();

        Locale[] locales = Locale.getAvailableLocales();

        for (Locale obj : locales) {

            if ((obj.getDisplayCountry() != null) && (!"".equals(obj.getDisplayCountry()))) {
                languagesMap.put(obj.getCountry(), obj.getLanguage());
                Log.e(TAG, "country: " + obj.getCountry());
            }

        }
    }


}
