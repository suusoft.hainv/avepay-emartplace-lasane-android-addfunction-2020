package com.aveplus.avepay_cpocket.taxi.interfaces;


import com.aveplus.avepay_cpocket.taxi.objects.book.ServiceObj;

public interface IVehicle {
    void onService(ServiceObj serviceObj);
}
