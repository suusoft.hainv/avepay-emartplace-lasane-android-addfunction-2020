package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.object.ObjectInfoUser;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;


import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FragmentOrderCartStepInfo extends BaseFragment {

    private View view;
    private Button btnContinue;
    private ToggleButton mToggleButton;
    private LinearLayout llParentInfoReceiver;
    private EditText edtNameUser, edtPhoneUser, edtAddressUser, edtPostCodeUser, edtMailUser, edtNameReceiver, edtPhoneReceiver, edtAddressReceiver, edtPostCodeReceiver, edtMailReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_order_step_info, container, false);
        initUI(view);
        initControl();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("ordercart");
        else Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("cart");
        else Log.i(TAG, "onTitleChanged is null");
    }

    private void initUI(View v) {
        btnContinue = (Button) v.findViewById(R.id.btn_continue);
        mToggleButton = (ToggleButton) findViewById(R.id.tgDeliver);
        llParentInfoReceiver = (LinearLayout) findViewById(R.id.llParentInfoReceiver);
        edtNameUser = (EditText) findViewById(R.id.edtUserEmail);
        edtAddressUser = (EditText) findViewById(R.id.edtAddress);
        edtPhoneUser = (EditText) findViewById(R.id.edtPhoneNumber);
        edtPostCodeUser = (EditText) findViewById(R.id.edtPostCode);
        edtMailUser = (EditText) findViewById(R.id.edtEmail);
        edtNameReceiver = (EditText) findViewById(R.id.edtReceiverName);
        edtAddressReceiver = (EditText) findViewById(R.id.edtReceiverAddress);
        edtPhoneReceiver = (EditText) findViewById(R.id.edtReceiverPhoneNumber);
        edtPostCodeReceiver = (EditText) findViewById(R.id.edtReceiverPostCode);
        edtMailReceiver = (EditText) findViewById(R.id.edtReceiverEmail);
        setOnClick();

    }

    private void initControl() {
        mToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    llParentInfoReceiver.setVisibility(View.GONE);
                } else {
                    llParentInfoReceiver.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private boolean validate() {

        Pattern pattern = Patterns.EMAIL_ADDRESS;

        if (edtNameUser.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.msg_field_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtPhoneUser.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.msg_field_phone), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtAddressUser.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.msg_field_address), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtPostCodeUser.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.msg_field_postal), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (edtMailUser.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.msg_field_email), Toast.LENGTH_SHORT).show();
            return false;
        }
        Matcher isUserMail = pattern.matcher(edtMailUser.getText().toString());
        if (!isUserMail.matches()) {
            Toast.makeText(getActivity(), getString(R.string.msg_field_email_invalid), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (llParentInfoReceiver.isShown()) {
            if (edtNameReceiver.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.msg_field_recieve_name), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (edtPhoneReceiver.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.msg_field_recieve_phone), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (edtAddressReceiver.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.msg_field_recieve_address), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (edtPostCodeReceiver.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.msg_field_recieve_postal), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (edtMailReceiver.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), getString(R.string.msg_field_recieve_mail), Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        if (llParentInfoReceiver.isShown()) {
            Matcher isReceiverMail = pattern.matcher(edtMailUser.getText().toString());
            if (!isReceiverMail.matches()) {
                Toast.makeText(getActivity(), getString(R.string.msg_field_email_invalid), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    private View findViewById(int idRes) {
        return view.findViewById(idRes);
    }

    public void setOnClick() {
        btnContinue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    ObjectInfoUser objectInfoUser = new ObjectInfoUser();
                    objectInfoUser.setNameUser(edtNameUser.getText().toString());
                    objectInfoUser.setAddressUser(edtAddressUser.getText().toString());
                    objectInfoUser.setPhoneUser(edtPhoneUser.getText().toString());
                    objectInfoUser.setPostCodeUser(edtPostCodeUser.getText().toString());
                    objectInfoUser.setMailUser(edtMailUser.getText().toString());

                    objectInfoUser.setNameReceiver(edtNameReceiver.getText().toString());
                    objectInfoUser.setAddressReceiver(edtAddressReceiver.getText().toString());
                    objectInfoUser.setPhoneReceiver(edtPhoneReceiver.getText().toString());
                    objectInfoUser.setPostCodeReceiver(edtPostCodeReceiver.getText().toString());
                    objectInfoUser.setMailReceiver(edtMailReceiver.getText().toString());
                    FragmentOrderCartStepConfirm fragment = new FragmentOrderCartStepConfirm();
                    Bundle bundle = getArguments();
                    bundle.putParcelable("object", objectInfoUser);
                    fragment.setArguments(bundle);
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_left,
                                    R.anim.slide_out_left)
                            .replace(R.id.layout_replace, fragment, "showPayment")
                            .addToBackStack(null).commit();
                }

            }
        });

    }

    @Override
    public void onDestroy() {
        self.stopService(new Intent(self, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data
                    .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("paymentExample",
                            "Payment OK. Response :" + confirm.toJSONObject());
                    JSONObject json = confirm.toJSONObject();
                } catch (Exception e) {
                    Log.e("paymentExample",
                            "an extremely unlikely failure occurred: ", e);
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample",
                    "An invalid payment was submitted. Please see the docs.");
        }
    }


    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        for (Fragment frag : fm.getFragments()) {
            if (frag != null) {
                getActivity().overridePendingTransition(R.anim.slide_in_left,
                        R.anim.slide_out_right);
                if (frag.isVisible()) {
                    FragmentManager childFm = frag.getChildFragmentManager();
                    if (childFm.getBackStackEntryCount() > 0) {
                        childFm.popBackStack(null,
                                FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        childFm.beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_left,
                                        R.anim.slide_out_right).commit();
                        return;
                    }
                }
            }
        }

        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            new AlertDialog.Builder(getActivity())
                    .setMessage(getString(R.string.quit_app_message))
                    .setPositiveButton(getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(
                                            R.anim.slide_in_left,
                                            R.anim.slide_out_right);
                                }
                            }).setNegativeButton(getString(R.string.no), null)
                    .show();
        }
    }
}
