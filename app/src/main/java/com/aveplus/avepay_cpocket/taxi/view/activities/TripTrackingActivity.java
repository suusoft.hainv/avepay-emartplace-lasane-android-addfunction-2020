package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.configs.SizeScreen;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.globals.Constants;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.interfaces.IConfirmation;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.SettingsObj;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.objects.book.DriverObj;
import com.aveplus.avepay_cpocket.taxi.parsers.JSONParser;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.DialogUtil;
import com.aveplus.avepay_cpocket.taxi.utils.NetworkUtility;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.direction.GMapV2Direction;
import com.aveplus.avepay_cpocket.taxi.utils.map.direction.GetRouteListTask;
import com.aveplus.avepay_cpocket.taxi.view.instant.InfoDriverFragment;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class TripTrackingActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        GMapV2Direction.DirecitonReceivedListener {

    private static final String TAG = TripTrackingActivity.class.getSimpleName();

    public static final int RQ_TRACKING = 1000;
    private static final int RC_LOCATION = 1;
    private static final int RC_TURN_ON_LOCATION = 2;
    private static final int RC_DEAL_COMPLETED = 999;
    public static final int RESULT_CANCEL_DEAL = 1000;

    private InfoDriverFragment fragment;
    private GoogleMap mMap;

    // Use google api to get current location
   // private GoogleApiClient mGoogleApiClient;
    protected Location mMyLocation;

    private Marker mDriverMarker, mFromMarker, mToMarker;

    private TripObj mTripObj;

    private Timer mTimer;

    private TextViewRegular mLblDealCompleted;

    private View frgDetailTrip;
    private View btnDetail, btnBack;


    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_trip_tracking1;
    }

    @Override
    protected void getExtraData(Intent intent) {

        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Args.KEY_TRIP_OJBECT)) {
                mTripObj = bundle.getParcelable(Args.KEY_TRIP_OJBECT);
                Log.e("transportObj", "transportObj = " + new Gson().toJson(mTripObj));
            }
        }

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {
        SizeScreen.init(this);
        initGoogleApiClient();
        DataStoreManager.saveTrackingTrip(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btnBack = findViewById(R.id.btn_back);
        btnDetail = findViewById(R.id.btn_detail);
        mLblDealCompleted = (TextViewRegular) findViewById(R.id.lbl_completed);

        initControl();


        frgDetailTrip = findViewById(R.id.fragment);
        fragment =  InfoDriverFragment.newInstance(mTripObj);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fr_detail_trip, fragment).commit();

        showDetailTrip();


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailTrip();
            }
        });

        getTripDetail();

    }

    private void initControl() {
        mLblDealCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mTripObj != null) {

                    showRatingDialog();
                }
            }
        });

    }

    private void showRatingDialog() {
        final Dialog dialog = DialogUtil.setDialogCustomView(self,R.layout.dialog_rating, false );

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.rating_bar);
        final EditText txtComment = (EditText) dialog.findViewById(R.id.txt_comment);
        TextViewBold lblSubmit = (TextViewBold) dialog.findViewById(R.id.lbl_submit);
        ((LayerDrawable) ratingBar.getProgressDrawable()).getDrawable(2)
                .setColorFilter(ratingBar.getContext().getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);


        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rate, boolean b) {
                if (rate < 1) {
                    ratingBar.setRating(1);
                }
            }
        });

        lblSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratingBar.getRating() == 0) {
                    Toast.makeText(self, R.string.rating_is_required, Toast.LENGTH_SHORT).show();
                } else if (txtComment.getText().toString().trim().equals("")) {
                    Toast.makeText(self, R.string.comment_is_required, Toast.LENGTH_SHORT).show();
                    txtComment.requestFocus();
                } else {
                    dialog.dismiss();

                    payDeal(ratingBar.getRating() * 2, txtComment.getText().toString().trim());
                }
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void payDeal(float rate, String comment) {

        AppUtil.hideSoftKeyboard(this);
        if (com.aveplus.avepay_cpocket.network1.NetworkUtility.getInstance(self).isNetworkAvailable()) {

            if (mTripObj != null) {


                RequestManager.finishTripInstantAndDeal(self,  mTripObj, null, null, rate, comment,
                        new BaseRequest.CompleteListener() {
                            @Override
                            public void onSuccess(ApiResponse response) {

                                EventBus.getDefault().post(new Global.FinishAllScreenProcessingTrip());

                                if (mTripObj.isInstantBooking())
                                    AppUtil.startActivity(self, EarnBonusActivity.class);

                                finish();

                            }

                            @Override
                            public void onError(String message) {

                                AppUtil.showToast(self, message);
                            }
                        });


            }
        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_share, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_share) {
            AppUtil.shareTrip(self, mTripObj);
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_TURN_ON_LOCATION) {
            if (MapsUtil.locationIsEnable(self)) {
                if (GlobalFunctions.locationIsGranted(self, RC_LOCATION, null)) {
                    initMyLocation();
                }
            } else {
                turnOnLocationReminder(RC_TURN_ON_LOCATION, false);
            }
        } else if (requestCode == RC_DEAL_COMPLETED) {
            if (resultCode == RESULT_OK) {
                // Close this activity if the payment is success
                setResult(RESULT_OK);
                onBackPressed();


                //finish khi app đang ở màn Trip finish thì driver cancel deal
            } else if (resultCode ==RESULT_CANCEL_DEAL){
                finish();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_LOCATION: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (MapsUtil.locationIsEnable(self)) {
                            initMyLocation();
                        } else {
                            turnOnLocationReminder(RC_TURN_ON_LOCATION, false);
                        }
                    }
                }
                break;
            }

            case InfoDriverFragment.RC_PHONE_CALL_DRIVER: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //callPhoneDriver();
                    } else {
                        Toast.makeText(self, R.string.msg_remind_user_grants_permissions, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    protected void onStart() {
       // mGoogleApiClient.connect();
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        //mGoogleApiClient.disconnect();
        super.onStop();

        // Stopping refresh events action.
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                MapsUtil.moveCameraShowAllLatLng(mMap, mTripObj.getLatLngPickup(), mTripObj.getLatLngDestination());
            }
        });


        initRoute();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Update driver's location
        updateDriverLocation();

        if (GlobalFunctions.locationIsGranted(self, RC_LOCATION, null)) {
            if (MapsUtil.locationIsEnable(self)) {
                //mMyLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                initMyLocation();
            } else {
                MapsUtil.displayLocationSettingsRequest((AppCompatActivity) self, RC_TURN_ON_LOCATION);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void OnDirectionListReceived(List<LatLng> mPointList) {
        if (mPointList != null) {
            PolylineOptions rectLine = new PolylineOptions().width(15).color(ContextCompat.getColor(self, R.color.colorAccent));
            for (int i = 0; i < mPointList.size(); i++) {
                rectLine.add(mPointList.get(i));
            }
            mMap.addPolyline(rectLine);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        EventBus.getDefault().unregister(this);
    }

    private void getTripDetail() {
        RequestManager.tripDetail(mTripObj.getId(), new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (!response.isError()) {
                    mTripObj = response.getDataObject(TripObj.class);
                    setData();

                }
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    private void setData() {
        fragment.setData(mTripObj);

    }


    private void showDetailTrip() {
        frgDetailTrip.setVisibility(View.VISIBLE);
    }


    public void hideDetailTrip(){
        frgDetailTrip.setVisibility(View.GONE);
    }


    private void initGoogleApiClient() {
//        if (mGoogleApiClient == null) {
//            mGoogleApiClient = new GoogleApiClient.Builder(this)
//                    .addConnectionCallbacks(this)
//                    .addOnConnectionFailedListener(this)
//                    .addApi(LocationServices.API)
//                    .build();
//        }
    }

    private Marker addMarker(Marker marker, LatLng latLng, int icon, String title) {
        // Clear old marker if it's
        if (marker != null) {
            marker.remove();
        }

        return MapsUtil.addMarker(mMap, latLng, title, icon, false);
    }

    private void initRoute() {
        if (mTripObj != null) {
            // Add pickup maker
            mFromMarker = addMarker(mFromMarker, mTripObj.getLatLngPickup(), R.drawable.ic_place_accent,
                    mTripObj.getPickup());
            MapsUtil.moveCameraTo(mMap, mTripObj.getLatLngPickup(), 14);

            // Add destination maker
            mToMarker = addMarker(mToMarker, mTripObj.getLatLngDestination(), R.drawable.ic_place_to,
                    mTripObj.getDestination());

            // Draw polyline
            new GetRouteListTask(self, mTripObj.getLatLngPickup(),
                    mTripObj.getLatLngDestination(), GMapV2Direction.MODE_DRIVING, this)
                    .execute();
        }
    }

    private void initMyLocation() {

        if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }

    }

    private void updateDriverLocation() {
        if (mTripObj.getDriverId()!=null)
        if (!mTripObj.getDriverId().equals(DataStoreManager.getUser().getId())) {
            if (mTimer == null) {
                mTimer = new Timer();

                mTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getDriverLocation();
                            }
                        });
                    }
                }, 0, Constants.TIME_TO_UPDATE);
            }
        }
    }

    private void getDriverLocation() {
        if (NetworkUtility.isNetworkAvailable()) {
            ModelManager.getDriverLocation(self, mTripObj.getId(), new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    JSONObject jsonObject = (JSONObject) object;
                    if (JSONParser.responseIsSuccess(jsonObject)) {
                        try {
                            JSONObject data = jsonObject.getJSONObject("data");
                            LatLng latLng = new LatLng(data.optDouble("lat"), data.optDouble("long"));
                            mDriverMarker = addMarker(mDriverMarker, latLng, getTransportIcon(mTripObj), "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else if (JSONParser.isTokenMissmatch(jsonObject)) {
                        com.aveplus.avepay_cpocket.taxi.utils.DialogUtil.showDialogTokenMismatch(self);
                    } else {
                        Toast.makeText(self, JSONParser.getMessage(jsonObject), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onError() {
                }
            });
        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }

    public static void start(Activity activity, TripObj tripObj) {
        if (tripObj != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
            GlobalFunctions.startActivityWithoutAnimation(activity, com.aveplus.avepay_cpocket.view.activities.TripTrackingActivity.class, bundle);
        }
    }

    public static void startForResult(Activity activity, TripObj tripObj, int reqCode) {
        if (tripObj != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
            Intent intent = new Intent(activity, com.aveplus.avepay_cpocket.view.activities.TripTrackingActivity.class);
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, reqCode);
        }
    }

    private void confirmDriverActivateAgain() {
        GlobalFunctions.showConfirmationDialog(self, getString(R.string.msg_ask_driver_activate_again),
                getString(R.string.yes), getString(R.string.no), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        changeDriverMode(Constants.ON, 0);
                    }

                    @Override
                    public void onNegative() {
                        Toast.makeText(self, R.string.msg_remind_driver_activate_again, Toast.LENGTH_LONG).show();

                        setResult(RESULT_OK);
                        onBackPressed();
                    }
                });
    }

    private void changeDriverMode(final String mode, int duration) {
        if (NetworkUtility.isNetworkAvailable()) {
            ModelManager.activateDriverMode(self, mode, duration, new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    JSONObject jsonObject = (JSONObject) object;

                    if (JSONParser.responseIsSuccess(jsonObject)) {
                        if (mode.equals(Constants.OFF)) {
                            // Set driver is unavailable
                            UserObj userObj = DataStoreManager.getUser();
                            userObj.getDriverData().setAvailable(DriverObj.DRIVER_UNAVAILABLE);
                            DataStoreManager.saveUser(userObj);
                            Toast.makeText(self, R.string.msg_deactivate_success, Toast.LENGTH_SHORT).show();
                        } else {
                            // Set driver is available
                            UserObj userObj = DataStoreManager.getUser();
                            userObj.getDriverData().setAvailable(DriverObj.DRIVER_AVAILABLE);
                            DataStoreManager.saveUser(userObj);

                            Toast.makeText(self, R.string.msg_activate_success, Toast.LENGTH_SHORT).show();
                        }

                        setResult(RESULT_OK);
                        onBackPressed();
                    } else {
                        if (mode.equals(Constants.ON)) {
                            showDurationBuyingDialog(R.string.msg_enter_duration_to_be_available);
                        }
                    }
                }

                @Override
                public void onError() {
                }
            });
        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }

    private void showDurationBuyingDialog(int msgId) {
        final Dialog dialog  = DialogUtil.setDialogCustomView(self, R.layout.dialog_buying_duration, true);

        final EditText txtDuration = (EditText) dialog.findViewById(R.id.txt_duration);
        TextViewRegular lblMsg = (TextViewRegular) dialog.findViewById(R.id.lbl_msg);
        final TextViewRegular lblFee = (TextViewRegular) dialog.findViewById(R.id.lbl_msg_fee);
        TextViewBold lblBuyCredits = (TextViewBold) dialog.findViewById(R.id.lbl_buy_credits);
        TextViewBold lblAvailable = (TextViewBold) dialog.findViewById(R.id.lbl_available);

        lblMsg.setText(msgId);
        lblFee.setText(String.format(getString(R.string.msg_subtract_credits), "0"));

        SettingsObj settingsObj = DataStoreManager.getSettingUtility();
        final int feePerHour = (settingsObj != null && settingsObj.getDeal_online_rate() != null
                && !settingsObj.getDeal_online_rate().equals("")) ?
                Integer.parseInt(settingsObj.getDeal_online_rate()) : 1;

        txtDuration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String duration = editable.toString().trim().isEmpty() ? "0" : editable.toString().trim();

                lblFee.setText(String.format(getString(R.string.msg_subtract_credits), String.valueOf((Integer.parseInt(duration) * feePerHour))));
            }
        });

        lblBuyCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                GlobalFunctions.startActivityWithoutAnimation(self, BuyCreditsActivity.class);
            }
        });

        lblAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int duration = 0;
                if (txtDuration.getText().toString().trim().length() > 0) {
                    duration = Integer.parseInt(txtDuration.getText().toString().trim());
                }

                if (duration == 0 || duration > 24) {
                    Toast.makeText(self, R.string.msg_duration_must_gt_zero, Toast.LENGTH_LONG).show();
                } else {
                    if (DataStoreManager.getUser().getBalance() < duration) {
                        Toast.makeText(self, String.format(getString(R.string.msg_balance_is_not_enough),
                                String.valueOf(duration)), Toast.LENGTH_LONG).show();
                    } else {
                        dialog.dismiss();

                        changeDriverMode(Constants.DURATION_BUYING, duration);
                    }
                }
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.FinishAllCurrentAct event) {
        finish();
    };


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.PushClickBtnFinishTrip event) {
        mLblDealCompleted.performClick();
    };

}
