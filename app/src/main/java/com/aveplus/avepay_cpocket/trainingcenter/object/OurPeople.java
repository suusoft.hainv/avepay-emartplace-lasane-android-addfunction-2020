package com.aveplus.avepay_cpocket.trainingcenter.object;

public class OurPeople {
	String id;
	String name;
	String image;
	String position;
	String email;
	String skype;
	String description;

	public OurPeople(String id, String name, String image, String position,
                     String email, String skype, String description) {
		super();
		this.id = id;
		this.name = name;
		this.image = image;
		this.position = position;
		this.email = email;
		this.skype = skype;
		this.description = description;
	}

	public OurPeople() {
		// TODO Auto-generated constructor stub
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

}
