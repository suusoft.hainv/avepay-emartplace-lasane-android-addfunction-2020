package com.aveplus.avepay_cpocket.retrofit;

import com.aveplus.avepay_cpocket.music.retrofit.param.Param;
import com.aveplus.avepay_cpocket.objects.reservationsold.ReservationSoldResponse;
import com.aveplus.avepay_cpocket.retrofit.response.BaseReponse;
import com.aveplus.avepay_cpocket.retrofit.response.ResponseBlogRecent;
import com.aveplus.avepay_cpocket.retrofit.response.ResponseListFaq;
import com.aveplus.avepay_cpocket.retrofit.response.ResponseUser;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIService {

    @GET("service")
    @Headers("Cache-Control: no-cache")
    Call<ResponseListFaq> List_Faq(
            @Query(Param.PARAM_PAGE) String page

    );

    @GET("news")
    @Headers("Cache-Control: no-cache")
    Call<ResponseBlogRecent> List_Blog_Recent(
            @Query(Param.PARAM_PAGE) String page,
            @Query(Param.PARAM_TYPE) String type
    );

    @POST("user/login")
    @Headers("Cache-Control: no-cache")
    Call<ResponseUser> login(@Query(Param.PARAM_USERNAME) String username,
                             @Query(Param.PARAM_PASSWORD) String password,
                             @Query(Param.PARAM_LOGIN_TYPE) String login_type,
                             @Query(Param.PARAM_NAME) String name
    );

    @POST("user/logout")
    @Headers("Cache-Control: no-cache")
    Call<BaseReponse> logout(@Query(Param.PARAM_TOKEN) String token);

    @Multipart
    @POST("user/register")
    @Headers("Cache-Control: no-cache")
    Call<ResponseUser> register(
            @Part(Param.PARAM_NAME) RequestBody name,
            @Part(Param.PARAM_USERNAME) RequestBody username,
            @Part(Param.PARAM_PASSWORD) RequestBody password,
            @Part(Param.PARAM_PHONE) RequestBody phone,
            @Part(Param.PARAM_ADDRESS) RequestBody address,
            @Part(Param.PARAM_GENDER) RequestBody gender,
            @Part(Param.PARAM_BIRTHDAY) RequestBody birthday,
            @Part MultipartBody.Part avatar);


    @Multipart
    @POST("user/update-profile")
    @Headers("Cache-Control: no-cache")
    Call<ResponseUser> update_profile(@Part(Param.PARAM_TOKEN) RequestBody token,
                                      @Part(Param.PARAM_NAME) RequestBody name,
                                      @Part(Param.PARAM_GENDER) RequestBody gender,
                                      @Part(Param.PARAM_PHONE) RequestBody phone,
                                      @Part(Param.PARAM_ADDRESS) RequestBody address,
                                      @Part MultipartBody.Part avatar
    );

    @POST("user/profile")
    @Headers("Cache-Control: no-cache")
    Call<ResponseUser> profile(@Query(Param.PARAM_TOKEN) String token

    );

    @GET("reservation/list")
    @Headers("Cache-Control: no-cache")
    Call<ReservationSoldResponse> getReserSold(@Query("token") String token, @Query("search_type") String searchType, @Query("deal_id") String dealId, @Query("page") int page);

};


