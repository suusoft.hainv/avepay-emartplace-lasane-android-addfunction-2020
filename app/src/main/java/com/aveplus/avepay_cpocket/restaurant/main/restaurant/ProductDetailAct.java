package com.aveplus.avepay_cpocket.restaurant.main.restaurant;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.listener.IOnClickListener;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.FoodActivity;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.ProductImage;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.RandomFood;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.Review;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.review.AdapteReview;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.toporder.AdapterTopOrder;
import com.aveplus.avepay_cpocket.restaurant.model.AdapterViewPageImage;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponseReview;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.util.StringUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.MyProgressDialog;
import com.aveplus.avepay_cpocket.restaurant.widgets.recyclerview.EndlessRecyclerOnScrollListener;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailAct extends BaseActivity implements View.OnClickListener, EndlessRecyclerOnScrollListener.OnLoadMoreListener {
    private CollapsingToolbarLayout collapseToolbar;
    private RelativeLayout imgListProduct;
    private ViewPager viewpgListProduct;
    private CircleIndicator circleIndicator;
    private RelativeLayout rlNodata;
    private ImageView img;
    private RelativeLayout rlBgNoImage;
    private Toolbar toolbarProduct;
    private ImageView imgBack;
    private NestedScrollView nestedContent;
    private TextView tvTitleProduct;
    private RelativeLayout lnRate;
    private RatingBar ratingBar;
    private TextView tvRateCount;
    private TextView tvCountPeople;
    private TextView tvPrice;
    private TextView tvOldPrice;
    private Button btnRate;
    private RelativeLayout productContent;
    private TextView content;
    private TextView tvContent;
    private TextView tvRateReview;
    private RecyclerView rcvData;
    private RecyclerView rcvProductNearby;
    private LinearLayout llNoData;

    private AdapterViewPageImage mAdapter;
    private RandomFood product;
    private ArrayList<RandomFood> listProduct;
    private List<ProductImage> product_image;

    private AdapteReview adapteReview;
    private AdapterTopOrder adapterTopOrder;
    private EndlessRecyclerOnScrollListener onScrollListener;

    private double rate = 0.0;
    private int rate_count = 0;
    private List<Review> reviewList;

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_detail_product;
    }


    @Override
    protected void initView() {
        collapseToolbar = findViewById(R.id.collapse_toolbar);
        imgListProduct = findViewById(R.id.img_list_product);
        viewpgListProduct = findViewById(R.id.viewpg_list_product);
        circleIndicator = findViewById(R.id.circle_indicator);
        rlNodata = findViewById(R.id.rl_nodata);
        img = findViewById(R.id.img);
        rlBgNoImage = findViewById(R.id.rl_bgNoImage);
        toolbarProduct = findViewById(R.id.toolbar_product);
        imgBack = findViewById(R.id.img_back);
        nestedContent = findViewById(R.id.nested_content);
        tvTitleProduct = findViewById(R.id.tv_title_product);
        lnRate = findViewById(R.id.ln_rate);
        ratingBar = findViewById(R.id.rating);
        tvRateCount = findViewById(R.id.tv_rate_count);
        tvCountPeople = findViewById(R.id.tv_count_people);
        tvPrice = findViewById(R.id.tv_price);
        tvOldPrice = findViewById(R.id.tv_old_price);
        btnRate = findViewById(R.id.btn_rate);
        productContent = findViewById(R.id.product_content);
        content = findViewById(R.id.content);
        tvContent = findViewById(R.id.tv_content);
        tvRateReview = findViewById(R.id.tv_rate_review);
        rcvData = findViewById(R.id.rcv_data);
        rcvProductNearby = findViewById(R.id.rcv_product_nearby);
        llNoData = findViewById(R.id.ll_no_data);

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onViewCreated() {
        setPageAdapter();
        initAdapter();

        if (product != null) {
            setDataProduct(product);
        }
        collapseToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
        nestedContent.scrollTo(0, 0);
        nestedContent.requestFocus();
        imgBack.setOnClickListener(this);
        btnRate.setOnClickListener(this);
        AppUtil.setRatingbarColor(ratingBar);
    }

    @SuppressLint("SetTextI18n")
    private void setDataProduct(RandomFood productObj) {

        Log.e("TAG", "onViewCreated: " + new Gson().toJson(productObj));
        if (productObj.getName() != null) {
            collapseToolbar.setTitle(productObj.getName());
            tvTitleProduct.setText(productObj.getName());
        }
        if (productObj.getContent() != null && !productObj.getContent().isEmpty()) {
            tvContent.setText(productObj.getContent());
        } else {
            tvContent.setVisibility(View.GONE);
            content.setVisibility(View.GONE);
        }
        if (productObj.getSale_price() != null && productObj.getSale_price() > 0) {
            tvPrice.setText(StringUtil.formatPrice(productObj.getSale_price()));
            tvOldPrice.setText(StringUtil.formatPrice(productObj.getPrice()));
        } else if (productObj.getPrice() != null) {
            tvPrice.setText(StringUtil.formatPrice(productObj.getPrice()));
        }
        if (productObj.getRate_count() != null) {
            rate_count = productObj.getRate_count();
            tvCountPeople.setText("(" + rate_count + " rating)");
        }
        if (productObj.getRate() != null) {
            rate = (productObj.getRate() / 2);
            ratingBar.setRating(Float.parseFloat(productObj.getRate().toString()) / 2);
            tvRateCount.setText(rate + "/5.0");
        }
        if (productObj.getProductImages() != null && productObj.getProductImages().size() > 0) {
            rlNodata.setVisibility(View.GONE);
            rlBgNoImage.setVisibility(View.GONE);
            if (product_image != null) {
                product_image.clear();
            }
            product_image.addAll(productObj.getProductImages());
            mAdapter.notifyDataSetChanged();
        }
        if (productObj.getReviewList() != null && product.getReviewList().size() > 0) {
            if (reviewList != null) {
                reviewList.clear();
            }
            reviewList.addAll(productObj.getReviewList());
            tvRateReview.setVisibility(View.VISIBLE);
            adapteReview.notifyDataSetChanged();
        } else {
            tvRateReview.setVisibility(View.GONE);
        }
    }

    private void setPageAdapter() {
        product_image = new ArrayList<>();
        mAdapter = new AdapterViewPageImage(self, product_image, new IOnClickListener() {
            @Override
            public void onClick(int position) {
                Bundle bundle = new Bundle();
                bundle.putString("img", product_image.get(position).getImage());
                AppUtil.startActivity(self, FoodActivity.class, bundle);
            }
        });
        viewpgListProduct.setAdapter(mAdapter);
        circleIndicator.setViewPager(viewpgListProduct);
        mAdapter.registerDataSetObserver(circleIndicator.getDataSetObserver());
    }

    private void initAdapter() {
        reviewList = new ArrayList<>();
        adapteReview = new AdapteReview(reviewList, self);
        onScrollListener = new EndlessRecyclerOnScrollListener(this);
        rcvData.setLayoutManager(new LinearLayoutManager(self));
        rcvData.setAdapter(adapteReview);
        rcvData.addOnScrollListener(onScrollListener);

        listProduct = RestaurantFrag.randomFoods;
        adapterTopOrder = new AdapterTopOrder(self, listProduct, new AdapterTopOrder.HomeCallBack() {
            @Override
            public void updateCartCount(Context context) {

            }
        });
        adapterTopOrder.setListener(new AdapterTopOrder.ItemimgAddListener() {
            @Override
            public void onImageclick(int position) {
//                product = listProduct.get(position);
//                setDataProduct(product);
//                nestedContent.scrollTo(0, 0);
                Bundle bundle = new Bundle();
                bundle.putParcelable("product", listProduct.get(position));
                AppUtil.startActivity(self, ProductDetailAct.class, bundle);
                finish();
            }

            @Override
            public void onDetailclick(int position) {
                Bundle bundle = new Bundle();
                bundle.putString("img", listProduct.get(position).getAttachment());
                AppUtil.startActivity(self, FoodActivity.class, bundle);
            }
        });

        rcvProductNearby.setLayoutManager(new LinearLayoutManager(self));
        rcvProductNearby.setAdapter(adapterTopOrder);
    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        product = bundle.getParcelable("product");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.btn_rate:
                if (product != null) {
                    showRatingDialog();
                }
                break;
        }
    }

    private void showRatingDialog() {
        final Dialog dialog = new Dialog(self);
        dialog.setContentView(R.layout.dialog_rating);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView lblmgs = dialog.findViewById(R.id.lbl_msg);
        TextView lblSubmit = dialog.findViewById(R.id.lbl_submit);
        final RatingBar ratingBar = dialog.findViewById(R.id.rating_bar);
        final EditText edtContent = dialog.findViewById(R.id.txt_comment);
        lblmgs.setText(R.string.msg_review_product);
        lblSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float rating = ratingBar.getRating();
                String content = edtContent.getText().toString();
                if (rating == 0) {
                    AppUtil.showToast(self, R.string.msg_rating_require);
                    return;
                } else if (content.isEmpty()) {
                    AppUtil.showToast(self, R.string.msg_content_require);
                    return;
                } else {
                    sendReponse(rating, content, product.getId());
                }
                dialog.dismiss();
            }
        });
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void sendReponse(float rating, String content, int id) {
        MyProgressDialog myProgressDialog = new MyProgressDialog(self);
        myProgressDialog.show();
        if (DataStoreManager.getUser() != null) {
            ApiUtils.getAPIService().reviewRestaurant(DataStoreManager.getToken(), rating * 2, content, "product", id).enqueue(new Callback<ResponseReview>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(Call<ResponseReview> call, Response<ResponseReview> response) {
                    myProgressDialog.cancel();
                    if (response.body() != null) {
                        if (response.body().isSuccess(ProductDetailAct.this)) {

                            if (response.body().getReview_count() != null) {
                                rate = response.body().getRate();
                                rate_count = response.body().getRate_count();

                                ratingBar.setRating(Float.parseFloat(response.body().getRate().toString()) / 2);
                                tvRateCount.setText((rate / 2) + "/5.0");
                                tvCountPeople.setText("(" + rate_count + " rating)");
                            }
                            if (response.body().getReviews() != null) {
                                if (reviewList != null && !reviewList.isEmpty()) {
                                    reviewList.clear();
                                } else {
                                    reviewList = new ArrayList<>();
                                }
                                reviewList.addAll(response.body().getReviews());
                                tvRateReview.setVisibility(View.VISIBLE);
                                adapteReview.notifyDataSetChanged();
                            }
                            for (int i = 0; i < RestaurantFrag.randomFoods.size(); i++) {
                                if (RestaurantFrag.randomFoods.get(i).getId() == id) {
                                    RestaurantFrag.randomFoods.get(i).setRate(rate);
                                    RestaurantFrag.randomFoods.get(i).setRate_count(rate_count);
                                    RestaurantFrag.randomFoods.get(i).setReviewList(reviewList);
                                }
                            }
                        }
                    } else {
                        Toast.makeText(self, R.string.msg_have_some_error, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseReview> call, Throwable t) {
                    myProgressDialog.cancel();
                    Log.e("TAG", "onFailure: " + t.getMessage());
                    Toast.makeText(self, R.string.msg_have_some_error, Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    @Override
    public void onLoadMore(int page) {

    }

    private void getData() {
    }
}
