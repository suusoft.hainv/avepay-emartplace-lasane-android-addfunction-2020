package com.aveplus.avepay_cpocket.music.main.addlist;

import android.content.Context;
import android.view.View;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.QueueManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class FavoriteAddListVM extends BaseViewModelList implements ItemMenuActionSongListener {
    private AddListObj obj;

    public FavoriteAddListVM(Context context, AddListObj obj) {
        super(context);
        this.obj = obj;
        getData(1);
    }

    @Override
    public void getData(int page) {
        addListData(obj.getListVideos());
    }

    public String getTitle() {
        return obj.getAddListName();
    }

    public String getImage() {
        return obj.getListVideos().get(0).getImage();
        //return album.image;
    }
    public String getItemsCount(){
        return  String.format(self.getString(R.string.bind_count),obj.getListVideos().size());
    }

    public void onClickPlay(View view){

        if(getListData().size()>0){
            QueueManager.getInstance().addNewQueue((ArrayList<Song>) getListData());
            EventBus.getDefault().post(new Global.HomeSongItemClicked());
        }
        else {
            Toast.makeText(self, "Album has no song!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().addNewQueue((ArrayList<Song>) getListData(),position);
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {

    }
}
