package com.aveplus.avepay_cpocket.trainingcenter.view;

import com.aveplus.avepay_cpocket.trainingcenter.object.Services;

public interface IOnClick {
    void OnClick(int position, Services productObj);

}
