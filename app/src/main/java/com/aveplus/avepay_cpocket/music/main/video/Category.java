package com.aveplus.avepay_cpocket.music.main.video;

import android.os.Parcel;
import android.os.Parcelable;

public class Category implements Parcelable {
    private int id,parent_id,is_active,is_video;
    private String name,img_thumb,img_banner,description,content,created_at,updated_at;

    public Category() {
    }

    public Category(int id, int parent_id, int is_active, int is_video, String name, String img_thumb, String img_banner, String description, String content, String created_at, String updated_at) {
        this.id = id;
        this.parent_id = parent_id;
        this.is_active = is_active;
        this.is_video = is_video;
        this.name = name;
        this.img_thumb = img_thumb;
        this.img_banner = img_banner;
        this.description = description;
        this.content = content;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public int getIs_video() {
        return is_video;
    }

    public void setIs_video(int is_video) {
        this.is_video = is_video;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_thumb() {
        return img_thumb;
    }

    public void setImg_thumb(String img_thumb) {
        this.img_thumb = img_thumb;
    }

    public String getImg_banner() {
        return img_banner;
    }

    public void setImg_banner(String img_banner) {
        this.img_banner = img_banner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    protected Category(Parcel in) {
        id = in.readInt();
        parent_id = in.readInt();
        is_active = in.readInt();
        is_video = in.readInt();
        name = in.readString();
        img_thumb = in.readString();
        img_banner = in.readString();
        description = in.readString();
        content = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(parent_id);
        parcel.writeInt(is_active);
        parcel.writeInt(is_video);
        parcel.writeString(name);
        parcel.writeString(img_thumb);
        parcel.writeString(img_banner);
        parcel.writeString(description);
        parcel.writeString(content);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
    }
}
