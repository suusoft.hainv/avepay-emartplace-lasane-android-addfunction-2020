package com.aveplus.avepay_cpocket.configs;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

import static com.aveplus.avepay_cpocket.movie.movie.configs.Config.URL_API;

public class Apis {
    public static final int REQUEST_TIME_OUT = 15000;

    private static final String APP_DOMAIN = AppController.getInstance().getString(R.string.URL_API) + "backend/web/index.php/api/";
    private static final String USER = "user/";
    private static final String DEAL = "deal/";
    private static final String TRANSPORT = "transport/";
    private static final String RESERVATION = "reservation";
    private static final String TRANSACTION = "transaction";
    private static final String UTILITY = "utility";
    private static final String CONTACT = "friend";

    public static final String URL_GET_FACEBOOK_INFO = "https://graph.facebook.com/me";

    public static final String URL_REGISTER_DEVICE = APP_DOMAIN + "device";
    public static final String URL_REGISTER_NORMAL_ACCOUNT = APP_DOMAIN + USER + "register";
    public static final String URL_LOGIN = APP_DOMAIN + USER + "login";

    // deal
    public static final String URL_DEAL_ACTION = APP_DOMAIN +"shopping/"+ DEAL;
    public static final String URL_GET_DEAL_LIST = APP_DOMAIN + "shopping/"+DEAL + "list";
    public static final String URL_GET_DEAL_DETAIL = APP_DOMAIN + DEAL + "detail";
    public static final String URL_FAVORITE = APP_DOMAIN + "favourite";
    public static final String URL_ACTIVATE_DEAL = APP_DOMAIN + DEAL + "switch";
    public static final String URL_UPDATE_DEAL_DURATION = APP_DOMAIN + DEAL + "update";

    // profile
    public static final String URL_UPDATE_PROFILE_NORMAL = APP_DOMAIN + USER + "update-profile";
    public static final String URL_UPDATE_PROFILE_PRO = APP_DOMAIN + USER + "update-pro-profile";
    public static final String URL_PROFILE = APP_DOMAIN + USER + "profile";
    public static final String URL_CHANGE_PASS_WORD = APP_DOMAIN + USER + "change-password";
    public static final String URL_GET_PROFILE_DRIVER = APP_DOMAIN + USER + "profile";
    public static final String URL_FORGET_PASSWORD = APP_DOMAIN + USER + "forget-password";

    //review
    public static final String URL_POST_REVIEW = APP_DOMAIN + "review";
    public static final String URL_GET_REVIEW = APP_DOMAIN + "review/list";

    public static final String URL_RESERVATION_ACTION = APP_DOMAIN + RESERVATION;
    public static final String URL_GET_RESERVATION = APP_DOMAIN + RESERVATION + "/list";
    public static final String URL_AVEPAY = APP_DOMAIN + "avepay/order";


    // Transport
    public static final String URL_GET_NEARBY_DRIVERS = APP_DOMAIN + TRANSPORT + "driver-list";
    public static final String URL_ACTIVATE_DRIVER_MODE = APP_DOMAIN + TRANSPORT + "online";
    public static final String URL_GET_TRIP_HISTORY = APP_DOMAIN + TRANSPORT + "trip-list";
    public static final String URL_TRIP_ACTION = APP_DOMAIN + TRANSPORT + "trip";
    public static final String URL_DELETE_ALL_TRIP = APP_DOMAIN + TRANSPORT + "trip-bulk-delete";

    //    settings
    public static final String URL_SETTINGS = APP_DOMAIN + USER + "setting";

    //    transaction
    public static final String URL_TRANSACTION = APP_DOMAIN + TRANSACTION;
    public static final String URL_HISTORY = APP_DOMAIN + TRANSACTION + "/list";
    public static final String URL_DELETE_HISTORY = APP_DOMAIN + TRANSACTION + "/delete";
    public static final String URL_GET_ACCOUNTING = APP_DOMAIN + TRANSACTION + "/accounting";
    //    utitlity
    public static final String URL_SETTING_UTILITY = APP_DOMAIN + UTILITY + "/setting";
    public static final String URL_UPDATE_LOCATION = APP_DOMAIN + UTILITY + "/update-location";
    public static final String URL_GET_DRIVER_LOCATION = APP_DOMAIN + TRANSPORT + "track-driver";

    public static final String URL_GET_CONTACTS = APP_DOMAIN + CONTACT + "/list";
    public static final String URL_CONTACTS_ACTION = APP_DOMAIN + CONTACT;

    public static final String URL_GET_ADDRESS_BY_LATLNG = "http://maps.googleapis.com/maps/api/geocode/json";



//    EBook

    private static final String URL_API = AppController.getInstance().getString(R.string.URL_API) + "backend/web/index.php/";
    public static final String getUrlBookDashboard = URL_API + "api/ebook/ebook/dashboard";
    public static final String getUrlGetBookByCondition = URL_API + "book/ebook/api/filter-book";
    public static final String getUrlCategory = URL_API + "api/ebook/ebook-category/list";
    public static final String getUrlChapterByBookId = URL_API + "api/ebook/ebook/chapter-list";
    public static final String getUrlBookList = URL_API + "api/ebook/ebook/book-list";
    public static final String getUrlFeedBack = URL_API + "app/api/feedback";
    public static final String getUrlGetComment = URL_API + "api/ebook/ebook/comment-list";
    public static final String getUrlAddComment = URL_API + "api/ebook/ebook/comment";
    public static final String getUrlChapterByChapterId = URL_API + "search_quote";
    public static final String getUrlCountReadBook = URL_API + "api/ebook/ebook/book";
    public static final String registerDevice = URL_API + "app/api/device";

}
