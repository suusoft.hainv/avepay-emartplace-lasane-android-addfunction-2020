package com.aveplus.avepay_cpocket.music.main.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.IOnClickListener;
import com.aveplus.avepay_cpocket.music.listener.IOnItemClickedListener;
import com.aveplus.avepay_cpocket.music.listener.IOnMenuItemClick;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionListener;
import com.aveplus.avepay_cpocket.music.model.Playlist;
import com.aveplus.avepay_cpocket.utils.AppUtil;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ItemPlaylistVM extends BaseAdapterVM implements IOnClickListener {

    private Playlist item;
    private int height, margin;

    public ItemPlaylistVM(Context context, Object data, int position) {
        super(context, position);
        this.item = (Playlist) data;
        height = AppUtil.getScreenWidth((Activity) self)/2 * 3 / 4 ;
        margin = AppUtil.convertDpToPixel(8);
    }


    public String getTitle(){
        return item.title;
    }

    public String getImage(){
        return item.image;
    }

    public String getCount(){
        return String.format(self.getString(R.string.bind_count),item.count);
    }

    public int getHeight(){
        return height;
    }

    @Override
    public void setData(Object object) {
        this.item = (Playlist) object;
        notifyChange();
    }

    @Override
    public void onClick(View view) {
        if (listener != null)
            ((IOnItemClickedListener)listener).onItemClicked(view, position);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.PREF_KEY_OBJECT,item);
    }

    public void onClickActionMore(final View view){
        Global.showPopupMenu(self, view, R.menu.playlist_item, new IOnMenuItemClick() {
            @Override
            public void onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_watch:
                        ((ItemMenuActionListener)listener).onClickWatch(position);
                        break;
                    case R.id.action_add_to_mylist:
                        ((ItemMenuActionListener)listener).onClickAddToList(position);
                        break;
                }
            }
        });

    }
}
