package com.aveplus.avepay_cpocket.ebook.viewmodel.fragment;

import android.content.Context;

import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.model.Book;


/**
 * Created by Suusoft on 9/12/2017.
 */
public class ChapterVM extends BaseViewModel {
    private Book mBook;

    @Override
    public void getData(int page) {


    }

    public ChapterVM(Context self, Book mBook) {
        super(self);
        this.mBook = mBook;
    }

}
