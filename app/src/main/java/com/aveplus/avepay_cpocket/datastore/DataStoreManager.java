package com.aveplus.avepay_cpocket.datastore;

import android.content.Context;
import android.util.Log;

import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.datastore.ChapterDao;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.model.Language;
import com.aveplus.avepay_cpocket.ebook.model.ListType;
import com.aveplus.avepay_cpocket.objects.ContactObj;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.objects.RecentChatObj;
import com.aveplus.avepay_cpocket.objects.ReservationObj;
import com.aveplus.avepay_cpocket.objects.SettingsObj;
import com.aveplus.avepay_cpocket.objects.TransportDealObj;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.folioreader.model.locators.ReadLocator;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Suusoft on 9/13/2016.
 */

public class DataStoreManager extends BaseDataStore {


    public static final String CHECK_FIRST_INSTALL_APP = "CHECK_FIRST_INSTALL_APP";
    public static final String PREF_LIST_TYPE = "PREF_LIST_TYPE";
    public static final String PREF_LANGUAGE = "PREF_LANGUAGE";
    public static final String Key_SETTING_TEXT_SIZE = "Key_SETTING_TEXT_SIZE";
    public static final String KEY_SETTING_SCREEN_ON = "KEY_SETTING_SCREEN_ON";
    // ============== User ============================
    private static final String PREF_USER = "PREF_USER";
    private static final String PREF_TOKEN_USER = "PREF_TOKEN_USER";
    // ============== Settings ============================
    private static final String PREF_SETTINGS_NOTIFY = "PREF_SETTINGS_NOTIFY";
    private static final String PREF_SETTINGS_NOTIFY_MESSAGE = "PREF_SETTINGS_NOTIFY_MESSAGE";
    private static final String PREF_SETTINGS_MY_FAVOURITE = "PREF_SETTING_MY_FAVOURITE";
    private static final String PREF_SETTINGS_TRANSPORT_DELIVERIES = "PREF_SETTINGS_TRANSPORT_DELIVERIES";
    private static final String PREF_SETTINGS_LABOR = "PREF_SETTINGS_LABOR";
    private static final String PREF_SETTINGS_FOOD_BERVERAGES = "PREF_SETTINGS_FOOD_BERVERAGES";
    private static final String PREF_SETTINGS_TRAVELLING = "PREF_SETTINGS_TRAVELLING";
    private static final String PREF_SETTINGS_SHOPPING = "PREF_SETTINGS_SHOPPING";
    private static final String PREF_SETTINGS_NEW_AND_EVENTS = "PREF_SETTINGS_NEW_AND_EVENTS";
    private static final String PREF_SETTINGS_NEAR_BY_DEALS = "PREF_SETTINGS_NEAR_BY_DEALS";
    private static final String PREF_LOGIN_CHECKED = "PREF_LOGIN_CHECKED";
    private static final String PREF_RECENT_CHAT = "PREF_RECENT_CHAT";
    private static final String PREF_CURRENT_CHAT = "PREF_CURRENT_CHAT";
    // setting utitlity
    private static final String PREF_SETTING_UTILITY = "SETTING_UTILITY";
    private static final String PREF_TRANSPORT_DEAL = "transportDeal";
    private static final String PREF_CONVERSATION_STATUS = "conversationStatus";
    private static final String PREF_DEAL_IS_NEGOTIATED = "dealIsNegotiated";
    private static final String PREF_CONTACTS_LIST = "contactsList";
    private static final String PREF_NEGOTIATED_RESERVATION = "reservationNegotiatedObj";
    //update deal
    private static final String PREF_UPDATE_DEAL = "PREF_UPDATE_DEAL";
    private static final String PREF_IS_LOGIN = "PREF_IS_LOGIN";
    private static final String PREF_CHAPTER = "PREF_CHAPTER";
    private static String TAG = DataStoreManager.class.getSimpleName();

    /**
     * save and get user
     */
    public static void saveUser(UserObj user) {
        if (user != null) {
            String jsonUser = new Gson().toJson(user);
            getInstance().sharedPreferences.putStringValue(PREF_USER, jsonUser);
        } else {
            Log.e("TAG", "Save user Null");
        }
    }

    public static void removeUser() {
        getInstance().sharedPreferences.putStringValue(PREF_USER, null);
    }

    public static void saveStatus(boolean isLogin) {
        DataStoreManager.getInstance().sharedPreferences.putBooleanValue(PREF_IS_LOGIN, isLogin);
    }

    public static boolean getStatus() {
        return DataStoreManager.getInstance().sharedPreferences.getBooleanValue(PREF_IS_LOGIN);
    }

    public static String getToken() {
        Log.d(TAG, "getToken: "+getInstance().sharedPreferences.getStringValue(PREF_TOKEN_USER));
        return getInstance().sharedPreferences.getStringValue(PREF_TOKEN_USER);
    }

    public static UserObj getUser() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_USER);
        UserObj user = new Gson().fromJson(jsonUser, UserObj.class);
        return user;
    }

    public static void clearUserToken() {
        UserObj userObj = getUser();
        userObj.setToken(null);
        saveUser(userObj);
    }

    public static void addToRecentChat(RecentChatObj obj) {
        if (obj != null) {
            QBUser qbUser = obj.getQbUser();
            ArrayList<RecentChatObj> recentChats = getRecentChat();
            for (int i = 0; i < recentChats.size(); i++) {
                if (recentChats.get(i).getQbUser().getId().equals(qbUser.getId())) {
                    // Remove old obj
                    recentChats.remove(i);
                    BaseDataStore.getInstance().sharedPreferences.putStringValue(PREF_RECENT_CHAT,
                            new Gson().toJson(recentChats));
                    break;
                }
            }

            // Add new obj and save into preference
            recentChats.add(obj);
            BaseDataStore.getInstance().sharedPreferences.putStringValue(PREF_RECENT_CHAT,
                    new Gson().toJson(recentChats));
        }
    }

    public static ArrayList<RecentChatObj> getRecentChat() {
        String json = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_RECENT_CHAT);
        if (json != null && !json.equals("")) {
            Type type = new TypeToken<ArrayList<RecentChatObj>>() {
            }.getType();

            return new Gson().fromJson(json, type);
        } else {
            return new ArrayList<>();
        }
    }

    public static void clearRecentChat() {
        BaseDataStore.getInstance().sharedPreferences.putStringValue(PREF_RECENT_CHAT, "");
    }

    public static void saveCurrentChat(RecentChatObj obj) {
        if (obj != null) {
            String jsonUser = new Gson().toJson(obj);
            getInstance().sharedPreferences.putStringValue(PREF_CURRENT_CHAT, jsonUser);
        }
    }

    public static RecentChatObj getCurrentChat() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_CURRENT_CHAT);

        return new Gson().fromJson(jsonUser, RecentChatObj.class);
    }

    public static void clearCurrentChat() {
        BaseDataStore.getInstance().sharedPreferences.putStringValue(PREF_CURRENT_CHAT, "");
    }

    public static void saveConversationStatus(boolean status) {
        BaseDataStore.getInstance().sharedPreferences.putBooleanValue(PREF_CONVERSATION_STATUS, status);
    }

    public static boolean conversationIsLive() {
        return BaseDataStore.getInstance().sharedPreferences.getBooleanValue(PREF_CONVERSATION_STATUS);
    }

    public static void saveDealNegotiation(RecentChatObj obj, boolean isNegotiated) {
        String key = "", userId = "";
        if (obj.getDealObj() != null) {
            /*if (obj.getDealObj().getSeller_id().equals(DataStoreManager.getUser().getId())) {
                userId = obj.getDealObj().getBuyerId();
            } else {
                userId = obj.getDealObj().getSeller_id();
            }*/
            userId = obj.getDealObj().getBuyerId() + "" + obj.getDealObj().getSeller_id();

            key = PREF_DEAL_IS_NEGOTIATED + userId + obj.getDealObj().getId();

            if (!isNegotiated) {
                ReservationObj reservationObj = new ReservationObj();
                reservationObj.setDeal(obj.getDealObj());
                saveNegotiatedReservation(reservationObj, true);
            }
        } else if (obj.getTransportDealObj() != null) {
            /*if (obj.getTransportDealObj().getDriverId().equals(DataStoreManager.getUser().getId())) {
                userId = obj.getTransportDealObj().getPassengerId();
            } else {
                userId = obj.getTransportDealObj().getDriverId();
            }*/
            userId = obj.getTransportDealObj().getPassengerId() + "" + obj.getTransportDealObj().getDriverId();

            key = PREF_DEAL_IS_NEGOTIATED + userId + obj.getTransportDealObj().getLatLngPickup().latitude
                    + "" + obj.getTransportDealObj().getLatLngPickup().longitude
                    + "" + obj.getTransportDealObj().getLatLngDestination().latitude
                    + "" + obj.getTransportDealObj().getLatLngDestination().longitude;
        }

        BaseDataStore.getInstance().sharedPreferences.putBooleanValue(key, isNegotiated);
    }

    public static boolean dealIsNegotiated(RecentChatObj obj) {
        if (obj.justForChatting() || obj.getQbUser() == null) {
            return false;
        }

        String key = "", userId = "";
        if (obj.getDealObj() != null) {
            /*if (obj.getDealObj().getSeller_id().equals(DataStoreManager.getUser().getId())) {
                userId = obj.getDealObj().getBuyerId();
            } else {
                userId = obj.getDealObj().getSeller_id();
            }*/
            userId = obj.getDealObj().getBuyerId() + "" + obj.getDealObj().getSeller_id();

            key = PREF_DEAL_IS_NEGOTIATED + userId + obj.getDealObj().getId();
        } else if (obj.getTransportDealObj() != null) {
            /*if (obj.getTransportDealObj().getDriverId().equals(DataStoreManager.getUser().getId())) {
                userId = obj.getTransportDealObj().getPassengerId();
            } else {
                userId = obj.getTransportDealObj().getDriverId();
            }*/
            userId = obj.getTransportDealObj().getPassengerId() + "" + obj.getTransportDealObj().getDriverId();

            key = PREF_DEAL_IS_NEGOTIATED + userId + obj.getTransportDealObj().getLatLngPickup().latitude
                    + "" + obj.getTransportDealObj().getLatLngPickup().longitude
                    + "" + obj.getTransportDealObj().getLatLngDestination().latitude
                    + "" + obj.getTransportDealObj().getLatLngDestination().longitude;
        }

        return BaseDataStore.getInstance().sharedPreferences.getBooleanValue(key);
    }

    /**
     * =======================================================
     * Settings screen
     */
    public static void saveSettingsNotify(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY, isChecked);
    }

    public static boolean getSettingsNotify() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY);
    }

    public static void saveSettingsMyFavourite(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_MY_FAVOURITE, isChecked);
    }

    public static boolean getSettingsMyFavourite() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_MY_FAVOURITE);
    }

    public static void saveSettingsTransportDeliveries(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_TRANSPORT_DELIVERIES, isChecked);
    }

    public static boolean getSettingsTransportDeliveries() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_TRANSPORT_DELIVERIES);
    }

    public static void saveSettingsFoodBeverages(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_FOOD_BERVERAGES, isChecked);
    }

    public static boolean getSettingsFoodBeverages() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_FOOD_BERVERAGES);
    }

    public static void saveSettingsLabor(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_LABOR, isChecked);
    }

    public static boolean getSettingsLabor() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_LABOR);
    }

    public static void saveSettingsTravelling(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_TRAVELLING, isChecked);
    }

    public static boolean getSettingsTravelling() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_TRAVELLING);
    }

    public static void saveSettingsShopping(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_SHOPPING, isChecked);
    }

    public static boolean getSettingsShopping() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_SHOPPING);
    }

    public static void saveSettingsNewsAndEvents(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NEW_AND_EVENTS, isChecked);
    }

    public static boolean getSettingsNewsAndEvents() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NEW_AND_EVENTS);
    }

    public static void saveSettingsNearByDeals(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NEAR_BY_DEALS, isChecked);
    }

    public static boolean getSettingsNearByDeals() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NEAR_BY_DEALS);
    }

    public static void saveSettingsNotifyMessage(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY_MESSAGE, isChecked);
    }

    public static boolean getSettingsNotifyMessage() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY_MESSAGE);
    }

    public static void saveSettingUtility(String setting) {
        getInstance().sharedPreferences.putStringValue(PREF_SETTING_UTILITY, setting);
    }

    public static SettingsObj getSettingUtility() {
        String setting = getInstance().sharedPreferences.getStringValue(PREF_SETTING_UTILITY);
        if (setting.isEmpty()) {
            return null;
        } else {
            try {
                JSONObject jsonObject = new JSONObject(setting);
                ApiResponse apiResponse = new ApiResponse(jsonObject);
                if (!apiResponse.isError()) {
                    SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
                    return utitlityObj;

                } else {
                    return null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static void saveTransport(TransportDealObj obj) {
        if (obj != null) {
            String jsonUser = new Gson().toJson(obj);
            getInstance().sharedPreferences.putStringValue(PREF_TRANSPORT_DEAL + obj.getDriverId(), jsonUser);
        }
    }

    public static TransportDealObj getTransport(String driverId) {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_TRANSPORT_DEAL + driverId);

        return new Gson().fromJson(jsonUser, TransportDealObj.class);
    }

    public static void saveContactsList(ArrayList<ContactObj> arr) {
        if (arr != null) {
            String contacts = new Gson().toJson(arr);
            getInstance().sharedPreferences.putStringValue(PREF_CONTACTS_LIST + getUser().getId(), contacts);
        }
    }

    public static ArrayList<ContactObj> getContactsList() {
        String json = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_CONTACTS_LIST + getUser().getId());
        if (json != null && !json.equals("")) {
            Type type = new TypeToken<ArrayList<ContactObj>>() {
            }.getType();

            return new Gson().fromJson(json, type);
        } else {
            return new ArrayList<>();
        }
    }

    public static void saveNegotiatedReservation(ReservationObj obj, boolean reset) {
        String json = new Gson().toJson(obj);

        String key = "", userId = "";
        if (obj.getDeal() != null) {
            /*if (obj.getDeal().getSeller_id().equals(DataStoreManager.getUser().getId())) {
                userId = obj.getDeal().getBuyerId();
            } else {
                userId = obj.getDeal().getSeller_id();
            }*/
            //comment by reskin
//            userId = obj.getDeal().getBuyerId() + "" + obj.getDeal().getSeller_id();

            //comment by reskin
            userId = obj.getBuyer_id() + "" + obj.getSeller_id();

            key = PREF_NEGOTIATED_RESERVATION + userId + obj.getDeal().getId();
        }

        Log.e("DataStoreManager", "saveNegotiatedReservation key = " + key);
        if (reset) {
            json = "";
        }
        Log.e("DataStoreManager", "saveNegotiatedReservation json = " + json);
        getInstance().sharedPreferences.putStringValue(key, json);
    }

    public static ReservationObj getNegotiatedReservation(DealObj dealObj) {
        String key = "", userId = "";
        if (dealObj != null) {
            /*if (dealObj.getSeller_id().equals(DataStoreManager.getUser().getId())) {
                userId = dealObj.getBuyerId();
            } else {
                userId = dealObj.getSeller_id();
            }*/
            userId = dealObj.getBuyerId() + "" + dealObj.getSeller_id();

            key = PREF_NEGOTIATED_RESERVATION + userId + dealObj.getId();
        }

        Log.e("DataStoreManager", "getNegotiatedReservation key = " + key);
        String json = getInstance().sharedPreferences.getStringValue(key);
        Log.e("DataStoreManager", "getNegotiatedReservation json = " + json);
        return new Gson().fromJson(json, ReservationObj.class);
    }

    public static boolean getUpdateDeal() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_UPDATE_DEAL);
    }

    public static void saveUpdateDeal(boolean isUpdated) {
        getInstance().sharedPreferences.putBooleanValue(PREF_UPDATE_DEAL, isUpdated);
    }

    /**
     * get firt install
     */
    public static boolean getFirstInstall() {
        return DataStoreManager.getInstance().sharedPreferences.getBooleanValue(CHECK_FIRST_INSTALL_APP);
    }

    /**
     * save first install
     */
    public static void saveFirstInstall(boolean isFirstInstall) {
        DataStoreManager.getInstance().sharedPreferences.putBooleanValue(CHECK_FIRST_INSTALL_APP, isFirstInstall);
    }

    public static void clearBook(String key) {
        DataStoreManager.getInstance().sharedPreferences.putStringValue(key, "");
    }

    public static int getSettingTextSize() {
        return DataStoreManager.getInt(Key_SETTING_TEXT_SIZE, 12);
    }

    public static void setSettingTextSize(int textSize) {
        DataStoreManager.putInt(Key_SETTING_TEXT_SIZE, textSize);
    }

    public static int getInt(String key, int defaultValues) {
        return DataStoreManager.getInstance().sharedPreferences.getIntValue(key, defaultValues);
    }

    public static boolean getSettingScreenOn() {
        return DataStoreManager.getBoolean(KEY_SETTING_SCREEN_ON);
    }

    public static void setSettingScreenOn(boolean screenOn) {
        DataStoreManager.putBoolean(KEY_SETTING_SCREEN_ON, screenOn);
    }

    public static boolean getBoolean(String key) {
        return DataStoreManager.getInstance().sharedPreferences.getBooleanValue(key);
    }

    public static void putBoolean(String key, boolean values) {
        DataStoreManager.getInstance().sharedPreferences.putBooleanValue(key, values);
    }

    public static void putInt(String key, int values) {
        DataStoreManager.getInstance().sharedPreferences.putIntValue(key, values);
    }


//Đây là phần của Ebook


    public static void saveInfoChapterInPath(Chapter chapter, String filePath) {
        String json = new Gson().toJson(chapter);
        getInstance().sharedPreferences.putStringValue(PREF_CHAPTER + filePath, json);
    }

    /*
     *  Save info chapter with path chapter
     */
    public static Chapter getInfoChapterInPath(String filePath) {
        String json = getInstance().sharedPreferences.getStringValue(PREF_CHAPTER + filePath);

        Log.e("Files", "chapter: " + json);
        return new Gson().fromJson(json, Chapter.class);
    }

    public static void addBook(Book book, String key) {
        List<Book> listBookHistory = getListBook(key);
        for (int i = 0; i < listBookHistory.size(); i++) {
            if (book.getId().equals(listBookHistory.get(i).getId())) {
                listBookHistory.remove(i);
                break;
            }
        }
        listBookHistory.add(0, book);
        DataStoreManager.getInstance().sharedPreferences.putStringValue(key, new Gson().toJson(listBookHistory));
    }

    public static void deleteBook(Book book, String key) {
        List<Book> listBookHistory = getListBook(key);
        for (int i = 0; i < listBookHistory.size(); i++) {
            if (book.getId().equals(listBookHistory.get(i).getId())) {
                listBookHistory.remove(i);
                break;
            }
        }
        DataStoreManager.getInstance().sharedPreferences.putStringValue(key, new Gson().toJson(listBookHistory));
    }


    public static List<Book> getListBook(String key) {
        String jsonListBook = DataStoreManager.getInstance().sharedPreferences.getStringValue(key, "");
        List<Book> listBook;
        if (!jsonListBook.isEmpty()) {
            listBook = new Gson().fromJson(jsonListBook, new TypeToken<ArrayList<Book>>() {
            }.getType());
        } else {
            listBook = new ArrayList<>();
        }
        return listBook;
    }

    /*
     *  add bookmark page
     */
    public static void addPageBookmark(Chapter chapter, int page) {
        chapter.setPageBookmark(page);
        boolean isExitsPageBook = false;
        List<Chapter> listChapter = getListChapter();
        for (int i = 0; i < listChapter.size(); i++) {

            if (chapter.getId().equals(listChapter.get(i).getId())
                    && chapter.getBookId().equals(listChapter.get(i).getBookId())
                    && page == listChapter.get(i).getPageBookmark()) {

                isExitsPageBook = true;
                break;
            }
        }
        if (!isExitsPageBook) {
            listChapter.add(0, chapter);
            DataStoreManager.getInstance().sharedPreferences.putStringValue(Constant.LIST_CHAPTER_MARKS, new Gson().toJson(listChapter));
        }
    }

    public static List<Chapter> getListChapter() {
        String jsonListChapter = DataStoreManager.getInstance().sharedPreferences.getStringValue(Constant.LIST_CHAPTER_MARKS, "");
        List<Chapter> listChapter;
        if (!jsonListChapter.isEmpty()) {
            listChapter = new Gson().fromJson(jsonListChapter, new TypeToken<ArrayList<Chapter>>() {
            }.getType());
        } else {
            listChapter = new ArrayList<>();
        }

        return listChapter;
    }

    public static void deletePageBookmark(Chapter chapter, int page) {
        List<Chapter> listChapter = getListChapter();
        for (int i = 0; i < listChapter.size(); i++) {
            if (chapter.getId().equals(listChapter.get(i).getId())
                    && chapter.getBookId().equals(listChapter.get(i).getBookId())
                    && page == listChapter.get(i).getPageBookmark()) {

                listChapter.remove(i);
                break;
            }
        }
        DataStoreManager.getInstance().sharedPreferences.putStringValue(Constant.LIST_CHAPTER_MARKS, new Gson().toJson(listChapter));
    }


    public static ReadLocator getLastReadLocator(Chapter chapter) {
        String jsonString = getReadPosition(chapter);
        return ReadLocator.fromJson(jsonString);
    }

    /**
     * get ReadPosition
     */
    public static String getReadPosition(Chapter chapter) {
        String keyChapter = "";
        if (chapter != null) {
            keyChapter = chapter.getBookId() + chapter.getId();
        }
        String json = DataStoreManager.getInstance().sharedPreferences.getStringValue(keyChapter);
        Log.e("readPosition", "getReadPosition " + json);
//        return  new Gson().fromJson(json, ReadPositionImpl.class);
        return json;
    }

    /**
     * save list type to sharePreferences
     */
    public static void saveListType(ListType listType) {
        if (listType != null) {
            String jsonListType = listType.toJSon();
            DataStoreManager.getInstance().sharedPreferences.putStringValue(PREF_LIST_TYPE, jsonListType);
        }
    }

    public static String getIndexChapter(Context context, Chapter model) {
        ChapterDao instance = ChapterDao.getInstance(context);
        String index = instance.getIndex(model);
        Log.e("index : ", "" + index);
        return index;
    }

    public static boolean isBookMarks(String id, String key) {
        List<Book> listBookHistory = getListBook(key);
        if (listBookHistory != null && listBookHistory.size() > 0) {
            for (int i = 0; i < listBookHistory.size(); i++) {
                if (id != null && id.equals(listBookHistory.get(i).getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * save ReadPosition dưới dạng json
     */
    public static void saveReadPosition(Chapter chapter, String readPosition) {

        String json = new Gson().toJson(readPosition);
        String keyChapter = "";
        if (chapter != null) {
            keyChapter = chapter.getBookId() + chapter.getId();
        }

        Log.e("readPosition", "saveReadPosition " + json);
        DataStoreManager.getInstance().sharedPreferences.putStringValue(keyChapter, readPosition);
    }

    // SQLite
    public static void saveIndexChapter(Context context, Chapter model, String index) {
        ChapterDao instance = ChapterDao.getInstance(context);
        boolean creat = instance.update(model, index);
        Log.e("updete : ", "" + creat + "  -  " + index);
    }

    /**
     * get list type from sharePreferences
     */
    public static ListType getListType() {
        String jsonListType = DataStoreManager.getInstance().sharedPreferences.getStringValue(PREF_LIST_TYPE);
        ListType listType = new Gson().fromJson(jsonListType, ListType.class);
        return listType;
    }

    /**
     * get language app from sharePreferences
     */
    public static Language getLanguage() {
        String jsonLanguage = DataStoreManager.getInstance().sharedPreferences.getStringValue(PREF_LANGUAGE);
        Language language = new Gson().fromJson(jsonLanguage, Language.class);
        return language;
    }

    /**
     * save language app to sharePreferences
     */
    public static void saveLanguage(Language language) {
        if (language != null) {
            String jsonLanguage = language.toJSon();
            DataStoreManager.getInstance().sharedPreferences.putStringValue(PREF_LANGUAGE, jsonLanguage);
        }
    }

    public static void saveToken(String token) {
        Log.d(TAG, "saveToken: " + token);
        getInstance().sharedPreferences.putStringValue(PREF_TOKEN_USER, token);
    }


}
