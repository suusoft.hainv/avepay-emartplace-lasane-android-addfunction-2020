package com.aveplus.avepay_cpocket.movie.movie.main;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.retrofit.base.ApiUtilsLogin;
import com.aveplus.avepay_cpocket.movie.movie.retrofit.base.BaseReponse;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.google.gson.Gson;


import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashCodeActivity extends AppCompatActivity {
    private static final String TAG = SplashCodeActivity.class.getSimpleName();

    private EditText edtCode;
    private Button btnConfirm;
    private TextView tvMacAddess;
    TelephonyManager tm;
    private TextView tvDateTime;
    private CheckBox checkBox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_code);


        tvDateTime = findViewById(R.id.tv_date_time);

        Date date = new Date();  // to get the date
        SimpleDateFormat df = new SimpleDateFormat("yy-MM-dd"); // getting date in this format
        String formattedDate = df.format(date.getTime());
        tvDateTime.setText(formattedDate);

        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();

        init();
        tvMacAddess.setText(info.getMacAddress());

        SharedPreferences preferences  = getSharedPreferences("checkbox", MODE_PRIVATE) ;
        String checkbox = preferences.getString("remember", "") ;
        if (checkbox.equals("true")){

        }else if (checkbox.equals("false")){

        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    SharedPreferences preferences = getSharedPreferences("checkbox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("remember", "true");
                    editor.apply();
                    Toast.makeText(SplashCodeActivity.this, "checked", Toast.LENGTH_SHORT).show();
                } else if (!buttonView.isChecked()) {
                    SharedPreferences preferences = getSharedPreferences("checkbox", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("remember", "false");
                    editor.apply();
                    Toast.makeText(SplashCodeActivity.this, "Unchecked", Toast.LENGTH_SHORT).show();

                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = edtCode.getText().toString();
                String mac_address = tvMacAddess.getText().toString();
                String date = tvDateTime.getText().toString();

//                AppUtil.startActivity(SplashCodeActivity.this, CategoryActivity.class);
//                finish();


                if (code == null || "".equals(code)) {
                    Toast.makeText(SplashCodeActivity.this, "You need enter code.", Toast.LENGTH_SHORT).show();
                } else if (code.length() != 6) {
                    Toast.makeText(SplashCodeActivity.this, "You must enter 6 numbers.", Toast.LENGTH_SHORT).show();
                } else {
                    ApiUtilsLogin.getAPIService().login_by_code(mac_address, code, date).enqueue(new Callback<BaseReponse>() {
                        @Override
                        public void onResponse(Call<BaseReponse> call, Response<BaseReponse> response) {
                          if (response.body() != null){
                              Log.e(TAG, "onResponse: " + new Gson().toJson(response.body().getMessage()));

                              AppUtil.startActivity(SplashCodeActivity.this, CategoryActivity.class);
                              finish();
                          }else {
                              Toast.makeText(SplashCodeActivity.this, "Login false", Toast.LENGTH_SHORT).show();
                          }
                        }

                        @Override
                        public void onFailure(Call<BaseReponse> call, Throwable t) {
                            Toast.makeText(SplashCodeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });


                }
            }
        });
    }

    private void init() {

        edtCode = findViewById(R.id.edt_code);
        btnConfirm = findViewById(R.id.btn_confirm);
        tvMacAddess = findViewById(R.id.tv_mac_address);
        checkBox = findViewById(R.id.remember_me);


    }

}