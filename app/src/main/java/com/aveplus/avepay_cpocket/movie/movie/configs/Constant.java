package com.aveplus.avepay_cpocket.movie.movie.configs;

/**
 * Copyright © 2019 SUUSOFT
 */
public class Constant {

    public static final String NOTIFICATION = "NOTIFICATION";
    public static final String LOGIN_TOKEN = "login_token";

    public static final String PREF_KEY_ID = "PREF_KEY_ID";

    public static final String KEY_TOTAL_PAGE = "total_page";
    public static final String KEY_URL ="url" ;
    public static final String KEY_TITLE ="title" ;
    public static final String KEY_VIDEO = "KEY_VIDEO";

    public class Caching{
        public static final String KEY_REQUEST = "request";
        public static final String KEY_RESPONSE = "response";
        public static final String KEY_TIME_UPDATED = "time_updated";
        public static final String CACHING_PARAMS_TIME_REQUEST = "caching_time_request";

    }
    //id menu left
    public static final int MENU_HOME   = 0;
    public static final int MENU_FAVORITE   = 1;
    public static final int MENU_CHAT       = 2;
    public static final int MENU_BUYER_NAMAGER = 3;
    public static final int MENU_SELLER     = 4;
    public static final int MENU_PAYMENT    = 5;
    public static final int MENU_PROFILE    = 6;

    public static final int MENU_NEW_EVENT  = 7;
    public static final int MENU_SHARE      = 8;
    public static final int MENU_FAQ        = 9;
    public static final int MORE_APP    = 10;
    public static final int MENU_HELP       = 11;
    public static final int MENU_ABOUT_US   = 12;
    public static final int MENU_LOGOUT     = 13;
}
