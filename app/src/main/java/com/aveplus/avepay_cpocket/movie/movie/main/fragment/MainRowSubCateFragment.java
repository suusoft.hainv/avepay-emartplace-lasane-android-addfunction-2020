package com.aveplus.avepay_cpocket.movie.movie.main.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.CategoryActivity;
import com.aveplus.avepay_cpocket.movie.movie.main.adapter.MainSubcategoryAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.customwidget.CenterLockHorizontalScrollview;
import com.aveplus.avepay_cpocket.movie.movie.main.detail.MovieDetailActivity;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onFocusRow;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onFocusRowAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onPageChangeAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Language;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;

import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public class MainRowSubCateFragment extends Fragment implements View.OnClickListener, onPageChangeAdapter, onFocusRowAdapter {
    public static final String CATEGORY_NAME = "cateogry_name";
    public static final String CATEGORY_POSITION = "category_pos";
    public static final String LANGUAGE_ID = "language id";
    private final int ITEM_PAGE = 5;
    private static String KEY_CATEGORY = "category";
    private static String KEY_POSITION = "position";
    private Button tvCategory;
    private ImageButton btnNext;
    private ImageButton btn_prev;
    private View row;
    //params
    private int langId;
    private String mCategory;
    private int mItemposition;

    private MainSubcategoryAdapter mainSubcategoryAdapter;
    private MoviesManagement moviesManagement;
    private List<Movie> newListMovies;
    private Language mlanguageCurrent;
    //scrol;
    private CenterLockHorizontalScrollview scrollview;
    private ViewGroup viewRowPage;

    private onFocusRow mOnFocusRow;
    private int itemPostition = 5;

    public static MainRowSubCateFragment newInstance(int langId, int itemPosition) {
        MainRowSubCateFragment subCategoryFragment = new MainRowSubCateFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_CATEGORY, langId);
        bundle.putInt(KEY_POSITION, itemPosition);
        subCategoryFragment.setArguments(bundle);
        return subCategoryFragment;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnFocusRow = (onFocusRow) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        moviesManagement = MoviesManagement.getInstance();

        langId = getArguments().getInt(KEY_CATEGORY);
        List<Language> listLangs = moviesManagement.getmLanguages();

        // Todo check list langs before all actions

        for (int i = 0; i < listLangs.size(); i++) {
            mlanguageCurrent = listLangs.get(i);
            if (langId == mlanguageCurrent.getLangId()) {
                mCategory = mlanguageCurrent.getLangName();
                break;
            }
        }
        mItemposition = getArguments().getInt(KEY_POSITION);
        newListMovies = moviesManagement.getmLangMovies().get(mlanguageCurrent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_row_view, null);
        initView(view);
        initControl();
        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRow();
    }

    private void initView(View view) {
        btnNext = (ImageButton) view.findViewById(R.id.btn_next);
        btn_prev = (ImageButton) view.findViewById(R.id.btn_prev);
        tvCategory = (Button) view.findViewById(R.id.tv_category);
        //lsvSubcategory = (HorizontalListView)view.findViewById(R.id.lsv_subcategory);
        scrollview = (CenterLockHorizontalScrollview) view.findViewById(R.id.horizontalScrollview);
        viewRowPage = (ViewGroup) view.findViewById(R.id.view_page);
        row = view.findViewById(R.id.row);

        //set font
        AssetManager assetManager = getActivity().getAssets();
        tvCategory.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));
    }

    private void setRow() {
        tvCategory.setText(mCategory);
        mainSubcategoryAdapter = new MainSubcategoryAdapter(getActivity(),
                R.layout.adapter_main_row_view, newListMovies, this, this);
        scrollview.setAdapter(getActivity(), mainSubcategoryAdapter);
        setForcusFirst();
    }

    private void setForcusFirst() {
        if (mItemposition == 0) {
            View child = viewRowPage.getChildAt(0);
            if (child != null)
                child.requestFocus();
        } else
            row.setBackgroundResource(R.drawable.line);
    }

    private void initControl() {
        btnNext.setOnClickListener(this);
        tvCategory.setOnKeyListener(mOnkeyLanguageListenner);
    }

    View.OnKeyListener mOnkeyLanguageListenner = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
            if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER) {

                    Intent intent = new Intent(getActivity(), CategoryActivity.class);
                    intent.putExtra(CATEGORY_NAME, mCategory);
                    intent.putExtra(LANGUAGE_ID, langId);
                    intent.putExtra(CATEGORY_POSITION, mItemposition);
                    getActivity().startActivity(intent);
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    mOnFocusRow.onFocus(mItemposition);
                }

            }
            return false;
        }
    };

    @Override
    public void onClick(View view) {
        if (view == btnNext) {
            scrollPage();
        }
    }

    private void scrollPage() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (newListMovies.size() <= itemPostition) {
                    itemPostition = 0;
                    scrollview.smoothScrollTo(0, 0);
                }
                if (newListMovies.size() > itemPostition) {
                    View view = viewRowPage.getChildAt(itemPostition);
                    scrollview.smoothScrollTo(view.getLeft(), 0);
                    itemPostition += ITEM_PAGE;
                }
            }
        });
    }

    private void callDetailMovie(int itemPostition) {
        moviesManagement.setmMovies(newListMovies);
        Bundle bundle = new Bundle();
        bundle.putInt(MovieDetailActivity.CATEGORY_EXTRA, mItemposition);
        bundle.putInt(MovieDetailActivity.MOVIE_EXTRA_POSITION, itemPostition);
        Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
        intent.putExtras(bundle);
        getActivity().startActivity(intent);

    }

    private void setVisibility(int position) {

        if (position == newListMovies.size() - 1) {
            btnNext.setVisibility(View.INVISIBLE);
        } else if (position == 0)
            btn_prev.setVisibility(View.INVISIBLE);
        else {
            btn_prev.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClickItemSubcate(int subcatePosition) {
        callDetailMovie(subcatePosition);
    }

    @Override
    public void onNextPage(int position) {
        scrollview.nextPage(position);

    }

    @Override
    public void onPrevPage(int position) {
        scrollview.prevPage(position);
    }


    @Override
    public void onFocusAdapter(int position, boolean hasfocus) {
        mOnFocusRow.onFocus(mItemposition);
        setVisibility(position);
    }
}
