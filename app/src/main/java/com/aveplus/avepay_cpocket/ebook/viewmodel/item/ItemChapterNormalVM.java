package com.aveplus.avepay_cpocket.ebook.viewmodel.item;

import android.content.Context;
import android.view.View;

import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModelAdapter;
import com.aveplus.avepay_cpocket.ebook.listener.IOnItemClickedListener1;
import com.aveplus.avepay_cpocket.ebook.model.ChapterNomal;
import com.aveplus.avepay_cpocket.movie.movie.listener.ISetDataListener;


public class ItemChapterNormalVM extends BaseViewModelAdapter implements IOnItemClickedListener1,
        ISetDataListener {

    private ChapterNomal chapterNomal;

    public ItemChapterNormalVM(Context context, ChapterNomal chapterNomal) {
        super(context);
        this.chapterNomal = chapterNomal;
    }

    public String getContent() {
        return chapterNomal.getContent();
    }

    public String getImage() {
            return chapterNomal.getImage();
    }

    public int getSize() {
        return DataStoreManager.getSettingTextSize();
    }


    @Override
    public void onItemClicked(View view) {
    }

    @Override
    public void setData(Object object) {
        this.chapterNomal = (ChapterNomal) object;
        notifyChange();
    }
}
