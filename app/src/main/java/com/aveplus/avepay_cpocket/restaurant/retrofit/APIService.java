package com.aveplus.avepay_cpocket.restaurant.retrofit;
/**
 * Created by suusoft.com on 11/12/19.
 */



import com.aveplus.avepay_cpocket.restaurant.main.notification.object.onResponeFCM;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.BannerRespone;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.NotifiRespone;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeCoupon;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeDetail;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeEmail;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeFavourite;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeListSave;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeOrder;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeSearch;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeUser;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponseListRestaurant;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponseRestaurantDetail;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponseReview;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_ADDRESS;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_AVATAR;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_BILLINGADDRESS;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_BILLINGNAME;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_BILLINGPHONE;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_BUYER_LAT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_BUYER_LONG;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_CODE;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_CONTENT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_DESTINATION;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_DESTINATION_ROLE;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_EMAIL;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_GCM;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_KEYWORD;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_NAME;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_NAME_RG;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_NUMBER_PER_PAGE;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_PAGE;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_PASSWORD;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_PAYMENTMETHOD;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_PHONE;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_RATE;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_RESTAURANT_ID;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_STATUS;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_TOKEN;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_TOTAL;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_USERNAME;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_USER_LAT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_USER_LONG;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_TYPE;


public interface APIService {

    @GET("restaurant/restaurant/recent")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponseListRestaurant> getRecentRestaurant();

    @GET("restaurant/recent")
    @Headers("Cache-Control:no-cache") // no cache
    Call<BannerRespone> getBanner();

    @GET("restaurant/restaurant/nearby?")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponseListRestaurant> getNearbyRestaurant(@Query(PARAM_BUYER_LAT) double buyer_lat,
                                                     @Query(PARAM_BUYER_LONG) double buyer_long,
                                                     @Query(PARAM_TOKEN) String token,
                                                     @Query(PARAM_PAGE) int page,
                                                     @Query(PARAM_NUMBER_PER_PAGE) int number);


    @GET("restaurant/restaurant/info")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponseRestaurantDetail> getRestaurantDetail(@Query(PARAM_TOKEN) String token,
                                                       @Query(PARAM_RESTAURANT_ID) int restaurant_id,
                                                       @Query(PARAM_USER_LAT) double user_lat,
                                                       @Query(PARAM_USER_LONG) double uer_long);


    // submit order api
    @GET("ecommerce/order")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeOrder> Order(@Query(PARAM_RESTAURANT_ID) int restaurant_id,
                             @Query("user_id") int user_id,
                             @Query(PARAM_BILLINGNAME) String billingName,
                             @Query(PARAM_BILLINGPHONE) String billingPhone,
                             @Query(PARAM_BILLINGADDRESS) String billingAddress,
                             @Query(PARAM_TOTAL) int total,
                             @Query(PARAM_PAYMENTMETHOD) String paymentMethod,
                             @Query("items") String items
//                             @Query("token")


    );


    @POST("user/register")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeUser> Register(@Query(PARAM_NAME_RG) String name,
                               @Query(PARAM_USERNAME) String username,
                               @Query(PARAM_PASSWORD) String password,
                               @Query(PARAM_PHONE) String phone,
                               @Query(PARAM_ADDRESS) String address
                               //  @Query(PARAM_AVATAR) String avatar
    );


    @GET("user/login")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeUser> Login(@Query(PARAM_USERNAME) String username,
                            @Query(PARAM_PASSWORD) String password,
                            @Query("login_type") String login_type);

@GET("user/login")
    @Headers("Cache-Control:no-cache") // no cache
Call<ResponeUser> Login(@Query(PARAM_USERNAME) String username,
                        @Query(PARAM_PASSWORD) String password,
                        @Query(PARAM_NAME) String name,
                        @Query(PARAM_AVATAR) String avatar,
                        @Query("login_type") String login_type);


    @GET("list-order/list_order_buyer")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeDetail> DetailOrder(@Query(PARAM_TOKEN) String token,
                                    @Query(PARAM_STATUS) int status);


    @GET("user/forget-password")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeEmail> Forgotpassword(@Query(PARAM_EMAIL) String email);


//    favourite/index

    @GET("favourite/index")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeFavourite> Favourite(@Query(PARAM_TOKEN) String token,
                                     @Query("object_id") int object_id,
                                     @Query("object_type") String object_type


    );

    @GET("restaurant/favourite/list")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeListSave> FavouriteList(@Query(PARAM_TOKEN) String token,
                                        @Query("type") String object_id,
                                        @Query("page") int page,
                                        @Query("number_per_page") int number_per_page


    );

    @GET("user/update-profile")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeUser> update(@Query(PARAM_TOKEN) String token,
                             @Query(PARAM_NAME) String username,
                             @Query(PARAM_PHONE) String phone,
                             @Query(PARAM_ADDRESS) String address);


    @GET("user/update-profile")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeUser> updatePassword(@Query(PARAM_TOKEN) String token,
                                     @Query(PARAM_PASSWORD) String password

    );


    @Multipart
    @POST("user/update-profile")
    Call<ResponeUser> updateAvatar(@Part(PARAM_TOKEN) RequestBody token,
                                   @Part MultipartBody.Part avatar

    );


    @GET("device/index/?type=1&status=1")
    Call<onResponeFCM> upDateGCM(@Query(PARAM_GCM) String gcm_id,
                                 @Query(PARAM_TYPE) int type,
                                 @Query(PARAM_STATUS) int status
    );

//http://suusoft-demo.com/products/product-foody/backend/web/index.php/api/device/index?token=&ime=&gcm_id=&type=1&status=(để trống nhé)

    @GET("list-product/bykeyword")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeSearch> search(@Query(PARAM_KEYWORD) String keyword,
                               @Query(PARAM_PAGE) int page,
                               @Query(PARAM_NUMBER_PER_PAGE) int number);



//http://suusoft-demo.com/products/product-foody/backend/web/index.php/push-notification/list?page=1&number_per_page=10

    @GET("push-notification/list")
    @Headers("Cache-Control:no-cache") // no cache
    Call<NotifiRespone> getListNotifi(@Query(PARAM_PAGE) int page,
                                      @Query(PARAM_NUMBER_PER_PAGE) int number_per_page
    );



//http://suusoft-demo.com/products/product-foody/backend/web/index.php/api/coupon/check?token=
    @GET("coupon/check")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponeCoupon> Coupon(@Query(PARAM_TOKEN) String token,
                               @Query(PARAM_CODE) String code

    );

    @GET("review/index?author_role=buyer&object_id=1&object_type=deal&")
    @Headers("Cache-Control:no-cache") // no cache
    Call<ResponseReview> reviewRestaurant(@Query(PARAM_TOKEN) String token,
                                          @Query(PARAM_RATE) double rate,
                                          @Query(PARAM_CONTENT) String content,
                                          @Query(PARAM_DESTINATION_ROLE) String shop_or_product,
                                          @Query(PARAM_DESTINATION) int restaurant_id);



}
