package com.aveplus.avepay_cpocket.restaurant.retrofit.response;

import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.Rate;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.Review;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;

public class ResponseReview extends BaseModel {
    @SerializedName("review")
    @Expose
    private ArrayList<Review> review;
    @SerializedName("review_count")
    @Expose
    private Rate review_count;


    @SerializedName("rate")
    @Expose
    private Double rate;
    @SerializedName("rate_count")
    @Expose
    private Integer rate_count;

    public ArrayList<Review> getReviews() {
        return review;
    }

    public Rate getReview_count() {
        return review_count;
    }

    public Double getRate() {
        return rate;
    }

    public Integer getRate_count() {
        return rate_count;
    }
}
