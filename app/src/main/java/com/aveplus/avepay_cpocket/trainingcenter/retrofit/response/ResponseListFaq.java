package com.aveplus.avepay_cpocket.trainingcenter.retrofit.response;


import com.aveplus.avepay_cpocket.movie.movie.retrofit.base.BaseReponse;
import com.aveplus.avepay_cpocket.trainingcenter.object.Services;

import java.util.ArrayList;

public class ResponseListFaq extends BaseReponse {
    public ArrayList<Services> data;

    public ArrayList<Services> getData() {
        return data;
    }

    public void setData(ArrayList<Services> data) {
        this.data = data;
    }
}
