package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer;

import android.util.Log;

import com.android.volley.RequestQueue;
import com.aveplus.avepay_cpocket.movie.movie.configs.WebService;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.ExtractCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.YoutubeCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.model.BaseYTParser;


import java.util.ArrayList;
import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public class Youtube {
    private final static String TAG = Youtube.class.getName();

    private String[] YTExtractURL = {WebService.YTExtractURLDetails};

    private String youtube_id ;
    private static Youtube mYoutube;
    private static RequestQueue mRequestQueue;
    public static Youtube getInstance(String id) {
        mYoutube = new Youtube(id);
        mRequestQueue = RequestManager.getRequestQueue();
        return mYoutube;
    }

    public Youtube(String id) {
        this.youtube_id = id;
    }

    private ArrayList<String> getYTExtractURL(String movie_id) {
        ArrayList<String> str = new ArrayList<String>();
        for (int i = 0; i < YTExtractURL.length; i++) {
            str.add(String.format(YTExtractURL[i], movie_id));
        }
        return str;
    }

    private String getCaptureURL(String movie_id) {
        return String.format(WebService.YTURL, movie_id);
    }

    public void extractStreaming(final YoutubeCallback ycb) {
        MoviesManagement.getInstance(mRequestQueue).extractYTStreaming(getYTExtractURL(youtube_id).get(0),
                new ExtractCallback() {
                    @Override
                    public void onFailed(Exception exception) {
                        // TODO Auto-generated method stub
                        ycb.onFailed(true);
                    }

                    @Override
                    public void onCompleted(List<BaseYTParser.VideoStream> videoStreams) {
                        // TODO Auto-generated method stub]
                        getLinksStream(videoStreams, true, ycb);
                    }
                });
    }

    private void captureStreaming(final YoutubeCallback ycb) {
        MoviesManagement.getInstance().streamCpatureYT(
                getCaptureURL(youtube_id),
                new ExtractCallback() {

                    @Override
                    public void onFailed(Exception exception) {
                        // TODO Auto-generated method stub
                        ycb.onFailed(true);
                    }

                    @Override
                    public void onCompleted(
                            List<BaseYTParser.VideoStream> videoStreams) {
                        // TODO Auto-generated method stub
                        getLinksStream(videoStreams, false,ycb);
                    }
                });

    }

    private void getLinksStream(List<BaseYTParser.VideoStream> videoStreams, boolean the_first, YoutubeCallback ycb) {
        if (videoStreams != null && videoStreams.size() > 0) {
            for (BaseYTParser.VideoStream videoStream : videoStreams) {
                Log.i(TAG, videoStream.vq + " : " + videoStream.url);
            }
//            ycb.onFailed(false);
            ycb.onCompleted(videoStreams);
        } else {
            if (the_first) {
                captureStreaming(ycb);
            } else {
                ycb.onFailed(true);
            }
        }
    }
}
