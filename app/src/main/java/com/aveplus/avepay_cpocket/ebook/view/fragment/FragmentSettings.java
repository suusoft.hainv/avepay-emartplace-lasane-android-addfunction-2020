package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.ViewDataBinding;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.FragmentSettingsEbookBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.SettingsVM;


/**
 * Created by Suusoft on 9/8/2017.
 */
public class FragmentSettings extends BaseFragmentBinding {
    private SettingsVM viewModel;
    private FragmentSettingsEbookBinding binding;

    public static FragmentSettings newInstance() {
        Bundle args = new Bundle();
        FragmentSettings fragment = new FragmentSettings();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_settings_ebook;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new SettingsVM(self);
        return viewModel;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        this.binding = (FragmentSettingsEbookBinding) binding;
        this.binding.setViewModel(viewModel);
    }


    @Override
    protected void initView(View view) {
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
