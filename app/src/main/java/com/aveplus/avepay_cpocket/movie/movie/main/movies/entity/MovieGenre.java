package com.aveplus.avepay_cpocket.movie.movie.main.movies.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieGenre implements Parcelable {
	private int id;
	private String name;
	private int parentId;
	public MovieGenre(){}
	public MovieGenre(int id, String name, int parent){
		this.id = id;
		this.name = name;
		this.parentId = parent;
	}

	public MovieGenre(int id, String name) {
		this.id = id;
		this.name = name;
	}

	protected MovieGenre(Parcel in) {
		id = in.readInt();
		name = in.readString();
		parentId = in.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeInt(parentId);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<MovieGenre> CREATOR = new Creator<MovieGenre>() {
		@Override
		public MovieGenre createFromParcel(Parcel in) {
			return new MovieGenre(in);
		}

		@Override
		public MovieGenre[] newArray(int size) {
			return new MovieGenre[size];
		}
	};

	// getters
	public int getId(){
		return this.id;
	}
	public String getName(){
		return this.name;
	}
	public int getParentId(){
		return this.parentId;
	}

	// setters
	public void setId(int id){
		this.id = id;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setParentId(int parentId){
		this.parentId = parentId;
	}
}
