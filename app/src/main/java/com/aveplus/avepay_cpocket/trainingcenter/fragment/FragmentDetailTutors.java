package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.squareup.picasso.Picasso;


public class FragmentDetailTutors extends BaseFragment {

    private TextView txtname, txtposition, txtNameTutors, txtWork, txtDesPeopleDetail;
    private ImageView imgpeople;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.detail_ourpeople, container, false);
        initView(view);
        initData();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("detailtutor");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("ourpeople");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    private void initData() {
        Context context = null;
        Bundle bundle = this.getArguments();
        String name = bundle.getString("name");
        String position = bundle.getString("position");
        String email = bundle.getString("email");
        String skype = bundle.getString("skype");
        String image = bundle.getString("image");
        String des = bundle.getString("description");
        txtNameTutors.setText(name);
        txtWork.setText(position);
        txtDesPeopleDetail.setText(des);
        Picasso.get().load(image).into(imgpeople);
    }

    public void initView(View v) {
        txtNameTutors = (TextView) v.findViewById(R.id.txtNameTutors);
//        txtname = (TextView) v.findViewById(R.id.txtNamePeopleDetail);
//        txtposition = (TextView) v.findViewById(R.id.txtPositionPeopleDetail);
        txtWork = (TextView) v.findViewById(R.id.txtWork);
        txtDesPeopleDetail = (TextView) v.findViewById(R.id.txtDesPeopleDetail);
        imgpeople = (ImageView) v.findViewById(R.id.imgPeopleDetail);
    }
}
