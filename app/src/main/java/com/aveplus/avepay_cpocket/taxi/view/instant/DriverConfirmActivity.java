package com.aveplus.avepay_cpocket.taxi.view.instant;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.AbstractActivity;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.configs.Action;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.parsers.JSONParser;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.ImageUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.taxi.view.activities.TripTrackingActivity;

import org.greenrobot.eventbus.EventBus;

import de.hdodenhof.circleimageview.CircleImageView;


public class DriverConfirmActivity extends BaseActivity implements View.OnClickListener {

    public static final int RQ_TRACKING_TRIP = 1001;
    private static final int RQ_CODE_DRIVER_SELECT_TIME = 1999;
    private TextView tvTime, tvEstimate, tvNote, tvAddressPickup, tvAddressDropOff,
            tvAddressFullPickup, tvAddressFullDropOff, tvName, tvPhone, tvPrice;
    private Button btnCancel, btnAccept;
    private ViewGroup layoutEstimate;
    private LinearLayout loParent;
    private CircleImageView imgAvatarPassenger;
    private RelativeLayout progressbar;

    private TripObj data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    protected ToolbarType getToolbarType() {
        return AbstractActivity.ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_detail_driver;
    }

    @Override
    protected void getExtraData(Intent intent) {
        data = intent.getParcelableExtra(Args.KEY_TRIP_OJBECT);
        if (data == null)
            finish();
    }

    @Override
    protected void initilize() {
    }

    @Override
    protected void initView() {
//        loParent = findViewById(R.id.lo_parent);
        tvTime = findViewById(R.id.tv_time);
        tvEstimate = findViewById(R.id.time_estimate);
        tvName = findViewById(R.id.name1);
        tvPhone = findViewById(R.id.phone1);
        tvPrice = findViewById(R.id.tv_info2);
        tvAddressPickup = findViewById(R.id.lbl_start_point);
        tvAddressFullPickup = findViewById(R.id.lbl_end_point);
        tvAddressDropOff = findViewById(R.id.lbl_start_point1);
        tvAddressFullDropOff = findViewById(R.id.lbl_end_point1);
        layoutEstimate = findViewById(R.id.layout_time_estimate);
        progressbar = findViewById(R.id.lo_loading);
        imgAvatarPassenger=findViewById(R.id.img_avatar_passenger);

        btnAccept = findViewById(R.id.btn_accept);
        btnCancel = findViewById(R.id.btn_cancel);


        btnCancel.setOnClickListener(this);
        btnAccept.setOnClickListener(this);
        layoutEstimate.setOnClickListener(this);
        timer.start();
    }

    @Override
    protected void onViewCreated() {
        setToolbarTitle(R.string.waiting_time);
        getTripDetail();
    }

    private void getTripDetail() {
        RequestManager.tripDetail(data.getId(), new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (!response.isError()) {
                    TripObj tripObj = JSONParser.parseTripFull(response.getRoot());
                    data = tripObj;
                    setData();
                    progressbar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(String message) {

            }
        });
    }


    private void setData() {
        tvName.setText(String.format(getString(R.string.bind_name1), data.getDriverName()));
        tvPhone.setText(String.format(getString(R.string.bind_phone1) , data.getPassengerPhone()));
        tvPrice.setText(StringUtil.convertNumberToPrice(data.getPrice()));
        tvEstimate.setText(data.getEstimateTime() + "");
        tvAddressPickup.setText(data.getPickup());
        tvAddressFullPickup.setText(MapsUtil.getCityFromLatLng(self, data.getLatLngPickup()));
        tvAddressDropOff.setText(data.getDestination());
        tvAddressFullDropOff.setText(MapsUtil.getCityFromLatLng(self, data.getLatLngDestination()));
        ImageUtil.setImage(self, imgAvatarPassenger, data.getPassengerAvatar());
    }
    @Override
    public void onClick(View view) {
        if (view == btnAccept) {
            onAccept();
        }
    }

    private void onAccept() {
        RequestManager.instantTripAction(Action.ACTION_ACCEPT, data.getId(), tvEstimate.getText().toString(), new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                //DataStoreManager.saveTripOld(data.getId());
                //data = JSONParser.getTripStatus(response.getRoot());
                TripObj data = response.getDataObject(TripObj.class);
                gotoTripTracking();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

    private void onCancelTrip() {
        RequestManager.instantTripAction(Action.ACTION_CANCEL, data.getId(), tvEstimate.getText().toString(), new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                //AppController.removeTripById(data.getId());
                finish();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

    private void gotoTripTracking() {
        if (data != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Args.KEY_TRIP_OJBECT, data);
            AppUtil.startActivityForResult(this, TripTrackingActivity.class, bundle, RQ_TRACKING_TRIP);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
        AppController.onPause();
    }

    /////////

    private CountDownTimer timer = new CountDownTimer(30000, 1000) {
        @Override
        public void onTick(long l) {
            tvTime.setText(l / 1000 + "");
//            loParent.invalidate();

        }

        @Override
        public void onFinish() {
            onCancelTrip();
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Reset recent chat list
        DataStoreManager.clearRecentChat();

        // Keep conversation status
        DataStoreManager.saveConversationStatus(false);
        EventBus.getDefault().unregister(this);
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(Global.PushTripCancel event) {
//
//        DialogUtil.showAlertDialog1(self, R.string.passenger_canceled_this_trip, false, true, new DialogUtil.IDialogConfirm() {
//            @Override
//            public void onClickOk() {
//                finish();
//
//            }
//        });
//    }



}
