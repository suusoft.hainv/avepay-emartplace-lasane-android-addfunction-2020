package com.aveplus.avepay_cpocket.restaurant.main.history;

import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DetailOrder extends BaseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("billingName")
    @Expose
    private String billingName;
    @SerializedName("billingPhone")
    @Expose
    private String billingPhone;
    @SerializedName("billingAddress")
    @Expose
    private String billingAddress;
    @SerializedName("billingEmail")
    @Expose
    private String billingEmail;
    @SerializedName("billingPostcode")
    @Expose
    private String billingPostcode;
    @SerializedName("shippingName")
    @Expose
    private String shippingName;
    @SerializedName("shippingPhone")
    @Expose
    private String shippingPhone;
    @SerializedName("shippingAddress")
    @Expose
    private String shippingAddress;
    @SerializedName("shippingEmail")
    @Expose
    private String shippingEmail;
    @SerializedName("shippingPostcode")
    @Expose
    private String shippingPostcode;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("status_user")
    @Expose
    private Integer statusUser;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("vat")
    @Expose
    private Integer vat;
    @SerializedName("transportFee")
    @Expose
    private Integer transportFee;
    @SerializedName("transportDes")
    @Expose
    private String transportDes;
    @SerializedName("transportType")
    @Expose
    private String transportType;
    @SerializedName("type_product")
    @Expose
    private String typeProduct;
    @SerializedName("token_payment")
    @Expose
    private Object tokenPayment;
    @SerializedName("createDate")
    @Expose
    private String createDate;
    @SerializedName("order_detail")
    @Expose
    private ArrayList<Detail> orderDetail = null;

    public ArrayList<Detail> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(ArrayList<Detail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    public DetailOrder() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingPhone() {
        return billingPhone;
    }

    public void setBillingPhone(String billingPhone) {
        this.billingPhone = billingPhone;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingPostcode() {
        return billingPostcode;
    }

    public void setBillingPostcode(String billingPostcode) {
        this.billingPostcode = billingPostcode;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingEmail() {
        return shippingEmail;
    }

    public void setShippingEmail(String shippingEmail) {
        this.shippingEmail = shippingEmail;
    }

    public String getShippingPostcode() {
        return shippingPostcode;
    }

    public void setShippingPostcode(String shippingPostcode) {
        this.shippingPostcode = shippingPostcode;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatusUser() {
        return statusUser;
    }

    public void setStatusUser(Integer statusUser) {
        this.statusUser = statusUser;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getVat() {
        return vat;
    }

    public void setVat(Integer vat) {
        this.vat = vat;
    }

    public Integer getTransportFee() {
        return transportFee;
    }

    public void setTransportFee(Integer transportFee) {
        this.transportFee = transportFee;
    }

    public String getTransportDes() {
        return transportDes;
    }

    public void setTransportDes(String transportDes) {
        this.transportDes = transportDes;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public String getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(String typeProduct) {
        this.typeProduct = typeProduct;
    }

    public Object getTokenPayment() {
        return tokenPayment;
    }

    public void setTokenPayment(Object tokenPayment) {
        this.tokenPayment = tokenPayment;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
