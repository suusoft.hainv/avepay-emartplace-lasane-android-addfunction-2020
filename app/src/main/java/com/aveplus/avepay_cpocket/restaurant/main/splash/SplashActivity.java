package com.aveplus.avepay_cpocket.restaurant.main.splash;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.configs.Global;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.listener.IConfirmation;
import com.aveplus.avepay_cpocket.restaurant.main.login.LoginActivity;
import com.aveplus.avepay_cpocket.restaurant.main.mainAct.MainActivity;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.util.PermissionUtil;


public class SplashActivity extends AppCompatActivity {


    private static final int RC_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getSupportActionBar().hide();
        setContentView(R.layout.activity_splash_restaurant);

        if (Global.isGranted(this, new String[]{Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSIONS, "")) {
            onContinue();
        }
        hideStatusBar();
        setContentView(R.layout.activity_splash_restaurant);

    }

    private void hideStatusBar() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSIONS: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED
                            || grantResults[1] == PackageManager.PERMISSION_DENIED) {
                        showPermissionsReminder(RC_PERMISSIONS, true);
                    } else {
                        //permission đã được cấp phép
                        onContinue();
                    }
                    break;
                }
            }
        }
    }

    private void onContinue() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (DataStoreManager.getUserModels() != null && DataStoreManager.getToken() != null) {
                    if (!DataStoreManager.getToken().equals("")) {
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }
                } else {
                    AppUtil.startActivity(SplashActivity.this, LoginActivity.class);
                }
                AppController.getInstance().setToken(DataStoreManager.getToken());
                //shimmer.cancel();
                finish();
            }
        }, 2000);
    }


    protected void showPermissionsReminder(final int reqCode, final boolean flag) {
        PermissionUtil.showConfirmationDialog(this, getString(R.string.msg_remind_user_grants_permissions),
                getString(R.string.allow), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        PermissionUtil.isGranted(SplashActivity.this, new String[]{
                                Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, reqCode, null);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            finish();
                        }
                    }
                });
    }
}


