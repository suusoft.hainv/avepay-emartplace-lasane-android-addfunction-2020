package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;


import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.listener.IOnItemClickedListener;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.CategoryProductAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ProductAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.CategoryProduct;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.aveplus.avepay_cpocket.trainingcenter.widget.EndlessRecyclerOnScrollListener;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class FragmentProducts extends BaseFragment {

    private ProductAdapter mAdapter = null;
    private View view;
    private CategoryProductAdapter categoryAdapter;
    private ArrayList<Product> listPro = new ArrayList<Product>();
    private ArrayList<CategoryProduct> arrCategories = new ArrayList<CategoryProduct>();
    private EditText txtSearch;
    private Spinner spnCategories;
    private int totalPage;
    private int categoryIndex = 0;
    private RecyclerView rcvData;
    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("product");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product, container, false);
        initUI();
        initControl();
        initGridView();
        initData();
        return view;
    }

    public void initUI() {
        rcvData = (RecyclerView) view.findViewById(R.id.rcv_data);
        txtSearch = (EditText) view.findViewById(R.id.txtsearchProduct);
        spnCategories = (Spinner) view.findViewById(R.id.spinerPro);

    }

    private void initControl() {
        txtSearch.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchProductByKey(txtSearch.getText().toString());
                }
                return false;
            }
        });

    }

    private void initGridView() {
        rcvData.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mAdapter = new ProductAdapter(self, listPro);
        mAdapter.setListener(new IOnItemClickedListener() {
            @Override
            public void onItemClicked(View view, int position) {
                onClickItem(position);
            }
        });
        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(new EndlessRecyclerOnScrollListener.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                getAllProduct(page);

            }
        });
        rcvData.addOnScrollListener(endlessRecyclerOnScrollListener);
        rcvData.setAdapter(mAdapter);


    }

    private void onClickItem(int position) {
        DetailCourse fragment = new DetailCourse();
        Product item = listPro.get(position);
        Bundle bundle = new Bundle();
        bundle.putParcelable("item", item);
        fragment.setArguments(bundle);

        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_left,
                        R.anim.slide_out_left)
                .replace(R.id.showPro, fragment, "detailCourse")
                .addToBackStack("detailCourse").commit();
    }

    private void searchProductByKey(String key) {
        ModelManager.getListProByCateAndKey("", key, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                listPro.clear();
                listPro.addAll(ParseUtility.parseListPro(response.getRootStr()));
                totalPage = ParseUtility.getAllPage(response.getRootStr());
                if (listPro.size() == 0) {
                    Toast.makeText(self, "No data is found !",
                            Toast.LENGTH_SHORT).show();
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

    private void initData() {
        //getCategories();
        getAllProduct(1);
    }

    private void getAllProduct(final int page) {
        ModelManager.getListProducts(page, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (page <= 1)
                    listPro.clear();
                listPro.addAll(ParseUtility.parseListPro(response.getRootStr()));
                totalPage = ParseUtility.getAllPage(response.getRootStr());
                mAdapter.notifyDataSetChanged();
                endlessRecyclerOnScrollListener.setEnded(endlessRecyclerOnScrollListener.getCurrentPage() >= totalPage);

            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }


    private void setDataCategoryToList(ArrayList<CategoryProduct> arrCategorys) {
        categoryAdapter = new CategoryProductAdapter(self,
                R.layout.layout_text_spinner, arrCategorys);
        categoryAdapter
                .setDropDownViewResource(R.layout.layout_text_row_spinner);
        spnCategories.setAdapter(categoryAdapter);
        spnCategories.setSelection(0);
    }


    protected void showToast(int idString) {
        Toast.makeText(getActivity(), idString, Toast.LENGTH_SHORT).show();
    }


}
