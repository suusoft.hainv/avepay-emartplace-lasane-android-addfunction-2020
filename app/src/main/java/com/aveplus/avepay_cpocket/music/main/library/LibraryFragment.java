package com.aveplus.avepay_cpocket.music.main.library;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.main.addlist.AddListAdapter2;
import com.aveplus.avepay_cpocket.music.main.addlist.AddListObj;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class LibraryFragment extends BaseFragmentBinding {
    private LibraryVM viewModel;
    private TextView tvTitle;
    private TextView tvMyPlaylist;
    private RecyclerView rcy_MyPlaylist;

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_library;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new LibraryVM(self);
        return viewModel;
    }

    @Override
    protected void initView(View view) {
        AppUtil.hideSoftKeyboard((Activity) self);
        tvTitle = view.findViewById(R.id.tv_title);
        tvTitle.setText(R.string.library);
        tvMyPlaylist = view.findViewById(R.id.tv_listplaylist);
        rcy_MyPlaylist = view.findViewById(R.id.recycler_view_playlist);
        ArrayList<AddListObj> listObjs;
        if (DataStoreManager.getMyPlaylist() == null) {
            listObjs = new ArrayList<>();
            tvMyPlaylist.setVisibility(View.GONE);
        } else {
            listObjs = DataStoreManager.getMyPlaylist().getListObjs();
            tvMyPlaylist.setVisibility(View.VISIBLE);
        }
        AddListAdapter2 adapter = new AddListAdapter2(listObjs);
        rcy_MyPlaylist.setLayoutManager(new LinearLayoutManager(self, RecyclerView.VERTICAL, false));
        rcy_MyPlaylist.setAdapter(adapter);
    }
}
