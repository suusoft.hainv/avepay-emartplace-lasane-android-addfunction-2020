package com.aveplus.avepay_cpocket.music.main.playvideo;

public interface OnTrailerListener {
    void onCreateTrailer(String mTrailerKey);

    void onPlayTrailer(String mTrailerKey);
}
