package com.aveplus.avepay_cpocket.ebook.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Suusoft on 10/1/2017.
 */
public class Comment {
    private int id, chapterId, status, bookId;
    private String name, content;
    @SerializedName("created_date")
    private String datetime;

    public int getId() {
        return id;
    }

    public int getChapterId() {
        return chapterId;
    }

    public int getStatus() {
        return status;
    }

    public int getBookId() {
        return bookId;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
