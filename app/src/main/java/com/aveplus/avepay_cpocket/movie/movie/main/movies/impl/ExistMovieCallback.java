package com.aveplus.avepay_cpocket.movie.movie.main.movies.impl;

/**
 */
public interface ExistMovieCallback {
    public void onCompleted(boolean isExist);
    public void onFailed(Exception e);
}
