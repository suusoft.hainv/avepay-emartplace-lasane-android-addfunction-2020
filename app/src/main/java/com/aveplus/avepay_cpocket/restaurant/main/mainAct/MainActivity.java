package com.aveplus.avepay_cpocket.restaurant.main.mainAct;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.main.home.HomeFrag;
import com.aveplus.avepay_cpocket.restaurant.main.notification.NotificationFragment;
import com.aveplus.avepay_cpocket.restaurant.main.profile.ProfileFragment;
import com.aveplus.avepay_cpocket.restaurant.main.save.SaveFragment;
import com.aveplus.avepay_cpocket.view.fragments.MyAccountMyInfoFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;


/**
 * Created by SuuSoft on 3/28/2018.
 */

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener{
    private final static String TAG = MainActivity.class.getSimpleName();



    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_main_restaurant;
    }

    @Override
    protected void initView() {
        BottomNavigationView navVieww = findViewById (R.id.nav_vieww);
        navVieww.setOnNavigationItemSelectedListener (this);






    }

    @Override
    protected void onViewCreated() {
        loadFragment (new HomeFrag());

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {

    }


    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager ()
                    .beginTransaction ()
                    .replace (R.id.root_layout, fragment)
                    .commit ();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch (menuItem.getItemId ()) {
            case R.id.menunav_home:
                fragment = new HomeFrag ();
                break;

            case R.id.menunav_save:
                fragment = new SaveFragment();
                break;

//            case R.id.menunav_history:
//                fragment = new HistoryFragment ();
//                break;

            case R.id.menunav_notification:
                fragment = new NotificationFragment();
                break;

            case R.id.menunav_profile:
                fragment = new MyAccountMyInfoFragment();
                break;

                case R.id.menunav_support:
                    String version = "com.whatsapp";
                    if (isAppExist(version)) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=+601115291110"));
                        intent.setPackage(version);
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + version)));
                    }
                break;

        }

        return loadFragment (fragment);
    }

    private boolean isAppExist(String version) {
        PackageManager pm = this.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(version, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void finish() {
        super.finish();
    }
}
