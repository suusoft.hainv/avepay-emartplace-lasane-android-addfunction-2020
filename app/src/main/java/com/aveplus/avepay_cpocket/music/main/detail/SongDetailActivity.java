package com.aveplus.avepay_cpocket.music.main.detail;

import android.os.Bundle;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseActivityBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.model.Song;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class SongDetailActivity extends BaseActivityBinding {

    SongDetailVM viewModel;
    Song item;


    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {
        Bundle bundle = getIntent().getBundleExtra(Constant.PREF_KEY_DATA);
        item = bundle.getParcelable(Constant.PREF_KEY_OBJECT);

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new SongDetailVM(self, item);
        return viewModel;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_song_detail;
    }

    @Override
    protected void onViewCreated() {
        initToolbar();
        initToolBarNav();
        setToolbarTitle(item.title);
    }

}
