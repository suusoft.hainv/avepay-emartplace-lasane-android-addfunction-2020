package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ScheduleAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.cart.CartManager;
import com.aveplus.avepay_cpocket.trainingcenter.object.Class;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.aveplus.avepay_cpocket.trainingcenter.object.Schedule;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class DetailSchedule extends BaseFragment {

    private TextView txttitle, tvWeek;
    private TextView tvPrice;
    private ImageView imgservice;
    private LinearLayout llParent;
    private TextView tvExpired;
    private RelativeLayout btnAddToCart;
    private ArrayList<Schedule> mListSchedule;
    private ListView mLvListClass;
    private ScheduleAdapter mScheduleAdapter;
    private Product product;
    private Class aClass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.detail_schedule, container, false);
        intiView(view);
        intiData();
        initControl();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("schedule");
        else
            Log.i(TAG, "onTitleChanged is null");
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("classes");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    public void intiView(View v) {
        txttitle = (TextView) v.findViewById(R.id.txtTitleServicesDetail);
        tvWeek = (TextView) v.findViewById(R.id.tvWeek);
        imgservice = (ImageView) v.findViewById(R.id.imgServiceDetail);
        mLvListClass = (ListView) v.findViewById(R.id.lvListSchedules);
        btnAddToCart = (RelativeLayout) v.findViewById(R.id.btnAddToCart);
        llParent = (LinearLayout) v.findViewById(R.id.llParent);
        tvExpired = (TextView) v.findViewById(R.id.tvExpired);
        txttitle.setSelected(true);
        tvPrice = (TextView) v.findViewById(R.id.tvPrice);
    }

    private void initControl() {
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!CartManager.getInstance().isExist(product)) {
                    product.setNumberCart(1);
                    product.setIdClassChose(aClass.getIdClass());
                    //CartManager.getInstance().getYourCart().add(product);
                    CartManager.getInstance().addCart(product);
                    Toast.makeText(getActivity(),
                            "Add " + product.getName() + " To Cart Successfully",
                            Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(),
                            "This item have already existed !",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void intiData() {
        Bundle bundle = this.getArguments();
        product = bundle.getParcelable("item");
        aClass = bundle.getParcelable("Class");
        if (aClass.isExpired()) {
            llParent.setVisibility(View.GONE);
            tvExpired.setVisibility(View.VISIBLE);
            btnAddToCart.setEnabled(false);
        } else {
            llParent.setVisibility(View.VISIBLE);
            tvExpired.setVisibility(View.GONE);
            btnAddToCart.setEnabled(true);
        }

        mListSchedule = aClass.getmListSchedule();
        mScheduleAdapter = new ScheduleAdapter(mListSchedule);
        mLvListClass.setAdapter(mScheduleAdapter);
        txttitle.setText(product.getName());
        tvWeek.setText(aClass.getmStartDate() + " - " + aClass.getmEndDate());
        tvPrice.setText(product.getPrice());
        Picasso.get().load(product.getImage().get(0)).into(imgservice);
    }

}
