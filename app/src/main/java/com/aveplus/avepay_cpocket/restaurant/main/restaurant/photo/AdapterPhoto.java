package com.aveplus.avepay_cpocket.restaurant.main.restaurant.photo;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.ImageVideo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;


import java.util.List;

public class AdapterPhoto extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ImageVideo> imageVideoList;
    private int ITEM_VIEWHOLER = 1;
    private int ITEM_LAST_PHOTO = 2;
    private ViewGroup.LayoutParams layoutParams;
    private final OnItemClickListener listener;

    public AdapterPhoto(Context context, List<ImageVideo> imageVideoList, int Wdp, OnItemClickListener listener) {
        this.context = context;
        this.imageVideoList = imageVideoList;
        this.listener = listener;
        initParam(Wdp);
    }

    public interface OnItemClickListener {
        void onItemClick(List<ImageVideo> imageVideo, int position);
    }

    private void initParam(int wDp) {
        int w, h;
        w = wDp / 3;
        h = wDp / 3;
        layoutParams = new RelativeLayout.LayoutParams(w, h);
    }

    @Override
    public int getItemViewType(int position) {
        if (imageVideoList.size() > 6 && position == 5) {
            return ITEM_LAST_PHOTO;
        } else {
            return ITEM_VIEWHOLER;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.e("TAG", "onCreateViewHolder: " + i);
        if (i == ITEM_VIEWHOLER) {
            View view = LayoutInflater.from(context).inflate(R.layout.row_photo, viewGroup, false);
            view.setLayoutParams(layoutParams);
            return new ItemPhoto(view);
        }
        if (i == ITEM_LAST_PHOTO) {
            View view2 = LayoutInflater.from(context).inflate(R.layout.row_photo_last, viewGroup, false);
            view2.setLayoutParams(layoutParams);
            return new LastPhoto(view2);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.row_photo, viewGroup, false);
            view.setLayoutParams(layoutParams);
            return new ItemPhoto(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ItemPhoto) {
            ((ItemPhoto) viewHolder).Container(imageVideoList.get(i), i);
        }
        if (viewHolder instanceof LastPhoto) {
            ((LastPhoto) viewHolder).Container(imageVideoList.get(i), i);
        }

        Log.e("Image2", "onBindViewHolder: " + imageVideoList);
    }

    @Override
    public int getItemCount() {
        if (imageVideoList.size() > 6) {
            return 6;
        } else {
            return imageVideoList.size();
        }
    }

    private class ItemPhoto extends RecyclerView.ViewHolder {
        private ImageView img_photo;

        private ItemPhoto(@NonNull View itemView) {
            super(itemView);
            img_photo = itemView.findViewById(R.id.img_photo);
        }

        void Container(ImageVideo imageVideo, final int position_img) {
            Glide.with(context).load(imageVideo.getImage()).apply(new RequestOptions()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .override(Target.SIZE_ORIGINAL)).into(img_photo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(imageVideoList, position_img);
                }
            });
        }
    }

    private class LastPhoto extends RecyclerView.ViewHolder {
        private TextView tvListsize;
        private ImageView imgPhotoLast;

        private LastPhoto(@NonNull View itemView) {
            super(itemView);
            tvListsize = itemView.findViewById(R.id.tv_listsize);
            imgPhotoLast = itemView.findViewById(R.id.img_photo_last);
        }

        void Container(ImageVideo imageVideo, final int position) {

            Glide.with(context).load(imageVideo.getImage()).apply(new RequestOptions()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .override(Target.SIZE_ORIGINAL)).into(imgPhotoLast);
            String size = "+ " + (imageVideoList.size() - 6);
            tvListsize.setText(size);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(imageVideoList, position);
                }
            });

        }
    }
}