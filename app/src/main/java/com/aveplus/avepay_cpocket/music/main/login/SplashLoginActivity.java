package com.aveplus.avepay_cpocket.music.main.login;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.GlobalFunctions;
import com.aveplus.avepay_cpocket.music.main.main2.MainActivity2;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewCondensedItalic;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;


public class SplashLoginActivity extends BaseActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = SplashLoginActivity.class.getSimpleName();

    private static final int RC_GOOGLE_SIGN_IN = 9001;
    private static final int RC_PERMISSIONS = 1;

    private TextView mLblLogin, mLblFacebook, mLblGoogle;
    private TextView mLblSignUp;
    private TextView mLblLoginLater;
    private TextViewCondensedItalic tvGuide;

    private GoogleApiClient mGoogleApiClient;

    private CallbackManager mCallbackManager;

    private RequestQueue mRequestQueue;

    private View mClickedView; // Keep button which was just clicked(google, facebook or login)

    private Bundle mBundle;
    private String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRequestQueue = Volley.newRequestQueue(self);
        initGoogleApiClient();
        // Init facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext());

    }

    @Override
    void inflateLayout() {
        setContentView(R.layout.activity_splash_reskin_music);
    }

    @Override
    void initUI() {
        // Hide actionbar
        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        mLblGoogle = findViewById(R.id.lbl_google_login);
        mLblFacebook = findViewById(R.id.lbl_facebook_login);
        mLblLogin = findViewById(R.id.lbl_login);
        mLblSignUp = findViewById(R.id.lbl_signup);
        mLblLoginLater = findViewById(R.id.lbl_loginlater);
        tvGuide = findViewById(R.id.tv_guide);
    }

    @Override
    void initControl() {
        mLblGoogle.setOnClickListener(this);
        mLblFacebook.setOnClickListener(this);
        mLblLogin.setOnClickListener(this);
        mLblSignUp.setOnClickListener(this);
        mLblLoginLater.setOnClickListener(this);
        tvGuide.setOnClickListener(this);
        Log.e("EE", "EE: SPLASH");

    }

    @Override
    protected void getExtraValues() {
        mBundle = getIntent().getExtras();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mBundle = intent.getExtras();
        Log.e("EE", "EE: NEW IN SPLASH");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCallbackManager != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSIONS: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED
                            || grantResults[1] == PackageManager.PERMISSION_DENIED) {
                        showPermissionsReminder(RC_PERMISSIONS, true);
                    } else {
                        if (mClickedView == mLblGoogle) {

                        } else if (mClickedView == mLblFacebook) {

                        } else if (mClickedView == mLblLogin) {
//                            Bundle bundle = new Bundle();
//
//                            bundle.putString(MainActivity.LOG_OUT, text);
                            GlobalFunctions.startActivityWithoutAnimation(self, LoginActivity.class);
                            finish();
                        }
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {

        if (v == mLblGoogle) {
            mClickedView = mLblGoogle;

            // Check permissions
//            if (GlobalFunctions.isGranted(self, new String[]{Manifest.permission.READ_PHONE_STATE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSIONS, "")) {

//            }
        } else if (v == mLblFacebook) {
            mClickedView = mLblFacebook;

            // Check permissions
//            if (GlobalFunctions.isGranted(self, new String[]{Manifest.permission.READ_PHONE_STATE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSIONS, "")) {

//            }
        } else if (v == mLblLogin) {

            mClickedView = mLblLogin;
            GlobalFunctions.startActivityWithoutAnimation(self, LoginActivity.class);

            finish();
            //    }
        } else if (v == mLblSignUp) {
            GlobalFunctions.startActivityWithoutAnimation(self, SignUpActivity.class);
            finish();
        } else if (v == mLblLoginLater) {
            GlobalFunctions.startActivityWithoutAnimation(self, MainActivity2.class);
            finish();
        } else if (v == tvGuide) {
            //GlobalFunctions.startActivityWithoutAnimation(self, GuideActivity.class);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void initGoogleApiClient() {
        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(self, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .enableAutoManage(this, this)
                .build();
        // [END build_client]
    }


    private void gotoHomePage() {
        if (mBundle == null) {
            mBundle = new Bundle();
        }

//        Log.e("EEEE", "EE: " + mBundle.getString(Args.KEY_ID_DEAL));
        GlobalFunctions.startActivityWithoutAnimation(self, MainActivity2.class, mBundle);
        Log.e("Splash", "gotoHomePage");
        // Close this activity
        finish();
    }


    @Override
    public void onBackPressed() {
        //   gotoHomePage ();
        // super.onBackPressed();
    }
}