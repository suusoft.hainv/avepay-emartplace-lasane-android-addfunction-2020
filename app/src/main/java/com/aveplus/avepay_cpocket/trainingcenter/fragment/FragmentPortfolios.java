package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.PortfoliosAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.Portfolios;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshBase;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshGridView;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class FragmentPortfolios extends BaseFragment {

    private EditText txtSearch;
    private Context context;
    private ArrayList<Portfolios> listPort = new ArrayList<Portfolios>();
    private PullToRefreshGridView gridPort;
    private PortfoliosAdapter mAdapter;
    private int currentPage = 1, totalPage = 1;
    private boolean isPullRefresh = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("portfolios");
        else
            Log.i(TAG, "onTitleChanged is null");

    }

    @Override
    public void onPause() {
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_portforlios, container, false);
        initView(view);
        initUIControls();
        refreshGridView(!isPullRefresh);
        return view;
    }

    private void initGridView() {
        mAdapter = new PortfoliosAdapter(getActivity(), listPort);
        gridPort.setAdapter(mAdapter);
        gridPort.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {

            @Override
            public void onPullDownToRefresh(
                    PullToRefreshBase<GridView> refreshView) {

                String label = DateUtils.formatDateTime(getActivity(),
                        System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
                                | DateUtils.FORMAT_SHOW_DATE
                                | DateUtils.FORMAT_ABBREV_ALL);
                refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

                refreshGridView(isPullRefresh);
            }

            @Override
            public void onPullUpToRefresh(
                    PullToRefreshBase<GridView> refreshView) {

                if (currentPage < totalPage) {
                    currentPage++;
                    loadMorePage(isPullRefresh);
                } else {
                    showNoMoreData();
                }
            }

        });
    }

    private void refreshGridView(boolean isPull) {
        currentPage = 1;
        getData(txtSearch.getText().toString(), currentPage, isPull);
    }

    private void loadMorePage(boolean isPull) {
        getData(txtSearch.getText().toString(), currentPage, isPull);
    }

    private void onSearch(String key) {
        currentPage = 1;
        getData(key, currentPage, !isPullRefresh);
    }

    private void initUIControls() {
        initGridView();
        txtSearch.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    onSearch(v.getText().toString());
                }
                return false;
            }
        });
        gridPort.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void getData(String keyword, final int currentpage2, boolean isPull) {
        ModelManager.getListPortfolios(currentpage2, keyword,
                new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        if (currentpage2 <= 1) {
                            listPort.clear();
                        }
                        listPort.addAll(ParseUtility.parseListPort(response.getRootStr()));
                        totalPage = ParseUtility.getAllPage(response.getRootStr());
                        mAdapter.notifyDataSetChanged();
                        closeLoadingPull();
                    }

                    @Override
                    public void onError(String message) {
                        AppUtil.showToast(self, message);
                    }
                });
    }

    private void initView(View view) {
        gridPort = (PullToRefreshGridView) view.findViewById(R.id.gridPort);
        txtSearch = (EditText) view.findViewById(R.id.txtSearchPort);
    }

    private void showNoMoreData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gridPort.onRefreshComplete();
            }
        }, 1000);
    }

    private void closeLoadingPull() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                gridPort.onRefreshComplete();
            }
        }, 1000);
    }
}
