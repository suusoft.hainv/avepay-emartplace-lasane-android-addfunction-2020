package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.OnFocusClickMovie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;


import java.util.List;


/**
 * Copyright © 2019 SUUSOFT
 */
public class AdapterPlayerMovie  extends ArrayAdapter<Movie> {
    private int mResource;
    private List<Movie> mMovies;
    private LayoutInflater inflater;
    private ImageLoader imageLoader;
    private int mPosition;
    private Context mContext;
    private OnFocusClickMovie focusClickMovie;

    public AdapterPlayerMovie(Context context, int resource, List<Movie> objects, OnFocusClickMovie focusClickMovie) {
        super(context, resource, objects);
        this.mResource = resource;
        this.mMovies = objects;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        imageLoader = RequestManager.getImageLoader();
        mContext = context;
        this.focusClickMovie = focusClickMovie;
    }

    @SuppressLint("WrongViewCast")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        HolderView holderView;
        if (view == null) {

            view = inflater.inflate(mResource, null);
            holderView = new HolderView();
            holderView.imgMovie = (NetworkImageView) view.findViewById(R.id.imageView);
            view.setTag(holderView);
        } else
            holderView = (HolderView) view.getTag();
        if (mMovies != null) {
            holderView.imgMovie.setImageUrl(mMovies.get(position).getMovie_thumb(), imageLoader);
            holderView.imgMovie.setDefaultImageResId(R.drawable.poster);
            holderView.imgMovie.setErrorImageResId(R.drawable.poster);
        }
        view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                   focusClickMovie.onFocusMovie(position ,hasFocus);
                }
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusClickMovie.onClickMovie(position);
            }
        });
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if(keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER ) {
                        focusClickMovie.onClickMovie(position);
                        return true;
                    }
                    if(keyCode == KeyEvent.KEYCODE_DPAD_RIGHT ) {
                        if(position != 0 && position % 7 == 0) {
                            focusClickMovie.onNextPageMovie(position);
                            return true;
                        }

                    }
                    if(keyCode == KeyEvent.KEYCODE_DPAD_LEFT ) {
                        if(position != 0 && position % 7 == 0) {
                            focusClickMovie.onPrevMovie(position);
                            return true;
                        }

                    }
                }
                return false;
            }
        });
        return view;
    }

    class HolderView {
        public NetworkImageView imgMovie;
    }
}