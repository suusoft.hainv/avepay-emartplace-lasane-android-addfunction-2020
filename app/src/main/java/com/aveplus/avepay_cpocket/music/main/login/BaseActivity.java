package com.aveplus.avepay_cpocket.music.main.login;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.GlobalFunctions;


public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    protected BaseActivity self;

    private static final Handler mainThreadHandler = new Handler(Looper.getMainLooper());
    protected boolean isAppSessionActive;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateLayout();
        self = this;
        getExtraValues();
        initUI();
       initControl();

//        // Init quickblox
//        if (DataStoreManager.getUser() != null && (DataStoreManager.getToken() != null
//                && !DataStoreManager.getToken().equals(""))) {
//            initSession(savedInstanceState);
//            initDialogsListener();
//            initPushManager();
//        }
    }

    abstract void inflateLayout();

    abstract void initUI();

    abstract void initControl();




    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isAppSessionActive) {

        }
    }

    protected void onSessionCreated() {
    }

    protected void getExtraValues() {
    }

    protected void showPermissionsReminder(final int reqCode, final boolean flag) {
        GlobalFunctions.showConfirmationDialog(self, getString(R.string.msg_remind_user_grants_permissions),
                getString(R.string.allow), getString(R.string.no_thank), false, new GlobalFunctions.IConfirmation() {
                    @Override
                    public void onPositive() {
                        GlobalFunctions.isGranted(self, new String[]{Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.ACCESS_FINE_LOCATION}, reqCode, null);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            finish();
                        }
                    }
                });
    }














    private BroadcastReceiver pushBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("message");
            String from = intent.getStringExtra("from");
            Log.e(TAG, "Receiving message: " + message + ", from " + from);
        }
    };


}
