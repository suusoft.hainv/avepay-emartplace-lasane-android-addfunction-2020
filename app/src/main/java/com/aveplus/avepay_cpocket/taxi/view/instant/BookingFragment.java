package com.aveplus.avepay_cpocket.taxi.view.instant;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.configs.Action;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.interfaces.IChangeTitle;
import com.aveplus.avepay_cpocket.taxi.interfaces.IConfirmation;
import com.aveplus.avepay_cpocket.taxi.interfaces.IPassenger;
import com.aveplus.avepay_cpocket.taxi.interfaces.ITransport;
import com.aveplus.avepay_cpocket.taxi.interfaces.IVehicle;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.TripPrice;
import com.aveplus.avepay_cpocket.taxi.objects.book.DriverObj;
import com.aveplus.avepay_cpocket.taxi.objects.book.ServiceObj;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseTypeDriver;
import com.aveplus.avepay_cpocket.taxi.schedule.ui.phone.SchedulActivity;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.CheckPermission;
import com.aveplus.avepay_cpocket.taxi.utils.DialogUtil;
import com.aveplus.avepay_cpocket.taxi.utils.LocationUtils;
import com.aveplus.avepay_cpocket.taxi.utils.NetworkUtility;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.LocationService;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.taxi.view.adapters.PassengerQuantityAdapter;
import com.aveplus.avepay_cpocket.taxi.view.adapters.PlaceAdapter;
import com.aveplus.avepay_cpocket.taxi.view.adapters.Transport1Adapter;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;
import static com.aveplus.avepay_cpocket.taxi.utils.CheckPermission.RC_PERMISSIONS;

/**
 * Created by SuuSoft.com on 10/05/2017.
 */

public class BookingFragment extends com.aveplus.avepay_cpocket.base.BaseFragment implements View.OnClickListener, OnMapReadyCallback,
        GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMarkerClickListener {

    private static final String TAG = BookingFragment.class.getSimpleName();
    private static final int RC_LOCATION = 1;
    private static final int RC_TURN_ON_LOCATION = 2;
    private static final int RC_GET_DEPARTURE = 3;
    private static final int RC_GET_DESTINATION = 4;
    public static boolean mMapIsTouched = false;
    private IChangeTitle iChangeTitle;
    private Location midlocation;
    private GoogleMap mMap;
    private TextViewRegular tvPickUp, tvAddressDestination, btnBooking;
    private View loPickUp, loDestination;
    private ImageView btnPickUp, btnDestination;
    private TextView mTvDistance, tvNotifi;
    private View btnCancel;
    private RelativeLayout progressBar;
    private SupportMapFragment mMapFragment;
    // Use google api to get current location
    //private GoogleApiClient mGoogleApiClient;
    private Place myPlace;
    private boolean mDepartureRequested, mDestinationRequested;
    private LatLng mylatLng;
    private LatLng fromLatLng, toLatLng;
    private Marker myLocMarker, mFromMarker, mToMarker;
    private Marker mMyLocMarker;
    // Select transport type and number of passengers
    private String mSelectedTransportType = ServiceObj.ALL;
    private LinearLayout mLlTransport, mLlPassengerQuantity, mLlListTransport;
    private ImageView mImgTransportType;
    private TextViewRegular mLblTransportType, mLblPassengerQuantity;
    private RecyclerView rcvChooseService, mRclPassengerQuantity;
    //info service
    private LinearLayout loInforSercive;
    private TextView tvNameService, tvPriceMin, tvPriceMinAndKilomet, tvPerson, tvPrice;
    private ArrayList<TripObj> mNearbyDriverDeals;
    private ArrayList<DriverObj> listDrivers;
    private ArrayList<Marker> mDriverMarkers;
    private TripObj mSelectedDriver;
    private ServiceObj chooseService;
    private TripObj tripObj;
    //list place recent
    private View loListPlace;
    private ArrayList<Location> listPlace;
    private RecyclerView rcvPlace;
    private PlaceAdapter placeAdapter;
    private ImageView btnRecentDeparture, btnRecentDestination, btnClose;
    private String mSelectedDriverId;
    //list vehicle bottom
    private Transport1Adapter typeDriverAdapter;
    private RecyclerView rcyVehicle;
    private ArrayList<ServiceObj> typeDrivers;
    private boolean isGetAutoComplete = false;
    private Dialog dialogEstimatePrice;
    private ImageView btnChoose;
    private CountDownTimer countDownTimer = new CountDownTimer(60 * 1000, /*30 **/ 1000) {
        @Override
        public void onTick(long l) {
            Log.e(TAG, "onTick");
        }

        @Override
        public void onFinish() {
            onCancel();
            countDownTimer.cancel();
            AppUtil.showToast(self, R.string.no_taxi_is_available_at_the_moment_please_try_again_later);
        }
    };
    private FusedLocationProviderClient mFusedLocationClient;
    //để tắt sự câp nhật location khi vị trí trung tâm camera được đánh dấu
    private boolean movedToMyLocation = false;
    private LatLng oldLatLng;
    private LocationManager mLocationManager;
    private Location locationOld;
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            if (LocationUtils.isPointChanged(locationOld, location, TAG)) {
                Log.e("location", "onLocationChanged true");
                locationOld = location;

                mylatLng = new LatLng(location.getLatitude(), location.getLongitude());
                mMyLocMarker = addMarker(mMyLocMarker, mylatLng, R.drawable.ic_place_to, false);
                MapsUtil.moveCameraTo(mMap, mylatLng);
                AppController.setMyLatLng(mylatLng);

            }


        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

            Log.e("location", "onStatusChanged");
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e("location", "onProviderEnabled");

        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e("location", "onProviderDisabled");

        }
    };

    public static BookingFragment newInstance() {
        Bundle args = new Bundle();
        BookingFragment fragment = new BookingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_instant_booking;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        tripObj = new TripObj();
        initControls(view);
        initGoogleApiClient();
        initPassengerQuantity();
        initListVehicle(view);
    }

    private void initListVehicle(View view) {
        rcyVehicle = (RecyclerView) view.findViewById(R.id.rcv_vehicle);
        typeDrivers = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false);
        rcyVehicle.setLayoutManager(layoutManager);
        rcyVehicle.setHasFixedSize(true);

        typeDriverAdapter = new Transport1Adapter(getContext(), typeDrivers, new ITransport() {
            @Override
            public void onTransportSelected(ServiceObj serviceObj) {
                tripObj = new TripObj();
                chooseService = serviceObj;
                inforTrip(serviceObj);
                setDataService(serviceObj);
                showEstimatePrice();

            }
        });

        rcyVehicle.setAdapter(typeDriverAdapter);

        getListTypeDriver();
    }

    private void getListTypeDriver() {
        Call<ResponseTypeDriver> call = ApiUtils.getAPIServiceTypeTaxi().getListTypeDriver(DataStoreManager.getTokenUser());
        Log.d(TAG, "getListTypeDriver: " + call.request());
        call.enqueue(new Callback<ResponseTypeDriver>() {
            @Override
            public void onResponse(Call<ResponseTypeDriver> call, Response<ResponseTypeDriver> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isSuccess()) {
                            typeDrivers.clear();
                            typeDrivers.addAll(response.body().data);
                            typeDriverAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseTypeDriver> call, Throwable t) {
                Log.d(TAG, "onFailureGetDriver: " + t.getMessage());
            }
        });


    }

    private void showListPlaceRecent() {
        listPlace.clear();
        listPlace.addAll(DataStoreManager.getRecentPlace().getListPlaces());
        placeAdapter.notifyDataSetChanged();
        loListPlace.setVisibility(View.VISIBLE);


    }

    private void hideListPlaceRecent() {
        loListPlace.setVisibility(View.GONE);
    }

    private void initControls(View view) {

//        receiverTripAction = new ReceiverTripAction();
//        intentFilter = new IntentFilter(Action.ACTION_ACCEPT);
//        intentFilter.addAction(Action.ACTION_CANCEL);

        mMapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        mMapFragment.getMapAsync(this);


        btnBooking = (TextViewRegular) view.findViewById(R.id.tv_booking);
        mTvDistance = (TextView) view.findViewById(R.id.tv_distance);
        tvPickUp = (TextViewRegular) view.findViewById(R.id.lbl_from);
        tvAddressDestination = (TextViewRegular) view.findViewById(R.id.lbl_to);
        btnPickUp = (ImageView) view.findViewById(R.id.img_location_departure);
        btnDestination = (ImageView) view.findViewById(R.id.img_location_destination);
        loPickUp = view.findViewById(R.id.lo_pick_up);
        loDestination = view.findViewById(R.id.lo_destination);

        mLlTransport = (LinearLayout) view.findViewById(R.id.ll_transport_type);
        mLlListTransport = view.findViewById(R.id.ll_list_vehicle);
        mImgTransportType = (ImageView) view.findViewById(R.id.img_transport_type);
        mLblTransportType = (TextViewRegular) view.findViewById(R.id.lbl_transport_type);
        mLblTransportType.setSelected(true);
        mLlPassengerQuantity = (LinearLayout) view.findViewById(R.id.ll_passenger_quantity);
        mLblPassengerQuantity = (TextViewRegular) view.findViewById(R.id.lbl_passenger_quantity);

        btnCancel = view.findViewById(R.id.btn_cancel);
        progressBar = view.findViewById(R.id.lo_loading);
        loInforSercive = view.findViewById(R.id.lo_infor_service);
        tvNameService = view.findViewById(R.id.tv_name);
        tvPriceMin = view.findViewById(R.id.tv_price_min);
        tvPrice = view.findViewById(R.id.tv_price);
        tvNotifi = view.findViewById(R.id.tv_notif);
        //tvPriceMinAndKilomet = view.findViewById(R.id.tv_price_min_kilomet);
        tvPerson = view.findViewById(R.id.tv_person);

        mRclPassengerQuantity = (RecyclerView) view.findViewById(R.id.rcl_passenger_quantity);
        mRclPassengerQuantity.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));
        mRclPassengerQuantity.setHasFixedSize(true);

        btnBooking.setOnClickListener(this);
//        tvPickUp.setOnClickListener(this);
//        tvAddressDestination.setOnClickListener(this);
        btnPickUp.setOnClickListener(this);
        btnDestination.setOnClickListener(this);
        mLlTransport.setOnClickListener(this);
        mLlPassengerQuantity.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        loPickUp.setOnClickListener(this);
        loDestination.setOnClickListener(this);

    }

    @Override
    protected void getData() {

    }

    private void inforTrip(ServiceObj serviceObj) {
        tripObj = new TripObj();
        tripObj.setType(TripObj.TYPE_INSTANT_BOOKING);
        tripObj.setLatLngPickup(fromLatLng);
        tripObj.setLatLngDestination(toLatLng);
        tripObj.setServiceObj(serviceObj);
        tripObj.setName(tvPickUp.getText().toString());
        tripObj.setPickup(tvPickUp.getText().toString());
        tripObj.setDestination(tvAddressDestination.getText().toString());
        Log.e("YYY", tripObj.getServiceObj().getPriceKilomet() + "");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        if (!isGetAutoComplete) {
            initEnableLocation();

        } else {
            isGetAutoComplete = false;
        }


//        getActivity().registerReceiver(receiverTripAction, intentFilter);
        ArrayList<ServiceObj> serviceObjs = ServiceObj.getListService();
        setDataService(serviceObjs.get(0));
        //checkBookLater();

    }

    public void checkBookLater() {
        if (DataStoreManager.isBookLater()) {
            Log.e(TAG, "checkBookLater");
            tripObj = DataStoreManager.getBookLater();

            findDriver();
            DataStoreManager.saveBookLater(false);
            DataStoreManager.removeBookLater();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
//        getActivity().unregisterReceiver(receiverTripAction);
    }

    private void slideOut() {
        mLlListTransport.setVisibility(View.GONE);
        Animation slideout = AnimationUtils.loadAnimation(self, R.anim.slide_out_bottom);
        mLlListTransport.startAnimation(slideout);
    }

    private void setDataService(ServiceObj serviceObj) {
        mImgTransportType.setImageResource(serviceObj.getIcon());
        mLblTransportType.setText(serviceObj.getName());
        tvPerson.setText(serviceObj.getPerson() + "");
        tvPriceMin.setText(StringUtil.convertNumberToPrice(serviceObj.getMinimumFare()));
    }

    public void showEstimatePrice() {
    }

    private void initGoogleApiClient() {

    }

    private void showDialogBooking() {

        if (dialogEstimatePrice == null)
            dialogEstimatePrice = DialogUtil.setDialogCustomView(self, R.layout.dialog_estimate_price, false);

        LinearLayout loDialog = dialogEstimatePrice.findViewById(R.id.lo_dialog);


        dialogEstimatePrice.show();
        TextView tvEstimatePrice = dialogEstimatePrice.findViewById(R.id.tv_price_estimate);
        TextView btnBookNow = dialogEstimatePrice.findViewById(R.id.btn_book_now);
        TextView btnNegociatePrice = dialogEstimatePrice.findViewById(R.id.btn_negociate_price);
        TextView btnBookLater = dialogEstimatePrice.findViewById(R.id.btn_book_later);
        TextView btnCancel = dialogEstimatePrice.findViewById(R.id.btn_cancel);

        btnNegociatePrice.setVisibility(View.GONE);
        tvEstimatePrice.setText(StringUtil.convertNumberToPrice(tripObj.getEstimateFare()));

        btnBookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogEstimatePrice.dismiss();
                bookNow();
            }
        });

        btnNegociatePrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogEstimatePrice.dismiss();

                bookNegociatePrice();
            }
        });

        btnBookLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogEstimatePrice.dismiss();
                bookingLater();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogEstimatePrice.dismiss();
            }
        });

    }

    private void bookNegociatePrice() {
//        AppUtil.showToast(self, "Opening soon");
        EventBus.getDefault().post(new Global.PushBookNegociatePrice(tripObj));
        Log.e(TAG, "bookNegociatePrice trip: " + new Gson().toJson(tripObj));


    }

    private void bookNow() {

        findDriver();
    }

    private void bookingLater() {
        DataStoreManager.saveBookLater(tripObj);
        AppUtil.startActivity(self, SchedulActivity.class);

    }

    private void showInfoCar(ServiceObj serviceObj) {
        loInforSercive.setVisibility(View.VISIBLE);
        tvNameService.setText(serviceObj.getName());
        tvPerson.setText(serviceObj.getPerson() + "");
        tvPriceMin.setText(String.format(getString(R.string.price_), String.valueOf(serviceObj.getMinimumFare())));
        tvPriceMinAndKilomet.setText(String.format(getString(R.string.price_percent_min_and_kilomet),
                String.valueOf(serviceObj.getPriceMinute()),
                String.valueOf(serviceObj.getPriceKilomet())));
    }

    private void onChangeTitle(int title) {
        iChangeTitle.onChangTitle(getResources().getString(title));
    }

    private void getNearbyDrivers() {
        if (oldLatLng != null) {
            Log.e("user", "lat " + oldLatLng.latitude + " " + fromLatLng.latitude);
            Log.e("user", "lat " + oldLatLng.longitude + " " + fromLatLng.longitude);
        }

        oldLatLng = fromLatLng;


    }

    private void addDriverMarkers() {
        if (mNearbyDriverDeals != null) {
            if (mNearbyDriverDeals.size() > 0) {
                for (int i = 0; i < mNearbyDriverDeals.size(); i++) {
                    TripObj tripObj = mNearbyDriverDeals.get(i);
                    Marker marker = addMarker(null,
                            tripObj.getLatLngDriver(),
                            R.drawable.ic_car
                            /*getTransportIcon(tripObj)*/, false);
                    marker.setTag(tripObj.getDriverId());
                    mDriverMarkers.add(marker);
                }
            }
        }
    }

    private void addNearDriverMarkers() {
        if (listDrivers != null) {
            if (listDrivers.size() > 0) {
                for (int i = 0; i < listDrivers.size(); i++) {
                    DriverObj driverObj = listDrivers.get(i);
                    Marker marker = addMarker(null,
                            driverObj.getLatLng(),
                            R.drawable.ic_car, false);
                    marker.setTag(driverObj.getUser_id());
                    mDriverMarkers.add(marker);
                }
            }
        }
    }

    protected int getTransportIcon(TripObj tripObj) {
        int icon = R.drawable.ic_taxi_type;
        if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.REGULAR)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_taxi_delivery_type;
            } else {
                icon = R.drawable.ic_taxi_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.AIR_CONDITIONED)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_vip_delivery_type;
            } else {
                icon = R.drawable.ic_vip_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.SUV)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_lifts_delivery_type;
            } else {
                icon = R.drawable.ic_lifts_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.VIP1)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_moto_delivery_type;
            } else {
                icon = R.drawable.ic_moto_type;
            }
        }

        return icon;
    }

    private Marker addMarker(Marker marker, LatLng latLng, int icon, boolean draggable) {
        // Clear old marker if it's
        if (marker != null) {
            marker.remove();
        }


        return MapsUtil.addMarker(mMap, latLng, "", icon, draggable);
    }

    private void initPassengerQuantity() {
        PassengerQuantityAdapter quantityAdapter = new PassengerQuantityAdapter(getContext(), new IPassenger() {
            @Override
            public void onQuantitySelected(String s) {
                mLblPassengerQuantity.setText(s);

                mRclPassengerQuantity.setVisibility(View.GONE);
                mLlPassengerQuantity.setBackgroundColor(getResources().getColor(R.color.transparent));
            }
        });

        mRclPassengerQuantity.setAdapter(quantityAdapter);
    }

    @Override
    public void onClick(View view) {
        if (view == loPickUp) {
            isGetAutoComplete = true;
            MapsUtil.getAutoCompletePlaces(BookingFragment.this, RC_GET_DEPARTURE);
        } else if (view == loDestination) {
            isGetAutoComplete = true;
            MapsUtil.getAutoCompletePlaces(BookingFragment.this, RC_GET_DESTINATION);
        } else if (view == btnPickUp) {
            if (mMap != null && mylatLng != null)
                MapsUtil.moveCameraTo(mMap, mylatLng);

            else {
                getLastLocation();
                AppUtil.showToast(self, R.string.please_wait_until_your_location_is_found);
            }

        } else if (view == btnDestination) {
            showCurrentLocation(btnDestination);
        } else if (view == mLlTransport) {
            checkPlace();
        } else if (view == mLlPassengerQuantity) {
            if (mRclPassengerQuantity.getVisibility() == View.GONE) {
                mRclPassengerQuantity.setVisibility(View.VISIBLE);
                mLlPassengerQuantity.setBackground(getResources().getDrawable(R.drawable.bg_radius_gray));
            } else {
                mRclPassengerQuantity.setVisibility(View.GONE);
                mLlPassengerQuantity.setBackgroundColor(getResources().getColor(R.color.transparent));
            }
        } else if (view == btnBooking) {
            if (fromLatLng == null) {
                Toast.makeText(self, R.string.msg_choose_name, Toast.LENGTH_SHORT).show();
            } else if (toLatLng == null) {
                Toast.makeText(self, R.string.msg_choose_destination, Toast.LENGTH_SHORT).show();
            } else if (fromLatLng != toLatLng) {
                setPickUp();
            }
        } else if (view == btnClose) {
            hideListPlaceRecent();
        }

        if (view == btnRecentDeparture) {
            if (btnChoose == btnRecentDestination) {
                showListPlaceRecent();
            } else if (loListPlace.isShown())
                hideListPlaceRecent();
            else
                showListPlaceRecent();

            btnChoose = btnRecentDeparture;

        } else if (view == btnRecentDestination) {
            if (btnChoose == btnRecentDeparture) {
                showListPlaceRecent();
            } else if (loListPlace.isShown())
                hideListPlaceRecent();
            else
                showListPlaceRecent();

            btnChoose = btnRecentDestination;

        } else if (view == btnCancel) {

            onCancel();
        }

    }

    public void hideProgress() {

        progressBar.setVisibility(View.GONE);
    }

    public void showProgress(TripObj tripObj) {
        this.tripObj = tripObj;
        progressBar.setVisibility(View.VISIBLE);
    }

    private void onCancel() {
        RequestManager.instantTripAction(Action.ACTION_CANCEL, tripObj.getId(), "", new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                countDownTimer.cancel();
                progressBar.setVisibility(View.GONE);
                AppUtil.showToast(self, "cancel trip");
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void checkPlace() {
        if (fromLatLng == null) {
            Toast.makeText(self, R.string.msg_choose_name, Toast.LENGTH_SHORT).show();
        } else if (toLatLng == null) {
            Toast.makeText(self, R.string.msg_choose_destination, Toast.LENGTH_SHORT).show();
        } else if (fromLatLng != toLatLng) {
            BottomSheetFragment bottomSheet = new BottomSheetFragment(new IVehicle() {
                @Override
                public void onService(ServiceObj serviceObj) {
                    tripObj = new TripObj();
                    chooseService = serviceObj;
                    inforTrip(serviceObj);
                    setDataService(serviceObj);
                    showEstimatePrice();
                }
            });
            bottomSheet.show(getActivity().getSupportFragmentManager(), bottomSheet.getTag());
        }
    }

    private void setPickUp() {
        tripObj.setPickup(MapsUtil.getAddressFromLatLng(self, fromLatLng));
        tripObj.setDestination(MapsUtil.getAddressFromLatLng(self, toLatLng));
        tripObj.setLatLngPickup(fromLatLng);
        tripObj.setLatLngDestination(toLatLng);
        tripObj.setServiceObj(chooseService);

        if (tripObj.getServiceObj() == null) {
            Toast.makeText(self, R.string.msg_choose_service, Toast.LENGTH_SHORT).show();
        } else if (tripObj.getServiceObj() != null) {

            getTripPrice();

        }
    }

    private void getTripPrice() {
        Log.d(TAG, "getTripPrice: ");
        RequestManager.tripPrice(tripObj.getServiceObj().getType() /*ServiceObj.ALL*/,
                tripObj.getLatLngPickup(), tripObj.getLatLngDestination(), "", new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {

                        TripPrice tripPrice = response.getDataObject(TripPrice.class);
                        Log.e(TAG, response.getRoot() + " json: " + new Gson().toJson(tripPrice));
                        tripObj.setEstimateFare(tripPrice.estimate_fare);
                        showDialogBooking();
                    }

                    @Override
                    public void onError(String message) {
                        AppUtil.showToast(self, message);
                    }
                });
    }

    private void findDriver() {

        progressBar.setVisibility(View.VISIBLE);
        String note = "";

        if (tripObj != null) {
            if (tripObj.getNote() != null)
                note = tripObj.getNote();
            if (NetworkUtility.isNetworkAvailable()) {


                RequestManager.findDriver(tripObj.getServiceObj().getType() /*ServiceObj.ALL*/,
                        tripObj.getLatLngPickup(), tripObj.getLatLngDestination(), note, tripObj.getType(), new BaseRequest.CompleteListener() {
                            @Override
                            public void onSuccess(ApiResponse response) {
                                //countDownTimer.start();
                                progressBar.setVisibility(View.GONE);
                                tripObj = response.getDataObject(TripObj.class);
                                tvNotifi.setText(getString(R.string.requesting_driver));

                                Log.e("find driver", "driver:" + new Gson().toJson(tripObj));

                            }

                            @Override
                            public void onError(String message) {
                                progressBar.setVisibility(View.GONE);
                                AppUtil.showToast(self, message);
                            }
                        });


            } else {
                Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void createBooking() {
//        tripObj = new TripObj();
//        tripObj.setName(tvPickUp.getText().toString());
//        tripObj.setAddressFrom(tvPickUp.getText().toString());
//        tripObj.setAddressTo(tvAddressDestination.getText().toString());
//        tripObj.setLatLngDestination(toLatLng);
//        tripObj.setLatLngPickup(fromLatLng);
//        tripObj.setServiceObj(chooseService);
//
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
//        AppUtil.startActivity(self, ConfirmBookingActivity.class, bundle);
    }

    private void keepTripInfo() {
        if (mNearbyDriverDeals != null) {
            if (mNearbyDriverDeals.size() > 0) {
                for (int i = 0; i < mNearbyDriverDeals.size(); i++) {
                    if (mSelectedDriverId.equals(mNearbyDriverDeals.get(i).getDriverId())) {
                        mSelectedDriver = mNearbyDriverDeals.get(i);
                        break;
                    }
                }
            }
        }

        mSelectedDriver.setPassengerQuantity(Integer.parseInt(mLblPassengerQuantity.getText().toString()));
        mSelectedDriver.setPickup(tvPickUp.getText().toString());
        mSelectedDriver.setDestination(tvAddressDestination.getText().toString());
        mSelectedDriver.setLatLngPickup(fromLatLng);
        mSelectedDriver.setLatLngDestination(toLatLng);
        mSelectedDriver.setPassengerId(DataStoreManager.getUser().getId());

    }

    private void showCurrentLocation(View view) {
        if (GlobalFunctions.locationIsGranted(getActivity(), RC_LOCATION, null)) {
            if (MapsUtil.locationIsEnable(self)) {
                initGetLocation();
            } else {
                MapsUtil.displayLocationSettingsRequest((AppCompatActivity) self, RC_TURN_ON_LOCATION);
            }
        }

        // If GoogleApiClient isn't connected, process the user's request by
        // setting mAddressRequested to true. Later, when GoogleApiClient connects,
        // launch the service to fetch the address. As far as the user is
        // concerned, pressing the Fetch Address button
        // immediately kicks off the process of getting the address.
        if (view == btnPickUp) {
            mDepartureRequested = true;
        } else if (view == btnDestination) {
            mDestinationRequested = true;
        }
    }

    private void fillAddress(final TextViewRegular lblAddress, final LatLng latLng) {
        if (NetworkUtility.isNetworkAvailable()) {
            ModelManager.getAddressByLatlng(getActivity(), latLng, new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    JSONObject jsonObject = (JSONObject) object;
                    try {
                        JSONArray jsonArray = jsonObject.getJSONArray("results");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            JSONObject firstResult = jsonArray.getJSONObject(0);
                            String address = firstResult.optString("formatted_address");
                            if (address.contains("\n")) {
                                address = address.replace("\n", ", ");
                            }
                            lblAddress.setText(address);
                            Log.e("FragmentTransport", "Address LatLng " + address.toString());

                            // Reset boolean var to void some bugs
                            if (lblAddress == tvPickUp) {
                                mDepartureRequested = false;
                                MapsUtil.moveCameraTo(mMap, latLng);

                                mFromMarker = addMarker(mFromMarker, latLng, R.drawable.ic_place_from, true);

                                fromLatLng = latLng;
                            } else if (lblAddress == tvAddressDestination) {
                                mDestinationRequested = false;

                                mToMarker = addMarker(mToMarker, latLng, R.drawable.ic_place_to, true);

                                toLatLng = latLng;
                            }


                            // Get nearby drivers
                            getNearbyDrivers();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError() {

                }
            });
        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }

    }

    private void initEnableLocation() {
        if (MapsUtil.locationIsEnable(self)) {
            initGetLocation();
            LocationService.start(self, LocationService.REQUEST_LOCATION);
        } else {
            MapsUtil.displayLocationSettingsRequest((AppCompatActivity) self, RC_TURN_ON_LOCATION);
        }
    }

    private void initGetLocation() {
        Log.e(TAG, "initGetLocation");
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(self);
        getLastLocation();
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener((Activity) self, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.

                        Log.e(TAG, "addOnSuccessListener onSuccess ");
                        if (location != null) {
                            Log.e("YYY", location + "");
                            mylatLng = new LatLng(location.getLatitude(), location.getLongitude());
                            tvPickUp.setText(MapsUtil.getAddressFromLatLng(self, mylatLng));
                            Log.e("location", "lat " + location.getLatitude() + " long " + location.getLongitude());

                            mMyLocMarker = addMarker(mMyLocMarker, mylatLng, R.drawable.ic_location_on_white, false);

                            MapsUtil.moveCameraTo(mMap, mylatLng);
                            movedToMyLocation = true;
                        } else {
                            getLastLocation();
                        }
                    }
                });
    }

    private void fillAddress(final TextViewRegular lblAddress, Place place) {
        LatLng latLng = place.getLatLng();
        String address = place.getAddress().toString();
        if (address.contains("\n")) {
            address = address.replace("\n", ", ");
        }
        lblAddress.setText(address);
        Log.e("FragmentTransport", "Address " + address.toString());

        // Reset boolean var to void some bugs
        if (lblAddress == tvPickUp) {
            mDepartureRequested = false;
            MapsUtil.moveCameraTo(mMap, latLng);

            //mFromMarker = addMarker(mFromMarker, latLng, R.drawable.ic_place_from, true);

            fromLatLng = latLng;

            movedToMyLocation = true;

        } else if (lblAddress == tvAddressDestination) {
            mDestinationRequested = false;

            mToMarker = addMarker(mToMarker, latLng, R.drawable.ic_place_to, true);

            toLatLng = latLng;
        }
        getNearbyDrivers();

    }

    private void fillAddress(final TextViewRegular lblAddress, Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        String address = location.getProvider();
        if (address.contains("\n")) {
            address = address.replace("\n", ", ");
        }
        lblAddress.setText(address);
        Log.e("FragmentTransport", "Address " + address.toString());

        // Reset boolean var to void some bugs
        if (lblAddress == tvPickUp) {
            mDepartureRequested = false;
            MapsUtil.moveCameraTo(mMap, latLng);

            mFromMarker = addMarker(mFromMarker, latLng, R.drawable.ic_place_from, true);

            fromLatLng = latLng;
        } else if (lblAddress == tvAddressDestination) {
            mDestinationRequested = false;

            mToMarker = addMarker(mToMarker, latLng, R.drawable.ic_place_to, true);

            toLatLng = latLng;
        }


        getNearbyDrivers();

    }

    @Override
    public View getInfoWindow(Marker marker) {
        @SuppressLint("RestrictedApi") ViewGroup window = (ViewGroup) getLayoutInflater(new Bundle()).inflate(R.layout.layout_windowinfo_marker_driver, null);

        TextViewRegular lblMinutes = (TextViewRegular) window.findViewById(R.id.lbl_minutes);
        TextViewRegular lblName = (TextViewRegular) window.findViewById(R.id.lbl_driver_name);
        TextViewRegular lblTransport = (TextViewRegular) window.findViewById(R.id.lbl_type);
        TextViewRegular lblRateQuantity = (TextViewRegular) window.findViewById(R.id.lbl_rate_quantity);
        TextViewRegular lblFare = (TextViewRegular) window.findViewById(R.id.lbl_fare);
        RatingBar ratingBar = (RatingBar) window.findViewById(R.id.rating);

        if (!marker.equals(myLocMarker) && !marker.equals(mToMarker) && !marker.equals(mFromMarker)) {
            TripObj tripObj = null;
            if (mNearbyDriverDeals != null) {
                if (mNearbyDriverDeals.size() > 0) {
                    for (int i = 0; i < mNearbyDriverDeals.size(); i++) {
                        tripObj = mNearbyDriverDeals.get(i);
                        if (marker.getTag().equals(tripObj.getDriverId())) {
                            break;
                        }
                    }
                }
            }

            if (tripObj != null) {
                String minutes = String.valueOf(tripObj.getDuration() / 60);
                lblMinutes.setText(String.format(getString(R.string.value_minutes_away), minutes));
                lblName.setText(tripObj.getDriverName());
                lblRateQuantity.setText(String.valueOf(tripObj.getRateQuantity()));
                ratingBar.setRating(tripObj.getRateOfDriver());

                if (tripObj.getEstimateFare() > 0) {
                    lblFare.setVisibility(View.VISIBLE);
                    lblFare.setText(String.format(getString(R.string.dollar_value), StringUtil.convertNumberToString(tripObj.getEstimateFare(), 1)));
                } else {
                    lblFare.setVisibility(View.INVISIBLE);
                }

                String transport = tripObj.getTransportType();
//                if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.REGULAR)) {
//                    if (tripObj.driverIsDelivery()) {
//                        transport = getString(R.string.taxi_delivery);
//                    } else {
//                        transport = getString(R.string.taxi);
//                    }
//                } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.AIR_CONDITIONED)) {
//                    if (tripObj.driverIsDelivery()) {
//                        transport = getString(R.string.vip_delivery);
//                    } else {
//                        transport = getString(R.string.vip);
//                    }
//                } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.SUV)) {
//                    if (tripObj.driverIsDelivery()) {
//                        transport = getString(R.string.lift_delivery);
//                    } else {
//                        transport = getString(R.string.lifts);
//                    }
//                } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.VIP1)) {
//                    if (tripObj.driverIsDelivery()) {
//                        transport = getString(R.string.motor_delivery);
//                    } else {
//                        transport = getString(R.string.motorbike);
//                    }
//                }
                lblTransport.setText(transport);
            }

            return window;
        } else {
            return null;
        }
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
//        if (!marker.equals(myLocMarker) && !marker.equals(mFromMarker) && !marker.equals(mToMarker)) {
//            mSelectedDriverId = marker.getTag().toString();
//
//        }
//        Log.e("a","AAAAA");
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Log.e("a", "BBBBB");
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        Log.e("a", "ccccc");
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
//        if (marker.equals(mFromMarker)) {
        fillAddress(tvPickUp, marker.getPosition());
//        } else if (marker.equals(mToMarker)) {
//            fillAddress(tvAddressDestination, marker.getPosition());
//        }
        Log.e("a", "dddd");
        tvPickUp.setText(MapsUtil.getAddressFromLatLng(self, marker.getPosition()));
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        initLocationListener();
        //check permission get location
//        if (CheckPermission.isGranted(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                RC_PERMISSIONS, "")) {
//            mMap.setMyLocationEnabled(true);
//        }

        //mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMarkerClickListener(this);


        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //get latlng at the center by calling
                //khi camera đã di chuyển về my location thì  movedToMyLocation = true
                if (movedToMyLocation) {
                    fromLatLng = mMap.getCameraPosition().target;

                    tvPickUp.setText(MapsUtil.getAddressFromLatLng(self, fromLatLng));
                    mDepartureRequested = false;

                    Log.e("mapListener", " onCameraIdle " + mMap.getCameraPosition().target.latitude
                            + " " + mMap.getCameraPosition().target.longitude);


                }

            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("TransportFragment", "onActivityResult");

        if (requestCode == RC_GET_DEPARTURE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.e(TAG, "Place: " + place.getName() + ", " + place.getId());

                fillAddress(tvPickUp, place);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }

        } else if (requestCode == RC_GET_DESTINATION) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                fillAddress(tvAddressDestination, place);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }

        } else if (requestCode == RC_TURN_ON_LOCATION) {
            if (MapsUtil.locationIsEnable(self)) {

                initGetLocation();

            } else {
                turnOnLocationReminder(RC_TURN_ON_LOCATION, true);
            }
        }
    }

    public void turnOnLocationReminder(final int reqCode, final boolean flag) {
        GlobalFunctions.showConfirmationDialog(self, getString(R.string.msg_remind_user_turn_on_location),
                getString(R.string.turn_on), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        MapsUtil.displayLocationSettingsRequest((AppCompatActivity) self, reqCode);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
//                            finish();
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RC_LOCATION: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (MapsUtil.locationIsEnable(self)) {
                            initGetLocation();

                        } else {
                            turnOnLocationReminder(RC_TURN_ON_LOCATION, true);
                        }
                    } else {
                        showPermissionsReminder(RC_LOCATION, true);
                    }
                }
                break;
            }

            //request permission location
            case RC_PERMISSIONS: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                        showPermissionsReminder(RC_PERMISSIONS, true);
                    } else {
                        //permission location đã được cấp phép
                        mMap.setMyLocationEnabled(true);
                        sendLocationToServer();
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    protected void showPermissionsReminder(final int reqCode, final boolean flag) {
        GlobalFunctions.showConfirmationDialog(self, getString(R.string.msg_remind_user_grants_permissions),
                getString(R.string.allow), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        GlobalFunctions.isGranted(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.ACCESS_FINE_LOCATION}, reqCode, null);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
//                            finish();
                        }
                    }
                });
    }

    private void initLocationListener() {
        if (CheckPermission.isGranted((Activity) self,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                RC_PERMISSIONS,
                "")) {


            sendLocationToServer();
        }
    }

    @SuppressLint("MissingPermission")
    private void sendLocationToServer() {
        mLocationManager = (LocationManager) self.getSystemService(LOCATION_SERVICE);


        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                0, mLocationListener);
    }


}
