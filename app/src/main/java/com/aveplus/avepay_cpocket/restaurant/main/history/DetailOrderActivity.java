package com.aveplus.avepay_cpocket.restaurant.main.history;

import android.content.Intent;
import android.util.Log;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeDetail;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailOrderActivity extends BaseActivity {

    public static final int REQUEST_CODE = 100;
    private static final String TAG = DetailOrderActivity.class.getSimpleName();
    private TextView tvName, tvBill_id, tvStatus, tvDate, tvPhone, tvAllAddress, tvPayMethod, tvPay, tvTransportFee;
    private RecyclerView rvDetail;
    private int order_id;
    private DetailOrderAdapter adapter;
    private ArrayList<Detail> details;
    private ArrayList<DetailOrder> orderArrayList;
    private int id_order;
    private int status;

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_order_details;
    }

    @Override
    protected void initView() {
        setToolbarTitle("Detail Order");

        tvBill_id = findViewById(R.id.tvIdBill);
        tvName = findViewById(R.id.tvName);
        tvDate = findViewById(R.id.tvDateTime);
        tvAllAddress = findViewById(R.id.tvAllAddress);
        tvStatus = findViewById(R.id.tvStatus);
        tvPhone = findViewById(R.id.tvPhone);
        tvPayMethod = findViewById(R.id.tvPayMethod);
        tvPay = findViewById(R.id.tvPay);
        tvTransportFee = findViewById(R.id.tvTransportFee);
        rvDetail = findViewById(R.id.rv_Detail);
        orderArrayList = new ArrayList<>();
        details = new ArrayList<>();
        setData();
        getData();


    }


    private void getData() {
        APIService apiService = ApiUtils.getAPIService();
        apiService.DetailOrder(DataStoreManager.getToken(), 0).enqueue(new Callback<ResponeDetail>() {
            @Override
            public void onResponse(Call<ResponeDetail> call, Response<ResponeDetail> response) {
                if (response.isSuccessful()) {
                    orderArrayList.addAll(response.body().getData());
                    for (int i = 0; i < orderArrayList.size(); i++) {
                        details.addAll(response.body().getData().get(i).getOrderDetail());
                    }
                    adapter = new DetailOrderAdapter(details, self);
                    rvDetail.setAdapter(adapter);
                    rvDetail.hasFixedSize();

                } else {
                    Log.e(TAG, "onResponse: False");
                }
            }

            @Override
            public void onFailure(Call<ResponeDetail> call, Throwable t) {
                AppUtil.showToast(self, t.getMessage());
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    private void setData() {
        Intent intent = getIntent();
        id_order = intent.getIntExtra("id", 0);
        status = intent.getIntExtra("status", 0);
        if (status == 0) {
            tvStatus.setText("Processing");
        } else if (status == 1) {
            tvStatus.setText("Approved");
        }

        tvBill_id.setText(String.valueOf(id_order));
        tvName.setText(intent.getStringExtra("name"));
        tvAllAddress.setText(intent.getStringExtra("address"));
        tvDate.setText(intent.getStringExtra("date"));
        tvPay.setText(intent.getStringExtra("total"));
        tvPhone.setText(intent.getStringExtra("phone"));
        tvTransportFee.setText(String.valueOf(intent.getStringExtra("transportFee")));
        tvPayMethod.setText(intent.getStringExtra("payment"));
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {

    }
}
