package com.aveplus.avepay_cpocket.movie.movie.modelmanager;

public interface ModelManagerListener {

    void onSuccess(Object object);

    void onError();

}
