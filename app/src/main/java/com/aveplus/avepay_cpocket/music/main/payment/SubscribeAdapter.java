package com.aveplus.avepay_cpocket.music.main.payment;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.NonNull;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseAdapterBinding;

import java.util.List;

public class SubscribeAdapter extends BaseAdapterBinding {
    private List<PackageSubscribe> listData;

    public SubscribeAdapter(Context context, List<?> datas) {
        super(context);
        this.listData = (List<PackageSubscribe>) datas;
    }

    @Override
    public void addDatas(List<?> data) {
        int positionStart = listData.size();
        notifyItemRangeInserted(positionStart, data.size());
    }

    @Override
    public void setDatas(List<?> data) {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(getViewBinding(parent, R.layout.item_subscribe));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PackageSubscribe item = listData.get(position);
        if (item != null) {
            holder.bind(new ItemSubscribeVM(context, item));

        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}
