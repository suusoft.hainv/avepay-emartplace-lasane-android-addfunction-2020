package com.aveplus.avepay_cpocket.restaurant.configs;

/**
 * Created by Trang on 6/14/2016.
 */
public class Constant {

    private static final String USER = "user/";
    private static final String DEAL = "deal/";

    public static final String NOTIFICATION = "NOTIFICATION";
    public static final String LOGIN_TOKEN = "login_token";

    public static final String PREF_KEY_ID = "PREF_KEY_ID";
    public static final String PREF_KEY_OBJECT = "PREF_KEY_OBJECT";
    public static final String PREF_KEY_POSITION = "PREF_KEY_POSITION";
    public static final String PREF_KEY_DATA = "PREF_KEY_DATA";
    public static final String PREF_KEY_DATA_OBJECT = "PREF_KEY_DATA_OBJECT";
    public static final String PREF_KEY_DATA_OBJECT_LIST = "PREF_KEY_DATA_OBJECT_LIST";
    public static final String PREF_KEY_DATA_PLAYLIST_ID = "PREF_KEY_DATA_PLAYLIST_ID";

    public static final String PREF_KEY_DATA_LIST = "PREF_KEY_DATA_LIST";

    public static final String PREF_KEY_ITEM_POSITION = "PREF_KEY_ITEM_POSITION";
    public static final String KEY_TOTAL_PAGE = "total_page";

    // Code pick image
    public static final int RQ_CODE_PICK_IMG = 1001;

    // broast cast for player
    public static final String BRC_CAN_PLAY = "BRC_CAN_PLAY";
    public static final String BRC_ACTION_PLAYER = "BRC_ACTION_PLAYER";
    public static final String BRC_NEED_TO_UPDATE_DATA = "BRC_NEED_TO_UPDATE_DATA";
    public static final String ACTION = "ACTION";

    public class Caching{
        public static final String KEY_REQUEST = "request";
        public static final String KEY_RESPONSE = "response";
        public static final String KEY_TIME_UPDATED = "time_updated";
        public static final String CACHING_PARAMS_TIME_REQUEST = "caching_time_request";

    }

    public static final int ID_NOTIFICATION = 290292;

    public class Player{
        public static final int PLAY = 2000;
        public static final int NEXT = 2001;
        public static final int PREVIOUS = 2002;
        public static final int PAUSE = 2003;
        public static final int RESUME = 2004;
        public static final int STOP = 2005;
        public static final int CANCEL_NOTIFICATION = 2006;
        public static final String ACTION = "ACTION";
        public static final int PREPARE = 2007;
    }


}
