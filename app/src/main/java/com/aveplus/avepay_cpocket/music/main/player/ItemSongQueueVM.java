package com.aveplus.avepay_cpocket.music.main.player;

import android.content.Context;
import android.view.View;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.main.home.ItemSongsVM;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ItemSongQueueVM extends ItemSongsVM {

    public ItemSongQueueVM(Context self, Object song, int position) {
        super(self, song, position);
    }

    public void onClickAction(View view){
        showPopupMenu(view, R.menu.menu_action_song_queue);
    }

}
