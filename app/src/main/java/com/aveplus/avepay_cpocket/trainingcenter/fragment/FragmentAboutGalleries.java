package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.GalleriesAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.Galleries;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class FragmentAboutGalleries extends BaseFragment implements
        OnItemClickListener {

    private GalleriesAdapter galleriesadapter;
    ArrayList<Galleries> listgalleries;
    private GridView gridGallery;
    Context context;

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("about");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("about");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.frm_about_galleries, container, false);
        initView(view);
        initData();
        return view;
    }


    private void initData() {
        ModelManager.getListAbout(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                listgalleries = ParseUtility.parseListGalleries(response.getRootStr());
                galleriesadapter = new GalleriesAdapter(getActivity(),
                        listgalleries);
                gridGallery.setAdapter(galleriesadapter);
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });

    }


    @SuppressLint("NewApi")
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.image_dialog);
        ImageView imageDialog = (ImageView) dialog.findViewById(R.id.imgDialog);
        Galleries img = listgalleries.get(position);
        String image = img.getUrl();
        Picasso.get().load(image).into(imageDialog);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, 500);
        imageDialog.setLayoutParams(layoutParams);

        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.show();
    }

    private void initView(View view) {
        gridGallery = (GridView) view.findViewById(R.id.gridGallery);
        gridGallery.setOnItemClickListener(this);
    }
}
