package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseListFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.SingleAdapter;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.ListChapterVM;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemChapterBookmarkVM;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemChapterDownloadVM;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class FragmentListChapterBookmark extends BaseListFragmentBinding {


    private ListChapterVM viewModel;
    private static boolean mLoadMore;
    public boolean isDownload, isNeedRefresh;

    public static FragmentListChapterBookmark newInstance(boolean loadMore) {
        mLoadMore = loadMore;
        Bundle args = new Bundle();
        FragmentListChapterBookmark fragment = new FragmentListChapterBookmark();
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentListChapterBookmark newInstance(boolean loadMore, boolean isDownload) {
        mLoadMore = loadMore;
        Bundle args = new Bundle();
        FragmentListChapterBookmark fragment = new FragmentListChapterBookmark();
        fragment.setArguments(args);
        fragment.isDownload = isDownload;
        return fragment;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(self));
        SingleAdapter adapter;
        if (isDownload)
            adapter = new SingleAdapter(self, R.layout.item_chapter_download, viewModel.getListData(), ItemChapterDownloadVM.class);
        else
            adapter = new SingleAdapter(self, R.layout.item_chapter_bookmark, viewModel.getListData(), ItemChapterBookmarkVM.class);

        recyclerView.setAdapter(adapter);
    }

    public void setUpListOrGrid(int type) {

    }


    @Override
    protected void initialize() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (isNeedRefresh){
//            viewModel.onRefresh();
//            isNeedRefresh = false;
//        }
        Log.e("data", "onResume");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Config.RefreshListChapter event) {
        Log.e("data", "onRefresh");
        //isNeedRefresh = true;
        viewModel.onRefresh();
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new ListChapterVM(self, getArguments());
        return viewModel;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 69 && resultCode == Activity.RESULT_OK) {
            // getmAdapter().removeAll();
            viewModel.onRefresh();
        }
    }
}
