package com.aveplus.avepay_cpocket.movie.movie.network;

/**
 * Copyright © 2019 SUUSOFT
 */

public interface ListenerLoading extends IBaseListener {
    void onLoadingIsProcessing();
    void onLoadingIsCompleted();
}
