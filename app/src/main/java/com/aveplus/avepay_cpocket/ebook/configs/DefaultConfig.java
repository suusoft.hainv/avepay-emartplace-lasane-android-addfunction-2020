package com.aveplus.avepay_cpocket.ebook.configs;


import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.ebook.model.ListType;

public class DefaultConfig {

    // List type default
    public static ListType listType = new ListType(Config.TYPE_GRID, Config.TYPE_GRID_NAME);

    // Background defaul
    public static String backgroundDefault = Config.TYPE_BACKGROUND_LIGHT;
}
