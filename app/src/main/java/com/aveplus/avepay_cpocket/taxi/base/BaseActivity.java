package com.aveplus.avepay_cpocket.taxi.base;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.interfaces.IConfirmation;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.book.ServiceObj;
import com.aveplus.avepay_cpocket.taxi.retrofit.APIService;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;


/**
 * Created by SuuSoft.com on 6/17/2016.
 */
public abstract class BaseActivity extends AbstractActivity {

    protected abstract ToolbarType getToolbarType();

    protected abstract int getLayoutInflate();

    protected abstract void getExtraData(Intent intent);

    protected abstract void initilize();

    protected abstract void initView();

    protected abstract void onViewCreated();

    protected APIService mAPIService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(getLayoutInflate(), contentLayout);
        initApiService();
        initilize();
        initView();
        onViewCreated();
    }

    private void initApiService() {
        mAPIService = ApiUtils.getAPIService();
    }

    public void showPermissionsReminder(final int reqCode, final boolean flag) {
        GlobalFunctions.showConfirmationDialog(self, getString(R.string.msg_remind_user_grants_permissions),
                getString(R.string.allow), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        GlobalFunctions.isGranted(BaseActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.ACCESS_FINE_LOCATION}, reqCode, null);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            finish();
                        }
                    }
                });
    }

    public void turnOnLocationReminder(final int reqCode, final boolean flag) {
        GlobalFunctions.showConfirmationDialog(self, getString(R.string.msg_remind_user_turn_on_location),
                getString(R.string.turn_on), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        MapsUtil.displayLocationSettingsRequest(BaseActivity.this, reqCode);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            finish();
                        }
                    }
                });
    }

    protected int getTransportIcon(TripObj tripObj) {
        int icon = R.drawable.ic_taxi_type;
        if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.TAXI)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_taxi_delivery_type;
            } else {
                icon = R.drawable.ic_taxi_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.VIP)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_vip_delivery_type;
            } else {
                icon = R.drawable.ic_vip_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.LIFTS)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_lifts_delivery_type;
            } else {
                icon = R.drawable.ic_lifts_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.MOTORBIKE)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_moto_delivery_type;
            } else {
                icon = R.drawable.ic_moto_type;
            }
        }

        return icon;
    }


}
