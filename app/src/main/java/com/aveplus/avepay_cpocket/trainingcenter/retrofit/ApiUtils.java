package com.aveplus.avepay_cpocket.trainingcenter.retrofit;


import com.aveplus.avepay_cpocket.configs.Config;

public class ApiUtils {
    public static final String BASE_URL = Config.URL_API_TRAINING_CENTER;

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
