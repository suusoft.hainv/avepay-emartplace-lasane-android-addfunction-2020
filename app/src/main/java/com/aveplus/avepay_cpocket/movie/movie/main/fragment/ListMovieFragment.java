package com.aveplus.avepay_cpocket.movie.movie.main.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;


import androidx.fragment.app.Fragment;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.adapter.GridMovieAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.detail.MovieDetailActivity;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onPageChangeAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.MoviePlayerActivity;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieGenre;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.MovieOfPageByGenreCallback;
import com.aveplus.avepay_cpocket.utils.Utils;

import java.util.ArrayList;
import java.util.List;

//import com.facebook.internal.LockOnGetVariable;


/**
 * Copyright © 2019 SUUSOFT
 */
public class ListMovieFragment extends Fragment implements onPageChangeAdapter {
    private final String TAG = getClass().getSimpleName();
    private static final String CATEGORY_POS = "CategoryPosition";
    private static final String CATEGORY_OBJECT = "CategoryObject";
    //params
    private int mCategoryPosition;

    private GridView mGrvMovie;
    private GridMovieAdapter movieAdapter;
    private MoviesManagement moviesManagement;
    private MovieGenre mCurrentGenre;
    //    private List<Movie> mMovies;
    private List<Movie> mMovies;
    private int mMoviePosition;
    private Movie mCurrentMovie;
    private int mSubcategoryPosition;
    // get movie by page number
    private int totalPages = 0;
    private int totalResults = 0;
    private int currentPageNumber = 1;

    private Context context;

    public static ListMovieFragment newInstance(int positionCategory, MovieGenre movieGenre) {
        ListMovieFragment subcateFragment = new ListMovieFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(CATEGORY_POS, positionCategory);
        bundle.putParcelable(CATEGORY_OBJECT, movieGenre);
        subcateFragment.setArguments(bundle);
        return subcateFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        moviesManagement = MoviesManagement.getInstance();
        mCategoryPosition = getArguments().getInt(CATEGORY_POS);
        mCurrentGenre = getArguments().getParcelable(CATEGORY_OBJECT);
        loadingData();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gridview_movie, container, false);
        mGrvMovie = (GridView) v.findViewById(R.id.gridView);
        return v;
    }

    private void setAdapter() {
        if (context != null) {
            movieAdapter = new GridMovieAdapter(
                    context, R.layout.adapter_grid_move, mMovies, mGrvMovie, this);
            mGrvMovie.setAdapter(movieAdapter);
            mGrvMovie.setOnItemClickListener(mItemClickListener);
        }

    }

    private AbsListView.OnScrollListener scrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {

        }

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            int position = firstVisibleItem + visibleItemCount;
            Log.e(TAG, "onScroll: " + "Vào rồi");
            if (movieAdapter.getCount() >= totalResults)
                return;
            if (position >= totalItemCount) {
                if (currentPageNumber < totalPages) {
                    currentPageNumber++;
                    Log.e(TAG, "onScroll: " + currentPageNumber);
                    updateData(currentPageNumber);
                }
            }

        }
    };

    // loading data
    private void loadingData() {
        if (Utils.isNetworkConnected(context)) {
            MoviesManagement.getInstance().moviesOfPageByGenre1(
                    context, mCurrentGenre.getId() + "",
                    currentPageNumber,
                    new MovieOfPageByGenreCallback() {

                        @Override
                        public void onFailed(Exception exception) {
                        }

                        @Override
                        public void onCompleted(SparseArray<Object> object) {

                            if (object != null && object.size() >= 2) {
                                if (mMovies != null)
                                    mMovies.clear();
                                else
                                    mMovies = new ArrayList<Movie>();
                                mMovies.addAll((ArrayList<Movie>) object.get(1));
                                totalPages = (Integer) object.get(2);
                                Log.e(TAG, "TOTALPAGE: " + totalPages);
                                totalResults = (Integer) object.get(3);
                                Log.e(TAG, "TOTALRESULTS" + totalResults);
                                setAdapter();
                                mGrvMovie.setOnScrollListener(scrollListener);

                            }
                        }
                    });

        } else {
            Toast.makeText(context, "No connection network",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void updateData(int currentPage) {
        if (Utils.isNetworkConnected(getActivity())) {
            MoviesManagement.getInstance().moviesOfPageByGenre1(
                    context, mCurrentGenre.getId() + "",
                    currentPage,
                    new MovieOfPageByGenreCallback() {

                        @Override
                        public void onFailed(Exception exception) {
                            Log.i(TAG, "updateData : onFailed");
                        }

                        @SuppressWarnings("unchecked")
                        @Override
                        public void onCompleted(SparseArray<Object> object) {
                            Log.i(TAG, "updateData : onCompleted");
                            if (object != null && object.size() >= 2) {
                                mMovies.addAll((ArrayList<Movie>) object.get(1));
                                //moviesManagement.getmMovies().addAll(mMovies);
                            }
                            movieAdapter.notifyDataSetChanged();
                        }
                    });
        } else {
            Toast.makeText(context, "no connection network",
                    Toast.LENGTH_SHORT).show();
        }

    }


    /**
     *
     */
    // gỡ cmt ra sau

    private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.e(TAG, "onItemClick: " + position);
            moviesManagement.setmMovies(mMovies);
            didSelectMovieAtPosition(position);
        }
    };


    private void didSelectMovieAtPosition(int position) {
        mCurrentMovie = mMovies.get(position);
        // Intent movieDetailIntent = new Intent(context, MovieDetailActivity.class);
//        DialogUtil.showAlertDialogMxPlayer(context, R.string.do_you_want_play_video_by_mx_player, new DialogUtil.IDialogConfirm1() {
//            @Override
//            public void onClickOk() {
//                if (isAppExist()) {
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    Uri videoUri = Uri.parse(mCurrentMovie.getMovie_video());
//                    intent.setDataAndType(videoUri, "application/x-mpegURL");
//                    intent.setPackage("com.mxtech.videoplayer.ad");
//                    startActivity(intent);
//                } else {
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.mxtech.videoplayer.ad")));
//                }
//            }
//
//            @Override
//            public void onClickCancel() {
        Intent movieDetailIntent = new Intent(context, MovieDetailActivity.class);
        // Set data for bundle
        Bundle bundle = new Bundle();
        bundle.putInt(MoviePlayerActivity.CATEGORY_EXTRA, position);//mCategoryPosition
        bundle.putInt(MoviePlayerActivity.MOVIE_EXTRA, position);
        bundle.putInt(MoviePlayerActivity.SUBCATEGORY_EXTRA, mSubcategoryPosition);
//        bundle.putInt(MoviePlayerActivity.MOVIE_EXTRA, mMoviePosition);

        bundle.putInt(MovieDetailActivity.CATEGORY_EXTRA, mCategoryPosition);

        bundle.putInt(MovieDetailActivity.MOVIE_EXTRA_POSITION, position);
        bundle.putParcelable(MovieDetailActivity.MOVIE_EXTRA_OBJECT, mCurrentMovie);

        bundle.putInt(MoviePlayerActivity.SUBCATEGORY_EXTRA, mSubcategoryPosition);
        // bundle.putInt(MoviePlayerActivity.MOVIE_EXTRA, mMoviePosition);
        bundle.putParcelable(MoviePlayerActivity.EXTRA_MOVIE, mCurrentMovie);
        movieDetailIntent.putExtras(bundle);
        startActivity(movieDetailIntent);
//            }
//        });


        //  AdsUtil.showInterstitialAdmob();
    }

    @Override
    public void onClickItemSubcate(int subcatePosition) {

    }

    @Override
    public void onNextPage(int position) {
    }

    @Override
    public void onPrevPage(int position) {

    }


}
