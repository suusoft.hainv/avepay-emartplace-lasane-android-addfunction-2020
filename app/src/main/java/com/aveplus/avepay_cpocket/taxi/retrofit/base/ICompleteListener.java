package com.aveplus.avepay_cpocket.taxi.retrofit.base;


import retrofit2.Call;
import retrofit2.Response;

public interface ICompleteListener {
    void onResponse(Call<Object> call, Response<Object> response);
    void onFailure(Call<Object> call, Throwable t);
}
