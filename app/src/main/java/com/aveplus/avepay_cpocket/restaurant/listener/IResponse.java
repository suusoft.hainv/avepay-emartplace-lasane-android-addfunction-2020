package com.aveplus.avepay_cpocket.restaurant.listener;


/**
 * Created by Suusoft on 09/04/2015.
 */
public interface IResponse {
    void onResponse(Object response);
}
