package com.aveplus.avepay_cpocket.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.view.activities.DealDetailActivity;
import com.aveplus.avepay_cpocket.view.adapters.DealAdapter;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Suusoft on 11/01/2017.
 */

public class HomeFragment extends com.aveplus.avepay_cpocket.base.BaseFragment implements ViewPager.OnPageChangeListener {

    private static final String TAG = HomeFragment.class.getSimpleName();
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private MainActivity activity;
    private PagerAdapter adapter;


    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_home;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        activity = (com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity) self;
        adapter = new PagerAdapter(getChildFragmentManager());
        adapter.addFragment(FragmentFavorite.newInstance());
        adapter.addFragment(AllDealsFragment.newInstance(Args.TYPE_OF_CATEGORY_ALL));
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        indicator = (CircleIndicator) view.findViewById(R.id.circle_indicator);

        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(this);
        indicator.setViewPager(viewPager);

    }

    private void setIndicatorAnimation(){
        Animation animFade = AnimationUtils.loadAnimation(self, R.anim.slide_fade_out);
        indicator.setAnimation(animFade);
    }

    @Override
    protected void getData() {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position){
            case 0:
                setIndicatorAnimation();
                activity.setTitle(R.string.favorite);
                break;

            case 1:
                setIndicatorAnimation();
                activity.setTitle(R.string.category);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class PagerAdapter extends FragmentPagerAdapter {
        private ArrayList<Fragment> listFragments;
        private ArrayList<String> listTabs;

        public PagerAdapter(FragmentManager fm) {
            super(fm);
            listFragments = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position) {
            if (position==0)
                return FragmentFavorite.newInstance();
            else if (position==1)
                return AllDealsFragment.newInstance(Args.TYPE_OF_CATEGORY_ALL);
            else return null;

        }

        @Override
        public int getCount() {
            return listFragments.size();
        }

        public void addFragment(Fragment fragment) {
            listFragments.add(fragment);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DealAdapter.RQ_UPDATE_DEAL) {
            Log.e(TAG, "requestCode == RQ_UPDATE_DEAL");
            if (resultCode == DealDetailActivity.RC_UPDATE_DEAL) {
                Log.e(TAG, "resultCode == DealDetailActivity.RC_UPDATE_DEAL");


            }
        }
    }
}
