package com.aveplus.avepay_cpocket.trainingcenter.object;

public class ProductHistory {
	
	private String nameProductHistory;
	private String quantity;
	
	public ProductHistory() {
		super();
	}

	public ProductHistory(String nameProductHistory, String quantity) {
		super();
		this.nameProductHistory = nameProductHistory;
		this.quantity = quantity;
	}

	public String getNameProductHistory() {
		return nameProductHistory;
	}

	public void setNameProductHistory(String nameProductHistory) {
		this.nameProductHistory = nameProductHistory;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}
