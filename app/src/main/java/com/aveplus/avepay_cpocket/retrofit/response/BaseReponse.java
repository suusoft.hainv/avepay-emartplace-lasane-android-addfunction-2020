package com.aveplus.avepay_cpocket.retrofit.response;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.quickblox.conversation.utils.DialogUtil;
import com.aveplus.avepay_cpocket.view.activities.LoginActivity;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BaseReponse {
    public String status;
    public int code;
    public String total_page;
    public String message;
    @SerializedName("token")
    @Expose
    public String login_token;


    public String toJson() {
        return new Gson().toJson(this);
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess(final Context context) {
        if (getCode() == 200 || getCode() == 202) {
            return true;
        } else if (getCode() == 205) {
            DialogUtil.showAlertDialog(context, R.string.error_token, new DialogUtil.IDialogConfirm() {
                @Override
                public void onClickOk() {
                    DataStoreManager.removeUser();
                    context.startActivity(new Intent(context, LoginActivity.class));
                }
            });
        } else if (getCode() == 134) {
            Toast.makeText(context, "Your balance not enough. Please choose another payment method!", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("BaseModel", "isSuccess: ");
            Toast.makeText(context, getMessage(), Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public String getTotal_page() {
        return total_page;


    }

    public String getLogin_token() {
        return login_token;
    }

    public void setLogin_token(String login_token) {
        this.login_token = login_token;
    }

    public void setTotal_page(String total_page) {
        this.total_page = total_page;
    }

    public boolean isEnded(int curPage) {
        int totalPage = total_page.equals("") ? 0 : Integer.parseInt(total_page);
        return totalPage == 0 || curPage >= totalPage;
    }
}
