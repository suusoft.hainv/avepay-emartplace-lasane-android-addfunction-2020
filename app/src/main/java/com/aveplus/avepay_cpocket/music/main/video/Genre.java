package com.aveplus.avepay_cpocket.music.main.video;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.music.base.model.BaseModel;


public class Genre extends BaseModel implements Parcelable {
    private int id,parent_id,is_active;
    private String name,img_thumb,img_banner,description,content;

    public Genre() {
    }

    public Genre(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Genre(int id, int parent_id, int is_active, String name, String img_thumb, String img_banner, String description, String content) {
        this.id = id;
        this.parent_id = parent_id;
        this.is_active = is_active;
        this.name = name;
        this.img_thumb = img_thumb;
        this.img_banner = img_banner;
        this.description = description;
        this.content = content;
    }

    protected Genre(Parcel in) {
        id = in.readInt();
        parent_id = in.readInt();
        is_active = in.readInt();
        name = in.readString();
        img_thumb = in.readString();
        img_banner = in.readString();
        description = in.readString();
        content = in.readString();
    }

    public static final Creator<Genre> CREATOR = new Creator<Genre>() {
        @Override
        public Genre createFromParcel(Parcel in) {
            return new Genre(in);
        }

        @Override
        public Genre[] newArray(int size) {
            return new Genre[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_thumb() {
        return img_thumb;
    }

    public void setImg_thumb(String img_thumb) {
        this.img_thumb = img_thumb;
    }

    public String getImg_banner() {
        return img_banner;
    }

    public void setImg_banner(String img_banner) {
        this.img_banner = img_banner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(parent_id);
        parcel.writeInt(is_active);
        parcel.writeString(name);
        parcel.writeString(img_thumb);
        parcel.writeString(img_banner);
        parcel.writeString(description);
        parcel.writeString(content);
    }
}
