package com.aveplus.avepay_cpocket.movie.movie.main.movies;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.aveplus.avepay_cpocket.movie.movie.configs.WebService;
import com.aveplus.avepay_cpocket.movie.movie.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.movie.movie.listener.MovieIdYoutubeCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.CallBackMoviesByLanguage;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.CallBackNewMovies;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.CallBackNewestMovie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Language;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieGenre;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.ExtractCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.MovieGenreCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.MovieOfPageByGenreCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.MoviesByGenreCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.SearchMoviesCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.UpdateGenreCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.onUpdateBackdropMovie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.onUpdateMovieDetail;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.model.MovieParser;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.model.YTParser;
import com.aveplus.avepay_cpocket.utils.toolbox.CustomStringRequest;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Observable;

public class MoviesManagement extends Observable {
    private static final String TAG = "MovieManagement";
    private final String API_KEY = "30715d01cfefabddf07afcb88d428b78";
    private final String URL_BACKDROP = "https://image.tmdb.org/t/p/original";
    // declare variables
    private static MoviesManagement mManager;
    private List<MovieGenre> mMovieGenres;
    private List<Movie> mMovies;

    private LinkedHashMap<Language, List<Movie>> mLangMovies;
    private List<Language> mLanguages;
    private UpdateGenreCallback onUpdateGenreCallback;

    // declare volley request
    private static RequestQueue movieQueue;

    private String urlBackDrop;

    public static MoviesManagement getInstance() {
        if (mManager == null) {
            synchronized (MoviesManagement.class) {
                if (mManager == null) {
                    mManager = new MoviesManagement();
                }
            }
            movieQueue = RequestManager.getRequestQueue();
        }
        return mManager;
    }

    public static MoviesManagement getInstance(RequestQueue requestQueue) {
        if (mManager == null) {
            mManager = new MoviesManagement(requestQueue);
        }
        return mManager;
    }

    public static void release() {
        if (mManager != null)
            mManager = null;
    }

    public String getUrlBackDrop() {
        return urlBackDrop;
    }

    public void setUrlBackDrop(String urlBackDrop) {
        this.urlBackDrop = urlBackDrop;
    }

    public List<Language> getmLanguages() {
        return mLanguages;
    }

    public void setmLanguages(List<Language> mLanguages) {
        this.mLanguages = mLanguages;
    }

    public HashMap<Language, List<Movie>> getmLangMovies() {
        return mLangMovies;
    }

    public void setmLangMovies(LinkedHashMap<Language, List<Movie>> mLangMovies) {
        this.mLangMovies = mLangMovies;
    }

    protected MoviesManagement(RequestQueue requestQueue) {
        this.movieQueue = requestQueue;
    }

    protected MoviesManagement() {
    }

    public void getNewestMovie(final CallBackNewestMovie callBackNewestMovie) {
        JsonArrayRequest request = new JsonArrayRequest(WebService.URL_NEWEST_MOVIE,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            Movie movie = MovieParser.parserNewestMovie(response);
                            callBackNewestMovie.success(movie);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        });
        movieQueue.add(request);
    }


    public List<MovieGenre> getmMovieGenres() {
        if (mMovieGenres == null)
            mMovieGenres = new ArrayList<>();
        return mMovieGenres;
    }

    public void setmMovieGenres(List<MovieGenre> mMovieGenres) {
        this.mMovieGenres = mMovieGenres;
    }

    public List<Movie> getmMovies() {
        if (mMovies == null)
            mMovies = new ArrayList<>();
        return mMovies;
    }

    public void setmMovies(List<Movie> mMovies) {
        this.mMovies = mMovies;
    }

    /*
     * LOAD ALL MOVIE GENRES
     *
     * @param UpdateGenreCallback updateGenreCallback
     */
    public void loadMovieGenres(UpdateGenreCallback updateGenreCallback) {
        this.onUpdateGenreCallback = updateGenreCallback;
        updateMovieGenres();
    }

    /*
     * UPDATE MOVIE GENRES
     *
     * @param null
     */
    public void updateMovieGenres() {
        MoviesManagement.getInstance().movieGenresRequest(
                new MovieGenreCallback() {

                    @Override
                    public void onFailed(Exception e) {
                        if (onUpdateGenreCallback != null)
                            onUpdateGenreCallback.onCompleted(false);
                    }

                    @Override
                    public void onCompleted(List<MovieGenre> genres) {
                        setGenres(genres);
                    }
                });
    }

    /**
     * @author: Trang pham
     * MOVIE GENRES REQUEST WHEN RELOAD FRAGMENT
     */
    public void setGenres(List<MovieGenre> genres) {
        if (mMovieGenres == null)
            mMovieGenres = new ArrayList<MovieGenre>();
        else
            mMovieGenres.clear();

        mMovieGenres.addAll(genres);

        if (!mMovieGenres.isEmpty()) {
            if (onUpdateGenreCallback != null)
                onUpdateGenreCallback.onCompleted(true);
        }
    }

    /*
     * MOVIE GENRES REQUEST
     *
     * @param MovieGenreCallback mcb
     */
    public void movieGenresRequest(final MovieGenreCallback mcb) {
        JsonObjectRequest request = new JsonObjectRequest(Method.GET,
                WebService.URL_GENRES, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject json) {
                mcb.onCompleted(MovieParser.getMovieGenres(json));
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mcb.onFailed(error);
            }
        }) {

        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(RequestManager.VOLLEY_TAG);
        movieQueue.add(request);
    }

    /*
     * GET URL FOR GET ALL MOVIES OF GENRES BY GENRE_ID
     *
     * @param String genrfe_id
     */
    public static String getMoviesByGenreURL(String genre_id) {
        return String.format(WebService.URL_MOVIES, genre_id);
    }

    /*
     * GET MOVIES OF GENRE REQUEST
     *
     * @param String genre_id
     *
     * @param MoviesByGenreCallback mcb
     */


    /* GET URL PAGE OF MOVIE BY GENRE */
    public String getPageMoviesByGenreURL1(String genre_id) {
        String urlnew = WebService.URL_MOVIES_NEW1 + DataStoreManager.getToken() + "&action=" + "&genre=" + genre_id;

        Log.e(TAG, "getPageMoviesByGenreURL " + urlnew);
        return urlnew;
    }
    //

    public String getPageMoviesByGenreURL(String genre_id) {
        String urlnew = WebService.URL_MOVIES_NEW + DataStoreManager.getToken() + "&action=" + "&genre=" + genre_id;

        Log.e(TAG, "getPageMoviesByGenreURL " + urlnew);
        return urlnew;
    }

    /* GET MOVIES OF PAGE NUMBER BY GENRE */

    public void moviesOfPageByGenre1(final Context context, String genre_id,
                                     int pageNumber, final MovieOfPageByGenreCallback mcb) {
        String url = getPageMoviesByGenreURL1(genre_id) + "&page=" + pageNumber;
        Log.e(TAG, "moviesOfPageByGenre: " + url);
        JsonObjectRequest request = new JsonObjectRequest(Method.GET, url
                , null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject json) {
                        mcb.onCompleted(MovieParser.getMoviesByGenre(context,
                                json));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mcb.onFailed(error);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        request.setTag(RequestManager.VOLLEY_TAG);
        movieQueue.add(request);
    }
//

    public void moviesOfPageByGenre(final Context context, String genre_id,
                                    int pageNumber, final MovieOfPageByGenreCallback mcb) {
        String url = getPageMoviesByGenreURL(genre_id) + "&page=" + pageNumber;
        Log.e(TAG, "moviesOfPageByGenre: " + url);
        JsonObjectRequest request = new JsonObjectRequest(Method.GET, url
                , null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject json) {
                        mcb.onCompleted(MovieParser.getMoviesByGenre(context,
                                json));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mcb.onFailed(error);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(RequestManager.VOLLEY_TAG);
        movieQueue.add(request);
    }

    /* MOVIES BY GENRE REQUEST - UPDATE BY PAGE NUMBER */

    /*
     * GET URL FOR SEARCH MOVIES BY KEY
     *
     * @param String key
     */

    public static String getSearchMoviesURL(String key) {
        Log.e(TAG, "getSearchMoviesURL: " + String.format(WebService.URL_MOVIES_SEARCH, key));
        return String.format(WebService.URL_MOVIES_SEARCH, key);
    }

    /*
     * SEARCH MOVIE REQUEST
     *
     * @param String key
     *
     * @param SearchMoviesCallback scb
     */
    public void searchMoviesRequest(final Context context, String key,
                                    final SearchMoviesCallback scb) {
        JsonObjectRequest request = new JsonObjectRequest(Method.GET,
                getSearchMoviesURL(key), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject json) {
                        scb.onCompleted(MovieParser.getMovies(context, json));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                scb.onFailed(error);
            }
        }) {

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(RequestManager.VOLLEY_TAG);
        movieQueue.add(request);
    }

    //get MovieInfo URL
    private String getMovieInfoURL(String imdb_id) {
        return String.format(WebService.URL_MOVIE_INFO, imdb_id);
    }


    //get URL Movie's backdrop image
    private String getBackdropUrlImage(String imdb_id, String api_key) {
        String url = String.format(WebService.URL_MOVIE_BACKDROP, new String[]{imdb_id, api_key});
        return url;
    }

    // GET Movie's backgrop image
    public void movieBackdropImgRequest(String imdb_id, final onUpdateBackdropMovie backdropCallback) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                getBackdropUrlImage(imdb_id, API_KEY), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "movieBackdropImgRequest" + response.toString());
                        String url = URL_BACKDROP + MovieParser.parserImageBackdrop(response);
                        backdropCallback.onCompleted(url);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
            }
        });
        movieQueue.add(jsonObjReq);
    }

    /**
     * Extract link film
     */
    public void extractYTStreaming(String url, final ExtractCallback etc) {
        CustomStringRequest request = new CustomStringRequest(Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // TODO Auto-generated method stub
                        Log.i(TAG, response);
                        if (YTParser.extractEmbedded(response) == null) {
                            etc.onFailed(new VolleyError());
                        } else {
                            etc.onCompleted(YTParser.extractEmbedded(response));
                        }

                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                etc.onFailed(error);
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(RequestManager.VOLLEY_TAG);
        movieQueue.add(request);
    }

    public void streamCpatureYT(String url, final ExtractCallback etc) {
        CustomStringRequest request = new CustomStringRequest(Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // TODO Auto-generated method stub
                        Log.i(TAG, response);
                        etc.onCompleted(YTParser.streamCpature(response));
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                etc.onFailed(error);
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(RequestManager.VOLLEY_TAG);
        movieQueue.add(request);
    }

    /*
     *
     * For get new movie
     * */


    // get link videoYoutubelive
    public static String getVideoYoutubeLiveURL(String idChannel) {
        Log.e(TAG, "getVideoYoutubeLiveURL: " + String.format(WebService.URL_YOUTUBE_LIVE, idChannel));
        return String.format(WebService.URL_YOUTUBE_LIVE, idChannel);

    }

    // get DataYoutube

    public void moviesVideoIdLive(String idChannel,
                                  final MovieIdYoutubeCallback mcb) {
        JsonObjectRequest request = new JsonObjectRequest(Method.GET,
                getVideoYoutubeLiveURL(idChannel), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject json) {
                        mcb.onCompleted(MovieParser.getIdLive(json));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mcb.onFailed(error);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setTag(RequestManager.VOLLEY_TAG);
        movieQueue.add(request);
    }

    public void getMovieInfo(String imdb_id, final onUpdateMovieDetail onUpdateMovie) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                getMovieInfoURL(imdb_id), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        onUpdateMovie.onResponde(MovieParser.parserMovieDetial(response));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
            }
        });
        movieQueue.add(jsonObjReq);
    }



}
