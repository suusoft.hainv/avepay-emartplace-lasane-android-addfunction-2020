package com.aveplus.avepay_cpocket.trainingcenter.modelmanager;

public interface ModelManagerListener {
	public void onError();

	public void onSuccess(String json);

}
