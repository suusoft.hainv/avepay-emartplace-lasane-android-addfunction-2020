package com.aveplus.avepay_cpocket.trainingcenter.network;


import com.aveplus.avepay_cpocket.movie.movie.network.IBaseListener;

/**
 * Created by suusoft.com on 1/26/18.
 */

public interface ListenerLoading extends IBaseListener {
    void onLoadingIsProcessing();
    void onLoadingIsCompleted();
}
