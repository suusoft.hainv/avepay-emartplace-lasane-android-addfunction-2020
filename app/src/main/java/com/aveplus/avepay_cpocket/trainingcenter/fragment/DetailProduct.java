package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.SlidesAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.cart.CartManager;
import com.aveplus.avepay_cpocket.trainingcenter.lib.CirclePageIndicator;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;

import java.util.ArrayList;

public class DetailProduct extends BaseFragment {
    private CirclePageIndicator mIndicator;
    private SlidesAdapter adapter;
    private ViewPager viewPager;
    private View v;
    private TextView txtname, txtdesc, txtcontent, txtprice;
    private LinearLayout btaddcart;
    private String id, desc, name, price, categoryID, content;
    private ArrayList<String> arrImage = new ArrayList<String>();

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("detailpro");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("product");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.detail_product, container, false);
        initView(v);
        initData();
        initSlide();
        return v;
    }

    private void initData() {
        Bundle bundle = this.getArguments();
        name = bundle.getString("name");
        desc = bundle.getString("description");
        content = bundle.getString("content");
        categoryID = bundle.getString("categoryId");
        price = bundle.getString("price");
        arrImage = bundle.getStringArrayList("image");
        id = bundle.getString("id");

        txtname.setText(name);
        txtdesc.setText(desc);
        txtcontent.setText(content);
        txtprice.setText(price);

    }

    public void initView(View v) {

        txtname = (TextView) v.findViewById(R.id.txtProNameDetail);
        txtdesc = (TextView) v.findViewById(R.id.txtDescProDetail);
        txtcontent = (TextView) v.findViewById(R.id.txtContentProDetail);
        txtprice = (TextView) v.findViewById(R.id.txtPriceProductDetail);

        btaddcart = (LinearLayout) v.findViewById(R.id.btAddCart);
        btaddcart.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Product pro = new Product();
                pro.setId(id);
                pro.setName(name);
                pro.setImage(arrImage);
                pro.setNumberCart(1);
                pro.setPrice(price);
                pro.setCategoryId(categoryID);
                pro.getCategoryId();
                if (!CartManager.getInstance().isExist(pro)) {
                    CartManager.getInstance().addCart(pro);
                    Toast.makeText(getActivity(),
                            "Add " + name + " To Cart Successfully",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(),
                            "This item have already existed !",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void initSlide() {
//        adapter = new SlidesAdapter(getActivity(), arrImage);
//        viewPager = (ViewPager) v.findViewById(R.id.pagerPro);
//        viewPager.setAdapter(adapter);
//        mIndicator = (CirclePageIndicator) v.findViewById(R.id.indicator);
//        mIndicator.setViewPager(viewPager);
//        mIndicator.setFillColor(Color.RED);
    }
}
