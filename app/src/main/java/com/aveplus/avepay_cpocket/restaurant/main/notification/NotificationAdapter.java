package com.aveplus.avepay_cpocket.restaurant.main.notification;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    public static final int RQ_NOTIFICATION = 35;
    private List<NotificationModel> notificationList;
    private Context context;
    private OnItemClickListener listener;

    public NotificationAdapter(List<NotificationModel> notificationList, Context context, OnItemClickListener listener) {
        this.notificationList = notificationList;
        this.context = context;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(NotificationModel model);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_notification, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.binData(notificationList.get(i));
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView idNotificationImage;
        private TextView idNotificationTitle;
        private TextView idNotificationContent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            idNotificationImage = itemView.findViewById(R.id.id_NotificationImage);
            idNotificationTitle = itemView.findViewById(R.id.id_NotificationTitle);
            idNotificationContent = itemView.findViewById(R.id.id_NotificationContent);

        }

        public void binData(NotificationModel model) {
            idNotificationTitle.setText(model.getMessage());
            idNotificationContent.setText(model.getContent());
            Glide.with(context).load(model.getImage())
                    .apply(RequestOptions.placeholderOf(R.drawable.placeholder))
                    .into(idNotificationImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(model);
                }
            });
        }
    }
}
