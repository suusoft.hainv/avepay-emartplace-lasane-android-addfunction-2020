package com.aveplus.avepay_cpocket.movie.movie.util;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

/**
 * Copyright © 2019 SUUSOFT
 */
public class TextColorDetail {

    public SpannableStringBuilder setTextSpanDirector(String text){
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(102, 241, 253));
        StyleSpan bss = new StyleSpan(Typeface.BOLD);
        // Span to make text bold
        sb.setSpan(fcs, 0, 9, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        // Set the text color
        sb.setSpan(bss, 0, 9, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return sb;
    }
    public SpannableStringBuilder setTextSpanCasts(String text){
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(102, 241, 253));
        StyleSpan bss = new StyleSpan(Typeface.BOLD);
        // Span to make text bold
        sb.setSpan(fcs, 0, 6, 0);
        // Set the text color
        sb.setSpan(bss, 0, 6,0);
        return sb;
    }
    public SpannableStringBuilder setTextReleaseDate(String text){
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(102, 241, 253));
        StyleSpan bss = new StyleSpan(Typeface.BOLD);
        // Span to make text bold
        sb.setSpan(fcs, 0, 14, 0);
        // Set the text color
        sb.setSpan(bss, 0, 14,0);
        return sb;
    }
    /*
    * Set color on Phone size
    * */

    public SpannableStringBuilder setTextSpanDirectorPhone(String text){
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 0, 0));
        StyleSpan bss = new StyleSpan(Typeface.BOLD);
        // Span to make text bold
        sb.setSpan(fcs, 0, 9, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        // Set the text color
        sb.setSpan(bss, 0, 9, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        return sb;
    }
    public SpannableStringBuilder setTextSpanCastsPhone(String text){
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 0, 0));
        StyleSpan bss = new StyleSpan(Typeface.BOLD);
        // Span to make text bold
        sb.setSpan(fcs, 0, 6, 0);
        // Set the text color
        sb.setSpan(bss, 0, 6,0);
        return sb;
    }
    public SpannableStringBuilder setTextReleaseDatePhone(String text){
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        ForegroundColorSpan fcs = new ForegroundColorSpan(Color.rgb(0, 0, 0));
        StyleSpan bss = new StyleSpan(Typeface.BOLD);
        // Span to make text bold
        sb.setSpan(fcs, 0, 14, 0);
        // Set the text color
        sb.setSpan(bss, 0, 14,0);
        return sb;
    }
 }
