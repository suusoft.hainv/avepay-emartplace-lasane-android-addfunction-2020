package com.aveplus.avepay_cpocket.trainingcenter.modelmanager;

import android.util.Log;


import com.aveplus.avepay_cpocket.trainingcenter.config.GlobalValue;
import com.aveplus.avepay_cpocket.trainingcenter.config.WebserviceConfig;
import com.aveplus.avepay_cpocket.trainingcenter.object.CategoryNewEvent;
import com.aveplus.avepay_cpocket.trainingcenter.object.CategoryProduct;
import com.aveplus.avepay_cpocket.trainingcenter.object.Class;
import com.aveplus.avepay_cpocket.trainingcenter.object.Contact;
import com.aveplus.avepay_cpocket.trainingcenter.object.Customers;
import com.aveplus.avepay_cpocket.trainingcenter.object.DetailOrderHistory;
import com.aveplus.avepay_cpocket.trainingcenter.object.Galleries;
import com.aveplus.avepay_cpocket.trainingcenter.object.Info;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.aveplus.avepay_cpocket.trainingcenter.object.OrderHistory;
import com.aveplus.avepay_cpocket.trainingcenter.object.OurPeople;
import com.aveplus.avepay_cpocket.trainingcenter.object.Portfolios;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.aveplus.avepay_cpocket.trainingcenter.object.ProductHistory;
import com.aveplus.avepay_cpocket.trainingcenter.object.SaleOff;
import com.aveplus.avepay_cpocket.trainingcenter.object.Schedule;
import com.aveplus.avepay_cpocket.trainingcenter.object.Services;
import com.aveplus.avepay_cpocket.trainingcenter.object.Setting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParseUtility {
    private final static String KEY_DATA = "data";

    public static ArrayList<CategoryNewEvent> parseListNewsCategories(
            String json) {
        ArrayList<CategoryNewEvent> arrCategories = new ArrayList<CategoryNewEvent>();
        try {
            JSONObject object = new JSONObject(json);
            JSONArray arrjson = object.getJSONArray(KEY_DATA);
            JSONObject item;
            CategoryNewEvent category;
            for (int i = 0; i < arrjson.length(); i++) {
                item = arrjson.getJSONObject(i);
                category = new CategoryNewEvent();
                category.setId(item.getInt(WebserviceConfig.KEY_CATEGORY_ID));
                category.setName(item
                        .getString(WebserviceConfig.KEY_CATEGORY_NAME));

                category.setImage(item
                        .getString(WebserviceConfig.KEY_CATEGORY_IMAGE));

                arrCategories.add(category);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return arrCategories;
    }

    public static ArrayList<CategoryProduct> parseListProductCategories(
            String json) {
        ArrayList<CategoryProduct> arrCategories = new ArrayList<CategoryProduct>();
        try {
            JSONObject object = new JSONObject(json);
            JSONArray arrjson = object.getJSONArray(KEY_DATA);
            JSONObject item;
            CategoryProduct category;
            for (int i = 0; i < arrjson.length(); i++) {
                item = arrjson.getJSONObject(i);
                category = new CategoryProduct();
                category.setId(item.getInt(WebserviceConfig.KEY_CATEGORY_ID));
                category.setName(item
                        .getString(WebserviceConfig.KEY_CATEGORY_NAME));

                category.setImage(item
                        .getString(WebserviceConfig.KEY_CATEGORY_IMAGE));

                arrCategories.add(category);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return arrCategories;
    }

    public static ArrayList<Galleries> parseListGalleries(String json) {
        ArrayList<Galleries> arrGalleries = new ArrayList<Galleries>();
        Galleries galler = null;

        try {
            JSONObject item;
            JSONObject jsonObject = new JSONObject(json);
            JSONObject itemdata = jsonObject.getJSONObject(KEY_DATA);
            JSONArray itemarray = itemdata.getJSONArray("gallery");
            for (int i = 0; i < itemarray.length(); i++) {
                item = itemarray.getJSONObject(i);
                galler = new Galleries();
                galler.setUrl(item.getString("url"));
                arrGalleries.add(galler);
            }
            Log.d("Parse", "arrGalleries:" + arrGalleries.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrGalleries;

    }

    public static Info parseListInfomation(String json) {
        Info intro = new Info();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject itemdata = jsonObject.getJSONObject(KEY_DATA);
            JSONObject iteminfo = itemdata.getJSONObject("information");
            intro.setId(iteminfo.getString("id"));
            intro.setIntroduction(iteminfo.getString("introduction"));

            Log.d("Parse", "arrInfo:" + intro.getIntroduction());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return intro;

    }

    public static Setting parseSetting(String json) {
        Setting setting = new Setting();
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONObject itemdata = jsonObject.getJSONObject(KEY_DATA);
            setting.setId(itemdata.getString("id"));
            setting.setVat(itemdata.getString("VAT"));
            setting.setTransportFee(itemdata.getString("transportFee"));

            // }
            Log.d("Parse", "setting:");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return setting;

    }

    public static Contact parseListContact(String json) {
        Contact cont = new Contact();
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONObject itemdata = jsonObject.getJSONObject(KEY_DATA);
            cont.setId(itemdata.getString("id"));
            cont.setCompanyName(itemdata.getString("companyName"));
            cont.setAddress(itemdata.getString("address"));
            cont.setPhone(itemdata.getString("phone"));
            cont.setEmail(itemdata.getString("email"));
            cont.setWebsite(itemdata.getString("website"));
            cont.setLat(itemdata.getDouble("lat"));
            cont.setIlong(itemdata.getDouble("long"));
            cont.setTwitter(itemdata.getString("twitter"));
            cont.setFacebook(itemdata.getString("facebook"));

            // }
            Log.d("Parse", "Contact:");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cont;

    }

    public static ArrayList<OurPeople> parseListOurPeople(String json) {
        ArrayList<OurPeople> arrPeople = new ArrayList<OurPeople>();
        OurPeople people = null;

        try {
            JSONObject item;
            JSONObject jsonObject = new JSONObject(json);
            // JSONObject itemdata = jsonObject.getJSONObject(KEY_DATA);
            JSONArray itemcate = jsonObject.getJSONArray(KEY_DATA);
            for (int i = 0; i < itemcate.length(); i++) {
                item = itemcate.getJSONObject(i);
                people = new OurPeople();
                people.setId(item.getString("id"));
                people.setName(item.getString("name"));
                people.setImage(item.getString("image"));
                people.setPosition(item.getString("position"));
                people.setEmail(item.getString("email"));
                people.setSkype(item.getString("skype"));
                people.setDescription(item.getString("description"));
                arrPeople.add(people);

            }
            Log.d("Parse", "arrPeople:" + arrPeople.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrPeople;

    }

    public static ArrayList<Customers> parseListCustomer(String json) {
        ArrayList<Customers> arrCustomer = new ArrayList<Customers>();
        Customers customer = null;

        try {
            JSONObject item;
            JSONObject jsonObject = new JSONObject(json);
            JSONArray itemcate = jsonObject.getJSONArray(KEY_DATA);
            for (int i = 0; i < itemcate.length(); i++) {
                item = itemcate.getJSONObject(i);
                customer = new Customers();
                customer.setId(item.getString("id"));
                customer.setName(item.getString("name"));
                customer.setDate(item.getString("location") + ", " + item.getString("time"));
                customer.setInformation(item.getString("content"));
                arrCustomer.add(customer);

            }
            Log.d("Parse", "arrCustomer:" + arrCustomer.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrCustomer;

    }

    public static ArrayList<NewEvent> parseListNews(String json) {
        ArrayList<NewEvent> arrNews = new ArrayList<NewEvent>();
        NewEvent news = null;

        try {
            JSONObject item;
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonarray = jsonObject.getJSONArray(KEY_DATA);
            for (int i = 0; i < jsonarray.length(); i++) {
                news = new NewEvent();
                item = jsonarray.getJSONObject(i);
                news.setId(item.getString("id"));
                news.setTitle(item.getString("title"));
                news.setDescription(item.getString("description"));
                news.setContent(item.getString("content"));
                news.setImage(item.getString("image"));
                news.setTime(item.getString("time"));
                news.setLinkNews(item.getString("link"));
                arrNews.add(news);

            }
            Log.d("Parse", "arrNews:" + arrNews.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrNews;

    }

    public static ArrayList<Services> parseListService(String json) {
        ArrayList<Services> arrServ = new ArrayList<Services>();
        Services serv = null;

        try {
            JSONObject item;
            JSONObject jsonObject = new JSONObject(json);
            // JSONObject itemdata = jsonObject.getJSONObject(KEY_DATA);
            // JSONObject itemcate = itemdata.getJSONObject(KEY_DATA);

            JSONArray jsonarray = jsonObject.getJSONArray(KEY_DATA);
            for (int i = 0; i < jsonarray.length(); i++) {
                serv = new Services();
                item = jsonarray.getJSONObject(i);
                serv.setId(item.getString("id"));
                serv.setTitle(item.getString("title"));
                serv.setDescription(item.getString("description"));
                serv.setContent(item.getString("content"));
                serv.setImage(item.getString("image"));
                arrServ.add(serv);
            }
            Log.d("Parse", "arrService:" + arrServ.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrServ;

    }

    public static ArrayList<SaleOff> parseListSale(String json) {
        ArrayList<SaleOff> arrSale = new ArrayList<SaleOff>();
        SaleOff sale = null;

        try {
            JSONObject item;
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonarray = jsonObject.getJSONArray(KEY_DATA);
            for (int i = 0; i < jsonarray.length(); i++) {
                sale = new SaleOff();
                item = jsonarray.getJSONObject(i);
                sale.setId(item.getString("id"));
                sale.setTitle(item.getString("title"));
                sale.setDescription(item.getString("description"));
                sale.setContent(item.getString("content"));
                sale.setImage(item.getString("image"));
                arrSale.add(sale);

            }
            Log.d("Parse", "arrSale:" + arrSale.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrSale;

    }

    public static ArrayList<Portfolios> parseListPort(String json) {
        ArrayList<Portfolios> arrPort = new ArrayList<Portfolios>();
        Portfolios port = null;

        try {
            JSONObject item;
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonarray = jsonObject.getJSONArray(KEY_DATA);
            for (int i = 0; i < jsonarray.length(); i++) {
                port = new Portfolios();
                item = jsonarray.getJSONObject(i);
                port.setId(item.getString("id"));
                port.setTitle(item.getString("title"));
                port.setDesc(item.getString("description"));
                port.setContent(item.getString("content"));
                port.setImage(item.getString("image"));
                arrPort.add(port);

            }
            Log.d("Parse", "arrPort:" + arrPort.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrPort;

    }

    public static ArrayList<Product> parseListPro(String json) {
        ArrayList<Product> arrPro = new ArrayList<Product>();
        Product pro;
        try {

            JSONObject item;
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonarray = jsonObject.getJSONArray(KEY_DATA);

            for (int i = 0; i < jsonarray.length(); i++) {
                item = jsonarray.getJSONObject(i);
                ArrayList<String> image = new ArrayList<String>();
                pro = new Product();
                ArrayList<Class> mListClass = new ArrayList<>();
                JSONArray arritems = item.getJSONArray("gallery");
                for (int j = 0; j < arritems.length(); j++) {
                    image.add(arritems.getJSONObject(j).getString("url"));
                }
                pro.setId(item.getString("id"));
                pro.setName(item.getString("name"));
                pro.setDescription(item.getString("description"));
                pro.setContent(item.getString("content"));
                pro.setPrice(item.getString("price"));
                pro.setCategoryId(item.getString("categoryId"));
                pro.setImage(image);
                JSONArray arrClass = item.getJSONArray("classes");
                for (int j = 0; j < arrClass.length(); j++) {
                    JSONObject mClass = arrClass.getJSONObject(j);
                    JSONArray arrSchedule = mClass.getJSONArray("schdules");
                    ArrayList<Schedule> mListSchedule = new ArrayList<>();
                    for (int k = 0; k < arrSchedule.length(); k++) {
                        JSONObject schedule = arrSchedule.getJSONObject(k);
                        Schedule sche = new Schedule(schedule.getString("weekday"), schedule.getString("startTime"), schedule.getString("endTime"));
                        mListSchedule.add(sche);

                    }
                    Class aClass = new Class(mClass.getString("id"), mClass.getString("name"), mClass.getString("startDate"), mClass.getString("endDate"), mListSchedule);
                    mListClass.add(aClass);
                }
                pro.setmListClass(mListClass);
                arrPro.add(pro);

            }
            Log.d("Parse", "arrPro:" + arrPro.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arrPro;

    }

    public static String putJsonCart(ArrayList<Product> list) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            JSONObject object = new JSONObject();
            try {
                object.put("productId", list.get(i).getId());
                object.put("quantity", list.get(i).getNumberCart());
                jsonArray.put(object);

            } catch (JSONException e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        JSONObject jsoncart = new JSONObject();
        try {
            jsoncart.put("data", jsonArray);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String jsonStr = jsoncart.toString();
        return jsonStr;
        // System.out.println("jsonString: " + jsonStr);
    }

    public static int getAllPage(String json) {
        int allPage = 0;
        JSONObject jsonitems;
        try {
            jsonitems = new JSONObject(json);
            allPage = jsonitems.getInt("totalpages");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return allPage;

    }

    public static ArrayList<OrderHistory> parseHistory(String json) {
        ArrayList<OrderHistory> list = new ArrayList<OrderHistory>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject.getString(GlobalValue.JSON_STATUS).equalsIgnoreCase(GlobalValue.JSON_SUCCESS)) {
                JSONArray arr = jsonObject.getJSONArray(KEY_DATA);
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);

                    OrderHistory his = new OrderHistory(obj.getString("id"), obj.getString("email"),
                            obj.getString("date"), obj.getString("status"));

                    list.add(his);
                }
            }
        } catch (JSONException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return list;
    }

    public static DetailOrderHistory parseDetailHistory(String json) throws JSONException {

        DetailOrderHistory detailHistory;
        JSONObject jsonObject = new JSONObject(json);
        JSONObject itemData = jsonObject.getJSONObject(KEY_DATA);
        detailHistory = new DetailOrderHistory();
        // set detail
        JSONObject detail = itemData.getJSONObject("detail");
        detailHistory.setOrderId(detail.getString("id"));
        detailHistory.setFullName(detail.getString("fullName"));
        detailHistory.setEmail(detail.getString("email"));
        detailHistory.setPaymentMethod(detail.getString("paymentMethod"));
        detailHistory.setStatus(detail.getString("status"));
        detailHistory.setTotal(detail.getString("total"));
        detailHistory.setVat(detail.getString("vat"));
        detailHistory.setCreateDate(detail.getString("date"));

//		detailHistory.setPayment_detail(jsonObject.getString("payment_detail"));

        // set item
        ArrayList<ProductHistory> list = new ArrayList<>();
        JSONArray arr = itemData.getJSONArray("items");
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            ProductHistory ph = new ProductHistory(obj.getString("productName"), obj.getString("quantity"));
            list.add(ph);
        }
        detailHistory.setListProHistory(list);

        return detailHistory;
    }

}
