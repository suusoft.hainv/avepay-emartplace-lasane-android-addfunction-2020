package com.aveplus.avepay_cpocket.view.adapters;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.interfaces.IOnItemClickListener;
import com.aveplus.avepay_cpocket.objects.Acceuil;

import java.util.ArrayList;

public class AcceuilAdapter extends RecyclerView.Adapter<AcceuilAdapter.ViewHolder> {

    private ArrayList<Acceuil> listAcceuil;
    private Context context;
    private IOnItemClickListener listener;

    public void setListener(IOnItemClickListener listener) {
        this.listener = listener;
    }

    public AcceuilAdapter(ArrayList<Acceuil> listAcceuil, Context context) {
        this.listAcceuil = listAcceuil;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_acceuil, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Acceuil acceuil = listAcceuil.get(position);
        holder.tvAcceuil.setText(acceuil.getName());
        holder.imgAcceuil.setImageResource(acceuil.getImage());


        if (listAcceuil.get(position).getName().equals("shopping")) {
            holder.imgAcceuil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(2);
                    }
                }
            });
        } else if (listAcceuil.get(position).getName().equals("movie")) {
            holder.imgAcceuil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(1);
                    }
                }
            });
        } else if (listAcceuil.get(position).getName().equals("music")) {
            holder.imgAcceuil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(3);
                    }
                }
            });
        } else if (listAcceuil.get(position).getName().equals("ebook")) {
            holder.imgAcceuil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(4);
                    }
                }
            });
        } else if (listAcceuil.get(position).getName().equals("restaurant")) {
            holder.imgAcceuil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(5);
                    }
                }
            });
        } else if (listAcceuil.get(position).getName().equals("training center")) {
            holder.imgAcceuil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(6);
                    }
                }
            });
        } else if (listAcceuil.get(position).getName().equals("Taxi")) {
            holder.imgAcceuil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemClick(7);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (listAcceuil == null) ? 0 : listAcceuil.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvAcceuil;
        private ImageView imgAcceuil;
        private LinearLayout lnItem;

        public ViewHolder(View itemView) {
            super(itemView);
            lnItem = itemView.findViewById(R.id.ln_item);
            tvAcceuil = itemView.findViewById(R.id.tv_acceuil);
            imgAcceuil = itemView.findViewById(R.id.img_acceuil);
        }
    }
}
