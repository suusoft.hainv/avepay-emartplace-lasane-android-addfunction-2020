package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.FragmentDetailsChapterNormalBinding;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.model.ChapterNomal;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;
import com.aveplus.avepay_cpocket.ebook.view.activity.DetailsBookActivity;
import com.aveplus.avepay_cpocket.ebook.view.adapter.ChapterDetailsNormalAdapter;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.DetailsChapterNormalVM;

import java.util.ArrayList;

public class FragmentDetailsChapterNORMAL extends BaseFragmentBinding {

    private Chapter chapter;
    private DetailsChapterNormalVM viewModel;
    private FragmentDetailsChapterNormalBinding binding;
    private TextView tvIndicator;
    private ChapterDetailsNormalAdapter adapter;
    private ArrayList<ChapterNomal> listChapterNormal;
    private int page = 1;
    private boolean isLoad = false;

    public static FragmentDetailsChapterNORMAL newInstance(Chapter chapter, Book book) {
        Bundle args = new Bundle();
        args.putParcelable(Constant.KEY_CHAPTER, chapter);
        args.putParcelable(Constant.KEY_BOOK, book);
        FragmentDetailsChapterNORMAL fragment = new FragmentDetailsChapterNORMAL();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_details_chapter_normal;
    }

    @Override
    protected void initialize() {
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            chapter = bundle.getParcelable(Constant.KEY_CHAPTER);
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new DetailsChapterNormalVM(self, chapter);
        return viewModel;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        this.binding = (FragmentDetailsChapterNormalBinding) binding;
        this.binding.setViewModel(viewModel);
    }


    @Override
    protected void initView(View view) {
        listChapterNormal = new ArrayList<>();
        adapter = new ChapterDetailsNormalAdapter(getChildFragmentManager(), listChapterNormal);
        binding.vpNormal.setAdapter(adapter);
        tvIndicator = (TextView) view.findViewById(R.id.tv_indicator);
        tvIndicator.setText("1/" + adapter.getCount());
        loadData();
        binding.vpNormal.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                tvIndicator.setText(position + 1 + "/" + adapter.getCount());
            }

            @Override
            public void onPageSelected(int position) {
                if (position == listChapterNormal.size()) {
                    loadData();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void loadData() {
        if (!isLoad) {
            RequestManager.getChapterByChapterId(self, chapter.getId(), String.valueOf(page), new ApiManager.CompleteListener() {
                @Override
                public void onSuccess(ApiResponse response) {
                    listChapterNormal.addAll(response.getDataList(ChapterNomal.class));
                    adapter.notifyDataSetChanged();
                    page++;
                    isLoad = false;
                }

                @Override
                public void onError(String message) {
                    isLoad = false;

                }
            });
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.details_chapter_epub_pdf_normal, menu);
        menu.findItem(R.id.action_bookmarks).setVisible(false);
        if (DataStoreManager.isBookMarks(chapter.getBookId(), Constant.LIST_BOOK_MARKS)) {
            menu.findItem(R.id.action_bookmarks).setIcon(R.drawable.ic_book_marks_yellow);
        } else {
            menu.findItem(R.id.action_bookmarks).setIcon(R.drawable.ic_book_marks_white);
        }
        menu.findItem(R.id.action_move).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_fullscreen:
                ((DetailsBookActivity) self).showFullScreen(true);
                tvIndicator.setVisibility(View.GONE);
                return true;
            case R.id.action_bookmarks:
                if (DataStoreManager.isBookMarks(chapter.getBookId(), Constant.LIST_BOOK_MARKS)) {
                    ((DetailsBookActivity) self).deleteBookrMarks();
                    item.setIcon(R.drawable.ic_book_marks_white);
                } else {
                    ((DetailsBookActivity) self).addBookrMarks();
                    item.setIcon(R.drawable.ic_book_marks_yellow);
                }
                return true;
            case R.id.action_move:
                showDialogMove();
                return true;
            case R.id.action_comment:
                ((FragmentActivity) self).getSupportFragmentManager().beginTransaction()
                        .add(R.id.content_details, FragmentComment.newInstance(chapter), Constant.FRAGMENT_COMENT)
                        .addToBackStack(null)
                        .commit();

                return true;
        }
        return false;
    }

    private void showDialogMove() {
        Toast.makeText(self, "dialog", Toast.LENGTH_SHORT).show();
    }
}