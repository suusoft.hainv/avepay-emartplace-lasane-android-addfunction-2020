package com.aveplus.avepay_cpocket.view.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseActivity;
import com.aveplus.avepay_cpocket.utils.AppUtil;

public class TermActivity extends BaseActivity {
    private String link;
    private WebView webview;
    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_term;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {


        webview = findViewById(R.id.webView1);


        Bundle bundle=getIntent().getExtras();
        link=bundle.getString("link");
        // web settings
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setBuiltInZoomControls(false);
        webview.getSettings().setSupportZoom(true);

        webview.setWebViewClient(new myWebClient());
        webview.loadUrl("http://suusoft.com");

        webview.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
                    webview.goBack();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onViewCreated() {

    }
    public class myWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            AppUtil.showToast(self, R.string.msg_no_network);
            view.loadUrl("about:blank");
        }

    }
}
