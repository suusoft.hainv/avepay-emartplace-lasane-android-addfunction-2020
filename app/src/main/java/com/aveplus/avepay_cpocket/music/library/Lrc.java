package com.aveplus.avepay_cpocket.music.library;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public class Lrc {
    private long time;
    private String text;

    public void setTime(long time) {
        this.time = time;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTime() {
        return time;
    }

    public String getText() {
        return text;
    }

}