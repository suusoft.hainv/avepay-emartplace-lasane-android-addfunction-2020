package com.aveplus.avepay_cpocket.ebook.network;

import android.content.Context;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.AbstractActivity;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Trang on 7/10/2017.
 */
public class BaseRequest {

    // default params
    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_USER_ID = "user_id";

    //private static MyProgressDialog progressDialog;
    private static BaseRequest instance;

    private AbstractActivity mActivity;

    /**
     * init this
     */
    public static void init(Context ctx) {
        instance = new BaseRequest();
        ApiManager.getInstance();
    }

    /**
     * set default pamrams
     */
    private static void addDefaultParams(HashMap<String, String> params) {
        String userId = DataStoreManager.getUser() != null ? DataStoreManager.getUser().getId() : "";
        params.put(PARAM_USER_ID, userId);
        params.put(PARAM_TOKEN, AppController.getInstance().getToken());
    }


    /**
     * get instance
     */

    public static BaseRequest getInstance() {
        if (instance != null) {
            return instance;
        } else {
            throw new IllegalStateException("Not initialized");
        }
    }


    //********************************************************************************
    public interface ListenerLoading {
        void onLoadingIsProcessing();

        void onLoadingIsCompleted();
    }

    /**
     * interface for listening progress.
     * purpose is show and hide progressbar
     */
    private ListenerLoading listenerLoading;

    public ListenerLoading getListenerLoading() {
        return listenerLoading;
    }

    public void setListenerLoading(ListenerLoading listenerLoading) {
        this.listenerLoading = listenerLoading;
    }

    /**
     * show hide progress bar
     */
    private void showProgress(boolean isOpen) {
        if (isOpen && listenerLoading != null) {
            listenerLoading.onLoadingIsProcessing();
        }
    }

    public void hideProgress(boolean isOpen) {
        if (isOpen && listenerLoading != null) {
            listenerLoading.onLoadingIsCompleted();
        }
    }

    // option manual
    public static void get(final String url, final HashMap<String, String> params, boolean isShowProgress, boolean isCaching, final ApiManager.CompleteListener completeListener) {
        getInstance().request(url, params, isShowProgress, isCaching, completeListener);
    }

    // caching is false. ProgressBar is option
    public static void get(final String url, final HashMap<String, String> params, boolean isShowProgress, final ApiManager.CompleteListener completeListener) {
        getInstance().request(url, params, isShowProgress, false, completeListener);
    }

    // default is showing progress and not caching
    public static void get(final String url, final HashMap<String, String> params, final ApiManager.CompleteListener completeListener) {
        getInstance().request(url, params, true, false, completeListener);
    }

    /**
     * Using method get.
     *
     * @param url
     * @param params
     * @param isShowProgress   showing progress or not?
     * @param isCaching        has caching or not?
     * @param completeListener listener
     */
    private void request(final String url, final HashMap<String, String> params, final boolean isShowProgress, final boolean isCaching, final ApiManager.CompleteListener completeListener) {
        if (NetworkUtility.isNetworkAvailable()) {
            showProgress(isShowProgress);
//            addDefaultParams(params);
            ApiManager.get(url, params, new ApiManager.CompleteListener() {

                @Override
                public void onSuccess(ApiResponse response) {
                    hideProgress(isShowProgress);
                    if (response.getDataObject() != null || response.getDataArray() != null) {
                        String objectRoot = response.getRoot().toString();

                        completeListener.onSuccess(response);

                        // if caching = true. data will be cached. save it to db
                        if (isCaching) {
                            DataStoreManager.saveCaching(url, objectRoot, response.getValueFromRoot(Constant.Caching.KEY_TIME_UPDATED));
                        }
                    } else {

                        // case when server response is "data is newest". No data and using caching
                        if (isCaching) {
                            completeListener.onSuccess(new ApiResponse(getOfflineResponse(url)));
                        } else if (!response.isError()) {
                            completeListener.onSuccess(response);
                        }
                    }

                }

                @Override
                public void onError(String message) {
                    hideProgress(isShowProgress);
                    if (!isCaching) {
                        completeListener.onError(message);
                    } else {
                        completeListener.onSuccess(new ApiResponse(getOfflineResponse(url)));
                    }
                }
            });

        } else {
            if (isCaching) {
                completeListener.onSuccess(new ApiResponse(getOfflineResponse(url)));
            } else {
                AppUtil.showToast(mActivity, R.string.msg_connection_network_error);
            }
        }


    }

    /**
     * get response from db. Filter by url (action's name)
     *
     * @param url
     */
    private JSONObject getOfflineResponse(String url) {
        try {
            return new JSONObject(DataStoreManager.getCaching(url));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
