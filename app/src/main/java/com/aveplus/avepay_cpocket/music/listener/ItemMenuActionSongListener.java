package com.aveplus.avepay_cpocket.music.listener;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public interface ItemMenuActionSongListener extends IBaseListener {
    void onClickWatch(int position);
    void onClickAddToQueue(int position);
    void onClickDelete(int position);

}
