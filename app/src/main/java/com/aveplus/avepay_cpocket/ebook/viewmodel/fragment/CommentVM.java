package com.aveplus.avepay_cpocket.ebook.viewmodel.fragment;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.model.Comment;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;


/**
 * Created by Suusoft on 9/8/2017.
 */
public class CommentVM extends BaseViewModelList {

    private Chapter chapter;

    public CommentVM(Context context, Chapter chapter) {
        super(context);
        this.chapter = chapter;
        getData(1);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(self);
    }

    @Override
    public void getData(int page) {
        RequestManager.getCommentByChapter(self, chapter.getId(), String.valueOf(page), new ApiManager.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                addListData(response.getDataList(Comment.class));
                checkLoadingMoreComplete(Integer.parseInt(response.getValueFromRoot(ApiResponse.KEY_TOTAL_PAGE)));
            }

            @Override
            public void onError(String message) {

            }
        });
    }
}

