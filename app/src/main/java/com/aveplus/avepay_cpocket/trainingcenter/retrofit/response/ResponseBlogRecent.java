package com.aveplus.avepay_cpocket.trainingcenter.retrofit.response;


import com.aveplus.avepay_cpocket.movie.movie.retrofit.base.BaseReponse;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.aveplus.avepay_cpocket.trainingcenter.object.Services;

import java.util.ArrayList;

public class ResponseBlogRecent extends BaseReponse {
    public ArrayList<NewEvent> data;

    public ArrayList<NewEvent> getData() {
        return data;
    }

    public void setData(ArrayList<NewEvent> data) {
        this.data = data;
    }
}
