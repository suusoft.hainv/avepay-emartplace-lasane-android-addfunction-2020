package com.aveplus.avepay_cpocket.ebook.viewmodel.activity;

import android.content.Context;
import android.os.Bundle;


import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.ListBookViewMoreActivity;
import com.aveplus.avepay_cpocket.ebook.view.fragment.HomeFragment;

import java.util.ArrayList;

/**
 * Created by trangpham on 3/4/18.
 */

public class ListBookViewMoreVM extends BaseViewModelList {

    int type;

    public ListBookViewMoreVM(Context context, Bundle bundle) {
        super(context, bundle);
        int position = bundle.getInt(ListBookViewMoreActivity.KEY_TYPE);
        if (position == HomeFragment.POSITION_TOP)
            type = 1;
        else if (position == HomeFragment.POSITION_NEW)
            type = 2;
        else type = 3;

        getData(1);
    }

    @Override
    public void getData(int page) {
        RequestManager.getBookByCondition(self, type, page + "", new ApiManager.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                ArrayList<Book> list = (ArrayList<Book>) response.getDataList(Book.class);
                addListData(list);
                checkLoadingMoreComplete(Integer.parseInt(response.getValueFromRoot(Config.KEY_TOTAL_PAGE)));
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }
}
