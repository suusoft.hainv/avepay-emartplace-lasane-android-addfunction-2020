package com.aveplus.avepay_cpocket.restaurant.main.restaurant.order;

import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;

/**
 * Created by suusoft.com on 11/12/19.
 */

public class ItemModel extends BaseModel {
    private int id;
    private String foodName;
    private int number;
    private int price;
    private int total;

    public ItemModel(int id, String foodName, int number, int price, int total) {
        this.id = id;
        this.foodName = foodName;
        this.number = number;
        this.price = price;
        this.total = total;
    }

    public ItemModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

}
