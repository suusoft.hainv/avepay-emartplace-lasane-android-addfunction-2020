package com.aveplus.avepay_cpocket.music.main.video;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.NonNull;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseAdapterBinding;

import java.util.List;

public class CategoryAdapter extends BaseAdapterBinding {
    private List<Category> listData;

    public CategoryAdapter(Context context, List<?> datas) {
        super(context);
        this.listData = (List<Category>) datas;
    }

    @Override
    public void addDatas(List<?> data) {
        int positionStart = listData.size();
        notifyItemRangeInserted(positionStart, data.size());
    }

    @Override
    public void setDatas(List<?> data) {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(getViewBinding(parent, R.layout.item_category_genre));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Category item = listData.get(position);
        if (item != null) {
            holder.bind(new ItemCategoryVM(context, item));
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}
