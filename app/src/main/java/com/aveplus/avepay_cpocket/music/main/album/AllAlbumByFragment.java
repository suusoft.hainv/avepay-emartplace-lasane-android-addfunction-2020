package com.aveplus.avepay_cpocket.music.main.album;

import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentNavBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.main.home.ItemAlbumVM;
import com.aveplus.avepay_cpocket.utils.AppUtil;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class AllAlbumByFragment extends BaseListFragmentNavBinding {

    public static final String KEY_OBJECT = "OBJECT";
    public static final String KEY_POSITION = "KEY_POSITION";
    AllAlbumByVM viewModel;
    String album;

    public static AllAlbumByFragment newInstance(String album, int position) {

        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        args.putString(KEY_OBJECT, album);

        AllAlbumByFragment fragment = new AllAlbumByFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new AllAlbumByVM(self, getArguments());
        return viewModel;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        setTitle(album);
        showHideActionMenu(false);
        recyclerView.setPadding(AppUtil.convertDpToPixel(8), 0, 0, 0);
        recyclerView.setLayoutManager(new GridLayoutManager(self, 2));
        recyclerView.setAdapter(new SingleAdapter(self, R.layout.item_album, viewModel.getListData(), ItemAlbumVM.class, viewModel.getActionListener()));
    }

    @Override
    protected void initialize() {
        album = getArguments().getString(KEY_OBJECT);
    }
}
