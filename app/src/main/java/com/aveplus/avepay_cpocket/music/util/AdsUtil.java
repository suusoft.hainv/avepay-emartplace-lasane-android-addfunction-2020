package com.aveplus.avepay_cpocket.music.util;

import android.content.Context;

import com.aveplus.avepay_cpocket.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class AdsUtil {

    public static final String TEST_DEVICE = "E4F547339E2F2AA8A3C840384EADA42F";
    private static int count = 0;
    private static long timeold = 0;
    // for ads
    public static void loadBanner(AdView adView){
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(TEST_DEVICE)
                .build();
        adView.loadAd(adRequest);
    }

    public static void loadInterstitial(Context context){
        final InterstitialAd mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(context.getString(R.string.admob_interstitial_id));
        mInterstitialAd.loadAd(new AdRequest.Builder()
                .addTestDevice(TEST_DEVICE)
                .build());
        mInterstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();

            }
        });

    }
    public static void checkShowAds(Context context) {
        count = count + 1;

        if (count == 5) {
            loadInterstitial (context);
            count = 0;
        }
//        else if (count == 10) {
//            loadInterstitial (context);
//            count = 5;
//
//        }
    }

}
