package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.listener.IOnClickListener;
import com.aveplus.avepay_cpocket.trainingcenter.object.Services;
import com.aveplus.avepay_cpocket.trainingcenter.view.IOnClick;
import com.aveplus.avepay_cpocket.utils.ImageUtil;

import java.util.ArrayList;

public class AdapterFaq extends RecyclerView.Adapter<AdapterFaq.MyViewHolder> {

    private ArrayList<Services> listServices;
    Context context;
    private IOnClick onClick;


    public AdapterFaq(ArrayList<Services> listService, Context context, IOnClick onClick) {
        this.listServices = listService;
        this.context = context;
        this.onClick = onClick;
    }

    public void addList(ArrayList<Services> listServices) {
        this.listServices = listServices;
        this.notifyDataSetChanged();
    }

    public AdapterFaq() {
    }


    @NonNull
    @Override
    public AdapterFaq.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_faq, parent, false);
        return new AdapterFaq.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFaq.MyViewHolder holder, int position) {
        final Services services = listServices.get(position);
        ImageUtil.setImage(holder.img, services.getImage());
        holder.tvTitleServicesDetail.setText(services.getTitle());
        holder.tvContentQA.setText(services.getContent());
        holder.tvDescreption.setText(services.getDescription());
        holder.llDetailFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClick != null) {
                    onClick.OnClick(position, services);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listServices == null ? 0 : listServices.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView tvTitleServicesDetail, tvContentQA, tvDescreption;
        private LinearLayout llDetailFaq;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imgServiceDetail);
            tvTitleServicesDetail = (TextView) itemView.findViewById(R.id.txtTitleServicesDetail);
            tvContentQA = (TextView) itemView.findViewById(R.id.tvContentQA);
            tvDescreption = (TextView) itemView.findViewById(R.id.tvDescreption);
            llDetailFaq = (LinearLayout) itemView.findViewById(R.id.ll_detail_faq);

        }
    }
}
