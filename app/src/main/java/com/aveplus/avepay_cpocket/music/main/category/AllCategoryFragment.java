package com.aveplus.avepay_cpocket.music.main.category;

import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentNavBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.main.home.ItemCategoryVM;
import com.aveplus.avepay_cpocket.utils.AppUtil;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class AllCategoryFragment extends BaseListFragmentNavBinding {

    private static final String KEY_OBJECT = "OBJECT";
    AllCategoryVM viewModel;

    public static AllCategoryFragment newInstance() {

        Bundle args = new Bundle();
        AllCategoryFragment fragment = new AllCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new AllCategoryVM(self);
        return viewModel;
    }


    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        setTitle(R.string.all_category);
        showHideActionMenu(false);
        recyclerView.setPadding(AppUtil.convertDpToPixel(10), 0, 0, 0);
        recyclerView.setLayoutManager(new GridLayoutManager(self, 2));
        recyclerView.setAdapter(new SingleAdapter(self, R.layout.item_category_music2, viewModel.getListData(), ItemCategoryVM.class, viewModel.getActionListener()));
    }

    @Override
    protected void initialize() {
    }
}
