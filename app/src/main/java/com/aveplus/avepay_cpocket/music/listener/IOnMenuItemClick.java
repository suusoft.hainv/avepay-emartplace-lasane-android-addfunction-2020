package com.aveplus.avepay_cpocket.music.listener;

import android.view.MenuItem;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public interface IOnMenuItemClick {

    void onMenuItemClick(MenuItem item);
}
