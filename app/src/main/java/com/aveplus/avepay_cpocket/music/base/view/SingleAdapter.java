package com.aveplus.avepay_cpocket.music.base.view;

import android.content.Context;
import android.view.ViewGroup;


import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.listener.IBaseListener;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class SingleAdapter extends BaseAdapterBinding {

    private List mDatas;
    private int mLayout;
    private BaseAdapterVM mItemVM;
    private Constructor<?> cons;
    private IBaseListener listener;

    public SingleAdapter(Context context, int layout, List<?> listData, Class itemVMClass) {
        super(context);
        mDatas = listData;
        mLayout = layout;

        try {
           cons = itemVMClass.getConstructor(Context.class, Object.class, int.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public SingleAdapter(Context context, int layout, List<?> listData, Class itemVMClass, IBaseListener listener) {
        this(context, layout,listData,itemVMClass);
        this.listener = listener;
    }

    @Override
    public void addDatas(List<?> data) {
        int positionStart = mDatas.size();
        notifyItemRangeInserted(positionStart, data.size());
    }

    @Override
    public void setDatas(List<?> data){
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getViewBinding(parent, mLayout));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Object item = mDatas.get(position);
        if (item != null) {
            try {
                mItemVM = (BaseAdapterVM) cons.newInstance(context, item, position);
                if (listener != null)
                    mItemVM.setListener(listener);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            holder.bind(mItemVM);
        }

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public void setListener(IBaseListener listener){
        this.listener = listener;
    }

}
