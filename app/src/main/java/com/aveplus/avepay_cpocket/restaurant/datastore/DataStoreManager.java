package com.aveplus.avepay_cpocket.restaurant.datastore;

import com.aveplus.avepay_cpocket.restaurant.main.login.models.UserModels;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.OrderModel;
import com.aveplus.avepay_cpocket.restaurant.main.save.savemodels.SaveMdoels;
import com.aveplus.avepay_cpocket.restaurant.model.User;
import com.google.gson.Gson;


/**
 * Created by phamv on 9/13/2016.
 */

public class DataStoreManager extends BaseDataStore {


    // ============== User ============================
    private static final String PREF_USER = "PREF_USER";
    private static final String PREF_TOKEN_USER = "PREF_TOKEN_USER";
    private static final String PREF_ITEM = "PREF_ITEM";

    // ============== Settings ============================
    private static final String PREF_SETTINGS_NOTIFY = "PREF_SETTINGS_NOTIFY";
    private static final String PREF_SETTINGS_KEEP_SCREEN = "PREF_SETTINGS_KEEP_SCREEN";
    private static final String PREF_SETTINGS_NOTIFY_MESSAGE = "PREF_SETTINGS_NOTIFY_MESSAGE";
    // ============== Data Cache ============================
    private static final String PREF_DATA_CHANNEL = "PREF_DATA_CHANNEL";

    private static final String PREF_ORDER = "PREF_ORDER";

    /**
     * save and get user
     */
    public static void saveUser(UserModels user) {
        if (user != null) {
            String jsonUser = user.toJSon();
            getInstance().sharedPreferences.putStringValue(PREF_USER, jsonUser);
        }
    }

    public static void removeUser() {
        getInstance().sharedPreferences.putStringValue(PREF_USER, null);
    }

    public static User getUser() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_USER);
        User user = new Gson().fromJson(jsonUser, User.class);
        return user;
    }

    public static UserModels getUserModels() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_USER);
        UserModels user = new Gson().fromJson(jsonUser, UserModels.class);
        return user;
    }

    public static void removeUserModels() {
        getInstance().sharedPreferences.putStringValue(PREF_USER, null);
    }

    public static void saveOder(OrderModel orderModel) {
        if (orderModel != null) {
            String jsonOrder = orderModel.toJSon();
            getInstance().sharedPreferences.putStringValue(PREF_ORDER, jsonOrder);
        }
    }

    public static OrderModel getOrderModels() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_USER);
        OrderModel orderModel = new Gson().fromJson(jsonUser, OrderModel.class);
        return orderModel;
    }

    public static void saveLike(SaveMdoels saveMdoels) {
        if (saveMdoels != null) {
            String jsonItem = saveMdoels.toJSon();
            getInstance().sharedPreferences.putStringValue(PREF_ITEM, jsonItem);
        }
    }

    public static SaveMdoels getLike() {
        String jsonItem = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_ITEM);
        SaveMdoels saveMdoels = new Gson().fromJson(jsonItem, SaveMdoels.class);
        return saveMdoels;
    }


    /**
     * save and get user's token
     */
    public static void saveToken(String token) {
        getInstance().sharedPreferences.putStringValue(PREF_TOKEN_USER, token);
    }

    public static String getToken() {
        return getInstance().sharedPreferences.getStringValue(PREF_TOKEN_USER);
    }


    /**
     * =======================================================
     * Settings screen
     */
    public static void saveSettingsNotify(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY, isChecked);
    }

    public static boolean getSettingsNotify() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY);
    }

    public static void saveSettingsNotifyMessage(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY_MESSAGE, isChecked);
    }

    public static boolean getSettingsNotifyMessage() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY_MESSAGE);
    }

    public static void setAwakeScreen(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_KEEP_SCREEN, isChecked);
    }

    public static boolean isAwakeScreen() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_KEEP_SCREEN);
    }

}
