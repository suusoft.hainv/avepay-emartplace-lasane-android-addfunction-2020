package com.aveplus.avepay_cpocket.restaurant.main.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeUser;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.util.StringUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.ViewDialog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FragRegister extends BaseFragment implements View.OnClickListener, View.OnTouchListener {

    private EditText edUsername, edName, edAddres, edPassword, edPhone, edConfirmPass;
    private TextView tvRegister, tvLogin;
    private ProgressDialog progressDialog;
    private ImageView imgAvatar;
    private Bitmap bitmap;

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_register;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        edUsername = view.findViewById(R.id.ed_username_rg);
        edName = view.findViewById(R.id.ed_name);
        edAddres = view.findViewById(R.id.ed_address_rg);
        edPassword = view.findViewById(R.id.ed_password_rg);
        edConfirmPass = view.findViewById(R.id.ed_password_confime);
        edPhone = view.findViewById(R.id.ed_phone_rg);
        tvLogin = view.findViewById(R.id.tv_login);
        tvRegister = view.findViewById(R.id.tv_register);
//        imgAvatar   = view.findViewById(R.id.circle_image);
//        imgAvatar.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
        tvRegister.setOnTouchListener(this);

        TelephonyManager tMgr = (TelephonyManager) self.getSystemService(Context.TELEPHONY_SERVICE);
        if ( ActivityCompat.checkSelfPermission(self, Manifest.permission.READ_PHONE_NUMBERS)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(self, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            Log.e("TAG", "no permission" );
            return;
        }else{
            String mPhoneNumber = tMgr.getLine1Number();
            Log.e("TAG", "initView: "+mPhoneNumber );
            if (mPhoneNumber!=null){
                edPhone.setText(mPhoneNumber);}}
    }

    @Override
    protected void getData() {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_register:
//                tvRegister.setBackgroundResource(R.color.white);
                Register();
                break;
            case R.id.tv_login:
                LoginActivity login = (LoginActivity) getActivity();
                login.showFragment(login.getFrmLogin());
                break;

//            case R.id.circle_image:
//                chooseFile();
//                break;
        }
    }


    public void Register(){
        final String email        = edUsername.getText().toString();
        final String password     = edPassword.getText().toString();
        String confirmpassword    = edConfirmPass.getText().toString();
        String name               = edName.getText().toString();
        String phone              = edPhone.getText().toString();
        String address            = edAddres.getText().toString();
        String picture            = null;
        if (bitmap == null) {
            picture = "";
        }else {
            picture = getStringImage(bitmap);
        }

        // Check email, name...
        if (!StringUtil.isValidEmail(email)) {
            edUsername.setFocusable(true);
            edUsername.setError(getString(R.string.email_error));
            return;
        }
        if (StringUtil.isEmpty(email)) {
            edUsername.setFocusable(true);
            edUsername.setError(getString(R.string.email_not_null));
            return;
        }
        if (StringUtil.isEmpty(address)) {
            edAddres.setFocusable(true);
            edAddres.setError(getString(R.string.address_not_null));
            return;
        }
        if (StringUtil.isEmpty(password) && StringUtil.isValidatePassword(password)) {
            edPassword.setFocusable(true);
            edPassword.setError(getString(R.string.password_not_null));
            return;
        }else {
            if (password.equals(confirmpassword) == false) {
                edConfirmPass.setFocusable(true);
                edConfirmPass.setError(getString(R.string.password_is_not_macth));
                return;
            }
        }

        // end check


        ViewDialog dialog = new ViewDialog(getActivity());
        dialog.showDialog();

        APIService apiService   = ApiUtils.getAPIService();
        apiService.Register(name, email, password, phone, address).enqueue(new Callback<ResponeUser>() {
            @Override
            public void onResponse(Call<ResponeUser> call, Response<ResponeUser> response) {
                if (response.isSuccessful()) {
                    LoginActivity login = (LoginActivity) getActivity();
                    login.showFragment(login.getFrmLogin());
                    FragmentLogin frmlogin = login.getFrmLogin();
                    frmlogin.getEdUsername().setText(email);
                    frmlogin.getEdPassword().setText(password);
                    AppUtil.showToast(self, "Register successful");

                }else {
                    AppUtil.showToast(self, "Register erorr");

                }
                dialog.hideDialog();
            }

            @Override
            public void onFailure(Call<ResponeUser> call, Throwable t) {
                dialog.hideDialog();
                Toast.makeText(self, "Register Fail",Toast.LENGTH_SHORT).show();
                Log.e("Fail: " , t.getMessage());
            }
        });



    }

    public String getStringImage(Bitmap btm){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        btm.compress(Bitmap.CompressFormat.JPEG, 40, baos);
        byte[] imageByte = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageByte, Base64.DEFAULT);
        return encodedImage;
    }

    private void chooseFile(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(self.getContentResolver(), uri);
                imgAvatar.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Set Background on touch
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.tv_register:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tvRegister.setBackgroundResource(R.drawable.shap_hover);
                    tvRegister.setTextColor(Color.BLACK);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    tvRegister.setBackgroundResource(R.drawable.shap_border_white);
                    tvRegister.setTextColor(Color.WHITE);
                }
                break;
        }
        return false;
    }
}
