package com.aveplus.avepay_cpocket.restaurant.listener;

/**
 * Created by suusoft.com on 1/29/18.
 */

public interface IOnFragmentNavListener {

    void onFragmentBack();
}
