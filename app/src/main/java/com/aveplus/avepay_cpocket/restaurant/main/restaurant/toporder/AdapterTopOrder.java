package com.aveplus.avepay_cpocket.restaurant.main.restaurant.toporder;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.CartModel;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.RandomFood;
import com.aveplus.avepay_cpocket.restaurant.util.StringUtil;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;


import java.util.ArrayList;

public class AdapterTopOrder extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    public static ArrayList<RandomFood> foodList;
    public static ArrayList<CartModel> cartModelArrayList = new ArrayList<>();
    public static CartModel cartModel;
    private ViewGroup.LayoutParams layoutParams;
    private ItemimgAddListener listener;
    private HomeCallBack homeCallBack;
    private BottomSheetDialog bottomSheetDialog;
    private ImageView cartDecrement, cartIncrement, closeDialog, imgFood;
    private TextView viewCartDialog, quantity, tvName, tvPrice, tvDialogTotal, tvDes;
    private RelativeLayout updateQtyDialog;
    private int count = 0;


    public AdapterTopOrder(Context context, ArrayList<RandomFood> foodList, HomeCallBack mhomeCallBack) {
        this.context = context;
        AdapterTopOrder.foodList = foodList;
        this.homeCallBack = mhomeCallBack;
    }


    public void setListener(ItemimgAddListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.rowtopolder, viewGroup, false);
        return new ItemFood(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        ((ItemFood) viewHolder).Container(foodList.get(i));
        ((ItemFood) viewHolder).lnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onImageclick(i);
            }
        });
        ((ItemFood) viewHolder).imgFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onDetailclick(i);
                }
            }
        });


    }

    // this interface creates for call the invalidateoptionmenu() for refresh the menu item
    public interface HomeCallBack {
        void updateCartCount(Context context);
    }


    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public class ItemFood extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imgAdd;
        private TextView tvNameFood;
        private TextView tvPrice, tvDes;
        private ImageView imgFood;
        private RelativeLayout relativeLayout;
        private LinearLayout lnClick;


        ItemFood(@NonNull View itemView) {
            super(itemView);
            imgAdd = itemView.findViewById(R.id.img_add);
            tvNameFood = itemView.findViewById(R.id.tv_name_food);
            tvPrice = itemView.findViewById(R.id.tv_price);
            imgFood = itemView.findViewById(R.id.img_foodd);
            lnClick = itemView.findViewById(R.id.ln_click);

            relativeLayout = itemView.findViewById(R.id.rl_topoder_item);
            tvDes = itemView.findViewById(R.id.tv_des);
            relativeLayout.setOnClickListener(this);

        }

        void Container(RandomFood randomFood) {
            Glide.with(context).load(randomFood.getAttachment()).into(imgFood);
            tvNameFood.setText(randomFood.getName());
            tvPrice.setText("RM " + StringUtil.formatPrice(randomFood.getPrice()));

            tvDes.setText(randomFood.getDescription());
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.rl_topoder_item:
//                    listener.onAddclick(relativeLayout, getAdapterPosition());
                    break;
                case R.id.img_add:
//                    listener.onAddFoodClick(imgAdd, getAdapterPosition());
                    break;
            }
        }
    }

    public interface ItemimgAddListener {
        void onImageclick(int position);

        void onDetailclick(int position);
    }

}
