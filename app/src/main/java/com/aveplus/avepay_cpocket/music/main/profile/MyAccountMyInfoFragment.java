package com.aveplus.avepay_cpocket.music.main.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.Args;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.listener.IOnFragmentNavListener;
import com.aveplus.avepay_cpocket.music.main.MainActivity;
import com.aveplus.avepay_cpocket.music.main.login.SplashLoginActivity;
import com.aveplus.avepay_cpocket.music.main.login.UserObj;
import com.aveplus.avepay_cpocket.music.model.DataPart;
import com.aveplus.avepay_cpocket.music.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.music.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.music.util.NetworkUtility;
import com.aveplus.avepay_cpocket.music.util.StringUtil;
import com.aveplus.avepay_cpocket.network1.MyProgressDialog;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * BaseFragment
 * Created by Suusoft on 01/12/2016.
 */

public class MyAccountMyInfoFragment extends BaseFragment implements View.OnClickListener, IObserver {
    private static final String TAG = MyAccountMyInfoFragment.class.getName();
    private static final int RC_ADDRESS = 134;
    private static final int RQ_CHANGE_PASS = 234;
    public static final String LOG_OUT = "LOG_OUT";
    private EditText edtBusinessName, edtPhoneNumber, edtAddress, edtEmail;
    private CircleImageView imgAvatar;
    private ImageView imgEditAvatar;
    private ImageView imgSymbolAccount;
    private FrameLayout frDivider;
    private TextViewRegular btnEdit;
    private TextViewRegular tvChangePassword, tvNumRate, btnViewReviews, tvLogout;
    private TextView tvPhoneCode, tvEg;
    private RelativeLayout btnSave;
    private TextView tvFunction;
    private RatingBar rating_bar;
    private boolean isEdit = false;
    private String passWord;
    private String mAddress;
    // toolbar
    protected ImageButton btnBack, btnAction;
    protected TextView tvTitle;
    private IOnFragmentNavListener listener;
    private View.OnFocusChangeListener listenerFocusAddress = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean isFocus) {
            if (isFocus) {
                //    MapsUtil.getAutoCompletePlaces(MyAccountMyInfoFragment.this, RC_ADDRESS);
            }
        }
    };

    public static MyAccountMyInfoFragment newInstance() {
        Bundle args = new Bundle();
        MyAccountMyInfoFragment fragment = new MyAccountMyInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public IOnFragmentNavListener getListener() {
        return listener;
    }

    public void setListener(IOnFragmentNavListener listener) {
        this.listener = listener;
    }

    @Override
    View inflateLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account_about_music, container, false);
    }

    @Override
    void initUI(View view) {
        btnAction = (ImageButton) view.findViewById(R.id.c_btn_action);
        btnBack = (ImageButton) view.findViewById(R.id.c_btn_back);
        tvTitle = (TextView) view.findViewById(R.id.c_tv_title);
        btnAction.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.profile));
        imgAvatar = (CircleImageView) view.findViewById(R.id.img_avatar);
        imgEditAvatar = (ImageView) view.findViewById(R.id.btn_edit_avatar);
        imgSymbolAccount = (ImageView) view.findViewById(R.id.img_symbol_account);
        tvNumRate = (TextViewRegular) view.findViewById(R.id.tv_num_rate);
        rating_bar = (RatingBar) view.findViewById(R.id.rating_bar);
        btnViewReviews = (TextViewRegular) view.findViewById(R.id.btn_view_reviews);

        edtBusinessName = (EditText) view.findViewById(R.id.edt_bussiness_name);
        edtPhoneNumber = (EditText) view.findViewById(R.id.edt_phone_number);
        edtAddress = (EditText) view.findViewById(R.id.edt_address);
        edtEmail = (EditText) view.findViewById(R.id.edt_email);
        tvEg = view.findViewById(R.id.tvEg);

        btnEdit = (TextViewRegular) view.findViewById(R.id.btn_edit_infomation);
        tvChangePassword = (TextViewRegular) view.findViewById(R.id.tv_change_password);
        //  tvLogout = (TextViewRegular) view.findViewById(R.id.tv_logout);
        btnSave = (RelativeLayout) view.findViewById(R.id.btn_functions);
        tvFunction = (TextView) view.findViewById(R.id.tv_btn);
        btnSave.setVisibility(View.GONE);
        tvFunction.setText(getString(R.string.button_save));

        frDivider = (FrameLayout) view.findViewById(R.id.fr_divider);
        tvPhoneCode = (TextView) view.findViewById(R.id.tv_phone_code);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onFragmentBack();
            }
        });
        getProfile();
//        passWord = DataStoreManager.getUser().getPassWord();


    }

    @Override
    void initControl() {
        enableEdit(false);

        imgEditAvatar.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        tvChangePassword.setOnClickListener(this);
        //tvLogout.setOnClickListener(this);
        btnViewReviews.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        tvPhoneCode.setOnClickListener(this);
//        edtAddress.setTag(edtAddress.getKeyListener());
////        edtAddress.setKeyListener(null);
//        edtAddress.setOnClickListener(this);
//        edtAddress.setOnFocusChangeListener(listenerFocusAddress);

        frDivider.setVisibility(View.VISIBLE);


        setData(DataStoreManager.getUser());
        edtEmail.setEnabled(false);
        edtBusinessName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT) {
                    edtEmail.setFocusable(false);
                    edtPhoneNumber.requestFocus();
                }
                return false;
            }
        });
        if (NetworkUtility.isNetworkAvailable()) {
            getProfile();
        } else {
            AppUtil.showToast(getActivity(), R.string.msg_network_not_available);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnEdit) {
            isEdit = !isEdit;
            if (DataStoreManager.getUser() != null) {
                if (isEdit) {
//                if (DataStoreManager.getUser().getAvatar().startsWith("http://iwanadeal.com") || DataStoreManager.getUser().getAvatar().isEmpty()) {
                    imgEditAvatar.setVisibility(View.VISIBLE);
//
//                } else {
//                    imgEditAvatar.setVisibility(View.GONE);
//
//                }
//                edtAddress.setOnFocusChangeListener(listenerFocusAddress);
                    btnSave.setVisibility(View.VISIBLE);
                    btnEdit.setBackgroundResource(R.drawable.bg_pressed_radius_accent);
                    edtBusinessName.requestFocus();
                    tvEg.setVisibility(View.VISIBLE);
                } else {
                    setData(DataStoreManager.getUser());
                    imgEditAvatar.setVisibility(View.GONE);
                    btnSave.setVisibility(View.GONE);
                    btnEdit.setBackgroundResource(R.drawable.bg_pressed_radius_grey);
                    //edtAddress.setOnFocusChangeListener(null);
                    tvEg.setVisibility(View.GONE);
                }
                enableEdit(isEdit);
            } else {
                showDialogLogout();
            }
        } else if (view == btnSave) {
            if (NetworkUtility.isNetworkAvailable()) {
                if (isEdit) {
                    if (isValid()) {

                        updateProfile();

                    }
                } else {
                    AppUtil.showToast(getActivity(), R.string.msg_enable_edit);
                }

            } else {
                AppUtil.showToast(getActivity(), R.string.msg_no_network);
            }

        } else if (view == tvChangePassword) {
//            Intent intent = new Intent(getActivity(), ChangePassWordActivity.class);
//            startActivityForResult(intent, RQ_CHANGE_PASS);
        }
//        else if(view == tvLogout){
//            if (DataStoreManager.getUser() != null) {
//                logout();
//            } else {
//                AppUtil.showToast(self, "No Account");
//            }
//        }
        else if (view == imgEditAvatar) {
            AppUtil.pickImage(self, AppUtil.PICK_IMAGE_REQUEST_CODE);
        } else if (view == btnViewReviews) {
//            Bundle bundle = new Bundle();
//            bundle.putString(Args.USER_ID, "");
//            bundle.putString(Args.NAME_DRIVER,"");
//            bundle.putString(Args.I_AM, Constants.SELLER);
//            Intent intent = new Intent(getApplicationContext(), ViewReviewsActivity.class);
//            intent.putExtras(bundle);
            //     AppUtil.startActivity(getActivity(), ViewReviewsActivity.class);
        } else if (view == tvPhoneCode) {
//            Intent intent = new Intent(getActivity(), PhoneCountryListActivity.class);
//            startActivityForResult(intent, Args.RQ_GET_PHONE_CODE);
        } else if (view == edtAddress) {
            // MapsUtil.getAutoCompletePlaces(this, RC_ADDRESS);
        }
    }


    private void showDialogLogout() {
        DialogUtil.showAlertDialog(self, R.string.notification, R.string.you_need_login, new DialogUtil.IDialogConfirm2() {
            @Override
            public void onClickOk() {
                goLogin();
            }


        });
    }

    private void goLogin() {
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.LOG_OUT, "logout");
        AppUtil.startActivity(self, SplashLoginActivity.class, bundle);
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("resultCode", "resultCode = " + resultCode);
        if (requestCode == RQ_CHANGE_PASS) {
            if (resultCode == Activity.RESULT_OK) {
//                String pass = data.getStringExtra(ChangePassWordActivity.PASS);
//                UserObj userObj = DataStoreManager.getUser();
//                userObj.setPassWord(pass);
//                DataStoreManager.saveUser(userObj);

            }
        }
        if (requestCode == AppUtil.PICK_IMAGE_REQUEST_CODE) {

            if (resultCode == Activity.RESULT_OK) {
                AppUtil.setImageFromUri(imgAvatar, data.getData());
            }
        } else if (requestCode == Args.RQ_GET_PHONE_CODE && resultCode == Activity.RESULT_OK) {
            String countryCodeSelected = data.getExtras().getString(Args.KEY_PHONE_CODE);
            tvPhoneCode.setText(countryCodeSelected);
        }


    }

    private boolean isValid() {
        String bussinessName = edtBusinessName.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        String phone = edtPhoneNumber.getText().toString().trim();
        if (StringUtil.isEmpty(bussinessName)) {
            AppUtil.showToast(getActivity(), R.string.msg_fill_full_name);
            edtBusinessName.requestFocus();
            return false;
        }
        if (StringUtil.isEmpty(phone)) {
            AppUtil.showToast(getActivity(), R.string.msg_phone_is_required);
            edtPhoneNumber.requestFocus();
            return false;
        }
        if (StringUtil.isEmpty(address)) {
            AppUtil.showToast(getActivity(), R.string.msg_address_is_required);
            edtAddress.requestFocus();
            return false;
        }
        return true;
    }

    private void enableEdit(boolean isEdit) {
        edtBusinessName.setEnabled(isEdit);
        edtAddress.setEnabled(isEdit);
        edtPhoneNumber.setEnabled(isEdit);
        tvPhoneCode.setEnabled(isEdit);
    }

    private void setData(UserObj userObj) {
        if (DataStoreManager.getUser() != null) {


            tvNumRate.setText(String.valueOf(userObj.getTotal_rate_count()));
            rating_bar.setRating(userObj.getAvg_rate());
            ImageUtil.setImage(getActivity(), imgAvatar, userObj.getAvatar(), 600, 600);
            edtBusinessName.setText(userObj.getName());
            edtPhoneNumber.setText(userObj.getPhoneNumber());

            if (userObj.getPhoneCode().isEmpty()) {
                getCountryCode();
            } else {
                tvPhoneCode.setText(userObj.getPhoneCode());
            }
            edtAddress.setText(userObj.getAddress());
            edtEmail.setText(userObj.getEmail());
//        if (userObj.isSecured()) {
            tvChangePassword.setText(getString(R.string.change_pass));
//        } else {
//            tvChangePassword.setText(getString(R.string.create_pass));
//        }
        }
    }

    private void getCountryCode() {
        String[] rl = getResources().getStringArray(R.array.CountryCodes);
        // int curPosition = AppUtil.getCurentPositionCountryCode(getActivity());
        int curPosition = 0;
        String phoneCode = rl[curPosition].split(",")[0];
        tvPhoneCode.setText(phoneCode);
    }

    private void updateProfile() {
        String id = DataStoreManager.getUser().getId();
        final String passWord = DataStoreManager.getUser().getPassWord();
        String email = DataStoreManager.getUser().getEmail();
        final String bussinessName = edtBusinessName.getText().toString().trim();
        final String address = edtAddress.getText().toString().trim();
        String phoneNumber = edtPhoneNumber.getText().toString().trim();
        String phoneCode = tvPhoneCode.getText().toString().trim();
        final String phone = phoneCode + " " + phoneNumber;
        DataPart avatar = null;
        if (imgAvatar.getDrawable() != null) {
            avatar = new DataPart("avatar.png", AppUtil.getFileDataFromDrawable(getActivity(), imgAvatar.getDrawable()), DataPart.TYPE_IMAGE);
        }

        ModelManager.updateProfile(self, avatar, id, bussinessName, address, phone, email, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                try {
                    JSONObject jsonObject = new JSONObject(object.toString());
                    ApiResponse response = new ApiResponse(jsonObject);
                    Log.e("hihi", " 2" + response.toString());

                    if (!response.isError()) {
                        UserObj userObj = response.getDataObject(UserObj.class);
                        //userObj.setToken(DataStoreManager.getToken());
                        //userObj.setRememberMe(DataStoreManager.getUser().isRememberMe());
                        userObj.setName(bussinessName);
                        userObj.setAddress(address);
                        userObj.setPhone(phone);
                        userObj.setPassWord(passWord);
                        DataStoreManager.saveUser(userObj);
                        AppController.getInstance().setUserUpdated(true);
                        enableEdit(false);
                        btnEdit.setBackgroundResource(R.drawable.bg_pressed_radius_grey);
                        isEdit = false;
                        btnSave.setVisibility(View.GONE);
                        imgEditAvatar.setVisibility(View.GONE);
                        AppUtil.showToast(getActivity(), R.string.msg_update_success);
                        setData(userObj);
                        ((MainActivity) getActivity()).setTitle(userObj.getName());
                        //  ((MainActivity) getActivity()).updateMenuLeftHeader();

//                        AddressManager.getInstance().getArray().clear();
//                        AddressManager.getInstance().addItem(new Person(bussinessName, phone, address));
                        Log.e("hihi", "3: " + userObj.toString());
                    } else {
                        Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    private void getProfile() {
        if (DataStoreManager.getUser() != null) {
            String id = DataStoreManager.getUser().getId();
            final String passWord = DataStoreManager.getUser().getPassWord();

            String address = DataStoreManager.getUser().getAddress();
            String phone = DataStoreManager.getUser().getPhone();
            String name = DataStoreManager.getUser().getName();
            if (passWord != null) {
                ModelManager.getProfile(getActivity(), id, passWord, address, phone, name, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        try {
                            JSONObject jsonObject = new JSONObject(object.toString());
                            ApiResponse response = new ApiResponse(jsonObject);

                            if (!response.isError()) {
                                UserObj userObj = response.getDataObject(UserObj.class);
                                userObj.setToken(DataStoreManager.getUser().getToken());
                                userObj.setPassWord(passWord);
                                userObj.setRememberMe(DataStoreManager.getUser().isRememberMe());
                                DataStoreManager.saveUser(userObj);
                                AppController.getInstance().setUserUpdated(true);
                                setData(userObj);
                            } else {
                                Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
            }
        }
    }


    @Override
    public void update() {
        setData(DataStoreManager.getUser());
    }

    private void logout() {
        showDialogLogout2();


    }

    private void showDialogLogout2() {
        DialogUtil.showAlertDialog(self, R.string.log_out, R.string.you_wanto_logout, new DialogUtil.IDialogConfirm2() {
            @Override
            public void onClickOk() {
                requestLogout();
            }
        });
    }

    private void requestLogout() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            final MyProgressDialog progressDialog = new MyProgressDialog(self);
            progressDialog.show();
            //  processBeforeLoggingOut(progressDialog);

        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }

//    private void processBeforeLoggingOut(MyProgressDialog progressDialog) {
//        if (DataStoreManager.getUser() != null ) {
//            if (DataStoreManager.getUser().getDriverData().isAvailable()) {
//                LocationService.start(self, LocationService.STOP_REQUESTING_LOCATION);
//
//                // Deactivate driver's mode before logging out
//                ModelManager.activateDriverMode(self, Constants.OFF, 0, new ModelManagerListener() {
//                    @Override
//                    public void onSuccess(Object object) {
//                    }
//
//                    @Override
//                    public void onError() {
//                    }
//                });
//            }
//        }
//
//        if (progressDialog.isShowing()) {
//            progressDialog.dismiss();
//        }
//
//        DataStoreManager.removeUser();
//        AddressManager.getInstance().getArray().clear();
//        AppController.getInstance().setUserUpdated(true);
//        Bundle bundle = new Bundle();
//        bundle.putString(LOG_OUT, "logout");
////        AppUtil.startActivity(self, SplashLoginActivity.class, bundle);
////        finish();
//
//
//        //ImageUtil.setImage(self, imgAvatar, null);
//        imgAvatar.setImageDrawable(null);
//
//    }
}
