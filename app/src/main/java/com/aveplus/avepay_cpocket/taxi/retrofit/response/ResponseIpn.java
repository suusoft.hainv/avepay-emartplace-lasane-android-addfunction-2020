package com.aveplus.avepay_cpocket.taxi.retrofit.response;

public class ResponseIpn extends BaseModel {

    public IPN data;

    public class IPN {
        public String user_id;
        public String reference;
        public String amount;
        public String msisdn;
        public String billmapTransactionId;
        public String ewpTransactionId;
        public String responseCode;
        public String responseMessage;
    }
}
