package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ProductHistoryAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.OrderHistory;
import com.aveplus.avepay_cpocket.trainingcenter.object.ProductHistory;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import org.json.JSONException;

import java.util.ArrayList;

public class DetailOrderHistory extends BaseFragment {

    private View v;
    private TextView tvOrderId, tvFullName, tvEmail, tvPaymentMethod,
            tvStatus, tvTotal, tvVAT, tvCreateDate;
    private ListView lvProductHistory;
    ProductHistoryAdapter adapter;

    com.aveplus.avepay_cpocket.trainingcenter.object.DetailOrderHistory detailHistory;

    String orderId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.detail_order_history, container, false);

        initUI();
        getDetailHistory();
        return v;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("detail_order_history");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("orderHistory");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    private void initUI() {

        tvOrderId = (TextView) v.findViewById(R.id.tvOrderId);
        tvFullName = (TextView) v.findViewById(R.id.tvFullName);
        tvEmail = (TextView) v.findViewById(R.id.tvEmail);
        tvPaymentMethod = (TextView) v.findViewById(R.id.tvPaymentMethod);
        tvStatus = (TextView) v.findViewById(R.id.tvStatus);
        tvTotal = (TextView) v.findViewById(R.id.tvTotal);
        tvVAT = (TextView) v.findViewById(R.id.tvVAT);
        tvCreateDate = (TextView) v.findViewById(R.id.tvCreateDate);

        lvProductHistory = (ListView) v.findViewById(R.id.lvProductHistory);
        justifyListViewHeightBasedOnChildren(lvProductHistory);
    }


    private void getDetailHistory() {
        Bundle bundle = this.getArguments();
        orderId = bundle.getString("orderID");
        ModelManager.getDetailHistory(orderId, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                detailHistory = new com.aveplus.avepay_cpocket.trainingcenter.object.DetailOrderHistory();
                try {
                    detailHistory = ParseUtility.parseDetailHistory(response.getRootStr());
                    if (detailHistory != null) {
                        tvOrderId.setText(detailHistory.getOrderId());
                        tvFullName.setText(detailHistory.getFullName());
                        tvEmail.setText(detailHistory.getEmail());
                        tvPaymentMethod.setText(detailHistory.getPaymentMethod());
//								tvStatus.setText(detailHistory.getStatus());
                        if (detailHistory.getStatus().equals(OrderHistory.ORDER_STATUS_BOOKED)) {
                            tvStatus.setText("BOOKED");
                        } else if (detailHistory.getStatus().equals(OrderHistory.ORDER_STATUS_CANCEL)) {
                            tvStatus.setText("CANCEL");
                        } else if (detailHistory.getStatus().equals(OrderHistory.ORDER_STATUS_REJECT)) {
                            tvStatus.setText("REJECT");
                        } else if (detailHistory.getStatus().equals(OrderHistory.ORDER_STATUS_NEW)) {
                            tvStatus.setText("NEW");
                        } else if (detailHistory.getStatus().equals(OrderHistory.ORDER_STATUS_IN_PROCESS)) {
                            tvStatus.setText("IN PROCESS");
                        } else if (detailHistory.getStatus().equals(OrderHistory.ORDER_STATUS_CLOSED)) {
                            tvStatus.setText("CLOSED");
                        }
                        tvTotal.setText("$" + detailHistory.getTotal());
                        tvVAT.setText(detailHistory.getVat() + "%");
                        tvCreateDate.setText(detailHistory.getCreateDate());

                        ArrayList<ProductHistory> lisProHistory = new ArrayList<>();
                        lisProHistory = detailHistory.getListProHistory();

                        if (detailHistory != null) {
                            adapter = new ProductHistoryAdapter(getActivity(), R.layout.item_product_history,
                                    lisProHistory);
                            lvProductHistory.setAdapter(adapter);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Not found", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

    public void justifyListViewHeightBasedOnChildren(ListView listView) {

        adapter = (ProductHistoryAdapter) listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }

}
