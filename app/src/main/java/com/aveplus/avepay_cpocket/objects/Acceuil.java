package com.aveplus.avepay_cpocket.objects;

public class Acceuil {
    private String name;
    private int image;

    public Acceuil(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public Acceuil() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
