package com.aveplus.avepay_cpocket.restaurant.main.notification.object;
/**
 * Created by suusoft.com on 11/12/19.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataFCM {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("ime")
    @Expose
    private String ime;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("gcm_id")
    @Expose
    private String gcmId;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }}
