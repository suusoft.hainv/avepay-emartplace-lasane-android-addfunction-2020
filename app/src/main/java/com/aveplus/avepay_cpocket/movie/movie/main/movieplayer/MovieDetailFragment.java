package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer;


import android.app.Fragment;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MovieDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class MovieDetailFragment extends Fragment implements View.OnLayoutChangeListener{
    private final String TAG = getClass().getSimpleName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "movie";
    private static final String ARG_PARAM2 = "param2";

    private String mParam2;

    private NetworkImageView mPosterImgView;
    private TextView mNameTxtView, mYearTxtView, mImdbTxtView, mDirectorTxtView, mCastsTxtView ,mPlotTxtView;

    private ImageLoader mImageLoader;

    private Movie mCurrentMovie;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment MovieDetailFragment.
     */
    public static MovieDetailFragment newInstance(Movie movie) {
        MovieDetailFragment fragment = new MovieDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, movie);
        fragment.setArguments(args);
        return fragment;
    }
    public MovieDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageLoader = RequestManager.getImageLoader();

        if (getArguments() != null) {
            mCurrentMovie = getArguments().getParcelable(ARG_PARAM1);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_movie_detail, container, false);

        mPosterImgView = (NetworkImageView) view.findViewById(R.id.imgview_poster);
        mNameTxtView = (TextView) view.findViewById(R.id.txtview_name_movie);
        mYearTxtView = (TextView) view.findViewById(R.id.txtview_year_movie);
        mImdbTxtView = (TextView) view.findViewById(R.id.txtview_imdb_movie);
        mDirectorTxtView = (TextView) view.findViewById(R.id.txtview_director_movie);
        mPlotTxtView = (TextView) view.findViewById(R.id.txtview_plot_movie);
        mCastsTxtView = (TextView) view.findViewById(R.id.txtview_casts_movie);
        // change font for texts
        AssetManager assetManager = getActivity().getAssets();
        mNameTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));
        mYearTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));
        mImdbTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));
        mDirectorTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));
        mCastsTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));
        mPlotTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaLTStd-Light.otf"));

        mPosterImgView.addOnLayoutChangeListener(this);
        mPosterImgView.setDefaultImageResId(R.drawable.poster);
        mPosterImgView.setErrorImageResId(R.drawable.poster);

        loadDetailOfMovie(mCurrentMovie);

        return view;
    }

    public void setInfor(Movie movie){
        mCurrentMovie  = movie;
        if (getArguments() != null) {
            getArguments().putParcelable(ARG_PARAM1, movie);
        }
        loadDetailOfMovie(movie);
    }

    private void loadDetailOfMovie(Movie movie) {
        if(movie == null)
            movie = new Movie();
        Spannable nameSpan = new SpannableString(movie.getMovie_name());
        nameSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, nameSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mNameTxtView.setText(nameSpan);

        Spannable yearSpan = new SpannableString("YEAR: "+movie.getMovie_year());
        yearSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 0, 210, 255)), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mYearTxtView.setText(yearSpan);

        Spannable imdbSpan = new SpannableString("IMDB: "+ movie.getMovie_rating());
        imdbSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 238, 177, 30)), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mImdbTxtView.setText(imdbSpan);

        Spannable directorSpan = new SpannableString("Director: "+ movie.getMovie_director());
        directorSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 0, 210, 255)), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mDirectorTxtView.setText(directorSpan);

        Spannable catstSpan = new SpannableString("Actors: "+ movie.getMovie_actor());
        catstSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 0, 210, 255)), 0, 6, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mCastsTxtView.setText(catstSpan);

        mPlotTxtView.setTextColor(Color.WHITE);
        mPlotTxtView.setText(movie.getMovie_desc());

        mPosterImgView.setImageUrl(movie.getMovie_thumb(), mImageLoader);
    }


    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        // Change size of Poster ImageView
        int posterHeight = mPosterImgView.getMeasuredHeight();
        ViewGroup.LayoutParams layoutParams = mPosterImgView.getLayoutParams();
        layoutParams.width = (int)(0.7f * posterHeight);
        mPosterImgView.setLayoutParams(layoutParams);
    }
}
