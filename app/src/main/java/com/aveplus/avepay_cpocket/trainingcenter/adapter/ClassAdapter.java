package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.Class;

import java.util.ArrayList;

/**
 * Created by suusoft on 29/08/2017.
 */

public class ClassAdapter extends BaseAdapter {
    private ArrayList<Class> mListClass;

    public ClassAdapter(ArrayList<Class> mListClass) {
        this.mListClass = mListClass;
    }

    @Override
    public int getCount() {
        return mListClass.size();
    }

    @Override
    public Object getItem(int position) {
        return mListClass.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_class, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Class aClass = mListClass.get(position);
        viewHolder.tvNameClass.setText(aClass.getNameClass());
        viewHolder.tvDuration.setText(aClass.getmStartDate() + " - " + aClass.getmEndDate());
        return convertView;
    }

    public class ViewHolder {
        private TextView tvNameClass;
        private TextView tvDuration;
        private View view;

        public ViewHolder(View view) {
            this.view = view;
            initUI();
        }

        private void initUI() {
            tvNameClass = (TextView) findViewById(R.id.tvWeek);
            tvDuration = (TextView) findViewById(R.id.tvDuration);
        }

        private View findViewById(int idRes) {
            return view.findViewById(idRes);
        }
    }
}
