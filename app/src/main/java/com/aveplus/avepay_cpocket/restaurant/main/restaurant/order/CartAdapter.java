package com.aveplus.avepay_cpocket.restaurant.main.restaurant.order;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.MycartActivity.grandTotalplus;
import static com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.MycartActivity.temparraylist;
import static com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.MycartActivity.tvTotal;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    private ArrayList<CartModel> cartModels;
    private Context context;
    private ProgressDialog progressDialog;

    public CartAdapter(ArrayList<CartModel> cartModels, Context context) {
        this.cartModels = cartModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart,
                viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.tvPrice.setText(cartModels.get(i).getPrice() + "$");
        viewHolder.tvName.setText(cartModels.get(i).getName());
        viewHolder.tvRestaurant.setText(cartModels.get(i).getDescription());
//        viewHolder.tvQtt.setText(String.valueOf(cartModels.get(i).getQuantity()));
        viewHolder.tvQuantity.setText(String.valueOf(cartModels.get(i).getQuantity()));
        Glide.with(context).load(cartModels.get(i).getImage()).into(viewHolder.imgFood);

        //for remove single item in cart and update the total value and list
        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage("Loading...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Alert");
                builder.setMessage(R.string.are_you_sure_want_to_delete);
                builder.setCancelable(false);
                builder.setIcon(R.drawable.bazar_ramadan_text_logo);
                builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int k) {

                    }
                });


                // remove item cart
                builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int k) {

                        if (cartModels.size() == 1) {
                            cartModels.remove(i);
                            notifyItemRemoved(i);
                            notifyItemRangeChanged(i, cartModels.size());
                            grandTotalplus = 0;
                            tvTotal.setText("$" + grandTotalplus);
                        }

                        if (cartModels.size() > 0) {
                            cartModels.remove(i);
                            notifyItemRemoved(i);
                            notifyItemRangeChanged(i, cartModels.size());
                            grandTotalplus = 0;
                            for (int j = 0; j < temparraylist.size(); j++) {
                                grandTotalplus = grandTotalplus + temparraylist.get(j).getTotalCash();
                            }

                            Log.d("totalcashthegun", String.valueOf(grandTotalplus));
                            tvTotal.setText("$" + grandTotalplus);
                        } else {

                        }

                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();


            }
        });


        // increment quantity and update quamtity and total cash
        viewHolder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // totalcsh = 0
                grandTotalplus = 0;
                final int[] cartCounter = {1};
                int cartUpdateCounter = (cartModels.get(i).getQuantity());
                Log.d("counterthegun", String.valueOf(cartModels.get(i).getQuantity()));
                cartUpdateCounter += 1;

                cartModels.get(i).setQuantity(cartUpdateCounter);
                int cash = (cartModels.get(i).getPrice() * (cartModels.get(i).getQuantity()));

                viewHolder.tvQuantity.setText(String.valueOf(cartModels.get(i).getQuantity()));

                cartModels.get(i).setTotalCash(cash);
                viewHolder.tvPrice.setText(String.valueOf(cartModels.get(i).getPrice()));

                for (int j = 0; j < temparraylist.size(); j++) {
                    grandTotalplus = grandTotalplus + temparraylist.get(j).getTotalCash();
                }

                Log.e("Quantity", "onClick: " + cartModels.get(i).getQuantity());
                tvTotal.setText(String.valueOf(grandTotalplus));
                notifyDataSetChanged();
            }
        });


        // decrement quantity and update quamtity and total cash
        viewHolder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //total_cash=0;
                grandTotalplus = 0;
                viewHolder.btnMinus.setEnabled(true);

                int cartUpdateCounter = (cartModels.get(i).getQuantity());
                Log.d("counterthegun", String.valueOf(cartModels.get(i).getQuantity()));

                if (cartUpdateCounter == 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Alert");
                    builder.setMessage(R.string.are_you_sure_want_to_delete);
                    builder.setCancelable(false);
                    builder.setIcon(R.drawable.bazar_ramadan_text_logo);
                    builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int k) {

                        }
                    });


                    // remove item cart
                    builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int k) {

                            if (cartModels.size() == 1) {
                                cartModels.remove(i);
                                notifyItemRemoved(i);
                                notifyItemRangeChanged(i, cartModels.size());
                                grandTotalplus = 0;
                                tvTotal.setText(String.valueOf(grandTotalplus));
                            }

                            if (cartModels.size() > 0) {
                                cartModels.remove(i);
                                notifyItemRemoved(i);
                                notifyItemRangeChanged(i, cartModels.size());
                                grandTotalplus = 0;
                                for (int j = 0; j < temparraylist.size(); j++) {
                                    grandTotalplus = grandTotalplus + temparraylist.get(j).getTotalCash();
                                }

                                Log.d("totalcashthegun", String.valueOf(grandTotalplus));
                                tvTotal.setText(String.valueOf(grandTotalplus));
                            } else {

                            }
                            notifyDataSetChanged();

                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                } else {
                    viewHolder.btnMinus.setEnabled(true);
                    cartUpdateCounter -= 1;
                    cartModels.get(i).setQuantity((cartUpdateCounter));

                    int cash = cartModels.get(i).getPrice() * cartModels.get(i).getQuantity();

                    viewHolder.tvQuantity.setText(String.valueOf(cartModels.get(i).getQuantity()));

                    cartModels.get(i).setTotalCash(cash);
                    for (int i = 0; i < temparraylist.size(); i++) {
                        grandTotalplus = grandTotalplus + temparraylist.get(i).getTotalCash();
                    }

                    Log.d("totalcashthegun", String.valueOf(grandTotalplus));
                    tvTotal.setText(String.valueOf(grandTotalplus));
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvRestaurant, tvQuantity, tvPrice;
        private ImageView imgFood, imgDelete;
        private ImageView btnMinus, btnPlus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_namefood_cart);
            tvRestaurant = itemView.findViewById(R.id.tv_cart_restaurant);
            tvQuantity = itemView.findViewById(R.id.tv_amount);
            tvPrice = itemView.findViewById(R.id.tv_cart_price);
            imgFood = itemView.findViewById(R.id.img_cart_item);
            btnMinus = itemView.findViewById(R.id.btn_minus);
            btnPlus = itemView.findViewById(R.id.btn_plus);
            imgDelete = itemView.findViewById(R.id.img_delete);
        }


    }
}
