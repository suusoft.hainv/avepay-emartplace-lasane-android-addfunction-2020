package com.aveplus.avepay_cpocket.ebook.view.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.configs.CheckPermission;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.configs.DefaultConfig;
import com.aveplus.avepay_cpocket.ebook.listener.IConfirmation;
import com.aveplus.avepay_cpocket.ebook.model.Category;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;
import com.aveplus.avepay_cpocket.ebook.network.NetworkUtility;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;

import java.io.File;
import java.util.ArrayList;

import static com.aveplus.avepay_cpocket.ebook.configs.CheckPermission.RC_PERMISSIONS;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Check permissions
        if (CheckPermission.isGranted(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                RC_PERMISSIONS, "")) {
            onContinue();
        }

    }

    private void onContinue() {
        checkFirstSettingApp();
        if (NetworkUtility.isNetworkAvailable()) {
            getCategories();

        } else {
            startHomeWithOffLineMode();
            AppUtil.showToast(SplashActivity.this, R.string.msg_connection_network_error);

        }
    }

    private void startHomeWithOffLineMode() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constant.OFFLINE_MODE, true);
        AppUtil.startActivityLTR(SplashActivity.this, MainActivity.class, bundle);
        finish();
    }

    private void startHome() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AppUtil.startActivityLTR(SplashActivity.this, MainActivity.class);
                finish();
            }
        }, 1500);
    }

    private void getCategories() {
        RequestManager.getCategory(this, new ApiManager.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                ArrayList<Category> list = (ArrayList<Category>) response.getDataList(Category.class);
                AppController.getInstance().setListCategories(list);
                startHome();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(SplashActivity.this, R.string.msg_connection_network_error);
                Log.e("SplashActivity", "can not get categoires");
            }
        });
    }

    /**
     * check first setting app
     */
    private void checkFirstSettingApp() {
        if (!DataStoreManager.getFirstInstall()) {
            Log.e("FIRST INSTALL APP:", "First install app");
            DataStoreManager.saveFirstInstall(true);
            DataStoreManager.saveListType(DefaultConfig.listType);
            deleteFolderCache();
        }
    }

    private void deleteFolderCache() {
        File file = new File(Environment.getExternalStorageDirectory() + Constant.FOLDER_EBOOK);
        deleteRecursive(file);
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        fileOrDirectory.delete();
    }


    //result

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSIONS: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED
                            || grantResults[1] == PackageManager.PERMISSION_DENIED) {
                        showPermissionsReminder(RC_PERMISSIONS, true);
                    } else {
                        //permission đã được cấp phép
                        onContinue();
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    protected void showPermissionsReminder(final int reqCode, final boolean flag) {
        CheckPermission.showConfirmationDialog(this, getString(R.string.msg_remind_user_grants_permissions),
                getString(R.string.allow), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        CheckPermission.isGranted(SplashActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, reqCode, null);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            finish();
                        }
                    }
                });
    }
}
