package com.aveplus.avepay_cpocket.utils.toolbox;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class CustomStringRequest extends StringRequest {

	public CustomStringRequest(int method, String url,
                               Listener<String> listener, Response.ErrorListener errorListener) {
		super(method, url, listener, errorListener);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response) {
		// TODO Auto-generated method stub
		StringBuilder contents = new StringBuilder();
		try {
			InputStreamReader reader = new InputStreamReader(
					new ByteArrayInputStream(response.data));
			BufferedReader br = new BufferedReader(reader);

			String line = null;
			
			while ((line = br.readLine()) != null) {
				contents.append(line);
				contents.append("\n");
			}
			reader.close();
			br.close();
			
		} catch (IOException e) {
			return Response.error(new ParseError());
		}
		return Response.success(contents.toString(),
                HttpHeaderParser.parseCacheHeaders(response));
	}
	
	
}
