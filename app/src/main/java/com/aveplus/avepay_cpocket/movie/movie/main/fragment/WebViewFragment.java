package com.aveplus.avepay_cpocket.movie.movie.main.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.movie.movie.configs.Constant;


/**
 * Copyright © 2019 SUUSOFT
 */

public class WebViewFragment extends BaseFragment {

    private static final String TAG = WebViewFragment.class.getSimpleName();
    private WebView webView;
    private String url;
    String title;

    public static WebViewFragment newInstance(Bundle args) {


        WebViewFragment fragment = new WebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_web_view;
    }

    @Override
    protected void init() {
        Bundle bundle = getArguments();
        title = bundle.getString(Constant.KEY_TITLE);

        url = bundle.getString(Constant.KEY_URL);
        Log.e(TAG, "init: "+url );
    }

    @Override
    protected void initView(View view) {
        webView = (WebView) view.findViewById(R.id.web_view);
        setUpWebView(webView);
    }

    @Override
    protected void getData() {

    }

    private void setUpWebView(WebView webView) {
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                return super.shouldOverrideUrlLoading(view, request);
            }
        });

    }
}
