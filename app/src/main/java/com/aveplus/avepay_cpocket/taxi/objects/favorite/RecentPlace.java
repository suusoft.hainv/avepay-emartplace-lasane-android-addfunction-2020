package com.aveplus.avepay_cpocket.taxi.objects.favorite;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class RecentPlace implements Parcelable {

    private ArrayList<Location> listPlaces;


    public RecentPlace() {
        listPlaces = new ArrayList<>();
    }

    protected RecentPlace(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RecentPlace> CREATOR = new Creator<RecentPlace>() {
        @Override
        public RecentPlace createFromParcel(Parcel in) {
            return new RecentPlace(in);
        }

        @Override
        public RecentPlace[] newArray(int size) {
            return new RecentPlace[size];
        }
    };

    public ArrayList<Location> getListPlaces() {
        if (listPlaces==null)
            listPlaces = new ArrayList<>();
        return listPlaces;
    }

    public void setListPlaces(ArrayList<Location> listPlaces) {
        this.listPlaces = listPlaces;
    }
}
