package com.aveplus.avepay_cpocket.music.retrofit;



import com.aveplus.avepay_cpocket.music.main.video2.DataVideo;
import com.aveplus.avepay_cpocket.music.main.video2.ResponeMoreVideo;
import com.aveplus.avepay_cpocket.music.main.video2.ResponseCategory;
import com.aveplus.avepay_cpocket.music.retrofit.param.Param;
import com.aveplus.avepay_cpocket.music.retrofit.response.ResponseLogin;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

import static com.aveplus.avepay_cpocket.music.retrofit.param.Param.PARAM_PASSWORD;


public interface APIService {


    @GET("user/login")
    @Headers("Cache-Control: no-cache")
        // no cache
    Call<ResponseLogin> login(@Query(Param.PARAM_USERNAME) String email,
                              @Query(PARAM_PASSWORD) String password,
                              @Query("login_type") String type

    );


    @GET("user/dangky")
    @Headers("Cache-Control: no-cache")
        // no cache
    Call<ResponseLogin> register(@Query(Param.PARAM_NAME) String fullname,
                                 @Query(Param.PARAM_USERNAME) String email,
                                 @Query(PARAM_PASSWORD) String password

    );

    @GET("home/video-statistics")
    @Headers("Cache-Control: no-cache")
        // no cache
    Call<DataVideo> getListVideo();
   @GET("video/view-more?page=1&number_per_page=20")
    @Headers("Cache-Control: no-cache")
        // no cache
   Call<ResponseCategory> getListCategory();


    @GET("video/title-view-more?page=1&number_per_page=20")
    @Headers("Cache-Control: no-cache")
        // no cache
    Call<ResponeMoreVideo> getVideo(@Query(Param.PARAM_VIDEO_TITLE_ID) String id);

@GET("video/category")
    @Headers("Cache-Control: no-cache")
        // no cache
Call<ResponeMoreVideo> getVideoCate(@Query(Param.PARAM_VIDEO_CATEGORY_ID) String id);

}
