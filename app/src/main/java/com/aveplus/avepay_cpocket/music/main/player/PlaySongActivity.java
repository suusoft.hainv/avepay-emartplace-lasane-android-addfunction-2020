package com.aveplus.avepay_cpocket.music.main.player;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.music.base.view.SingleListFragment;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.IOnMenuItemClick;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.MusicService;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.util.BindingUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import me.relex.circleindicator.CircleIndicator;

public class PlaySongActivity extends BaseActivity implements View.OnClickListener {
    private static final long PROGRESS_UPDATE_INTERNAL = 1000;
    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;
    // player
    private boolean check = false;
    private LinearLayout dragView;
    private ImageView pImgBg;

    private TextView tvStartTime, tvEndTime, tvTitlePlayer, tvArtists;
    private ImageButton btnPlay, btnNext, btnPrev, btnShuffer, btnRepeat, btnBackPlayer, btnActionPlayer;
    private SeekBar seekBarPlayMusic;
    private ViewPager vpgMusicPlayer;
    private CircleIndicator circleIndicator;

    private final ScheduledExecutorService mExecutorService =
            Executors.newSingleThreadScheduledExecutor();

    private ScheduledFuture<?> mScheduleFuture;
    private final Handler mHandler = new Handler();
    private final Runnable mUpdateProgressTask = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    private MediaBrowserCompat mMediaBrowser;
    private PlaybackStateCompat playbackStateCompat;

    private final MediaBrowserCompat.ConnectionCallback mConnectionCallbacks =
            new MediaBrowserCompat.ConnectionCallback() {
                @Override
                public void onConnected() {
                    connectToSession();
                }

                @Override
                public void onConnectionFailed() {
                    Log.e("MediaBrowserCompat", "onConnectionFailed");
                }

                @Override
                public void onConnectionSuspended() {
                    Log.e("MediaBrowserCompat", "onConnectionSuspended");
                }
            };

    private void connectToSession() {
        MediaSessionCompat.Token token = mMediaBrowser.getSessionToken();
        MediaControllerCompat mediaController = null;
        try {
            mediaController = new MediaControllerCompat(self, // Context
                    token);
            MediaControllerCompat.setMediaController((PlaySongActivity) self, mediaController);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
        buildTransportControls();
    }

    MediaControllerCompat.Callback controllerCallback =
            new MediaControllerCompat.Callback() {
                @Override
                public void onMetadataChanged(MediaMetadataCompat metadata) {
                    if (metadata != null) {
                        updateMediaDescription(metadata.getDescription());
                        updateMediaSong();
                        updateDuration(metadata);
                    }
                }

                @Override
                public void onPlaybackStateChanged(PlaybackStateCompat state) {
                    updatePlaybackState(state);
                }
            };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ItemChanged event) {
        updateMediaSong();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.HomeSongItemClicked event) {
        MediaControllerCompat.getMediaController(PlaySongActivity.this).getTransportControls().playFromMediaId("t", null);
        showNowPlayingScreen(true);
    }

    private void buildTransportControls() {
        MediaControllerCompat mediaController = MediaControllerCompat.getMediaController(PlaySongActivity.this);
        mediaController.registerCallback(controllerCallback);
        PlaybackStateCompat state = mediaController.getPlaybackState();
        updatePlaybackState(state);
        MediaMetadataCompat metadata = mediaController.getMetadata();
        if (metadata != null) {
            updateMediaDescription(metadata.getDescription());
            updateDuration(metadata);
        }
        updateProgress();
        if (state != null && (state.getState() == PlaybackStateCompat.STATE_PLAYING ||
                state.getState() == PlaybackStateCompat.STATE_BUFFERING)) {
            scheduleSeekbarUpdate();
        }
    }

    private void showNowPlayingScreen(boolean isShow) {
        if (isShow) {
            vpgMusicPlayer.setCurrentItem(1);
        } else {

        }

    }

    private void updateMediaDescription(MediaDescriptionCompat description) {
        if (description == null) {
            return;
        }
        tvArtists.setText(description.getSubtitle());
        tvTitlePlayer.setText(description.getTitle());


        Song song = QueueManager.getInstance().getCurrentItem();
        if (song != null) {

            if (song.image == null || song.image.isEmpty()) {
                setDefaultBgNowPlaying();
            } else {
                BindingUtil.setImageResizeQuare(pImgBg, song.image);
            }
        }
    }

    private void updateMediaSong() {
        Song song = QueueManager.getInstance().getCurrentItem();
        if (song != null) {
            //  tvArtists.setText(song.getMusicArtists().toString());
            tvTitlePlayer.setText(song.getTitle());

            if (song.image == null || song.image.isEmpty()) {
                setDefaultBgNowPlaying();
            } else {
                BindingUtil.setImageResizeQuare(pImgBg, song.image);
            }
        }
    }

    private void setDefaultBgNowPlaying() {
        pImgBg.setImageBitmap(ImageUtil.blur(this, BitmapFactory.decodeResource(getResources(), R.drawable.bg_player_default)));
    }

    private void updateDuration(MediaMetadataCompat metadata) {
        if (metadata == null) {
            return;
        }
        int duration = (int) metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
        seekBarPlayMusic.setMax(duration);
        tvEndTime.setText(DateUtils.formatElapsedTime(duration / 1000));
    }

    private void initViewPlayer() {

        // content player
        dragView = findViewById(R.id.dragView);
        tvTitlePlayer = findViewById(R.id.c_tv_title);
        tvArtists = findViewById(R.id.c_tv_artists);
        tvArtists.setVisibility(View.VISIBLE);
        vpgMusicPlayer = findViewById(R.id.viewPager);
        seekBarPlayMusic = findViewById(R.id.srb_progress);
        tvStartTime = findViewById(R.id.txt_start_time);
        tvEndTime = findViewById(R.id.txt_total_time);
        btnShuffer = findViewById(R.id.btn_shuffer);
        btnPlay = findViewById(R.id.btn_play);
        btnRepeat = findViewById(R.id.btn_repeat);
        btnNext = findViewById(R.id.btn_next);

        btnPrev = findViewById(R.id.btn_prev);
        btnBackPlayer = findViewById(R.id.c_btn_back);
        btnActionPlayer = findViewById(R.id.c_btn_action);
        circleIndicator = findViewById(R.id.circleIndicator);
        pImgBg = findViewById(R.id.player_img_bg);


        btnShuffer.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnRepeat.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnBackPlayer.setOnClickListener(this);
        btnActionPlayer.setOnClickListener(this);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(PlaySongActivity.this).getTransportControls();
                controls.skipToNext();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(PlaySongActivity.this).getTransportControls();
                controls.skipToPrevious();
            }
        });

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionPlayPause();
            }
        });


        seekBarPlayMusic.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvStartTime.setText(DateUtils.formatElapsedTime(progress / 1000));
                lyricFragment.updateTime(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                stopSeekbarUpdate();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MediaControllerCompat.getMediaController(PlaySongActivity.this).getTransportControls().seekTo(seekBar.getProgress());
                scheduleSeekbarUpdate();
            }
        });

        mMediaBrowser = new MediaBrowserCompat(this,
                new ComponentName(this, MusicService.class),
                mConnectionCallbacks, null); // optional Bundle


    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.layout_player_content;
    }

    @Override
    protected void initView() {
        initViewPlayer();
        if (check) {
            actionPlayPause();
        }

    }

    @Override
    protected void onViewCreated() {
        initPagerPlayer();

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        mMediaBrowser.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        MediaControllerCompat controllerCompat = MediaControllerCompat.getMediaController(this);
        if (controllerCompat != null) {
            controllerCompat.unregisterCallback(controllerCallback);
        }
        mMediaBrowser.disconnect();
    }


    @Override
    public void onClick(View v) {
        if (v == btnBackPlayer) {
            finish();
        } else if (v == btnRepeat) {
            actionRepeat();
        } else if (v == btnShuffer) {
            actionShuffer();
        } else if (v == btnActionPlayer) {
            showMenuActionPlayer();
        }
    }

    private void actionPlayPause() {
        PlaybackStateCompat state = MediaControllerCompat.getMediaController(PlaySongActivity.this).getPlaybackState();
        if (state != null) {

            MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(PlaySongActivity.this).getTransportControls();
            switch (state.getState()) {
                case PlaybackStateCompat.STATE_PLAYING: // fall through
                case PlaybackStateCompat.STATE_BUFFERING:
                    controls.pause();
                    check = true;
                    stopSeekbarUpdate();
                    break;
                case PlaybackStateCompat.STATE_PAUSED:
                    check = false;
                case PlaybackStateCompat.STATE_STOPPED:
                    controls.play();

                    scheduleSeekbarUpdate();
                    break;
                default:
                    Log.d("onClick with state ", " " + state.getState());
            }
        }
    }

    private void updateUIState(boolean isPlaying) {
        if (isPlaying) {
            btnPlay.setImageResource(R.drawable.ic_pause_white_24dp);

        } else {
            btnPlay.setImageResource(R.drawable.ic_play_white);

        }
    }


    private void scheduleSeekbarUpdate() {
        stopSeekbarUpdate();
        if (!mExecutorService.isShutdown()) {
            mScheduleFuture = mExecutorService.scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            mHandler.post(mUpdateProgressTask);
                        }
                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
        }
    }

    private void stopSeekbarUpdate() {
        if (mScheduleFuture != null) {
            mScheduleFuture.cancel(false);
        }
    }

    private void updateProgress() {
        if (playbackStateCompat == null) {
            return;
        }
        long currentPosition = playbackStateCompat.getPosition();
        if (playbackStateCompat.getState() == PlaybackStateCompat.STATE_PLAYING) {
            long timeDelta = SystemClock.elapsedRealtime() -
                    playbackStateCompat.getLastPositionUpdateTime();
            currentPosition += (int) timeDelta * playbackStateCompat.getPlaybackSpeed();
        }
        seekBarPlayMusic.setProgress((int) currentPosition);
    }

    private void updatePlaybackState(PlaybackStateCompat state) {
        if (state == null) {
            return;
        }
        playbackStateCompat = state;
        MediaControllerCompat controllerCompat = MediaControllerCompat.getMediaController(PlaySongActivity.this);
        if (controllerCompat != null && controllerCompat.getExtras() != null) {
        }

        switch (state.getState()) {
            case PlaybackStateCompat.STATE_PLAYING:
                updateUIState(true);
                startAnimCircleImagePlaying(true);
                scheduleSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_PAUSED:
                updateUIState(false);
                startAnimCircleImagePlaying(false);
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_NONE:
            case PlaybackStateCompat.STATE_STOPPED:
                updateUIState(false);
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_BUFFERING:
                stopSeekbarUpdate();
                break;
            default:
                Log.d("updatePlaybackState", "Unhandled state " + state.getState());
        }

    }

    private void initPagerPlayer() {
        tvArtists.setText("");
        tvTitlePlayer.setText("");
        setDefaultBgNowPlaying();
        vpgMusicPlayer.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        circleIndicator.setViewPager(vpgMusicPlayer);
        vpgMusicPlayer.setCurrentItem(1);

    }

    public void startAnimCircleImagePlaying(boolean isStart) {
        if (fragmentNowPlaying != null) {
            if (isStart) fragmentNowPlaying.startAnimation();
            else fragmentNowPlaying.stopAnimation();
        }
    }

    NowPlayingFragment fragmentNowPlaying;
    LyricFragment lyricFragment;

    class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                SingleListFragment fragment = SingleListFragment.newInstance(QueueSongVM.class, R.layout.layout_list_playing, R.layout.item_song_queue, ItemSongQueueVM.class);
                //fragment.setBaseListener(this);
                return fragment;
            }
            if (position == 1) {

                fragmentNowPlaying = NowPlayingFragment.newInstance();
                return fragmentNowPlaying;
            }
            if (position == 2) {
                lyricFragment = LyricFragment.newInstance();
                return lyricFragment;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "";
            } else {
                return "";
            }
        }

        @Override
        public int getCount() {
            return 3;
        }


    }


    private void actionRepeat() {
        QueueManager queueManager = QueueManager.getInstance();
        if (queueManager.isRepeat) {
            queueManager.setRepeat(false);
            btnRepeat.setImageResource(R.drawable.ic_player_repeat_all);
            AppUtil.showToast(this, R.string.no_repeat);
        } else {
            queueManager.setRepeat(true);
            btnRepeat.setImageResource(R.drawable.ic_player_repeat_all_pressed);
            AppUtil.showToast(this, R.string.repeat);
        }
    }

    private void actionShuffer() {
        QueueManager queueManager = QueueManager.getInstance();
        if (queueManager.isShuffle) {
            queueManager.setShuffle(false);
            btnShuffer.setImageResource(R.drawable.ic_player_shuffle);
            AppUtil.showToast(this, R.string.no_shuffe);
        } else {
            queueManager.setShuffle(true);
            btnShuffer.setImageResource(R.drawable.ic_player_shuffle_selected);
            AppUtil.showToast(this, R.string.shuffer);
        }
    }

    private void showMenuActionPlayer() {
        Global.showPopupMenu(self, btnActionPlayer, R.menu.menu_action_player, new IOnMenuItemClick() {
            @Override
            public void onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_volumn:
                        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_SAME, AudioManager.FLAG_SHOW_UI);
                        break;

                }
            }
        });
    }
}
