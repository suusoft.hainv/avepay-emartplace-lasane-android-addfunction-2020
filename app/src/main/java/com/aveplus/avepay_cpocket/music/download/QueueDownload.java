package com.aveplus.avepay_cpocket.music.download;

import android.app.DownloadManager;

import com.aveplus.avepay_cpocket.music.model.Song;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class QueueDownload {

    public static final String QUEUE = "Queue";
    public static final String DOWNLOADING = "Downloading";

    public String id;
    public String name;
    public String total;
    public String status;
    public DownloadManager.Request request;
    public Song song;
    public int progress;

    public QueueDownload() {
    }

    public QueueDownload(String id, String name, String status, DownloadManager.Request request) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.request = request;
    }

    public boolean getProgressListener() {
        return DOWNLOADING.equals(status);
    }

    public boolean isQueue() {
        return QUEUE.equals(status);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DownloadManager.Request getRequest() {
        return request;
    }

    public void setRequest(DownloadManager.Request request) {
        this.request = request;
    }

    public void setSong(Song song) {
        this.song = song;
    }
}
