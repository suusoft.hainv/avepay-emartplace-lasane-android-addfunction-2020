package com.aveplus.avepay_cpocket.music.listener;

import android.view.View;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public interface IOnClickListener {

    public void onClick(View view);
}
