package com.aveplus.avepay_cpocket.ebook.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Suusoft on 1/5/2017.
 */

public class Actor implements Parcelable {
    private String id;
    private String name;
    private String image;
    private String gender;
    private String dob;
    private String address;
    private String description;
    @SerializedName("book_id")
    private String bookId;
    @SerializedName("chapter_id")
    private String chapterId;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getGender() {
        return gender;
    }

    public String getDob() {
        return dob;
    }

    public String getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }

    public String getBookId() {
        return bookId;
    }

    public String getChapterId() {
        return chapterId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.image);
        dest.writeString(this.gender);
        dest.writeString(this.dob);
        dest.writeString(this.address);
        dest.writeString(this.description);
        dest.writeString(this.bookId);
        dest.writeString(this.chapterId);
    }

    public Actor() {
    }

    protected Actor(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.image = in.readString();
        this.gender = in.readString();
        this.dob = in.readString();
        this.address = in.readString();
        this.description = in.readString();
        this.bookId = in.readString();
        this.chapterId = in.readString();
    }

    public static final Creator<Actor> CREATOR = new Creator<Actor>() {
        @Override
        public Actor createFromParcel(Parcel source) {
            return new Actor(source);
        }

        @Override
        public Actor[] newArray(int size) {
            return new Actor[size];
        }
    };
}
