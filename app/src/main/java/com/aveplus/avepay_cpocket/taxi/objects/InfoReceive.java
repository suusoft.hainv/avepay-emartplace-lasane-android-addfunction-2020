package com.aveplus.avepay_cpocket.taxi.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class InfoReceive implements Parcelable {

    public String duration, driver_name, action , trip_id, lat ;

    @SerializedName("long")
    public String lng;

    protected InfoReceive(Parcel in) {
        duration = in.readString();
        driver_name = in.readString();
        action = in.readString();
        trip_id = in.readString();
        lat = in.readString();
        lng = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(duration);
        dest.writeString(driver_name);
        dest.writeString(action);
        dest.writeString(trip_id);
        dest.writeString(lat);
        dest.writeString(lng);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InfoReceive> CREATOR = new Creator<InfoReceive>() {
        @Override
        public InfoReceive createFromParcel(Parcel in) {
            return new InfoReceive(in);
        }

        @Override
        public InfoReceive[] newArray(int size) {
            return new InfoReceive[size];
        }
    };
}
