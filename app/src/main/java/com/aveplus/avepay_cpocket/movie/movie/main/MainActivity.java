package com.aveplus.avepay_cpocket.movie.movie.main;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.fragment.MainRowSubCateFragment;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.CallBackNewestMovie;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onFocusRow;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Language;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.onUpdateBackdropMovie;
import com.aveplus.avepay_cpocket.utils.Utils;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends Activity implements View.OnClickListener, onFocusRow, CallBackNewestMovie {
    private final int ITEM_PAGE = 3;
    private View viewRow;
    private EditText txtSearch;
    private ImageButton btnNextPage;
    private ImageButton btnPrevPage;
    private ImageButton btnSearch;
    private NetworkImageView img_backdrop;

    private ScrollView scrollView;
    private ViewGroup view_row_category;
    private List<Language> languages;

    private MoviesManagement moviesManagement;
    private ImageLoader mImageLoader;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Utils.isNetworkConnected(this)) {
            Toast.makeText(this, "No network connection!",
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        setContentView(R.layout.activity_main_movie);
        initMana();
        initView();
        initControl();
        initData();


    }

    private void initMana() {
        moviesManagement = MoviesManagement.getInstance();
        moviesManagement.getNewestMovie(this);
    }

    private void initData() {

        languages = new ArrayList<>();

        HashMap<Language, List<Movie>> mLangMovies = moviesManagement.getmLangMovies();
        for (Language language : mLangMovies.keySet()) {
            languages.add(language);
        }
        moviesManagement.setmLanguages(languages);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MainRowSubCateFragment subCategoryFragment;
        for (int i = 0; i < languages.size(); i++) {
            subCategoryFragment = MainRowSubCateFragment.newInstance(languages.get(i).getLangId(), i);
            fragmentTransaction.add(R.id.view_row_category, subCategoryFragment);
        }
        fragmentTransaction.commit();

    }

    private void initView() {
        txtSearch = (EditText) findViewById(R.id.txt_search);
        txtSearch.clearFocus();
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        view_row_category = (ViewGroup) findViewById(R.id.view_row_category);
        btnNextPage = (ImageButton) findViewById(R.id.btn_next_page);
        btnSearch = (ImageButton) findViewById(R.id.btnSearch);
        btnPrevPage = (ImageButton) findViewById(R.id.btn_prev_page);
        img_backdrop = (NetworkImageView) findViewById(R.id.img_backdrop);

    }

    private void initControl() {
        btnPrevPage.setOnClickListener(this);
        btnNextPage.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        txtSearch.setOnEditorActionListener(editorActionListener);
        // txtSearch.setOnFocusChangeListener(focusChangeListener);
    }

    EditText.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtSearch, InputMethodManager.SHOW_IMPLICIT);
            } else {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txtSearch.getWindowToken(), 0);
            }
        }
    };
    EditText.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
            if (keyCode == EditorInfo.IME_ACTION_SEARCH || keyCode == KeyEvent.KEYCODE_ENTER) {
                sendSearch(txtSearch.getText().toString());
                return true;
            }
            return false;
        }
    };

    private void sendSearch(String text) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(SearchActivity.TEXT_SEARCH, text);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        if (view == btnNextPage) {
            scrollPage();
        } else if (view == btnSearch) {
            sendSearch(txtSearch.getText().toString());
        }
    }

    int lastItemPosition = 3;

    private void scrollPage() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
//                if(categories.length < lastItemPosition) {
//                    lastItemPosition = 0;
//                    scrollView.smoothScrollTo(0, 0);
//                }
                if (languages.size() > lastItemPosition) {
                    View view = view_row_category.getChildAt(lastItemPosition);
                    scrollView.smoothScrollTo(0, view.getTop());
                    lastItemPosition += ITEM_PAGE;
                }
            }
        });

    }

    @Override
    public void onFocus(int position) {
        setBackgroundCategory(position);
    }

    // set background for Category when focus row
    int oldPos = -2;

    private void setBackgroundCategory(int position) {

        if (oldPos != position) {
            if (oldPos != -2) {
                if (position <= 0) { //hide button previous
                    btnPrevPage.setVisibility(View.INVISIBLE);
                } else if (position == languages.size() - 1) { //hide button next
                    btnNextPage.setVisibility(View.INVISIBLE);
                } else {
                    btnPrevPage.setVisibility(View.VISIBLE);
                    btnNextPage.setVisibility(View.VISIBLE);
                }
            }

            oldPos = position;
        }
    }

    private void getBackDropBg(String imdbMovie) {
        mImageLoader = RequestManager.getImageLoader();
        moviesManagement.movieBackdropImgRequest(imdbMovie, new onUpdateBackdropMovie() {
            @Override
            public void onCompleted(String response) {
                moviesManagement.setUrlBackDrop(response);
                img_backdrop.setImageUrl(response, mImageLoader);
            }

            @Override
            public void onError(Exception ex) {
            }
        });
    }

    @Override
    public void success(Movie movie) {
        getBackDropBg(movie.getMovie_imdb());
    }

}
