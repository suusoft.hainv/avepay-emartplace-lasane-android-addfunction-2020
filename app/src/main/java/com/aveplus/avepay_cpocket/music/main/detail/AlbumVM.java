package com.aveplus.avepay_cpocket.music.main.detail;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Album;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.aveplus.avepay_cpocket.music.player.QueueManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class AlbumVM extends BaseViewModelList implements ItemMenuActionSongListener {

    Album album;

    public AlbumVM(Context context, Album data) {
        super(context);
        this.album = data;
        getData(1);
    }

    public String getTitle(){
        return album.name;
    }

    public String getImage(){
        return album.thumbnail;
        //return album.image;
    }

    public String getThumb(){
        return album.thumbnail;
    }

    public String getDescription(){
        return album.description;
    }


    public String getItemsCount(){
        return  String.format(self.getString(R.string.bind_count),album.songs_count);
    }

    public void onClickPlay(View view){

        if(getListData().size()>0){
            QueueManager.getInstance().addNewQueue((ArrayList<Song>) getListData());
            EventBus.getDefault().post(new Global.HomeSongItemClicked());
        }
        else {
            Toast.makeText(self, "Album has no song!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void getData(int page) {
        RequestManager.getItemByPlaylist(page, album.id, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (!response.isError()) {
                    ArrayList<Song> list = (ArrayList<Song>) response.getDataList(Song.class);
                    addListData(list);
                    checkLoadingMoreComplete(1);
                }
            }

            @Override
            public void onError(String message) {
                Log.d("MusicByCategoryVM","can not get video detail");

            }
        });
    }

    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().addNewQueue((ArrayList<Song>) getListData(),position);
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {

    }
}
