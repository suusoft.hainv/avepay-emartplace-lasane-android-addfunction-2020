package com.aveplus.avepay_cpocket.music.listener;


import com.aveplus.avepay_cpocket.music.model.Artist;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public interface IOnItemArtistClickListener extends IBaseListener {
    void onArtistItemClicked(Artist artist);
}
