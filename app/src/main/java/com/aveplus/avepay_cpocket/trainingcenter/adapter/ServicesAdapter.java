package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.Services;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class ServicesAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Services> objects;
    private LayoutInflater inflater;

    public ServicesAdapter(Context context, ArrayList<Services> objects) {
        this.context = context;
        this.objects = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_ourservice, null);
            vh = new ViewHoler();
            vh.txtTitleServices = (TextView) rowView.findViewById(R.id.txtTitleServices);
            vh.txtDescriptionService = (TextView) rowView.findViewById(R.id.txtDescriptionService);
            vh.imgService = (ImageView) rowView.findViewById(R.id.imgService);
            vh.txtDescriptionService.setSelected(true);
            vh.txtTitleServices.setSelected(true);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        final Services c = objects.get(position);
        if (c != null) {
            vh.txtTitleServices.setText(c.getTitle());
            vh.txtDescriptionService.setText(c.getDescription());
            Picasso.get().load(c.getImage()).into(vh.imgService);
        }
        return rowView;
    }

    static class ViewHoler {
        public TextView txtTitleServices, txtDescriptionService;
        public ImageView imgService;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
