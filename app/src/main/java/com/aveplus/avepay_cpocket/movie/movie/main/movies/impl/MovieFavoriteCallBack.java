package com.aveplus.avepay_cpocket.movie.movie.main.movies.impl;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface MovieFavoriteCallBack {
    public void setFavorite(int position);
}
