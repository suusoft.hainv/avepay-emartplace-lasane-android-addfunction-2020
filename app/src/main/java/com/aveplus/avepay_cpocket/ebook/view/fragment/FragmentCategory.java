package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseListFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity;
import com.aveplus.avepay_cpocket.ebook.view.adapter.CategoryType1Adapter;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.CategoryVM;


/**
 * Created by Suusoft on 9/8/2017.
 */
public class FragmentCategory extends BaseListFragmentBinding {
    private CategoryType1Adapter mAdapterCategoryType1;
    private CategoryVM viewModel;

    public static FragmentCategory newInstance() {
        Bundle args = new Bundle();
        FragmentCategory fragment = new FragmentCategory();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(viewModel.getLayoutManager());

        mAdapterCategoryType1 = new CategoryType1Adapter(self, viewModel.getListData());
        recyclerView.setAdapter(mAdapterCategoryType1);
        int sizePadding = AppUtil.convertDpToPixel(self, 8);

    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new CategoryVM(self);
        return viewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) self).setToolbarTitle(R.string.category);
        ((MainActivity) self).showIconToolbar();
    }
}
