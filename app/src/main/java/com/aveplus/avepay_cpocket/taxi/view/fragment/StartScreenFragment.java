package com.aveplus.avepay_cpocket.taxi.view.fragment;

import android.view.View;
import android.widget.ImageView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseFragment;
import com.aveplus.avepay_cpocket.taxi.utils.ImageUtil;


public class StartScreenFragment extends BaseFragment {


    private int image;
    private ImageView img;

    public static StartScreenFragment newInstance(int image){
        StartScreenFragment fragment = new StartScreenFragment();
        fragment.image = image;
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_start_screen;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        img = view.findViewById(R.id.img);

        ImageUtil.setImage(img, image);
    }

    @Override
    protected void getData() {

    }
}

