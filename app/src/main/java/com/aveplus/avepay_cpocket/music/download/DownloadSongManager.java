package com.aveplus.avepay_cpocket.music.download;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.Config;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.util.NetworkUtility;
import com.aveplus.avepay_cpocket.music.util.StringUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class DownloadSongManager {

    private static final String TAG = DownloadSongManager.class.getName();
    public static final Uri CONTENT_URI = Uri.parse("content://" + Config.getNameDownloadFolder());
    private static final int QUEUE_DOWNLOAD_LIMIT = 1;
    private Context context;
    private DownloadManager downloadManager;
    private DownloadManagerPro downloadManagerPro;
    private DownloadChangeObserver downloadObserver;

    private List<QueueDownload> queueList;
    private IDownloadListener listener;
    private Handler handler;

    private List<IDownloadingListener> mDownloadingListeners;

    public static DownloadSongManager instance;

    public interface IDownloadListener {
        void onDownloadUpdated(String downloadId);
    }

    public interface IDownloadingListener {
        void onDownloading(String downloadId, int progress, int total);
    }

    public static DownloadSongManager getInstance(Context context) {
        if (instance == null) {
            instance = new DownloadSongManager(context);
            instance.downloadManager = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
            instance.downloadManagerPro = new DownloadManagerPro(instance.downloadManager);
        }
        return instance;
    }

    public DownloadSongManager(Context context) {
        this.context = context;
        queueList = new ArrayList<>();
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        context.registerReceiver(downloadReceiver, filter);
    }

    public long download(Song song) {
        long downloadReference = 0;
        if (NetworkUtility.isNetworkAvailable())
            if (existedSongDownload(song)) {
                AppUtil.showToast(context, context.getString(R.string.msg_song_downloaded));
                return 0;
            } else {
                checkFolderDownload();

                // Create request for android download manager
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(song.getSong_file()));
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setTitle(song.title);
                request.setDescription("Downloading...");
                request.allowScanningByMediaScanner();
//        request.setDestinationInExternalPublicDir("/" + DataStoreManager.getPathDir(context), song.name + ".mp3");
                request.setDestinationUri(Uri.withAppendedPath(Uri.fromFile(new File(Config.getPathDir())), song.title + ".mp3"));
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterByStatus(DownloadManager.STATUS_RUNNING);
                //Query the download manager about downloads that have been requested.
                Cursor cursor = downloadManager.query(query);
                if (cursor.getCount() < QUEUE_DOWNLOAD_LIMIT) {
                    downloadReference = downloadManager.enqueue(request);
                    QueueDownload download = new QueueDownload(downloadReference + "", song.title, QueueDownload.DOWNLOADING, request);
                    download.setSong(song);
                    queueList.add(download);
                } else {
                    QueueDownload download = new QueueDownload(downloadReference + "", song.title, QueueDownload.QUEUE, request);
                    download.setSong(song);
                    queueList.add(download);
                }
                cursor.close();

                // checkStatusDownloadQueue(downloadReference);
            }
        else AppUtil.showToast(context, R.string.msg_connection_network_error);
        return downloadReference;
    }

    private void checkFolderDownload() {
        //Set the local destination for the downloaded file to a path within the application's external files directory
        File direct = new File(Config.getPathDir());

        if (!direct.exists()) {
            direct.mkdirs();
        }
    }

    private boolean existedSongDownload(Song song) {
        File file = new File(Config.getPathDir() + "/" + song.title + ".mp3");
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public void registerDownloadContent() {
        handler = new Handler();
        downloadObserver = new DownloadChangeObserver();
        context.getContentResolver().registerContentObserver(CONTENT_URI, true, downloadObserver);
    }

    public void unRegisterDownloadContent() {
        context.getContentResolver().unregisterContentObserver(downloadObserver);
    }

    public void unRegisterDownload() {
        context.unregisterReceiver(downloadReceiver);

    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            updateCountDownloadSong(context, String.valueOf(referenceId));
            // remove file is just done
            removeFromQueue(referenceId);

            // add next queue file to downloading
            if (queueList.size() > 0) {
                for (QueueDownload item : queueList) {
                    if (item.isQueue()) {
                        long requetsId = downloadManager.enqueue(item.getRequest());
                        item.setId(requetsId + "");
                        item.setStatus(QueueDownload.DOWNLOADING);
                        break;
                    }
                }

            }

            // notify downloading is done
            if (listener != null)
                listener.onDownloadUpdated(referenceId + "");

        }
    };

    private void updateCountDownloadSong(Context context, String id) {


    }

    private void removeFromQueue(long referenceId) {
        for (QueueDownload item : queueList) {
            if (item.getProgressListener() && item.getId().equals(referenceId + "")) {
                queueList.remove(item);
                return;
            }
        }
    }

    class DownloadChangeObserver extends ContentObserver {

        public DownloadChangeObserver() {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            String strUri = uri.toString();
            String downloadId = strUri.substring(strUri.lastIndexOf("/") + 1, strUri.length());
            Log.e("DownloadChangeObserver", " onChange ");

            if (StringUtil.isNumeric(downloadId)) {
                Log.e("DownloadChangeObserver", "uri: " + uri + " id: " + downloadId);
                int[] bytesAndStatus = downloadManagerPro.getBytesAndStatus(Long.parseLong(downloadId));
                for (IDownloadingListener listener : mDownloadingListeners) {
                    listener.onDownloading(downloadId, bytesAndStatus[0], bytesAndStatus[1]);
                }
            }

           /* for (QueueDownload item : queueList){
                int[] bytesAndStatus = downloadManagerPro.getBytesAndStatus(Long.parseLong(item.getId()));
                Log.e("DownloadChangeObserver", "uri: " + uri + " id: " + Long.parseLong(item.getId()));
                if (bytesAndStatus[1] != -1){

                    *//*handler.sendMessage(handler.obtainMessage(Integer.parseInt(item.getId()), bytesAndStatus[0], bytesAndStatus[1],
                            bytesAndStatus[2]));*//*
                }
            }*/

           /* for (IDownloadingListener listener : mDownloadingListeners) {
                listener.onDownloading(downloadId, bytesAndStatus[0], bytesAndStatus[1]);
            }*/

            /*int[] bytesAndStatus = downloadManagerPro.getBytesAndStatus(downloadId);
            handler.sendMessage(handler.obtainMessage(0, bytesAndStatus[0], bytesAndStatus[1], bytesAndStatus[2]));*/
        }

    }

    public List<IDownloadingListener> getDownloadingListeners() {
        if (mDownloadingListeners == null)
            mDownloadingListeners = new ArrayList<>();
        return mDownloadingListeners;
    }

    public void addDownloadingListeners(IDownloadingListener downloadingListeners) {
        getDownloadingListeners().add(downloadingListeners);
    }

    /**
     * Remove a download Id
     *
     * @param downloadId is downloadId
     */
    public void removeDownload(long downloadId) {
        downloadManagerPro.resumeDownload();
        removeFromQueue(downloadId);
        listener.onDownloadUpdated(String.valueOf(downloadId));
    }

    public DownloadManagerPro getDownloadManagerPro() {
        return downloadManagerPro;
    }

    public List<QueueDownload> getQueueList() {
        return queueList;
    }

    public void setQueueList(List<QueueDownload> queueList) {
        this.queueList = queueList;
    }

    public IDownloadListener getListener() {
        return listener;
    }

    public void setListener(IDownloadListener listener) {
        this.listener = listener;
    }

    public void removeListener() {
        this.listener = null;
    }
}
