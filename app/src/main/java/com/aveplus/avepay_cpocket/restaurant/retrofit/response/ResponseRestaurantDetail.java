package com.aveplus.avepay_cpocket.restaurant.retrofit.response;

import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.RestaurantDetail;

/**
 * Created by suusoft.com on 11/12/19.
 */



public class ResponseRestaurantDetail extends BaseModel {

    private RestaurantDetail data;

    public RestaurantDetail getData() {
        return data;
    }

    public void setData(RestaurantDetail data) {
        this.data = data;
    }
}