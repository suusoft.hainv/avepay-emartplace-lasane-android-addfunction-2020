package com.aveplus.avepay_cpocket.music.main.localmusic;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragmentNav;
import com.aveplus.avepay_cpocket.music.base.view.SingleListFragment;
import com.aveplus.avepay_cpocket.music.listener.IOnFragmentNavListener;
import com.aveplus.avepay_cpocket.music.listener.IOnItemArtistClickListener;
import com.aveplus.avepay_cpocket.music.model.Artist;
import com.google.android.material.tabs.TabLayout;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class LocalMusicFragment extends BaseFragmentNav implements IOnItemArtistClickListener, IOnFragmentNavListener {

    private static final String KEY_POSITION = "KEY_POSITION";

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private int curPosition;

    public static LocalMusicFragment newInstance(int position) {

        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        LocalMusicFragment fragment = new LocalMusicFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_local_music;
    }

    @Override
    protected void init() {
        curPosition = getArguments().getInt(KEY_POSITION);
    }

    @Override
    protected void initView(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.view_pagger);
        viewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager()));
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(curPosition);
        setTitle(R.string.local_music);
     //   setIvTitile(R.drawable.music);
        showHideActionMenu(false);

    }

    @Override
    protected void getData() {

    }

    @Override
    public void onArtistItemClicked(Artist artist) {
        contentLayout1.startAnimation(animSlideIn);
        LocalSongByArtistFragment fragment = LocalSongByArtistFragment.newInstance(artist.id, artist.name);
        fragment.setListener(this);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.content1, fragment, "LocalSongByArtistFragment");
        transaction.commit();
        //contentLayout1.setVisibility(View.VISIBLE);

    }

    //LocalSongByArtistFragment back listener
    @Override
    public void onFragmentBack() {
        contentLayout1.startAnimation(animSlideOut);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.remove(getChildFragmentManager().findFragmentByTag("LocalSongByArtistFragment"));
        transaction.commit();
        // contentLayout1.setVisibility(View.GONE);


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private String[] tabs = getResources().getStringArray(R.array.tab_music);

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                LocalSongFragment fragment = LocalSongFragment.newInstance();
                return fragment;
            }
            if (position == 1) {
                SingleListFragment fragment = SingleListFragment.newInstance(ArtistLocalVM.class, R.layout.item_artist, ItemArtistVM.class);
                fragment.setBaseListener(LocalMusicFragment.this);
                return fragment;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public int getCount() {
            return tabs.length;
        }


    }


}
