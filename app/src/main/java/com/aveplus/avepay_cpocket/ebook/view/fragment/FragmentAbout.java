package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.ViewDataBinding;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.FragmentAboutBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.AboutVM;


/**
 * Created by Suusoft on 9/8/2017.
 */
public class FragmentAbout extends BaseFragmentBinding {
    private AboutVM viewModel;
    private FragmentAboutBinding binding;

    public static FragmentAbout newInstance() {
        Bundle args = new Bundle();
        FragmentAbout fragment = new FragmentAbout();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_about;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new AboutVM(self);
        return viewModel;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        this.binding = (FragmentAboutBinding) binding;
        this.binding.setViewModel(viewModel);
    }


    @Override
    protected void initView(View view) {
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) self).setToolbarTitle(R.string.about);
        ((MainActivity) self).showIconToolbar();
    }

}
