package com.aveplus.avepay_cpocket.restaurant.listener;

import android.view.View;

/**
 * Created by Trang on 7/7/2016.
 */
public interface IOnItemClickedListener extends IBaseListener {
     void onItemClicked(View view, int position);
}
