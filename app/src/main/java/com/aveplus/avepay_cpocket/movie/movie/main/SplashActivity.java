package com.aveplus.avepay_cpocket.movie.movie.main;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.UpdateGenreCallback;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.Utils;
import com.aveplus.avepay_cpocket.view.activities.LoginActivity;


public class SplashActivity extends AppCompatActivity implements UpdateGenreCallback {
    private static final String TAG = SplashActivity.class.getSimpleName();
    private RequestQueue requestQueue;
    private MoviesManagement moviesManagement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.e(TAG, "tokenFCM1: " + DataStoreManager.getTokenFCM());


//        boolean isTabblet = getResources().getBoolean(R.bool.isTablet);
//        if (isTabblet) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        } else {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(R.drawable.anh_nen);//R.drawable.splash
        if (!Utils.isNetworkConnected(this)) {
            Toast.makeText(this, "No network connection!",
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        setContentView(R.layout.splash);
        initProgress();
        startHome();
        // loadData();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void initProgress() {
        findViewById(R.id.progressView).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onCompleted(boolean isSuccess) {
        if (isSuccess) {
//            findViewById(R.id.progressView).setVisibility(View.GONE);
//////			nextActivity();


        } else {
            Toast.makeText(SplashActivity.this, getString(R.string.cannot_load_data),
                    Toast.LENGTH_LONG).show();
            findViewById(R.id.progressView).setVisibility(View.GONE);
            finish();
            return;
        }
    }

    private void nextActivity() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }

    private void startHome() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*comment log in*/
//                AppUtil.startActivity(SplashActivity.this, CategoryActivity.class);
                AppUtil.startActivity(SplashActivity.this, SplashCodeActivity.class);
                finish();
//                if (DataStoreManager.getUser() == null) {
//                    AppUtil.startActivity(SplashActivity.this, LoginActivity.class);
//                    finish();
//                } else {
//
//                    AppController.getInstance().setToken(DataStoreManager.getToken());
//                    AppUtil.startActivity(SplashActivity.this, CategoryActivity.class);
//                    finish();
//                }


//                AppUtil.startActivityLTR(SplashActivity.this, MainActivity.class);
//                finish();
            }
        }, 1500);
    }
}
