package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.Galleries;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class GalleriesAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Galleries> objects;
    private LayoutInflater inflater;

    public GalleriesAdapter(Context context, ArrayList<Galleries> objects) {
        this.context = context;
        this.objects = objects;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_galleries_about, null);
            vh = new ViewHoler();
            vh.imageGallery = (ImageView) rowView.findViewById(R.id.imgGalleries);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        final Galleries c = objects.get(position);
        if (c != null) {
            Picasso.get().load(c.getUrl()).into(vh.imageGallery);
        }
        return rowView;
    }

    static class ViewHoler {
        public ImageView imageGallery;
    }
}
