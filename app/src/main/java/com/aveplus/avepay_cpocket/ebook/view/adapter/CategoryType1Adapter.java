package com.aveplus.avepay_cpocket.ebook.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.ItemCategoryType1Binding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseAdapter;
import com.aveplus.avepay_cpocket.ebook.model.Category;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemCategoryVM;

import java.util.List;


public class CategoryType1Adapter extends BaseAdapter {

    private List<Category> listData;

    public CategoryType1Adapter(Context context, List<?> datas) {
        super(context);
        this.listData = (List<Category>) datas;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCategoryType1Binding binding = (ItemCategoryType1Binding) getViewBinding(parent, R.layout.item_category_type_1);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        ((ViewHolder) holder).bindViewModel(listData.get(position));
        ((ViewHolder) holder).itemView.setLayoutParams(
                new LinearLayout.LayoutParams(
                        (int) ((AppUtil.getScreenWidth((Activity) context) - AppUtil.convertDpToPixel(context, 8)) / 2),
                        (int) ((AppUtil.getScreenWidth((Activity) context) - AppUtil.convertDpToPixel(context, 8)) / 2 * 0.6)));
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    @Override
    public void setDatas(List<?> data) {
        int size = listData.size();
        listData.addAll((List<Category>) data);
        notifyItemRangeInserted(size, listData.size());
    }


    class ViewHolder extends BaseViewHolder {
        private ItemCategoryType1Binding binding;

        public ViewHolder(ItemCategoryType1Binding binding) {
            super(binding);
            this.binding = binding;
        }

        public void bindViewModel(Category category) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemCategoryVM(context, category));
            } else {
                binding.getViewModel().setData(category);
            }
        }
    }
}
