package com.aveplus.avepay_cpocket.view.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.view.fragments.FragmentPagerIndicatorCate;

import java.util.ArrayList;

/**
 * Created by SUN on 3/7/2018.
 */

public class AdapterViewPaggerCategory extends FragmentPagerAdapter {


    private ArrayList<DealObj> mDealObjs;

    public AdapterViewPaggerCategory(FragmentManager fm, ArrayList<DealObj> dealObjs ) {
        super(fm);
        mDealObjs = dealObjs;
    }

    @Override
    public Fragment getItem(int position) {

        return FragmentPagerIndicatorCate.newInstance(mDealObjs.get(position));
    }

    @Override
    public int getCount() {
        return mDealObjs.size();
    }
}
