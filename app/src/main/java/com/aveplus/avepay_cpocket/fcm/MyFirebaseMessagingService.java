package com.aveplus.avepay_cpocket.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.quickblox.messages.services.fcm.QBFcmPushListenerService;
import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.view.activities.SplashLoginActivity;
import com.aveplus.avepay_cpocket.configs.ChatConfigs;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.Constants;
import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.objects.PaymentMethodObj;
import com.aveplus.avepay_cpocket.objects.RecentChatObj;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.aveplus.avepay_cpocket.quickblox.SharedPreferencesUtil;

public class MyFirebaseMessagingService extends QBFcmPushListenerService {

    private static final String TAG = "MyFirebaseMsgService";

    private static int REQUEST_CODE = 0;
    private static int NOTIFICATION_ID = 0;
    private static String notifi_chanel = "notify_001";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e(TAG, "From: " + remoteMessage.getFrom() + ", message: " + remoteMessage.getData().get("message"));

        // Check if message contains a data payload.
        String message = "", type = "";
        if (remoteMessage.getData().size() > 0) {
            message = remoteMessage.getData().get(Args.MESSAGE);
            type = remoteMessage.getData().get(Args.NOTIFICATION_TYPE);
            Log.e(TAG, "onMessageReceived: "+type );
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        if (type.equals(ChatConfigs.QUICKBLOX_MESSAGE) || type.equalsIgnoreCase(ChatConfigs.QUICKBLOX_PAY_FOR_DEAL)) {
            String json = remoteMessage.getData().get(Args.RECENT_CHAT_OBJ);
            // Replace json if it's not correct format
            if (json.contains("=>")) {
                json = json.replace("=>", ":");
            }
            Log.e(TAG, json);
            RecentChatObj obj = new Gson().fromJson(json, RecentChatObj.class);

            if (type.equals(ChatConfigs.QUICKBLOX_MESSAGE)) {
                if (SharedPreferencesUtil.hasQbUser() && (!GlobalFunctions.isForeground(obj) || !DataStoreManager.conversationIsLive())) {
                    // Remove sender's name(sender's name is just for iOS)
                    if (message.contains(":")) {
                        message = message.substring((message.indexOf(":") + 1)).trim();
                    }

                    obj.setLastMessage(message);
                    sendQBNotification(message, type, obj);

                    // Must also set negotiation to false here
                    if (message.endsWith(String.format(getString(R.string.user_canceled_deal), "").trim())) {
                        DataStoreManager.saveDealNegotiation(obj, false);
                    }
                }
            } else if (type.equalsIgnoreCase(ChatConfigs.QUICKBLOX_PAY_FOR_DEAL)) {
                // Remove sender's name(sender's name is just for iOS)
                if (message.contains(":")) {
                    message = message.substring((message.indexOf(":") + 1)).trim();
                }

                // Must also set negotiation to false here
                DataStoreManager.saveDealNegotiation(obj, false);
                if (GlobalFunctions.isForeground(obj)) {
                    // Update option menu
                    sendMessage(false);
                }

                // Update balance if client paid via credits
                String paymentMethod = remoteMessage.getData().get(Args.PAYMENT_METHOD);
                if (paymentMethod.equalsIgnoreCase(PaymentMethodObj.CREDITS)) {
                    String fare = remoteMessage.getData().get(Args.FARE);
                    if (fare == null || fare.equals("")) {
                        fare = "0";
                    } else if (fare.contains(",")) {
                        fare = fare.replace(",", "");
                    }
                    UserObj user = DataStoreManager.getUser();
                    user.setBalance(user.getBalance() + Float.parseFloat(fare));
                    DataStoreManager.saveUser(user);
                    if (AppController.getInstance().getiRedeem() != null) {
                        AppController.getInstance().getiRedeem().updateBalace(user.getBalance());
                    }
                }

                sendNotification(message);
            }
        } else if (type.equals(ChatConfigs.TYPE_DEAL)) {
            String id = remoteMessage.getData().get(Args.BALANCE);
            sendNewDeal(message, ChatConfigs.TYPE_DEAL, id);
        } else if (type.equals(Args.TYPE_BALANCE)) {
            UserObj user = DataStoreManager.getUser();
            String balance = remoteMessage.getData().get(Args.BALANCE);
            user.setBalance(Float.parseFloat(balance));
            DataStoreManager.saveUser(user);
            if (AppController.getInstance().getiRedeem() != null) {
                AppController.getInstance().getiRedeem().updateBalace(user.getBalance());
            }

            sendNotification(message);
        } else {
            sendNotification(message);
        }

        /*if (type.equals(Args.TYPE_BALANCE)) {
            UserObj user = DataStoreManager.getUser();
            String balance = remoteMessage.getData().get(Args.BALANCE);
            Log.e("EE", "BAL: " + balance);
            user.setBalance(Float.parseFloat(balance));
            DataStoreManager.saveUser(user);
            if (AppController.getInstance().getiRedeem() != null) {
                AppController.getInstance().getiRedeem().updateBalace(user.getBalance());
            }
        }*/
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }
        Intent intent = new Intent(this, SplashLoginActivity.class);
        REQUEST_CODE++;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, notifi_chanel)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        Log.e("DDD","1");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NOTIFICATION_ID++;
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    private void sendQBNotification(String messageBody, String notificationType, RecentChatObj obj) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }

        Intent intent = new Intent(this, SplashLoginActivity.class);
        intent.putExtra(Args.RECENT_CHAT_OBJ, obj);
        intent.putExtra(Args.NOTIFICATION_TYPE, notificationType);

        REQUEST_CODE++;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE, intent,
                PendingIntent.FLAG_ONE_SHOT);

//        Uri defaultSoundUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, notifi_chanel)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(obj.getQbUser().getFullName())
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        Log.e("DDD","2");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(obj.getQbUser().getId(), notificationBuilder.build());
    }

    private void sendNewDeal(String messageBody, String notificationType, String idDeal) {

        Log.e(TAG, "sendNewDeal: jump" );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }

        Intent intent = new Intent(this, SplashLoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Bundle bundle = new Bundle();
        bundle.putString(Args.KEY_ID_DEAL, idDeal);
        bundle.putString(Args.NOTIFICATION_TYPE, notificationType);
        intent.putExtras(bundle);

        REQUEST_CODE++;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, notifi_chanel)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        Log.e("DDD","3");
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(Integer.parseInt(idDeal), notificationBuilder.build());
    }

    private void sendMessage(boolean negotiated) {
        // The string "my-integer" will be used to filer the intent
        Intent intent = new Intent(Constants.INTENT_ACTION_UPDATE_MENU);
        // Adding some data
        intent.putExtra(Args.NEGOTIATED, negotiated);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        NotificationManager
                mNotificationManager =
                (NotificationManager)
                        getSystemService(Context.NOTIFICATION_SERVICE);
        // The id of the channel.
        String id = notifi_chanel;
        // The user-visible name of the channel.
        CharSequence name = "Notification";
        // The user-visible description of the channel.
        String description = "Notification Application";
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        // Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        mNotificationManager.createNotificationChannel(mChannel);
    }
}
