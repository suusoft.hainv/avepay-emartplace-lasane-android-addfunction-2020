package com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.bumptech.glide.Glide;


public class FoodActivity extends BaseActivity implements View.OnClickListener {
    private ImageView imgFood, imgRemove;
    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_view;
    }

    @Override
    protected void initView() {
        imgFood  = findViewById(R.id.img_food);
        imgRemove= findViewById(R.id.img_remove);
        imgRemove.setOnClickListener(this);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        Glide.with(self).load(bundle.get("img")).into(imgFood);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view == imgRemove) {
            finish();
        }
    }
}
