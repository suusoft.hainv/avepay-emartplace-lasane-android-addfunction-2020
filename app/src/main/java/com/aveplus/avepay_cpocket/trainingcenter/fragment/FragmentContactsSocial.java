package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.config.GlobalValue;
import com.aveplus.avepay_cpocket.trainingcenter.object.Contact;
import com.google.android.gms.maps.model.LatLng;


public class FragmentContactsSocial extends BaseFragment implements OnClickListener {
    static final LatLng HAMBURG = new LatLng(21.043201, 105.802422);

    private TextView lblcompanyName, lbladdress, lblphone, lblemail,
            lblwebsite;
    private TextView lblFacebook;
    private TextView lblTwitter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("contact");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_contact_social, container, false);
        initViews(rootView);
        getData();
        return rootView;
    }

    @Override
    public void onClick(View v) {
        Contact contact = new Contact();
        switch (v.getId()) {
            case R.id.lblEmail:
                break;

            case R.id.lblPhone:
                break;

            case R.id.lblWebsite:
                break;
        }

    }

    private void getData() {
        Contact contact = new Contact();

//            contact = GlobalValue.pref.getContact();

//        lblcompanyName.setText(contact.getCompanyName());
//        lbladdress.setText(contact.getAddress());
//        lblphone.setText(contact.getPhone());
//        lblwebsite.setText(contact.getWebsite());
//        lblemail.setText(contact.getEmail());
//        lblFacebook.setText(contact.getFacebook());
//        lblTwitter.setText(contact.getTwitter());lblcompanyName.setText(contact.getCompanyName());
        lblcompanyName.setText(getString(R.string.about_company));
        lbladdress.setText(getString(R.string.about_address));
        lblphone.setText(getString(R.string.about_phone));
        lblwebsite.setText(getString(R.string.about_website));
        lblemail.setText(getString(R.string.about_email));
        lblFacebook.setText(getString(R.string.about_facebook));
        lblTwitter.setText("");
    }

    private void initViews(View v) {
        lblcompanyName = (TextView) v.findViewById(R.id.lblCompanyName);
        lbladdress = (TextView) v.findViewById(R.id.lblAddress);
        lblemail = (TextView) v.findViewById(R.id.lblEmail);
        lblphone = (TextView) v.findViewById(R.id.lblPhone);
        lblwebsite = (TextView) v.findViewById(R.id.lblWebsite);
        lblphone = (TextView) v.findViewById(R.id.lblPhone);
        lblFacebook = (TextView) v.findViewById(R.id.lblFacebook);
        lblTwitter = (TextView) v.findViewById(R.id.lblTwitter);
        lblphone.setOnClickListener(this);
        lblemail.setOnClickListener(this);
        lblwebsite.setOnClickListener(this);
    }
}
