package com.aveplus.avepay_cpocket.ebook.base.model.vm;

import android.content.Context;

import androidx.databinding.BaseObservable;

import com.aveplus.avepay_cpocket.ebook.listener.IBaseListener;


/**
 * Created by phamv on 7/18/2017.
 */
public abstract class BaseAdapterVM extends BaseObservable {
    public int position;

    public Context self;

    public abstract void setData(Object object);

    protected IBaseListener listener;


    public BaseAdapterVM(Context self) {
        this.self = self;
    }

    public BaseAdapterVM(Context self, int position) {
        this.self = self;
        this.position = position;
    }

    public IBaseListener getListener() {
        return listener;
    }

    public void setListener(IBaseListener listener) {
        this.listener = listener;
    }
}
