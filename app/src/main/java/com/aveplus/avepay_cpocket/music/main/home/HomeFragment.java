package com.aveplus.avepay_cpocket.music.main.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.music.model.Home;
import com.aveplus.avepay_cpocket.music.model.HomeCategory;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.google.gson.Gson;


import java.util.ArrayList;


/**
 * "Copyright © 2019 SUUSOFT"
 */
public class HomeFragment extends BaseFragment {
    public static final String TAG = HomeFragment.class.getName();
    private RecyclerView rcvCollection;
    private ArrayList<HomeCategory> listData;
    private CategoryAdapter adapter;
    private Home home;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public static final int POSITION_TOP = 4;
    public static final int POSITION_HOT = 3;
    public static final int POSITION_NEW = 2;
    public static final int POSITION_CATEGORY = 1;
    public static final int POSITION_FEATURE = 0;
    public static final int POSITION_SONG = 5;
    private int count = 0;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_home_music;
    }

    @Override
    protected void init() {
    }

    @Override
    protected void initView(View view) {
        AppUtil.hideSoftKeyboard((Activity) self);
        rcvCollection = view.findViewById(R.id.rcv_Collection);
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow, R.color.green, R.color.blue);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                getDataRefresh();
            }
        });
        setUpReclerView();
        setHasOptionsMenu(true);
        getData();

    }

    private void getDataRefresh() {
        getHome();
    }

    @Override
    protected void getData() {
        getHome();
    }

    private void getHome() {
        RequestManager.getHome(self, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (!listData.isEmpty()) {
                    listData.clear();
                }

                home = response.getDataObject(Home.class);
                if (home != null) {
                    convertData();
                    adapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onError(String message) {
                Log.e(TAG, message);
            }
        });

    }


    private void setUpReclerView() {
        listData = new ArrayList<>();
        adapter = new CategoryAdapter(self, listData);
        rcvCollection.setHasFixedSize(true);
        rcvCollection.setLayoutManager(new LinearLayoutManager(self));
        rcvCollection.setAdapter(adapter);

    }

    private void convertData() {
        HomeCategory category;
        count = 0;
        for (int i = 0; i < home.getTotal_list(); i++) {
            category = new HomeCategory();
            if (i == POSITION_FEATURE) {
                category.setType(HomeCategory.FEATURED);
                //  category.getListAlbum ().addAll (home.getPlaylist_title ().get (0).getList_playlist ());
                category.getListAlbum().addAll(home.getPlaylist_banner());
//               category.setName (getString (R.string.home_item_title_top_playlist));
                category.setName("");
            } else if (i == POSITION_CATEGORY) {
                category.setType(HomeCategory.CATEGORY);
                category.getListCategory().addAll(home.getCategories());
//                category.setName (getString (R.string.home_item_category));
                category.setName("Category");
            } else if (i == home.getTotal_list() - 1) {
                category.setType(HomeCategory.SONGS);
                category.setName("New Songs");
//                category.setName (getString (R.string.home_item_title_new_song));
                category.getListSongs().addAll(home.getSongs());
                Log.e(TAG, "onSuccess: " + new Gson().toJson(home.getSongs()));
                // add the first song to queue
                if (home.getSongs().size() > 0)
                    QueueManager.getInstance().addNewQueue(home.getSongs());
            } else {
                category.setType(HomeCategory.NEW);
                category.getListAlbum().clear();
                category.setId(String.valueOf(home.getPlaylist_title().get(count).getId()));
                category.getListAlbum().addAll(home.getPlaylist_title().get(count).getList_playlist());
                category.setName(home.getPlaylist_title().get(count).getTitle());
                count++;
            }
            listData.add(category);
        }
    }

}
