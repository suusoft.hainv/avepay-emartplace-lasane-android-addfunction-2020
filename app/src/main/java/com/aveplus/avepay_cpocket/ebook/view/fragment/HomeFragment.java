package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseFragment;
import com.aveplus.avepay_cpocket.ebook.model.Category;
import com.aveplus.avepay_cpocket.ebook.model.Dashboard;
import com.aveplus.avepay_cpocket.ebook.model.HomeCategory;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;
import com.aveplus.avepay_cpocket.ebook.network.NetworkUtility;
import com.aveplus.avepay_cpocket.ebook.view.adapter.ItemHomeAdapter;

import java.util.ArrayList;


/**
 * Created by Trang on 6/29/2017.
 */
public class HomeFragment extends BaseFragment {
    public static final String TAG = HomeFragment.class.getName();
    private RecyclerView rcvCollection;
    private View tvNotifi;
    private ArrayList<HomeCategory> listData;
    private ItemHomeAdapter adapter;
    private Dashboard home;

    public static final int POSITION_TOP = 4;
    public static final int POSITION_HOT = 3;
    public static final int POSITION_NEW = 2;
    public static final int POSITION_CATEGORY = 1;
    public static final int POSITION_FEATURE = 0;
    public static final int POSITION_SONG = 5;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_home_ebook;
    }

    @Override
    protected void init() {
        //setHasOptionsMenu(false);
    }

    @Override
    protected void initView(View view) {
        rcvCollection = (RecyclerView) view.findViewById(R.id.rcv_Collection);
        tvNotifi = view.findViewById(R.id.lo_no_internet);
        setUpReclerView();
        getData();

    }


    @Override
    protected void getData() {
        if (NetworkUtility.isNetworkAvailable()) {
            getHome();
        } else {
            rcvCollection.setVisibility(View.GONE);
            tvNotifi.setVisibility(View.VISIBLE);
        }

    }

    private void getHome() {
        RequestManager.getLastest(self, "1", new ApiManager.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (!listData.isEmpty()) {
                    listData.clear();
                }
                home = response.getDataObject(Dashboard.class);
                home.setCategories(AppController.getInstance().getListCategories());
                convertData();
                adapter.notifyDataSetChanged();
                checkCategoryList();
            }

            @Override
            public void onError(String message) {

            }
        });


    }

    private void checkCategoryList() {
        if (home.getCategories().size() == 0) {
            getCategories();
        }
    }


    private void setUpReclerView() {
        listData = new ArrayList<>();
        adapter = new ItemHomeAdapter(self, listData);
        rcvCollection.setHasFixedSize(true);
        rcvCollection.setLayoutManager(new LinearLayoutManager(self));
        rcvCollection.setAdapter(adapter);

    }

    private void convertData() {
        HomeCategory category;
        for (int i = 0; i < 5; i++) {
            category = new HomeCategory();
            if (i == POSITION_FEATURE) {
                category.setType(HomeCategory.FEATURED);
                category.getListBooks().addAll(home.getListTop());
                category.setName(self.getString(R.string.features));
            } else if (i == POSITION_CATEGORY) {
                category.setType(HomeCategory.CATEGORY);
                category.getListCategory().addAll(home.getCategories());
                category.setName(self.getString(R.string.category));
            } else if (i == POSITION_NEW) {
                category.setType(HomeCategory.NEW);
                category.getListBooks().addAll(home.getmListBookNew());
                category.setName(self.getString(R.string.home_new));
            } else if (i == POSITION_HOT) {
                category.setType(HomeCategory.HOT);
                category.getListBooks().addAll(home.getmListBookHot());
                category.setName(self.getString(R.string.home_hot));
            } else if (i == POSITION_TOP) {
                category.setType(HomeCategory.TOP);
                category.getListBooks().addAll(home.getmListBookFeature());
                category.setName(self.getString(R.string.home_feature));
            }
            listData.add(category);
        }
    }

    private void getCategories() {
        RequestManager.getCategory(self, new ApiManager.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                ArrayList<Category> list = (ArrayList<Category>) response.getDataList(Category.class);
                AppController.getInstance().setListCategories(list);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String message) {
                Log.e("HomeFragment", "can not get categoires");
            }
        });
    }


}
