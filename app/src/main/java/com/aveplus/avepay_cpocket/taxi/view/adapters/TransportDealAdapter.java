package com.aveplus.avepay_cpocket.taxi.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.interfaces.ITransportDeal;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.utils.DateTimeUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;

import java.util.ArrayList;

/**
 * Created by SuuSoft.com on 10/15/2015.
 */
public class TransportDealAdapter extends RecyclerView.Adapter<TransportDealAdapter.ViewHolder> {

    private Context context;
    private ArrayList<TripObj> transportDeals;
    private ITransportDeal iTransportDeal;

    public TransportDealAdapter(Context context, ArrayList<TripObj> dealObjs, ITransportDeal iTransportDeal) {
        this.context = context;
        this.transportDeals = dealObjs;
        this.iTransportDeal = iTransportDeal;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transport_deal_taxi, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (getItemCount() > 0) {
            final TripObj tripObj = transportDeals.get(position);
            if (tripObj != null) {
                holder.lblTripId.setText(String.format(context.getString(R.string.trip_value), tripObj.getId()));
                holder.lblTime.setText(DateTimeUtil.convertTimeStampToDate(tripObj.getTime(), "HH:mm, EEE dd/MM/yyyy"));
                holder.lblFrom.setText(tripObj.getPickup());
                holder.lblTo.setText(tripObj.getDestination());

                String routeDistance = "";
                if (tripObj.getRouteDistance() >= 100) {
                    routeDistance = String.format(context.getString(R.string.value_km),
                            StringUtil.convertNumberToString((tripObj.getRouteDistance() / 1000), 1));
                } else {
                    routeDistance = String.format(context.getString(R.string.value_meter),
                            StringUtil.convertNumberToString(tripObj.getRouteDistance(), 0));
                }
                holder.lblDistance.setText(routeDistance);

                holder.lblETA.setText(DateTimeUtil.convertTimeStampToDate(tripObj.getRouteDuration() + tripObj.getTime(),
                        "HH:mm"));
                holder.lblFare.setText(String.format(context.getString(R.string.dollar_value),
                        StringUtil.convertNumberToString(tripObj.getEstimateFare(), 1)));

                holder.imgCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        iTransportDeal.onCancel(tripObj);
                    }
                });

                holder.lblTrack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        iTransportDeal.onTracking(tripObj);
                    }
                });

                holder.lblFinish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        iTransportDeal.onFinish(tripObj);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        try {
            return transportDeals.size();
        } catch (NullPointerException ex) {
            return 0;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgCancel;
        private TextViewRegular lblTripId, lblTime, lblFrom, lblTo, lblDistance, lblETA, lblFare;
        private TextViewBold lblTrack, lblFinish;

        public ViewHolder(View view) {
            super(view);

            imgCancel = (ImageView) view.findViewById(R.id.img_cancel);
            lblTripId = (TextViewRegular) view.findViewById(R.id.lbl_trip_id);
            lblTime = (TextViewRegular) view.findViewById(R.id.lbl_time);
            lblFrom = (TextViewRegular) view.findViewById(R.id.lbl_from);
            lblTo = (TextViewRegular) view.findViewById(R.id.lbl_to);
            lblDistance = (TextViewRegular) view.findViewById(R.id.lbl_distance);
            lblETA = (TextViewRegular) view.findViewById(R.id.lbl_eta);
            lblFare = (TextViewRegular) view.findViewById(R.id.lbl_fare);
            lblTrack = (TextViewBold) view.findViewById(R.id.lbl_track);
            lblFinish = (TextViewBold) view.findViewById(R.id.lbl_finish);
        }
    }
}
