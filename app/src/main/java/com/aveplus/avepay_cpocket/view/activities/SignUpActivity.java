package com.aveplus.avepay_cpocket.view.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

import com.aveplus.avepay_cpocket.music.util.ImageUtil;
import com.aveplus.avepay_cpocket.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.retrofit.response.ResponseUser;
import com.aveplus.avepay_cpocket.utils.FileUtil;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.material.textfield.TextInputLayout;

import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.objects.DataPart;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.aveplus.avepay_cpocket.quickblox.conversation.utils.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.DateTimeUtil;
import com.aveplus.avepay_cpocket.utils.StringUtil;
import com.aveplus.avepay_cpocket.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.widgets.edittext.CustomEditText;
import com.aveplus.avepay_cpocket.widgets.tabLayout.TabIndicatorLine;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewCondensedItalic;

import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_AVATAR;
import static com.aveplus.avepay_cpocket.utils.AppUtil.PICK_IMAGE_REQUEST_CODE;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private final String TAG = SignUpActivity.class.getSimpleName();
    private static final int RC_ADDRESS = 134;
    ProgressBar progressBar;
    private LinearLayout llSignUp;
    private RelativeLayout rlSignUp;
    private TabIndicatorLine tabIndicatorLine;
    private TextInputLayout layoutFullName, layoutEmail, layoutPassword, layoutRetypePassword,
            layoutAddress, layoutZipcode, layoutGenre, layoutBirthday;
    private LinearLayout llStep1, llStep2, llStep3, layoutPhoneNumber;

    private CircleImageView imgAvatar;
    private ImageView imgSignUp;
    private EditText mTxtFullName, mTxtEmail, mTxtPhoneNumber, mTxtPassword, mTxtRetypePassword,
            mTxtZipcode;
    private CustomEditText mTxtAddress, mTxtGenre, mTxtBrithday;
    private String mFullName, mEmail, mPassword, mRetypePassword,
            mAddress, mZipcode, mPhoneNumber,
            mGender, mAvatar, mBirthday;
    private DataPart avatar = null;

    private TextView mLblCreateAccount;
    private TextViewCondensedItalic mLblAlreadyAMember;
    private CheckBox mChkRememberMe;
    private TextView tvPhoneCode;
    private boolean mIsRegistered;
    private Dialog dialogGender;
    private RadioButton rbFemale, rbMale;


    private String countryCodeSelected = "";

    private String pathImage = "";
    private MultipartBody.Part partImage1 = null;
    private RequestBody RQFullName, RQEmail, RQPassword, RQPhone, RQAddress, RQGender, RQBirthday;
    private File file1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void inflateLayout() {
        setContentView(R.layout.activity_sign_up_reskin_scroll);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void initUI() {
        // Hide actionbar
        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        llSignUp = (LinearLayout) findViewById(R.id.ll_sign_up);
        rlSignUp = (RelativeLayout) findViewById(R.id.rl_sign_up);
        imgAvatar = (CircleImageView) findViewById(R.id.img_avatar);
        imgSignUp = (ImageView) findViewById(R.id.img_sign_up);
        tabIndicatorLine = (TabIndicatorLine) findViewById(R.id.tab_indicator_line);
        mLblCreateAccount = (TextView) findViewById(R.id.lbl_create_account);
        mLblAlreadyAMember = (TextViewCondensedItalic) findViewById(R.id.lbl_already_a_member);
        mChkRememberMe = (CheckBox) findViewById(R.id.chk_remember_me);
        tvPhoneCode = (TextView) findViewById(R.id.tv_phone_code);
        llSignUp.getLayoutParams().height = AppUtil.getScreenHeight(this) - AppUtil.getStatusBarHeight(this);

        llStep1 = (LinearLayout) findViewById(R.id.llStep1);
        llStep2 = (LinearLayout) findViewById(R.id.llStep2);
        llStep3 = (LinearLayout) findViewById(R.id.llStep3);
        mTxtPhoneNumber = (EditText) findViewById(R.id.txt_phone);
        mTxtFullName = (EditText) findViewById(R.id.txt_full_name);
        mTxtEmail = (EditText) findViewById(R.id.txt_email);
        mTxtPassword = (EditText) findViewById(R.id.txt_password);
        mTxtRetypePassword = (EditText) findViewById(R.id.txt_retype_password);
        mTxtZipcode = (EditText) findViewById(R.id.txt_zipcode);
        mTxtGenre = (CustomEditText) findViewById(R.id.txt_genre);
        mTxtAddress = (CustomEditText) findViewById(R.id.txt_address);
        mTxtAddress.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        mTxtBrithday = (CustomEditText) findViewById(R.id.txt_birthday);

        layoutFullName = (TextInputLayout) findViewById(R.id.layout_full_name);
        layoutEmail = (TextInputLayout) findViewById(R.id.layout_email);
        layoutPassword = (TextInputLayout) findViewById(R.id.layout_password);
        layoutRetypePassword = (TextInputLayout) findViewById(R.id.layout_retype_password);
        layoutAddress = (TextInputLayout) findViewById(R.id.layout_address);
        layoutZipcode = (TextInputLayout) findViewById(R.id.layout_zipcode);
//        layoutGenre             = (TextInputLayout) findViewById(R.id.layout_genre);
//        layoutBirthday          = (TextInputLayout) findViewById(R.id.layout_birthday);
        layoutPhoneNumber = (LinearLayout) findViewById(R.id.layout_phonenumber);

        mTxtAddress.setDrawableClickListener(clickListenerAddress);

        mTxtGenre.setDrawableClickListener(clickListenerGenre);

        mTxtBrithday.setDrawableClickListener(clickListenerBirthday);

        showScreenStep1();

        getCountryCode();
    }

    private CustomEditText.DrawableClickListener clickListenerAddress = new CustomEditText.DrawableClickListener() {
        @Override
        public void onClick(DrawablePosition target) {
            switch (target) {
                case RIGHT:
                    //Do something here
                    MapsUtil.getAutoCompletePlaces(self, RC_ADDRESS);

                    break;

                default:
                    break;
            }
        }
    };

    private CustomEditText.DrawableClickListener clickListenerBirthday = new CustomEditText.DrawableClickListener() {
        @Override
        public void onClick(DrawablePosition target) {
            switch (target) {
                case RIGHT:
                    //Do something here
                    processingShowDialogDate();

                    break;

                default:
                    break;
            }
        }
    };

    private CustomEditText.DrawableClickListener clickListenerGenre = new CustomEditText.DrawableClickListener() {
        @Override
        public void onClick(DrawablePosition target) {
            switch (target) {
                case RIGHT:
                    //Do something here
                    processingShowDialogGender();

                    break;

                default:
                    break;
            }
        }
    };

    private void processingShowDialogGender() {
        if (dialogGender == null) {
            dialogGender = DialogUtil.setDialogCustomView(self, R.layout.dialog_choose_gender, false);
            rbMale = (RadioButton) dialogGender.findViewById(R.id.rb_male);
            rbFemale = (RadioButton) dialogGender.findViewById(R.id.rb_female);
        }


        if (mTxtGenre.getText().equals(getResources().getString(R.string.male))) {
            rbMale.setChecked(true);
        } else if (mTxtGenre.getText().equals(getResources().getString(R.string.female))) {
            rbFemale.setChecked(true);
        }
        dialogGender.show();

        rbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTxtGenre.setText(getResources().getString(R.string.male));
                dialogGender.dismiss();

            }
        });

        rbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTxtGenre.setText(getResources().getString(R.string.female));
                dialogGender.dismiss();
            }
        });

    }

    private void processingShowDialogDate() {
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Log.e("date", "y/m/d=" + year + "/" + monthOfYear + "/" + dayOfMonth);
                mTxtBrithday.setText(DateTimeUtil.convertDateToString(myCalendar.getTime(), DateTimeUtil.FORMAT_DATE));
            }

        };


        new DatePickerDialog(self, R.style.MyDatePickerStyle, date, 2000 /* myCalendar
                .get(Calendar.YEAR)*/, myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();


        Log.e("date1", "y/m/d=" + myCalendar
                .get(Calendar.YEAR) + "/" + myCalendar
                .get(Calendar.MONTH) + "/" + myCalendar
                .get(Calendar.DAY_OF_MONTH));
    }

    private void showScreenStep1() {
        tabIndicatorLine.setSteps(TabIndicatorLine.Steps.STEPS1);
        imgSignUp.setImageResource(R.drawable.ic_piggybank);
        imgSignUp.setVisibility(View.VISIBLE);
        imgAvatar.setVisibility(View.GONE);
        mLblCreateAccount.setText(R.string.continue_);

        llStep1.setVisibility(View.VISIBLE);
        llStep2.setVisibility(View.GONE);
        llStep3.setVisibility(View.GONE);
    }

    private void showScreenStep2() {
        tabIndicatorLine.setSteps(TabIndicatorLine.Steps.STEPS2);
        imgSignUp.setImageResource(R.drawable.ic_call_text);
        imgSignUp.setVisibility(View.VISIBLE);
        imgAvatar.setVisibility(View.GONE);
        mLblCreateAccount.setText(R.string.continue_);

//        mTxtAddress.setText("");
//        mTxtZipcode.setText("");
//        mTxtGenre.setText("");

        llStep1.setVisibility(View.GONE);
        llStep2.setVisibility(View.VISIBLE);
        llStep3.setVisibility(View.GONE);

    }


    private void showScreenStep3() {
        tabIndicatorLine.setSteps(TabIndicatorLine.Steps.STEPS3);
        imgAvatar.setVisibility(View.VISIBLE);
        imgSignUp.setVisibility(View.GONE);
        mLblCreateAccount.setText(R.string.start_now);

//        mTxtBrithday.setText("");
//        mTxtPhoneNumber.setText("");

        llStep1.setVisibility(View.GONE);
        llStep2.setVisibility(View.GONE);
        llStep3.setVisibility(View.VISIBLE);
    }

    @Override
    protected void initControl() {
        imgAvatar.setOnClickListener(this);
        mTxtAddress.setOnClickListener(this);
        mLblCreateAccount.setOnClickListener(this);
        mLblAlreadyAMember.setOnClickListener(this);
        tvPhoneCode.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mTxtAddress) {

        } else if (v == imgAvatar) {
            chooseImage();
        } else if (v == mLblCreateAccount) {
            //Check processing in steps?
            if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS1) {
                if (isValidStep1())
                    showScreenStep2();
            } else if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS2) {
                if (isValidStep2())
                    showScreenStep3();
            } else if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS3) {
                if (isValidStep3()) {
                    createAccount();
                }
            }

        } else if (v == mLblAlreadyAMember) {
            GlobalFunctions.startActivityWithoutAnimation(self, SplashLoginActivity.class);
            finish();
        } else if (v == tvPhoneCode) {
//            tvPhoneCode.setBackgroundResource(R.drawable.bg_border_grey);
            GlobalFunctions.startActivityForResult(this, PhoneCountryListActivity.class, Args.RQ_GET_PHONE_CODE);
        }
    }

    private void chooseImage() {
        AppUtil.pickImage(this, PICK_IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {

            ImageUtil.setImageFromUri(imgAvatar, data.getData());
            pathImage = FileUtil.getPath(self, data.getData()).trim();
            Log.e(TAG, "onActivityResult: " + pathImage);
            file1 = new File(pathImage);


        }


        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == PICK_IMAGE_REQUEST_CODE) {
                AppUtil.setImageFromUri(imgAvatar, data.getData());

            } else if (requestCode == Args.RQ_GET_PHONE_CODE) {
                countryCodeSelected = data.getExtras().getString(Args.KEY_PHONE_CODE);
                tvPhoneCode.setText(countryCodeSelected);
            } else if (requestCode == RC_ADDRESS) {
                if (resultCode == -1) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    AppUtil.fillAddress(self, mTxtAddress, place);
                    mTxtZipcode.requestFocus();
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(self, data);
                    Log.e(TAG, status.getStatusMessage());
                    mTxtAddress.requestFocus();
                } else if (resultCode == 0) {
                    mTxtAddress.requestFocus();
                    // The user canceled the operation.
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS3) {
            showScreenStep2();
        } else if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS2) {
            showScreenStep1();
        } else if (tabIndicatorLine.getSteps() == TabIndicatorLine.Steps.STEPS1) {
            GlobalFunctions.startActivityWithoutAnimation(self, SplashLoginActivity.class);
            finish();
        }

    }

    private void getCountryCode() {
        String[] rl = getResources().getStringArray(R.array.CountryCodes);
        int curPosition = AppUtil.getCurentPositionCountryCode(this);
        String phoneCode = rl[curPosition].split(",")[0];
        tvPhoneCode.setText(phoneCode);
    }

    private void gotoLoginPage() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Args.IS_FROM_SIGNUP, true);
        if (mIsRegistered && mChkRememberMe.isChecked()) {
            String email = mTxtEmail.getText().toString().trim();
            String password = mTxtPassword.getText().toString().trim();

            if (!email.isEmpty()) {
                bundle.putString(Args.EMAIL, email);
            }
            if (!password.isEmpty()) {
                bundle.putString(Args.PASSWORD, password);
            }

        }

        GlobalFunctions.startActivityWithoutAnimation(self, LoginActivity.class, bundle);
        finish();
    }


    private void createAccount() {
        part();
        account();

    }

    private void account() {
        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        ApiUtils.getAPIService().register(RQFullName, RQEmail, RQPassword, RQPhone, RQAddress, RQGender, RQBirthday, partImage1).enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                if (response.body().isSuccess(self)) {
                    if (response.body() != null) {
                        AppUtil.startActivity(self, LoginActivity.class);
                        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                        intent.putExtra("email", mTxtEmail.getText().toString());
                        intent.putExtra("password", mTxtRetypePassword.getText().toString());
                        startActivity(intent);
                        AppUtil.showToast(self, "Register success");
                        progressBar.setVisibility(View.GONE);
                        finish();

                    } else {
                        progressBar.setVisibility(View.GONE);

                        Toast.makeText(SignUpActivity.this, "Some thing went wrong.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void part() {
        parseMultipart();
        String name = mTxtFullName.getText().toString();
        String email = mTxtEmail.getText().toString();
        String password = mTxtRetypePassword.getText().toString();
        String phone = tvPhoneCode.getText().toString().trim() + " " + mPhoneNumber;
        String address = mTxtAddress.getText().toString();
        String gender = mTxtGenre.getText().toString();
        String birthday = mTxtBrithday.getText().toString();


        RQFullName = RequestBody.create(MediaType.parse("text/plain"), name);
        RQEmail = RequestBody.create(MediaType.parse("text/plain"), email);
        RQPassword = RequestBody.create(MediaType.parse("text/plain"), password);
        RQPhone = RequestBody.create(MediaType.parse("text/plain"), phone);
        RQAddress = RequestBody.create(MediaType.parse("text/plain"), address);
        RQGender = RequestBody.create(MediaType.parse("text/plain"), gender);
        RQBirthday = RequestBody.create(MediaType.parse("text/plain"), birthday);

    }

    private void parseMultipart() {
        if (!pathImage.isEmpty()) {
            Log.e(TAG, "parseMultipart: " + pathImage);
            File fileImage = new File(pathImage);
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), fileImage);
            partImage1 = MultipartBody.Part.createFormData(PARAM_AVATAR, fileImage.getName(), fileReqBody);
        }
    }

    private boolean isValidStep1() {
        mFullName = mTxtFullName.getText().toString().trim();
        mEmail = mTxtEmail.getText().toString().trim();
        mPassword = mTxtPassword.getText().toString().trim();
        mRetypePassword = mTxtRetypePassword.getText().toString().trim();

        if (mFullName.isEmpty()) {
            AppUtil.showToast(self, R.string.msg_name_is_required);
            mTxtFullName.requestFocus();

            return false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            AppUtil.showToast(self, R.string.msg_email_is_required);
            mTxtEmail.requestFocus();

            return false;
        }

        if (!StringUtil.isValidatePassword(mPassword)) {
            AppUtil.showToast(self, R.string.msg_password_is_required);
            mTxtPassword.requestFocus();

            return false;
        }
        if (!mRetypePassword.equals(mPassword)) {
            AppUtil.showToast(self, R.string.msg_password_is_not_match);
            mTxtRetypePassword.requestFocus();

            return false;
        }

        return true;
    }

    private boolean isValidStep2() {

        mPhoneNumber = mTxtPhoneNumber.getText().toString().trim();
        mAddress = mTxtAddress.getText().toString().trim();
        mZipcode = mTxtZipcode.getText().toString().trim();

        if (mPhoneNumber.isEmpty()) {
            AppUtil.showToast(self, R.string.msg_phone_is_required);
            mTxtPhoneNumber.requestFocus();

            return false;
        }
        if (mAddress.isEmpty()) {
            AppUtil.showToast(self, R.string.msg_address_is_required);
            mTxtAddress.requestFocus();

            return false;
        }
        if (mZipcode.isEmpty()) {
            AppUtil.showToast(self, R.string.msg_zipcode_is_required);
            mTxtZipcode.requestFocus();

            return false;
        }
        return true;
    }

    private boolean isValidStep3() {
        if (imgAvatar.getDrawable() != getResources().getDrawable(R.drawable.ic_cam)) {
            avatar = new DataPart("avatar.png", AppUtil.getFileDataFromDrawable(self, imgAvatar.getDrawable()), DataPart.TYPE_IMAGE);
        }
        mGender = mTxtGenre.getText().toString().trim();
        mBirthday = mTxtBrithday.getText().toString().trim();

        if (imgAvatar == null) {
            AppUtil.showToast(self, R.string.msg_image_is_required);

            return false;
        }
        if (mGender.isEmpty()) {
            AppUtil.showToast(self, R.string.msg_gneder_is_required);
            mTxtGenre.requestFocus();

            return false;
        }
        if (mBirthday.isEmpty()) {
            AppUtil.showToast(self, R.string.msg_birthday_is_required);
            mTxtBrithday.requestFocus();

            return false;
        }


        return true;
    }


    private boolean isValid() {
        String fullName = mTxtFullName.getText().toString().trim();
        String email = mTxtEmail.getText().toString().trim();
        String phoneNumber = mTxtPhoneNumber.getText().toString().trim();
        String password = mTxtPassword.getText().toString().trim();
        String retypePassword = mTxtRetypePassword.getText().toString().trim();

        if (fullName.isEmpty()) {
            Toast.makeText(self, R.string.msg_name_is_required, Toast.LENGTH_SHORT).show();
            mTxtFullName.requestFocus();

            return false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(self, R.string.msg_email_is_required, Toast.LENGTH_SHORT).show();
            mTxtEmail.requestFocus();

            return false;
        }
        if (phoneNumber.isEmpty()) {
            Toast.makeText(self, R.string.msg_phone_is_required, Toast.LENGTH_SHORT).show();
            mTxtPhoneNumber.requestFocus();

            return false;
        }
        if (!StringUtil.isValidatePassword(password)) {
            Toast.makeText(self, R.string.msg_password_is_required, Toast.LENGTH_LONG).show();
            mTxtPassword.requestFocus();

            return false;
        }
        if (!retypePassword.equals(password)) {
            Toast.makeText(self, R.string.msg_password_is_not_match, Toast.LENGTH_SHORT).show();
            mTxtRetypePassword.requestFocus();

            return false;
        }

        return true;
    }

    private void getListCode() {

        Map<String, String> languagesMap = new TreeMap<>();

        Locale[] locales = Locale.getAvailableLocales();

        for (Locale obj : locales) {

            if ((obj.getDisplayCountry() != null) && (!"".equals(obj.getDisplayCountry()))) {
                languagesMap.put(obj.getCountry(), obj.getLanguage());
                Log.e(TAG, "country: " + obj.getCountry());
            }

        }
    }


}
