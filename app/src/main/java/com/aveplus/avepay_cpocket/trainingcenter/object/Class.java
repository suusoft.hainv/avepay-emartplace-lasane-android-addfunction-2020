package com.aveplus.avepay_cpocket.trainingcenter.object;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by SuuSoft on 29/08/2017.
 */

public class Class implements Parcelable {
    private String nameClass;
    private String mStartDate;
    private String mEndDate;
    private String idClass;
    private boolean isExpired;
    private ArrayList<Schedule> mListSchedule;

    public Class(String idClass, String nameClass, String mStartDate, String mEndDate, ArrayList<Schedule> mListSchedule) {
        this.idClass = idClass;
        this.nameClass = nameClass;
        this.mStartDate = mStartDate;
        this.mEndDate = mEndDate;
        this.mListSchedule = mListSchedule;
    }


    protected Class(Parcel in) {
        nameClass = in.readString();
        mStartDate = in.readString();
        mEndDate = in.readString();
        idClass = in.readString();
        isExpired = in.readByte() != 0;
        mListSchedule = in.createTypedArrayList(Schedule.CREATOR);
    }

    public static final Creator<Class> CREATOR = new Creator<Class>() {
        @Override
        public Class createFromParcel(Parcel in) {
            return new Class(in);
        }

        @Override
        public Class[] newArray(int size) {
            return new Class[size];
        }
    };

    public String getNameClass() {
        return nameClass;
    }

    public void setNameClass(String nameClass) {
        this.nameClass = nameClass;
    }

    public String getmStartDate() {
        return mStartDate;
    }

    public void setmStartDate(String mStartDate) {
        this.mStartDate = mStartDate;
    }

    public String getIdClass() {
        return idClass;
    }

    public void setIdClass(String idClass) {
        this.idClass = idClass;
    }

    public String getmEndDate() {
        return mEndDate;
    }

    public void setmEndDate(String mEndDate) {
        this.mEndDate = mEndDate;
    }

    public ArrayList<Schedule> getmListSchedule() {
        return mListSchedule;
    }

    public void setmListSchedule(ArrayList<Schedule> mListSchedule) {
        this.mListSchedule = mListSchedule;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean expired) {
        isExpired = expired;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameClass);
        dest.writeString(mStartDate);
        dest.writeString(mEndDate);
        dest.writeString(idClass);
        dest.writeByte((byte) (isExpired ? 1 : 0));
        dest.writeTypedList(mListSchedule);
    }
}
