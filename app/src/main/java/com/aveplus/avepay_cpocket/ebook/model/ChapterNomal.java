package com.aveplus.avepay_cpocket.ebook.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Suusoft on 9/29/2017.
 */
public class ChapterNomal implements Parcelable {
    private String id;
    private String quoteIndex;
    private String content;
    private String chapterId;
    private String image;

    public String getId() {
        return id;
    }

    public String getQuoteIndex() {
        return quoteIndex;
    }

    public String getContent() {
        return content;
    }

    public String getChapterId() {
        return chapterId;
    }

    public String getImage() {
        return image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.quoteIndex);
        dest.writeString(this.content);
        dest.writeString(this.chapterId);
        dest.writeString(this.image);
    }

    public ChapterNomal() {
    }

    protected ChapterNomal(Parcel in) {
        this.id = in.readString();
        this.quoteIndex = in.readString();
        this.content = in.readString();
        this.chapterId = in.readString();
        this.image = in.readString();
    }

    public static final Creator<ChapterNomal> CREATOR = new Creator<ChapterNomal>() {
        @Override
        public ChapterNomal createFromParcel(Parcel source) {
            return new ChapterNomal(source);
        }

        @Override
        public ChapterNomal[] newArray(int size) {
            return new ChapterNomal[size];
        }
    };
}
