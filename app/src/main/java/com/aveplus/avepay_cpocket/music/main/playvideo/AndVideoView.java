package com.aveplus.avepay_cpocket.music.main.playvideo;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.VideoView;


public class AndVideoView extends VideoView {
    public interface OnPlayPauseListener {
        public void onPlay();
        public void onPause();
    }

    private OnPlayPauseListener mPlayPauseListener;

    public AndVideoView(Context context) {
        super(context);
    }

    public AndVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AndVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AndVideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    public void setPlayPauseListener(OnPlayPauseListener playPauseListener) {
        mPlayPauseListener = playPauseListener;
    }

    @Override
    public void pause() {
        super.pause();
        if (mPlayPauseListener != null) {
            mPlayPauseListener.onPause();
        }
    }

    @Override
    public void start() {
        super.start();
        if (mPlayPauseListener != null) {
            mPlayPauseListener.onPlay();
        }
    }

}
