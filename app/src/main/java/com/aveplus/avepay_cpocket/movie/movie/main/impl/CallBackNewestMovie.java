package com.aveplus.avepay_cpocket.movie.movie.main.impl;


import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface CallBackNewestMovie {
    public void success(Movie movie);
}
