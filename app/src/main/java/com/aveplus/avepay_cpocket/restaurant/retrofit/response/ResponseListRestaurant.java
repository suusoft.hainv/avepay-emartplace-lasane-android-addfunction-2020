package com.aveplus.avepay_cpocket.restaurant.retrofit.response;
/**
 * Created by suusoft.com on 11/12/19.
 */

import com.aveplus.avepay_cpocket.restaurant.main.home.RestaurantModel;

import java.util.ArrayList;

public class ResponseListRestaurant extends BaseModel {
    private ArrayList<RestaurantModel> data;


    public ArrayList<RestaurantModel> getData() {
        return data;
    }

    public void setData(ArrayList<RestaurantModel> data) {
        this.data = data;
    }
}
