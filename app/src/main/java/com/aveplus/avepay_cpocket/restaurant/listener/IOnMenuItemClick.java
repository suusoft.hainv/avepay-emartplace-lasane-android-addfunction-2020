package com.aveplus.avepay_cpocket.restaurant.listener;

import android.view.MenuItem;

/**
 * Created by suusoft.com on 11/20/17.
 */

public interface IOnMenuItemClick {

    void onMenuItemClick(MenuItem item);
}
