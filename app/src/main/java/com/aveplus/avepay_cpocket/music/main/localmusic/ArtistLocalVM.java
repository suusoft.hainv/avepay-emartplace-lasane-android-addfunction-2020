package com.aveplus.avepay_cpocket.music.main.localmusic;

import android.content.Context;
import android.os.AsyncTask;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.model.Artist;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ArtistLocalVM extends BaseViewModelList {

    private ArrayList<Artist> list;

    public ArtistLocalVM(Context context) {
        super(context);
        getData(1);
    }

    @Override
    public void getData(int page) {
        new AsyncTask<String, String, ArrayList<Artist>>(){

            @Override
            protected ArrayList<Artist> doInBackground(String... strings) {
                list = LocalContentWrapper.getArtist(self);
                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Artist> list) {
                super.onPostExecute(list);
                addListData(list);
                checkLoadingMoreComplete(1);

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
