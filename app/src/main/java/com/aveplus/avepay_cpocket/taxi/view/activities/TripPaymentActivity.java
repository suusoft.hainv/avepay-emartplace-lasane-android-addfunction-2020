package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.BillMap;
import com.aveplus.avepay_cpocket.taxi.configs.Action;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.taxi.objects.PaymentMethodObj;
import com.aveplus.avepay_cpocket.taxi.objects.ReservationObj;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseIpn;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseOnlinePayment;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.DialogUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.view.negociate.WaitingDriverEstimateActivity;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TripPaymentActivity extends BaseActivity {

    private static final String TAG = TripPaymentActivity.class.getSimpleName();

    private int mTransactionFee = 1; // Set 1 credit as default

    private TripObj mTripObj;
    private ReservationObj mReservation;

    private TextView mLblDollar, btnPay;
    private TextView mLblCredits, mLblMonetary, mLblTotal;
    private EditText mTxtAmount;
    private RecyclerView mRcl;
    private LinearLayout mLlNote;
    private TextView btnCancel;

    private String mSelectedPayment = PaymentMethodObj.CASH;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    void inflateLayout() {
        setContentView(R.layout.activity_trip_finishing_taxi);
    }

    @Override
    protected void getExtraValues() {
        super.getExtraValues();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Args.KEY_TRIP_OJBECT)) {
                mTripObj = bundle.getParcelable(Args.KEY_TRIP_OJBECT);

                try {
                    mTransactionFee = Integer.parseInt(DataStoreManager.getSettingUtility().getTrip_payment_fee());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (bundle.containsKey(Args.RESERVATION_DEAL_OBJ)) {
                mReservation = bundle.getParcelable(Args.RESERVATION_DEAL_OBJ);

                try {
                    mTransactionFee = Integer.parseInt(DataStoreManager.getSettingUtility().getDeal_payment_fee());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    void initUI() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        // Show as up button
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        mLblDollar = findViewById(R.id.lbl_dollar);
        btnPay = findViewById(R.id.lbl_pay);
        mLblCredits = findViewById(R.id.lbl_credits);
        mLblMonetary = findViewById(R.id.lbl_monetary_unit);
        mLblTotal = findViewById(R.id.lbl_total);
        mTxtAmount = findViewById(R.id.txt_amount);
        mLlNote = findViewById(R.id.ll_note);
        btnCancel = findViewById(R.id.btn_cancel);

        mRcl = findViewById(R.id.rcl_payment_methods);
        mRcl.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));
        mRcl.setHasFixedSize(true);

        // Should call this methods at the end of declaring UI
        initData();
        initPaymentMethod();
    }

    @Override
    void initControl() {
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkAndPay();
            }
        });

        mTxtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                updateTotalFare();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtil.showAlertDialog(self, R.string.app_name, R.string.do_you_want_to_cancel_trip, new DialogUtil.IDialogConfirm() {
                    @Override
                    public void onClickOk() {

                        onCancel();

                    }
                });
            }
        });
    }


    private void onCancel() {
        RequestManager.actionTrip(self, mTripObj, Action.ACTION_CANCEL, "", "", "", "", new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                AppUtil.showToast(self, R.string.the_trip_was_canceled);
                finish();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }


    private void checkAndPay() {
        if (mSelectedPayment.equals(PaymentMethodObj.MNT_MONEY)) {

            requestPaymentBillmap();
        } else {
            onPay();
        }
    }

    private void onPay() {

        RequestManager.payTrip(self, mTripObj, mSelectedPayment, mTripObj.getEstimateFare() + "", new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {

                if (!response.isError()) {
                    AppUtil.showToast(self, R.string.payment_success);

                    mTripObj = response.getDataObject(TripObj.class);

                    //TripManager.directionTrip(self, mTripObj, TAG);

                    AppUtil.showToast(self, R.string.you_are_on_a_trip);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Args.KEY_TRIP_OJBECT, mTripObj);
                    AppUtil.startActivity(self, WaitingDriverEstimateActivity.class, bundle);

                    finish();

                } else {
                    AppUtil.showToast(self, response.getMessageFromCode());
                }
            }

            @Override
            public void onError(String message) {

                AppUtil.showToast(self, message);

            }
        });

    }


    private void showDialogComplete() {
        DialogUtil.showAlertDialog(self, R.string.app_name, R.string.do_you_want_to_continues, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                payDeal(4, "");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String strPrice = "0";
    private float rate;
    private String comment;
    private double price;

    private void payDeal(float rate, String comment) {
        this.rate = rate;
        this.comment = comment;

        AppUtil.hideSoftKeyboard(TripPaymentActivity.this);
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            //strPrice = mTxtAmount.getText().toString().trim();
            if (strPrice.equals("")) {
                strPrice = "0";
            } else if (strPrice.contains(",")) {
                strPrice = strPrice.replace(",", "");
            }
            Log.e(TAG, "price:" + strPrice);

            price = Double.parseDouble(strPrice);

            if (mSelectedPayment.equals(PaymentMethodObj.BONUS)) {
                if (DataStoreManager.getUser().getBalance() < price) {

                    AppUtil.showToast(self, R.string.your_bonus_is_not_enough_to_pay);
                    return;
                }
            }

            if (mTripObj != null) {

                if (mSelectedPayment.equals(PaymentMethodObj.MNT_MONEY)) {

                    requestPaymentBillmap();


                } else {
                    requestPaymentSuccess(rate, comment);


                }


            }
        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }


    private void requestPaymentSuccess(float rate, String comment) {
        RequestManager.finishTripInstantAndDeal(self, mTripObj, mSelectedPayment, strPrice, rate, comment,
                new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {

                        UserObj userObj = DataStoreManager.getUser();
                        userObj.setBalance((float) (userObj.getBalance() - price));
                        DataStoreManager.saveUser(userObj);

                        Toast.makeText(self, R.string.msg_paid_successfully, Toast.LENGTH_SHORT).show();

                        setResult(RESULT_OK);
                        onBackPressed();

                    }

                    @Override
                    public void onError(String message) {

                    }
                });
    }

    private void requestPaymentBillmap() {
        showProgress(true);
        ApiUtils.getAPIPayment().processOnlinePayment(BillMap.Service_Provider_Code,
                BillMap.PASSWORD, BillMap.NUMBER, BillMap.REF, strPrice, BillMap.MetaData)
                .enqueue(new Callback<ResponseOnlinePayment>() {
                    @Override
                    public void onResponse(Call<ResponseOnlinePayment> call, Response<ResponseOnlinePayment> response) {
                        showProgress(false);
                        if (response.body() != null) {
                            if (response.body().isSuccess(self)) {
                                ResponseOnlinePayment responseOnlinePayment = response.body();

                                Log.e(TAG, "onResponse: " + new Gson().toJson(responseOnlinePayment));

                                callIpn(responseOnlinePayment);
                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseOnlinePayment> call, Throwable t) {
                        Log.e("XML", t.getMessage());
                        showProgress(false);

                    }
                });
    }

    private void callIpn(ResponseOnlinePayment responseOnlinePayment) {
        ApiUtils.getAPIService().ipn(BillMap.FROM_MOBILE, DataStoreManager.getTokenUser(),
                responseOnlinePayment.billMapTransactionId,
                responseOnlinePayment.eWPTransactionId,
                responseOnlinePayment.responseCode,
                responseOnlinePayment.responseMessage)
                .enqueue(new Callback<ResponseIpn>() {
                    @Override
                    public void onResponse(Call<ResponseIpn> call, Response<ResponseIpn> response) {

                        if (response.isSuccessful()) {
                            if (response.body().isSuccess(self)) {
                                Log.e(TAG, "ipn onResponse: " + response.body());

                                onPay();
                                System.gc();
                            } else {

                                Log.e(TAG, "onResponse: " + response.body().getMessage());
                            }
                        } else {
                            Log.e(TAG, "onResponse: " + response.message());
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseIpn> call, Throwable t) {
                        AppUtil.showToast(self, R.string.msg_have_some_errors);

                    }
                });
    }

    private void initData() {
        if (mTripObj != null) {
            mLblDollar.setText(StringUtil.convertNumberToPrice(mTripObj.getEstimateFare()));

            mLblCredits.setText(StringUtil.convertNumberToPrice(mTripObj.getEstimateFare()));

            String amount = StringUtil.convertNumberPrice(mTripObj.getEstimateFare());

            mTxtAmount.setText(amount);
            strPrice = (int) mTripObj.getEstimateFare() + "";
            mLblTotal.setText(StringUtil.convertNumberToPrice((mTripObj.getEstimateFare())));

        }
    }

    private void initPaymentMethod() {
        PaymentMethodAdapter adapter = new PaymentMethodAdapter(new IPayment() {
            @Override
            public void onPaymentMethodSelected(PaymentMethodObj obj) {


                updateTotalFare();
            }
        });

        mRcl.setAdapter(adapter);
    }

    private void updateTotalFare() {

    }

    private void showRatingDialog() {
        final Dialog dialog = DialogUtil.setDialogCustomView(self, R.layout.dialog_rating, false);

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.rating_bar);
        final EditText txtComment = (EditText) dialog.findViewById(R.id.txt_comment);
        TextViewBold lblSubmit = (TextViewBold) dialog.findViewById(R.id.lbl_submit);
        ((LayerDrawable) ratingBar.getProgressDrawable()).getDrawable(2)
                .setColorFilter(ratingBar.getContext().getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);


        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rate, boolean b) {
                if (rate < 1) {
                    ratingBar.setRating(1);
                }
            }
        });

        lblSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratingBar.getRating() == 0) {
                    Toast.makeText(self, R.string.rating_is_required, Toast.LENGTH_SHORT).show();
                } else if (txtComment.getText().toString().trim().equals("")) {
                    Toast.makeText(self, R.string.comment_is_required, Toast.LENGTH_SHORT).show();
                    txtComment.requestFocus();
                } else {
                    dialog.dismiss();

                    payDeal(ratingBar.getRating() * 2, txtComment.getText().toString().trim());
                }
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder> {

        private ArrayList<PaymentMethodObj> paymentMethodObjs;
        private IPayment iPayment;

        public PaymentMethodAdapter(IPayment iPayment) {
            this.iPayment = iPayment;

            if (paymentMethodObjs == null) {
                paymentMethodObjs = new ArrayList<>();
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.CASH, getString(R.string.cash), true));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.CREDITS, getString(R.string.credits), false));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.BONUS, getString(R.string.bonus_pay), false));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.MNT_MONEY, getString(R.string.mnt_money), false));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.ORANGE_MONEY, getString(R.string.orange_money), false));
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.MOOV_MONEY, getString(R.string.moov_money), false));
            }
        }

        @Override
        public PaymentMethodAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_method, parent, false);

            return new PaymentMethodAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final PaymentMethodAdapter.ViewHolder holder, final int position) {
            if (getItemCount() > 0) {

                final PaymentMethodObj obj = paymentMethodObjs.get(position);

                if (obj != null) {
                    holder.lblMethod.setText(obj.getName());
                    if (obj.isSelected()) {
                        holder.lblMethod.setTextColor(ContextCompat.getColor(self, R.color.white));
                        holder.lblMethod.setBackgroundResource(R.drawable.bg_accent_shadow);
                    } else {
                        holder.lblMethod.setTextColor(ContextCompat.getColor(self, R.color.textColorPrimary));
                        holder.lblMethod.setBackgroundResource(R.drawable.bg_pressed_grey);
                    }

                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (/*!obj.getId().equals(PaymentMethodObj.MNT_MONEY)
                                    &&*/ !obj.getId().equals(PaymentMethodObj.MOOV_MONEY)
                                    && !obj.getId().equals(PaymentMethodObj.ORANGE_MONEY)) {

                                if (!mSelectedPayment.equals(obj.getId())) {


                                    for (int i = 0; i < getItemCount(); i++) {
                                        PaymentMethodObj paymentMethodObj = paymentMethodObjs.get(i);
                                        if (paymentMethodObj.isSelected()) {
                                            paymentMethodObj.setSelected(false);
                                            break;
                                        }
                                    }

                                    obj.setSelected(true);
                                    notifyDataSetChanged();
                                    mSelectedPayment = obj.getId();
                                    iPayment.onPaymentMethodSelected(obj);
                                }

                            } else {
                                AppUtil.showToast(self, R.string.currently_unavailable);
                            }


                        }
                    });
                }


            }
        }

        @Override
        public int getItemCount() {
            try {
                return paymentMethodObjs.size();
            } catch (NullPointerException ex) {
                return 0;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView lblMethod;

            public ViewHolder(View view) {
                super(view);

                lblMethod = view.findViewById(R.id.lbl_payment_method);
            }
        }
    }


    interface IPayment {
        void onPaymentMethodSelected(PaymentMethodObj obj);
    }

    public static void start(Activity activity, TripObj tripObj, ReservationObj reservationObj) {
        Bundle bundle = new Bundle();
        if (tripObj != null) {
            bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
        } else if (reservationObj != null) {
            bundle.putParcelable(Args.RESERVATION_DEAL_OBJ, reservationObj);
        }
        GlobalFunctions.startActivityWithoutAnimation(activity, TripPaymentActivity.class, bundle);
    }

    public static void startForResult(Activity activity, TripObj tripObj, ReservationObj reservationObj, int reqCode) {
        Bundle bundle = new Bundle();
        if (tripObj != null) {
            bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
        } else if (reservationObj != null) {
            bundle.putParcelable(Args.RESERVATION_DEAL_OBJ, reservationObj);
        }

        Intent intent = new Intent(activity, TripPaymentActivity.class);
        intent.putExtras(bundle);
        activity.startActivityForResult(intent, reqCode);
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.FinishAllScreenTrip event) {/* Do something */
        finish();
    }

    ;


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.PushCancelTrip event) {/* Do something */

        finish();

    }

    ;


}
