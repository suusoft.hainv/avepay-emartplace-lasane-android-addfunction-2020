package com.aveplus.avepay_cpocket.movie.movie.main.movies.impl;


import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieGenre;

import java.util.List;

/**
 */
public interface MovieGenreCallback {
    public void onCompleted(List<MovieGenre> genres);

    public void onFailed(Exception e);
}
