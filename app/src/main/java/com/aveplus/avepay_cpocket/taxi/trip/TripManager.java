package com.aveplus.avepay_cpocket.taxi.trip;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.view.activities.TripPaymentActivity;
import com.aveplus.avepay_cpocket.taxi.view.activities.TripTrackingActivity;
import com.aveplus.avepay_cpocket.taxi.view.instant.DriverConfirmActivity;
import com.aveplus.avepay_cpocket.taxi.view.negociate.WaitingDriverEstimateActivity;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;

public class TripManager {

    public static void directionTrip(Context self, TripObj tripObj, String tag) {

        if (tripObj != null) {
            Log.e(tag, "trip : " + new Gson().toJson(tripObj));

            if (tripObj.isDriverPutTime()) {

                Bundle bundle = new Bundle();
                bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
                AppUtil.startActivity(self, WaitingDriverEstimateActivity.class, bundle);

            } else if (tripObj.isPaymentPending()) {

                Bundle bundle = new Bundle();
                bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
                AppUtil.startActivity(self, TripPaymentActivity.class, bundle);

            } else if (tripObj.isOnGoing()) {

                AppUtil.showToast(self, R.string.you_are_on_a_trip);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
                AppUtil.startActivity(self, TripTrackingActivity.class, bundle);

            } else if (tripObj.isNew()) {
                EventBus.getDefault().post(new Global.NotifiShowRequestTrip(tripObj));

                return;
            }


            Log.e(tag, "finish ");
            if (self instanceof DriverConfirmActivity) {
                ((DriverConfirmActivity) self).finish();

            } else if (self instanceof TripPaymentActivity) {

                ((TripPaymentActivity) self).finish();

            } else if (self instanceof WaitingDriverEstimateActivity) {

                ((WaitingDriverEstimateActivity) self).finish();

            }


        } else {
            Log.e(tag, "trip : null");
        }


    }
}
