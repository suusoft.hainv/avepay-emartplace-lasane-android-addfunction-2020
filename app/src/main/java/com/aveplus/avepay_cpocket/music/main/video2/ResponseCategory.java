package com.aveplus.avepay_cpocket.music.main.video2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseCategory {
    @SerializedName("data")
    @Expose
    private ArrayList<ObjVideoCategory> data;

    public ArrayList<ObjVideoCategory> getData() {
        return data;
    }

    public void setData(ArrayList<ObjVideoCategory> data) {
        this.data = data;
    }
}
