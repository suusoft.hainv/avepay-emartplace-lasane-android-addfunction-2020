package com.aveplus.avepay_cpocket.music.main.settings;

import android.content.Context;

import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class SettingsVM extends BaseViewModel {

    private boolean isKeepScreen;

    public SettingsVM(Context self) {
        super(self);
        checkSettings();
    }

    private void checkSettings() {
        if (DataStoreManager.isAwakeScreen()){
            setKeepScreen(true);
        }else {
            setKeepScreen(false);
        }
    }

    public boolean isKeepScreen() {
        return isKeepScreen;
    }



    public void setKeepScreen(boolean keepScreen) {
        isKeepScreen = keepScreen;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        DataStoreManager.setAwakeScreen(isKeepScreen);

    }
}
