package com.aveplus.avepay_cpocket.interfaces;

/**
 * Created by Suusoft on 27/02/2017.
 */

public interface IObserver {
    void update();
}
