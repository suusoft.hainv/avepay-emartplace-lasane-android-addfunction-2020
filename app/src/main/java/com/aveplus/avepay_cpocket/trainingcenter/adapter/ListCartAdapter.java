package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.cart.CartManager;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.aveplus.avepay_cpocket.trainingcenter.view.PickerListener;
import com.aveplus.avepay_cpocket.trainingcenter.view.SimpleDialogPicker;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class ListCartAdapter extends ArrayAdapter<Product> {

    public interface UpdateQuantityListener {
        void onUpdate(boolean isPlus, int index);
    }

    private Context context;
    private Button btnRemove;
    private LayoutInflater inflater;
    private UpdateQuantityListener listener;

    public UpdateQuantityListener getListener() {
        return listener;
    }

    public void setListener(UpdateQuantityListener listener) {
        this.listener = listener;
    }

    public ListCartAdapter(Context context, int layoutID,
                           ArrayList<Product> list2, UpdateQuantityListener listener) {
        super(context, layoutID);
        this.context = context;
        this.listener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return CartManager.getInstance().size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_carl, null);
            vh = new ViewHoler();
            vh.txtName = (TextView) rowView.findViewById(R.id.textTitle_Cart);
            vh.txtName.setSelected(true);
            vh.txtPrice = (TextView) rowView.findViewById(R.id.textPrice);
            vh.imgCart = (ImageView) rowView.findViewById(R.id.image_cart);
            vh.txtNumberQuantity = (TextView) rowView.findViewById(R.id.tvNumberQuantity);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        final Product p = CartManager.getInstance().getYourCart().get(position);
        String arrImg = p.getImage().get(0).toString();
        if (p != null) {
            Picasso.get().load(arrImg).into(vh.imgCart);
            vh.txtName.setText(p.getName());
            vh.txtPrice.setText("$" + String.format("%.2f", Float.parseFloat(p.getPrice())));
            vh.txtNumberQuantity.setText(p.getNumberCart() + "");
        }

        vh.txtNumberQuantity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> arr = new ArrayList<>();
                SimpleDialogPicker dialog = new SimpleDialogPicker(
                        getContext(), arr, p.getNumberCart() + "", "",
                        new PickerListener() {

                            @Override
                            public void simplePicker(String simplePicker,
                                                     String id) {
                                vh.txtNumberQuantity.setText(simplePicker);
                                p.setNumberCart(Integer
                                        .parseInt(vh.txtNumberQuantity
                                                .getText().toString()));
                                p.getNumberCart();
                                listener.onUpdate(true, position);
                            }

                            @Override
                            public void dateTimePicker(int year, int month,
                                                       String monthName, int day,
                                                       String dayOfWeek, int hour, int min) {
                            }
                        });
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.show();
            }
        });

        btnRemove = (Button) rowView.findViewById(R.id.btRemove);
        btnRemove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CartManager.getInstance().removeCart(position);
                listener.onUpdate(true, position);
                if (CartManager.getInstance().size() == 0) {
                    listener.onUpdate(false, 1);
                }
                Toast.makeText(context, "Remove Successfully!", Toast.LENGTH_SHORT)
                        .show();
                notifyDataSetChanged();
            }

        });

        return rowView;

    }

    static class ViewHoler {
        public TextView txtName, txtPrice, txtNumberQuantity;
        public ImageView imgCart;
    }
}
