package com.aveplus.avepay_cpocket.music.player;

public interface PlayerListener {
	public void onSeekChanged(int maxProgress, String lengthTime, String currentTime, int progress);
}
