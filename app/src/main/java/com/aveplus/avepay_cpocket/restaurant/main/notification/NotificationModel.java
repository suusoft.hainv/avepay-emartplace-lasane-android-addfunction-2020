package com.aveplus.avepay_cpocket.restaurant.main.notification;
/**
 * Created by suusoft.com on 11/12/19.
 */

import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationModel extends BaseModel {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("action")
    @Expose
    private String image;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("receiver_count")
    @Expose
    private int receiver_count;
    @SerializedName("eceiver_users")
    @Expose
    private String eceiver_users;
    @SerializedName("sent_date")
    @Expose
    private String sent_date;
    @SerializedName("created_date")
    @Expose
    private String created_date;
    @SerializedName("created_user")
    @Expose
    private String created_user;
    @SerializedName("application_id")
    @Expose
    private String application_id;


    public NotificationModel() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getReceiver_count() {
        return receiver_count;
    }

    public void setReceiver_count(int receiver_count) {
        this.receiver_count = receiver_count;
    }

    public String getEceiver_users() {
        return eceiver_users;
    }

    public void setEceiver_users(String eceiver_users) {
        this.eceiver_users = eceiver_users;
    }

    public String getSent_date() {
        return sent_date;
    }

    public void setSent_date(String sent_date) {
        this.sent_date = sent_date;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getCreated_user() {
        return created_user;
    }

    public void setCreated_user(String created_user) {
        this.created_user = created_user;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }
}
