package com.aveplus.avepay_cpocket.restaurant.main.restaurant.order;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.main.history.Detail;
import com.aveplus.avepay_cpocket.restaurant.main.history.DetailOrder;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.RestaurantFrag;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.confirmorder.ConfirmActivity;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;

import java.util.ArrayList;

import static com.aveplus.avepay_cpocket.restaurant.main.restaurant.toporder.AdapterTopOrder.cartModelArrayList;


public class MycartActivity extends AppCompatActivity implements View.OnClickListener {
    public static TextView tvTotal;
    public static int grandTotalplus;
    public static int restaurantid = 0;
    public static ArrayList<CartModel> temparraylist;
    public static ArrayList<ItemModel> itemModels;
    private TextView tvName, tvRestaurant, tvAmoun, tvPrice, tvSubtotal, tvDelivery, tvCoupon, tvQuantity;
    private TextView btnPayment;
    private RecyclerView rcCart;
    private CartAdapter cartAdapter;
    private Toolbar mToolbar;
    private int id_rest;
    public ArrayList<DetailOrder> orders;
    public ArrayList<Detail> arrdetails;
    private ItemModel ItemModel;
    private String pay;
    private String total;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        initView();

    }

    private void initView() {

        Log.e("TAG", "initView: " + restaurantid);

        temparraylist = new ArrayList<>();
        tvTotal = findViewById(R.id.tv_cart_total);
        btnPayment = findViewById(R.id.btn_payment);
        tvQuantity = findViewById(R.id.tv_amount);
        rcCart = findViewById(R.id.rc_cart);
        mToolbar = findViewById(R.id.cart_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("MY CART");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnPayment.setOnClickListener(this);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // these lines of code for show the same  cart for future refrence
                grandTotalplus = 0;
                for (int i = 0; i < temparraylist.size(); i++) {

                }
                cartModelArrayList.addAll(temparraylist);
//                addItemInCart.clear();
                RestaurantFrag.cart_count = (temparraylist.size());
                finish();
            }
        });
        RestaurantFrag.cart_count = 0;

        Log.d("sizecart_1", String.valueOf(temparraylist.size()));
        Log.d("sizecart_2", String.valueOf(cartModelArrayList.size()));


        // from these lines of code we remove the duplicacy of cart and set last added quantity in cart
        // for replace same item
        for (int i = 0; i < cartModelArrayList.size(); i++) {
            for (int j = i + 1; j < cartModelArrayList.size(); j++) {
                if (cartModelArrayList.get(i).getId() == cartModelArrayList.get(j).getId()) {
                    cartModelArrayList.get(i).setQuantity(cartModelArrayList.get(j).getQuantity());
                    cartModelArrayList.get(i).setTotalCash(cartModelArrayList.get(j).getTotalCash());
                    cartModelArrayList.remove(j);
                    j--;
                    Log.d("remove", String.valueOf(cartModelArrayList.size()));

                }
            }

        }
        temparraylist.addAll(cartModelArrayList);
        cartModelArrayList.clear();

        Log.d("sizecart_11", String.valueOf(temparraylist.size()));
        Log.d("sizecart_22", String.valueOf(cartModelArrayList.size()));


        itemModels = new ArrayList<>();

        String name = "";


        // this code is for get total cash
        for (int i = 0; i < temparraylist.size(); i++) {
            grandTotalplus = grandTotalplus + temparraylist.get(i).getTotalCash();
            // Add id, quantity, price, total to convert Json
            ItemModel = new ItemModel(temparraylist.get(i).getId(), name, temparraylist.get(i).getQuantity(),
                    temparraylist.get(i).getPrice(), temparraylist.get(i).getTotalCash());
            itemModels.add(ItemModel);


        }


        tvTotal.setText(String.valueOf(grandTotalplus));
        total = tvTotal.getText().toString();
        cartAdapter = new CartAdapter(temparraylist, this);
        rcCart.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        rcCart.setAdapter(cartAdapter);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_payment:

                AppUtil.startActivity(this, ConfirmActivity.class);

                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        grandTotalplus = 0;
        for (int i = 0; i < temparraylist.size(); i++) {
            RestaurantFrag.cart_count = (temparraylist.size());

        }
        cartModelArrayList.addAll(temparraylist);
    }

    @Override
    public void finish() {
        super.finish();
    }
}
