package com.aveplus.avepay_cpocket.music.main.category;

import android.content.Context;
import android.util.Log;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Category;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.aveplus.avepay_cpocket.music.player.QueueManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class MusicByCategoryVM extends BaseViewModelList implements ItemMenuActionSongListener {

    Category item;

    public MusicByCategoryVM(Context context, Category category) {
        super(context);
        this.item = category;
        getData(1);
    }

    @Override
    public void getData(int page) {
        RequestManager.getItemByCategory(page, item.id, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (!response.isError()) {
                    ArrayList<Song> list = (ArrayList<Song>) response.getDataList(Song.class);
                    addListData(list);
                    checkLoadingMoreComplete(Integer.parseInt(response.getValueFromRoot("total_page")));
                }
            }

            @Override
            public void onError(String message) {
                Log.d("MusicByCategoryVM", "can not get video detail");

            }
        });
    }

    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().addNewQueue((ArrayList<Song>) getListData(), position);
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {

    }
}
