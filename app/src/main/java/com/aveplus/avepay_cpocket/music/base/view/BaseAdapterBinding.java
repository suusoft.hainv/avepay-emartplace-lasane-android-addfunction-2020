package com.aveplus.avepay_cpocket.music.base.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.listener.IBaseListener;

import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public abstract class BaseAdapterBinding extends RecyclerView.Adapter<BaseAdapterBinding.ViewHolder> {

    protected Context context;

    public abstract void addDatas(List<?> data);

    public abstract void setDatas(List<?> data);

    public BaseAdapterBinding(Context context) {
        this.context = context;
    }

    public void setItems(List<?> data, boolean isAppend) {
        if (isAppend) {
            addDatas(data);
        } else {
            setDatas(data);
        }
    }

    /**
     * shortcut for binding view
     *
     * @param parent is viewgroup
     * @param layout is layout
     */
    protected ViewDataBinding getViewBinding(ViewGroup parent, int layout) {
        return DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layout, parent, false);

    }

    /**
     * class view holder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(BaseAdapterVM adapterVM) {
            binding.setVariable(BR.viewModel, adapterVM);
        }
    }

    public void setListener(IBaseListener listener) {
    }


}
