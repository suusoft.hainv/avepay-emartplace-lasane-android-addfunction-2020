package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.Portfolios;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class PortfoliosAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private ArrayList<Portfolios> objects;
    private ArrayList<Portfolios> listPort;
    private PortfoliosFilter mFilter = new PortfoliosFilter();
    private LayoutInflater inflater;

    public PortfoliosAdapter(Context context, ArrayList<Portfolios> objects) {
        this.context = context;
        this.objects = objects;
        this.listPort = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_portfolios, null);
            vh = new ViewHoler();
            vh.txtPortTitle = (TextView) rowView.findViewById(R.id.txtPortTitle);
            vh.txtPortContent = (TextView) rowView.findViewById(R.id.txtPortContent);
            vh.txtPortTitle.setSelected(true);
            vh.txtPortContent.setSelected(true);
            vh.imgPort = (ImageView) rowView.findViewById(R.id.imgPort);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        final Portfolios c = objects.get(position);
        if (c != null) {
            vh.txtPortTitle.setText(c.getTitle());
            Picasso.get().load(c.getImage()).into(vh.imgPort);
        }
        return rowView;
    }

    static class ViewHoler {
        public TextView txtPortTitle, txtPortContent;
        public ImageView imgPort;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class PortfoliosFilter extends Filter {

        @SuppressLint({"NewApi", "DefaultLocale"})
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final ArrayList<Portfolios> list1 = listPort;
            int count = list1.size();
            final ArrayList<Portfolios> nlist = new ArrayList<Portfolios>(count);
            if (!filterString.isEmpty()) {
                String filterableString;
                for (int i = 0; i < count; i++) {
                    filterableString = list1.get(i).getTitle();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(listPort.get(i));
                    }
                }
                if (nlist.size() > 0) {
                    results.values = nlist;
                    results.count = nlist.size();
                } else {
                    results.values = listPort;
                    results.count = listPort.size();
                }
            } else {
                results.values = listPort;
                results.count = listPort.size();
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count == 0) {
                notifyDataSetInvalidated();
            } else {
                objects = (ArrayList<Portfolios>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}
