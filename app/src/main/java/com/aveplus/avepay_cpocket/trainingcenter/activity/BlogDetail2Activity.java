package com.aveplus.avepay_cpocket.trainingcenter.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.Args;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.aveplus.avepay_cpocket.view.activities.BaseActivity;
import com.squareup.picasso.Picasso;

public class BlogDetail2Activity extends BaseActivity {
    private TextView txttitle, txtcontent;
    private ImageView imgnews, btnBack;

    private NewEvent item;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail2);
        initView();
        initData();
    }

    @Override
    protected void inflateLayout() {

    }

    @Override
    protected void initUI() {

    }

    @Override
    protected void initControl() {

    }
    public void initView() {
        txttitle = (TextView) findViewById(R.id.txtNewTitle);
        txtcontent = (TextView) findViewById(R.id.ContentNews);
        imgnews = (ImageView) findViewById(R.id.imgNewEvent);
        btnBack = (ImageView) findViewById(R.id.imgBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private void initData() {
        //        Context context = null;
//        Bundle bundle = getIntent().getBundleExtra("bundle");
        Bundle bundle = getIntent().getExtras();
        item = bundle.getParcelable(Args.KEY_PRODUCT_OBJECT);
//        String title = bundle.getString("title");
//        String description = bundle.getString("description");
//        String content = bundle.getString("content");
//        String time = bundle.getString("time");
//        String image = bundle.getString("image");
//        link = bundle.getString("link");
        txttitle.setText(item.getTitle());
        txtcontent.setText(item.getContent());
        Picasso.get().load(item.getImage()).into(imgnews);
    }
    @Override
    public void onBackPressed() {
        finish();
    }

}