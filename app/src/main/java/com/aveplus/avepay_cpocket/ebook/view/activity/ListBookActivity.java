package com.aveplus.avepay_cpocket.ebook.view.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseListActivityBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.SingleAdapter;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.ListBookVM;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemBookVM;


/**
 * Created by trangpham on 3/5/18.
 */

public class ListBookActivity extends BaseListActivityBinding {

    private ListBookVM viewModel;
    private Bundle bundle;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void getExtraData(Intent intent) {
        bundle = intent.getExtras();
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new ListBookVM(self, bundle);
        return viewModel;
    }

    @Override
    protected void onViewCreated() {
        String title = bundle.getString(Constant.KEY_TITLE_TOOLBAR);
        setToolbarTitle(title);
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(self));
        recyclerView.setAdapter(new SingleAdapter(self, R.layout.item_book_grid_all, viewModel.getListData(), ItemBookVM.class));
    }


}
