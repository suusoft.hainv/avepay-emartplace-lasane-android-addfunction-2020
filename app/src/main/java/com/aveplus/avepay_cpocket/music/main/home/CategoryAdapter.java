package com.aveplus.avepay_cpocket.music.main.home;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Album;
import com.aveplus.avepay_cpocket.music.model.HomeCategory;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.util.BindingUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.BaseViewHolder> implements ItemMenuActionSongListener {
    public static final int ITEM_NORMAL = 1;
    public static final int ITEM_FEATURE = 2;
    private ArrayList<HomeCategory> listData;
    private Context context;
    private int posSongItem = -1;

    public CategoryAdapter(Context context, ArrayList<HomeCategory> mListCategory) {
        this.listData = mListCategory;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position != 0)
            return ITEM_NORMAL;
        return ITEM_FEATURE;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_NORMAL) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_hot, parent, false);
            return new ViewHolderHeader(view);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            HomeCategory item = listData.get(position);
            if (item != null) {
                viewHolder.tvName.setText(item.name);
                if (HomeCategory.SONGS.equals(item.type)) {
                    viewHolder.tvMore.setVisibility(View.GONE);
                    posSongItem = position;
                    viewHolder.rcvData.setPadding(0, 0, 0, 90);
                    viewHolder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                    viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_song, item.getListSongs(), ItemSongsVM.class, this));
                } else if (HomeCategory.CATEGORY.equals(item.type)) {
                    viewHolder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_category_music, item.getListCategory(), ItemCategoryVM.class));
                    setOnClickViewMore(viewHolder.tvMore, item.name, position);
                } else {
                    viewHolder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    viewHolder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_album_home, item.getListAlbum(), ItemAlbumVM.class));
                    setOnClickViewMore(viewHolder.tvMore, item.name, Integer.parseInt(item.id));
                }
            }
        } else {
            HomeCategory item = listData.get(position);
            if (item != null) {
                ViewHolderHeader viewHolder = (ViewHolderHeader) holder;
                viewHolder.setAdapter(item.getListAlbum());
            }
        }
    }

    private void setOnClickViewMore(TextView tvMore, final String object, final int type) {
        tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new Global.ViewMoreClicked(object, type));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public void onClickWatch(int position) {
        if (posSongItem != -1) {
            HomeCategory item = listData.get(posSongItem);
            QueueManager.getInstance().addNewQueue(item.getListSongs(), position);
            EventBus.getDefault().post(new Global.HomeSongItemClicked());
        }
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {

    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvName, tvMore;
        RecyclerView rcvData;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMore = itemView.findViewById(R.id.tv_more);
            tvName = itemView.findViewById(R.id.tv_name);
            rcvData = itemView.findViewById(R.id.rcv_data);
            rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        }
    }

    public class ViewHolderHeader extends BaseViewHolder {
        ViewPager viewPager;
        CircleIndicator circleIndicator;
        HeaderItemAdapter adapter;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            viewPager = itemView.findViewById(R.id.viewPagger);
            circleIndicator = itemView.findViewById(R.id.circleIndicator);
        }

        public void setAdapter(ArrayList<Album> listData) {
            adapter = new HeaderItemAdapter(listData);
            viewPager.setAdapter(adapter);
            circleIndicator.setViewPager(viewPager);
            adapter.registerDataSetObserver(circleIndicator.getDataSetObserver());
            adapter.setAutoSlide();
        }

        public class HeaderItemAdapter extends PagerAdapter {
            private static final long DELAY = 5000;
            private ArrayList<Album> mListData;
            private Handler handler = new Handler();
            public ImageView imageView;
            private Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    int item = viewPager.getCurrentItem();
                    int totalItem = mListData.size() - 1;
                    if (item < totalItem) {
                        item++;
                        viewPager.setCurrentItem(item);
                    } else if (item == totalItem) {
                        viewPager.setCurrentItem(0);
                    }
                    handler.postDelayed(this, DELAY);
                }
            };

            public HeaderItemAdapter(ArrayList<Album> mListData) {
                this.mListData = mListData;
            }

            @Override
            public int getCount() {
                return mListData.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, final int position) {
                View view = LayoutInflater.from(context).inflate(R.layout.item_pager_header, container, false);
                imageView = view.findViewById(R.id.imageView);
                BindingUtil.setImage(imageView, mListData.get(position).thumbnail);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EventBus.getDefault().post(new Global.ItemAlbumClicked(mListData.get(position)));
                    }
                });
                container.addView(view);

                return view;

            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                handler.removeCallbacks(runnable);
                container.removeView((RelativeLayout) object);
            }

            public void setAutoSlide() {
                handler.postDelayed(runnable, DELAY);

            }
        }
    }
}
