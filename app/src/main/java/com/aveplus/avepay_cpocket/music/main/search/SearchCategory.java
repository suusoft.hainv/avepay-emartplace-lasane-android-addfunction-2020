package com.aveplus.avepay_cpocket.music.main.search;


import com.aveplus.avepay_cpocket.music.base.model.BaseModel;
import com.aveplus.avepay_cpocket.music.main.video2.ObjVideo;
import com.aveplus.avepay_cpocket.music.model.Album;
import com.aveplus.avepay_cpocket.music.model.Artist;
import com.aveplus.avepay_cpocket.music.model.Song;

import java.util.ArrayList;

public class SearchCategory extends BaseModel {

    public static final String SONGS = "SONGS";
    public static final String VIDEOS = "VIDEOS";
    public static final String ARTIST = "ARTIST";
    public static final String PLAYLIST = "PLAYLIST";

    private String name;

    private ArrayList<Song> listSongs;
    private ArrayList<Artist> listArtist;
    private ArrayList<Album> listAlbum;
    private ArrayList<ObjVideo> listVideo;

    public static String getSONGS() {
        return SONGS;
    }

    public static String getARTIST() {
        return ARTIST;
    }

    public static String getPLAYLIST() {
        return PLAYLIST;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Song> getListSongs() {
        if (listSongs == null) {
            listSongs = new ArrayList<>();
        }
        return listSongs;
    }

    public ArrayList<Artist> getListArtist() {
        if (listArtist == null) {
            listArtist = new ArrayList<>();
        }
        return listArtist;
    }

    public ArrayList<Album> getListAlbum() {
        if (listAlbum == null) {
            listAlbum = new ArrayList<>();
        }
        return listAlbum;
    }

    public ArrayList<ObjVideo> getListVideo() {
        if (listVideo == null) {
            listVideo = new ArrayList<>();
        }
        return listVideo;
    }
}
