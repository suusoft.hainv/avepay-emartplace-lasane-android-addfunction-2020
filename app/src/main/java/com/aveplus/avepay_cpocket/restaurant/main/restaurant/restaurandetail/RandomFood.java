package com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RandomFood implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("attachment")
    @Expose
    private String attachment;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("sale_price")
    @Expose
    private double sale_price;
    @SerializedName("product_images")
    @Expose
    private List<ProductImage> productImages = null;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("rate")
    @Expose
    private double rate;
    @SerializedName("rate_count")
    @Expose
    private int rate_count;

    @SerializedName("review")
    @Expose
    private List<Review> reviewList;


    protected RandomFood(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        attachment = in.readString();
        description = in.readString();
        categoryId = in.readString();
        content = in.readString();
        if (in.readByte() == 0) {
            quantity = null;
        } else {
            quantity = in.readInt();
        }
        price = in.readDouble();
        sale_price = in.readDouble();
        productImages = in.createTypedArrayList(ProductImage.CREATOR);
        reviewList = in.createTypedArrayList(Review.CREATOR);
        city = in.readString();
        rate = in.readDouble();
        rate_count = in.readInt();
    }

    public static final Creator<RandomFood> CREATOR = new Creator<RandomFood>() {
        @Override
        public RandomFood createFromParcel(Parcel in) {
            return new RandomFood(in);
        }

        @Override
        public RandomFood[] newArray(int size) {
            return new RandomFood[size];
        }
    };

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public void setRate(double rate) {
        this.rate = rate;
    }

    public Double getRate() {
        return rate;
    }

    public Integer getRate_count() {
        return rate_count;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    public void setRate_count(int rate_count) {
        this.rate_count = rate_count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getDescription() {
        return description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSale_price() {
        return sale_price;
    }

    public void setSale_price(Double sale_price) {
        this.sale_price = sale_price;
    }

    public List<ProductImage> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<ProductImage> productImages) {
        this.productImages = productImages;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(name);
        parcel.writeString(attachment);
        parcel.writeString(description);
        parcel.writeString(categoryId);
        parcel.writeString(content);
        if (quantity == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(quantity);
        }
        parcel.writeDouble(price);
        parcel.writeDouble(sale_price);
        parcel.writeTypedList(productImages);
        parcel.writeTypedList(reviewList);
        parcel.writeString(city);
        parcel.writeDouble(rate);
        parcel.writeInt(rate_count);
    }

}
