package com.aveplus.avepay_cpocket.taxi.retrofit;


import android.util.Log;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

public class ApiUtils {

    public static final String BASE_URL = AppController.getInstance().getString(R.string.URL_API_TAXI) + "backend/web/index.php/api/";
    public static final String BASE_URL_TYPE_TAXI = AppController.getInstance().getString(R.string.URL_API_TAXI_TYPE) + "backend/web/index.php/api/";

    public static final String BASE_URL_PAYMENT_BILLMAP = AppController.getInstance().getString(R.string.URL_API_PAYMENT);

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }

    public static APIService getAPIServiceTypeTaxi() {

        return RetrofitClient.getClientTypeTaxi(BASE_URL_TYPE_TAXI).create(APIService.class);
    }


    // payment

    public static ApiPaymentBillMap getAPIPayment() {

        return RetrofitClient.getClientBillmap(BASE_URL_PAYMENT_BILLMAP).create(ApiPaymentBillMap.class);
    }
}
