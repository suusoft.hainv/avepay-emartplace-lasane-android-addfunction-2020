package com.aveplus.avepay_cpocket.music.base.model;

import com.google.gson.Gson;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public class BaseModel {

    public String toJSon(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
    
}
