package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.BillMap;
import com.aveplus.avepay_cpocket.taxi.base.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.taxi.objects.PaymentMethodObj;
import com.aveplus.avepay_cpocket.taxi.objects.SettingsObj;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.paypal.Paypal;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseIpn;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseOnlinePayment;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SuuSoft.com on 05/12/2016.
 */

public class BuyCreditsActivity extends com.aveplus.avepay_cpocket.base.BaseActivity implements View.OnClickListener {
    private static final String TAG = BuyCreditsActivity.class.getName();
    private static final String ACTION_BUY_CREDIT = "exchange";
    private static final String PAYPAL = "Paypal";
    private static final String SKRILL = "Skrill";
    private TextView tvCreditsAvailable;
    private FrameLayout btnPayViaPaypal;
    private FrameLayout btnPayViaSkrill;
    private EditText edtCredits;
    private TextViewRegular tvTotal, tvSubTotal, tvFee;
    private TextView btnBuy;
    private ImageView imgPaypal, imgSkrill;
    private UserObj user;
    private String methodPayment;

    private boolean isNeedFinished;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_buy_credits_taxi;
    }

    @Override
    protected void getExtraData(Intent intent) {
        if (intent.hasExtra(Args.IS_NEED_FINISH)) {
            isNeedFinished = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.onPause();
    }

    @Override
    protected void initilize() {
        Paypal.startPaypalService(this);
        user = DataStoreManager.getUser();

    }

    @Override
    protected void initView() {
        setToolbarTitle(R.string.iwana_pay_buy_credit);
        tvCreditsAvailable = findViewById(R.id.tv_credits);
        btnPayViaPaypal = findViewById(R.id.btn_paypal);
        btnPayViaSkrill = findViewById(R.id.btn_skrill);
        edtCredits = findViewById(R.id.edt_credits);
        tvTotal = findViewById(R.id.tv_total);
        tvSubTotal = findViewById(R.id.tv_sub_total);
        tvFee = findViewById(R.id.tv_fee);
        btnBuy = findViewById(R.id.btn_buy);
        imgPaypal = findViewById(R.id.img_paypal);
        imgSkrill = findViewById(R.id.img_skrill);


        mRcl = findViewById(R.id.rcl_payment_methods);
        mRcl.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));
        mRcl.setHasFixedSize(true);
        initPaymentMethod();

        getProfile();
    }

    @Override
    protected void onViewCreated() {
        btnPayViaPaypal.setOnClickListener(this);
        btnPayViaSkrill.setOnClickListener(this);
        if (NetworkUtility.getInstance(getApplicationContext()).isNetworkAvailable()) {
            processButtonPaypal();
        } else {
            AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
        }
        btnBuy.setOnClickListener(this);
        edtCredits.requestFocus();

        tvCreditsAvailable.setText(StringUtil.convertNumberPrice(user.getBalance()));

        edtCredits.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence != null && !charSequence.toString().isEmpty()) {
                    double price = Double.valueOf(charSequence.toString());
                    int exchange = Integer.parseInt(DataStoreManager.getSettingUtility().getExchange_rate());
                    int fee = Integer.parseInt(DataStoreManager.getSettingUtility().getExchange_fee());
                    price = price / exchange;
                    tvTotal.setText(AppUtil.formatCurrency(price + fee));
                } else {
                    tvTotal.setText("0");
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getSettingUtility();
    }

    @Override
    public void onClick(View view) {
        if (view == btnBuy) {
            if (NetworkUtility.getInstance(getApplicationContext()).isNetworkAvailable()) {
                if (isValid()) {
                    if (mSelectedPayment == null) {
                        AppUtil.showToast(getApplicationContext(), R.string.msg_chose_method_payment);

                    } else {

                        if (mSelectedPayment.equals(PaymentMethodObj.MNT_MONEY)) {
                            requestPaymentBillmap();
                        } else if (mSelectedPayment.equals(PaymentMethodObj.PAYPAL)) {
                            java.text.DecimalFormat format = new java.text.DecimalFormat();
                            try {

                                Number number = format.parse(tvTotal.getText().toString());
                                double price = number.doubleValue();

                                Paypal.requestPaypalPayment(this, price);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            AppUtil.showToast(self, "Developing");
                        }
                    }

                }
            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        }
    }


    private void requestPaymentBillmap() {
        showProgress(true);
        ApiUtils.getAPIPayment().processOnlinePayment(BillMap.Service_Provider_Code,
                BillMap.PASSWORD, BillMap.NUMBER, BillMap.REF, amount, BillMap.MetaData)
                .enqueue(new Callback<ResponseOnlinePayment>() {
                    @Override
                    public void onResponse(Call<ResponseOnlinePayment> call, Response<ResponseOnlinePayment> response) {
                        showProgress(false);
                        if (response.body() != null) {
                            if (response.body().isSuccess(self)) {
                                ResponseOnlinePayment responseOnlinePayment = response.body();
                                Log.e(TAG, "onResponse: " + new Gson().toJson(responseOnlinePayment));
                                callIpn(responseOnlinePayment);
                            }


                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseOnlinePayment> call, Throwable t) {
                        Log.e("XML", t.getMessage());
                        showProgress(false);

                    }
                });
    }

    private void callIpn(ResponseOnlinePayment responseOnlinePayment) {
        showProgress(true);
        ApiUtils.getAPIService().ipn(BillMap.FROM_MOBILE, DataStoreManager.getTokenUser(),
                responseOnlinePayment.billMapTransactionId,
                responseOnlinePayment.eWPTransactionId,
                responseOnlinePayment.responseCode,
                responseOnlinePayment.responseMessage)
                .enqueue(new Callback<ResponseIpn>() {
                    @Override
                    public void onResponse(Call<ResponseIpn> call, Response<ResponseIpn> response) {
                        showProgress(false);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().isSuccess(self)) {

                                    ResponseIpn responseIpn = response.body();
                                    Log.e(TAG, "ipn onResponse: " + responseIpn.toJson());

                                    //onPay();
                                    sendTransactionsToServer(responseIpn.data.billmapTransactionId);
                                } else {

                                    Log.e(TAG, "onResponse: " + response.body().getMessage());
                                }
                            }

                        } else {
                            Log.e(TAG, "onResponse: " + response.message());
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseIpn> call, Throwable t) {
                        AppUtil.showToast(self, R.string.msg_have_some_errors);
                        showProgress(false);

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Paypal.PAYPAL_REQUEST_CODE) {
            if (Paypal.isConfirm(data)) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                JSONObject jsonObject = confirm.toJSONObject();
                try {
                    JSONObject response = jsonObject.getJSONObject("response");
                    if (NetworkUtility.getInstance(getApplicationContext()).isNetworkAvailable()) {
                        sendTransactionsToServer(response.getString("id"));
                    } else {
                        AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

    }


    private void getProfile() {

        RequestManager.getProfile(self, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(com.aveplus.avepay_cpocket.taxi.network.ApiResponse response) {
                UserObj userObj = response.getDataObject(UserObj.class);
                UserObj user = DataStoreManager.getUser();
                user.setBalance(userObj.getBalance());
                DataStoreManager.saveUser(user);
                tvCreditsAvailable.setText(StringUtil.convertNumberPrice(user.getBalance()));
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    private void sendTransactionsToServer(String id) {
        Log.e(TAG, "sendTransactionsToServer:  " + id);
        String amount = edtCredits.getText().toString();
        ModelManager.transaction(this, id, amount, ACTION_BUY_CREDIT, "", "", methodPayment, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                ApiResponse apiResponse = new ApiResponse(jsonObject);
                if (!apiResponse.isError()) {
                    JSONObject obj = apiResponse.getDataObject();
                    try {
                        String balance = obj.getString("balance");
                        if (balance != null) {
                            tvCreditsAvailable.setText(StringUtil.convertNumberPrice(Float.parseFloat(balance)));
                            UserObj user = DataStoreManager.getUser();
                            user.setBalance(Float.parseFloat(balance));
                            DataStoreManager.saveUser(user);
                            AppUtil.showToast(getApplicationContext(), R.string.msg_transaction_success);

                            if (isNeedFinished) {
                                setResult(Args.RQ_BUY_CREDIT, new Intent());
                                finish();
                            } else {
                                finish();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    AppUtil.showToast(getApplicationContext(), apiResponse.getMessage());
                }
            }

            @Override
            public void onError() {
                Log.e(TAG, "Send BuyCredit to Server Error!");
            }
        });
    }

    private String amount;

    private boolean isValid() {
        amount = edtCredits.getText().toString();
        if (StringUtil.isEmpty(amount)) {
            AppUtil.showToast(getApplicationContext(), R.string.msg_amout_is_require);
            return false;
        } else {
            int amountCredit = Integer.parseInt(amount);
            if (amountCredit <= 0) {
                AppUtil.showToast(getApplicationContext(), R.string.msg_value_credits);
                return false;
            }
        }
        return true;
    }

    private void processButtonSkrill() {
        methodPayment = SKRILL;
        imgSkrill.setImageResource(R.drawable.stripe_on);
        imgPaypal.setImageResource(R.drawable.paypal_off);
    }

    private void processButtonPaypal() {
        methodPayment = PAYPAL;
        imgSkrill.setImageResource(R.drawable.stripe_off);
        imgPaypal.setImageResource(R.drawable.paypal_on);
    }

    private void getSettingUtility() {
        ModelManager.getSettingUtility(this, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                ApiResponse apiResponse = new ApiResponse(jsonObject);
                if (!apiResponse.isError()) {
                    DataStoreManager.saveSettingUtility(jsonObject.toString());
                    SettingsObj settingsObj = DataStoreManager.getSettingUtility();
                    tvFee.setText(String.format(getString(R.string.you_have_to_pay_01_dollar_transaction_fee), settingsObj.getExchange_fee()));
                } else {
                    Log.e(TAG, "GET SETTING UTILITY ERROR!");
                }
            }

            @Override
            public void onError() {
                Log.e(TAG, "GET SETTING UTILITY ERROR!");
            }
        });
    }


    private void initPaymentMethod() {
        PaymentMethodAdapter adapter = new PaymentMethodAdapter(new IPayment() {
            @Override
            public void onPaymentMethodSelected(PaymentMethodObj obj) {


            }
        });

        mRcl.setAdapter(adapter);
    }

    private RecyclerView mRcl;

    private String mSelectedPayment = PaymentMethodObj.MNT_MONEY;

    class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder> {

        private ArrayList<PaymentMethodObj> paymentMethodObjs;
        private IPayment iPayment;

        public PaymentMethodAdapter(IPayment iPayment) {
            this.iPayment = iPayment;

            if (paymentMethodObjs == null) {
                paymentMethodObjs = new ArrayList<>();
                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.PAYPAL, getString(R.string.paypal), true));
//                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.ORANGE_MONEY, getString(R.string.orange_money), false));
//                paymentMethodObjs.add(new PaymentMethodObj(PaymentMethodObj.MOOV_MONEY, getString(R.string.moov_money), false));
            }
        }

        @Override
        public PaymentMethodAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_method, parent, false);

            return new PaymentMethodAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final PaymentMethodAdapter.ViewHolder holder, final int position) {
            if (getItemCount() > 0) {

                final PaymentMethodObj obj = paymentMethodObjs.get(position);

                if (obj != null) {
                    holder.lblMethod.setText(obj.getName());
                    if (obj.isSelected()) {
                        holder.lblMethod.setTextColor(ContextCompat.getColor(self, R.color.white));
                        holder.lblMethod.setBackgroundResource(R.drawable.bg_accent_shadow);
                    } else {
                        holder.lblMethod.setTextColor(ContextCompat.getColor(self, R.color.textColorPrimary));
                        holder.lblMethod.setBackgroundResource(R.drawable.bg_pressed_grey);
                    }

                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (!obj.getId().equals(PaymentMethodObj.MOOV_MONEY)
                                    && !obj.getId().equals(PaymentMethodObj.ORANGE_MONEY)) {

                                if (!mSelectedPayment.equals(obj.getId())) {


                                    for (int i = 0; i < getItemCount(); i++) {
                                        PaymentMethodObj paymentMethodObj = paymentMethodObjs.get(i);
                                        if (paymentMethodObj.isSelected()) {
                                            paymentMethodObj.setSelected(false);
                                            break;
                                        }
                                    }

                                    obj.setSelected(true);
                                    notifyDataSetChanged();
                                    mSelectedPayment = obj.getId();
                                    iPayment.onPaymentMethodSelected(obj);
                                }


                            } else {
                                AppUtil.showToast(self, R.string.currently_unavailable);
                            }

                        }
                    });
                }


            }
        }

        @Override
        public int getItemCount() {
            try {
                return paymentMethodObjs.size();
            } catch (NullPointerException ex) {
                return 0;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView lblMethod;

            public ViewHolder(View view) {
                super(view);

                lblMethod = view.findViewById(R.id.lbl_payment_method);
            }
        }
    }


    interface IPayment {
        void onPaymentMethodSelected(PaymentMethodObj obj);
    }
}
