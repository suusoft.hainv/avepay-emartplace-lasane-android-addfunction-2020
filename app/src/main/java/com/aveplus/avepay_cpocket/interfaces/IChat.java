package com.aveplus.avepay_cpocket.interfaces;

import android.view.View;

/**
 * Created by Suusoft on 12/17/2016.
 */

public interface IChat {

    void onUserClicked(Object obj);

    void onActionClicked(View view, Object obj);
}
