package com.aveplus.avepay_cpocket.view.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.ActionReceiver;
import com.aveplus.avepay_cpocket.objects.DealCateObj;
import com.aveplus.avepay_cpocket.objects.DealReviewObj;
import com.aveplus.avepay_cpocket.objects.RecentChatObj;
import com.aveplus.avepay_cpocket.quickblox.SharedPreferencesUtil;
import com.aveplus.avepay_cpocket.quickblox.conversation.utils.DialogUtil;
import com.aveplus.avepay_cpocket.utils.DateTimeUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.aveplus.avepay_cpocket.utils.StringUtil;
import com.aveplus.avepay_cpocket.view.adapters.DealReviewAdapter;
import com.aveplus.avepay_cpocket.view.fragments.DealAboutFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealReviewFragment;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.Constants;
import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.objects.SettingsObj;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.aveplus.avepay_cpocket.parsers.JSONParser;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.widgets.recyclerview.EndlessRecyclerOnScrollListener;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;
import com.quickblox.users.model.QBUser;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Suusoft on 11/24/2016.
 */

public class DealDetailActivity extends com.aveplus.avepay_cpocket.base.BaseActivity implements View.OnClickListener, EndlessRecyclerOnScrollListener.OnLoadMoreListener {
    private static final String TAG = DealDetailActivity.class.getName();
    private static final int RC_ACTIVATE_DEAL = 1;
    private static final int RQ_UPDATE_DEAL = 332;
    public static final int RC_UPDATE_DEAL = 344;
    /*reskin*/
    private ImageView imgDeal, imgFavorite;
    private TextView tvAddress, tvFavoriteCount, tvPrice, tvOldPrice,lblSalePercent,
            tvName, tvAbout, tvFileName, lblRateQuantity, tvEndTime, tvDeal;

    private RelativeLayout btnDeal;
    private LinearLayout llBtnDeal;
    private TextView btnUpdate, btnRenewDeal, btnDeactivate;

    private RatingBar ratingBar;
    private LinearLayout llParentTime;
    private RecyclerView rcvData;
    private DealReviewAdapter mAdapter;
    private ArrayList<DealReviewObj> mDatas;
    private EndlessRecyclerOnScrollListener onScrollListener;
    private int page = 1;
    //end reskin

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MenuItem itemActivate;
    private MenuItem itemDeActivate;
    private Menu menu;

    private DealObj item;
    private int percent;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_deal_detail;
    }

    @Override
    protected void getExtraData(Intent intent) {
        Bundle args = intent.getExtras();
        if (args.containsKey(Args.KEY_DEAL_OBJECT)) {
            item = args.getParcelable(Args.KEY_DEAL_OBJECT);
            if (item == null)
                finish();
        } else if (args.containsKey(Args.NOTIFICATION_TYPE)) {

            processNotificationNewDeal(args);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle args = intent.getExtras();
        if (args.containsKey(Args.NOTIFICATION_TYPE)) {

            processNotificationNewDeal(args);
        } else if (args.containsKey(Args.KEY_DEAL_OBJECT)) {
            DealObj item = args.getParcelable(Args.KEY_DEAL_OBJECT);
            getDetailDeal(item.getId());

        }
    }

    @Override
    protected void initilize() {
        if (item != null)
            setToolbarTitle(item.getName());
    }

    @Override
    protected void initView() {
        //reskin
        //initViewRs();

        init();
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if (item != null)
            setAdapter();
    }

    private void init() {
        tvDeal = (TextView) findViewById(R.id.tv_btn);
        btnDeal = (RelativeLayout) findViewById(R.id.btn_functions);
        btnDeal.setOnClickListener(this);

        // Set text for 'iwanadeal' button
        Log.d(TAG, "init: "+item.getSeller_id());
        if (item.getSeller_id().equals(DataStoreManager.getUser().getId())) {
            tvDeal.setText(R.string.iwanachat);
        } else {
            tvDeal.setText(R.string.deal);
        }
    }

    @Override
    public void onLoadMore(int page) {

    }

    private void getData() {
        ModelManager.getReview(self,Constants.DEAL, item.getSeller_id(), page, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                ApiResponse response = new ApiResponse(jsonObject);
                if (!response.isError()) {
                    mDatas.addAll(response.getDataList(DealReviewObj.class));
                    mAdapter.notifyDataSetChanged();
                    onScrollListener.onLoadMoreComplete();
                    onScrollListener.setEnded(JSONParser.isEnded(response, page));
                    if (mDatas.isEmpty()) {
//                        llNoData.setVisibility(View.VISIBLE);
                    } else {
//                        llNoData.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError() {
                Toast.makeText(self, R.string.msg_have_some_errors, Toast.LENGTH_SHORT).show();

            }
        });
    }

    //reskin
    @Override
    public void onClick(View v) {
        if (v == btnDeal) {
            gotoChat();
        } else if (v == tvAddress) {
            gotoMap();
        } else if (v == imgFavorite) {
            favorite();
        } else if (v == tvFileName) {
            showFile();
        } else if (v==btnUpdate){

        } else if (v==btnRenewDeal){

        } else if (v==btnDeactivate){

        }

    }
    //end reskin

    private void showFile() {
        Dialog builder = new Dialog(self);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imageView = new ImageView(self);
        ImageUtil.setImage(self, imageView, item.getAttachment());
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }


    private void favorite() {
        ModelManager.favorite(self, item.getId(), ModelManager.FAVORITE_TYPE_DEAL, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                ApiResponse response = new ApiResponse(jsonObject);
                if (!response.isError()) {
                    item.setFavorite(!item.isFavorite());
                    if (item.isFavorite()) {
                        item.setFavoriteQuantity(item.getFavoriteQuantity() + 1);
                        Toast.makeText(self, getString(R.string.favorited), Toast.LENGTH_LONG).show();
                    } else {
                        item.setFavoriteQuantity(item.getFavoriteQuantity() - 1);
                        Toast.makeText(self, getString(R.string.unfavorited), Toast.LENGTH_LONG).show();
                    }
                    setFavourite();
                    ActionReceiver.sendBroadcast(self, ActionReceiver.ACTION_FAVORITE);
                } else {
                    Toast.makeText(self, response.getMessage(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onError() {

            }
        });
    }



    private void gotoMap() {
        FindUsOnMapActivity.start(DealDetailActivity.this, item, null);
    }


    private void gotoChat() {
        item.setBuyerId(DataStoreManager.getUser().getId());
        item.setBuyerName(DataStoreManager.getUser().getName());

        if (item.getSeller_id().equals(DataStoreManager.getUser().getId())) {
            RecentChatObj recentChatObj = new RecentChatObj(null, item, null);
            gotoChatForResult(recentChatObj);
        } else {
            /*QBUser qbUser = new QBUser();
            qbUser.setId(item.getSellerQbId());
            qbUser.setFullName(item.getProData().getname());
            String phone = DataStoreManager.getUser().getPhone() == null ? "" : DataStoreManager.getUser().getPhone();
            qbUser.setPhone(phone);*/

            String phone = DataStoreManager.getUser().getPhone() == null ? "" : DataStoreManager.getUser().getPhone();
            QBUser qbUser = SharedPreferencesUtil.getQbUser();
            Log.d(TAG, "gotoChat: "+qbUser+ " "+item);
            qbUser.setPhone(phone);

            RecentChatObj recentChatObj = new RecentChatObj(null, item, qbUser);
            RecentChatsActivity.start(DealDetailActivity.this, recentChatObj);
        }
    }

    private void gotoChatForResult(RecentChatObj obj) {
        Log.e(TAG, "gotoChatForResult mRecentChatObj " + new Gson().toJson(obj));
        Intent intent = new Intent(self, ChatActivityReskin2.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(Args.RECENT_CHAT_OBJ, obj);
        startActivityForResult(intent, RC_ACTIVATE_DEAL);
    }


    private void setFavourite() {
        if (item!=null){
            if (item.isFavorite()) {
                imgFavorite.setImageResource(R.drawable.ic_like_active);
            } else {
                imgFavorite.setImageResource(R.drawable.ic_like);
            }
            tvFavoriteCount.setText(item.getFavoriteQuantity() + "");
        }

    }


    @Override
    protected void onViewCreated() {

        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                setResult(RC_UPDATE_DEAL);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Args.KEY_DEAL_OBJECT, item);
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(RC_UPDATE_DEAL, intent);
                Log.e(TAG, "setResult(RC_UPDATE_DEAL, intent)" );
                finish();
            }
        });

       // setData();
    }

    public int percentPriceOldAndPriceNew(double priceOld, double priceNew){
        int i = (int) (((priceOld - priceNew) / priceOld ) * 100);
        return i;
    }

    public int percentPriceOldAndPriceSale(double priceOld, double priceSale){
        int i = (int) ( (priceSale / priceOld ) * 100);
        return i;
    }

    public void setData() {
        if (item != null) {
            ImageUtil.setImage(self, imgDeal, item.getImageUrl());
            tvName.setText(item.getName());
            tvName.setSelected(true);
            if (item.getDiscount_type() == null || item.getCategory_id() == Integer.parseInt(DealCateObj.LABOR)||item.getDiscount_type() != null && item.getDiscount_type().isEmpty()) {
                tvPrice.setText(String.format(getString(R.string.dollar_value),
                        StringUtil.convertNumberToString(item.getPrice(), 1)));
                tvOldPrice.setVisibility(View.GONE);
                lblSalePercent.setVisibility(View.GONE);
            } else if (item.getDiscount_type() != null && item.getDiscount_type().equals(Constants.AMOUNT)) {

                if (item.getDiscount_price() != 0) {
                    tvPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice() - item.getDiscount_price(), 1)));
                    tvOldPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice(), 1)));
                    tvOldPrice.setVisibility(View.VISIBLE);
                    percent = percentPriceOldAndPriceSale(item.getPrice(), item.getDiscount_price() );
                    lblSalePercent.setText(percent + " %");
                    lblSalePercent.setVisibility(View.VISIBLE);
                } else {
                    tvPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice(), 1)));
                    tvOldPrice.setVisibility(View.GONE);
                    lblSalePercent.setVisibility(View.GONE);

                }
            } else if (item.getDiscount_type() != null && item.getDiscount_type().equals(Constants.PERCENT)) {
                if (item.getDiscount_rate() > 0) {
                    tvPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getSale_price(), 1)));
                    tvOldPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice(), 1)));
                    tvOldPrice.setVisibility(View.VISIBLE);
                    percent = percentPriceOldAndPriceNew(item.getPrice(), item.getSale_price());
                    lblSalePercent.setText(percent + " %");
                    lblSalePercent.setVisibility(View.VISIBLE);
                } else {
                    tvPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice(), 1)));
                    tvOldPrice.setVisibility(View.GONE);
                    lblSalePercent.setVisibility(View.GONE);
                }
            }

            tvAbout.setText(item.getDescription());
            tvFavoriteCount.setText(item.getFavoriteQuantity() + "");
            tvAddress.setText(item.getAddress());

            if (item.getRateQuantity() > 0) {
               ratingBar.setVisibility(View.VISIBLE);
               lblRateQuantity.setVisibility(View.VISIBLE);

               ratingBar.setRating(item.getRate());
               lblRateQuantity.setText(String.valueOf(item.getRateQuantity()));
            } else {
                ratingBar.setVisibility(View.GONE);
                lblRateQuantity.setVisibility(View.GONE);
            }

            int favRes = item.isFavorite() ? R.drawable.ic_like_active : R.drawable.ic_like;
            imgFavorite.setImageResource(favRes);
            if (item.isOnline() && item.getSeller_id().equals(DataStoreManager.getUser().getId())) {
                llParentTime.setVisibility(View.VISIBLE);
                long time = Long.parseLong(item.getOnline_started()) + (item.getOnline_duration() * 3600);
                String datetime = DateTimeUtil.convertTimeStampToDate(time, "HH:mm, EEE dd-MM-yyyy");
                tvEndTime.setText(datetime);
            } else if (!item.isOnline() && item.getSeller_id().equals(DataStoreManager.getUser().getId())) {
                llParentTime.setVisibility(View.VISIBLE);
                tvEndTime.setText(getString(R.string.msg_expired));
            }
            if (!item.getSeller_id().equals(DataStoreManager.getUser().getId()) || item.isHideEndTime()) {
                llParentTime.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onBackPressed() {
//        setResult(RC_UPDATE_DEAL);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Args.KEY_DEAL_OBJECT, item);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RC_UPDATE_DEAL, intent);
        Log.e(TAG, "setResult(RC_UPDATE_DEAL, intent)" );
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        if (item != null) {
            initMenu(menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    private void initMenu(Menu menu) {
        if (DealDetailActivity.this.item.getSeller_id().equals(DataStoreManager.getUser().getId())) {


            if (itemActivate == null && itemDeActivate == null) {
                getMenuInflater().inflate(R.menu.menu_detail_deal, menu);
                itemActivate = menu.findItem(R.id.action_activate);
                itemDeActivate = menu.findItem(R.id.action_deactivate);
            }
            updateOptionsMenu();
            btnDeal.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_update:
                Intent intent = new Intent(getApplicationContext(), UpdateDealActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Args.KEY_DEAL_OBJECT, DealDetailActivity.this.item);
                intent.putExtras(bundle);
                startActivityForResult(intent, RQ_UPDATE_DEAL);
                break;
            case R.id.action_activate:
                if (NetworkUtility.getInstance(getApplicationContext()).isNetworkAvailable()) {
                    activateDeal(DealDetailActivity.this.item.getId(), Constants.ON);
                } else {
                    AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
                }

                break;
            case R.id.action_deactivate:
                if (NetworkUtility.getInstance(getApplicationContext()).isNetworkAvailable()) {
                    activateDeal(DealDetailActivity.this.item.getId(), Constants.OFF);
                } else {
                    AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
                }

                break;
            case R.id.action_renew_deal:
                showDurationBuyingDialog(R.string.msg_enter_deal_duration_to_be_available);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_UPDATE_DEAL) {
            Log.e(TAG, "requestCode == RQ_UPDATE_DEAL");
            if (resultCode == Args.RC_UPDATE_DEAL) {

                Log.e(TAG, "resultCode == Args.RC_UPDATE_DEAL");
                Bundle args = data.getExtras();
                if (args != null) {
                    if (args.containsKey(Args.KEY_DEAL_OBJECT)) {
                        item = args.getParcelable(Args.KEY_DEAL_OBJECT);
                        //setData();
                        setAdapter();


                        setToolbarTitle(item.getName());
                        updateOptionsMenu();
                    }
                }
//                finish();

            }

        } else
            //Reskin
        if (requestCode == RC_ACTIVATE_DEAL) {
            if (resultCode == Activity.RESULT_OK) {
                // Update deal status(active or inactive) after coming back from Chat screen
                if (item != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        if (bundle.containsKey(Args.IS_ACTIVATED_DEAL)) {
                            item.setIs_online(bundle.getBoolean(Args.IS_ACTIVATED_DEAL) ? DealObj.DEAL_ACTIVE : DealObj.DEAL_INACTIVE);

                            // Refresh deal object in parent activity
                            getItem().setIs_online(bundle.getBoolean(Args.IS_ACTIVATED_DEAL) ? DealObj.DEAL_ACTIVE : DealObj.DEAL_INACTIVE);
                            updateOptionsMenu();
                        }
                    }
                }
            }
        }
        //end reskin
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void setAdapter() {
        viewPager.setAdapter(new AdapterViewPagger(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }




    private class AdapterViewPagger extends FragmentStatePagerAdapter {

        String[] tabs = getResources().getStringArray(R.array.deal_detail);

        public AdapterViewPagger(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return DealAboutFragment.newInstance(item);
            } else if (position == 1) {
                return DealReviewFragment.newInstance(item.getId(), Constants.DEAL, item.getSeller_id());
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public int getCount() {
            return tabs.length;
        }
    }

    public void updateOptionsMenu() {
        //cái này sử dụng khi app phải phân biệt các deal active và inactive
//        Date dateSystem = Calendar.getInstance().getTime(), dateTime;
//        dateTime = DateTimeUtil.convertTimeStampTooDate(Long.parseLong(item.getOnline_started()) + (item.getOnline_duration() * 3600));
//        if (dateSystem.before(dateTime)){
//            itemActivate.setVisible(false);
//            itemDeActivate.setVisible(true);
//        }else {
//            itemDeActivate.setVisible(false);
//            itemActivate.setVisible(true);
//        }

        if (item.isOnline()) {
            itemActivate.setVisible(false);
            itemDeActivate.setVisible(true);
        } else {
            itemDeActivate.setVisible(false);
            itemActivate.setVisible(true);
        }
    }

    public DealObj getItem() {
        return item;
    }

    private void activateDeal(String dealId, final String mode) {
        ModelManager.activateDeal(self, dealId, mode, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                if (JSONParser.responseIsSuccess(jsonObject)) {
                    if (mode.equals(Constants.OFF)) {
                        item.setIs_online(DealObj.DEAL_INACTIVE);

                        itemActivate.setVisible(true);
                        itemDeActivate.setVisible(false);

                        Toast.makeText(self, R.string.msg_deactivate_success, Toast.LENGTH_SHORT).show();
                    } else {
                        item.setIs_online(DealObj.DEAL_ACTIVE);

                        itemActivate.setVisible(false);
                        itemDeActivate.setVisible(true);

                        Toast.makeText(self, R.string.msg_activate_success, Toast.LENGTH_SHORT).show();
                    }
                    setAdapter();
                } else {
                    if (mode.equals(Constants.ON)) {
                        showDurationBuyingDialog(R.string.msg_enter_deal_duration_to_be_available);
                    }
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    private void showDurationBuyingDialog(int msgId) {
        final Dialog dialog = DialogUtil.setDialogCustomView(self, R.layout.dialog_buying_duration, true);

        final EditText txtDuration = (EditText) dialog.findViewById(R.id.txt_duration);
        TextViewRegular lblMsg = (TextViewRegular) dialog.findViewById(R.id.lbl_msg);
        final TextViewRegular lblFee = (TextViewRegular) dialog.findViewById(R.id.lbl_msg_fee);
        TextViewBold lblBuyCredits = (TextViewBold) dialog.findViewById(R.id.lbl_buy_credits);
        TextViewBold lblAvailable = (TextViewBold) dialog.findViewById(R.id.lbl_available);

        lblMsg.setText(msgId);
        lblFee.setText(String.format(getString(R.string.msg_subtract_credits), "0"));

        SettingsObj settingsObj = DataStoreManager.getSettingUtility();
        final int feePerHour;
        if (item.getIs_premium() == 1) {
            feePerHour = (settingsObj != null && settingsObj.getPremium_deal_online_rate() != null
                    && !settingsObj.getPremium_deal_online_rate().equals("")) ?
                    Integer.parseInt(settingsObj.getPremium_deal_online_rate()) : 1;
        } else {
            feePerHour = (settingsObj != null && settingsObj.getDeal_online_rate() != null
                    && !settingsObj.getDeal_online_rate().equals("")) ?
                    Integer.parseInt(settingsObj.getDeal_online_rate()) : 1;
        }


        txtDuration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String duration = editable.toString().trim().isEmpty() ? "0" : editable.toString().trim();

                lblFee.setText(String.format(getString(R.string.msg_subtract_credits), String.valueOf((Integer.parseInt(duration) * feePerHour))));
            }
        });

        lblBuyCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                GlobalFunctions.startActivityWithoutAnimation(self, BuyCreditsActivity.class);
            }
        });

        lblAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int duration = 0;
                if (txtDuration.getText().toString().trim().length() > 0) {
                    duration = Integer.parseInt(txtDuration.getText().toString().trim());
                }

                if (duration == 0 || duration > 24) {
                    Toast.makeText(self, R.string.msg_duration_must_gt_zero, Toast.LENGTH_LONG).show();
                } else {
                    if (DataStoreManager.getUser().getBalance() < duration) {
                        Toast.makeText(self, String.format(getString(R.string.msg_balance_is_not_enough),
                                String.valueOf(duration)), Toast.LENGTH_LONG).show();
                    } else {
                        dialog.dismiss();

                        if (item != null) {
                            updateDurationOfDeal(item.getId(), duration, (duration * feePerHour));
                        }
                    }
                }
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void updateDurationOfDeal(String dealId, final int duration, final int creditsFee) {
        ModelManager.updateDurationOfDeal(self, dealId, String.valueOf(duration), new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                if (JSONParser.responseIsSuccess(jsonObject)) {
                    ApiResponse apiResponse = new ApiResponse(jsonObject);
                    DealObj dealObj = apiResponse.getDataObject(DealObj.class);
                    item.setIs_online(DealObj.DEAL_ACTIVE);

                    itemDeActivate.setVisible(true);
                    itemActivate.setVisible(false);

                    UserObj userObj = DataStoreManager.getUser();
                    userObj.setBalance(userObj.getBalance() - creditsFee);
                    DataStoreManager.saveUser(userObj);
                    item.setOnline_duration(item.getOnline_duration() + duration);
                    item.setOnline_started(dealObj.getOnline_started());
                    setAdapter();
                    Toast.makeText(self, R.string.msg_success, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    private void processNotificationNewDeal(Bundle args) {
        if (args != null) {
            if (args.containsKey(Args.KEY_ID_DEAL)) {
                String id = args.getString(Args.KEY_ID_DEAL);
                if (id != null && !id.equalsIgnoreCase(""))
                    if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                        getDetailDeal(id);
                    } else {
                        AppUtil.showToast(this, R.string.msg_network_not_available);
                    }
            }
        }
    }

    private void getDetailDeal(final String id) {
        ModelManager.getDetailDeal(this, id, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                org.json.JSONObject jsonObject = (JSONObject) object;
                ApiResponse response = new ApiResponse(jsonObject);
                if (!response.isError()) {
                    item = response.getDataObject(DealObj.class);
                    if (item != null) {
                        setAdapter();
                        setToolbarTitle(item.getName());
                        if (itemActivate == null && itemDeActivate == null) {
                            initMenu(menu);
                        }
                    } else {
                        finish();
                    }
                }
            }

            @Override
            public void onError() {
                Log.e(TAG, "ERROR: GET DETAIL DEAL");
            }
        });
    }
}
