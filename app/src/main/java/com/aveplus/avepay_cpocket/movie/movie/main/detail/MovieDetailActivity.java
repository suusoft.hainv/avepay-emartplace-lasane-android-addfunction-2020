package com.aveplus.avepay_cpocket.movie.movie.main.detail;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.MoviePlayerActivity;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.onUpdateBackdropMovie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.onUpdateMovieDetail;


import java.lang.ref.WeakReference;
import java.util.List;

import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;


public class MovieDetailActivity extends Activity implements View.OnClickListener, View.OnLayoutChangeListener {
    public final String TAG = getClass().getSimpleName();

    public static final String CATEGORY_EXTRA = "position category extra";
    public static final String SUBCATEGORY_EXTRA = "position subcategory extra";
    public static final String MOVIE_EXTRA_POSITION = "position movie extra";
    public static final String MOVIE_EXTRA_OBJECT = "MOVIE_EXTRA_OBJECT";

    private ImageButton mBackBtn, mPlayBtn;
    private NetworkImageView mPosterImgView;
    private NetworkImageView img_backdrop;
    private TextView mNameTxtView, mYearTxtView, mImdbTxtView, mDirectorTxtView, mPlotTxtView;
    private HListView mMoviesHListView;

    private int mMoviePosition;
    private int mCategoryPosition;
    private int mSubcategoryPosition;

    private List<Movie> movies;
    private MoviesManagement moviesManagement;
    private ImageLoader mImageLoader;
    private Movie mCurrentMovie;
    private boolean isTabblet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        isTabblet = getResources().getBoolean(R.bool.isTablet);
        if (isTabblet) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        super.onCreate(savedInstanceState);
        //set defaul background
        if (isTabblet) {
            getWindow().setBackgroundDrawableResource(R.drawable.anhnen);
        } else {
            getWindow().setBackgroundDrawableResource(R.drawable.anh_nen);
        }

        // Extract intent
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mCategoryPosition = bundle.getInt(CATEGORY_EXTRA);
            mSubcategoryPosition = bundle.getInt(SUBCATEGORY_EXTRA);
            mMoviePosition = bundle.getInt(MOVIE_EXTRA_POSITION);
            mCurrentMovie = bundle.getParcelable(MOVIE_EXTRA_OBJECT);
        }

        // Load views
        setContentView(R.layout.activity_movie_detail);
        mBackBtn = (ImageButton) findViewById(R.id.btn_back);
        mPlayBtn = (ImageButton) findViewById(R.id.btn_play);
        mPosterImgView = (NetworkImageView) findViewById(R.id.imgview_poster);
        img_backdrop = (NetworkImageView) findViewById(R.id.img_backdrop);
        mNameTxtView = (TextView) findViewById(R.id.txtview_name_movie);
        mYearTxtView = (TextView) findViewById(R.id.txtview_year_movie);
        mDirectorTxtView = (TextView) findViewById(R.id.txtview_director_movie);
        mImdbTxtView = (TextView) findViewById(R.id.txtview_imdb_movie);
        mPlotTxtView = (TextView) findViewById(R.id.txtview_plot_movie);
        mMoviesHListView = (HListView) findViewById(R.id.hlistview_movies);

        // change font for texts
        AssetManager assetManager = getAssets();
        mNameTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));
        mYearTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));
        mImdbTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));
        mDirectorTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));
        mPlotTxtView.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaLTStd-Light.otf"));

        mPosterImgView.addOnLayoutChangeListener(this);
        mBackBtn.setOnClickListener(this);
        mPlayBtn.setOnClickListener(this);

        mMoviesHListView.setSelector(R.drawable.bg_focus);
        mMoviesHListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e(TAG, "onItemClick: " + i);
                mMoviePosition = i;
                loadDetailOfMovieAtPosition(i);
            }
        });
        mMoviesHListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            private WeakReference weakFocusedView = null;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e(TAG, "onItemSelected: ");
                if (weakFocusedView != null && weakFocusedView.get() != null) {
                    View prevFocusedView = (View) weakFocusedView.get();
                    View dimView = prevFocusedView.findViewById(R.id.dimview);
                    if (dimView != null)
                        dimView.setVisibility(View.VISIBLE);
                    float scale = 1f;
                    prevFocusedView.setScaleX(scale);
                    prevFocusedView.setScaleY(scale);
                }

                View dimView = view.findViewById(R.id.dimview);
                if (dimView != null)
                    dimView.setVisibility(View.GONE);
                float scale = 1.1f;
                view.setScaleX(scale);
                view.setScaleY(scale);

                // store focused view
                View focusedView = view;
                weakFocusedView = new WeakReference(focusedView);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if (weakFocusedView != null && weakFocusedView.get() != null) {
                    View prevFocusedView = (View) weakFocusedView.get();
                    View dimView = prevFocusedView.findViewById(R.id.dimview);
                    if (dimView != null)
                        dimView.setVisibility(View.VISIBLE);
                    float scale = 1f;
                    prevFocusedView.setScaleX(scale);
                    prevFocusedView.setScaleY(scale);
                }
            }
        });
        mMoviesHListView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mMoviesHListView.setSelected(hasFocus);
                mMoviesHListView.setSelectionInt(-1);
            }
        });
        mMoviesHListView.setNextFocusLeftId(R.id.hlistview_movies);

        initDataMovies();
        loadDetailOfMovieAtPosition(mMoviePosition);
        mPlayBtn.requestFocus();

        loadListMovies();
        // AdsUtil.loadBanner(findViewById(R.id.adView));
    }

    private void initDataMovies() {
        mImageLoader = RequestManager.getImageLoader();
        moviesManagement = MoviesManagement.getInstance();
        movies = moviesManagement.getmMovies();
    }

    private void loadListMovies() {
        MoviesAdapter adapter = new MoviesAdapter(this, movies);
        mMoviesHListView.setAdapter(adapter);
        mMoviesHListView.setSelection(mMoviePosition);
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        // Change size of Poster ImageView
        int posterHeight = mPosterImgView.getMeasuredHeight();
        ViewGroup.LayoutParams layoutParams = mPosterImgView.getLayoutParams();
        layoutParams.width = (int) (0.68f * posterHeight);
        mPosterImgView.setLayoutParams(layoutParams);

        layoutParams = mPlayBtn.getLayoutParams();
        layoutParams.width = (int) (0.68f * posterHeight);
        mPlayBtn.setLayoutParams(layoutParams);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_back:
                pressedBackButton();
                break;
            case R.id.btn_play:
                //AdsUtil.showInterstitialAdmob();
                pressedPlayButton();
                break;

            default:
                break;
        }
    }

    private void pressedBackButton() {
        finish();
    }

    private void pressedPlayButton() {
        Intent mMovieIntent = new Intent(MovieDetailActivity.this, MoviePlayerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(MoviePlayerActivity.CATEGORY_EXTRA, mCategoryPosition);
        bundle.putInt(MoviePlayerActivity.SUBCATEGORY_EXTRA, mSubcategoryPosition);
        bundle.putInt(MoviePlayerActivity.MOVIE_EXTRA, mMoviePosition);
        bundle.putParcelable(MoviePlayerActivity.EXTRA_MOVIE, mCurrentMovie);

        mMovieIntent.putExtras(bundle);
        startActivity(mMovieIntent);

//        // Test Android MediaPlayer
//        Intent iMovie = new Intent(this, SecondActivity.class);
//        iMovie.putExtra(SecondActivity.EXTRA_VIDEO, mCurrentMovie);
//        startActivity(iMovie);
    }

    private void loadDetailOfMovieAtPosition(int i) {
        mCurrentMovie = movies.get(i);
        if (mCurrentMovie != null) {
            mPosterImgView.setDefaultImageResId(R.drawable.poster);
            mPosterImgView.setErrorImageResId(R.drawable.poster);
            mPosterImgView.setImageUrl(mCurrentMovie.getMovie_thumb(), mImageLoader);
            // getBackDropBg(mCurrentMovie.getMovie_imdb());
            Spannable nameSpan = new SpannableString(mCurrentMovie.getMovie_name());
            nameSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, nameSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mNameTxtView.setText(nameSpan);

            Spannable yearSpan = new SpannableString("YEAR: " + mCurrentMovie.getMovie_year());
            yearSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 0, 210, 255)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mYearTxtView.setText(yearSpan);

            Spannable imdbSpan = new SpannableString("IMDB: " + mCurrentMovie.getMovie_rating());
            imdbSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 238, 177, 30)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mImdbTxtView.setText(imdbSpan);

            Spannable directorSpan = new SpannableString("Actors: " + mCurrentMovie.getMovie_actor());
            directorSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 0, 210, 255)), 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mDirectorTxtView.setText(directorSpan);

            mPlotTxtView.setTextColor(Color.WHITE);
            mPlotTxtView.setText(mCurrentMovie.getMovie_desc());
            // updateMovieInfo(mCurrentMovie.getMovie_imdb());
        }
    }

    private void getBackDropBg(String imdbMovie) {
        moviesManagement.movieBackdropImgRequest(imdbMovie, new onUpdateBackdropMovie() {
            @Override
            public void onCompleted(String response) {
                img_backdrop.setImageUrl(response, mImageLoader);
            }

            @Override
            public void onError(Exception ex) {
                img_backdrop.setBackgroundResource(R.drawable.backdrop);
            }
        });
    }

    private void updateMovieInfo(String imdb_id) {
        moviesManagement.getMovieInfo(imdb_id, new onUpdateMovieDetail() {
            @Override
            public void onResponde(Movie movie) {
                mCurrentMovie.setMovie_director(movie.getMovie_director());
                mCurrentMovie.setMovie_actor(movie.getMovie_actor());
                Spannable directorSpan = new SpannableString("Director: " + movie.getMovie_director());
                directorSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 0, 210, 255)), 0, 9, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                mDirectorTxtView.setText(directorSpan);
            }

            @Override
            public void onError() {

            }
        });
    }


}
