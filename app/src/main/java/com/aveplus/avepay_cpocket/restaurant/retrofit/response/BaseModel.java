package com.aveplus.avepay_cpocket.restaurant.retrofit.response;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.main.home.Banner.BannerModel;
import com.aveplus.avepay_cpocket.restaurant.main.login.LoginActivity;
import com.google.gson.Gson;


import java.util.List;

public class BaseModel {

    public String status;
    public int code;
    public String message;
    public String total_page;
    public List<BannerModel> banner;

    public List<BannerModel> getBanner() {
        return banner;
    }

    public void setBanner(List<BannerModel> banner) {
        this.banner = banner;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return getCode() == 200 || getCode() == 200;
    }

    public boolean isSuccess(Activity context) {
        if (getCode() == 200) {
            return true;
        } else if (getCode() == 205) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Login session expired");
            builder.setCancelable(false);
            builder.setMessage(R.string.login_session_expired);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    DataStoreManager.removeUser();
                    context.startActivity(new Intent(context, LoginActivity.class));
                    context.finish();
                }
            });
            builder.show();
            return false;
        } else {
            Toast.makeText(context, getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }

    }


    public String getTotal_page() {
        return total_page;
    }

    public void setTotal_page(String total_page) {
        this.total_page = total_page;
    }


}
