package com.aveplus.avepay_cpocket.trainingcenter.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseActivity;
import com.aveplus.avepay_cpocket.music.configs.Args;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.squareup.picasso.Picasso;


/**
 * Created by suusoft on 05/09/2017.
 */

public class BlogDetailActivity extends AppCompatActivity {
    private TextView txttitle, txtcontent;
    private ImageView imgnews, btnBack;

    private NewEvent item;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);
        initView();
        initData();
    }

    private void initData() {

        Context context = null;
        Bundle bundle = getIntent().getBundleExtra("bundle");
        String title = bundle.getString("title");
        String description = bundle.getString("description");
        String content = bundle.getString("content");
        String time = bundle.getString("time");
        String image = bundle.getString("image");
//        link = bundle.getString("link");
        txttitle.setText(title);
        txtcontent.setText(content);
        Picasso.get().load(image).into(imgnews);


    }


    public void initView() {
        txttitle = (TextView) findViewById(R.id.txtNewTitle);
        txtcontent = (TextView) findViewById(R.id.ContentNews);
        imgnews = (ImageView) findViewById(R.id.imgNewEvent);
        btnBack = (ImageView) findViewById(R.id.imgBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        String title = bundle.getString("title");
//        String content = bundle.getString("content");
//        String image = bundle.getString("image");
//
//        if (item != null) {
//            txttitle.setText(title);
//            txtcontent.setText(content);
//            Picasso.get().load(image);
//        }
//        btnBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });

    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
