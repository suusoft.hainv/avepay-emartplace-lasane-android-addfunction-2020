package com.aveplus.avepay_cpocket.ebook.viewmodel.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.util.FileUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ListChapterVM extends BaseViewModelList {
    private Bundle bundle;

    public ListChapterVM(Context context, Bundle bundle) {
        super(context,bundle);
        this.bundle = bundle;
        getData(1);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(self, Config.TYPE_NUMBER_COLUMN_GRID_BOOK);
    }

    // click on album home
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Config.MyStoreEvent event) {
        if (bundle.getInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK) == Constant.MENU_LEFT_HISTORY){
            new AlertDialog.Builder(self)
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.message_clear_history)
                    .setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            getListData().clear();
                            DataStoreManager.clearBook(Constant.LIST_BOOK_HISTORY);
                            notifyDataChanged(false);
                            AppUtil.showToast(self,R.string.successfully);

                        }
                    })
                    .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create()
                    .show();
        }
    }



    public void onRefresh() {
        getListData().clear();
        getData(1);
    }


    @Override
    public void getData(int page) {
        Log.e("eee", page + " Page");
        switch (bundle.getInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK)) {
            case Constant.MENU_LEFT_HISTORY:
                setListData(FileUtil.getAllFilesDownloaded());
                break;

            case Constant.MENU_LEFT_BOOKMARKS:
                setListData(DataStoreManager.getListChapter());
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                        }
//                    }, 500);
                break;

        }

//        Log.e("eee", page + " Page");
//        addListData(DataStoreManager.getListChapter());
//        Log.e("data", "list chapter:" + new Gson().toJson(getListData()));
    }
}

