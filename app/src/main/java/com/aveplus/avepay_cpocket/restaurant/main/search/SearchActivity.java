package com.aveplus.avepay_cpocket.restaurant.main.search;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.RestaurantAct;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.ObjectCustom;
import com.aveplus.avepay_cpocket.restaurant.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.restaurant.network.ApiResponse;
import com.aveplus.avepay_cpocket.restaurant.network.BaseRequest;
import com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.recyclerview.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;

import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.ID_RESTAURANT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_SELLLER_ID;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LAT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LONG;


public class SearchActivity extends BaseActivity implements View.OnClickListener, EndlessRecyclerOnScrollListener.OnLoadMoreListener {
    private EditText edSearch;
    private ImageView btnBack;
    private LinearLayout llNoData, btnSearch;
    private SearchAdapter adapter;
    private ArrayList<ObjectCustom> objectArrayList;
    private RecyclerView rcData;
    private double longitude = 105.777013;
    private double latitude = 21.037464;
    public static String TAG = SearchActivity.class.getSimpleName();
    private EndlessRecyclerOnScrollListener onScrollListener;
    private int page;
    private String query;


    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_search_restaurant;
    }

    @Override
    protected void initView() {
        llNoData = findViewById(R.id.ll_no_data);
        edSearch = findViewById(R.id.ed_search);
        btnSearch = findViewById(R.id.btn_search);
        btnBack = findViewById(R.id.btn_back);
        rcData = findViewById(R.id.rc_data);
        btnBack.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        if (objectArrayList == null) {
            objectArrayList = new ArrayList<>();
        } else {
            objectArrayList.clear();
        }

        edSearch.setImeActionLabel("Search", EditorInfo.IME_ACTION_SEARCH);
        edSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH || i == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == KeyEvent.ACTION_DOWN || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {
                    objectArrayList.clear();
                    query = edSearch.getText().toString();
                    page = 1;
                    onScrollListener.setCurrentPage(page);
                    getData(page, query);
                    adapter.notifyDataSetChanged();
                }
                return true;

            }
        });


        adapter = new SearchAdapter(self, objectArrayList, new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ObjectCustom object) {
                if (object.getType().equals("restaurant")) {
                    Intent intent = new Intent(self, RestaurantAct.class);
                    intent.putExtra(USER_LONG, longitude);
                    intent.putExtra(USER_LAT, latitude);
                    intent.putExtra(ID_RESTAURANT, object.getId());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(self, RestaurantAct.class);
                    intent.putExtra(USER_LONG, longitude);
                    intent.putExtra(USER_LAT, latitude);
                    intent.putExtra(PARAM_SELLLER_ID, object.getSeller_id());
                    startActivity(intent);
                }

            }
        });
        rcData.setAdapter(adapter);
        rcData.hasFixedSize();
        onScrollListener = new EndlessRecyclerOnScrollListener(this);
        rcData.addOnScrollListener(onScrollListener);
        onScrollListener.setEnded(false);

    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_search:
                objectArrayList.clear();
                query = edSearch.getText().toString();
                page = 1;
                onScrollListener.setCurrentPage(page);
                getData(page, query);
                adapter.notifyDataSetChanged();
                break;

            case R.id.btn_back:
                finish();
                break;
        }
    }


    // search here
    private void getData(int number, String text) {
        // Check network
        if (NetworkUtility.isNetworkAvailable()) {
            View view = this.getCurrentFocus();
            if (NetworkUtility.isNetworkAvailable()) {
                RequestManager.search(text, number, 10, new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        if (!response.isError()) {
                            objectArrayList.addAll(response.getDataList(ObjectCustom.class));
                            onScrollListener.onLoadMoreComplete();
                            onScrollListener.setEnded(ApiResponse.isEnded(response, page));
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }


                            checkViewHideShow();
                            adapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(String message) {

                    }
                });
            }

        } else {
            AppUtil.showToast(this, R.string.msg_no_network);
        }
    }

    //Check view show or hiden
    private void checkViewHideShow() {
        if (objectArrayList.size() == 0) {
            llNoData.setVisibility(View.VISIBLE);
        } else {
            llNoData.setVisibility(View.GONE);
        }
    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onLoadMore(int page) {
        this.page = page;
        getData(page, query);
        Log.e(TAG, "onLoadMore: " + page);
    }
}
