package com.aveplus.avepay_cpocket.music.main.main2;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.music.base.view.SingleListFragment;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.listener.IOnFragmentNavListener;
import com.aveplus.avepay_cpocket.music.main.addlist.FavoriteAddListFragment;
import com.aveplus.avepay_cpocket.music.main.album.AllAlbumByFragment;
import com.aveplus.avepay_cpocket.music.main.category.AllCategoryFragment;
import com.aveplus.avepay_cpocket.music.main.category.MusicByCategoryFragment;
import com.aveplus.avepay_cpocket.music.main.detail.AlbumFragment;
import com.aveplus.avepay_cpocket.music.main.downloaded.DownloadedFileFragment;
import com.aveplus.avepay_cpocket.music.main.favorite.FavoriteSongFragment;
import com.aveplus.avepay_cpocket.music.main.home.HomeFragment;
import com.aveplus.avepay_cpocket.music.main.library.LibraryFragment;
import com.aveplus.avepay_cpocket.music.main.localmusic.LocalMusicFragment;
import com.aveplus.avepay_cpocket.music.main.login.SplashLoginActivity;
import com.aveplus.avepay_cpocket.music.main.mymusic.MyMusicFragment;
import com.aveplus.avepay_cpocket.music.model.Category;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.main.player.ItemSongQueueVM;
import com.aveplus.avepay_cpocket.music.player.MusicService;
import com.aveplus.avepay_cpocket.music.main.player.NowPlayingFragment;
import com.aveplus.avepay_cpocket.music.main.player.PlaySongActivity;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.main.player.QueueSongVM;
import com.aveplus.avepay_cpocket.music.main.search.SearchFragment;
import com.aveplus.avepay_cpocket.music.util.BindingUtil;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.network1.MyProgressDialog;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class MainActivity2 extends BaseActivity {
    private static final String TAG = MainActivity2.class.getSimpleName();
    private BottomNavigationView mNavigationView;
    private FragmentTransaction transaction;
    private Toolbar toolbar;
    private DrawerLayout mDrawerLayout;
    private String tagFragment2Current = "", tagFragment3Current = "";

    private static final String TAG_LOCAL_MUSIC = "TAG_LOCAL_MUSIC";
    private static final String TAG_DOWNLOADED_FILE = "TAG_DOWNLOADED_FILE";
    private static final String TAG_FAVORITE = "TAG_FAVORITE";
    private static final String TAG_PROFILE = "TAG_PROFILE";
    private static final String TAG_AlBUM = "TAG_AlBUM";
    private static final String TAG_MUSIC_BY_CATEGORY = "TAG_MUSIC_BY_CATEGORY";
    private static final String TAG_MUSIC_ALL_CATEGORY = "TAG_MUSIC_ALL_CATEGORY";
    private static final String TAG_MUSIC_ALL_ALBUM = "TAG_MUSIC_ALL_ALBUM";
    private static final String TAG_FRAG_SEARCH = "TAG_FRAG_SEARCH";
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.home_fragment:
                    llPlayerBottom.setVisibility(View.VISIBLE);
                    removeFragment();
                    toolbar.setVisibility(View.GONE);
                    fragment = HomeFragment.newInstance();
                    loadFragment(fragment);
                    return true;
                case R.id.search_fragment:
                    llPlayerBottom.setVisibility(View.GONE);
                    removeFragment();
                    toolbar.setVisibility(View.GONE);
                    fragment = new SearchFragment();
                    loadFragment(fragment);
                    return true;

                case R.id.library_fragment:
                    llPlayerBottom.setVisibility(View.GONE);
                    removeFragment();
                    toolbar.setVisibility(View.GONE);
                    fragment = new LibraryFragment();
                    loadFragment(fragment);

                    return true;
                case R.id.account_fragment:
                    llPlayerBottom.setVisibility(View.GONE);
                    removeFragment();
                    toolbar.setVisibility(View.GONE);
                    fragment = MyMusicFragment.newInstance();
                    loadFragment(fragment);
                    return true;
            }

            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.framelayout, fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }

    // switch screen from right to left
    private void switchFragment2(Fragment fragment, String tag) {

        tagFragment2Current = tag;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.framelayout2, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();


    }

    private void removeScreen2(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            tagFragment2Current = "";
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }

    }

    // switch screen from bottom to top
    private void switchFragment3(Fragment fragment, String tag) {

        tagFragment3Current = tag;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.framelayout2, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    private void removeScreen3(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            tagFragment3Current = "";
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_main2;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void initView() {

        setTitle("");
        Song song = QueueManager.getInstance().getCurrentItem();
        mNavigationView = (BottomNavigationView) findViewById(R.id.navigation_view);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setVisibility(View.GONE);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        //initNavigationMenuLeft();

        //BottomNavigationViewHelper.disableShiftMode(mNavigationView);
        mNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        Fragment fragment = new HomeFragment();
        loadFragment(fragment);
        initViewPlayer();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initNavigationMenuLeft() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setVisibility(View.GONE);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();
                        int id = menuItem.getItemId();
                        if (id == R.id.nav_policy) {
                            AppUtil.goToWeb(self, getString(R.string.url_policy));
                        } else if (id == R.id.nav_aboutus) {
                            AppUtil.goToWeb(self, getString(R.string.about_web));
                        } else if (id == R.id.nav_term) {
                            AppUtil.goToWeb(self, getString(R.string.url_term));
                        } else if (id == R.id.nav_logout) {
                            if (DataStoreManager.getUser() != null) {
                                logout();
                            } else {
                                AppUtil.showToast(self, "No Account");
                            }
                        }
                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        return true;
                    }
                });
        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);


        MenuItem searchItem = menu.findItem(R.id.action_seach);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                toolbar.setVisibility(View.GONE);
                SearchFragment fragment = SearchFragment.newInstance();
                fragment.setListener(new IOnFragmentNavListener() {
                    @Override
                    public void onFragmentBack() {
                        toolbar.setVisibility(View.GONE);
                        removeScreen2(TAG_FRAG_SEARCH);

                    }
                });
                switchFragment2(fragment, TAG_FRAG_SEARCH);
                return false;
            }
        });


        return true;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        mMediaBrowser.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        MediaControllerCompat controllerCompat = MediaControllerCompat.getMediaController(this);
        if (controllerCompat != null) {
            controllerCompat.unregisterCallback(controllerCallback);
        }
        mMediaBrowser.disconnect();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ItemObjectClicked event) {

        MusicByCategoryFragment fragment = MusicByCategoryFragment.newInstance((Category) event.object);
        toolbar.setVisibility(View.GONE);
        fragment.setListener(new IOnFragmentNavListener() {
            @Override
            public void onFragmentBack() {
                toolbar.setVisibility(View.GONE);
                removeScreen3(TAG_MUSIC_BY_CATEGORY);

            }
        });
        switchFragment3(fragment, TAG_MUSIC_BY_CATEGORY);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ItemAlbumClicked event) {
        toolbar.setVisibility(View.GONE);
        AlbumFragment fragment = AlbumFragment.newInstance(event.object);
        fragment.setListener(new IOnFragmentNavListener() {
            @Override
            public void onFragmentBack() {
                toolbar.setVisibility(View.GONE);
                removeScreen3(TAG_AlBUM);

            }
        });
        switchFragment3(fragment, TAG_AlBUM);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ClickFavoriteAddList event) {
        toolbar.setVisibility(View.GONE);
        FavoriteAddListFragment fragment = FavoriteAddListFragment.newInstance(event.type);
        fragment.setListener(new IOnFragmentNavListener() {
            @Override
            public void onFragmentBack() {
                toolbar.setVisibility(View.GONE);
                removeScreen3(TAG_AlBUM);

            }
        });
        switchFragment3(fragment, TAG_AlBUM);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.HomeSongItemClicked event) {
        Log.e(TAG, "onMessageEvent: ClickSong");
        MediaControllerCompat.getMediaController(MainActivity2.this).getTransportControls().playFromMediaId("t", null);

        // actionPlayPause();
        AppUtil.startActivity(self, PlaySongActivity.class);
    }

    // receive from MyMusicVM class
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.MyMusicItemClicked event) {

        if (event.type == Global.LocalItemTypeClick.TYPE_DOWNLOADED) {

            DownloadedFileFragment fragment = DownloadedFileFragment.newInstance();
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {

                    removeScreen3(TAG_DOWNLOADED_FILE);

                }
            });
            switchFragment3(fragment, TAG_DOWNLOADED_FILE);
        } else if (event.type == Global.LocalItemTypeClick.TYPE_SONG) {
            LocalMusicFragment fragment = LocalMusicFragment.newInstance(0);
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen3(TAG_LOCAL_MUSIC);


                }
            });
            switchFragment3(fragment, TAG_LOCAL_MUSIC);
        } else if (event.type == Global.LocalItemTypeClick.TYPE_ARITST) {
            LocalMusicFragment fragment = LocalMusicFragment.newInstance(1);
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen3(TAG_LOCAL_MUSIC);

                }
            });
            switchFragment3(fragment, TAG_LOCAL_MUSIC);
        } else if (event.type == Global.LocalItemTypeClick.TYPE_FAVORITE) {
            FavoriteSongFragment fragment = FavoriteSongFragment.newInstance();
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    removeScreen3(TAG_FAVORITE);


                }
            });
            switchFragment3(fragment, TAG_FAVORITE);
        }
    }

    // event click more button
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ViewMoreClicked event) {
        if (event.position == 1) {
            toolbar.setVisibility(View.GONE);
            AllCategoryFragment fragment = AllCategoryFragment.newInstance();
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    toolbar.setVisibility(View.GONE);
                    removeScreen2(TAG_MUSIC_ALL_CATEGORY);

                }
            });
            switchFragment2(fragment, TAG_MUSIC_ALL_CATEGORY);
        } else {
            toolbar.setVisibility(View.GONE);
            AllAlbumByFragment fragment = AllAlbumByFragment.newInstance(event.object, event.position);
            fragment.setListener(new IOnFragmentNavListener() {
                @Override
                public void onFragmentBack() {
                    toolbar.setVisibility(View.GONE);
                    removeScreen2(TAG_MUSIC_ALL_ALBUM);

                }
            });
            switchFragment2(fragment, TAG_MUSIC_ALL_ALBUM);
        }

    }

    //bottom player
    private static final long PROGRESS_UPDATE_INTERNAL = 1000;
    private static final long PROGRESS_UPDATE_INITIAL_INTERVAL = 100;
    // player

    private LinearLayout llPlayerBottom;
    private ImageView btnPlayFooter;
    private ImageView imgSongFooter;
    private TextView tvNameSongFooter, tvNameSingerFooter;
    private ImageButton btnNextBottom, btnPrevBottom;
    private final ScheduledExecutorService mExecutorService =
            Executors.newSingleThreadScheduledExecutor();

    private ScheduledFuture<?> mScheduleFuture;
    private final Handler mHandler = new Handler();
    private final Runnable mUpdateProgressTask = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };

    private MediaBrowserCompat mMediaBrowser;
    private PlaybackStateCompat playbackStateCompat;

    private final MediaBrowserCompat.ConnectionCallback mConnectionCallbacks =
            new MediaBrowserCompat.ConnectionCallback() {
                @Override
                public void onConnected() {
                    connectToSession();
                }

                @Override
                public void onConnectionFailed() {
                    Log.e("MediaBrowserCompat", "onConnectionFailed");
                }

                @Override
                public void onConnectionSuspended() {
                    Log.e("MediaBrowserCompat", "onConnectionSuspended");
                }
            };

    private void connectToSession() {
        MediaSessionCompat.Token token = mMediaBrowser.getSessionToken();
        MediaControllerCompat mediaController = null;
        try {
            mediaController = new MediaControllerCompat(MainActivity2.this, // Context
                    token);
            MediaControllerCompat.setMediaController(MainActivity2.this, mediaController);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
        buildTransportControls();
    }

    MediaControllerCompat.Callback controllerCallback =
            new MediaControllerCompat.Callback() {
                @Override
                public void onMetadataChanged(MediaMetadataCompat metadata) {
                    if (metadata != null) {
                        updateMediaDescription(metadata.getDescription());
                        //updateMediaSong();
                        updateDuration(metadata);
                    }
                }

                @Override
                public void onPlaybackStateChanged(PlaybackStateCompat state) {
                    updatePlaybackState(state);
                }
            };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ItemChanged event) {
        updateMediaSong();
    }

    private void buildTransportControls() {
        MediaControllerCompat mediaController = MediaControllerCompat.getMediaController(MainActivity2.this);
        mediaController.registerCallback(controllerCallback);
        PlaybackStateCompat state = mediaController.getPlaybackState();
        updatePlaybackState(state);
        MediaMetadataCompat metadata = mediaController.getMetadata();
        if (metadata != null) {
            updateMediaDescription(metadata.getDescription());
            updateDuration(metadata);
        }
        updateProgress();
        if (state != null && (state.getState() == PlaybackStateCompat.STATE_PLAYING ||
                state.getState() == PlaybackStateCompat.STATE_BUFFERING)) {
            scheduleSeekbarUpdate();
        }
    }


    private void updateMediaDescription(MediaDescriptionCompat description) {
        if (description == null) {
            return;
        }
        tvNameSongFooter.setText(description.getTitle());
        tvNameSingerFooter.setText(description.getSubtitle());

        Song song = QueueManager.getInstance().getCurrentItem();
        if (song != null) {
            BindingUtil.setImage(imgSongFooter, song.image);

        }
    }

    private void updateMediaSong() {
        Song song = QueueManager.getInstance().getCurrentItem();
        if (song != null) {

            tvNameSongFooter.setText(song.getTitle());
            tvNameSingerFooter.setText(song.nameSinger);
            BindingUtil.setImage(imgSongFooter, song.image);

        }
    }


    private void updateDuration(MediaMetadataCompat metadata) {
        if (metadata == null) {
            return;
        }
        int duration = (int) metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);

    }

    private void initViewPlayer() {
        imgSongFooter = findViewById(R.id.img_song_footer);
        tvNameSongFooter = findViewById(R.id.tv_songname_footer);
        tvNameSingerFooter = findViewById(R.id.tv_artist_name_footer);
        btnPlayFooter = findViewById(R.id.btn_player_footer);
        llPlayerBottom = findViewById(R.id.llPlayerBottom);
        // content player

        btnPrevBottom = findViewById(R.id.btn_prev_bottom);
        btnNextBottom = findViewById(R.id.btn_next_bottom);


        llPlayerBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtil.startActivity(self, PlaySongActivity.class);
            }
        });
        btnNextBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(MainActivity2.this).getTransportControls();
                controls.skipToNext();
            }
        });

        btnPrevBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(MainActivity2.this).getTransportControls();
                controls.skipToPrevious();
            }
        });


        btnPlayFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionPlayPause();
            }
        });


        mMediaBrowser = new MediaBrowserCompat(this,
                new ComponentName(this, MusicService.class),
                mConnectionCallbacks, null); // optional Bundle

    }

    private void actionPlayPause() {
        PlaybackStateCompat state = MediaControllerCompat.getMediaController(MainActivity2.this).getPlaybackState();
        if (state != null) {
            MediaControllerCompat.TransportControls controls = MediaControllerCompat.getMediaController(MainActivity2.this).getTransportControls();
            switch (state.getState()) {
                case PlaybackStateCompat.STATE_PLAYING: // fall through
                case PlaybackStateCompat.STATE_BUFFERING:
                    controls.pause();

                    stopSeekbarUpdate();
                    break;
                case PlaybackStateCompat.STATE_PAUSED:
                case PlaybackStateCompat.STATE_STOPPED:
                    controls.play();
                    scheduleSeekbarUpdate();
                    break;
                default:
                    Log.d("onClick with state ", " " + state.getState());
            }
        }
    }

    private void updateUIState(boolean isPlaying) {
        if (isPlaying) {

            btnPlayFooter.setImageResource(R.drawable.ic_pause_white_24dp);
        } else {

            btnPlayFooter.setImageResource(R.drawable.ic_play_white);
        }
    }


    private void scheduleSeekbarUpdate() {
        stopSeekbarUpdate();
        if (!mExecutorService.isShutdown()) {
            mScheduleFuture = mExecutorService.scheduleAtFixedRate(
                    new Runnable() {
                        @Override
                        public void run() {
                            mHandler.post(mUpdateProgressTask);
                        }
                    }, PROGRESS_UPDATE_INITIAL_INTERVAL,
                    PROGRESS_UPDATE_INTERNAL, TimeUnit.MILLISECONDS);
        }
    }

    private void stopSeekbarUpdate() {
        if (mScheduleFuture != null) {
            mScheduleFuture.cancel(false);
        }
    }

    private void updateProgress() {
        if (playbackStateCompat == null) {
            return;
        }
        long currentPosition = playbackStateCompat.getPosition();
        if (playbackStateCompat.getState() == PlaybackStateCompat.STATE_PLAYING) {
            long timeDelta = SystemClock.elapsedRealtime() -
                    playbackStateCompat.getLastPositionUpdateTime();
            currentPosition += (int) timeDelta * playbackStateCompat.getPlaybackSpeed();
        }

    }

    private void updatePlaybackState(PlaybackStateCompat state) {
        if (state == null) {
            return;
        }
        playbackStateCompat = state;
        MediaControllerCompat controllerCompat = MediaControllerCompat.getMediaController(MainActivity2.this);
        if (controllerCompat != null && controllerCompat.getExtras() != null) {
        }

        switch (state.getState()) {
            case PlaybackStateCompat.STATE_PLAYING:
                updateUIState(true);
                scheduleSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_PAUSED:
                updateUIState(false);
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_NONE:
            case PlaybackStateCompat.STATE_STOPPED:
                updateUIState(false);
                stopSeekbarUpdate();
                break;
            case PlaybackStateCompat.STATE_BUFFERING:
                stopSeekbarUpdate();
                break;
            default:
                Log.d("updatePlaybackState", "Unhandled state " + state.getState());
        }

    }


    public void startAnimCircleImagePlaying(boolean isStart) {
        if (fragmentNowPlaying != null) {
            if (isStart) fragmentNowPlaying.startAnimation();
            else fragmentNowPlaying.stopAnimation();
        }
    }

    NowPlayingFragment fragmentNowPlaying;

    class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                SingleListFragment fragment = SingleListFragment.newInstance(QueueSongVM.class, R.layout.layout_list_playing, R.layout.item_song_queue, ItemSongQueueVM.class);
                //fragment.setBaseListener(this);
                return fragment;
            }
            if (position == 1) {
                fragmentNowPlaying = NowPlayingFragment.newInstance();
                return fragmentNowPlaying;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "";
            } else {
                return "";
            }
        }

        @Override
        public int getCount() {
            return 2;
        }


    }

    private void logout() {
        showDialogLogout();


    }

    private void showDialogLogout() {
        DialogUtil.showAlertDialog(self, R.string.log_out, R.string.you_wanto_logout, new DialogUtil.IDialogConfirm2() {
            @Override
            public void onClickOk() {
                requestLogout();
            }
        });
    }

    private void requestLogout() {
        DataStoreManager.removeUser();
        //  AppController.getInstance().setUserUpdated(true);

        AppUtil.startActivity(self, SplashLoginActivity.class);
        finish();

    }

    private void processBeforeLoggingOut(MyProgressDialog progressDialog) {
        if (DataStoreManager.getUser() != null) {

        }

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        DataStoreManager.removeUser();

        AppController.getInstance().setUserUpdated(true);

        AppUtil.startActivity(self, SplashLoginActivity.class);
        finish();

    }

    private void removeFragment() {
        removeScreen3(TAG_MUSIC_BY_CATEGORY);
        removeScreen3(TAG_FAVORITE);
        removeScreen3(TAG_LOCAL_MUSIC);
        removeScreen3(TAG_AlBUM);
        removeScreen2(TAG_FRAG_SEARCH);
        removeScreen2(TAG_MUSIC_ALL_CATEGORY);
        removeScreen2(TAG_MUSIC_ALL_ALBUM);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
