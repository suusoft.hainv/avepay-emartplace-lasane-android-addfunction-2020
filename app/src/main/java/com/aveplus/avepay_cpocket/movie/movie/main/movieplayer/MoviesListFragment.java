package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.customwidget.CenterLockHorizontalScrollview;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.CallBackOnPressLanguage;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.LoadMovieCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.OnFocusClickMovie;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onPageChangeAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.adapter.AdapterPlayerMovie;
import com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.adapter.AdapterPlayerSubcategory;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieGenre;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.MovieOfPageByGenreCallback;
import com.aveplus.avepay_cpocket.utils.Utils;


import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MoviesListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MoviesListFragment extends Fragment implements View.OnLayoutChangeListener,
        onPageChangeAdapter, CallBackOnPressLanguage, OnFocusClickMovie {
    public static final String EXTRA_MOVIE = "movie";
    public static final String CATEGORY_EXTRA = "position category extra";
    public static final String SUBCATEGORY_EXTRA = "position subcategory extra";
    public static final String MOVIE_EXTRA = "position movie extra";
    private final int ITEM_PER_PAGE = 7;

    private int mMoviePosition;
    private int mCategoryPosition;
    private int mSubcategoryPosition;
    private Movie mCurrentMovie;

    private List<MovieGenre> categories;

    private List<Movie> mMovies;
    private MoviesManagement moviesManagement;
    private ImageLoader mImageLoader;

    private CenterLockHorizontalScrollview mScrollSubCategory;
    private CenterLockHorizontalScrollview mScrollMovie;
    private ViewGroup mRowCategory, mRowMovie;
    private TextView mNameText, mYearText, mImdbText;
    private ImageButton btnBack, btnNext;
    private NetworkImageView mPosterImg;

    private Context mContext;
    // get movie by page number
    private AdapterPlayerMovie adapterPlayerMovie;
    private int totalPages = 0;
    private int totalResults = 0;
    private int currentPageNumber = 1;

    private LoadMovieCallback loadMovieCallback;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MoviesListFragment.
     */
    public static MoviesListFragment newInstance(int categoryPosition, int subcategoryPosition, int moviePosition, Movie movie) {
        MoviesListFragment fragment = new MoviesListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(MoviePlayerActivity.CATEGORY_EXTRA, categoryPosition);
        bundle.putInt(MoviePlayerActivity.SUBCATEGORY_EXTRA, subcategoryPosition);
        bundle.putInt(MoviePlayerActivity.MOVIE_EXTRA, moviePosition);
        bundle.putParcelable(MoviePlayerActivity.EXTRA_MOVIE, movie);
        fragment.setArguments(bundle);
        return fragment;
    }

    public MoviesListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageLoader = RequestManager.getImageLoader();
        moviesManagement = MoviesManagement.getInstance();
        categories = moviesManagement.getmMovieGenres();

        if (getArguments() != null) {
            mCurrentMovie = getArguments().getParcelable(EXTRA_MOVIE);
            mCategoryPosition = getArguments().getInt(CATEGORY_EXTRA, -1);
            // mSubcategoryPosition = getArguments().getInt(SUBCATEGORY_EXTRA, -1);

            mMoviePosition = getArguments().getInt(MOVIE_EXTRA, -1);
        }
        //TODO: change value
        mCategoryPosition = 0;
        mSubcategoryPosition = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movies_list, container, false);
        mScrollSubCategory = (CenterLockHorizontalScrollview) view.findViewById(R.id.scollSubCategory);
        mScrollMovie = (CenterLockHorizontalScrollview) view.findViewById(R.id.scollMovies);

        //mRowCategory = (ViewGroup) view.findViewById(R.id.rowCategory);
        mRowCategory = (ViewGroup) view.findViewById(R.id.rowSubcategory);
        mRowMovie = (ViewGroup) view.findViewById(R.id.rowMovies);

        btnBack = (ImageButton) view.findViewById(R.id.btnBack);
        btnNext = (ImageButton) view.findViewById(R.id.btnNext);
        mNameText = (TextView) view.findViewById(R.id.txtview_name_movie);
        mYearText = (TextView) view.findViewById(R.id.txtview_year_movie);
        mImdbText = (TextView) view.findViewById(R.id.txtview_imdb_movie);
        mPosterImg = (NetworkImageView) view.findViewById(R.id.imgview_poster);

        mPosterImg.addOnLayoutChangeListener(this);
        mPosterImg.setDefaultImageResId(R.drawable.poster);
        mPosterImg.setErrorImageResId(R.drawable.poster);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // change font for texts
        AssetManager assetManager = getActivity().getAssets();
        mNameText.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));
        mYearText.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));
        mImdbText.setTypeface(Typeface.createFromAsset(assetManager, "fonts/HelveticaNeueLTStd-Md.otf"));

        //setAdapter(mCategoryPosition, mSubcategoryPosition);
        setAdapter(0);
    }

    private void setAdapter(int subcatePosition) {
        if (mContext != null) {
            setMovieInfo(mCurrentMovie);
            setAdapterSubCategory();
            loadingData(categories.get(subcatePosition));
            setLayoutPositionAtFirst(subcatePosition);

        }
    }

    private void setLayoutPositionAtFirst(int subcatePosition) {
//        RelativeLayout layoutCate = (RelativeLayout) mRowCategory.getChildAt(0);
//        layoutCate.getLayoutParams().width = dpToPx(150);
//        layoutCate.requestLayout();
//        mRowCategory.getChildAt(0).setSelected(true);

        RelativeLayout layout = (RelativeLayout) mRowCategory.getChildAt(subcatePosition);
        layout.getLayoutParams().width = dpToPx(150);
        layout.requestLayout();
        mRowCategory.getChildAt(subcatePosition).setSelected(true);
    }

    private int dpToPx(int dp) {
        return (int) (dp * (getResources().getDisplayMetrics().densityDpi / 160f));
    }

    private void setAdapterSubCategory() {
        //get subcate by cate
        if (mContext != null) {
            AdapterPlayerSubcategory subcategory = new AdapterPlayerSubcategory(
                    mContext, R.layout.adapter_player_list_category, categories, this);
            mScrollSubCategory.setAdapter(getActivity(), subcategory);
        }

    }

    private void setAdapterSubcategoryTemp() {
        if (mContext != null) {
            List<MovieGenre> temp = new ArrayList<>();
            AdapterPlayerSubcategory subcategory = new AdapterPlayerSubcategory(
                    mContext, R.layout.adapter_player_list_category, temp, this);
            mScrollSubCategory.setAdapter(mContext, subcategory);
        }
    }

    private void setAdapterMovie(int position) {
        if (mContext != null) {
            adapterPlayerMovie = new AdapterPlayerMovie(
                    mContext, R.layout.adapter_player_gridmovie, mMovies, this);
            mScrollMovie.setAdapter(mContext, adapterPlayerMovie);
            if (mRowMovie.getChildAt(position) != null)
                mRowMovie.getChildAt(position).requestFocus();
        }
    }

    private void setMovieInfo(Movie movie) {
        mPosterImg.setImageUrl(movie.getMovie_thumb(), mImageLoader);
        Spannable nameSpan = new SpannableString(movie.getMovie_name());
        nameSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, nameSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mNameText.setText(nameSpan);

        Spannable yearSpan = new SpannableString("YEAR: " + movie.getMovie_year());
        yearSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 0, 210, 255)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mYearText.setText(yearSpan);

        Spannable imdbSpan = new SpannableString("IMDB: " + movie.getMovie_rating());
        imdbSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 238, 177, 30)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mImdbText.setText(imdbSpan);

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        loadMovieCallback = (LoadMovieCallback) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int posterHeight = mPosterImg.getMeasuredHeight();
        ViewGroup.LayoutParams layoutParams = mPosterImg.getLayoutParams();
        layoutParams.width = (int) (0.65f * posterHeight);
        mPosterImg.setLayoutParams(layoutParams);
    }

    private void setVisibleBackNext(int position) {
        if (position >= mMovies.size() - ITEM_PER_PAGE) {
            btnNext.setVisibility(View.INVISIBLE);
        } else if (position < ITEM_PER_PAGE)
            btnBack.setVisibility(View.INVISIBLE);
        else {
            btnNext.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.VISIBLE);
        }
    }

    private void loadMoreMovie(int position) {
        if (position == mMovies.size() - 1) {
            if (currentPageNumber < totalPages) {
                currentPageNumber++;
                updateData(currentPageNumber);
            }
        }
    }

    int oldCatePos = 0;

    @Override
    public void onClickItemSubcate(int catePosition) {
        currentPageNumber = 1;
        mMoviePosition = 0;
        loadingData(categories.get(catePosition));
        if (oldCatePos == 0)
            mRowCategory.getChildAt(mSubcategoryPosition).setSelected(false);
        else
            mRowCategory.getChildAt(oldCatePos).setSelected(false);
        mRowCategory.getChildAt(catePosition).setSelected(true);
        oldCatePos = catePosition;
    }


    @Override
    public void onNextPage(int position) {
        mScrollSubCategory.nextPage(position);
    }

    @Override
    public void onPrevPage(int position) {
        mScrollSubCategory.setItemPerPage(ITEM_PER_PAGE);
        mScrollSubCategory.prevPageV1(position);
    }

    int oldSubCatePos = 0;

    @Override
    public void OnPressLanguage(int catePosition, int langId) {
        //   langCurrent = langId;
//        if (langId == ENGLISH_ID) {
//            setAdapterSubCategory();
//            loadingData(categories.get(0)); // 0 is default
//        }else{
//            loadMovieByLanguage(langId);
//            setAdapterSubcategoryTemp();
//        }
//        if(oldSubCatePos == 0 )
//            mRowCategory.getChildAt(mCategoryPosition).setSelected(false);
//        else
//            mRowCategory.getChildAt(oldSubCatePos).setSelected(false);
//
//        mRowCategory.getChildAt(catePosition).setSelected(true);
        oldSubCatePos = catePosition;
    }


    @Override
    public void onFocusMovie(int position, boolean hasfocus) {
        setVisibleBackNext(position);
        setMovieInfo(mMovies.get(position));
        loadMoreMovie(position);
        //if(langCurrent == ENGLISH_ID)
    }

    @Override
    public void onClickMovie(int position) {
        loadMovieCallback.PlayMovie(mMovies.get(position));
        setMovieInfo(mMovies.get(position));
    }

    @Override
    public void onNextPageMovie(int position) {
        mScrollMovie.nextPage(position);
    }

    @Override
    public void onPrevMovie(int position) {

        mScrollMovie.setItemPerPage(ITEM_PER_PAGE);
        mScrollMovie.prevPageV1(position);
    }

    // loading data
    private void loadingData(MovieGenre mCurrentGenre) {
        if (Utils.isNetworkConnected(getActivity())) {
            MoviesManagement.getInstance().moviesOfPageByGenre(
                    getActivity(), mCurrentGenre.getId() + "",
                    currentPageNumber,
                    new MovieOfPageByGenreCallback() {

                        @Override
                        public void onFailed(Exception exception) {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onCompleted(SparseArray<Object> object) {
                            // TODO Auto-generated method stub
                            if (object != null && object.size() >= 2) {
                                if (mMovies != null)
                                    mMovies.clear();
                                else
                                    mMovies = new ArrayList<Movie>();
                                mMovies.addAll((ArrayList<Movie>) object.get(1));
                                moviesManagement.setmMovies(mMovies);
                                totalPages = (Integer) object.get(2);
                                totalResults = (Integer) object.get(3);
                                setAdapterMovie(mMoviePosition);

                            }
                        }
                    });

        } else {
            Toast.makeText(getActivity(), "No connection network",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void updateData(int currentPage) {
        if (Utils.isNetworkConnected(getActivity())) {
            MoviesManagement.getInstance().moviesOfPageByGenre(
                    getActivity(), categories.get(mSubcategoryPosition).getName().toLowerCase(),
                    currentPage,
                    new MovieOfPageByGenreCallback() {

                        @Override
                        public void onFailed(Exception exception) {
                        }

                        @SuppressWarnings("unchecked")
                        @Override
                        public void onCompleted(SparseArray<Object> object) {
                            if (object != null && object.size() >= 2) {
                                mMovies.addAll((ArrayList<Movie>) object.get(1));
                                adapterPlayerMovie.notifyDataSetChanged();
                                mScrollMovie.AddViewWithAdapter(adapterPlayerMovie);
                            }

                        }
                    });
        } else {
            Toast.makeText(getActivity(), "no connection network",
                    Toast.LENGTH_SHORT).show();
        }

    }
}
