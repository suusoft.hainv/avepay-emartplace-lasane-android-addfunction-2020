package com.aveplus.avepay_cpocket.music.suuplayer;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.aveplus.avepay_cpocket.music.util.AppUtil;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class SuuPlayerImpl implements SuuPlayer, Player.EventListener, SuuPlayerView.SuuPlayerFullScreenListener {

    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    public SimpleExoPlayer player;
    private Context context;
    private SuuPlayerView playerView;
    private boolean shouldAutoPlay;
    private boolean isStartedPlay;

    private SuuPlayer.EventListener listener;

    public SuuPlayerImpl(Context context, SuuPlayerView playerView) {
        this.context = context;
        this.playerView = playerView;
        shouldAutoPlay = true;
        initializePlayer();
        initPlayerView();
    }

    public SuuPlayerImpl(Context context) {
        this.context = context;
        initializePlayer();
    }


    @Override
    public void initializePlayer() {
        player = ExoPlayerFactory.newSimpleInstance(
                context,
                new DefaultTrackSelector(), new DefaultLoadControl());
        player.setPlayWhenReady(true);
        player.addListener(this);


    }

    private void initPlayerView() {
        playerView.setPlayer(player);
        playerView.setFullScreenListener(this);
    }

    @Override
    public void prepare(String urlStream, int type) {
        isStartedPlay = true;
        MediaSource mediaSource = buildMediaSource(Uri.parse(urlStream), type);
        player.prepare(mediaSource, true, false);


    }

    @Override
    public void setShouldAutoPlay(boolean shouldAutoPlay) {
        this.shouldAutoPlay = shouldAutoPlay;
    }

    @Override
    public boolean isPlaying() {
        return player.getPlayWhenReady();
    }

    @Override
    public void togglePlay() {
        if (player.getPlayWhenReady()) {
            player.setPlayWhenReady(false);
        } else {
            player.setPlayWhenReady(true);
        }
    }

    @Override
    public void play() {
        player.setPlayWhenReady(true);
    }

    @Override
    public void pause() {
        player.setPlayWhenReady(false);
    }

    @Override
    public long getDuration() {
        if (player == null) {
            return 0;
        } else {
            return player.getDuration();
        }
    }

    @Override
    public long getCurrentPosition() {
        if (player == null) {
            return 0;
        } else {
            return player.getCurrentPosition();
        }
    }

    @Override
    public int getState() {
        return player.getPlaybackState();
    }

    @Override
    public void seekTo(long positionMs) {
        player.seekTo(positionMs);
    }


    @Override
    public void releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player.getPlayWhenReady();
            player.release();
            player = null;
            playerView = null;
        }
    }

    @Override
    public void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @Override
    public void setFullScreen(boolean isFullScreen) {
        playerView.setFullScreen(isFullScreen);
    }

    @Override
    public void addEventListener(SuuPlayer.EventListener listener) {
        this.listener = listener;
    }

    private MediaSource buildMediaSource(Uri uri, int type) {
        switch (type) {
            case TYPE_SS:
                return new SsMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultSsChunkSource.Factory(buildDataSourceFactory(true)), null, null);
            case TYPE_DASH:
                return new DashMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultDashChunkSource.Factory(buildDataSourceFactory(true)), null, null);
            case TYPE_HLS:
                return new HlsMediaSource(uri, buildDataSourceFactory(true), null, null);
            case TYPE_OTHER:
                return new ExtractorMediaSource(uri, buildDataSourceFactory(true), new DefaultExtractorsFactory(),
                        null, null);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }

    /**
     * Returns a new DataSource factory.
     *
     * @param useBandwidthMeter Whether to set {@link #BANDWIDTH_METER} as a listener to the new
     *                          DataSource factory.
     * @return A new DataSource factory.
     */
    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        return new DefaultDataSourceFactory(context, useBandwidthMeter ? BANDWIDTH_METER : null,
                new DefaultHttpDataSourceFactory(Util.getUserAgent(context, "SUU"), BANDWIDTH_METER));
    }


    public void onTimelineChanged(Timeline timeline, Object manifest) {
        Log.e("onTimelineChanged", "value: " + timeline.getPeriodCount());
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        Log.e("onTracksChanged", "");
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        Log.e("onLoadingChanged", "" + isLoading);

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (listener != null) {
            if (playbackState == Player.STATE_READY) {
                if (playWhenReady) {
                    if (isStartedPlay) {
                        listener.onStared();
                        isStartedPlay = false;
                    }
                    listener.onPlaying();
                } else {
                    listener.onPaused();
                }
            } else if (playbackState == Player.STATE_ENDED) {
                listener.onCompletion();
            } else if (playbackState == Player.STATE_BUFFERING) {
                listener.onBuffering(playWhenReady);
            } else if (playbackState == Player.STATE_IDLE) {
                listener.onPaused();
            }

        }

        Log.d("onPlayerStateChanged", "" + playWhenReady);

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {
        if (listener != null)
            listener.onRepeatModeChanged(repeatMode);
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        if (listener != null)
            listener.onPlayerError(error);
        AppUtil.showToast(context, error.getMessage());
        Log.e("onPlayerError", "" + error.getMessage());

    }

    public void onPositionDiscontinuity() {
        Log.d("onPositionDiscontinuity", "");

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    @Override
    public void onFullScreen(boolean isFull) {
        if (listener != null)
            listener.onFullScreen(isFull);
    }
}
