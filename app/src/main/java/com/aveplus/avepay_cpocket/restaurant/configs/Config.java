package com.aveplus.avepay_cpocket.restaurant.configs;

import android.os.Environment;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

import java.io.File;

/**
 * Created by phamv on 7/22/2017.
 */

public class Config {
    public static final String APP_DOMAIN = "https://www.googleapis.com/youtube/v3/";
    public static final String URL_API = AppController.getInstance().getString(R.string.URL_API_RESTAURANT) + "api/";
    public static final String URL_API_SEARCH = AppController.getInstance().getString(R.string.URL_API_RESTAURANT_PROJECT);
    public static final int MAX_RESULT = 10;


    public static class Api {
        // Domain
        public static final String PLAYLIST = Config.APP_DOMAIN + "playlists";
        public static final String PLAYLIST_ITEMS = Config.APP_DOMAIN + "playlistItems";
        public static final String VIDEO = Config.APP_DOMAIN + "videos";
        public static final String CHANNELS = Config.APP_DOMAIN + "channels";


        public static final String HOME = URL_API+ "home/statistics";
        public static final String ITEM_BY_CATEGORY = URL_API+ "music-song/get-song-by-category";
        public static final String ITEM_BY_PLAYLIST = URL_API+ "music-song/get-song-by-playlist";
        public static final String ALL_CATEGORIES = URL_API+ "category/list";
        public static final String ALL_ALBUM_BY= URL_API+ "music-playlist/search";
        public static final String SEARCH = URL_API_SEARCH+ "restaurant/list-product/bykeyword";
        public static final String RESTAURANT = URL_API_SEARCH+ "restaurant/restaurant/recent";
        public static final String RESTAURANT_NEARBY = URL_API_SEARCH+ "restaurant/restaurant/nearby";

        // Urls
        public static final String BOOK = "book";
        public static final String URL_REGISTER_DEVICE = Config.APP_DOMAIN + "device";
        public static final String URL_LOGIN = Config.APP_DOMAIN + "login";
        public static final String URL_LOGOUT = Config.APP_DOMAIN + "logout";
        public static final String URL_REGISTER = Config.APP_DOMAIN + "register";
        public static final String URL_RESET_PASSWORD = Config.APP_DOMAIN + "resetPassword";
        public static final String URL_GET_ALL = Config.APP_DOMAIN + "browseTask";

        //temp
        public static final String API_BROWTASK = "browseTask";
        public static final String URL_TASK_DETAIL = Config.APP_DOMAIN + "taskDetail";

    }

    public static String getPathDir() {
        File file = Environment.getExternalStorageDirectory();
        return file.getPath() + "/" + getNameDownloadFolder();

    }

    public static String getExternalFolder() {
        File file = Environment.getExternalStorageDirectory();
        return file.getPath();

    }

    public static String getNameDownloadFolder(){
        return  "suusoft.com/downloads";
    }

}
