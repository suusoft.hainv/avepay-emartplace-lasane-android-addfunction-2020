package com.aveplus.avepay_cpocket.taxi.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.retrofit.base.BaseRequestApi;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.google.android.material.snackbar.Snackbar;

/**
 * Created by SuuSoft.com on 7/10/2016.
 */
public abstract class AbstractActivity extends AppCompatActivity implements BaseRequest.ListenerLoading , BaseRequestApi.ListenerLoading {

    private View progressBar;

    public enum ToolbarType{
        MENU_LEFT, // get menu left
        NAVI,    // get button back
        NORMAL, // only toolbar
        NONE   // none
    }

    protected Activity self;

    protected FrameLayout contentLayout;
    // toolbar
    protected Toolbar toolbar;
    protected TextView tvTitle;

    protected abstract ToolbarType getToolbarType();
    protected abstract void getExtraData(Intent intent);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self =  this;
        getExtraData(getIntent());
        setView();
        initViewBase();
        regisNetworkListener();
    }

    private void setView() {
        if (getToolbarType() == ToolbarType.MENU_LEFT) {
            setContentView(R.layout._base_drawer_taxi);
            initToolbar();
        }else if (getToolbarType() == ToolbarType.NAVI){
            setContentView(R.layout._base_frame);
            initToolbar();
            initToolBarNav();
        }else if (getToolbarType() == ToolbarType.NORMAL){
            setContentView(R.layout._base_frame);
            initToolbar();
        }else if (getToolbarType() == ToolbarType.NONE){
            setContentView(R.layout._base_content);
        }
    }

    private void initViewBase(){
        contentLayout = (FrameLayout) findViewById(R.id.content_main);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        BaseRequest.getInstance().setListenerLoading(this);
        // BaseRequestApi.getInstance().setListenerLoading(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadCastNetwork);

    }
    /**
     * initialize toolbar
     *
     */
    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

    }

    protected void initToolBarNav(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public Toolbar getToolbar(){
        return toolbar;
    }

    /**
     * set title for toolbar
     *
     */
    public void setToolbarTitle(String title){
        tvTitle.setText(title);
    }

    public void setToolbarTitle(int title){
        tvTitle.setText(getString(title));
    }

    /**
     * start activity
     * @param clz class name
     *
     */
    public void startActivity(Class<?> clz){
        AppUtil.startActivity(self, clz);
    }

    public void startActivity(Class<?> clz, Bundle bundle){
        AppUtil.startActivity(self, clz, bundle);
    }

    /**
     * show toast message
     * @param message
     *
     */
    public void showToast(String message){
        AppUtil.showToast(self,message);
    }

    public void showToast(int message){
        AppUtil.showToast(self,getString(message));
    }

    /**
     * show snack bar message
     * @param message
     *
     */
    public void showSnackBar(int message){
        Snackbar.make(contentLayout, getString(message), Snackbar.LENGTH_LONG).show();
    }
    public void showSnackBar(String message){
        Snackbar.make(contentLayout, message, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Listener network state
     *
     */
    private BroadcastReceiver broadCastNetwork = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("broadCastNetwork","changed");
        }
    };

    private void regisNetworkListener() {
        registerReceiver(broadCastNetwork, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    public void onLoadingIsCompleted() {
        showProgress(false);
    }

    @Override
    public void onLoadingIsProcessing() {
        showProgress(true);
    }

    /**
     * show hide progress bar on screen
     * @param isShow true is show
     */
    public void showProgress(boolean isShow){
        if (progressBar!=null)
        if (isShow) {
                progressBar.setVisibility(View.VISIBLE);
        }else{
                progressBar.setVisibility(View.GONE);
        }
    }


}
