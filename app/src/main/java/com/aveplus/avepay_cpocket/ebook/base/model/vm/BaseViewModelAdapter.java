package com.aveplus.avepay_cpocket.ebook.base.model.vm;

import android.content.Context;

import androidx.databinding.BaseObservable;

/**
 * Created by phamv on 7/18/2017.
 */
public abstract class BaseViewModelAdapter extends BaseObservable {

    public Context self;

    public abstract void setData(Object object);

    public BaseViewModelAdapter(Context self) {
        this.self = self;
    }
}
