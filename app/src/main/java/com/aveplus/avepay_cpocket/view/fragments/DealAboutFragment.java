package com.aveplus.avepay_cpocket.view.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.aveplus.avepay_cpocket.view.activities.ChatActivityReskin2;
import com.quickblox.users.model.QBUser;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.view.activities.DealDetailActivity;
import com.aveplus.avepay_cpocket.view.activities.FindUsOnMapActivity;
import com.aveplus.avepay_cpocket.view.activities.RecentChatsActivity;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.Constants;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.objects.DealCateObj;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.objects.RecentChatObj;
import com.aveplus.avepay_cpocket.quickblox.SharedPreferencesUtil;
import com.aveplus.avepay_cpocket.utils.DateTimeUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.aveplus.avepay_cpocket.utils.StringUtil;
import com.aveplus.avepay_cpocket.utils.ViewUtil;

import org.json.JSONObject;

/**
 * Created by Suusoft on 11/24/2016.
 */

public class DealAboutFragment extends com.aveplus.avepay_cpocket.base.BaseFragment implements View.OnClickListener {

    private static final int RC_ACTIVATE_DEAL = 1;
    private static final String TAG = DealAboutFragment.class.getSimpleName();

    private ImageView imgDeal, imgFavorite;
    private TextView tvAddress, tvFavoriteCount, tvPrice, tvOldPrice, tvName, btnDeal, tvAbout, tvFileName, tvNumReviews, tvEndTime;
    private RatingBar rbDeal;
    private DealObj item;
    private LinearLayout llParentTime;

    public static DealAboutFragment newInstance(DealObj item) {
        Bundle args = new Bundle();
        DealAboutFragment fragment = new DealAboutFragment();
        fragment.item = item;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_deal_about;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        imgDeal = (ImageView) view.findViewById(R.id.img_deal);
        imgFavorite = (ImageView) view.findViewById(R.id.img_favorite);
        tvAddress = (TextView) view.findViewById(R.id.edt_address);
        tvPrice = (TextView) view.findViewById(R.id.lbl_price);
        tvOldPrice = (TextView) view.findViewById(R.id.lbl_price_old);
        tvName = (TextView) view.findViewById(R.id.lbl_deal_name);
        btnDeal = (TextView) view.findViewById(R.id.tv_deal);
        tvAbout = (TextView) view.findViewById(R.id.tv_about);
        tvFavoriteCount = (TextView) view.findViewById(R.id.lbl_favorite_quantity);
        rbDeal = (RatingBar) view.findViewById(R.id.rating);
        tvFileName = (TextView) view.findViewById(R.id.tv_file_name);
        tvNumReviews = (TextView) view.findViewById(R.id.lbl_rate_quantity);
        tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);
        llParentTime = (LinearLayout) view.findViewById(R.id.ll_parent_end_time);

        ViewUtil.setTextLineCenter(tvOldPrice);
        ViewUtil.setRatingbarColor(rbDeal);

        imgFavorite.setOnClickListener(this);
        btnDeal.setOnClickListener(this);
        tvAddress.setOnClickListener(this);
        tvFileName.setOnClickListener(this);

        setFavourite();

        // Set text for 'iwanadeal' button
        if (item.getSeller_id().equals(DataStoreManager.getUser().getId())) {
            btnDeal.setText(R.string.iwanachat);
        } else {
            btnDeal.setText(R.string.deal);
        }

    }

    @Override
    protected void getData() {
        setData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_ACTIVATE_DEAL) {
            if (resultCode == Activity.RESULT_OK) {
                // Update deal status(active or inactive) after coming back from Chat screen
                if (item != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        if (bundle.containsKey(Args.IS_ACTIVATED_DEAL)) {
                            item.setIs_online(bundle.getBoolean(Args.IS_ACTIVATED_DEAL) ? DealObj.DEAL_ACTIVE : DealObj.DEAL_INACTIVE);

                            // Refresh deal object in parent activity
                            ((DealDetailActivity) getActivity()).getItem().setIs_online(bundle.getBoolean(Args.IS_ACTIVATED_DEAL) ? DealObj.DEAL_ACTIVE : DealObj.DEAL_INACTIVE);
                            ((DealDetailActivity) getActivity()).updateOptionsMenu();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnDeal) {
            gotoChat();
        } else if (view == tvAddress) {
            gotoMap();
        } else if (view == imgFavorite) {
            favorite();
        } else if (view == tvFileName) {
            showFile();
        }
    }

    private void gotoChat() {
        item.setBuyerId(DataStoreManager.getUser().getId());
        item.setBuyerName(DataStoreManager.getUser().getName());

        if (item.getSeller_id().equals(DataStoreManager.getUser().getId())) {
            RecentChatObj recentChatObj = new RecentChatObj(null, item, null);
            gotoChatForResult(recentChatObj);
        } else {
            /*QBUser qbUser = new QBUser();
            qbUser.setId(item.getSellerQbId());
            qbUser.setFullName(item.getProData().getname());
            String phone = DataStoreManager.getUser().getPhone() == null ? "" : DataStoreManager.getUser().getPhone();
            qbUser.setPhone(phone);*/

            String phone = DataStoreManager.getUser().getPhone() == null ? "" : DataStoreManager.getUser().getPhone();
            QBUser qbUser = SharedPreferencesUtil.getQbUser();
            qbUser.setPhone(phone);

            RecentChatObj recentChatObj = new RecentChatObj(null, item, qbUser);
            RecentChatsActivity.start(getActivity(), recentChatObj);
        }
    }

    private void gotoChatForResult(RecentChatObj obj) {
        Log.e(TAG, "gotoChatForResult mRecentChatObj " + new Gson().toJson(obj));
        Intent intent = new Intent(self, ChatActivityReskin2.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(Args.RECENT_CHAT_OBJ, obj);
        startActivityForResult(intent, RC_ACTIVATE_DEAL);
    }

    private void setFavourite() {
        if (item!=null){
            if (item.isFavorite()) {
                imgFavorite.setImageResource(R.drawable.ic_like_active);
            } else {
                imgFavorite.setImageResource(R.drawable.ic_like);
            }
            tvFavoriteCount.setText(item.getFavoriteQuantity() + "");
        }

    }

    private void favorite() {
        ModelManager.favorite(self, item.getId(), ModelManager.FAVORITE_TYPE_DEAL, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                ApiResponse response = new ApiResponse(jsonObject);
                if (!response.isError()) {
                    item.setFavorite(!item.isFavorite());
                    if (item.isFavorite()) {
                        item.setFavoriteQuantity(item.getFavoriteQuantity() + 1);
                        Toast.makeText(self, getString(R.string.favorited), Toast.LENGTH_LONG).show();
                    } else {
                        item.setFavoriteQuantity(item.getFavoriteQuantity() - 1);
                        Toast.makeText(self, getString(R.string.unfavorited), Toast.LENGTH_LONG).show();
                    }

                    setFavourite();
//                    sendBroastcast();
                } else {
                    Toast.makeText(self, response.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    private void sendBroastcast() {
        Intent intent = new Intent(Args.NOTIFY_NEED_UPDATE);
        intent.putExtra(Args.POSITION, item.getPositionInList());
        intent.putExtra(Args.IS_FAVORITE, item.isFavorite());

        self.sendBroadcast(intent);
    }

    private void gotoMap() {
        FindUsOnMapActivity.start(getActivity(), item, null);
    }

    public void setData() {
        if (item != null) {
            ImageUtil.setImage(self, imgDeal, item.getImageUrl());
            tvName.setText(item.getName());
            if (item.getDiscount_type() == null || item.getCategory_id() == Integer.parseInt(DealCateObj.LABOR)||item.getDiscount_type() != null && item.getDiscount_type().isEmpty()) {
                tvPrice.setText(String.format(getString(R.string.dollar_value),
                        StringUtil.convertNumberToString(item.getPrice(), 1)));
                tvOldPrice.setVisibility(View.GONE);
            } else if (item.getDiscount_type() != null && item.getDiscount_type().equals(Constants.AMOUNT)) {

                if (item.getDiscount_price() != 0) {
                    tvPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice() - item.getDiscount_price(), 1)));
                    tvOldPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice(), 1)));
                    tvOldPrice.setVisibility(View.VISIBLE);
                } else {
                    tvPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice(), 1)));
                    tvOldPrice.setVisibility(View.GONE);

                }
            } else if (item.getDiscount_type() != null && item.getDiscount_type().equals(Constants.PERCENT)) {
                if (item.getDiscount_rate() > 0) {
                    tvPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getSale_price(), 1)));
                    tvOldPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice(), 1)));
                    tvOldPrice.setVisibility(View.VISIBLE);
                } else {
                    tvPrice.setText(String.format(getString(R.string.dollar_value),
                            StringUtil.convertNumberToString(item.getPrice(), 1)));
                    tvOldPrice.setVisibility(View.GONE);
                }
            }

            tvAbout.setText(item.getDescription());
            tvFavoriteCount.setText(item.getFavoriteQuantity() + "");
            tvAddress.setText(item.getAddress());
            rbDeal.setRating(item.getRate());
            if (item.getRateQuantity() > 0) {
                tvNumReviews.setVisibility(View.VISIBLE);
                tvNumReviews.setText(String.valueOf(item.getRateQuantity()));
            } else {
                tvNumReviews.setVisibility(View.INVISIBLE);
            }

            int favRes = item.isFavorite() ? R.drawable.ic_like_active : R.drawable.ic_like;
            imgFavorite.setImageResource(favRes);
            if (item.isOnline() && item.getSeller_id().equals(DataStoreManager.getUser().getId())) {
                llParentTime.setVisibility(View.VISIBLE);
                long time = Long.parseLong(item.getOnline_started()) + (item.getOnline_duration() * 3600);
                String datetime = DateTimeUtil.convertTimeStampToDate(time, "HH:mm, EEE dd-MM-yyyy");
                tvEndTime.setText(datetime);
            } else if (!item.isOnline() && item.getSeller_id().equals(DataStoreManager.getUser().getId())) {
                llParentTime.setVisibility(View.VISIBLE);
                tvEndTime.setText(getString(R.string.msg_expired));
            }
            if (!item.getSeller_id().equals(DataStoreManager.getUser().getId()) || item.isHideEndTime()) {
                llParentTime.setVisibility(View.GONE);
            }

        }
    }

    private void showFile() {
        Dialog builder = new Dialog(self);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView imageView = new ImageView(self);
        ImageUtil.setImage(self, imageView, item.getAttachment());
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }

}
