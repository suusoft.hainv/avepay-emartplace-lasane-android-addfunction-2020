package com.aveplus.avepay_cpocket.restaurant.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.listener.IConfirmation;



/**
 * Copyright © 2020 SUUSOFT
 */


public class PermissionUtil {


    public static void showConfirmationDialog(Context context, String msg, String positive, String negative,
                                              boolean isCancelable, final IConfirmation iConfirmation) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_confirmation);

        TextView lblMsg = dialog.findViewById(R.id.lbl_msg);
        TextView lblNegative = dialog.findViewById(R.id.lbl_negative);
        TextView lblPositive = dialog.findViewById(R.id.lbl_positive);

        lblMsg.setText(msg);
        lblNegative.setText(negative);
        lblPositive.setText(positive);

        lblNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                iConfirmation.onNegative();
            }
        });

        lblPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                iConfirmation.onPositive();
            }
        });

        dialog.setCancelable(isCancelable);

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public static final int RC_PERMISSIONS = 1;

    /**
     * Check permissions in runtime
     *
     * @param activity
     * @param permissions
     * @param reqCode
     * @param notification
     * @return
     */
    public static boolean isGranted(Activity activity, String[] permissions, int reqCode, String notification) {
        boolean granted = true;

        if (isMarshmallow()) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];

                granted = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
                if (!granted) {
                    if (notification != null && notification.length() > 0) {
                        Toast.makeText(activity, notification, Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

            // Ask permissions
            if (!granted) {
                ActivityCompat.requestPermissions(activity, permissions, reqCode);
            }
        }

        return granted;
    }


    public static boolean isMarshmallow() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M;
    }
}
