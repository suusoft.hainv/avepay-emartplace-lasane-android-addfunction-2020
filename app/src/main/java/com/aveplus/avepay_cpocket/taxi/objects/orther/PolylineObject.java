package com.aveplus.avepay_cpocket.taxi.objects.orther;

public class PolylineObject {

    private String points;

    public PolylineObject(String points) {
        this.points = points;
    }


    public String getPoints() {
        return points;
    }
}
