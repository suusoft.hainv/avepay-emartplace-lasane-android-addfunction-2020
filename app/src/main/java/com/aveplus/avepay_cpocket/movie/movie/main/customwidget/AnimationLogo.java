package com.aveplus.avepay_cpocket.movie.movie.main.customwidget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.aveplus.avepay_cpocket.R;


/**
 * Copyright © 2019 SUUSOFT
 */
public class AnimationLogo extends FrameLayout {

    private ImageView mLogoPart1ImgView, mLogoPart2ImgView;
    private Animation mFlipAnimation;

    public AnimationLogo(Context context) {
        super(context);
    }

    public AnimationLogo(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimationLogo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AnimationLogo(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void initView() {
        mLogoPart1ImgView = (ImageView) this.findViewById(R.id.imgview_logo_part1);
        mLogoPart2ImgView = (ImageView) this.findViewById(R.id.imgview_logo_part2);
        mFlipAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.flip_left_right);
        mLogoPart2ImgView.setAnimation(mFlipAnimation);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initView();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        startAnimation();
    }

    private void startAnimation() {
        mFlipAnimation.start();
    }

//    private void stopAnimation() {
//
//    }
}
