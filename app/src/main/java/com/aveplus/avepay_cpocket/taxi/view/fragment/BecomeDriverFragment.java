package com.aveplus.avepay_cpocket.taxi.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseFragment;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.view.activities.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class BecomeDriverFragment extends BaseFragment {


    public static BecomeDriverFragment newInstance() {

        Bundle args = new Bundle();

        BecomeDriverFragment fragment = new BecomeDriverFragment();
        fragment.setArguments(args);
        return fragment;
    }


    private static final String TAG = BecomeDriverFragment.class.getSimpleName();
    private CheckBox ckbBecomePro;
    private EditText edtBusinessName, edtEmail, edtPhone, edtAddress;
    private TextView btnBecomePro, tvNotif;
    private UserObj userObj;


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_become_driver;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        ckbBecomePro = view.findViewById(R.id.ckb_become_pro);
        edtBusinessName = view.findViewById(R.id.edt_bussiness_name);
        edtPhone = view.findViewById(R.id.edt_phone_number);
        edtAddress = view.findViewById(R.id.edt_address);

        btnBecomePro = view.findViewById(R.id.btn_become_pro);
        tvNotif = view.findViewById(R.id.tv_notif);


        btnBecomePro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValid()) {
                    onRequest();
                }

            }
        });

        getProfile();

    }


    private void setData() {
        userObj = DataStoreManager.getUser();
        if (userObj != null) {
            edtBusinessName.setText(userObj.getName());
            edtAddress.setText(userObj.getAddress());
            edtPhone.setText(userObj.getPhone());


            if (userObj.getProData() != null) {
                setEnable(false);
                tvNotif.setVisibility(View.VISIBLE);
                btnBecomePro.setVisibility(View.GONE);

            } else {
                setEnable(true);
                tvNotif.setVisibility(View.GONE);
                btnBecomePro.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setEnable(boolean isEnable) {
        edtAddress.setEnabled(isEnable);
        edtBusinessName.setEnabled(isEnable);
        ckbBecomePro.setEnabled(isEnable);
    }

    private String bussinessName = "", phoneNumber = "", address = "";

    private boolean isValid() {
        bussinessName = edtBusinessName.getText().toString().trim();
        phoneNumber = edtPhone.getText().toString().trim();
        address = edtAddress.getText().toString().trim();

        if (StringUtil.isEmpty(bussinessName)) {
            AppUtil.showToast(self, R.string.msg_name_is_required);
            return false;
        }

        if (StringUtil.isEmpty(phoneNumber)) {
            AppUtil.showToast(self, R.string.msg_phone_is_required);
            return false;
        }

        if (StringUtil.isEmpty(address)) {
            AppUtil.showToast(self, R.string.msg_address_is_required);
            return false;
        }

        return true;
    }

    @Override
    protected void getData() {

    }


    private void onRequest() {

        ModelManager.requestBecomeDriver(self, bussinessName, address, phoneNumber, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                try {
                    JSONObject jsonObject = new JSONObject(object.toString());
                    ApiResponse response = new ApiResponse(jsonObject);
                    if (!response.isError()) {
                        UserObj userObj = response.getDataObject(UserObj.class);
                        userObj.setToken(DataStoreManager.getTokenUser());
                        userObj.setRememberMe(DataStoreManager.getUser().isRememberMe());
                        DataStoreManager.saveUser(userObj);
                        setData();
                        AppUtil.showToast(self, R.string.submit_successfully);
                    } else {
                        Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError() {
                Log.e(TAG, "ERROR: update profile pro!");

            }
        });

    }


    private void getProfile() {
        ((MainActivity) self).showProgress(true);
        RequestManager.getProfile(self, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(com.aveplus.avepay_cpocket.taxi.network.ApiResponse response) {
                ((MainActivity) self).showProgress(false);
                UserObj userObj = response.getDataObject(UserObj.class);
                userObj.setToken(DataStoreManager.getTokenUser());
                DataStoreManager.saveUser(userObj);
                setData();

            }

            @Override
            public void onError(String message) {
                ((MainActivity) self).showProgress(false);
            }
        });
    }
}
