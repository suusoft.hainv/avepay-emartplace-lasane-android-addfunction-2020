package com.aveplus.avepay_cpocket.taxi.view.instant;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.configs.Action;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;


public class CompleteAndRateActivity extends BaseActivity implements View.OnClickListener {

    public static final int RQ_RATING = 1002;
    public static final int RS_RATING_SUCCESS = 1003;

    private static final int RC_DEAL_COMPLETED = 999;
    public static final int RESULT_CANCEL_DEAL = 1000;

    private TextView tvPaidWithVisa, tvPrice, tvDistanceAndTime, lblPickup,
            tvPickupAddress, lblDropoff, tvDropoffAddress,
            tvInforDriver, btnCompleteAndRate;
    private RatingBar ratingBar;

    private TripObj data;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_arrived_and_rate;
    }

    @Override
    protected void getExtraData(Intent intent) {
        data = intent.getExtras().getParcelable(Args.KEY_TRIP_OJBECT);
        if (data==null)
            finish();
    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.complete);


//        tvPaidWithVisa = findViewById(R.id.tv_paid_with_visa);
        tvPrice= findViewById(R.id.tv_price);
        tvDistanceAndTime= findViewById(R.id.tv_info_distance_time);
        lblPickup= findViewById(R.id.lbl_pickup);

        tvPickupAddress= findViewById(R.id.tv_pick_up_address);
        lblDropoff= findViewById(R.id.lbl_dropoff);
        tvDropoffAddress= findViewById(R.id.tv_dropoff_address);

        tvInforDriver= findViewById(R.id.tv_info_driver);
        btnCompleteAndRate = findViewById(R.id.btn_rate_my_ride);
        ratingBar= findViewById(R.id.rating_bar);

        ((LayerDrawable) ratingBar.getProgressDrawable()).getDrawable(2)
                .setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);



        ratingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        btnCompleteAndRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestManager.instantTripAction(Action.ACTION_FINISH, data.getId(), "", new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        AppUtil.showToast(self, "Finish Trip " +  data.getId() );
                        DataStoreManager.saveTripOld(data.getId());
                        setResult(RS_RATING_SUCCESS);
                        finish();
                    }

                    @Override
                    public void onError(String message) {

                    }
                });
            }
        });



    }

    private void onRating() {

        setResult(RS_RATING_SUCCESS);
        finish();


    }

    @Override
    protected void onViewCreated() {
        bindData();
    }

    private void bindData() {
        tvDistanceAndTime.setText((double) (data.getEstimateDistance()/1000) + " Km" /*+ (double)(data.getEstimate_duration()) + " min"  */ );
        tvPrice.setText( StringUtil.convertNumberToPrice(data.getEstimateFare() ));
        //tvPrice.setText( String.format(getString(R.string.price_),data.getEstimateFare() + "" ) );
        tvPickupAddress.setText(MapsUtil.getAddressFromLatLng(self, data.getLatLngPickup()) );
        tvDropoffAddress.setText(MapsUtil.getAddressFromLatLng(self, data.getLatLngDestination()) );
        tvInforDriver.setText(data.getDriverName());
    }

    @Override
    public void onClick(View v) {

    }
}
