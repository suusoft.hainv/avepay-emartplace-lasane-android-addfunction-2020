package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;


import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.CategoryNewAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.NewEventAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.TabSilderAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.object.CategoryNewEvent;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshListView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class FragmentNewEvent extends BaseFragment {
    private EditText txtSearch;
    private ArrayList<NewEvent> arraylist = new ArrayList<NewEvent>();
    private ArrayList<CategoryNewEvent> arrCategories = new ArrayList<CategoryNewEvent>();
    private CategoryNewAdapter categoryAdapter;
    private NewEventAdapter adapter = null;
    private ListView listview;
    private PullToRefreshListView lGv;
    private Spinner spnCategories;
    private int currentPage, totalPage;
    private int categoryIndex = 0;
    private boolean isPullRefresh = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("newevent");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {

        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();
//        if (onTitleChanged != null)
//            onTitleChanged.onTitleChanged("home");
//        else
//            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_newevent, container,
                false);

        initView(view);

        return view;
    }

    public void initView(View view) {

        ViewPager pager = (ViewPager) view.findViewById(R.id.pagerSliderBlog);
        ArrayList<Fragment> list = new ArrayList<Fragment>();
        list.add(new FragmentBlogRecent());
        list.add(new FragmentBlogPopular());
        list.add(new FragmentBlogFeature());
        pager.setAdapter(new TabSilderAdapter(getActivity(), getChildFragmentManager(), list));
        TabLayout tabs = (TabLayout) view.findViewById(R.id.tab_layout);
        tabs.setupWithViewPager(pager);
    }


    protected void showToast(int idString) {
        Toast.makeText(getActivity(), idString, Toast.LENGTH_SHORT).show();
    }


}
