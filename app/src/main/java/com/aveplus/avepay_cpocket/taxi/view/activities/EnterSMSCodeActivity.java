package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.toolbox.Volley;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.configs.Config;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class EnterSMSCodeActivity extends BaseActivity {


    private final String TAG = EnterSMSCodeActivity.class.getSimpleName();
    public static final int RQ_SMS_CODE = 1000;

    private EditText edtSMSCode;
    private TextView btnOk, tvDescription, btnResendCode;
    private String phoneNumber, email, phoneCode, phoneFull;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_enter_sms_code;
    }

    @Override
    protected void getExtraData(Intent intent) {
        email = intent.getExtras().getString(Args.KEY_EMAIL);
        phoneNumber = intent.getExtras().getString(Args.KEY_NUMBER);
        phoneCode = intent.getExtras().getString(Args.KEY_PHONE_CODE);
        phoneFull = phoneCode + phoneNumber;


    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {
        setToolbarTitle(R.string.enter_sms_code);

        tvDescription = findViewById(R.id.tv_description);
        edtSMSCode = findViewById(R.id.edt_sms_code);
        btnOk = findViewById(R.id.btn_ok);
        btnResendCode = findViewById(R.id.btn_resend_code);

        tvDescription.setText(String.format(getString(R.string.we_ve_sent_you_an_sms_with_the_code_to), phoneNumber));


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Config.choPhepXacThucSdt) {
                    if (!edtSMSCode.getText().toString().equals("")) {
                        onSendSMSCode(edtSMSCode.getText().toString());

                    }
                } else {
                    verificationSuccess();
                }
            }
        });


        btnResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mResendToken == null) {
                    return;
                }
                resendVerificationCode(phoneFull, mResendToken);
            }
        });


        if (Config.choPhepXacThucSdt) {
            mAuth = FirebaseAuth.getInstance();
            // [END initialize_auth]
            initApiVertifiPhoneNumber();

            //send request verification to server firebase
            startPhoneNumberVerification(phoneFull);
        } else {
            verificationSuccess();
        }
    }

    private void onSendSMSCode(String code) {
        verifyPhoneNumberWithCode(mVerificationId, code);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    public void onBackPressed() {
        finish();
    }


    //vertifi phone number

    private FirebaseAuth mAuth;
    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;


    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        showProgress();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void initApiVertifiPhoneNumber() {
        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.e(TAG, "onVerificationCompleted:" + credential);

                hideProgress();

                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                hideProgress();
                Log.e(TAG, "onVerificationFailed", e);
                AppUtil.showToast(self, e.getMessage());

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                    Log.e(TAG, "Invalid credential: " + e.getLocalizedMessage());

                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...

                    Log.e(TAG, " SMS Quota exceeded ");
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                hideProgress();
                Log.e(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                btnResendCode.setVisibility(View.VISIBLE);

                // ...
            }
        };
        // [END phone_auth_callbacks]
    }

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgress();
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.e(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            // [START_EXCLUDE]
                            // updateUI(STATE_SIGNIN_SUCCESS, user);
                            // [END_EXCLUDE]

                            verificationSuccess();
                        } else {

                            // Sign in failed, display a message and update the UI
                            Log.e(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                AppUtil.showToast(self, "Invalid code.");

                                // mVerificationField.setError("Invalid code.");
                                // [END_EXCLUDE]
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
                            // updateUI(STATE_SIGNIN_FAILED);
                            // [END_EXCLUDE]
                        }
                    }
                });
    }


    // [END sign_in_with_phone]


    private void verificationSuccess() {

        //login

        login("", phoneNumber, "1");

    }


    private void login(final String email, String phone, final String type) {
        Log.e("Splash", "login");
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {

            ModelManager.loginWithPhone1(self, Volley.newRequestQueue(self), email, phone, new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    Log.e(TAG, "Login onSuccess");
                    JSONObject jsonObject = (JSONObject) object;
                    ApiResponse response = new ApiResponse(jsonObject);
                    if (!response.isError()) {
                        UserObj userObj = response.getDataObject(UserObj.class);
                        DataStoreManager.saveTokenUser(response.getValueFromRoot(Args.TOKEN));
                        Log.e(TAG, "token : " + response.getValueFromRoot(Args.TOKEN));
                        // It's remember default if users login as social

                        Log.e("Splash", "user " + new Gson().toJson(userObj));
                        DataStoreManager.saveUser(userObj);

                        gotoHomePage(userObj);

                    } else {
                        Log.e("Splash", "response onError");
                        Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onError() {
                    Log.e("Splash", "Login onError");
                    Toast.makeText(self, R.string.msg_have_some_errors, Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(self, getString(R.string.msg_no_network), Toast.LENGTH_SHORT).show();
        }
    }

    private void gotoHomePage(UserObj userObj) {

        if (userObj.getName() != null) {
            if (!userObj.getName().equals(""))
                GlobalFunctions.startActivityWithoutAnimation(self, MainActivity.class);
            else
                GlobalFunctions.startActivityWithoutAnimation(self, UpdateInfoUserActivity.class);

        } else {
            GlobalFunctions.startActivityWithoutAnimation(self, UpdateInfoUserActivity.class);
        }
        EventBus.getDefault().post(new Global.FinishScreenLogin());
        finish();

    }


    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        showProgress();
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        showProgress();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]


    private void showProgress() {
        findViewById(R.id.progress).setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        findViewById(R.id.progress).setVisibility(View.GONE);
    }
}
