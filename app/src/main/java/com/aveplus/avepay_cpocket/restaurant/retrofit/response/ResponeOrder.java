package com.aveplus.avepay_cpocket.restaurant.retrofit.response;

import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.OrderModel;

/**
 * Created by suusoft.com on 11/12/19.
 */

public class ResponeOrder extends BaseModel {
    private OrderModel data;

    public OrderModel getData() {
        return data;
    }

    public void setData(OrderModel data) {
        this.data = data;
    }
}
