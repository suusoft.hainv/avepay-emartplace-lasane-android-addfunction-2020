package com.aveplus.avepay_cpocket.music.main.video2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.main.main2.MainActivity2;
import com.aveplus.avepay_cpocket.music.main.payment.BottomPaymentFragment;
import com.aveplus.avepay_cpocket.music.main.payment.IPayement;
import com.aveplus.avepay_cpocket.music.main.payment.ListSubscribeActivity;
import com.aveplus.avepay_cpocket.music.main.playvideo.PlayVideoActivity;
import com.aveplus.avepay_cpocket.music.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoFrag extends BaseFragment {
    private static String TAG = VideoFrag.class.getSimpleName();
    private int count = 0;
    private ObjVideo video;

    public static VideoFrag newInstance() {
        VideoFrag fragment = new VideoFrag();
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private RecyclerView rcvVideo;
    private AdapterVideo adapterVideo;
    private ArrayList<Video> arrVideo;

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_video2;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        rcvVideo = view.findViewById(R.id.rcv_video);

        setAdapterVD();
        getData();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setAdapterVD() {
        arrVideo = new ArrayList<>();
        adapterVideo = new AdapterVideo(self, arrVideo);
        rcvVideo.setLayoutManager(new LinearLayoutManager(self, RecyclerView.VERTICAL, false));
        rcvVideo.setAdapter(adapterVideo);
    }

    @Override
    protected void getData() {
        ((BaseActivity) self).showProgress(true);
        ApiUtils.getAPIService().getListVideo().enqueue(new Callback<DataVideo>() {
            @Override
            public void onResponse(Call<DataVideo> call, Response<DataVideo> response) {
                if (response.body() != null) {
                    ((BaseActivity) self).showProgress(false);
                    setArrVideo(response.body().getData());
                    adapterVideo.notifyDataSetChanged();
                } else {
                    ((BaseActivity) self).showProgress(false);
                    AppUtil.showToast(self, R.string.msg_have_some_errors);
                }

            }

            @Override
            public void onFailure(Call<DataVideo> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                ((BaseActivity) self).showProgress(false);
            }
        });

    }

    private void setArrVideo(Video data) {
        count = 0;
        Video video;
        if (data.getTotal_list() == 0) {
            AppUtil.showToast(self, R.string.empty);
        } else {
            arrVideo.clear();
            for (int i = 0; i < data.getTotal_list(); i++) {
                video = new Video();
                if (i == 0 && !data.getArrVideoCate().isEmpty()) {
                    video.setName(DataVideo.CATEGORY_VIDEO);
                    video.getArrVideoCate().addAll(data.getArrVideoCate());
                } else if (i == data.getTotal_list() - 1 && !data.getArrVideo().isEmpty()) {
                    video.setName(DataVideo.VIDEO);
                    video.getArrVideo().addAll(data.getArrVideo());
                } else if (data.getArrVideoTitle().size() > 0) {
                    video.setName(data.getArrVideoTitle().get(count).getTitle());
                    video.setId_playlist(data.getArrVideoTitle().get(count).getId());
                    video.getArrVideo().addAll(data.getArrVideoTitle().get(count).getObjVideoArrayList());
                    count++;
                }
                arrVideo.add(video);
            }

        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.VideoItemClicked event) {
        Log.e(TAG, "onMessageEvent: +VideoFrag");
        video = event.type;
        ArrayList<ObjVideo> arrVD = new ArrayList<>();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.PREF_KEY_OBJECT, video);

        if (video.getVideo_title_id() != null) {
            for (int i = 0; i < arrVideo.size(); i++) {
                if (video.getVideo_title_id() == arrVideo.get(i).getId_playlist()) {
                    arrVD.addAll(arrVideo.get(i).getArrVideo());
                }
            }
        } else {
            arrVD.addAll(arrVideo.get(arrVideo.size() - 1).getArrVideo());
        }
        bundle.putParcelableArrayList(Constant.PREF_KEY_DATA_LIST, arrVD);
//        bundle.putParcelableArrayList (Constant.PREF_KEY_DATA_LIST, arrVideo.get (arrVideo.size () - 1).getArrVideo ());

        AppUtil.checkPayment(DataStoreManager.getUser().getToken());
        if (video.getIsFree() == 0) {
            if (DataStoreManager.getCheckPayment()) {
                Intent intent = new Intent(self, PlayVideoActivity.class);
                intent.putExtra(Constant.PREF_KEY_DATA, bundle);
                self.startActivity(intent);
            } else {
                BottomPaymentFragment bottomPaymentFragment = new BottomPaymentFragment(new IPayement() {
                    @Override
                    public void onPayment(String payment) {
                        Intent intent = new Intent(self, ListSubscribeActivity.class);
                        self.startActivity(intent);
                    }
                });
                bottomPaymentFragment.show(((MainActivity2) self).getSupportFragmentManager(), bottomPaymentFragment.getTag());
            }

        } else {
            Intent intent = new Intent(self, PlayVideoActivity.class);
            intent.putExtra(Constant.PREF_KEY_DATA, bundle);
            self.startActivity(intent);
        }

    }
}
