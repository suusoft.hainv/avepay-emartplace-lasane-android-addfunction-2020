package com.aveplus.avepay_cpocket.restaurant.modelmanager;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.aveplus.avepay_cpocket.restaurant.listener.IResponse;
import com.aveplus.avepay_cpocket.restaurant.listener.ModelManagerListener;
import com.aveplus.avepay_cpocket.restaurant.network.VolleyRequestManager;
import com.aveplus.avepay_cpocket.restaurant.retrofit.param.Paramfb;


public class ModelManager {
    private static final String PARAM_FIELDS = "fields";
    private static final String PARAM_ACCESS_TOKEN = "access_token";

    public static void getFacebookInfo(Context ctx, RequestQueue volleyQueue, String accessToken,
                                       final ModelManagerListener listener) {

        String url = Paramfb.URL_GET_FACEBOOK_INFO;

        Uri.Builder builder = Uri.parse(url).buildUpon();
        builder.appendQueryParameter(PARAM_FIELDS, "id,name,email,first_name,last_name,picture");
        builder.appendQueryParameter(PARAM_ACCESS_TOKEN, accessToken);

        new VolleyRequestManager(ctx, true, false).getJSONObject(builder, volleyQueue, new IResponse() {
            @Override
            public void onResponse(Object response) {
                Log.e("ModelManager", "onResponse: "+builder.toString() );
                if (response != null) {
                    listener.onSuccess(response);
                } else {
                    listener.onError();
                }
            }
        });
    }
}
