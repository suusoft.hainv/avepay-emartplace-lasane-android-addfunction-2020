package com.aveplus.avepay_cpocket.movie.movie.main;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;


import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.PacketUtility;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.Constants;
import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.interfaces.IOnItemClickListener;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.movie.movie.configs.Constant;
import com.aveplus.avepay_cpocket.movie.movie.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.movie.movie.main.adapter.SubcategoryAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.fragment.ListMovieFragment;
import com.aveplus.avepay_cpocket.movie.movie.main.fragment.WebViewFragment;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.CallBackNewestMovie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieGenre;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.UpdateGenreCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.onUpdateBackdropMovie;
import com.aveplus.avepay_cpocket.movie.movie.util.AppUtil;
import com.aveplus.avepay_cpocket.movie.movie.util.NetworkUtility;
import com.aveplus.avepay_cpocket.music.MusicActivity;
import com.aveplus.avepay_cpocket.music.fragment.MusicFragment;
import com.aveplus.avepay_cpocket.network1.MyProgressDialog;
import com.aveplus.avepay_cpocket.objects.ContactObj;
import com.aveplus.avepay_cpocket.objects.DealCateObj;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.objects.MenuLeft;
import com.aveplus.avepay_cpocket.objects.SettingsObj;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.aveplus.avepay_cpocket.parsers.JSONParser;
import com.aveplus.avepay_cpocket.quickblox.QbAuthUtils;
import com.aveplus.avepay_cpocket.quickblox.QbDialogHolder;
import com.aveplus.avepay_cpocket.quickblox.SharedPreferencesUtil;
import com.aveplus.avepay_cpocket.quickblox.chat.ChatHelper;
import com.aveplus.avepay_cpocket.quickblox.conversation.utils.DialogUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.aveplus.avepay_cpocket.utils.map.LocationService;
import com.aveplus.avepay_cpocket.view.activities.BaseActivity;
import com.aveplus.avepay_cpocket.view.activities.DealsActivity;
import com.aveplus.avepay_cpocket.view.activities.SplashLoginActivity;
import com.aveplus.avepay_cpocket.view.adapters.MenuLeftAdapte;
import com.aveplus.avepay_cpocket.view.fragments.AllDealsFragment;
import com.aveplus.avepay_cpocket.view.fragments.ContactFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealListFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealManagerFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealsFragment;
import com.aveplus.avepay_cpocket.view.fragments.FragmentFavorite;
import com.aveplus.avepay_cpocket.view.fragments.IwanaPayFragment;
import com.aveplus.avepay_cpocket.view.fragments.IwanabizFragment;
import com.aveplus.avepay_cpocket.view.fragments.MyAccountMyInfoFragment;
import com.aveplus.avepay_cpocket.view.fragments.MyDealFragment;
import com.aveplus.avepay_cpocket.view.fragments.NewsFragment;
import com.aveplus.avepay_cpocket.view.fragments.ProducerManagerFragment;
import com.aveplus.avepay_cpocket.view.fragments.SellerFragment;
import com.aveplus.avepay_cpocket.view.fragments.SettingsFragment;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.SubscribeService;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Copyright © 2019 SUUSOFT
 */
public class CategoryActivity extends BaseActivity implements ListView.OnItemClickListener,
        View.OnClickListener, UpdateGenreCallback, CallBackNewestMovie, NavigationView.OnNavigationItemSelectedListener,
        DealListFragment.IListenerDealsChange, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, IOnItemClickListener {
    private static final String TAG = CategoryActivity.class.getSimpleName();

    private ListView mSubcateListView;
    private ImageButton btnHome;
    private ImageButton btnSearch;//, btnLogout
    private ImageButton btn_next_page;
    private ImageButton btn_prev_page;
    private EditText txtSearch;
    private TextView lblCategory;
    private NetworkImageView img_backdrop;
    private ArrayList<MenuLeft> menuLefts;
    private GoogleApiClient mGoogleApiClient;

    private MoviesManagement moviesManagement;
    private List<MovieGenre> genres;

    private ImageLoader mImageLoader;

    // for phone
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageButton btnSearchBox;

    private boolean isTabblet;
    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private RecyclerView lvMenu;
    private CircleImageView imgAvatar;
    private TextViewBold tvName, tvEmail;
    private MenuLeftAdapte adapter;
    private int indexMenu = 0;
    private FragmentTransaction fragmentTransaction;
    private WebViewFragment mWebViewFragment;


    private MusicFragment mMusicFragment;
    private NewsFragment mNewsFragment;
    private SettingsFragment mSettingsFragment;
    private IwanaPayFragment mIwanaPayFragment;
    //    private MyAccountFragment myAccountFragment;
    private MyAccountMyInfoFragment myAccountFragment;
    private IwanabizFragment mFrgIwanabiz;
    private MyDealFragment mFrgMyDeal;
    private DealsFragment mFrgDeal;
    private DealManagerFragment mFrgDealManager;
    private ContactFragment mContactFragment;
    private FragmentFavorite mFavoriteFragment;
    private SellerFragment mSellerFragment;
    private ProducerManagerFragment mProManaFragment;


    private CoordinatorLayout bgMain;
    private Toolbar toolbar;


    private static final String FRAG_HOME = "Home";
    private static final String FRAG_DEAL_MANAGER = "dealManager";
    private static final String FRAG_SELLER_MANAGER = "sellerManager";
    private static final String FRAG_BUYER_MANAGER = "buyerManager";
    private static final String FRAG_ALL_DEALS = "allDeals";
    private static final String FRAG_NEWS = "newsAndEvents";
    private static final String FRAG_SETTINGS = "settings";
    private static final String FRAG_IWANA_PAY = "iwanapay";
    private static final String FRAG_MY_ACCOUNT = "myaccount";
    private static final String FRAG_MY_DEAL = "mydeal";
    private static final String FRAG_IWANA_CHAT = "FRAG_IWANA_CHAT";
    private static final String FRAG_ABOUT = "FRAG_ABOUT";
    private static final String FRAG_FAQ = "FRAG_FAQ";
    private static final String FRAG_WEBVIEW = "FRAG_WEBVIEW";
    private static final String FRAG_MOVIE = "FRAG_MOVIE";
    private static final String FRAG_MUSIC = "FRAG_MUSIC";

    private RelativeLayout rltMainMovie;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        isTabblet = getResources().getBoolean(R.bool.isTablet);
        if (isTabblet) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
//        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        initView();
        initData();
        checkAndInit();


        // AdsUtil.loadBanner(findViewById(R.id.adView));


        ////////////////////////////////////////////////////////////////////////////////////////////
        initGoogleApiClient();
        // Get saved instances
        if (savedInstanceState != null) {
            mFavoriteFragment = (FragmentFavorite) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_HOME);
            mFrgDealManager = (DealManagerFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_DEAL_MANAGER);
            mSellerFragment = (SellerFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_SELLER_MANAGER);
            mFrgDeal = (DealsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_BUYER_MANAGER);
            mNewsFragment = (NewsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_NEWS);
            mSettingsFragment = (SettingsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_SETTINGS);
            mIwanaPayFragment = (IwanaPayFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_IWANA_PAY);
            myAccountFragment = (MyAccountMyInfoFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_MY_ACCOUNT);
            mFrgMyDeal = (MyDealFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_MY_DEAL);
            mContactFragment = (ContactFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_IWANA_CHAT);
            mWebViewFragment = (WebViewFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_WEBVIEW);
            mMusicFragment = (MusicFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_MUSIC);
        }


        // Init quickblox
        if (com.aveplus.avepay_cpocket.datastore.DataStoreManager.getUser() != null && (com.aveplus.avepay_cpocket.datastore.DataStoreManager.getToken() != null
                && !com.aveplus.avepay_cpocket.datastore.DataStoreManager.getToken().equals(""))) {
            initSession(savedInstanceState);
            initDialogsListener();
            initPushManager();
        }

        // Start location service in some cases(driver closes app without deactivating)
        updateDriverLocation();

        // Get contacts from scratch
        getContacts();

        com.aveplus.avepay_cpocket.utils.AppUtil.logSizeMultiScreen(this);
    }

    private void updateDriverLocation() {
        if (com.aveplus.avepay_cpocket.datastore.DataStoreManager.getUser() != null && com.aveplus.avepay_cpocket.datastore.DataStoreManager.getUser().getDriverData() != null) {
            if (com.aveplus.avepay_cpocket.datastore.DataStoreManager.getUser().getDriverData().isAvailable()) {
                LocationService.start(self, LocationService.REQUEST_LOCATION);
            }
        }
    }

    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(self)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    protected void inflateLayout() {

    }

    @Override
    protected void initUI() {

    }


    @Override
    protected void initControl() {

    }


    private void checkAndInit() {
        if (isTabblet) {
            initList();
        } else {
            initDataPhone();
        }

    }

    private void initView() {
//        initNavigationView();
//        initMenuLeft();


        bgMain = (CoordinatorLayout) findViewById(R.id.bg_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rltMainMovie = findViewById(R.id.rlt_main_movie);

        initNavigationView();

        initMenuLeft();

        switchMenu(Constants.MENU_ALL_DEAL);


        mNavigationView.setNavigationItemSelectedListener(this);
//        btnSearch = (ImageButton) findViewById(R.id.btnSearch);
//        btnSearch.setOnClickListener(this);
//        btnLogout = (ImageButton) findViewById(R.id.btnLogout);
//        btnLogout.setOnClickListener(this);
        img_backdrop = (NetworkImageView) findViewById(R.id.img_backdrop);
//        btnHome = (ImageButton) findViewById(R.id.btn_home);
//        btnHome.setOnClickListener(this);

        if (isTabblet) {
            img_backdrop.setBackgroundResource(R.drawable.backdrop);
//            initViewTablet();
        } else {
            img_backdrop.setBackgroundResource(R.drawable.backdrop_port);
            initViewPhone();
        }

    }

    private void initData() {
        moviesManagement = MoviesManagement.getInstance();
        if (genres == null || genres.size() == 0) {
            MoviesManagement.getInstance().loadMovieGenres(this);
        }
    }

    public List<MovieGenre> getGenres() {
        if (genres == null)
            genres = new ArrayList<>();
        return genres;
    }

    private void setListSubcate() {
        SubcategoryAdapter subcategoryAdapter = new SubcategoryAdapter(
                getApplicationContext(), R.layout.adapter_subcategory, genres);
        mSubcateListView.setAdapter(subcategoryAdapter);

        mSubcateListView.setItemChecked(0, true);
        mSubcateListView.requestFocus();

    }

//    private void setAdapterViewPagger(int position) {
//
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.pagger_subcate, ListMovieFragment.newInstance(position, genres.get(position)));
//        fragmentTransaction.commit();
//    }

    private void requestNewestMovie() {
        moviesManagement.getNewestMovie(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnHome) {

            mDrawer.openDrawer(GravityCompat.START);
            // mDrawer.closeDrawer(GravityCompat.START);
        } else if (view == btnSearch) {
            sendSearch(txtSearch != null ? txtSearch.getText().toString() : "");
        }
//        } else if (view == btnLogout) {
//            showDialogLogout();
//        }
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//        setAdapterViewPagger(position);
    }

    EditText.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
            if (keyCode == EditorInfo.IME_ACTION_SEARCH || keyCode == KeyEvent.KEYCODE_ENTER) {
                sendSearch(txtSearch.getText().toString());
                return true;
            }
            return false;
        }
    };
    EditText.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txtSearch, InputMethodManager.SHOW_IMPLICIT);
            } else {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txtSearch.getWindowToken(), 0);
            }
        }
    };
    AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {

        }

        @Override
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            if (i + i2 == i3) {
                btn_next_page.setVisibility(View.INVISIBLE);
            } else if (i == 0) {
                btn_prev_page.setVisibility(View.INVISIBLE);
            } else {
                btn_next_page.setVisibility(View.VISIBLE);
                btn_prev_page.setVisibility(View.VISIBLE);
            }
        }
    };

    private void sendSearch(String text) {
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(SearchActivity.TEXT_SEARCH, text);
        startActivity(intent);
    }


    private void initList() {
        genres = moviesManagement.getmMovieGenres();
        setListSubcate();
        Log.e("BUG", "initList: " + genres.size());
        if (genres.size() > 0) {
//            setAdapterViewPagger(0);
        }
    }

    @Override
    public void onCompleted(boolean isSuccess) {
        if (isTabblet) {
            initList();
        } else {
            initDataPhone();
        }
    }

    @Override
    public void success(Movie movie) {
        mImageLoader = RequestManager.getImageLoader();
        moviesManagement.movieBackdropImgRequest(movie.getMovie_imdb(), new onUpdateBackdropMovie() {
            @Override
            public void onCompleted(String response) {
                moviesManagement.setUrlBackDrop(response);
                img_backdrop.setImageUrl(response, mImageLoader);
            }

            @Override
            public void onError(Exception ex) {
                img_backdrop.setBackgroundResource(R.drawable.backdrop);
            }
        });
    }

    //--------------------------------- PHONE --------------------------------
    private void initViewPhone() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
    }

    private void initDataPhone() {
        genres = moviesManagement.getmMovieGenres();
        viewPager.setAdapter(new ViewPaggerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }


    private void getContacts() {
        if (com.aveplus.avepay_cpocket.network1.NetworkUtility.getInstance(self).isNetworkAvailable()) {
            ModelManager.getContacts(self, false, new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    Log.e(TAG, "contacts " + new Gson().toJson(object));
                    JSONObject jsonObject = (JSONObject) object;
                    if (JSONParser.responseIsSuccess(jsonObject)) {
                        ArrayList<ContactObj> contactObjs = JSONParser.parseContacts(jsonObject);
                        if (contactObjs.size() > 0) {
                            // Save contacts into preference
                            com.aveplus.avepay_cpocket.datastore.DataStoreManager.saveContactsList(contactObjs);
                        }
                    }
                }

                @Override
                public void onError() {
                }
            });
        }
    }


    @Override
    public void onChanged(ArrayList<DealObj> mDealObj) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }


    // adapter for viewpager phone
    private class ViewPaggerAdapter extends FragmentStatePagerAdapter {

        public ViewPaggerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ListMovieFragment.newInstance(position, getGenres().get(position));
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getGenres().size() != 0 ? getGenres().get(position).getName() : "";
        }

        @Override
        public int getCount() {
            return getGenres().size();
        }
    }

    private void initNavigationView() {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AppUtil.hideSoftKeyboard(CategoryActivity.this);

//                updateMenuHeader();
            }
        };
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_drawer, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switchMenu(id);
        return super.onOptionsItemSelected(item);

    }

    private void initListMenu() {
        menuLefts = new ArrayList<>();

        menuLefts.add(new MenuLeft(Constants.MENU_ALL_DEAL, R.drawable.ic_all_deals_white,
                getResources().getString(R.string.home), true));

        menuLefts.add(new MenuLeft(Constants.MENU_CHAT, R.drawable.ic_chatbubble_,
                getResources().getString(R.string.chat), false));
        menuLefts.add(new MenuLeft(Constants.MENU_PROFILE, R.drawable.ic_profiles_white,
                getResources().getString(R.string.profile), false));

        menuLefts.add(new MenuLeft(Constants.MENU_SHARE, R.drawable.ic_share_white,
                getResources().getString(R.string.share), false));
        menuLefts.add(new MenuLeft(Constants.MENU_FAQ, R.drawable.ic_faq,
                getResources().getString(R.string.faq), false));
        menuLefts.add(new MenuLeft(Constants.MENU_SETTING, R.drawable.ic_setting_white,
                getResources().getString(R.string.settings), false));
        menuLefts.add(new MenuLeft(Constants.MENU_HELP, R.drawable.ic_help_white,
                getResources().getString(R.string.help), false));
        menuLefts.add(new MenuLeft(Constants.MENU_ABOUT_US, R.drawable.ic_aboutus_white,
                getResources().getString(R.string.about_us), false));
        menuLefts.add(new MenuLeft(Constants.MENU_LOGOUT, R.drawable.ic_signout_white,
                getResources().getString(R.string.log_out), false));


    }

    private void initMenuLeft() {
        imgAvatar = (CircleImageView) findViewById(R.id.img_avatar);
        tvName = (TextViewBold) findViewById(R.id.lbl_deal_name);
        tvEmail = (TextViewBold) findViewById(R.id.tv_email);
        lvMenu = (RecyclerView) findViewById(R.id.lv_menu);

        initMenuLeftHeader();

        initListMenu();

        adapter = new MenuLeftAdapte(menuLefts, self, new IOnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (indexMenu != position) {
                    menuLefts.get(indexMenu).setSelected(false);
                    indexMenu = position;
                    switchMenu(menuLefts.get(position).getId());
                    menuLefts.get(position).setSelected(true);
                } else mDrawer.closeDrawer(GravityCompat.START);

                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager manager = new LinearLayoutManager(self);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        lvMenu.setAdapter(adapter);
        lvMenu.setLayoutManager(manager);
        lvMenu.setHasFixedSize(true);

    }

    private void initMenuLeftHeader() {
        UserObj userObj = com.aveplus.avepay_cpocket.datastore.DataStoreManager.getUser();
        if (userObj != null) {
            tvName.setText(userObj.getName());
            tvEmail.setText(userObj.getEmail());
            ImageUtil.setImage(self, imgAvatar, userObj.getAvatar());
        }
    }

    public void switchMenu(int idMenuLeft) {
        setbackGroundMainHaveData();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (idMenuLeft) {
            case Constants.MENU_ALL_DEAL:
//                if (mMusicFragment == null) {
//                    mMusicFragment = MusicFragment.newInstance(Args.TYPE_OF_CATEGORY_ALL);
//                }
//                fragmentTransaction.replace(R.id.frl_main, mMusicFragment).commit();
                rltMainMovie.setVisibility(View.VISIBLE);
                setTitle(R.string.movie);
                break;

            case Constants.MENU_CHAT:
                if (mContactFragment == null) {
                    mContactFragment = ContactFragment.newInstance();
                }
                rltMainMovie.setVisibility(View.GONE);
                fragmentTransaction.replace(R.id.frl_main, mContactFragment).commit();

                setTitle(R.string.chat);
                break;



            case Constants.MENU_PROFILE:
                if (myAccountFragment == null) {
                    myAccountFragment = MyAccountMyInfoFragment.newInstance();
                }
                rltMainMovie.setVisibility(View.GONE);
                fragmentTransaction.replace(R.id.frl_main, myAccountFragment).commit();

                setTitle(R.string.profile);
                break;


            case Constants.MENU_SHARE:
                AppUtil.share(this, "http://play.google.com/store/apps/details?id=" + new PacketUtility().getPacketName());

                setTitle(R.string.share);

                break;

            case Constants.MENU_FAQ:
                rltMainMovie.setVisibility(View.GONE);
                showScreenFaq();
                break;

            case Constants.MENU_SETTING:
                rltMainMovie.setVisibility(View.GONE);
                fragmentTransaction.replace(R.id.frl_main, SettingsFragment.newInstance()).commit();
                setTitle(R.string.settings);
                break;

            case Constants.MENU_HELP:
                rltMainMovie.setVisibility(View.GONE);
                showScreenHelp();
                break;

            case Constants.MENU_ABOUT_US:
                rltMainMovie.setVisibility(View.GONE);
                showScreenAboutUs();
                break;

            case Constants.MENU_LOGOUT:
                logout();
                break;
        }

        mDrawer.closeDrawer(GravityCompat.START);

    }

    private void setbackGroundMainHaveData() {
        bgMain.setBackgroundResource(R.drawable.bg_deal);
    }

    private void logout() {
        showDialogLogout();
    }

    private void showDialogLogout() {
        DialogUtil.showAlertDialog(self, R.string.log_out, R.string.you_wanto_logout, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                requestLogout();
            }
        });
    }

    private void requestLogout() {
        if (com.aveplus.avepay_cpocket.network1.NetworkUtility.getInstance(self).isNetworkAvailable()) {
            final MyProgressDialog progressDialog = new MyProgressDialog(self);
            progressDialog.show();

            if (QbAuthUtils.isSessionActive()) {
                ChatHelper.getInstance().logout(new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        processBeforeLoggingOut(progressDialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Log.e(TAG, "Log out - onError: " + e.getMessage());
                    }
                });
            } else {
                ChatHelper.getInstance().login(self, SharedPreferencesUtil.getQbUser(), new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        processBeforeLoggingOut(progressDialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Toast.makeText(self, "Fail to logout: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }


    private void processBeforeLoggingOut(MyProgressDialog progressDialog) {
        if (com.aveplus.avepay_cpocket.datastore.DataStoreManager.getUser() != null && com.aveplus.avepay_cpocket.datastore.DataStoreManager.getUser().getDriverData() != null) {
            if (com.aveplus.avepay_cpocket.datastore.DataStoreManager.getUser().getDriverData().isAvailable()) {
                LocationService.start(self, LocationService.STOP_REQUESTING_LOCATION);

                // Deactivate driver's mode before logging out
                ModelManager.activateDriverMode(self, Constants.OFF, 0, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                    }

                    @Override
                    public void onError() {
                    }
                });
            }
        }

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        SubscribeService.unSubscribeFromPushes(self);
        QBChatService.getInstance().destroy();
        SharedPreferencesUtil.removeQbUser();
        QbDialogHolder.getInstance().clear();

        com.aveplus.avepay_cpocket.datastore.DataStoreManager.clearUserToken();
        AppController.getInstance().setUserUpdated(true);
        com.aveplus.avepay_cpocket.utils.AppUtil.startActivity(self, SplashLoginActivity.class);
        finish();
    }

    private void showScreenFaq() {
        SettingsObj setting = com.aveplus.avepay_cpocket.datastore.DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (com.aveplus.avepay_cpocket.network1.NetworkUtility.getInstance(this).isNetworkAvailable()) {
                ModelManager.getSettingUtility(this, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        JSONObject jsonObject = (JSONObject) object;
                        ApiResponse apiResponse = new ApiResponse(jsonObject);
                        if (!apiResponse.isError()) {
                            com.aveplus.avepay_cpocket.datastore.DataStoreManager.saveSettingUtility(jsonObject.toString());
                            SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.faq), utitlityObj.getFaq());

                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.KEY_URL, utitlityObj.getFaq());
                            mWebViewFragment = WebViewFragment.newInstance(bundle);

                            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                            setTitle(getString(R.string.faq));

//                                    openWebView(getString(R.string.about_us), DataStoreManager.getSettingUtility().getAbout());
                        }

                    }

                    @Override
                    public void onError() {
                    }
                });
            } else {
                com.aveplus.avepay_cpocket.utils.AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_URL, com.aveplus.avepay_cpocket.datastore.DataStoreManager.getSettingUtility().getFaq());
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

            setTitle(getString(R.string.faq));

//                    openWebView(getString(R.string.faq), DataStoreManager.getSettingUtility().getFaq());
        }
    }

    private void showScreenAboutUs() {
        SettingsObj setting = DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (NetworkUtility.isNetworkAvailable()) {


                Bundle bundle = new Bundle();
                bundle.putString(Constant.KEY_URL, "http://suusoft-demo.com/products/movie/backend/web/index.php/setting/about");
                mWebViewFragment = WebViewFragment.newInstance(bundle);

                fragmentTransaction.replace(R.id.frl_main1, mWebViewFragment).addToBackStack("help").commit();

                setTitle(getString(R.string.help));


            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constant.KEY_URL, "http://suusoft-demo.com/products/movie/backend/web/index.php/setting/about");
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main1, mWebViewFragment).commit();

            setTitle(getString(R.string.help));

//                    openWebView(getString(R.string.help), DataStoreManager.getSettingUtility().getHelp());
        }
    }

    private void showScreenHelp() {
        SettingsObj setting = DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (NetworkUtility.isNetworkAvailable()) {


                Bundle bundle = new Bundle();
                bundle.putString(Constant.KEY_URL, "http://suusoft-demo.com/products/movie/backend/web/index.php/setting/help");
                mWebViewFragment = WebViewFragment.newInstance(bundle);

                fragmentTransaction.replace(R.id.frl_main1, mWebViewFragment).addToBackStack("help").commit();

                setTitle(getString(R.string.help));


            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constant.KEY_URL, "http://suusoft-demo.com/products/movie/backend/web/index.php/setting/help");
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main1, mWebViewFragment).commit();

            setTitle(getString(R.string.help));

//                    openWebView(getString(R.string.help), DataStoreManager.getSettingUtility().getHelp());
        }
    }


}

