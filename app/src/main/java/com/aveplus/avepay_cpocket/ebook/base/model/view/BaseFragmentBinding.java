package com.aveplus.avepay_cpocket.ebook.base.model.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.Fragment;

import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;


/**
 * Created by Trang on 6/29/2017.
 */
public abstract class BaseFragmentBinding extends Fragment {

    protected Context self;
    protected ViewDataBinding binding;
    protected BaseViewModel viewModel;

    protected abstract int getLayoutInflate();
    protected abstract void initialize();
    protected abstract BaseViewModel getViewModel();
    protected abstract void initView(View view);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self = getActivity();
        initialize();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,getLayoutInflate(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        viewModel = getViewModel();
        setViewDataBinding(binding);
        checkHasOptionMenu();
        initView(view);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (viewModel.isHasMenuOption()) {
            menu.clear();
            inflater.inflate(viewModel.getMenuLayout(), menu);
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (viewModel != null) {
            viewModel.setVisibleToUser(isVisibleToUser);
        }
    }

    private void checkHasOptionMenu(){
        if (viewModel.isHasMenuOption()) {
            setHasOptionsMenu(true);
        }
    }

    protected void setViewDataBinding(ViewDataBinding binding){
        this.binding = binding;
        this.binding.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewModel != null)
             viewModel.onDestroyView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        viewModel.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (viewModel != null)
            viewModel.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (viewModel != null)
            viewModel.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (viewModel != null)
            viewModel.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null)
            viewModel.onDestroy();
    }
}
