package com.aveplus.avepay_cpocket.music.main.video2.vm;

import android.content.Context;
import android.util.Log;
import android.view.View;


import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.main.video2.ObjVideo;

import org.greenrobot.eventbus.EventBus;

public class ItemVideoVM extends BaseAdapterVM {

    private ObjVideo video;
    private int mPosition;


    public ItemVideoVM(Context self, Object object, int potision, int mPo) {
        super (self, potision);
        this.video = (ObjVideo) object;
        this.mPosition = mPo;

        Log.e ("ItemVideoVM", "ItemVideoVM: " + mPosition);
    }

    public ItemVideoVM(Context self, ObjVideo video) {
        super (self);
        this.video = video;
    }

    public String getImage() {
        return video.getImage ();
    }

    public String getName() {
        return video.getName ();
    }

    public String getVideo() {
        return video.getUrl ();
    }

    public String getDescription() {
        return video.getDescription ();
    }

    @Override
    public void setData(Object object) {

    }

    public int isVisible() {
        if (video.getIsFree () == 0) {
            return View.VISIBLE;
        }
        return View.GONE;
    }

    public void onItemClicked(View view) {
        EventBus.getDefault ().post (new Global.VideoItemClicked (video));
    }


}
