package com.aveplus.avepay_cpocket.music.main.player;

import android.content.Context;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class QueueSongVM extends BaseViewModelList implements ItemMenuActionSongListener {

    ArrayList<Song> listData;

    public QueueSongVM(Context context) {
        super(context);
        initData();
        EventBus.getDefault().register(this);

    }


    private void initData(){
        listData = QueueManager.getInstance().getPlayingQueue();
        getData(1);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void getData(int page) {
        setListData(listData);
        checkLoadingMoreComplete(1);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.ItemChanged event) {
        getData(1);
    }

    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().setPositionCurrentItem(position);
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {
        if(QueueManager.getInstance().remove(position)) {
            AppUtil.showToast(self, R.string.removed);
            getData(1);
        }else {
            AppUtil.showToast(self, R.string.cannot_remove_this_song);
        }
    }
}
