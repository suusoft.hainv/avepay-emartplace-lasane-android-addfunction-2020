package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.FragmentSearchBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.SingleAdapter;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.util.StringUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.SearchVM;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemBookVM;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Suusoft on 9/8/2017.
 */
public class FragmentSearch extends BaseFragmentBinding {

    private SearchVM viewModel;
    private FragmentSearchBinding binding;
    private String key;
    private List<Book> dataList;

    public static FragmentSearch newInstance() {
        Bundle args = new Bundle();
        FragmentSearch fragment = new FragmentSearch();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_search;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new SearchVM(self);
        return viewModel;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        this.binding = (FragmentSearchBinding) binding;
        this.binding.setViewModel(viewModel);
    }


    @Override
    protected void initView(View view) {
        binding.edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    binding.imvSearch.performClick();
                    return true;
                }
                return false;
            }
        });
        binding.imvSearch.setColorFilter(Color.BLACK);
        binding.imvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtil.hideSoftKeyboard((Activity) self);
                key = binding.edtSearch.getText().toString();
                if (StringUtil.isEmpty(key)) {
                    ((MainActivity) self).showSnackBar(R.string.message_search_empty);
                    setDataRecycleView(dataList);
                    return;
                }
                ((MainActivity) self).showProgress(true);
                RequestManager.getBookList(self, Constant.TYPE_ALL, "", key, String.valueOf(1),
                        new ApiManager.CompleteListener() {
                            @Override
                            public void onSuccess(ApiResponse response) {
                                dataList = response.getDataList(Book.class);
                                setDataRecycleView(dataList);
                                if (dataList.isEmpty()) {
                                    ((MainActivity) self).showSnackBar(R.string.no_data);
                                } else {
                                    ((MainActivity) self).showSnackBar(R.string.successfully);
                                }
                                ((MainActivity) self).showProgress(false);
                            }

                            @Override
                            public void onError(String message) {
                                ((MainActivity) self).showSnackBar(R.string.error);
                                setDataRecycleView(dataList);
                            }
                        });
            }
        });
    }

    void setDataRecycleView(List<Book> dataList) {
        if (dataList == null) {
            dataList = new ArrayList<>();
        }
        binding.rcvData.setLayoutManager(new LinearLayoutManager(self));
        SingleAdapter adapter = new SingleAdapter(self, R.layout.item_book_grid_all, dataList, ItemBookVM.class);
        binding.rcvData.setAdapter(adapter);
    }


    @Override
    public void onResume() {
        super.onResume();
    }
}
