package com.aveplus.avepay_cpocket.music.datastore;

import com.aveplus.avepay_cpocket.music.main.addlist.Myplaylist;
import com.aveplus.avepay_cpocket.music.main.login.UserObj;
import com.aveplus.avepay_cpocket.music.model.Playlist;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.model.Video;
import com.google.gson.Gson;


import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class DataStoreManager extends BaseDataStore {


    // ============== User ============================
    private static final String PREF_USER = "PREF_USER";
    private static final String PREF_TOKEN_USER = "PREF_TOKEN_USER";

    // ============== Settings ============================
    private static final String PREF_SETTINGS_NOTIFY = "PREF_SETTINGS_NOTIFY";
    private static final String PREF_SETTINGS_KEEP_SCREEN = "PREF_SETTINGS_KEEP_SCREEN";
    private static final String PREF_SETTINGS_NOTIFY_MESSAGE = "PREF_SETTINGS_NOTIFY_MESSAGE";
    // ============== Data Cache ============================
    private static final String PREF_DATA_CHANNEL = "PREF_DATA_CHANNEL";
    private static final String PREF_STATUS_PAYMENT_METHOD = "PREF_STATUS_PAYMENT_METHOD";
    private static final String PREF_CHECK_PAYMENT = "PREF_CHECK_PAYMENT";
    private static final String PREF_CHECK_PUSH = "PREF_CHECK_PUSH";
    private static final String PREF_MYPLAYLIST = "PREF_MYPLAYLIST";

    /**
     * save and get user
     */
    public static void saveUser(UserObj user) {
        if (user != null) {
            String jsonUser = new Gson().toJson(user);
            getInstance().sharedPreferences.putStringValue(PREF_USER, jsonUser);
        }
    }

    public static void removeUser() {
        BaseDataStore.getInstance().sharedPreferences.putStringValue(PREF_USER, null);
        getInstance().sharedPreferences.putStringValue(PREF_USER, null);
    }

    public static UserObj getUser() {
        String jsonUser = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_USER);
        UserObj user = new Gson().fromJson(jsonUser, UserObj.class);
        return user;
    }

    /**
     * save and get user's token
     *
     */
//    public static void saveToken(String token) {
//        getInstance().sharedPreferences.putStringValue(PREF_TOKEN_USER, token);
//    }
//    public static String getToken() {
//        return getInstance().sharedPreferences.getStringValue(PREF_TOKEN_USER);
//    }


    /**
     * =======================================================
     * Settings screen
     */
    public static void saveSettingsNotify(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY, isChecked);
    }

    public static boolean getSettingsNotify() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY);
    }

    public static void saveSettingsNotifyMessage(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_NOTIFY_MESSAGE, isChecked);
    }

    public static boolean getSettingsNotifyMessage() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_NOTIFY_MESSAGE);
    }

    public static void setAwakeScreen(boolean isChecked) {
        getInstance().sharedPreferences.putBooleanValue(PREF_SETTINGS_KEEP_SCREEN, isChecked);
    }

    public static boolean isAwakeScreen() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_SETTINGS_KEEP_SCREEN);
    }

    /**
     * Favourite
     */
    public static ArrayList<Song> getFavourites() {
        return getInstance().dbConnection.getFavourites();
    }

    public static void saveFavorited(Song item) {
        getInstance().dbConnection.saveFavorited(item);
    }

    public static boolean isFavorited(String videoId) {
        return getInstance().dbConnection.isFavorited(videoId);
    }

    public static void unFavorite(String id) {
        getInstance().dbConnection.unFavorite(id);
    }

    public static void clearFavorites() {
        getInstance().dbConnection.clearFavorites();
    }

    /**
     * MyList
     */
    public static ArrayList<Playlist> getMylist() {
        return getInstance().dbConnection.getMyList();
    }

    public static void saveMyList(Video item) {
        getInstance().dbConnection.saveMyList(item);
    }

    public static void saveMyList(Playlist item) {
        getInstance().dbConnection.saveMyList(item);
    }

    public static boolean isSavedMyList(String id) {
        return getInstance().dbConnection.isSavedMyList(id);
    }

    public static void deleteMyList(String id) {
        getInstance().dbConnection.deleteMyList(id);
    }

    public static void deleteAllMyList() {
        getInstance().dbConnection.deleteAllMyList();
    }

    /**
     * save channel image
     */
    public static void saveChannelInfor(Video data) {
        getInstance().sharedPreferences.putStringValue(PREF_DATA_CHANNEL, data.toJSon());
    }

    public static Video getChannelInfor() {
        Gson gson = new Gson();
        return gson.fromJson(getInstance().sharedPreferences.getStringValue(PREF_DATA_CHANNEL), Video.class);
    }

    public static String getStatusPaymentMethod() {
        return getInstance().sharedPreferences.getStringValue(PREF_STATUS_PAYMENT_METHOD);
    }

    public static void saveStatusPaymentMethod(String status) {
        getInstance().sharedPreferences.putStringValue(PREF_STATUS_PAYMENT_METHOD, status);
    }

    public static boolean getCheckPayment() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_CHECK_PAYMENT);
    }

    public static void saveCheckPayment(boolean status) {
        getInstance().sharedPreferences.putBooleanValue(PREF_CHECK_PAYMENT, status);
    }

    public static boolean getCheckPush() {
        return getInstance().sharedPreferences.getBooleanValue(PREF_CHECK_PUSH);
    }

    public static void saveCheckPush(boolean status) {
        getInstance().sharedPreferences.putBooleanValue(PREF_CHECK_PUSH, status);
    }

    public static void saveMyPlaylist(Myplaylist user) {
        if (user != null) {
            String jsonSetting = user.toJSon();
            getInstance().sharedPreferences.putStringValue(PREF_MYPLAYLIST, jsonSetting);
        }
    }


    public static Myplaylist getMyPlaylist() {
        String jsonSetting = BaseDataStore.getInstance().sharedPreferences.getStringValue(PREF_MYPLAYLIST);
        Myplaylist user = new Gson().fromJson(jsonSetting, Myplaylist.class);
        return user;
    }
}
