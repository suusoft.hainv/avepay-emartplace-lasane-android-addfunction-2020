package com.aveplus.avepay_cpocket.view.activities;

import android.os.Bundle;

import com.aveplus.avepay_cpocket.R;

public class VideoActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void inflateLayout() {
        setContentView(R.layout.activity_video);
    }

    @Override
    protected void initUI() {
    }

    @Override
    protected void initControl() {
    }
}
