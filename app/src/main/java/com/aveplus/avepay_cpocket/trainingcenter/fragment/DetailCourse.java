package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ClassAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.object.Class;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.squareup.picasso.Picasso;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DetailCourse extends BaseFragment {

    TextView txttitle, txtdesc, txtcontent;
    ImageView imgservice;
    private ArrayList<Class> mListClass;
    private ListView mLvListClass;
    private ClassAdapter mClassAdapter;
    private Bundle bundle;
    private TextView btnComment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.detail_course, container, false);
        intiView(view);
        intiData();
        initControl();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("classes");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("product");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    public void intiView(View v) {
        txttitle = (TextView) v.findViewById(R.id.txtTitleServicesDetail);
        imgservice = (ImageView) v.findViewById(R.id.imgServiceDetail);
        mLvListClass = (ListView) v.findViewById(R.id.lvListSchedules);
        btnComment = (TextView) v.findViewById(R.id.btn_comment);
        txttitle.setSelected(true);


    }

    private void initControl() {
        mLvListClass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DetailSchedule fragment = new DetailSchedule();
                Class aClass = mListClass.get(position);
                long startTime = parserStringDateToTimeStamp(aClass.getmStartDate() + " 23:59:59");
                if (System.currentTimeMillis() < startTime) {
                    aClass.setExpired(false);
                } else {
                    aClass.setExpired(true);
                }
                bundle.putParcelable("Class", aClass);
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left,
                                R.anim.slide_out_left)
                        .replace(R.id.showPro, fragment, "detailschedule")
                        .addToBackStack("detailschedule").commit();
            }
        });

        btnComment.setText(Html.fromHtml("<u>" + String.format(getString(R.string.click_testimonial) + "</u>")));
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(
                                R.anim.slide_in_left,
                                R.anim.slide_out_left)
                        .replace(R.id.showPro, new FragmentComment(), "fragmentcomment")
                        .addToBackStack("fragmentcomment")
                        .commit();
            }
        });
    }

    private long parserStringDateToTimeStamp(String stringDate) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public void intiData() {
        bundle = this.getArguments();
        Product product = bundle.getParcelable("item");
        mListClass = product.getmListClass();
        mClassAdapter = new ClassAdapter(mListClass);
        mLvListClass.setAdapter(mClassAdapter);
        txttitle.setText(product.getName());
        String image = product.getImage().size() > 1 ? product.getImage().get(1) : product.getImage().get(0);
        Picasso.get().load(image).into(imgservice);
    }
}
