package com.aveplus.avepay_cpocket.view.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.Constants;
import com.aveplus.avepay_cpocket.interfaces.IOnItemClickMenuListener;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.objects.ReservationObj;
import com.aveplus.avepay_cpocket.objects.reservationsold.ReserSoldItem;
import com.aveplus.avepay_cpocket.objects.reservationsold.ReservationSoldResponse;
import com.aveplus.avepay_cpocket.quickblox.conversation.utils.DialogUtil;
import com.aveplus.avepay_cpocket.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.ViewUtil;
import com.aveplus.avepay_cpocket.view.activities.DealDetailActivity;
import com.aveplus.avepay_cpocket.view.activities.TripFinishingActivity;
import com.aveplus.avepay_cpocket.view.adapters.DealAdapter;
import com.aveplus.avepay_cpocket.view.adapters.ReservationAdatper;
import com.aveplus.avepay_cpocket.widgets.recyclerview.EndlessRecyclerOnScrollListener;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Suusoft on 11/28/2016.
 */

public class ReservationListFragment extends com.aveplus.avepay_cpocket.base.BaseFragment implements EndlessRecyclerOnScrollListener.OnLoadMoreListener, IOnItemClickMenuListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRclDeal;
    private ReservationAdatper mAdapter;
    private DealAdapter adapter;
    private ArrayList<DealObj> mDatas;
    private ArrayList<ReserSoldItem> reserSoldItemList;
    private List<ReservationObj> reservationList;
    private LinearLayout llNoData, llNoConnection;
    private SwipeRefreshLayout swipeRefreshLayout;

    private String typeOfSearch;
    private int page = 1;

    private GridLayoutManager layoutManager;
    private EndlessRecyclerOnScrollListener onScrollListener;
    private String TAG = ReservationListFragment.class.getSimpleName();

    public static ReservationListFragment newInstance(Bundle args) {

        ReservationListFragment fragment = new ReservationListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.layout_list_reservation;
    }

    @Override
    protected void init() {
        Bundle bundle = getArguments();
        typeOfSearch = bundle.getString(Args.TYPE_OF_SEARCH_DEAL, "");

    }

    @Override
    protected void initView(View view) {
        mRclDeal = (RecyclerView) view.findViewById(R.id.rcv_data);
        llNoData = (LinearLayout) view.findViewById(R.id.ll_no_data);
        llNoConnection = (LinearLayout) view.findViewById(R.id.ll_no_connection);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        if (AppUtil.getWidthDp(getActivity()) > 600) {
            layoutManager = new GridLayoutManager(self, 2);
        } else {
            layoutManager = new GridLayoutManager(self, 1);
        }

        onScrollListener = new EndlessRecyclerOnScrollListener(this, layoutManager);
        mRclDeal.setLayoutManager(layoutManager);
        mRclDeal.addOnScrollListener(onScrollListener);

        // Init adapter first
        if (mDatas == null) {
            mDatas = new ArrayList<>();
        } else {
            mDatas.clear();
        }
        reservationList = new ArrayList<>();
        reserSoldItemList = new ArrayList<>();
        mAdapter = new ReservationAdatper(this, reserSoldItemList, typeOfSearch, this);

//        adapter =  new DealAdapter(this, mDatas,typeOfSearch);
        mRclDeal.setAdapter(mAdapter);
    }

    @Override
    public void onLoadMore(int page) {
        Log.e("xx", "more");

        if (!mDatas.isEmpty()) {
            this.page = page;
            getData();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ReservationAdatper.RQ_UPDATE_DEAL) {
            if (resultCode == DealDetailActivity.RC_UPDATE_DEAL) {
                mDatas.clear();
                mAdapter.notifyDataSetChanged();
                page = 1;
                onScrollListener.setEnded(false);
                onScrollListener.setCurrentPage(page);
                getData();
            }
        }
    }

    private void showRatingDialog(final DealObj item, final ReservationObj reservationObj) {
        final Dialog dialog = DialogUtil.setDialogCustomView(self, R.layout.dialog_rating, false);

        TextViewBold lblSubmit = (TextViewBold) dialog.findViewById(R.id.lbl_submit);
        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.rating_bar);
        final EditText edtContent = (EditText) dialog.findViewById(R.id.txt_comment);
        ((LayerDrawable) ratingBar.getProgressDrawable()).getDrawable(2)
                .setColorFilter(ratingBar.getContext().getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        ViewUtil.setRatingbarColor(ratingBar);

        lblSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float rating = ratingBar.getRating();
                String content = edtContent.getText().toString();
                if (rating == 0) {
                    AppUtil.showToast(getActivity(), R.string.msg_rating_require);
                    return;
                }
                if (content.isEmpty()) {
                    AppUtil.showToast(getActivity(), R.string.msg_content_require);
                    return;
                } else {
                    if (typeOfSearch.equals(Constants.SOLD)) {
                        postComment(rating * 2, content, String.valueOf(reservationObj.getBuyer_id()), item.getId(), Constants.DEAL);
                    } else {
                        postComment(rating * 2, content, item.getSeller_id(), item.getId(), Constants.DEAL);
                    }
                }

                dialog.dismiss();
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }


    @Override
    protected void getData() {
        if (!onScrollListener.isEnded()) {
            Log.e("ReservationListFragment", "xxx: " + typeOfSearch + " page " + page);
            swipeRefreshLayout.setRefreshing(true);
            Call<ReservationSoldResponse> call = ApiUtils.getAPIService().getReserSold(DataStoreManager.getToken(), typeOfSearch, "", page);
            Log.d(TAG, "getDataSoldF: " + call.request());
            call.enqueue(new Callback<ReservationSoldResponse>() {
                @Override
                public void onResponse(Call<ReservationSoldResponse> call, Response<ReservationSoldResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            mDatas.clear();
                            Log.e("ReservationListFragment", "onSuccess response success");
                            reservationList.clear();
                            List<ReserSoldItem> items = response.body().getData();
                            reserSoldItemList.clear();
                            reserSoldItemList.addAll(response.body().getData());
//                            for (int i = 0; i < items.size(); i++) {
//                                ReservationObj reservationObj = new ReservationObj();
//                                reservationObj.setBuyer_avatar(items.get(i).getBuyerAvatar());
//                                reservationObj.setBuyer_confirm(items.get(i).getBuyerConfirm());
//                                reservationObj.setBuyer_id(items.get(i).getBuyerId());
//                                reservationObj.setId(items.get(i).getId());
//                                reservationObj.setDeal_id(items.get(i).getDealId());
//                                reservationObj.setSeller_id(items.get(i).getSellerId());
//                                reservationObj.setBuyer_name(items.get(i).getBuyerName());
//                                reservationObj.setTime(Long.parseLong(items.get(i).getTime()));
//                                reservationObj.setBuyer_visible(items.get(i).getBuyerVisible());
//                                reservationObj.setStatus(items.get(i).getStatus());
//                                reservationObj.setSeller_visible(items.get(i).getSellerVisible());
//                                reservationObj.setSeller_avatar(items.get(i).getSellerAvatar());
//                                reservationObj.setSeller_confirm(items.get(i).getSellerConfirm());
//                                reservationObj.setSeller_name(items.get(i).getSellerName());
//                                reservationObj.setCreated_date(items.get(i).getCreatedDate());
//                                reservationObj.setDeal_name(items.get(i).getDealName());
//                                reservationObj.setPrice(items.get(i).getPrice());
//                                reservationObj.setPayment_status(items.get(i).getPaymentStatus());
//                                reservationObj.setIs_active(items.get(i).getIsActive());
//                                reservationObj.setPayment_method(items.get(i).getPaymentMethod());
//                                reservationObj.setDeal(items.get(i).getDeal());
//                                reservationList.add(reservationObj);
//                            }
//                            for (ReserSoldItem item : reserSoldItemList) {
////                                if (item.getDeal() != null)
//                                    mDatas.add(item.getDeal());
////                                Log.e("ReservationListFragment", "add" + item.getDeal().toString());
//                            }
                            Log.e("ReservationListFragment", "add" + mDatas.toString());
                            Log.e("ReservationListFragment", "add" + mDatas.size());

                            mAdapter.notifyDataSetChanged();

                            onScrollListener.onLoadMoreComplete();
                            int totalPage = response.body().getTotalPage();
                            boolean isEnded = totalPage == 0 || page >= totalPage;
                            onScrollListener.setEnded(isEnded);

                            swipeRefreshLayout.setRefreshing(false);
                            checkViewHideShow();
                        } else {
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(self, "No data", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(self, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ReservationSoldResponse> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(self, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });


        }
    }

    public void sortDealSoldList(int position, boolean isAsc) {
        switch (position) {
            case 0:
                break;
            case 1:
                reserSoldItemList.sort(new Comparator<ReserSoldItem>() {
                    @Override
                    public int compare(ReserSoldItem reserSoldItem, ReserSoldItem t1) {
                        if (isAsc)
                            return Float.compare(reserSoldItem.getDeal().getRate(), t1.getDeal().getRate());
                        else
                            return Float.compare(t1.getDeal().getRate(), reserSoldItem.getDeal().getRate());
                    }
                });
                break;
            case 2:
                reserSoldItemList.sort(new Comparator<ReserSoldItem>() {
                    @Override
                    public int compare(ReserSoldItem dealObj, ReserSoldItem t1) {
                        if (isAsc)
                            return dealObj.getDealName().compareTo(t1.getDealName());
                        else return t1.getDealName().compareTo(dealObj.getDealName());
                    }
                });
                break;
            case 3:
                reserSoldItemList.sort(new Comparator<ReserSoldItem>() {
                    @Override
                    public int compare(ReserSoldItem dealObj, ReserSoldItem t1) {
                        if (isAsc)
                            return Double.compare(dealObj.getPrice(), t1.getPrice());
                        else   return Double.compare(t1.getPrice(), dealObj.getPrice());
                    }
                });
                break;
        }
        mAdapter.notifyDataSetChanged();
    }

    public void postComment(final float rating, final String content, String sellerId, String objectId, String objectType) {
        String from;
        String to;
        if (typeOfSearch.equals(Constants.SOLD)) {
            from = Constants.SELLER;
            to = Constants.BUYER;
        } else {
            from = Constants.BUYER;
            to = Constants.SELLER;
        }
        ModelManager.postReview(self, sellerId, to, from,
                objectId, objectType, content, rating + "", new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        JSONObject jsonObject = (JSONObject) object;
                        ApiResponse response = new ApiResponse(jsonObject);
                        if (!response.isError()) {
                            Toast.makeText(self, R.string.rating_success, Toast.LENGTH_SHORT).show();
                            refreshData();
                        } else {
                            Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError() {
                        Toast.makeText(self, R.string.msg_have_some_errors, Toast.LENGTH_SHORT).show();
                    }
                });
    }


    @Override
    public void onClickPay(int position) {
        ReservationObj item = reservationList.get(position);
        TripFinishingActivity.start(getActivity(), null, item);
    }

    @Override
    public void onClickRate(int position) {
        DealObj item = mDatas.get(position);
        showRatingDialog(item, reservationList.get(position));
    }

    private void checkViewHideShow() {
        if (reserSoldItemList.isEmpty()) {
            llNoData.setVisibility(View.VISIBLE);
        } else {
            llNoData.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        refreshData();
    }

    private void refreshData() {
        mDatas.clear();
        mAdapter.notifyDataSetChanged();
        page = 1;
        onScrollListener.onLoadMoreComplete();
        onScrollListener.setEnded(false);
        getData();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }
}
