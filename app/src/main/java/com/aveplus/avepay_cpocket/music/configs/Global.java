package com.aveplus.avepay_cpocket.music.configs;

import android.content.Context;
import android.view.MenuItem;
import android.view.View;

import com.aveplus.avepay_cpocket.music.listener.IOnMenuItemClick;
import com.aveplus.avepay_cpocket.music.main.addlist.AddListObj;
import com.aveplus.avepay_cpocket.music.main.video2.ObjVideo;
import com.aveplus.avepay_cpocket.music.model.Album;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Global {


    public static void showPopupMenu(Context context, View view, int layout, final IOnMenuItemClick listener){
        android.widget.PopupMenu popup = new android.widget.PopupMenu(context, view);
        popup.getMenuInflater()
                .inflate(layout, popup.getMenu());
        popup.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                listener.onMenuItemClick(item);
                return true;
            }
        });
        popup.show(); //showing popup menu
    }

    public static class HomeSongItemClicked { /* Additional fields if needed */ }
    public static class VideoItemClicked { /* Additional fields if needed */
        public ObjVideo type;
        public VideoItemClicked(ObjVideo video){
            this.type = video;
        }
    }

    public static class SubscribeItemClicked { /* Additional fields if needed */
        public String type;
        public SubscribeItemClicked(String type){
            this.type = type;
        }
    }


    public static class MyMusicItemClicked {
        public LocalItemTypeClick type;
        public MyMusicItemClicked(LocalItemTypeClick type){
            this.type = type;
        }
    }
    public static class ItemAlbumClicked {
        public Album object;
        public  ItemAlbumClicked(Album o){
            this.object = o;
        }
    }

    // event click for view more
    public static class ViewMoreClicked {
        public int position;
        public String object;
        public  ViewMoreClicked(String object, int position){
            this.position = position;
            this.object = object;
        }
    }

    //
    public static class ItemObjectClicked {
        public Object object;
        public int type;
        public  ItemObjectClicked(Object o, int type){
            this.object = o;
            this.type = type;
        }
    }
    //
    public static class ItemChanged { /* Additional fields if needed */}

    //
    public static class NewSongSelected { /* Additional fields if needed */} // for new song is played to play

    public static class QueueSongSelected{ /* Additional fields if needed */} //


    public static enum LocalItemTypeClick{
        TYPE_FAVORITE,
        TYPE_SONG,
        TYPE_ARITST,
        TYPE_DOWNLOADED
    }


    public static class ClickFavoriteAddList {
        public AddListObj type;

        public ClickFavoriteAddList(AddListObj o) {
            this.type = o;
        }
    }

}
