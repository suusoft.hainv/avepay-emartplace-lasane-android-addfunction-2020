package com.aveplus.avepay_cpocket.taxi.view.negociate;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.view.activities.TripPaymentActivity;
import com.aveplus.avepay_cpocket.taxi.view.activities.TripTrackingActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class WaitingDriverEstimateActivity extends BaseActivity {

    private TripObj data;
    private String TAG  = WaitingDriverEstimateActivity.class.getSimpleName();

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_waiting_pay;
    }

    @Override
    protected void getExtraData(Intent intent) {
        data = intent.getParcelableExtra(Args.KEY_TRIP_OJBECT);
        if (data == null)
            finish();

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void onViewCreated() {

    }

    private void getTripDetail() {
        RequestManager.tripDetail(data.getId(), new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (!response.isError()) {

                    data = response.getDataObject(TripObj.class);

                    if (data.isDriverPutTime()){

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Args.KEY_TRIP_OJBECT, data);
                        AppUtil.startActivity(self, WaitingDriverEstimateActivity.class, bundle);
                        finish();

                    }else if (data.isPaymentPending()){

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Args.KEY_TRIP_OJBECT, data);
                        AppUtil.startActivity(self, TripPaymentActivity.class, bundle);
                        finish();

                    }else if (data.isOnGoing()){

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Args.KEY_TRIP_OJBECT, data);
                        AppUtil.startActivity(self, TripTrackingActivity.class, bundle);
                        finish();
                    }
                }
            }

            @Override
            public void onError(String message) {

            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.PushDriverEstimateTime event) {/* Do something */
        //show main fragment

        getTripDetail();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.FinishAllScreenProcessingTrip event) {
        Log.e(TAG, "FinishAllScreenProcessingTrip");

        finish();
    }


}
