package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.AboutAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class Fragment_Gallery extends BaseFragment {

    private FragmentAboutIntroduce frmIntro;
    private ViewPager pager;
    private TabLayout tabs;
    private FragmentAboutGalleries frmGalerry;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_gallery, container,
                false);
        initTabView(view);
        viewData();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
//        viewData();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("about");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("about");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    public void initTabView(View view) {
        pager = (ViewPager) view.findViewById(R.id.pagerSliderAbout);
        tabs = (TabLayout) view.findViewById(R.id.tab_layout);

        if (frmIntro == null) {
            frmIntro = new FragmentAboutIntroduce();
        }
        if (frmGalerry == null) {
            frmGalerry = new FragmentAboutGalleries();
        }
    }

    public void viewData() {
        ArrayList<Fragment> list = new ArrayList<>();
        list.add(frmIntro);
        list.add(frmGalerry);

        pager.setAdapter(new AboutAdapter(getActivity(), getChildFragmentManager(), list));
        tabs.setupWithViewPager(pager);
    }
}
