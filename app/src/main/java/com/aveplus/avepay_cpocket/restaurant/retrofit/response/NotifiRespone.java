package com.aveplus.avepay_cpocket.restaurant.retrofit.response;
/**
 * Created by suusoft.com on 11/12/19.
 */
import com.aveplus.avepay_cpocket.restaurant.main.notification.NotificationModel;

import java.util.ArrayList;

public class NotifiRespone extends BaseModel {
    public ArrayList<NotificationModel> data;

    public ArrayList<NotificationModel> getData() {
        return data;
    }

    public void setData(ArrayList<NotificationModel> data) {
        this.data = data;
    }
}
