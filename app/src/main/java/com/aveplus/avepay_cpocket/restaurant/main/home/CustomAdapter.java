package com.aveplus.avepay_cpocket.restaurant.main.home;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

/**
 * Created by suusoft.com on 11/12/17.
 */

public class CustomAdapter extends PagerAdapter {
    private HomeFrag homeFrag;
    private Integer[] imagesArray;
    private String[] namesArray;

    public CustomAdapter(HomeFrag homeFrag, Integer[] imagesArray, String[] namesArray) {
        this.homeFrag = homeFrag;
        this.imagesArray = imagesArray;
        this.namesArray = namesArray;
    }


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return false;
    }
}
