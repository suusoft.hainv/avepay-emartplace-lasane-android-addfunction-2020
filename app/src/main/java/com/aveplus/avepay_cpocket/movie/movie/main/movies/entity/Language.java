package com.aveplus.avepay_cpocket.movie.movie.main.movies.entity;

/**
 * Copyright © 2019 SUUSOFT
 */
public class Language {
    private int langId;
    private String langName;

    public Language(int langId, String langName) {
        this.langId = langId;
        this.langName = langName;
    }

    public int getLangId() {
        return langId;
    }

    public void setLangId(int langId) {
        this.langId = langId;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }
}
