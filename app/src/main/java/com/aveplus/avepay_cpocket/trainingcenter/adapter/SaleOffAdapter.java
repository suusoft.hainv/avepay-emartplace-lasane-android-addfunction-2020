package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.SaleOff;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class SaleOffAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<SaleOff> objects;
    private LayoutInflater inflater;
   // public int pos;
    private IPromotion mIPromotion;

    public interface IPromotion {
        void showDetail(int pos);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public SaleOffAdapter(Context context, ArrayList<SaleOff> objects, IPromotion mIPromotion) {
        super();
        this.context = context;
        this.objects = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mIPromotion = mIPromotion;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_saleoff, null);
            vh = new ViewHoler();
            vh.txtSaleTitle = (TextView) rowView.findViewById(R.id.txtSaleTitle);
            vh.textDescSale = (TextView) rowView.findViewById(R.id.textDescSale);
            vh.txtReadMore = (TextView) rowView.findViewById(R.id.txtReadMore);
            vh.txtReadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // pos = (int) v.getTag();
                    mIPromotion.showDetail(position);
                }
            });
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // pos = (int) v.getTag();
                    mIPromotion.showDetail(position);
                }
            });
            vh.imgSale = (ImageView) rowView.findViewById(R.id.imageSaleOff);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        final SaleOff c = objects.get(position);
        if (c != null) {
            vh.txtSaleTitle.setText(c.getTitle());
            vh.textDescSale.setText(c.getDescription());
            Picasso.get().load(c.getImage()).into(vh.imgSale);
            vh.txtReadMore.setTag(position);
        }

        return rowView;
    }

    static class ViewHoler {
        public TextView txtSaleTitle, textDescSale;
        public ImageView imgSale;
        public TextView txtReadMore;
    }
}
