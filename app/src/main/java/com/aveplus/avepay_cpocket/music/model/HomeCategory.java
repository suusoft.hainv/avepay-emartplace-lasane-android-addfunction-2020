package com.aveplus.avepay_cpocket.music.model;


import com.aveplus.avepay_cpocket.music.base.model.BaseModel;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public class HomeCategory extends BaseModel {
    public static final String FEATURED = "feature";
    public static final String HOT = "category hot";
    public static final String TOP = "category top";
    public static final String HEADER = "header";
    public static final String ARTIST = "single";
    public static final String CATEGORY = "category";
    public static final String SONGS = "song";
    public static final String NEW = "new";


    public String id, title, name, description, image, type, content;
    private ArrayList<Album> listAlbum;
    private ArrayList<Song> listSongs;
    private ArrayList<Artist> listArtist;
    private ArrayList<Category> listCategory;

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Album> getListAlbum() {
        if (listAlbum == null)
            listAlbum = new ArrayList<>();
        return listAlbum;
    }
    public ArrayList<Category> getListCategory() {
        if (listCategory == null)
            listCategory = new ArrayList<>();
        return listCategory;
    }

    public ArrayList<Artist> getListArtist() {
        if (listArtist == null)
            listArtist = new ArrayList<>();
        return listArtist;
    }

    public void setListData(ArrayList<Album> listData) {
        this.listAlbum = listData;
    }

    public ArrayList<Song> getListSongs() {
            if (listSongs == null)
                listSongs = new ArrayList<>();
            return listSongs;
        }

        public void setListAlbum(ArrayList<Song> listData) {
            this.listSongs = listData;
        }



    public void setName(String name) {
        this.name = name;
    }
    public void setId(String id) {
        this.id = id;
    }
}
