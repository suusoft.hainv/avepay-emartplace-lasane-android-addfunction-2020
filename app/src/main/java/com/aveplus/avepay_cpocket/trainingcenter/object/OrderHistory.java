package com.aveplus.avepay_cpocket.trainingcenter.object;

public class OrderHistory {
	
	public static final String ORDER_STATUS_REJECT = "-2";
	public static final String ORDER_STATUS_CANCEL = "-1";
	public static final String ORDER_STATUS_NEW = "0";
	public static final String ORDER_STATUS_BOOKED = "1";
	public static final String ORDER_STATUS_IN_PROCESS = "2";
	public static final String ORDER_STATUS_CLOSED = "3";
	
	private String orderID;
	private String email;
	private String date;
	private String status;
	
	public OrderHistory() {
		super();
	}

	public OrderHistory(String orderID, String email, String date, String status) {
		super();
		this.orderID = orderID;
		this.email = email;
		this.date = date;
		this.status = status;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
