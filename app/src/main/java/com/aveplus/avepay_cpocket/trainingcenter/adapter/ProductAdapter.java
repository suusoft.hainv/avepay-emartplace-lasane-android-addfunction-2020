package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.listener.IOnItemClickedListener;
import com.aveplus.avepay_cpocket.movie.movie.util.BindingUtil;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHoler> implements Filterable {
    private ArrayList<Product> objects = new ArrayList<Product>();
    private ArrayList<Product> listFilter = new ArrayList<Product>();
    private ProductFilter mFilter = new ProductFilter();

    private IOnItemClickedListener listener;

    private int sizeOfFirstItem;

    public ProductAdapter(Context context, ArrayList<Product> objects) {
        this.objects = objects;
        this.listFilter = objects;
        sizeOfFirstItem = AppUtil.convertDpToPixel(context, 32);
    }


    @Override
    public ViewHoler onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ViewHoler(view);
    }

    @Override
    public void onBindViewHolder(ViewHoler holder, final int position) {
        Product product = objects.get(position);
        if (product != null) {
            holder.bind(product, position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClicked(view, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    public class ViewHoler extends RecyclerView.ViewHolder {
        public TextView txtProName, txtPriceProduct;
        public ImageView imgPro;

        public ViewHoler(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            txtProName = (TextView) itemView.findViewById(R.id.txtProName);
            txtPriceProduct = (TextView) itemView.findViewById(R.id.txtPriceProduct);
            imgPro = (ImageView) itemView.findViewById(R.id.imgPro);
        }

        public void bind(Product item, int position) {
            String arrImg = "http//:google.com.vn";
            if (!item.getImage().isEmpty()) {
                arrImg = item.getImage().get(0).toString();
            }
            if (position == 0) {
                ViewGroup.LayoutParams params = imgPro.getLayoutParams();
                params.height = params.height - sizeOfFirstItem;
                imgPro.setLayoutParams(params);
            }
            txtProName.setText(item.getName());
            txtProName.setSelected(true);
            txtPriceProduct.setText("$" + String.format("%.2f", Float.parseFloat(item.getPrice())));
            BindingUtil.setImage(imgPro, arrImg);
        }

    }

    public IOnItemClickedListener getListener() {
        return listener;
    }

    public void setListener(IOnItemClickedListener listener) {
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ProductFilter extends Filter {

        @SuppressLint("NewApi")
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final ArrayList<Product> list1 = listFilter;
            int count = list1.size();
            final ArrayList<Product> nlist = new ArrayList<Product>(count);
            if (!filterString.isEmpty()) {
                String filterableString;
                for (int i = 0; i < count; i++) {
                    filterableString = list1.get(i).getName();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(listFilter.get(i));
                    }
                }
                if (nlist.size() > 0) {
                    results.values = nlist;
                    results.count = nlist.size();
                } else {
                    results.values = listFilter;
                    results.count = listFilter.size();
                }
            } else {
                results.values = listFilter;
                results.count = listFilter.size();
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count != 0) {
                objects = (ArrayList<Product>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}
