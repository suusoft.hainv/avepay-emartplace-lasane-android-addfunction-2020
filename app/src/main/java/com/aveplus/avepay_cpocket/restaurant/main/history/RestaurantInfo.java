package com.aveplus.avepay_cpocket.restaurant.main.history;

import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestaurantInfo extends BaseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("attachment")
    @Expose
    private String attachment;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("content")
    @Expose
    private Object content;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("sale_price")
    @Expose
    private Integer salePrice;
    @SerializedName("discount")
    @Expose
    private Object discount;
    @SerializedName("discount_rate")
    @Expose
    private Integer discountRate;
    @SerializedName("discount_type")
    @Expose
    private String discountType;
    @SerializedName("discount_expired")
    @Expose
    private Object discountExpired;
    @SerializedName("is_online")
    @Expose
    private Integer isOnline;
    @SerializedName("online_started")
    @Expose
    private Object onlineStarted;
    @SerializedName("online_duration")
    @Expose
    private Integer onlineDuration;
    @SerializedName("is_premium")
    @Expose
    private Integer isPremium;
    @SerializedName("is_renew")
    @Expose
    private Integer isRenew;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("long")
    @Expose
    private String _long;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;
    @SerializedName("country")
    @Expose
    private Object country;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("seller_id")
    @Expose
    private Integer sellerId;
    @SerializedName("view_count")
    @Expose
    private Integer viewCount;
    @SerializedName("like_count")
    @Expose
    private Integer likeCount;
    @SerializedName("is_favourite")
    @Expose
    private Object isFavourite;
    @SerializedName("reservation_count")
    @Expose
    private Integer reservationCount;
    @SerializedName("rate")
    @Expose
    private Double rate;
    @SerializedName("rate_count")
    @Expose
    private Integer rateCount;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("modified_date")
    @Expose
    private String modifiedDate;
    @SerializedName("created_user")
    @Expose
    private Object createdUser;
    @SerializedName("modified_user")
    @Expose
    private Object modifiedUser;
    @SerializedName("balance")
    @Expose
    private Object balance;
    @SerializedName("seller_avatar")
    @Expose
    private Object sellerAvatar;
    @SerializedName("seller_qb_id")
    @Expose
    private Object sellerQbId;
    @SerializedName("pro_data")
    @Expose
    private Prodata proData;

    public Prodata getProData() {
        return proData;
    }

    public void setProData(Prodata proData) {
        this.proData = proData;
    }

    public RestaurantInfo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }

    public Object getDiscount() {
        return discount;
    }

    public void setDiscount(Object discount) {
        this.discount = discount;
    }

    public Integer getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Integer discountRate) {
        this.discountRate = discountRate;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public Object getDiscountExpired() {
        return discountExpired;
    }

    public void setDiscountExpired(Object discountExpired) {
        this.discountExpired = discountExpired;
    }

    public Integer getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Integer isOnline) {
        this.isOnline = isOnline;
    }

    public Object getOnlineStarted() {
        return onlineStarted;
    }

    public void setOnlineStarted(Object onlineStarted) {
        this.onlineStarted = onlineStarted;
    }

    public Integer getOnlineDuration() {
        return onlineDuration;
    }

    public void setOnlineDuration(Integer onlineDuration) {
        this.onlineDuration = onlineDuration;
    }

    public Integer getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(Integer isPremium) {
        this.isPremium = isPremium;
    }

    public Integer getIsRenew() {
        return isRenew;
    }

    public void setIsRenew(Integer isRenew) {
        this.isRenew = isRenew;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String get_long() {
        return _long;
    }

    public void set_long(String _long) {
        this._long = _long;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Object getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(Object isFavourite) {
        this.isFavourite = isFavourite;
    }

    public Integer getReservationCount() {
        return reservationCount;
    }

    public void setReservationCount(Integer reservationCount) {
        this.reservationCount = reservationCount;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getRateCount() {
        return rateCount;
    }

    public void setRateCount(Integer rateCount) {
        this.rateCount = rateCount;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Object getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Object createdUser) {
        this.createdUser = createdUser;
    }

    public Object getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(Object modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Object getBalance() {
        return balance;
    }

    public void setBalance(Object balance) {
        this.balance = balance;
    }

    public Object getSellerAvatar() {
        return sellerAvatar;
    }

    public void setSellerAvatar(Object sellerAvatar) {
        this.sellerAvatar = sellerAvatar;
    }

    public Object getSellerQbId() {
        return sellerQbId;
    }

    public void setSellerQbId(Object sellerQbId) {
        this.sellerQbId = sellerQbId;
    }
}
