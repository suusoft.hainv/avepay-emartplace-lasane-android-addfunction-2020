package com.aveplus.avepay_cpocket.movie.movie.main.movies.entity;

public class MovieBuilder {
    private String movie_id;
    private String movie_name;
    private String movie_video;
    private String movie_sub_title;
    private String movie_thumb;
    private String movie_desc;
    private String movie_year;
    private String movie_imdb;
    private String movie_sourceId;
    private float movie_rating;
    private boolean movie_isFavo = false;
    private String movie_lang;
    private String actors, directors, play_with;

    public MovieBuilder setPlayWith(String play_with) {
        this.play_with = play_with;
        return this;
    }

    public MovieBuilder setMovieActor(String movie_id) {
        this.actors = movie_id;
        return this;
    }

    public MovieBuilder setMovieDirector(String movie_id) {
        this.directors = movie_id;
        return this;
    }

    public MovieBuilder setMovie_id(String movie_id) {
        this.movie_id = movie_id;
        return this;
    }

    public MovieBuilder setMovie_lang(String movie_lang) {
        this.movie_lang = movie_lang;
        return this;
    }

    public MovieBuilder setMovie_name(String movie_name) {
        this.movie_name = movie_name;
        return this;
    }

    public MovieBuilder setMovie_sub_title(String movie_sub_title) {
        this.movie_sub_title = movie_sub_title;
        return this;
    }


    public MovieBuilder setMovie_video(String movie_video) {
        this.movie_video = movie_video;
        return this;
    }

    public MovieBuilder setMovie_thumb(String movie_thumb) {
        this.movie_thumb = movie_thumb;
        return this;
    }

    public MovieBuilder setMovie_desc(String movie_desc) {
        this.movie_desc = movie_desc;
        return this;
    }

    public MovieBuilder setMovie_year(String movie_year) {
        this.movie_year = movie_year;
        return this;
    }

    public MovieBuilder setMovie_imdb(String movie_imdb) {
        this.movie_imdb = movie_imdb;
        return this;
    }

    public MovieBuilder setMovie_sourceId(String movie_sourceId) {
        this.movie_sourceId = movie_sourceId;
        return this;
    }

    public MovieBuilder setMovie_rating(float movie_rating) {
        this.movie_rating = movie_rating;
        return this;
    }

    public MovieBuilder setMovie_isFavo(boolean movie_isFavo) {
        this.movie_isFavo = movie_isFavo;
        return this;
    }

    public Movie createMovie() {
        return new Movie(movie_id, movie_name, movie_video, movie_sub_title, movie_thumb, movie_desc, movie_year, movie_imdb, movie_sourceId,
                movie_rating, movie_isFavo, movie_lang, actors, directors, play_with);
    }
}