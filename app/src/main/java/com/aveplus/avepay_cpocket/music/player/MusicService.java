package com.aveplus.avepay_cpocket.music.player;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.media.MediaBrowserServiceCompat;
import androidx.media.session.MediaButtonReceiver;

import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.suuplayer.SuuPlayer;
import com.aveplus.avepay_cpocket.music.suuplayer.SuuPlayerImpl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class MusicService extends MediaBrowserServiceCompat implements SuuPlayer.EventListener {
    private static final String TAG = MusicService.class.getSimpleName();

    public static final String ACTION_CMD = "com.android.music.ACTION_CMD";
    public static final String CMD_NAME = "CMD_NAME";
    public static final String CMD_PAUSE = "CMD_PAUSE";
    public static final String MEDIA_ID_EMPTY_ROOT = "__EMPTY_ROOT__";
    public static final String MEDIA_ID_ROOT = "__ROOT__";

    private static final int DELAY = 1000;

    public SuuPlayer mPlayer;
    private boolean ringPhone = false;
    public int lengthSong;
    private Handler handler = new Handler();
    private MediaNotificationManager mMediaNotificationManager;
    private MediaSessionCompat mSession;
    private PlaybackStateCompat.Builder mStateBuilder;
    private QueueManager queueManager;

    // for first time click play button
    private boolean isPlayed;

    private MediaSessionCompat.Callback callback = new MediaSessionCompat.Callback() {
        @Override
        public void onPlayFromMediaId(String mediaId, Bundle extras) {
            handlePrepare();
        }

        @Override
        public void onPlay() {
            if (isPlayed) {
                handlePlay();
            } else {
                handlePrepare();
            }
        }

        @Override
        public void onPause() {
            handlePause();
        }

        @Override
        public void onSkipToNext() {
            handleNext();
        }

        @Override
        public void onSkipToPrevious() {
            handlePrev();
        }

        @Override
        public void onSeekTo(long pos) {
            handleSeekTo(pos);
        }
    };


    @Nullable
    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName, int clientUid, @Nullable Bundle rootHints) {
//        if (allowBrowsing(clientPackageName, clientUid)) {
//            return new BrowserRoot(MEDIA_ID_ROOT, null);
//        } else {
        return new BrowserRoot(MEDIA_ID_EMPTY_ROOT, null);
        // }
    }

    @Override
    public void onLoadChildren(@NonNull String parentId, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {
        if (TextUtils.equals(MEDIA_ID_EMPTY_ROOT, parentId)) {
            result.sendResult(null);
            return;
        }
        List<MediaBrowserCompat.MediaItem> mediaItems = new ArrayList();
        if (MEDIA_ID_EMPTY_ROOT.equals(parentId)) {
            result.sendResult(mediaItems);
        } else {
            result.sendResult(mediaItems);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // checkCall();
        initQueueManager();
        initPlayer();
        initMediaSession();
        initNotification();
    }

    @Override
    public int onStartCommand(@Nullable Intent startIntent, int flags, int startId) {
        if (startIntent != null) {
            String action = startIntent.getAction();
            String command = startIntent.getStringExtra(CMD_NAME);
            if (ACTION_CMD.equals(action)) {
                if (CMD_PAUSE.equals(command)) {
                    mPlayer.pause();
                }
            } else {
                // Try to handle the intent as a media button event wrapped by MediaButtonReceiver
                MediaButtonReceiver.handleIntent(mSession, startIntent);
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMediaNotificationManager.stopNotification();
        mSession.release();
        mPlayer.releasePlayer();
        stopForeground(true);

    }


    private void initQueueManager() {
        queueManager = QueueManager.getInstance();
        queueManager.setListener(new QueueManager.IQueueChangeListener() {
            @Override
            public void onItemSet() {
                mSession.setMetadata(queueManager.getMetadataCurrentItem(mPlayer.getDuration()));
                notificationChangeSong();
            }
        });
    }

    private void initPlayer() {
        if (mPlayer == null) {
            mPlayer = new SuuPlayerImpl(this);
            mPlayer.setShouldAutoPlay(false);
            mPlayer.addEventListener(this);
        }
    }

    private void initNotification() {
        try {
            mMediaNotificationManager = new MediaNotificationManager(this);
        } catch (RemoteException e) {
            throw new IllegalStateException("Could not create a MediaNotificationManager", e);
        }
    }

    private void initMediaSession() {
        mSession = new MediaSessionCompat(this, TAG);
        setSessionToken(mSession.getSessionToken());

        mSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        updatePlaybackState();
        mSession.setCallback(callback);
    }


    private void updatePlaybackState() {
        mStateBuilder = new PlaybackStateCompat.Builder()
                .setActions(getAvailableActions());

        int state = getState();
        long position = mPlayer != null ? mPlayer.getCurrentPosition() : 0;
        ;
        mStateBuilder.setState(state, position, 1.0f, SystemClock.elapsedRealtime());

        mSession.setPlaybackState(mStateBuilder.build());

        if (state == PlaybackStateCompat.STATE_PLAYING ||
                state == PlaybackStateCompat.STATE_PAUSED && mMediaNotificationManager != null) {
            onNotificationRequired();
        }
    }

    private long getAvailableActions() {
        long actions =
                PlaybackStateCompat.ACTION_PLAY_PAUSE |
                        PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID |
                        PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH |
                        PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
                        PlaybackStateCompat.ACTION_SKIP_TO_NEXT;
        if (mPlayer.isPlaying()) {
            actions |= PlaybackStateCompat.ACTION_PAUSE;
        } else {
            actions |= PlaybackStateCompat.ACTION_PLAY;
        }
        return actions;
    }

    private void getMetaData() {

    }

    public int getState() {
        if (mPlayer == null) {
            return PlaybackStateCompat.STATE_NONE;
        }

        switch (mPlayer.getState()) {
            case Player.STATE_IDLE:
                return PlaybackStateCompat.STATE_PAUSED;
            case Player.STATE_BUFFERING:
                return PlaybackStateCompat.STATE_BUFFERING;
            case Player.STATE_READY:
                return mPlayer.isPlaying()
                        ? PlaybackStateCompat.STATE_PLAYING
                        : PlaybackStateCompat.STATE_PAUSED;
            case Player.STATE_ENDED:
                return PlaybackStateCompat.STATE_PAUSED;
            default:
                return PlaybackStateCompat.STATE_NONE;
        }
    }

    private void onNotificationRequired() {
        mMediaNotificationManager.startNotification();
    }

    private void notificationChangeSong() {
        EventBus.getDefault().post(new Global.NewSongSelected());
    }

    private void handlePrepare() {
        isPlayed = true;
        Song song = queueManager.getCurrentItem();
        notificationChangeSong();
        if (song != null) {
            startService(new Intent(getApplicationContext(), MusicService.class));
            mPlayer.prepare(song.getSong_file(), SuuPlayer.TYPE_OTHER);
        }
    }


    private void handlePause() {
        mSession.setActive(false);
        stopForeground(false);
        mPlayer.pause();

    }

    private void handlePlay() {
        mSession.setActive(true);
        startService(new Intent(getApplicationContext(), MusicService.class));
        mPlayer.play();
    }

    private void handlePrev() {
        queueManager.prevSong();
        handlePrepare();

    }

    private void handleNext() {
        queueManager.nextSong();
        handlePrepare();
    }

    private void handleSeekTo(long pos) {
        mPlayer.seekTo(pos);
    }


    private void checkCall() {
        PhoneStateListener phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    if (mPlayer != null)
                        if (mPlayer.isPlaying())
                            mPlayer.pause();
                    ringPhone = true;

                } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                    if (ringPhone) {
//                        if (mPlayer != null)
//                            if (!mPlayer.isPlaying() && played) {
//                                resumeSong();
//                            }
                        ringPhone = false;
                    }
                } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    if (mPlayer != null)
                        if (mPlayer.isPlaying())
                            mPlayer.pause();
                    ringPhone = true;
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    public String getLengSong() {
        return getTime(lengthSong);
    }

    private static String getTime(int millis) {
        long second = (millis / 1000) % 60;
        long minute = millis / (1000 * 60);
        return String.format("%02d:%02d", minute, second);
    }

    @Override
    public void onStared() {

    }

    @Override
    public void onPlaying() {
        isPlayed = true;
        mSession.setMetadata(queueManager.getMetadataCurrentItem(mPlayer.getDuration()));
        updatePlaybackState();

    }

    @Override
    public void onPaused() {
        updatePlaybackState();

    }

    @Override
    public void onStopped() {

    }

    @Override
    public void onCompletion() {
        handleNext();
    }

    @Override
    public void onFullScreen(boolean isFullScreen) {

    }

    @Override
    public void onBuffering(boolean var1) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        updatePlaybackState();
    }
}
