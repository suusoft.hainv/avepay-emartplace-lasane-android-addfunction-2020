package com.aveplus.avepay_cpocket.restaurant.retrofit.response;
/**
 * Created by suusoft.com on 11/12/19.
 */

import com.aveplus.avepay_cpocket.restaurant.main.save.savemodels.ListFavourite;

import java.util.ArrayList;

public class ResponeListSave extends BaseModel {
    public ArrayList<ListFavourite> data;


    public ArrayList<ListFavourite> getData() {
        return data;
    }

    public void setData(ArrayList<ListFavourite> data) {
        this.data = data;
    }
}
