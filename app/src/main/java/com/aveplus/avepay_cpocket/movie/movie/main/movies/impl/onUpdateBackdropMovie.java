package com.aveplus.avepay_cpocket.movie.movie.main.movies.impl;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface onUpdateBackdropMovie {
    public void onCompleted(String response);
    public void onError(Exception ex);
}
