package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.TabSilderAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class FragmentSaleOff extends BaseFragment {

    FragmentPromotionRecent frmRecent;
    FragmentPromotionPopular frmPopular;
    FragmentPromotionFeatured frm_feature;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_saleoff, container, false);
        initUI(view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("sale");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void initUI(View view) {
        ViewPager pager = (ViewPager) view.findViewById(R.id.pagerSliderPromotion);
        ArrayList<Fragment> list = new ArrayList<Fragment>();
        frm_feature = new FragmentPromotionFeatured();
        frmPopular = new FragmentPromotionPopular();
        frmRecent = new FragmentPromotionRecent();
        list.add(frmRecent);
        list.add(frmPopular);
        list.add(frm_feature);
        pager.setAdapter(new TabSilderAdapter(getActivity(), getChildFragmentManager(), list));
        TabLayout tabs = (TabLayout) view.findViewById(R.id.tab_layout);
        tabs.setupWithViewPager(pager);
    }
}
