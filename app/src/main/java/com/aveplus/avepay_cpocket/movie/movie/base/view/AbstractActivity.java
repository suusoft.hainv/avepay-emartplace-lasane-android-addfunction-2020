package com.aveplus.avepay_cpocket.movie.movie.base.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.NetworkUtility;
import com.google.android.material.snackbar.Snackbar;


/**
 * Copyright © 2019 SUUSOFT
 */
public abstract class AbstractActivity extends AppCompatActivity {

    public Context self;

    protected FrameLayout contentLayout;
    protected ProgressBar progressBar;
    // toolbar
    protected Toolbar toolbar;
    protected TextView tvTitle;

    public enum ToolbarType {
        MENU_LEFT,// make screen with menu left
        NAVI,    // make screen with toolbar has a button back
        NONE   // none
    }

    /**
     * get toolbar type. Each toolbar type match with a layout
     */
    protected abstract ToolbarType getToolbarType();

    /**
     * This function is called before view is created
     */
    protected abstract void onPrepareCreateView();

    /**
     * Listener network state
     */
    protected abstract void getExtraData(Intent intent);

    private BroadcastReceiver broadCastNetwork = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (NetworkUtility.isNetworkAvailable()) {
                showSnackBar(R.string.msg_network_connected);
            } else {
                showSnackBar(R.string.msg_network_notify);
            }

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self = this;
        regisNetworkListener();
        getExtraData(getIntent());
        onPrepareCreateView();
        setView();
        initViewBase();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadCastNetwork);

    }

    //================================ view ===============================================

    /**
     * At the moment. There are 3 types layout
     */
    private void setView() {
        if (getToolbarType() == ToolbarType.MENU_LEFT) {
            setContentView(R.layout._base_drawer);
            initToolbar();
        } else if (getToolbarType() == ToolbarType.NAVI) {
            setContentView(R.layout._base_nav);
            initToolbar();
        } else if (getToolbarType() == ToolbarType.NONE) {
            setContentView(R.layout._base_content);
        }
    }

    private void initViewBase() {
        contentLayout = (FrameLayout) findViewById(R.id.content_main);
        progressBar = (ProgressBar) findViewById(R.id.progress);
    }

    /**
     * initialize toolbar
     */
    protected void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        if (getToolbarType() == ToolbarType.NAVI) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    /**
     * set title for toolbar
     */
    public void setToolbarTitle(String title) {
        tvTitle.setText(title);
    }

    public void setToolbarTitle(int title) {
        tvTitle.setText(getString(title));
    }


    /**
     * show snack bar message
     *
     * @param message
     */
    public void showSnackBar(int message) {
        Snackbar.make(contentLayout, getString(message), Snackbar.LENGTH_LONG).show();
    }

    public void showSnackBar(String message) {
        Snackbar.make(contentLayout, message, Snackbar.LENGTH_LONG).show();
    }

    /**
     * show hide progress bar on screen
     *
     * @param isShow true is show
     */
    public void showProgress(boolean isShow) {
        if (isShow) {
            if (!progressBar.isShown())
                progressBar.setVisibility(View.VISIBLE);
        } else {
            if (progressBar.isShown())
                progressBar.setVisibility(View.GONE);
        }
    }


    //================================ end view ===========================================

    private void regisNetworkListener() {
        registerReceiver(broadCastNetwork, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    /**
     * start activity
     *
     * @param clz class name
     */
    public void startActivity(Class<?> clz) {
        AppUtil.startActivity(self, clz);
    }

    public void startActivity(Class<?> clz, Bundle bundle) {
        AppUtil.startActivity(self, clz, bundle);
    }



    /**
     * show toast message
     *
     * @param message
     */
    public void showToast(String message) {
        AppUtil.showToast(self, message);
    }

    public void showToast(int message) {
        AppUtil.showToast(self, getString(message));
    }

    /**
     * switch fragment
     *
     * @param tag      tag of fragment
     * @param content  destination view by R.id.
     * @param fragment destination fragment
     */
    public void switchFragment(String tag, int content, Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(content, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }


}
