package com.aveplus.avepay_cpocket.music.main.video;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;


public class ListVideoFragment extends BaseListFragmentBinding {

    private VideoVM viewModel;

    private VideoAdapter mAdapter;
    private CategoryAdapter cAdapter;
    private Genre mGenre;


    public static ListVideoFragment newInstance(Context context, Genre genre) {
        ListVideoFragment listVideoFragment = new ListVideoFragment();
        // mGenre = genre;
        Bundle bundle = new Bundle();
        bundle.putParcelable("GENRE", genre);
        listVideoFragment.setArguments(bundle);
        return listVideoFragment;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        if (mGenre.getId() != 40000) {
            mAdapter = new VideoAdapter(self, viewModel.getListData(), mGenre.getId());
            Log.e("ListVideoFragment", "setUpRecyclerView: " + mGenre.getId());
            recyclerView.setLayoutManager(viewModel.getLayoutManager());
            recyclerView.setAdapter(mAdapter);
            recyclerView.addOnScrollListener(viewModel.getOnScrollListener());
            mAdapter.notifyDataSetChanged();
        } else {
            cAdapter = new CategoryAdapter(self, viewModel.getListData());
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
            recyclerView.setAdapter(cAdapter);
            recyclerView.addOnScrollListener(viewModel.getOnScrollListener());
            cAdapter.notifyDataSetChanged();
        }

    }

    @Override
    protected void initialize() {
        mGenre = getArguments().getParcelable("GENRE");
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new VideoVM(self, mGenre.getId());
        return viewModel;
    }
}
