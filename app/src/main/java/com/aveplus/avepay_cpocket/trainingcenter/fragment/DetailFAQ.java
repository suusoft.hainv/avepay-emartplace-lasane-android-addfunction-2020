package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.object.Services;
import com.squareup.picasso.Picasso;


public class DetailFAQ extends BaseFragment {

    TextView txttitle, txtdesc, txtcontent;
    ImageView imgservice;
    private Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.detail_faq, container, false);
        intiView(view);
        intiData();
        initControl();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("detailservice");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("service");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    public void intiView(View v) {
        txttitle = (TextView) v.findViewById(R.id.txtTitleServicesDetail);
        txtcontent = (TextView) v.findViewById(R.id.tvContentQA);
        imgservice = (ImageView) v.findViewById(R.id.imgServiceDetail);
        txttitle.setSelected(true);


    }

    private void initControl() {

    }

    public void intiData() {
        bundle = this.getArguments();
        Services product = bundle.getParcelable("item");

        txttitle.setText(product.getTitle());
        txtcontent.setText(product.getContent());
        Picasso.get().load(product.getImage()).into(imgservice);
    }
}
