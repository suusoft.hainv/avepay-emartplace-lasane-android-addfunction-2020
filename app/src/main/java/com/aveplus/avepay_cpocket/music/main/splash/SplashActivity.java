package com.aveplus.avepay_cpocket.music.main.splash;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.GlobalFunctions;
import com.aveplus.avepay_cpocket.music.main.main2.MainActivity2;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import static com.aveplus.avepay_cpocket.music.configs.GlobalFunctions.RC_PERMISSIONS;


public class SplashActivity extends AppCompatActivity {

    private Shimmer shimmer;
    private String[] listPermission = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_music);

        shimmer = new Shimmer();
        shimmer.start((ShimmerTextView) findViewById(R.id.shimmer_tv));
        RequestManager.registerDevice(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                Log.e("Register device", response.getMessage());
            }

            @Override
            public void onError(String message) {
                Log.e("Register device", message);

            }
        });

        // Check permissions
        if (GlobalFunctions.isGranted(this, listPermission, RC_PERMISSIONS, "")) {
            onContinue();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSIONS: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED
                            || grantResults[1] == PackageManager.PERMISSION_DENIED) {
                        showPermissionsReminder(RC_PERMISSIONS, true);
                    } else {
                        //permission đã được cấp phép
                        onContinue();
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    protected void showPermissionsReminder(final int reqCode, final boolean flag) {
        GlobalFunctions.showConfirmationDialog(this, getString(R.string.msg_remind_user_grants_permissions),
                getString(R.string.allow), getString(R.string.no_thank), false, new GlobalFunctions.IConfirmation() {
                    @Override
                    public void onPositive() {
                        GlobalFunctions.isGranted(SplashActivity.this, listPermission, reqCode, null);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            finish();
                        }
                    }
                });
    }

    private void onContinue() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AppUtil.startActivity(SplashActivity.this, MainActivity2.class);//MainAct
//                if(DataStoreManager.getUser()!=null){
//                    AppUtil.startActivity(SplashActivity.this, MainActivity2.class);//MainAct
//
//                }
//                else {
//                    AppUtil.startActivity(SplashActivity.this, SplashLoginActivity.class);//MainAct
//                }

                shimmer.cancel();
                finish();

            }
        }, 3000);
    }


}
