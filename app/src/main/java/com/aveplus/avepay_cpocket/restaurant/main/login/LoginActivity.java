package com.aveplus.avepay_cpocket.restaurant.main.login;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.aveplus.avepay_cpocket.R;


public class LoginActivity extends AppCompatActivity {
    private FragmentLogin frmLogin = new FragmentLogin();
    private FragRegister frmRegister = new FragRegister();
    private FragIndex frmIndex = new FragIndex();
    private Fragment[] fragments = {frmLogin, frmRegister};
    private SharedPreferences loginPreferences;
    private InputMethodManager inputMethodManager;
    private FrameLayout frameLayout;

    public InputMethodManager getInputMethodManager() {
        return inputMethodManager;
    }

    public SharedPreferences getLoginPreferences() {
        return loginPreferences;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_restaurant);
        initFragment();
        showFragment(frmIndex);
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public void initFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.panel, frmRegister);
        transaction.add(R.id.panel, frmLogin);
        transaction.add(R.id.panel, frmIndex);
        transaction.commit();
    }

    public void showFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        //hide all fragment
        fragmentTransaction.hide(frmLogin);
        fragmentTransaction.hide(frmIndex);
        //show fragment need display
        fragmentTransaction.show(fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }

    public FragmentLogin getFrmLogin() {
        return frmLogin;
    }

    public FragRegister getFrmRegister() {
        return frmRegister;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void finish() {
        super.finish();
    }
}
