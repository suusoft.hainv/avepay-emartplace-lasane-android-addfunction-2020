package com.aveplus.avepay_cpocket.view.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.parsers.JSONParser;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.view.activities.DealDetailActivity;
import com.aveplus.avepay_cpocket.view.adapters.DealAdapter;
import com.aveplus.avepay_cpocket.widgets.recyclerview.EndlessRecyclerOnScrollListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.aveplus.avepay_cpocket.globals.Constants.COLUM_GIRD_DEAL;
import static com.aveplus.avepay_cpocket.globals.Constants.COLUM_LIST_DEAL;

/**
 * Created by Suusoft on 11/28/2016.
 */

public class DealListFragment extends com.aveplus.avepay_cpocket.base.BaseFragment implements EndlessRecyclerOnScrollListener.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    IListenerDealsChange listenerDealsChange;
    private RecyclerView mRclDeal;
    private DealAdapter mAdapter;
    private ArrayList<DealObj> mDatas;
    private LinearLayout llNoData, llNoConnection;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String dealCateogy;
    private String typeOfSearch;
    private String dealCategoryName, sortType, sortBy;
    private int page = 1;
    private String mKeyWord;
    private String mDistance;
    private String mNumDealPerPage;
    private String isActiveDeal;

    private GridLayoutManager layoutManager;
    private EndlessRecyclerOnScrollListener onScrollListener;

    private boolean isNeedUpdate;

    // receive when need update. from DealAboultFragment
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isNeedUpdate = true;
            int position = intent.getIntExtra(Args.POSITION, -1);
            if (position != -1) {
                boolean isFavourite = intent.getBooleanExtra(Args.IS_FAVORITE, false);
                DealObj item = mDatas.get(position);
                item.setFavorite(isFavourite);
                if (isFavourite) {
                    item.setFavoriteQuantity(item.getFavoriteQuantity() + 1);
                } else {
                    item.setFavoriteQuantity(item.getFavoriteQuantity() + -1);
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    public static DealListFragment newInstance(Bundle args) {

        DealListFragment fragment = new DealListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listenerDealsChange = (IListenerDealsChange) (context);
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.layout_single_list;
    }

    @Override
    protected void init() {

        Bundle bundle = getArguments();
        dealCateogy = bundle.getString(Args.KEY_ID_DEAL_CATE);
        dealCategoryName = bundle.getString(Args.TYPE_OF_DEAL_NAME, "");
        typeOfSearch = bundle.getString(Args.TYPE_OF_SEARCH_DEAL, "");
        mKeyWord = bundle.getString(Args.KEY_KEY_WORD);
        mDistance = bundle.getString(Args.KEY_DISTANCE);
        mNumDealPerPage = bundle.getString(Args.KEY_NUM_DEAL_PER_PAGE);

        isActiveDeal = bundle.getString(Args.KEY_ACTIVE_DEAL);
    }

    @Override
    protected void initView(View view) {
        mRclDeal = (RecyclerView) view.findViewById(R.id.rcv_data);
        llNoData = (LinearLayout) view.findViewById(R.id.ll_no_data);
        llNoConnection = (LinearLayout) view.findViewById(R.id.ll_no_connection);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        if (AppUtil.getWidthDp(getActivity()) > 600) {
            layoutManager = new GridLayoutManager(self, COLUM_GIRD_DEAL);
        } else {
            layoutManager = new GridLayoutManager(self, COLUM_LIST_DEAL);
        }

        onScrollListener = new EndlessRecyclerOnScrollListener(this, layoutManager);
        mRclDeal.setLayoutManager(layoutManager);
        mRclDeal.addOnScrollListener(onScrollListener);

        if (mDatas == null) {
            mDatas = new ArrayList<>();
        } else {
            mDatas.clear();
        }

        mAdapter = new DealAdapter(this, mDatas, typeOfSearch);
        mRclDeal.setAdapter(mAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == DealAdapter.RQ_UPDATE_DEAL) {
            if (resultCode == DealDetailActivity.RC_UPDATE_DEAL) {
                mDatas.clear();

                mAdapter.notifyDataSetChanged();
                page = 1;
                onScrollListener.setEnded(false);
                onScrollListener.setCurrentPage(page);
                getData();

            }
        }
    }

    public void refreshData(ArrayList<DealObj> dealObjList) {
        mDatas = dealObjList;
        mAdapter.notifyDataSetChanged();
    }

    public void sortData(String sortBy, String sortType) {
        this.sortBy = sortBy;
        this.sortType = sortType;
        page = 1;
        if (mDatas != null) mDatas.clear();
        onScrollListener.setEnded(false);
        onScrollListener.setCurrentPage(1);
        getData();
    }

    public void sortDealSoldList(int position, boolean isAsc) {
        switch (position) {
            case 0:
                break;
            case 1:
                Collections.sort(mDatas, new Comparator<DealObj>() {
                    @Override
                    public int compare(DealObj dealObj, DealObj t1) {
                        if (isAsc)
                            return Float.compare(dealObj.getRate(), t1.getRate());
                        else return Float.compare(t1.getRate(), dealObj.getRate());
                    }
                });
                break;
            case 2:
                Collections.sort(mDatas, new Comparator<DealObj>() {
                    @Override
                    public int compare(DealObj dealObj, DealObj t1) {
                        if (isAsc)
                            return dealObj.getName().compareTo(t1.getName());
                        else return  t1.getName().compareTo(dealObj.getName());
                    }
                });
                break;
            case 3:
                Collections.sort(mDatas, new Comparator<DealObj>() {
                    @Override
                    public int compare(DealObj dealObj, DealObj t1) {
                        if (isAsc)
                            return Double.compare(dealObj.getPrice(), t1.getPrice());
                        else return Double.compare(t1.getPrice(), dealObj.getPrice());
                    }
                });
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
//        self.registerReceiver(broadcastReceiver, new IntentFilter(Args.NOTIFY_NEED_UPDATE));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        if (self != null) {
//            self.unregisterReceiver(broadcastReceiver);
//        }

    }

    @Override
    public void onRefresh() {
        mDatas.clear();
        mAdapter.notifyDataSetChanged();
        page = 1;
        onScrollListener.setEnded(false);
        onScrollListener.setCurrentPage(page);
        getData();
    }

    @Override
    protected void getData() {

        if (!onScrollListener.isEnded()) {
            swipeRefreshLayout.setRefreshing(true);
            ModelManager.getDealList(self, mKeyWord, isActiveDeal, dealCateogy, typeOfSearch, mDistance, mNumDealPerPage, AppController.getInstance().getLatMyLocation(),
                    AppController.getInstance().getLongMyLocation(), sortBy, sortType, page, new ModelManagerListener() {
                        @Override
                        public void onSuccess(Object object) {
                            Log.e("DealListFragment", "onSuccess " + object.toString());
                            JSONObject jsonObject = (JSONObject) object;
                            ApiResponse response = new ApiResponse(jsonObject);
                            DealObj dealObj;
                            if (!response.isError()) {
                                Log.e("DealListFragment", "onSuccess response success");
                                mDatas.addAll(response.getDataList(DealObj.class));

                                //cái này sử dụng khi app phải phân biệt các deal active và inactive
                                //                                Date  dateSystem = Calendar.getInstance().getTime(), dateTime;
//                                ArrayList<DealObj> mDatasContainElementEndTime = new ArrayList<>();
//                                mDatasContainElementEndTime.addAll(response.getDataList(DealObj.class));
//                                Log.e("DealListFragment", "onSuccess mDatasContainElementEndTime = " + mDatasContainElementEndTime.size() );

//                                if (isActiveDeal==null) {
//                                    for (int i = 0; i < mDatasContainElementEndTime.size(); i++) {
//                                        dealObj = mDatasContainElementEndTime.get(i);
//                                        //compare time now and end time
//                                        dateTime = DateTimeUtil.convertTimeStampTooDate(Long.parseLong(dealObj.getOnline_started()) + (dealObj.getOnline_duration() * 3600));
//                                        if (dateSystem.before(dateTime))
//                                            mDatas.add(dealObj);
//                                    }
//                                    Log.e("DealListFragment", "isActiveDeal==null mDatas = " +  mDatas.size());
//                                    //List from my deal
//                                }else if (isActiveDeal.equals("0")){
//                                    for (int i = 0; i < mDatasContainElementEndTime.size(); i++) {
//                                        dealObj = mDatasContainElementEndTime.get(i);
//                                        //compare time now and end time
//                                        dateTime = DateTimeUtil.convertTimeStampTooDate(Long.parseLong(dealObj.getOnline_started()) + (dealObj.getOnline_duration() * 3600));
//                                        if (dateSystem.after(dateTime))
//                                            mDatas.add(dealObj);
//                                    }
//                                    Log.e("DealListFragment", "isActiveDeal==0 mDatas = " +  mDatas.size());
//
//
//
//                                }else if (isActiveDeal.equals("1")){
//                                    //list from category
//                                    for (int i = 0; i < mDatasContainElementEndTime.size(); i++) {
//                                        dealObj = mDatasContainElementEndTime.get(i);
//                                        //compare time now and end time
//                                        dateTime = DateTimeUtil.convertTimeStampTooDate(Long.parseLong(dealObj.getOnline_started()) + (dealObj.getOnline_duration() * 3600));
//                                        if (dateSystem.before(dateTime))
//                                            mDatas.add(dealObj);
//                                    }
//                                    Log.e("DealListFragment", "isActiveDeal==1 mDatas = " +  mDatas.size());
//                                }  /*End list from my deal*/
//

                                listenerDealsChange.onChanged(mDatas);
                                mAdapter.notifyDataSetChanged();
                                onScrollListener.onLoadMoreComplete();
                                onScrollListener.setEnded(JSONParser.isEnded(response, page));
                                swipeRefreshLayout.setRefreshing(false);
                                checkViewHideShow();
                            } else {
                                Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                                swipeRefreshLayout.setRefreshing(false);
                            }


                        }

                        @Override
                        public void onError() {
                            Toast.makeText(self, R.string.msg_have_some_errors, Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });
        }
    }

    @Override
    public void onLoadMore(int page) {


        if (!mDatas.isEmpty()) {
            this.page = page;
            getData();
        }
    }

    private void checkViewHideShow() {
        if (mDatas.isEmpty()) {
            llNoData.setVisibility(View.VISIBLE);
        } else {
            llNoData.setVisibility(View.GONE);
        }
    }

    public interface IListenerDealsChange {
        void onChanged(ArrayList<DealObj> mDealObj);
    }

}
