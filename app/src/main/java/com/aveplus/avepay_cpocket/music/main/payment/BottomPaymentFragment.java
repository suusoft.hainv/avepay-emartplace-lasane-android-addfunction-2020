package com.aveplus.avepay_cpocket.music.main.payment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class BottomPaymentFragment extends BottomSheetDialogFragment {
    private IPayement iPayement;
    private View view;

    public BottomPaymentFragment(IPayement iPayement) {
        this.iPayement = iPayement;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bottom_payment2, container, false);
        dishplayListService();
        return view;
    }

    private void dishplayListService() {
        ImageView lnCash = (ImageView) view.findViewById(R.id.ln_payment_cash);
        ImageView lnCredit = (ImageView) view.findViewById(R.id.ln_payment_credits);
        //ImageView imgDownFrag =(ImageView) view.findViewById(R.id.btn_downfrag);
//        imgDownFrag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dismiss();
//            }
//        });
        lnCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iPayement.onPayment("Esawa");
                DataStoreManager.saveStatusPaymentMethod("esewa");
                dismiss();
            }
        });
        lnCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iPayement.onPayment("Khalti");
                DataStoreManager.saveStatusPaymentMethod("khalti");
                dismiss();

            }
        });
    }
}
