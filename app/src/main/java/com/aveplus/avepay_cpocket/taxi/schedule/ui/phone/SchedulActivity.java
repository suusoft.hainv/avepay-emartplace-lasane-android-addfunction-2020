package com.aveplus.avepay_cpocket.taxi.schedule.ui.phone;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.schedule.service.ScheduleClient;

import java.util.Calendar;
import java.util.Locale;

/**
 * This is the Main Activity of our app.
 * Here we allow the user to select a date,
 * we then set a notification for that date to appear in the status bar
 *  
 * @author paul.blundell
 */
public class SchedulActivity extends BaseActivity {
	// This is a handle so that we can call methods on our service
    //private ScheduleClient scheduleClient;
    // This is the date picker used to select the date for our notification
	private DatePicker picker;
	private TimePicker timePicker;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NORMAL;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.schedule_activity_schedule;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {

        // Create a new service client and bind our activity to this service
//        scheduleClient = new ScheduleClient(this);
//        scheduleClient.doBindService();
        Locale locale = getResources().getConfiguration().locale;
        Locale.setDefault(locale);


        ScheduleClient.getInstance().doBindService();

        // Get a reference to our date picker
        picker =  findViewById(R.id.scheduleTimePicker);

        timePicker= findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);



    }

    @Override
    protected void onViewCreated() {
        setToolbarTitle(R.string.book_later);
    }


    /**
     * This is the onClick called from the XML to set a new notification 
     */
    public void onDateSelectedButtonClick(View v){
    	// Get the date from our datepicker
    	int day = picker.getDayOfMonth();
    	int month = picker.getMonth();
    	int year = picker.getYear();

        int hour, minute;
        if (Build.VERSION.SDK_INT >= 23 ){
            hour = timePicker.getHour();
            minute = timePicker.getMinute();
        }
        else{
            hour = timePicker.getCurrentHour();
            minute = timePicker.getCurrentMinute();
        }

    	// Create a new calendar set to the date chosen
    	// we set the time to midnight (i.e. the first minute of that day)
    	Calendar c = Calendar.getInstance();
    	c.set(year, month, day);
    	c.set(Calendar.HOUR_OF_DAY, hour);
    	c.set(Calendar.MINUTE, minute);
    	c.set(Calendar.SECOND, 0);

    	// Ask our service to set an alarm for that date, this activity talks to the client that talks to the service
    	ScheduleClient.getInstance().setAlarmForNotification(c);
    	// Notify the user what they just did
        String timeLater = String.format( getString(R.string.book_later_set_for), hour+ ":" + minute + " \non " +  day +"/"+ (month+1) +"/"+ year );
    	Toast.makeText(this,timeLater , Toast.LENGTH_SHORT).show();

        TripObj tripObj = DataStoreManager.getBookLater();
        tripObj.setTimeBookLater(timeLater);
        DataStoreManager.saveBookLater(tripObj);

    	finish();
    }
    
    @Override
    protected void onStop() {

    	// When our activity is stopped ensure we also stop the connection to the service
    	// this stops us leaking our activity into the system *bad*
//    	if(scheduleClient != null)
//    		scheduleClient.doUnbindService();
    	super.onStop();
    }
}