package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.FragmentDetailsChapterMp3Binding;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.configs.GlobalFunction;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.DetailsBookActivity;
import com.aveplus.avepay_cpocket.ebook.view.adapter.ActorAdapter;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.DetailsChapterMP3VM;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;


public class FragmentDetailsChapterMP3 extends BaseFragmentBinding {
    private static final String TAG = "MP3";
    private VideoView emVideoView;
    private Chapter chapter;
    private RelativeLayout rlVideo;
    private DetailsChapterMP3VM viewModel;
    private FragmentDetailsChapterMp3Binding binding;

    public static FragmentDetailsChapterMP3 newInstance(Chapter chapter, Book book) {
        Bundle args = new Bundle();
        args.putParcelable(Constant.KEY_CHAPTER, chapter);
        args.putParcelable(Constant.KEY_BOOK, book);
        FragmentDetailsChapterMP3 fragment = new FragmentDetailsChapterMP3();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_details_chapter_mp3;
    }

    @Override
    protected void initialize() {
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            chapter = bundle.getParcelable(Constant.KEY_CHAPTER);
            GlobalFunction.addCountReadBook(self, chapter.getBookId());
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new DetailsChapterMP3VM(self, chapter);
        return viewModel;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        this.binding = (FragmentDetailsChapterMp3Binding) binding;
        this.binding.setViewModel(viewModel);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.details_chapter_video_mp3, menu);
        menu.findItem(R.id.action_bookmarks).setVisible(false);
        if (DataStoreManager.isBookMarks(chapter.getBookId(), Constant.LIST_BOOK_MARKS)) {
            menu.findItem(R.id.action_bookmarks).setIcon(R.drawable.ic_book_marks_yellow);
        } else {
            menu.findItem(R.id.action_bookmarks).setIcon(R.drawable.ic_book_marks_white);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bookmarks:
                if (DataStoreManager.isBookMarks(chapter.getBookId(), Constant.LIST_BOOK_MARKS)) {
                    ((DetailsBookActivity) self).deleteBookrMarks();
                    item.setIcon(R.drawable.ic_book_marks_white);
                } else {
                    ((DetailsBookActivity) self).addBookrMarks();
                    item.setIcon(R.drawable.ic_book_marks_yellow);
                }
                return true;
            case R.id.action_comment:
                ((FragmentActivity) self).getSupportFragmentManager().beginTransaction()
                        .add(R.id.content_details, FragmentComment.newInstance(chapter), Constant.FRAGMENT_COMENT)
                        .addToBackStack(null)
                        .commit();
                return true;

        }
        return false;
    }

    @Override
    protected void initView(View view) {
        binding.tvCast.setText(Html.fromHtml("<u>" + getString(R.string.the_cast) + "</u>"));
        rlVideo = binding.rlVideo;
        emVideoView = binding.videoView;
        emVideoView.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared() {
                emVideoView.start();
            }
        });
        emVideoView.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError(Exception e) {
                showDialodDownloadFailed();
                return false;
            }


        });
        AppUtil.setRateFollowWithDevice(rlVideo, 100, 56);

        emVideoView.setVideoURI(Uri.parse(chapter.getAttachment()));
        // emVideoView.getVideoControls().btnFull.setVisibility(View.GONE);
        //emVideoView.getVideoControls().setAnimHide(false);

        if (chapter.getActor() != null && chapter.getActor().size() > 0) {
            ActorAdapter mAdapter = new ActorAdapter(self, chapter.getActor());
            binding.rcvActor.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));
            binding.rcvActor.setAdapter(mAdapter);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        emVideoView.release();
    }

    public void showDialodDownloadFailed() {
        new AlertDialog.Builder(self)
                .setTitle(R.string.app_name)
                .setMessage(R.string.error_LoadBook)
                .setCancelable(false)
                .setNegativeButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        emVideoView.setVideoURI(Uri.parse(chapter.getAttachment()));
                    }
                })
                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((Activity) self).onBackPressed();
                    }
                })
                .create()
                .show();
    }
}

