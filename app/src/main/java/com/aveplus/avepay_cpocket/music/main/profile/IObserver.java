package com.aveplus.avepay_cpocket.music.main.profile;


public interface IObserver {
    void update();
}
