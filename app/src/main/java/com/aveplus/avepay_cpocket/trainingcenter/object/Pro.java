package com.aveplus.avepay_cpocket.trainingcenter.object;

/**
 * Created by SuuSoft on 05/09/2017.
 */

public class Pro {
    private int productId;
    private String productName;
    private String quantity;
    private String price;
    private String classId;

    public Pro(int productId, String productName, String quantity, String price, String classId) {
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;
        this.classId = classId;
    }
}
