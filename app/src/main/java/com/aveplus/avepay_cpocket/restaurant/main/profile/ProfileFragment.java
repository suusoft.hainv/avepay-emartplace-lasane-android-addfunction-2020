package com.aveplus.avepay_cpocket.restaurant.main.profile;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.main.login.ChangePasswordActivity;
import com.aveplus.avepay_cpocket.restaurant.main.login.LoginActivity;
import com.aveplus.avepay_cpocket.restaurant.main.login.models.UserModels;
import com.aveplus.avepay_cpocket.restaurant.main.shop.ShopAct;
import com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeUser;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.util.DialogUtil;
import com.aveplus.avepay_cpocket.restaurant.util.ImageUtil;
import com.aveplus.avepay_cpocket.restaurant.util.StringUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.FileUtils;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.MyProgressDialog;
import com.bumptech.glide.Glide;


import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = ProfileFragment.class.getName();
    private static final int RC_ADDRESS = 134;
    private static final int RQ_CHANGE_PASS = 234;
    private static final String PARAM_AVATAR = "avatar";
    private EditText edtBusinessName, edtPhoneNumber, edtAddress, edtEmail;
    private CircleImageView imgAvatar;
    private ImageView imgEditAvatar;
    private LinearLayout lnOpenShop;
    private ImageView imgSymbolAccount;
    private FrameLayout frDivider;
    private TextView btnEdit;
    private MultipartBody.Part partImage = null;
    private String image = "";
    private File file;
    private RelativeLayout rlChangePass;
    private TextView tvChangePass;
    private TextView tvNumRate;
    private TextView tvPhoneCode, tvEg, tvLogout;
    private RelativeLayout btnSave;
    private TextView tvFunction;
    private RatingBar rating_bar;
    private boolean isEdit = false;
    private String passWord;
    private String mAddress;
    private Bitmap bitmap;
    public static final String LOG_OUT = "LOG_OUT";
    private int PIC_IMAGE_REQUETS = 4;
    private RequestBody token;

    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void init() {
        Log.e(TAG, "logUser: " + DataStoreManager.getUser() );
    }

    @Override
    protected void initView(View view) {
        imgAvatar = view.findViewById(R.id.img_avatar);
        imgEditAvatar = view.findViewById(R.id.btn_edit_avatar);
        tvLogout = view.findViewById(R.id.tv_logout);
        edtBusinessName = view.findViewById(R.id.edt_bussiness_name);
        edtPhoneNumber = view.findViewById(R.id.edt_phone_number);
        edtAddress = view.findViewById(R.id.edt_address);
        edtEmail = view.findViewById(R.id.edt_email);
        tvEg = view.findViewById(R.id.tvEg);
        btnEdit = view.findViewById(R.id.btn_edit_infomation);

        lnOpenShop = view.findViewById(R.id.ln_open_shop);

        rlChangePass = view.findViewById(R.id.rl_change_pass);
        tvChangePass = view.findViewById(R.id.tv_change_pass);

        btnSave = view.findViewById(R.id.btn_functions);
        tvFunction = view.findViewById(R.id.tv_btn);
        frDivider = view.findViewById(R.id.fr_divider);
        tvPhoneCode = view.findViewById(R.id.tv_phone_code);
        btnSave.setVisibility(View.GONE);
        tvFunction.setText(getString(R.string.save));
        getProfile();
        enableEdit(false);
        btnEdit.setOnClickListener(this);
        rlChangePass.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        tvPhoneCode.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        lnOpenShop.setOnClickListener(this);
        imgAvatar.setOnClickListener(this);

        frDivider.setVisibility(View.VISIBLE);
        setData(DataStoreManager.getUserModels());
        edtEmail.setEnabled(false);
//        if (com.pinsetbeauty.bro.network.NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
//            getProfile();
//        } else {
//            AppUtil.showToast(getActivity(), R.string.msg_connection_network_error);
//        }
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
        Log.e(TAG, "profile: " + DataStoreManager.getUser());

        if (DataStoreManager.getUser() != null) {

            ImageUtil.setImage(imgAvatar, DataStoreManager.getUser().getImage());
            edtBusinessName.setText(DataStoreManager.getUser().getName());
            edtEmail.setText(DataStoreManager.getUser().getEmail());
            edtPhoneNumber.setText(DataStoreManager.getUser().getPhone());
            edtAddress.setText(DataStoreManager.getUser().getAddress());
        }

    }

    @Override
    protected void getData() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_logout:
                if (DataStoreManager.getUserModels() != null) {
                    logout();
                } else {
                    AppUtil.showToast(self, "No account");
                }
                break;
            case R.id.btn_edit_infomation:
                isEdit = !isEdit;
                if (DataStoreManager.getUser() != null) {
                    if (isEdit) {

                        btnSave.setVisibility(View.VISIBLE);
                        btnEdit.setBackgroundResource(R.drawable.bg_pressed_radius_accent);
                        edtBusinessName.requestFocus();
                        tvEg.setVisibility(View.VISIBLE);
                    } else {
                        setData(DataStoreManager.getUserModels());
                        btnSave.setVisibility(View.GONE);
                        btnEdit.setBackgroundResource(R.drawable.bg_press_radius_gray);
                        tvEg.setVisibility(View.GONE);
                    }
                    enableEdit(isEdit);
                } else {
                    showDialogLogout();
                }
                break;
            case R.id.btn_functions:
                if (com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
                    if (isEdit) {
                        if (isValid()) {

                            updateProfile();

                        }
                    } else {
                        AppUtil.showToast(getActivity(), R.string.msg_enable_edit);
                    }

                } else {
                    AppUtil.showToast(getActivity(), R.string.msg_no_network);
                }
                break;

            case R.id.rl_change_pass:
                Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivityForResult(intent, RQ_CHANGE_PASS);
                break;
            case R.id.ln_open_shop:
                startActivity(new Intent(self, ShopAct.class));
//                Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
//                startActivityForResult(intent, RQ_CHANGE_PASS);
                break;


            case R.id.img_avatar:
                ImageUtil.pickImage(this, ImageUtil.PICK_IMAGE_REQUEST_CODE);
                break;
        }

    }

    private void logout() {
        showDialogLogout();
    }

    private void showDialogLogout() {
        DialogUtil.showAlertDialog(self, R.string.do_you_logout, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                requestLogout();
            }
        });

    }

    private void requestLogout() {
        if (NetworkUtility.isNetworkAvailable()) {
            final MyProgressDialog progressDialog = new MyProgressDialog(self);
            progressDialog.show();
            processBeforeLoggingOut(progressDialog);
        } else {
            AppUtil.showToast(self, getString(R.string.msg_no_network));
        }
    }

    private void processBeforeLoggingOut(MyProgressDialog progressDialog) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        DataStoreManager.removeUserModels();
        Bundle bundle = new Bundle();
        bundle.putString(LOG_OUT, "log out");
        edtBusinessName.setText(null);
        imgAvatar.setImageDrawable(null);
        edtPhoneNumber.setText(null);
        edtAddress.setText(null);
        edtEmail.setText(null);
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finishAffinity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ImageUtil.PICK_IMAGE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getData() != null) {
                    ImageUtil.setImageFromUri(imgAvatar, data.getData());
                    image = FileUtils.getPath(self, data.getData()).trim();

                    file = new File(image);
                    androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(self);
                    builder.setTitle("Your avatar editing");
                    builder.setCancelable(false);
                    builder.setMessage("Would you like to change your avatar?");
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            parseMultipart();
                            saveAvatar(partImage);
                        }
                    });
                    builder.show();

                }
            }
        }
        if (requestCode == RQ_CHANGE_PASS) {
            if (resultCode == Activity.RESULT_OK) {
                String pass = data.getStringExtra(ChangePasswordActivity.PASS);
                UserModels userObj = DataStoreManager.getUserModels();
                userObj.setPassword(pass);
                DataStoreManager.saveUser(userObj);

            }
        }

//        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
//            Uri uri = data.getData();
//            //uploadFile(uri);
//            imgAvatar.setImageURI(uri);
//        }
    }


    private void parseMultipart() {
        if (!image.isEmpty()) {
            File fileImage1 = new File(image);
            Log.e("TAG", "parseMultipart:1 " + image);
            RequestBody fileReqBody1 = RequestBody.create(MediaType.parse("multipart/form-data"), fileImage1);
            partImage = MultipartBody.Part.createFormData(PARAM_AVATAR, fileImage1.getName(), fileReqBody1);
            token = RequestBody.create(MediaType.parse("text/plain"), DataStoreManager.getToken());

        }
    }


    private void saveAvatar(MultipartBody.Part partIMG) {
        MyProgressDialog myProgressDialog = new MyProgressDialog(self);
        myProgressDialog.show();
        ApiUtils.getAPIService().updateAvatar(token, partIMG).enqueue(new Callback<ResponeUser>() {
            @Override
            public void onResponse(Call<ResponeUser> call, Response<ResponeUser> response) {
                myProgressDialog.cancel();
                if (response.body().isSuccess(getActivity())) {
                    DataStoreManager.saveUser(response.body().getData());
                    AppUtil.showToast(self, "update avatar successful");
                }
            }

            @Override
            public void onFailure(Call<ResponeUser> call, Throwable t) {
                AppUtil.showToast(self, "update avatar fail");
                myProgressDialog.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(self, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void setData(UserModels userObj) {
        if (userObj != null) {
            if (userObj.getAvatar() != null && !userObj.getAvatar().isEmpty()) {
                Glide.with(self).load(userObj.getAvatar()).into(imgAvatar);
            }
            if (userObj.getName() != null) {
                edtBusinessName.setText(userObj.getName());
            }
            if (userObj.getPhone() != null) {
                edtPhoneNumber.setText(userObj.getPhone());
            }
            if (userObj.getAddress() != null) {
                edtAddress.setText(userObj.getAddress());
            }
            if (userObj.getEmail() != null) {
                edtEmail.setText(userObj.getEmail());
            }
//            if (userObj.isSecured()) {
//                tvChangePass.setText(getString(R.string.change_pass));
//            } else {
//                tvChangePass.setText(getString(R.string.button_creat_password));
//            }
        }
    }


    private void getCountryCode() {
        String[] rl = getResources().getStringArray(R.array.CountryCodes);
        int curPosition = AppUtil.getCurentPositionCountryCode(getActivity());
        String phoneCode = rl[curPosition].split(",")[0];
        tvPhoneCode.setText(phoneCode);
    }

    private void enableEdit(boolean isEdit) {
        edtBusinessName.setEnabled(isEdit);
        edtAddress.setEnabled(isEdit);
        edtPhoneNumber.setEnabled(isEdit);
        tvPhoneCode.setEnabled(isEdit);
    }

    private void updateProfile() {
        String token = DataStoreManager.getToken();
        final String bussinessName = edtBusinessName.getText().toString().trim();
        final String address = edtAddress.getText().toString().trim();
        final String phoneNumber = edtPhoneNumber.getText().toString().trim();
        APIService apiService = ApiUtils.getAPIService();
        apiService.update(token, bussinessName, phoneNumber, address).enqueue(new Callback<ResponeUser>() {
            @Override
            public void onResponse(Call<ResponeUser> call, Response<ResponeUser> response) {
                if (response.body().isSuccess()) {
                    UserModels user = response.body().getData();
                    user.setName(bussinessName);
                    user.setPhone(phoneNumber);
                    user.setAddress(address);
                    DataStoreManager.saveUser(user);
                    AppController.getInstance().setUserUpdated(true);
                    enableEdit(false);
                    btnEdit.setBackgroundResource(R.drawable.bg_press_radius_gray);
                    isEdit = false;
                    btnSave.setVisibility(View.GONE);
                    AppUtil.showToast(getActivity(), R.string.msg_update_success);
                    setData(user);
                } else {
                    Toast.makeText(self, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponeUser> call, Throwable t) {

            }
        });
    }

    private void getProfile() {


//        if (DataStoreManager.getUser() != null) {
//            String token = DataStoreManager.getToken();
//            final String passWord = DataStoreManager.getUserModels().getPassword();
//
//            String address = DataStoreManager.getUserModels().getAddress();
//            String phone = DataStoreManager.getUserModels().getPhone();
//            String name = DataStoreManager.getUserModels().getName();
//            if (passWord != null) {
////                if (!response.isError()) {
////                    UserObj userObj = response.getDataObject(UserObj.class);
////                    userObj.setToken(DataStoreManager.getToken());
////                    userObj.setPassWord(passWord);
////                    userObj.setRememberMe(DataStoreManager.getUser().isRememberMe());
////                    DataStoreManager.saveUser(userObj);
////                    AppController.getInstance().setUserUpdated(true);
////                    setData(userObj);
////                } else {
////                    Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
////                }
//            }
//        }
    }

    private boolean isValid() {
        String bussinessName = edtBusinessName.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        String phone = edtPhoneNumber.getText().toString().trim();
        if (StringUtil.isEmpty(bussinessName)) {
            AppUtil.showToast(getActivity(), R.string.msg_fill_full_name);
            edtBusinessName.requestFocus();
            return false;
        }
        if (StringUtil.isEmpty(phone)) {
            AppUtil.showToast(getActivity(), R.string.msg_phone_is_required);
            edtPhoneNumber.requestFocus();
            return false;
        }
        if (StringUtil.isEmpty(address)) {
            AppUtil.showToast(getActivity(), R.string.msg_address_is_required);
            edtAddress.requestFocus();
            return false;
        }
        return true;
    }

    private void uploadFile(Uri fileUri) {
        // create upload service client

        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile(self, fileUri);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getActivity().getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

        // add another part within the multipart request
        String token = DataStoreManager.getToken();
        RequestBody description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);

        // finally, execute the request
        APIService apiService = ApiUtils.getAPIService();
        apiService.updateAvatar(description, body).enqueue(new Callback<ResponeUser>() {
            @Override
            public void onResponse(Call<ResponeUser> call, Response<ResponeUser> response) {
                Log.e("Upload", "success");
                DataStoreManager.saveUser(response.body().getData());
            }

            @Override
            public void onFailure(Call<ResponeUser> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }


}
