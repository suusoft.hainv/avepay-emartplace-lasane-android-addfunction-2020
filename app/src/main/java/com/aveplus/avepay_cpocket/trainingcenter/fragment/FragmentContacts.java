package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ContactUsAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class FragmentContacts extends BaseFragment {
    private TabLayout tabs;
    private ViewPager pager;
    private FragmentContactsSocial frmSocial;
    private FragmentContactsLocation frmLocation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("contact");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_contact_training_center, container, false);
        initViews(rootView);
        viewPages();
        return rootView;
    }

    public void initViews(View view) {
        pager = (ViewPager) view.findViewById(R.id.pagerSliderContact);
        tabs = (TabLayout) view.findViewById(R.id.tab_layout);
        if (frmSocial == null) {
            frmSocial = new FragmentContactsSocial();
        }
        if (frmLocation == null) {
            frmLocation = new FragmentContactsLocation();
        }
    }

    public void viewPages() {
        ArrayList<Fragment> list = new ArrayList<>();
        list.add(frmSocial);
        list.add(frmLocation);
        pager.setAdapter(new ContactUsAdapter(getActivity(), getChildFragmentManager(), list));
        tabs.setupWithViewPager(pager);
    }
}
