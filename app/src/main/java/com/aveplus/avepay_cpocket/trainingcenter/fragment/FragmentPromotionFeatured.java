package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


import androidx.fragment.app.FragmentActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.SaleOffAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.SaleOff;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class FragmentPromotionFeatured extends BaseFragment {

    private ArrayList<SaleOff> listSale = new ArrayList<SaleOff>();
    private ListView lstSale;
    private SaleOffAdapter mAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("sale");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_promotion_featre, container, false);
        initUI(view);
        initData();
        return view;
    }

    public void initUI(View view) {
        lstSale = (ListView) view.findViewById(R.id.lstPromotionFeature);
    }

    public void initData() {
        ModelManager.getListSaleFeature(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                listSale = ParseUtility.parseListSale(response.getRootStr());
                mAdapter = new SaleOffAdapter(getActivity(), listSale, new SaleOffAdapter.IPromotion() {
                    @Override
                    public void showDetail(int pos) {
                        DetailSaleOff fragment = new DetailSaleOff();
                        SaleOff item = listSale.get(pos);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", item.getId());
                        bundle.putString("title", item.getTitle());
                        bundle.putString("description",
                                item.getDescription());
                        bundle.putString("content", item.getContent());
                        bundle.putString("image", item.getImage());
                        fragment.setArguments(bundle);
                        ((FragmentActivity) self).getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(
                                        R.anim.slide_in_left,
                                        R.anim.slide_out_left,
                                        R.anim.slide_in_right,
                                        R.anim.slide_out_left)
                                .replace(R.id.childReplace, fragment,
                                        "detailsale")
                                .addToBackStack(null).commit();
                    }
                });
                lstSale.setAdapter(mAdapter);

                lstSale.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view, int position, long id) {
                        onItemSelected(listSale.get(position));
                    }
                });
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self,message);
            }
        });
    }

    public void onItemSelected(SaleOff item) {

    }
}
