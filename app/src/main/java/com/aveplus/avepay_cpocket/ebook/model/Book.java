package com.aveplus.avepay_cpocket.ebook.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Suusoft on 9/8/2017.
 */
public class Book implements Parcelable {

    private String id;
    private String title;
    private String author;
    private String publisher;
    private String description;
    private String type;
    private String attachment;
    private String categoryId;
    private String status;
    private List<Chapter> listChapterBooks;
    private String image;
    private String dateHistory;
    private String image_banner;
    @SerializedName("last chapter")
    private LastChapter lastChapter;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getAttachment() {
        return attachment;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getStatus() {
        return status;
    }

    public List<Chapter> getListChapterBooks() {
        return listChapterBooks;
    }

    public String getImage() {
        return image;
    }

    public void setDateHistory(String dateHistory) {
        this.dateHistory = dateHistory;
    }

    public String getDateHistory() {
        return dateHistory;
    }

    public LastChapter getLastChapter() {
        return lastChapter;
    }

    public String getImage_banner() {
        return image_banner;
    }

    public void setImage_banner(String image_banner) {
        this.image_banner = image_banner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.author);
        dest.writeString(this.publisher);
        dest.writeString(this.description);
        dest.writeString(this.type);
        dest.writeString(this.attachment);
        dest.writeString(this.categoryId);
        dest.writeString(this.status);
        dest.writeTypedList(this.listChapterBooks);
        dest.writeString(this.image);
        dest.writeString(this.dateHistory);
        dest.writeParcelable(this.lastChapter, flags);
        dest.writeString(this.image_banner);
    }

    public Book() {
    }

    protected Book(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.author = in.readString();
        this.publisher = in.readString();
        this.description = in.readString();
        this.type = in.readString();
        this.attachment = in.readString();
        this.categoryId = in.readString();
        this.status = in.readString();
        this.listChapterBooks = in.createTypedArrayList(Chapter.CREATOR);
        this.image = in.readString();
        this.dateHistory = in.readString();
        this.image_banner = in.readString();
        this.lastChapter = in.readParcelable(LastChapter.class.getClassLoader());
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel source) {
            return new Book(source);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };
}
