package com.aveplus.avepay_cpocket;

import android.app.Application;
import android.location.Location;
import android.util.Log;

import com.aveplus.avepay_cpocket.ebook.model.Category;
import com.aveplus.avepay_cpocket.ebook.network.BaseRequest;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.widgets.textview.TypeFaceUtil;
import com.aveplus.avepay_cpocket.movie.movie.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.quickblox.auth.session.QBSettings;
import com.aveplus.avepay_cpocket.datastore.BaseDataStore;
import com.aveplus.avepay_cpocket.interfaces.IRedeem;
import com.aveplus.avepay_cpocket.network.VolleyRequestManager;
import com.aveplus.avepay_cpocket.objects.DealCateObj;
import com.aveplus.avepay_cpocket.quickblox.conversation.utils.QBResRequestExecutor;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by Suusoft on 1/8/2016.
 */
public class AppController extends Application {
    private String token;


    private static final String TAG = AppController.class.getSimpleName();

    private static AppController instance;

    public static LatLng myLatLng;
    public boolean wasInBackground;
    public boolean isInstantBooking;


    public static boolean fromMainToSellFragment = true;
    private TimerTask mActivityTransitionTimerTask;
    private Timer mActivityTransitionTimer;
    private final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;


    private Location myLocation;
    private ArrayList<DealCateObj> mDealCates;
    private ArrayList<Category> listCategories;

    private QBResRequestExecutor qbResRequestExecutor;

    public static AppController getInstance() {
        return instance;
    }

    public String getToken() {
        if (token == null)
            token = DataStoreManager.getToken();
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    // true if user is updated.
    private boolean isUserUpdated;

    private IRedeem iRedeem;
    private com.aveplus.avepay_cpocket.taxi.interfaces.IRedeem iRedeem2;

    public com.aveplus.avepay_cpocket.taxi.interfaces.IRedeem getiRedeem2() {
        return iRedeem2;
    }

    public void setiRedeem2(com.aveplus.avepay_cpocket.taxi.interfaces.IRedeem iRedeem2) {
        this.iRedeem2 = iRedeem2;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        BaseDataStore.init(this);
        AppUtil.getFacebookKeyHash(this);
        isUserUpdated = true;
        initCredentials();
        ActivityLifecycle.init(this);
        DataStoreManager.init(getApplicationContext());
        VolleyRequestManager.init(this);

        //ebook
        DataStoreManager.init(getApplicationContext());
        com.aveplus.avepay_cpocket.ebook.network.VolleyRequestManager.init(getApplicationContext());
        BaseRequest.init(this);
        TypeFaceUtil.getInstant().initTypeFace(this);

        //Music
        com.aveplus.avepay_cpocket.music.datastore.BaseDataStore.init(this);
        com.aveplus.avepay_cpocket.music.network.VolleyRequestManager.init(this);

//        Taxi
        com.aveplus.avepay_cpocket.taxi.datastore.BaseDataStore.init(this);
        com.aveplus.avepay_cpocket.taxi.network.VolleyRequestManager.init(this);
        Places.initialize(getApplicationContext(), getString(R.string.own_google_api_key));

        // Create a new Places client instance
        PlacesClient placesClient = Places.createClient(this);

//        Restaurant
        com.aveplus.avepay_cpocket.restaurant.network.VolleyRequestManager.init(this);
        com.aveplus.avepay_cpocket.restaurant.datastore.BaseDataStore.init(this);
        com.aveplus.avepay_cpocket.restaurant.network.BaseRequest.getInstance();

        init();
    }

    public Location getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(Location myLocation) {
        this.myLocation = myLocation;
    }

    // Init Quickblox credentials
    private void initCredentials() {
        QBSettings.getInstance().init(getApplicationContext(),
                getResources().getString(R.string.QB_APP_ID),
                getResources().getString(R.string.QB_AUTH_KEY),
                getResources().getString(R.string.QB_AUTH_SECRET));
        QBSettings.getInstance().setAccountKey(
                getResources().getString(R.string.QB_ACCOUNT_KEY));
        QBSettings.getInstance().setEnablePushNotification(true);
    }


    public Double getLatMyLocation() {
        if (myLocation == null)
            return 0.0;
        return myLocation.getLatitude();
    }

    public Double getLongMyLocation() {
        if (myLocation == null)
            return 0.0;
        return myLocation.getLongitude();
    }

    public ArrayList<DealCateObj> getDealCategories() {
        if (mDealCates == null) {
            mDealCates = new ArrayList<>();
            // init and get categories of deal

//            DealCateObj dealCateObj = new DealCateObj(DealCateObj.MY_FAVORITES, this.getString(R.string.my_favorites),this.getString(R.string.des_my_favourite));
//            mDealCates.add(dealCateObj);
//            dealCateObj = new DealCateObj(DealCateObj.FOOD_AND_BEVERAGES, this.getString(R.string.food_beverages),this.getString(R.string.des_foodandbever));
//            mDealCates.add(dealCateObj);
//            dealCateObj = new DealCateObj(DealCateObj.LABOR, this.getString(R.string.labor), R.drawable.ic_labor_white );
//            mDealCates.add(dealCateObj);
//            dealCateObj = new DealCateObj(DealCateObj.TRAVEL, this.getString(R.string.travel_hotel), R.drawable.ic_hotel_white_24dp);
//            mDealCates.add(dealCateObj);
//            dealCateObj = new DealCateObj(DealCateObj.SHOPPING, this.getString(R.string.shopping), R.drawable.ic_shop_white_24dp);
//            mDealCates.add(dealCateObj);
//            dealCateObj = new DealCateObj(DealCateObj.NEWS_AND_EVENTS, this.getString(R.string.news_and_events), R.drawable.ic_event_white_24dp);
//            mDealCates.add(dealCateObj);
//            dealCateObj = new DealCateObj(DealCateObj.OTHER, this.getString(R.string.other_deals), R.drawable.ic_other_white_24dp );
//            mDealCates.add(dealCateObj);

            DealCateObj dealCateObj = new DealCateObj(DealCateObj.LABOR, this.getString(R.string.labor), R.drawable.bg_item_labor, this.getString(R.string.description_labor));
            mDealCates.add(dealCateObj);
            dealCateObj = new DealCateObj(DealCateObj.TRAVEL, this.getString(R.string.travel_hotel), R.drawable.bg_item_hotel, this.getString(R.string.description_hotel));
            mDealCates.add(dealCateObj);
            dealCateObj = new DealCateObj(DealCateObj.SHOPPING, this.getString(R.string.shopping), R.drawable.bg_item_shopping, this.getString(R.string.description_shopping));
            mDealCates.add(dealCateObj);
            dealCateObj = new DealCateObj(DealCateObj.NEWS_AND_EVENTS, this.getString(R.string.news_and_events), R.drawable.bg_item_new_event, this.getString(R.string.description_beauty));
            mDealCates.add(dealCateObj);
            dealCateObj = new DealCateObj(DealCateObj.FOOD_AND_BEVERAGES, this.getString(R.string.food_beverages), this.getString(R.string.des_foodandbever));
            mDealCates.add(dealCateObj);
            dealCateObj = new DealCateObj(DealCateObj.OTHER, this.getString(R.string.other_deals), R.drawable.bg_item_orther, this.getString(R.string.description_orther));
            mDealCates.add(dealCateObj);

        }
        return mDealCates;
    }

    public ArrayList<DealCateObj> getListCateForCreateNewDeals() {

        ArrayList<DealCateObj> mDealCates = new ArrayList<>();

        DealCateObj dealCateObj = new DealCateObj("0", this.getString(R.string.chose_category), R.drawable.bg_item_labor, this.getString(R.string.description_labor));
        mDealCates.add(dealCateObj);
        dealCateObj = new DealCateObj(DealCateObj.LABOR, this.getString(R.string.labor), R.drawable.bg_item_labor, this.getString(R.string.description_labor));
        mDealCates.add(dealCateObj);
        dealCateObj = new DealCateObj(DealCateObj.FOOD_AND_BEVERAGES, this.getString(R.string.food_beverages), this.getString(R.string.des_foodandbever));
        mDealCates.add(dealCateObj);
        dealCateObj = new DealCateObj(DealCateObj.TRAVEL, this.getString(R.string.travel_hotel), R.drawable.bg_item_hotel, this.getString(R.string.description_hotel));
        mDealCates.add(dealCateObj);
        dealCateObj = new DealCateObj(DealCateObj.SHOPPING, this.getString(R.string.shopping), R.drawable.bg_item_shopping, this.getString(R.string.description_shopping));
        mDealCates.add(dealCateObj);
        dealCateObj = new DealCateObj(DealCateObj.NEWS_AND_EVENTS, this.getString(R.string.news_and_events), R.drawable.bg_item_new_event, this.getString(R.string.description_beauty));
        mDealCates.add(dealCateObj);
        dealCateObj = new DealCateObj(DealCateObj.OTHER, this.getString(R.string.other_deals), R.drawable.bg_item_orther, this.getString(R.string.description_orther));
        mDealCates.add(dealCateObj);

        return mDealCates;
    }

    public void setDealCategories(ArrayList<DealCateObj> dealCategories) {
        this.mDealCates = dealCategories;
    }

    public synchronized QBResRequestExecutor getQbResRequestExecutor() {
        return qbResRequestExecutor == null
                ? qbResRequestExecutor = new QBResRequestExecutor()
                : qbResRequestExecutor;
    }

    public void init() {
        RequestManager.init(this);
    }

    public boolean isUserUpdated() {
        return isUserUpdated;
    }

    public void setUserUpdated(boolean userUpdated) {
        isUserUpdated = userUpdated;
    }

    public IRedeem getiRedeem() {
        return iRedeem;
    }

    public void setiRedeem(IRedeem iRedeem) {
        this.iRedeem = iRedeem;
    }

    public ArrayList<Category> getListCategories() {
        if (listCategories == null)
            listCategories = new ArrayList<>();
        return listCategories;
    }

    public void setListCategories(ArrayList<Category> listCategories) {
        this.listCategories = listCategories;
    }

    public static void setMyLatLng(LatLng latLng) {
        myLatLng = latLng;
    }

    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        wasInBackground = false;
    }

    //[Start] dành cho việc check status foreground and background
    public void startActivityTransitionTimer() {
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                wasInBackground = true;
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                MAX_ACTIVITY_TRANSITION_TIME_MS);
    }


    public static void onResume() {
        if (instance.wasInBackground) {
            //Do specific came-here-from-background code
        }
        instance.stopActivityTransitionTimer();
    }

    public static void onPause() {
        instance.startActivityTransitionTimer();
    }


}
