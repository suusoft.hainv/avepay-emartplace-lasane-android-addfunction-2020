package com.aveplus.avepay_cpocket.trainingcenter.object;

public class Contact {
    String id, companyName, address, phone, email, website;
    Double lat, ilong;
    private String facebook;
    private String twitter;

    public Contact(String id, String companyName, String address, String phone,
                   String email, String website, Double lat, Double ilong) {
        super();
        this.id = id;
        this.companyName = companyName;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.website = website;
        this.lat = lat;
        this.ilong = ilong;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public Contact() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getIlong() {
        return ilong;
    }

    public void setIlong(Double ilong) {
        this.ilong = ilong;
    }

}
