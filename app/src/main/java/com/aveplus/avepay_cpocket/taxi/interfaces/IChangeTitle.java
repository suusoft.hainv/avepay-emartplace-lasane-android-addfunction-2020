package com.aveplus.avepay_cpocket.taxi.interfaces;

/**
 * Created by SuuSoft.com on 10/05/2017.
 */

public interface IChangeTitle {
    void onChangTitle(String title);
}
