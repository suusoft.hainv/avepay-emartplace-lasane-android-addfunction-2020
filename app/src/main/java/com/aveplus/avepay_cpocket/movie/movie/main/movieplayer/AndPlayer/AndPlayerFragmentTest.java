package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.AndPlayer;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.mediarouter.app.MediaRouteButton;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.SuuPlayer;
import com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.Youtube;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.YoutubeCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.model.BaseYTParser;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AndPlayerFragmentTest#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AndPlayerFragmentTest extends Fragment implements View.OnKeyListener, View.OnClickListener, View.OnFocusChangeListener {


    Context mContext;

    public interface OnOptionsKeyPressListener {
        public void onPressCaptionButton();

        public void onPressInfoButton();
    }

    private final String TAG = getClass().getSimpleName();

    // the fragment initialization parameter
    private static final String EXTRA_VIDEO = "video";

    // Value of what variable in message
    private static final int HIDE_CONTROLLER = 1;
    private static final int SHOW_CONTROLLER = 2;
    private static final int UPDATE_PROGRESS = 3;
    private static final int UPDATE_SUBTITLE = 4;
    private static final int STOP_SUBTITLE = 5;

    private static final int DISPLAY_TIMEOUT = 3000;

    private Movie mMovie;
    private List<String> mVideoSources;
    private int indexSource;

    private RelativeLayout loEmVideo;
    private VideoView emVideoView;

    // views
    private AndVideoView mVideoView;
    private RelativeLayout mMediaController;
    private ImageButton mPlayPauseBtn, mCaptionBtn, mInfoBtn, mCastBtn;
    private TextView mNameTxtView, mYearTxtView, mImdbTxtView, mCurrentTimeTxtView, mDurationTxtView;
    private SeekBar mTimeSeekBar;
    private ProgressBar mProgressBar;

    // MediaPlayer
    private OnOptionsKeyPressListener mOptionsKeyPressListener;
    private boolean mEnableMediaController = true;
    private boolean mDraggingByUser = false;
    private MediaPlayer mPlayer;

    View view;

    private MediaRouteButton mMediaRouteButton;
    private CastSession mCastSession;
    private SuuPlayer player;
    private CastContext mCastContext;
    private MediaInfo mSelectedMedia;
    private SessionManagerListener<CastSession> mSessionManagerListener;
    private View loController;


    // cast new

    // private AbsSubtitleReader mSubtitleReader;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HIDE_CONTROLLER:
                    setVisibleMediaController(View.INVISIBLE);
                    removeMessages(UPDATE_PROGRESS);
                    break;

                case SHOW_CONTROLLER:
                    setVisibleMediaController(View.VISIBLE);
                    sendEmptyMessage(UPDATE_PROGRESS);
                    break;

                case UPDATE_PROGRESS:
                    //if (mMovie.isSourceM3U8()){
                    updateProgressM3U8();
//                    }else {
//                        updateProgressVideo();
//                    }


                    break;

                case UPDATE_SUBTITLE:
                    try {
//                        if (mSubtitleReader != null && mPlayer != null) {
//                            mCaptionTxtView.setText(mSubtitleReader.readTextWithTime(mPlayer.getCurrentPosition()));
//                            sendEmptyMessage(UPDATE_SUBTITLE);
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    private void updateProgressM3U8() {
        try {
            if (emVideoView != null && mMediaController.getVisibility() == View.VISIBLE) {
                Log.e(TAG, "Drag by user: " + mDraggingByUser);
                if (!mDraggingByUser) {
                    int position = (int) emVideoView.getCurrentPosition();
                    mCurrentTimeTxtView.setText(convertIntToTime(position));
                    int progress = (int) ((float) position / emVideoView.getDuration() * mTimeSeekBar.getMax());
                    mTimeSeekBar.setProgress(progress);
                }
                mHandler.sendEmptyMessageDelayed(UPDATE_PROGRESS, 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateProgressVideo() {
        try {
            if (mPlayer != null && mMediaController.getVisibility() == View.VISIBLE) {
                Log.e(TAG, "Drag by user: " + mDraggingByUser);
                if (!mDraggingByUser) {
                    int position = mPlayer.getCurrentPosition();
                    mCurrentTimeTxtView.setText(convertIntToTime(position));
                    int progress = (int) ((float) position / mPlayer.getDuration() * mTimeSeekBar.getMax());
                    mTimeSeekBar.setProgress(progress);
                }
                mHandler.sendEmptyMessageDelayed(UPDATE_PROGRESS, 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            mPlayer = mp;
            if (mPlayer != null)
                mPlayer.setOnInfoListener(mInfoListener);
            if (mProgressBar != null)
                mProgressBar.setVisibility(View.GONE);

            int duration = mp.getDuration();
            mDurationTxtView.setText(convertIntToTime(duration));
            mMediaController.setEnabled(true);

            mVideoView.start();
            showMediaController();
        }
    };

    private MediaPlayer.OnErrorListener mErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            if (mProgressBar != null)
                mProgressBar.setVisibility(View.GONE);

            if (indexSource < mVideoSources.size() - 1) {
                playVideo(indexSource + 1);
                return true;
            }

            return false;
        }
    };

    private MediaPlayer.OnInfoListener mInfoListener = new MediaPlayer.OnInfoListener() {
        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            // Todo
            return false;
        }
    };

    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {


            // Todo
        }
    };

    private AndVideoView.OnPlayPauseListener mPlayPauseListener = new AndVideoView.OnPlayPauseListener() {
        @Override
        public void onPlay() {
            mPlayPauseBtn.setImageResource(R.drawable.btn_big_pause);
            mHandler.sendEmptyMessage(UPDATE_PROGRESS);
        }

        @Override
        public void onPause() {
            mPlayPauseBtn.setImageResource(R.drawable.btn_big_play);
            mHandler.removeMessages(UPDATE_PROGRESS);
        }
    };

    private SeekBar.OnSeekBarChangeListener mTimeSeekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // update time text
            if (!fromUser)
                return;

            progressChanged(seekBar, progress, fromUser);

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mDraggingByUser = true;
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            onTrackingTouch(seekBar);

        }
    };

    private void progressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        //if (mMovie.isSourceM3U8()){

        long duration = emVideoView.getDuration();
        int position = (int) ((float) progress / seekBar.getMax() * duration);
        mCurrentTimeTxtView.setText(convertIntToTime(position));
        showMediaController();

//        }else {
//            int duration = mVideoView.getDuration();
//            int position = (int) ((float)progress / seekBar.getMax() * duration);
//            mCurrentTimeTxtView.setText(convertIntToTime(position));
//            showMediaController();
//        }
    }

    private void onTrackingTouch(SeekBar seekBar) {
        //if (mMovie.isSourceM3U8()){
        long duration = emVideoView.getDuration();
        int position = (int) ((float) seekBar.getProgress() / seekBar.getMax() * duration);
        emVideoView.seekTo(position);
        mDraggingByUser = false;

//        }else {
//            int duration = mVideoView.getDuration();
//            int position = (int) ((float)seekBar.getProgress() / seekBar.getMax() * duration);
//            mVideoView.seekTo(position);
//            mDraggingByUser = false;
//        }
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param movie movie for play
     * @return A new instance of fragment AndPlayerFragment.
     */
    public static AndPlayerFragmentTest newInstance(Movie movie) {
        AndPlayerFragmentTest fragment = new AndPlayerFragmentTest();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_VIDEO, movie);
        fragment.setArguments(args);
        return fragment;
    }

    public static AndPlayerFragmentTest newInstance1() {
        AndPlayerFragmentTest fragment = new AndPlayerFragmentTest();
        return fragment;
    }

    public AndPlayerFragmentTest() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMovie = getArguments().getParcelable(EXTRA_VIDEO);

        }

    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_and_player, container, false);

        // Make sure to use the correct VideoView import
        loEmVideo = view.findViewById(R.id.lo_em_video);
//        emVideoView = view.findViewById(R.id.exomedia);
        emVideoView.setOnPreparedListener(onPreparedListener);
        emVideoView.setOnErrorListener(onErrorListener);
        emVideoView.setOnCompletionListener(onCompletionListener);


        mVideoView = (AndVideoView) view.findViewById(R.id.videoview);
        mMediaController = (RelativeLayout) view.findViewById(R.id.controller_movies_play);
        mPlayPauseBtn = (ImageButton) view.findViewById(R.id.btn_play_pause);
        mCaptionBtn = (ImageButton) view.findViewById(R.id.btn_cc);
        mInfoBtn = (ImageButton) view.findViewById(R.id.btn_info);

        mNameTxtView = (TextView) view.findViewById(R.id.txtview_name_movie);
        mYearTxtView = (TextView) view.findViewById(R.id.txtview_year_movie);
        mImdbTxtView = (TextView) view.findViewById(R.id.txtview_imdb_movie);
        mCurrentTimeTxtView = (TextView) view.findViewById(R.id.txtview_time_current);
        mDurationTxtView = (TextView) view.findViewById(R.id.txtview_time_total);
        mTimeSeekBar = (SeekBar) view.findViewById(R.id.seekbar_time);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        // setup font TextViews
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-Md.otf");
        mNameTxtView.setTypeface(typeface);
        mYearTxtView.setTypeface(typeface);
        mImdbTxtView.setTypeface(typeface);

        // setup VideoView
        mVideoView.setOnPreparedListener(mPreparedListener);
        mVideoView.setOnErrorListener(mErrorListener);
        mVideoView.setOnCompletionListener(mCompletionListener);
        mVideoView.setPlayPauseListener(mPlayPauseListener);

        // touch event listener
        view.setOnClickListener(this);
        mPlayPauseBtn.setOnClickListener(this);
        mCaptionBtn.setOnClickListener(this);
        mInfoBtn.setOnClickListener(this);


        // press key event listener
        view.setOnKeyListener(this);
        mPlayPauseBtn.setOnKeyListener(this);
        mCaptionBtn.setOnKeyListener(this);
        mInfoBtn.setOnKeyListener(this);
        mTimeSeekBar.setOnKeyListener(this);

        mTimeSeekBar.setOnSeekBarChangeListener(mTimeSeekBarListener);

        mPlayPauseBtn.setOnFocusChangeListener(this);
        mCaptionBtn.setOnFocusChangeListener(this);
        mInfoBtn.setOnFocusChangeListener(this);
        mTimeSeekBar.setOnFocusChangeListener(this);

        // focus directions
        mPlayPauseBtn.setNextFocusRightId(R.id.btn_cc);
        mCaptionBtn.setNextFocusDownId(R.id.btn_info);
        mInfoBtn.setNextFocusUpId(R.id.btn_cc);
        mInfoBtn.setNextFocusDownId(R.id.seekbar_time);
        mPlayPauseBtn.setNextFocusDownId(R.id.seekbar_time);
        mTimeSeekBar.setNextFocusUpId(R.id.btn_play_pause);

        if (mMovie != null)
            loadMovie();

        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        mPlayPauseBtn.requestFocus();
        //cast

//        mCastContext = CastContext.getSharedInstance(view.getContext());
//        mMediaRouteButton = view.findViewById(R.id.btn_cast);
//        CastButtonFactory.setUpMediaRouteButton(view.getContext(),mMediaRouteButton);
        initCastTV(view);
        return view;
    }

    //cast

    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

    private PlaybackLocation mLocation;


    private void initCastTV(View view) {
        setupCastListener();
        mMediaRouteButton = view.findViewById(R.id.btn_cast);
        Context context = getActivity();
        CastButtonFactory.setUpMediaRouteButton(context, mMediaRouteButton);

        mCastContext = CastContext.getSharedInstance(context);
        mCastSession = mCastContext.getSessionManager().getCurrentCastSession();
    }

    private void setupCastListener() {
        mSessionManagerListener = new SessionManagerListener<CastSession>() {

            @Override
            public void onSessionEnded(CastSession session, int error) {
                Log.e("cast", "onSessionEnded " + session + " " + error);
                onApplicationDisconnected();
            }

            @Override
            public void onSessionResumed(CastSession session, boolean wasSuspended) {
                Log.e("cast", "onSessionResumed " + wasSuspended);
                onApplicationConnected(session);
            }

            @Override
            public void onSessionResumeFailed(CastSession session, int error) {
                Log.e("cast", "onSessionResumeFailed " + session + " " + error);
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarted(CastSession session, String sessionId) {
                Log.e("cast", "onSessionStarted " + session + " " + sessionId);
                onApplicationConnected(session);
            }

            @Override
            public void onSessionStartFailed(CastSession session, int error) {
                Log.e("cast", "onSessionStartFailed " + session + " " + error);
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarting(CastSession session) {
                Log.e("cast", "onSessionStarting " + session);
            }

            @Override
            public void onSessionEnding(CastSession session) {
                Log.e("cast", "onSessionEnding " + session);
            }

            @Override
            public void onSessionResuming(CastSession session, String sessionId) {
                Log.e("cast", "onSessionResuming " + session + " " + sessionId);
            }

            @Override
            public void onSessionSuspended(CastSession session, int reason) {
                Log.e("cast", "onSessionSuspended " + session + " " + reason);
            }

            private void onApplicationConnected(CastSession castSession) {
                Log.e("cast", "onApplicationConnected");
                mCastSession = castSession;

//                if (null != mSelectedMedia) {
//
//                    if (mPlaybackState == PlaybackState.PLAYING) {
//                        mVideoView.pause();
//                        loadRemoteMedia(mSeekbar.getProgress(), true);
//                        return;
//                    } else {
//                        mPlaybackState = PlaybackState.IDLE;
//                        updatePlaybackLocation(PlaybackLocation.REMOTE);
//                    }
//                }
//                updatePlayButton(mPlaybackState);
//                invalidateOptionsMenu();
                if (null != mSelectedMedia) {

                    if (player.isPlaying()) {
                        player.pause();
                        loadRemoteMedia((int) player.getCurrentPosition(), true);
                        return;
                    } else {
                        //mPlaybackState = PlaybackState.IDLE;
                        updatePlaybackLocation(PlaybackLocation.REMOTE);
                    }
                }
                updatePlayButton();
                //invalidateOptionsMenu();
            }

            private void onApplicationDisconnected() {
                updatePlaybackLocation(PlaybackLocation.LOCAL);
                //mPlaybackState = PlaybackState.IDLE;
                mLocation = PlaybackLocation.LOCAL;
                updatePlayButton();
                //invalidateOptionsMenu();
            }
        };
    }

    private void loadRemoteMedia(int position, boolean autoPlay) {
        if (mCastSession == null) {
            return;
        }
        final RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
        if (remoteMediaClient == null) {
            return;
        }
//        remoteMediaClient.registerCallback(new RemoteMediaClient.Callback() {
//            @Override
//            public void onStatusUpdated() {
//                Intent intent = new Intent(self, ExpandedControlsActivity.class);
//                startActivity(intent);
//                remoteMediaClient.unregisterCallback(this);
//            }
//        });

        remoteMediaClient.addListener(new RemoteMediaClient.Listener() {
            @Override
            public void onStatusUpdated() {

            }

            @Override
            public void onMetadataUpdated() {

            }

            @Override
            public void onQueueStatusUpdated() {

            }

            @Override
            public void onPreloadStatusUpdated() {

            }

            @Override
            public void onSendingRemoteMediaRequest() {

            }

            @Override
            public void onAdBreakStatusUpdated() {

            }
        });

        remoteMediaClient.load(mSelectedMedia,
                new MediaLoadOptions.Builder()
                        .setAutoplay(autoPlay)
                        .setPlayPosition(position).build());
    }

    private void updatePlaybackLocation(PlaybackLocation location) {
        mLocation = location;
//        if (location == PlaybackLocation.LOCAL) {
//            if (mPlaybackState == PlaybackState.PLAYING
//                    || mPlaybackState == PlaybackState.BUFFERING) {
//                setCoverArtStatus(null);
//                startControllersTimer();
//            } else {
//                stopControllersTimer();
//                setCoverArtStatus(MediaUtils.getImageUrl(mSelectedMedia, 0));
//            }
//        } else {
//            stopControllersTimer();
//            setCoverArtStatus(MediaUtils.getImageUrl(mSelectedMedia, 0));
//            updateControllersVisibility(false);
//        }
        if (location == PlaybackLocation.LOCAL) {
//            if (player.isPlaying()) {
//                setCoverArtStatus(null);
//                startControllersTimer();
//            } else {
//                stopControllersTimer();
//                setCoverArtStatus(MediaUtils.getImageUrl(mSelectedMedia, 0));
//            }
        } else {
//            stopControllersTimer();
//            setCoverArtStatus(MediaUtils.getImageUrl(mSelectedMedia, 0));
            updateControllersVisibility(false);
        }
    }

    // should be called from the main thread
    private void updateControllersVisibility(boolean show) {
        if (show) {
            //getSupportActionBar().show();
            loController.setVisibility(View.VISIBLE);
        } else {
//            if (!Utils.isOrientationPortrait(this)) {
//                getSupportActionBar().hide();
//            }
            loController.setVisibility(View.INVISIBLE);
        }
        //doLayout();
    }

    private void updatePlayButton() {
        Log.e(TAG, "Controls: PlayBackState: " + player.isPlaying());
        boolean isConnected = (mCastSession != null)
                && (mCastSession.isConnected() || mCastSession.isConnecting());
        loController.setVisibility(isConnected ? View.GONE : View.VISIBLE);
//        mPlayCircle.setVisibility(isConnected ? View.GONE : View.VISIBLE);
//        switch (state) {
//            case PLAYING:
//                mLoading.setVisibility(View.INVISIBLE);
//                mPlayPause.setVisibility(View.VISIBLE);
//                mPlayPause.setImageDrawable(
//                        getResources().getDrawable(R.drawable.ic_av_pause_dark));
//                mPlayCircle.setVisibility(isConnected ? View.VISIBLE : View.GONE);
//                break;
//            case IDLE:
//                mPlayCircle.setVisibility(View.VISIBLE);
//                mControllers.setVisibility(View.GONE);
//                mCoverArt.setVisibility(View.VISIBLE);
//                mVideoView.setVisibility(View.INVISIBLE);
//                break;
//            case PAUSED:
//                mLoading.setVisibility(View.INVISIBLE);
//                mPlayPause.setVisibility(View.VISIBLE);
//                mPlayPause.setImageDrawable(
//                        getResources().getDrawable(R.drawable.ic_av_play_dark));
//                mPlayCircle.setVisibility(isConnected ? View.VISIBLE : View.GONE);
//                break;
//            case BUFFERING:
//                mPlayPause.setVisibility(View.INVISIBLE);
//                mLoading.setVisibility(View.VISIBLE);
//                break;
//            default:
//                break;
//        }
    }


    private OnPreparedListener onPreparedListener = new OnPreparedListener() {
        @Override
        public void onPrepared() {
            //Starts the video playback as soon as it is ready
            emVideoView.start();
            if (mProgressBar != null)
                mProgressBar.setVisibility(View.GONE);
            int duration = (int) emVideoView.getDuration();
            mDurationTxtView.setText(convertIntToTime(duration));
            mPlayPauseBtn.setImageResource(R.drawable.btn_big_pause);
            mHandler.sendEmptyMessage(UPDATE_PROGRESS);

        }
    };

    private OnErrorListener onErrorListener = new OnErrorListener() {
        @Override
        public boolean onError(Exception e) {

            if (mProgressBar != null)
                mProgressBar.setVisibility(View.GONE);

            return true;
        }
    };

    private OnCompletionListener onCompletionListener = new OnCompletionListener() {
        @Override
        public void onCompletion() {
            Log.e(TAG, "onCompletion ");
            emVideoView.seekTo(0);
            emVideoView.restart();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        showMediaController();
    }

    @Override
    public void onPause() {
        super.onPause();
        //emVideoView.pause();
        //mPlayPauseBtn.setImageResource(R.drawable.btn_big_play);
        //mHandler.removeMessages(UPDATE_PROGRESS);
    }

    @Override
    public void onDestroy() {
        mHandler.removeMessages(HIDE_CONTROLLER);
        mHandler.removeMessages(UPDATE_PROGRESS);
        mHandler.removeMessages(UPDATE_SUBTITLE);
        super.onDestroy();
    }

    public void setMovie(Movie movie) {
        if (movie == null || TextUtils.isEmpty(movie.getMovie_video())) {
            Log.i(TAG, "Invalid video Id: " + movie);
            return;
        }

        mMovie = movie;
        Log.e(TAG, "setMovie :" + new Gson().toJson(movie));
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            bundle.putParcelable(EXTRA_VIDEO, mMovie);
        }

        if (this.isInLayout()) {
            // stop current movie
            mVideoView.stopPlayback();
            emVideoView.stopPlayback();
            loadMovie();
        }
    }

    public void setOnOptionsKeyPressListener(OnOptionsKeyPressListener optionsKeyPressListener) {
        mOptionsKeyPressListener = optionsKeyPressListener;
    }

    public void setEnableMediaController(boolean enable) {
        mEnableMediaController = enable;
        if (enable) {
            this.getView().requestFocus();
        } else {
            mHandler.sendEmptyMessage(HIDE_CONTROLLER);
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN)
            return false;

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (view.equals(mPlayPauseBtn)) {
                    playPauseMovie();
                    return true;
                } else if (view.equals(mCaptionBtn)) {
                    showConfigCaptionView();
                    return true;
                } else if (view.equals(mInfoBtn)) {
                    showInfoView();
                    return true;
                }
                break;

            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (view.equals(mTimeSeekBar) && mPlayer != null) {
                    int currentPosition = mVideoView.getCurrentPosition();
                    int duration = mVideoView.getDuration();
                    int changedPosition = currentPosition;
                    if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                        changedPosition += 10000;
                        changedPosition = changedPosition < duration ? changedPosition : duration;
                    } else {
                        changedPosition -= 10000;
                        changedPosition = changedPosition > 500 ? changedPosition : 0;
                    }
                    mVideoView.seekTo(changedPosition);
                }
                break;

            default:
                break;
        }

        // visible when press any key
        showMediaController();
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play_pause:
                playPauseMovie();
                break;

            case R.id.btn_cc:
                showConfigCaptionView();
                break;

            case R.id.btn_info:
                showInfoView();
                break;

            case R.id.seekbar:
                break;

            default:
                // visible when click any where
                showMediaController();
                break;
        }
    }


    private void loadMovie() {


        Spannable nameSpan = new SpannableString(mMovie.getMovie_name());
        nameSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, nameSpan.length(), 0);
        mNameTxtView.setText(nameSpan);

        Spannable yearSpan = new SpannableString("YEAR: " + mMovie.getMovie_year());
        yearSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 0, 210, 255)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mYearTxtView.setText(yearSpan);

        Spannable imdbSpan = new SpannableString("IMDB: " + mMovie.getMovie_rating());
        imdbSpan.setSpan(new ForegroundColorSpan(Color.argb(255, 238, 177, 30)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mImdbTxtView.setText(imdbSpan);

        mCurrentTimeTxtView.setText("00:00");
        mDurationTxtView.setText("00:00");
        mTimeSeekBar.setProgress(0);

        // Extract video sources
        mProgressBar.setVisibility(View.VISIBLE);


        checkLoadSource();

    }

    private void checkLoadSource() {
        if (mMovie.isSourceM3U8()) {
            loadSrcM3U8();
        } else {
            loadSrcYoutube();
        }
    }

    private void loadSrcM3U8() {
        Log.e(TAG, "loadSrcM3U8");
        //Enter your m3u8 URL below

        emVideoView.setVideoURI(Uri.parse(mMovie.getMovie_video()));
        Log.e(TAG, "video " + mMovie.getMovie_video());

    }

    private void loadSrcYoutube() {
        Log.e(TAG, "loadSrcYoutube");

        Youtube.getInstance(mMovie.getMovie_video()).extractStreaming(new YoutubeCallback() {
            @Override
            public void onCompleted(List<BaseYTParser.VideoStream> videoStreams) {
                if (videoStreams != null && videoStreams.size() > 0) {
                    mVideoSources = new ArrayList<>();
                    for (BaseYTParser.VideoStream stream : videoStreams) {
                        mVideoSources.add(stream.url.toString());
                    }
                    Log.e(TAG, "onCompleted" + " URL: " + videoStreams.get(0).url.toString());

                    //findLocalSubtitleForMovies(mMovie.getMovie_name());
                    playVideo(0);
                }
            }

            @Override
            public void onFailed(boolean b) {
                // failed to extract video sources
                Context context = getActivity();
                if (context == null)
                    return;

                if (mProgressBar != null)
                    mProgressBar.setVisibility(View.GONE);
                Log.e(TAG, "onFailed ");
                Toast.makeText(context, "Error1", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void playVideo(int indexSource) {
        if (indexSource >= 0 && indexSource < mVideoSources.size()) {
            // Reset all
            mMediaController.setEnabled(false);

            this.indexSource = indexSource;
            mPlayer = null;
            emVideoView.setVideoURI(Uri.parse(mVideoSources.get(indexSource)));
            Log.e("Playing Video", "INDEX : " + indexSource);
        }
    }

    private void playPauseMovie() {

        // if (mMovie.isSourceM3U8()){
        playPauseM3U8();
//        }else {
//            playPauseVideoOther();
//        }

    }

    private void playPauseM3U8() {
        if (emVideoView == null)
            return;

        if (emVideoView.isPlaying()) {
            emVideoView.pause();
            mPlayPauseBtn.setImageResource(R.drawable.btn_big_play);
            mHandler.sendEmptyMessage(UPDATE_PROGRESS);
        } else {
            emVideoView.start();
            mPlayPauseBtn.setImageResource(R.drawable.btn_big_pause);
            mHandler.removeMessages(UPDATE_PROGRESS);
        }
    }

    private void playPauseVideoOther() {
        if (mPlayer == null)
            return;

        if (mPlayer.isPlaying()) {
            mVideoView.pause();
        } else {
            mVideoView.start();
        }
    }

    private void showConfigCaptionView() {
        if (mOptionsKeyPressListener != null)
            mOptionsKeyPressListener.onPressCaptionButton();
    }

    private void showInfoView() {
        if (mOptionsKeyPressListener != null)
            mOptionsKeyPressListener.onPressInfoButton();
    }

    private void showMediaController() {
        showMediaController(DISPLAY_TIMEOUT);
    }

    private void showMediaController(int displayTimeOut) {
        if (!mEnableMediaController)
            return;

        if (displayTimeOut < 500)
            displayTimeOut = 0;

        mHandler.removeMessages(HIDE_CONTROLLER);
        mHandler.sendEmptyMessage(SHOW_CONTROLLER);
        mHandler.sendEmptyMessageDelayed(HIDE_CONTROLLER, displayTimeOut);
    }

    private void setVisibleMediaController(int visibility) {
        if (mMediaController != null && visibility != mMediaController.getVisibility()) {
            mMediaController.setVisibility(visibility);
            if (visibility == View.VISIBLE) {
                mPlayPauseBtn.requestFocus();
            }
            // Focus MediaController View when it is enable
            else if (mEnableMediaController) {
                this.getView().requestFocus();
            }
        }
    }

    private String convertIntToTime(int time) {
        time = time / 1000;
        int h = time / 3600;
        int m = (time - h * 3600) / 60;
        int s = (time - h * 3600 - m * 60);
        return String.format("%02d:%02d:%02d", h, m, s);
    }


}


