package com.aveplus.avepay_cpocket.restaurant.main.login;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;


public class FragIndex extends BaseFragment implements View.OnClickListener, View.OnTouchListener {
    private TextView tvSingup, tvSingin, tvGuilde;


    @Override
    protected int getLayoutInflate() {
        return R.layout.logologin_activity;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        tvSingin = view.findViewById(R.id.tv_singin);
        tvSingup = view.findViewById(R.id.tv_singup);
        tvGuilde = view.findViewById(R.id.guide);

        tvGuilde.setOnClickListener(this);
        tvSingin.setOnClickListener(this);
        tvSingup.setOnClickListener(this);
        tvSingin.setOnTouchListener(this);
        tvSingup.setOnTouchListener(this);
        tvGuilde.setOnTouchListener(this);
    }

    @Override
    protected void getData() {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_singin:
                LoginActivity login = (LoginActivity) getActivity();
                login.showFragment(login.getFrmLogin());
                break;

            case R.id.tv_singup:
                LoginActivity lg = (LoginActivity) getActivity();
                lg.showFragment(lg.getFrmRegister());
                break;
            case R.id.guide:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bazzaramadan.com/"));
                startActivity(browserIntent);
                break;
        }
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.tv_singin:

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tvSingin.setBackgroundResource(R.drawable.shap_hover);
                    tvSingin.setTextColor(Color.BLACK);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    tvSingin.setBackgroundResource(R.drawable.shap_border_white);
                    tvSingin.setTextColor(Color.WHITE);
                }

                break;
            case R.id.tv_singup:

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tvSingup.setBackgroundResource(R.drawable.shap_hover);
                    tvSingup.setTextColor(Color.BLACK);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    tvSingup.setBackgroundResource(R.drawable.shap_border_white);
                    tvSingup.setTextColor(Color.WHITE);
                }

                break;
            case R.id.guide:
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    tvGuilde.setTextColor(Color.BLACK);
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    tvGuilde.setTextColor(Color.WHITE);
                }
                break;
        }
        return false;
    }
}
