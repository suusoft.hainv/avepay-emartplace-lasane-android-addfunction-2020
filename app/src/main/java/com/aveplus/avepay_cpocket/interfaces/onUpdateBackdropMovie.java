package com.aveplus.avepay_cpocket.interfaces;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface onUpdateBackdropMovie {
    public void onCompleted(String response);
    public void onError(Exception ex);
}
