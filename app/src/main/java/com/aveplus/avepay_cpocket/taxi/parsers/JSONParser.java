package com.aveplus.avepay_cpocket.taxi.parsers;

import android.util.Log;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.taxi.base.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.globals.Constants;
import com.aveplus.avepay_cpocket.taxi.objects.RevenueObj;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.book.DriverObj;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.google.android.gms.maps.model.LatLng;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SuuSoft.com on 10/07/2015.
 */
public class JSONParser {

    private static final String TAG = JSONParser.class.getSimpleName();

    public static final int ERROR_CODE_NOERROR = 0;
    public static final int ERROR_CODE_UNKOWN = -9000;
    public static final int ERROR_CODE_JSON_ERROR = -9001;
    public static final int ERROR_CODE_INVALID_RESPONSE_FORMAT = -9002;
    public static final int ERROR_CODE_REQUEST_TIMEOUT = -9003;
    public static final int ERROR_CODE_NOT_ENOUGH_MONEY = 412;
    public static final int ERROR_CODE_TOKEN_MISS_MATCHES = 205;



    // Common keys
    private static final String KEY_STATUS = "status";
    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_TOTAL_PAGE = "total_page";
    private static final String VALUE_SUCCESS = "success";
    private static final String KEY_CODE = "code";

    // Keys
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String IMAGE = "image";
    private static final String AVATAR = "avatar";
    private static final String DESCRIPTION = "description";
    private static final String TIME_WAITING = "time_waiting";
    private static final String DATE_TIME = "date";
    private static final String STATUS = "status";
    private static final String TYPE = "type";
    private static final String DRIVER_TYPE = "driver_type";
    private static final String DRIVER_IS_DELIVERY = "driver_is_delivery";
    private static final String LAST_NAME = "last_name";
    private static final String ROOM_NO = "room_no";
    private static final String URL = "url";
    private static final String LATITUDE = "lat";
    private static final String LONGITUDE = "long";
    private static final String RATE = "rate";
    private static final String ESTIMATE_FARE = "estimate_fare";
    private static final String FARE = "fare";
    private static final String ACTUAL_FARE = "actual_fare";
    private static final String RATE_COUNT = "rate_count";
    private static final String IS_DELIVERY = "is_delivery";
    private static final String EMAIL = "email";
    private static final String BUSINESS_PHONE = "business_phone";
    private static final String QB_ID = "qb_id";
    private static final String DURATION = "duration";
    private static final String ESTIMATE_DURATION = "estimate_duration";
    private static final String DISTANCE = "distance";
    private static final String ESTIMATE_DISTANCE = "estimate_distance";
    private static final String START_LOCATION = "start_location";
    private static final String END_LOCATION = "end_location";
    private static final String CREATED_DATE = "created_date";
    private static final String PAYMENT_METHOD = "payment_method";
    private static final String PAYMENT_STATUS = "payment_status";
    private static final String REVENUE = "revenue";
    private static final String EXPENSE = "expense";
    private static final String TIME = "time";
    private static final String START_LAT = "start_lat";
    private static final String START_LONG = "start_long";
    private static final String END_LAT = "end_lat";
    private static final String END_LONG = "end_long";
    private static final String DRIVER_ID = "driver_id";
    private static final String DRIVER_QB_ID = "driver_qb_id";
    private static final String PASSENGER_QB_ID = "passenger_qb_id";
    private static final String PASSENGER_ID = "passenger_id";
    private static final String DRIVER_NAME = "driver_name";
    private static final String DRIVER_PHONE = "driver_phone";
    private static final String FRIEND_ID = "friend_id";
    private static final String PHONE = "phone";

    public static boolean responseIsSuccess(JSONObject response) {
        return response.optString(KEY_STATUS).equalsIgnoreCase(VALUE_SUCCESS);
    }

    public static boolean isTokenMissmatch(JSONObject response) {
        return response.optInt(KEY_CODE)== (ERROR_CODE_TOKEN_MISS_MATCHES);
    }

    public static String getMessage(JSONObject response) {
        return response.optString(KEY_MESSAGE);
    }

    public static int getTotalPage(JSONObject response) {
        return response.optInt(KEY_TOTAL_PAGE);
    }

    public static boolean isEnded(ApiResponse response, int curPage) {
        int totalPage = Integer.parseInt(response.getValueFromRoot(Constants.TOTAL_PAGE));
        return totalPage == 0 || curPage >= totalPage;
    }


    public static ArrayList<TripObj> parseDrivers(JSONObject jsonObject) {
        ArrayList<TripObj> arr = new ArrayList<>();

        try {
            JSONArray datum = jsonObject.getJSONArray(KEY_DATA);
            if (datum.length() > 0) {
                for (int i = 0; i < datum.length(); i++) {
                    JSONObject obj = datum.getJSONObject(i);

                    TripObj tripObj = new TripObj();
                    tripObj.setDriverName(obj.optString(NAME));
                    tripObj.setTransportType(obj.optString(TYPE));
                    tripObj.setDuration(obj.optInt(DURATION));
                    tripObj.setDistance(obj.optDouble(DISTANCE));

                    LatLng latLngDriver = new LatLng(obj.optDouble(LATITUDE), obj.optDouble(LONGITUDE));
                    tripObj.setDriverId(obj.optString(ID));
                    tripObj.setLatLngDriver(latLngDriver);
                    tripObj.setRateOfDriver((float) obj.optDouble(RATE));
                    tripObj.setRateQuantity(obj.optInt(RATE_COUNT));
                    tripObj.setDriverIsDelivery(obj.optString(IS_DELIVERY).equals("1"));
                    tripObj.setDriverEmail(obj.optString(EMAIL));
                    tripObj.setDriverPhone(obj.optString(BUSINESS_PHONE));
                    tripObj.setDriverQBId(obj.optInt(QB_ID));

                    tripObj.setRouteDistance(jsonObject.optDouble(DISTANCE));
                    tripObj.setRouteDuration(jsonObject.optInt(DURATION));
                    Log.e("json", "TripObj = " + jsonObject.toString()  );
                    float estimateFare = 0;

                    if (obj.optString(FARE)==null || obj.optString(FARE).equals("")){

                    }else {
                        estimateFare = (float) (obj.optDouble(FARE) * (jsonObject.optDouble(DISTANCE) / 1000));
                    }

                    tripObj.setEstimateFare(estimateFare);

                    arr.add(tripObj);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arr;
    }

    public static ArrayList<TripObj> parseTrips(JSONObject jsonObject) {
        ArrayList<TripObj> arr = new ArrayList<>();

        try {
            JSONArray datum = jsonObject.getJSONArray(KEY_DATA);
            if (datum.length() > 0) {
                for (int i = 0; i < datum.length(); i++) {
                    JSONObject obj = datum.getJSONObject(i);

                    TripObj tripObj = new TripObj();
                    tripObj.setId(obj.optString(ID));
                    tripObj.setActualFare((float) obj.optDouble(ACTUAL_FARE));
                    tripObj.setEstimateFare((float) obj.optDouble(ESTIMATE_FARE));

                    tripObj.setPickup(obj.optString(START_LOCATION));
                    LatLng latLngPickup = new LatLng(obj.optDouble(START_LAT), obj.optDouble(START_LONG));
                    tripObj.setLatLngPickup(latLngPickup);

                    tripObj.setDestination(obj.optString(END_LOCATION));
                    LatLng latLngDestination = new LatLng(obj.optDouble(END_LAT), obj.optDouble(END_LONG));
                    tripObj.setLatLngDestination(latLngDestination);

                    tripObj.setDriverId(obj.optString(DRIVER_ID));
                    tripObj.setDriverQBId(obj.optInt(DRIVER_QB_ID));
                    tripObj.setPassengerId(obj.optString(PASSENGER_ID));
                    tripObj.setPassengerQBId(obj.optInt(PASSENGER_QB_ID));
                    tripObj.setDriverName(obj.optString(DRIVER_NAME));
                    tripObj.setDriverPhone(obj.optString(DRIVER_PHONE));
                    tripObj.setStatus(obj.optString(STATUS));
                    tripObj.setTime(obj.optLong(TIME));
                    tripObj.setDistance(0);
                    tripObj.setRouteDistance(obj.optDouble(ESTIMATE_DISTANCE));
                    tripObj.setRouteDuration(obj.optInt(ESTIMATE_DURATION));
                    tripObj.setPaymentMethod(obj.optString(PAYMENT_METHOD));
                    tripObj.setPaymentStatus(obj.optString(PAYMENT_STATUS));
                    tripObj.setTransportType(obj.optString(DRIVER_TYPE));
                    tripObj.setDriverIsDelivery(obj.optString(DRIVER_IS_DELIVERY).equals("1"));

                    arr.add(tripObj);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arr;
    }

    public static ArrayList<RevenueObj> parseRevenues(JSONObject jsonObject) {
        ArrayList<RevenueObj> arr = new ArrayList<>();

        try {
            JSONArray datum = jsonObject.getJSONArray(KEY_DATA);
            if (datum.length() > 0) {
                for (int i = 0; i < datum.length(); i++) {
                    JSONObject obj = datum.getJSONObject(i);

                    RevenueObj revenueObj = new RevenueObj(Float.parseFloat(obj.optString(REVENUE)),
                            Float.parseFloat(obj.optString(EXPENSE)));

                    arr.add(revenueObj);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return arr;
    }


    public static DriverObj getDriver(JSONObject jsonObject) {
        DriverObj driverObj = new DriverObj();
        try {
            JSONArray datum = jsonObject.getJSONArray(KEY_DATA);
            if (datum.length()>0){

                for (int i = 0; i < datum.length(); i++) {
                    JSONObject obj = datum.getJSONObject(i);

                    driverObj.setId(obj.getString("id"));
                    driverObj.setName(obj.getString("name"));
                    driverObj.setPhone(obj.getString("phone"));
                    driverObj.setEmail(obj.getString("email"));
                    driverObj.setTrip_id(obj.getString("trip_id"));
                    LatLng latLng = new LatLng(obj.optDouble(LATITUDE), obj.optDouble(LONGITUDE));
                    driverObj.setLatLng(latLng);

                    driverObj.setRate(obj.optDouble(RATE));
                    driverObj.setRate_count(obj.optDouble(RATE_COUNT));
                    driverObj.setType(obj.getString(TYPE));
                    driverObj.setDistance(obj.optDouble(DISTANCE));
                    driverObj.setDuration(obj.optInt(DURATION));
                    driverObj.setEstimate_fare(obj.optInt(ESTIMATE_FARE));
                }

            }else {
                driverObj=null;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return  driverObj;
    }


    public static TripObj getTripStatus(JSONObject jsonObject) {

        TripObj tripObj = new TripObj();
        try {
            JSONObject json = jsonObject.getJSONObject(KEY_DATA);

            tripObj.setId(json.optString(ID));
            tripObj.setPrice(json.optDouble(ACTUAL_FARE));
            tripObj.setCreated_date(json.optString(CREATED_DATE));
            tripObj.setPassengerId(json.optString("passenger_id"));
            tripObj.setPassengerPhone(json.getString("passenger_phone"));
            tripObj.setPassengerAvatar(json.getString("passenger_avatar"));
            tripObj.setPassengerConfirm(json.optString("passenger_confirm"));
            tripObj.setPassengerRate(json.optString("passenger_rated"));
            tripObj.setPassengerFinished(json.optString("passenger_finished"));

            LatLng latLngStart = new LatLng(json.optDouble(START_LAT), json.getDouble(START_LONG));
            LatLng latLngEnd = new LatLng(json.optDouble(END_LAT), json.getDouble(END_LONG));

            tripObj.setLatLngPickup(latLngStart);
            tripObj.setLatLngDestination(latLngEnd);

            tripObj.setDriverId(json.optString(DRIVER_ID));
            tripObj.setDriverAvatar(json.optString("driver_avatar"));
            tripObj.setDriverName(json.optString(DRIVER_NAME));
            tripObj.setDriverPhone(json.optString(DRIVER_PHONE));
            tripObj.setDriverType(json.optString("driver_type"));
            tripObj.setDriverFinished(json.optString("driver_finished"));


            tripObj.setEstimateDistance(json.optDouble(ESTIMATE_DISTANCE));
            tripObj.setEstimateFare(json.optDouble(ESTIMATE_FARE));

            DriverObj driverObj = new DriverObj();
            driverObj.setUser_id(json.optInt(DRIVER_ID));
            driverObj.setAvatar(json.optString("driver_avatar"));
            driverObj.setName(json.optString(DRIVER_NAME));
            driverObj.setPhone(json.optString(DRIVER_PHONE));
            driverObj.setType(json.optString("driver_type"));
            tripObj.setDriverObj(driverObj);

            tripObj.setDistance(json.optInt(DISTANCE));
            tripObj.setStatus(json.getString(STATUS));
            tripObj.setEstimate_duration(json.optInt(ESTIMATE_DURATION));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return tripObj;
    }

    public static TripObj parseTripFull(JSONObject jsonObject){

        TripObj tripObj = new TripObj();
        JSONObject json = null;
        try {
            json = jsonObject.getJSONObject(KEY_DATA);
            tripObj.setId(json.optString(ID));
            tripObj.setStatus(json.optString(KEY_STATUS));
            tripObj.setPassengerName(json.optString("passenger_name"));
            tripObj.setPassengerPhone(json.optString("passenger_phone"));
            tripObj.setPrice(json.optDouble("actual_fare"));
            tripObj.setPassengerAvatar(json.optString("passenger_avatar"));

            LatLng startLatLng = new LatLng( json.optDouble(START_LAT), json.optDouble(START_LONG));
            tripObj.setPickup(MapsUtil.getAddressFromLatLng(AppController.getInstance(), startLatLng));
            tripObj.setLatLngPickup(startLatLng);

            LatLng endLatLng = new LatLng(json.optDouble(END_LAT), json.optDouble(END_LONG));
            tripObj.setLatLngDestination(endLatLng);
            tripObj.setDestination(MapsUtil.getAddressFromLatLng(AppController.getInstance(), endLatLng));

            tripObj.setEstimateDistance(json.optDouble(ESTIMATE_DISTANCE));
            tripObj.setEstimateFare((float) json.optDouble(ESTIMATE_FARE));

            tripObj.setDistance(json.optInt(DISTANCE));
            tripObj.setStatus(json.getString(STATUS));
            tripObj.setEstimate_duration(json.optInt(ESTIMATE_DURATION));

            // Log.e("json", "TripObj = " + new Gson().toJson(tripObj));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return tripObj;
    }
}
