package com.aveplus.avepay_cpocket.configs;


import android.content.Context;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.model.Language;
import com.aveplus.avepay_cpocket.ebook.model.OurApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public class Config {
    public static final boolean cho_phep_xac_thuc_sdt = true;
    public static final boolean cho_phep_kiem_tra_xac_thuc_acc = true;
    public static final boolean logout_co_nhan_push = false;


    //    public static final String URL_API = AppController.getInstance().getString(R.string.URL_API) + "backend/web/index.php/api/";
    public static final String URL_API = AppController.getInstance().getString(R.string.URL_API) + "backend/web/index.php/api/";
    public static final String URL_API_TRAINING_CENTER = AppController.getInstance().getString(R.string.URL_API_TRAINING_CENTER) + "backend/web/index.php/api/";

//    public static final String URL_API_LOCALHOST = AppController.getInstance().getString(R.string.URL_API_LOCALHOST) + "backend/web/index.php/api/";

    public static final int MAX_RESULT = 10;
    public static final String KEY_TOTAL_PAGE = "total_page";

    public static class Api {
        //register
        public static final String URL_REGISTER = Config.URL_API + "user/register";


        public static final String URL_SETTING = Config.URL_API + "utility/setting";
        public static final String URL_LOGIN = Config.URL_API + "user/login";
        public static final String URL_LOGOUT = Config.URL_API + "user/logout";
        public static final String URL_ACTIVE_ACCOUNT = Config.URL_API + "user/verify-account";
        public static final String URL_CHECK_PHONE = Config.URL_API + "user/check-phone";
        public static final String URL_FORGOT_PASSWORD = Config.URL_API + "user/forget-password";
        public static final String URL_CHANGE_PASSWORD = Config.URL_API + "user/change-password";
        public static final String URL_UPDATE_PROFILE = Config.URL_API + "user/update-profile";
        public static final String URL_UPDATE_IMAGE = Config.URL_API + "user/upload-image";

        public static final String URL_GET_BALANCE = Config.URL_API + "user/get-balance";
        public static final String URL_GET_REVIEW_DETAIL = Config.URL_API + "review/detail";
        public static final String URL_GET_SERVICE_DETAIL = Config.URL_API + "user/service-detail";
        public static final String URL_SERVICE_LIST = Config.URL_API + "user/service-list";
        public static final String URL_REGISTER_SERVICE = Config.URL_API + "user/service-add";
        public static final String URL_UPDATE_SERVICE = Config.URL_API + "user/service-update";
        public static final String URL_REGISTER_DEVICE = Config.URL_API + "device/index";
        public static final String URL_ORDER_HISTORY = Config.URL_API + "user/order-history";
        public static final String URL_SEARCH_SELLER_BY_KEY = Config.URL_API + "user/search-user";
        public static final String URL_SEARCH_SELLER_BY_SERVICE = Config.URL_API + "user/find-user-by-service";
        public static final String URL_HOME_USER = Config.URL_API + "user/home-user";
        public static final String URL_PROFILE = Config.URL_API + "user/profile";
        public static final String URL_BOOK_SERVICE = Config.URL_API + "user/order";
        public static final String URL_ORDER_OWED = Config.URL_API + "user/order-owed";
        public static final String URL_LIST_SERVICE = Config.URL_API + "user/service-by-user";
        public static final String URL_NOTIFICATION = Config.URL_API + "user/user-notification";
        public static final String URL_RATE = Config.URL_API + "review/index";
        public static final String URL_LIST_RATE = Config.URL_API + "review/list";
        public static final String URL_RESET_PASSWORD = Config.URL_API + "resetPassword";
        public static final String URL_ACCOUNT_BANK = Config.URL_API + "user/account-bank";
        public static final String URL_PAYMENT = Config.URL_API + "user/request-payment";
        public static final String URL_TRANSACTION_STATISTIC = Config.URL_API + "user/transaction-history";


    }


//    Đây là EBook


    // 0: pager; 1: list vertical
    public static final int TYPE_ITEM_CHAPTER = 0;
    public static final int TYPE_NUMBER_COLUMN_GRID_BOOK = 3;


    // Config background
    public static final String TYPE_BACKGROUND_LIGHT = "Light";
    public static final String TYPE_BACKGROUND_DARK = "Dark";

    // config type list
    public static final String TYPE_LIST_NAME = "Listview";
    public static final String TYPE_GRID_NAME = "Gridview";
    public static final int TYPE_LIST = 1;
    public static final int TYPE_GRID = 2;

    // Config type category
    public static final int TYPE_CATEGORY_TYPE_1 = 1;// grid. 2 column
    public static final int TYPE_CATEGORY_TYPE_2 = 2;// list, full parent
    public static final int TYPE_CATEGORY_TYPE_3 = 3;// list, name:image
    public static final int TYPE_CATEGORY_TYPE_4 = 4;//list, image:name, description


    public static final int TYPE_BOOK_IS_FEATURE = 1;//list, image:name, description
    public static final int TYPE_BOOK_IS_NEW = 2;//list, image:name, description
    public static final int TYPE_BOOK_IS_HOT = 3;//list, image:name, description


    public static int getTypeCategory() {
        return TYPE_CATEGORY_TYPE_1;
    }

    // Config theme
    public static final String FLLE_THEME_NAME = "configTheme.json";
    public static final String KEY_DATA = "data";
    public static final String KEY_NAME = "name";
    public static final String KEY_COLOR_PRIMARY = "colorPrimary";
    public static final String KEY_COLOR_PRIMARY_DARK = "colorPrimaryDark";
    public static final String KEY_COLOR_ACCENT = "colorAccent";
    public static final String KEY_BACKGROUND_MAIN_DARK = "background_main_dark";
    public static final String KEY_TEXT_COLOR_PRIMARY_DARK = "textColorPrimary_dark";
    public static final String KEY_TEXT_COLOR_SECONDARY_DARK = "textColorSecondary_dark";
    public static final String KEY_BACKGROUND_MAIN_LIGHT = "background_main_light";
    public static final String KEY_TEXT_COLOR_PRIMARY_LIGHT = "textColorPrimary_light";
    public static final String KEY_TEXT_COLOR_SECONDARY_LIGHT = "textColorSecondary_light";

    // Config More App
    public static final String FLLE_MORE_APP_NAME = "configMoreApp.json";
    public static final String KEY_ID = "id";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_LINK_SITE = "link_site";
    public static final String KEY_LINK_STORE = "link_store";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_SEARCH_MORE_APP_GOOGLE_PLAY = "suusoft";
    public static final int TYPE_MORE_APP_FROM_GOOGLE_PLAY = 0;
    public static final int TYPE_MORE_APP_FROM_FILE_JSON = 1;

    // Config Language
    public static final String FLLE_LANGUAGE_NAME = "configLanguage.json";
    public static final String KEY_CODE = "code";

    public static String loadJSONFromAsset(Context context, String file) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static ArrayList<OurApp> getListOurApp(Context context) {
        ArrayList<OurApp> list = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(context, FLLE_MORE_APP_NAME));
            JSONArray m_jArry = obj.getJSONArray(KEY_DATA);
            OurApp ourApp;
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                ourApp = new OurApp(jo_inside.getString(KEY_ID), jo_inside.getString(KEY_NAME),
                        jo_inside.getString(KEY_IMAGE), jo_inside.getString(KEY_LINK_SITE),
                        jo_inside.getString(KEY_LINK_STORE), jo_inside.getString(KEY_DESCRIPTION));
                list.add(ourApp);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ArrayList<Language> getListLanguage(Context context) {
        ArrayList<Language> list = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(context, FLLE_LANGUAGE_NAME));
            JSONArray m_jArry = obj.getJSONArray(KEY_DATA);
            Language language;
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                language = new Language(jo_inside.getString(KEY_CODE), jo_inside.getString(KEY_NAME));
                list.add(language);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    // event bus

    //
    public static class MyStoreEvent { /* Additional fields if needed */
    }

    public static class RefreshListChapter { /* Additional fields if needed */
    }

}
