package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.AbstractActivity;
import com.aveplus.avepay_cpocket.base.BaseFragment;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.listener.IOnRefresh;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;
import com.aveplus.avepay_cpocket.ebook.view.activity.DetailsBookActivity;
import com.aveplus.avepay_cpocket.ebook.view.adapter.ChapterAdapter;
import com.aveplus.avepay_cpocket.ebook.view.adapter.ListChapterAdapter;
import com.folioreader.FolioReader;
import com.folioreader.model.HighLight;
import com.folioreader.model.locators.ReadLocator;
import com.folioreader.util.OnHighlightListener;
import com.folioreader.util.ReadLocatorListener;
import com.google.gson.Gson;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by apptemplate.co on 6/19/17.
 */

public class FragmentListChapter extends BaseFragment implements IOnRefresh, OnHighlightListener, ReadLocatorListener {

    private RecyclerView rcvData;
    private ListChapterAdapter mAdapter;
    private List<Chapter> listData;
    private Book mBook;
    private Bundle bundle;
    private Menu menu;
    private boolean isBookMasrks = true;

    public static FragmentListChapter newInstance(Bundle args) {
        FragmentListChapter fragment = new FragmentListChapter();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_list_chapter;
    }

    @Override
    protected void init() {
        listData = new ArrayList<>();
        setHasOptionsMenu(true);
        bundle = getArguments();
        if (bundle != null) {
            mBook = new Gson().fromJson(bundle.getString(Constant.KEY_BOOK), Book.class);
        }
    }

    @Override
    protected void initView(View view) {
        LinearLayout layoutAll = (LinearLayout) view.findViewById(R.id.layoutAll);

        rcvData = (RecyclerView) view.findViewById(R.id.rcv_data);
        rcvData.setLayoutManager(new LinearLayoutManager(self));
    }

    @Override
    protected void getData() {
        if (null != mBook) {
            RequestManager.getChapterByBookId(self, Constant.TYPE_ALL, "", mBook.getId(), String.valueOf(1), new ApiManager.CompleteListener() {
                @Override
                public void onSuccess(ApiResponse response) {
                    if (listData == null) listData = new ArrayList<>();
                    else listData.clear();
                    listData = response.getDataList(Chapter.class);
                    Log.e("List chapter", listData.size() + "");
                    setDataFragment();
                }

                @Override
                public void onError(String message) {
                    ((AbstractActivity) self).showSnackBar(R.string.error);
                }
            });


        }
    }

    private void setDataFragment() {
        mAdapter = new ListChapterAdapter(self, listData);
        rcvData.setAdapter(mAdapter);
        mAdapter.setIOnItemClick(new ListChapterAdapter.IOnItemClick() {
            @Override
            public void onClick(int position, int flag) {
                Fragment fragment = null;
                Chapter chapter = listData.get(position);

                switch (chapter.getType()) {
                    case Chapter.TYPE_PDF:
                        fragment = FragmentDetailsChapterPDF.newInstance(chapter, mBook, flag).setOnRefresh(FragmentListChapter.this);
                        break;
                    case Chapter.TYPE_MP3:
                        fragment = FragmentDetailsChapterMP3.newInstance(chapter, mBook);
                        break;
                    case Chapter.TYPE_VIDEO:
                        fragment = FragmentDetailsChapterVideo.newInstance(chapter, mBook);
                        break;
                    case Chapter.TYPE_EPUB:
                        loadFileEpub(chapter, flag);
                        //fragment = FragmentDetailsChapterEpub.newInstance(chapter, mBook, flag).setOnRefresh(FragmentListChapter.this);
                        break;
                    case Chapter.TYPE_NORMAL:
                        fragment = FragmentDetailsChapterNORMAL.newInstance(chapter, mBook);
                        break;
                }

                if (!chapter.getType().equals(Chapter.TYPE_EPUB))
                    ((FragmentActivity) self).getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.content_details, fragment, Constant.FRAGMENT_DETAILS_CHAPTER)
                            .addToBackStack(null)
                            .commit();
            }
        });
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.e("EEE:", "onCreateOptionsMenu");
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.chapter, menu);
        this.menu = menu;
        MenuItem itemBookmarks = menu.findItem(R.id.action_bookmarks);
        if (DataStoreManager.isBookMarks(mBook.getId().toString(), Constant.LIST_BOOK_MARKS)) {
            itemBookmarks.setIcon(R.drawable.ic_book_marks_yellow);
            isBookMasrks = true;
        } else {
            itemBookmarks.setIcon(R.drawable.ic_book_marks_white);
            isBookMasrks = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e("EEE:", "onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.action_bookmarks:
                if (null != menu) {
                    if (isBookMasrks) {
                        ((DetailsBookActivity) self).deleteBookrMarks();
                        item.setIcon(R.drawable.ic_book_marks_white);
                        isBookMasrks = false;
                    } else {
                        ((DetailsBookActivity) self).addBookrMarks();
                        item.setIcon(R.drawable.ic_book_marks_yellow);
                        isBookMasrks = true;
                    }
                }
                return true;

        }
        return false;
    }

    @Override
    public void refresh() {
        getData();
    }


    private void loadFileEpub(Chapter chapter, int flag) {
        this.flag = flag;
        this.chapter = chapter;
        download();
    }

    //////////////////////////////////
    //download with file Epub
    private int flag;
    private Chapter chapter;
    private ProgressDialog mProgressDialog;
    private AsyncTask<Void, Integer, Boolean> downloadAsyntask;

    public void download() {
        downloadAsyntask = new AsyncTask<Void, Integer, Boolean>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(self);
                mProgressDialog.setMessage(self.getString(R.string.downloading));
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((FragmentActivity) self).onBackPressed();
                    }
                });
                Log.e("EEE", "" + chapter.getAttachment());
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    String downloadUrl = chapter.getAttachment();
                    String downloadPath = chapter.getSdcardURL();

                    long downloadedLength = 0;

                    File file = new File(downloadPath);
                    URL url = new URL(downloadUrl);

                    BufferedInputStream inputStream = null;
                    BufferedOutputStream outputStream = null;

                    URLConnection connection = url.openConnection();

                    if (file.exists()) {
                        Log.e("XX", "XXXXX");
                        downloadedLength = file.length();
                        connection.setRequestProperty("Range", "bytes=" + downloadedLength + "-");
                        outputStream = new BufferedOutputStream(new FileOutputStream(file, true));

                    } else {
                        outputStream = new BufferedOutputStream(new FileOutputStream(file));

                    }

                    connection.connect();

                    if (connection.getContentLength() <= 0 && file.length() > 0) {
                        Log.e("XX", "Co nghĩa gì đâu");
                        return true;
                    } else {
                        ((Activity) self).runOnUiThread(new Runnable() {
                            public void run() {
                                if (mProgressDialog.isShowing()) {
                                    mProgressDialog.dismiss();
                                    mProgressDialog.show();
                                } else {
                                    mProgressDialog.show();
                                }

                            }
                        });
                    }
                    inputStream = new BufferedInputStream(connection.getInputStream());

                    byte[] buffer = new byte[1024 * 8];
                    int byteCount;

                    long total = 0;

                    int fileLength = connection.getContentLength();
                    Log.e("eee", "File Length: " + fileLength);
                    Log.e("XX", "Làm gì kệ tao");
                    while ((byteCount = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, byteCount);
                        Log.e("XX", "Má ơi nó lại ghi lại à..!");
                        total += byteCount;
                        if (fileLength > 0) {
                            Log.e("eee", "progres: " + (int) (total * 100 / fileLength));
                            publishProgress((int) (total * 100 / fileLength));
                        }
                    }

                    inputStream.close();
                    outputStream.flush();
                    outputStream.close();

                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onProgressUpdate(Integer... values) {

                mProgressDialog.setProgress(values[0]);
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                mProgressDialog.dismiss();
                if (aVoid) {
                    if (flag == ChapterAdapter.START) {
                        //startRead();
                        openEpub();
                    } else if (flag == ChapterAdapter.CONTINUES) {
                        //readContinues();
                        openEpubContinue();
                    }
                    Log.e("XX", "ghi xong rồi...");
//                    showDialogContinue();
                    // openEpub();
                } else {
                    showDialodDownloadFailed();
                }
                Log.e("eee", "End FragmentListChapter");
                System.gc();
                Runtime.getRuntime().gc();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private AlertDialog alertDialogReload;

    public void showDialodDownloadFailed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(self);
        builder.setTitle(R.string.app_name)
                .setCancelable(false)
                .setMessage(R.string.error_LoadBook)
                .setNegativeButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        downloadEpub();
                        download();
                    }
                })
                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((FragmentActivity) self).onBackPressed();
                    }
                });
        alertDialogReload = builder.create();
        if (this.isVisible()) {
            if (!alertDialogReload.isShowing()) {
                alertDialogReload.show();
            }
        } else {
            alertDialogReload.dismiss();
        }
    }


    private void openEpubContinue() {
//        ObjectReader objectReader = ObjectMapperSingleton.getObjectMapper().reader();
        ReadLocator readLocator = DataStoreManager.getLastReadLocator(chapter);
        FolioReader.get()
                .setOnHighlightListener(this)
                .setReadLocatorListener(this)
                .setReadLocator(readLocator)
                .openBook(chapter.getSdcardURL());

    }

    private FolioReader folioReader;

    private void openEpub() {

        FolioReader.get()
                .setOnHighlightListener(this)
                .setReadLocatorListener(this)
                .setReadLocator(null)
                .openBook(chapter.getSdcardURL());

//        if(folioReader==null){
//            folioReader = FolioReader.getInstance(self).setOnHighlightListener(this).setReadPositionListener(this);
//        }
//        folioReader.setReadPosition(null).openBook(chapter.getSdcardURL());
//        Log.e("epub_path", chapter.getSdcardURL());

    }

    @Override
    public void onHighlight(HighLight highlight, HighLight.HighLightAction type) {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        FolioReader.clear();
    }

    @Override
    public void saveReadLocator(ReadLocator readLocator) {
        DataStoreManager.saveReadPosition(chapter, readLocator.toJson());
    }
}
