package com.aveplus.avepay_cpocket.music.util;

import android.graphics.Paint;
import android.widget.TextView;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class TextViewUtil {

    /**
     * draw a center line on textview
     * @param textView TextView is need to draw
     *
     */
    public static void drawCenterLine(TextView textView){
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    /**
     * draw a under line on textview
     * @param textView TextView is need to draw
     *
     */
    public static void drawUnderLine(TextView textView){
        textView.setPaintFlags(textView.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
    }

}
