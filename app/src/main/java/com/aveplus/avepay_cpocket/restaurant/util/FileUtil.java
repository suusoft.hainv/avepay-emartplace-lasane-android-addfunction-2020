package com.aveplus.avepay_cpocket.restaurant.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by phamv on 12/14/2016.
 */

public class FileUtil {

    // max fix size is uploaded. in bytes
    public static final long MAX_FILE_SIZE = 3000000;

    public static byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 2048;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    public  static byte[] read(File file) {
        if (file.length() > MAX_FILE_SIZE) {
            return null;
        }
        ByteArrayOutputStream ous = null;
        InputStream ios = null;
        try {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            ios = new FileInputStream(file);
            int read = 0;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
            }

            try {
                if (ios != null)
                    ios.close();
            } catch (IOException e) {
            }
        }
        return ous.toByteArray();
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument( Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public static final String ALBUM_ARTIST = "album_artist";


    public static Cursor getSongsByArtist(Context context, String artist){
        String selection = MediaStore.Audio.Media.ARTIST_ID + " = " + artist;
        String order = MediaStore.Audio.AudioColumns.TITLE + " ASC";
        return getAllSongsWithSelection(context, selection,order);
    }

    public static Cursor getSongsByAlbum(Context context, String album){
        String selection = MediaStore.Audio.Media.ALBUM_ID + " = " +  album;
        String order = MediaStore.Audio.AudioColumns.TITLE + " ASC";
        return getAllSongsWithSelection(context, selection,order);
    }
    /**
     * Queries MediaStore and returns a cursor with songs limited
     * by the selection parameter.
     */
    public static Cursor getAllSongsWithSelection(Context context,
                                                  String selection,
                                                  String sortOrder) {
        String[] projection = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.COMPOSER,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.SIZE,
                MediaStore.Audio.Media.ARTIST_ID
        };
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        return contentResolver.query(uri, projection, selection, null, sortOrder);

    }

    /**
     * Queries MediaStore and returns a cursor with all songs.
     */
    public static Cursor getAllSongs(Context context) {
        String[] projection = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.COMPOSER,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.SIZE,
                MediaStore.Audio.Media.ARTIST_ID

        };
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String order = MediaStore.Audio.AudioColumns.TITLE + " ASC";

        return contentResolver.query(uri, projection, selection, null, order);

    }

    /**
     * Queries MediaStore and returns a cursor with all unique artists,
     * their ids, and their number of albums.
     */
    public static Cursor getAllUniqueArtists(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String[] projection = {
                MediaStore.Audio.Artists._ID,
                MediaStore.Audio.Artists.ARTIST,
                MediaStore.Audio.Artists.NUMBER_OF_TRACKS };

        return contentResolver.query(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                MediaStore.Audio.Artists.ARTIST + " ASC");

    }

    /**
     * Queries MediaStore and returns a cursor with all unique albums,
     * their ids, and their number of songs.
     */
    public static Cursor getAllUniqueAlbums(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String[] projection = { MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.NUMBER_OF_SONGS };

        return contentResolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                MediaStore.Audio.Albums.ALBUM + " ASC");

    }

    /**
     * Queries MediaStore and returns a cursor with all unique genres
     * and their ids.
     */
    public static Cursor getAllUniqueGenres(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String[] projection = { MediaStore.Audio.Genres._ID,
                MediaStore.Audio.Genres.NAME };

        return contentResolver.query(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                MediaStore.Audio.Genres.NAME + " ASC");

    }

    /**
     * Queries MediaStore and returns a cursor with all unique playlists.
     */
    public static Cursor getAllUniquePlaylists(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        String[] projection = { MediaStore.Audio.Playlists._ID,
                MediaStore.Audio.Playlists.NAME };

        return contentResolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                MediaStore.Audio.Playlists.NAME + " ASC");

    }

    public static boolean deleteFile(String path){
        File file = new File(path);
        return file.delete();
    }

    public static boolean isExist(String path){
        return new File(path).exists();
    }

}
