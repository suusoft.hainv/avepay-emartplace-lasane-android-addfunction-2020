package com.aveplus.avepay_cpocket.restaurant;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.PacketUtility;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.Constants;
import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.interfaces.IOnItemClickListener;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;

import com.aveplus.avepay_cpocket.network1.MyProgressDialog;
import com.aveplus.avepay_cpocket.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.objects.ContactObj;
import com.aveplus.avepay_cpocket.objects.DealCateObj;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.objects.MenuLeft;
import com.aveplus.avepay_cpocket.objects.SettingsObj;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.aveplus.avepay_cpocket.parsers.JSONParser;
import com.aveplus.avepay_cpocket.quickblox.QbAuthUtils;
import com.aveplus.avepay_cpocket.quickblox.QbDialogHolder;
import com.aveplus.avepay_cpocket.quickblox.SharedPreferencesUtil;
import com.aveplus.avepay_cpocket.quickblox.chat.ChatHelper;
import com.aveplus.avepay_cpocket.quickblox.conversation.utils.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.aveplus.avepay_cpocket.utils.map.LocationService;
import com.aveplus.avepay_cpocket.view.activities.BaseActivity;
import com.aveplus.avepay_cpocket.view.activities.DealsActivity;
import com.aveplus.avepay_cpocket.view.activities.SplashLoginActivity;
import com.aveplus.avepay_cpocket.view.adapters.MenuLeftAdapte;
import com.aveplus.avepay_cpocket.view.fragments.AllDealsFragment;
import com.aveplus.avepay_cpocket.view.fragments.ContactFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealListFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealManagerFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealsFragment;
import com.aveplus.avepay_cpocket.view.fragments.FragmentFavorite;
import com.aveplus.avepay_cpocket.view.fragments.IwanaPayFragment;
import com.aveplus.avepay_cpocket.view.fragments.IwanabizFragment;
import com.aveplus.avepay_cpocket.view.fragments.MyAccountMyInfoFragment;
import com.aveplus.avepay_cpocket.view.fragments.MyDealFragment;
import com.aveplus.avepay_cpocket.view.fragments.NewsFragment;
import com.aveplus.avepay_cpocket.view.fragments.ProducerManagerFragment;
import com.aveplus.avepay_cpocket.view.fragments.SellerFragment;
import com.aveplus.avepay_cpocket.view.fragments.SettingsFragment;
import com.aveplus.avepay_cpocket.view.fragments.WebViewFragment;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.SubscribeService;

import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RestaurantActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        DealListFragment.IListenerDealsChange, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, IOnItemClickListener {

    private static final String TAG = RestaurantActivity.class.getSimpleName();

    private CoordinatorLayout bgMain;

    private static final int RC_LOCATION_PERMISSION_TO_UPDATE_DRIVER_LOCATION = 1;
    private static final int RC_TURN_ON_LOCATION_TO_UPDATE_DRIVER_LOCATION = 2;
    private static final int RC_LOCATION_PERMISSION_TO_UPDATE_USER_LOCATION = 3;
    private static final int RC_TURN_ON_LOCATION_TO_UPDATE_USER_LOCATION = 4;

    private static final String FRAG_HOME = "Home";
    private static final String FRAG_DEAL_MANAGER = "dealManager";
    private static final String FRAG_SELLER_MANAGER = "sellerManager";
    private static final String FRAG_BUYER_MANAGER = "buyerManager";
    private static final String FRAG_ALL_DEALS = "allDeals";
    private static final String FRAG_NEWS = "newsAndEvents";
    private static final String FRAG_SETTINGS = "settings";
    private static final String FRAG_IWANA_PAY = "iwanapay";
    private static final String FRAG_MY_ACCOUNT = "myaccount";
    private static final String FRAG_MY_DEAL = "mydeal";
    private static final String FRAG_IWANA_CHAT = "FRAG_IWANA_CHAT";
    private static final String FRAG_ABOUT = "FRAG_ABOUT";
    private static final String FRAG_FAQ = "FRAG_FAQ";
    private static final String FRAG_WEBVIEW = "FRAG_WEBVIEW";
    private static final String FRAG_MOVIE = "FRAG_MOVIE";
    private static final String FRAG_RESTAURANT = "FRAG_RESTAURANT";
    private FragmentTransaction fragmentTransaction;
    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private Toolbar toolbar;
    private ArrayList<MenuLeft> menuLefts;
    private MenuLeftAdapte adapter;
    private LinearLayout llMenuLeft;
    private RecyclerView lvMenu;
    private CircleImageView imgAvatar;
    private TextViewBold tvName, tvEmail;

    private AllDealsFragment mAllDealsFragment;

    private NewsFragment mNewsFragment;
    private SettingsFragment mSettingsFragment;
    private IwanaPayFragment mIwanaPayFragment;
    //    private MyAccountFragment myAccountFragment;
    private MyAccountMyInfoFragment myAccountFragment;
    private IwanabizFragment mFrgIwanabiz;
    private MyDealFragment mFrgMyDeal;
    private DealsFragment mFrgDeal;
    private DealManagerFragment mFrgDealManager;
    private ContactFragment mContactFragment;
    private WebViewFragment mWebViewFragment;
    private FragmentFavorite mFavoriteFragment;
    private SellerFragment mSellerFragment;
    private ProducerManagerFragment mProManaFragment;

    private GoogleApiClient mGoogleApiClient;

    private int mSelectedNav;
    private int indexMenu = 0;

    private boolean userLocationIsUpdated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initGoogleApiClient();
        // Get saved instances
        if (savedInstanceState != null) {
            mFavoriteFragment = (FragmentFavorite) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_HOME);
            mFrgDealManager = (DealManagerFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_DEAL_MANAGER);
            mSellerFragment = (SellerFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_SELLER_MANAGER);
            mFrgDeal = (DealsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_BUYER_MANAGER);
            mAllDealsFragment = (AllDealsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_ALL_DEALS);
            mNewsFragment = (NewsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_NEWS);
            mSettingsFragment = (SettingsFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_SETTINGS);
            mIwanaPayFragment = (IwanaPayFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_IWANA_PAY);
            myAccountFragment = (MyAccountMyInfoFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_MY_ACCOUNT);
            mFrgMyDeal = (MyDealFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_MY_DEAL);
            mContactFragment = (ContactFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_IWANA_CHAT);
            mWebViewFragment = (WebViewFragment) getSupportFragmentManager().getFragment(savedInstanceState, FRAG_WEBVIEW);

        }


        // Init quickblox
        if (DataStoreManager.getUser() != null && (DataStoreManager.getToken() != null
                && !DataStoreManager.getToken().equals(""))) {
            initSession(savedInstanceState);
            initDialogsListener();
            initPushManager();
        }

        // Start location service in some cases(driver closes app without deactivating)
        updateDriverLocation();

        // Get contacts from scratch
        getContacts();

        AppUtil.logSizeMultiScreen(this);
    }


    @Override
    protected void inflateLayout() {
        setContentView(R.layout.activity_restaurant);

    }

    @Override
    protected void initUI() {
        bgMain = (CoordinatorLayout) findViewById(R.id.bg_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initNavigationView();

        initMenuLeft();

        switchMenu(Constants.MENU_ALL_DEAL);
    }

    @Override
    protected void initControl() {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onChanged(ArrayList<DealObj> mDealObj) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(self)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private void updateDriverLocation() {
        if (DataStoreManager.getUser() != null && DataStoreManager.getUser().getDriverData() != null) {
            if (DataStoreManager.getUser().getDriverData().isAvailable()) {
                LocationService.start(self, LocationService.REQUEST_LOCATION);
            }
        }
    }

    private void getContacts() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            ModelManager.getContacts(self, false, new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    Log.e(TAG, "contacts " + new Gson().toJson(object));
                    JSONObject jsonObject = (JSONObject) object;
                    if (JSONParser.responseIsSuccess(jsonObject)) {
                        ArrayList<ContactObj> contactObjs = JSONParser.parseContacts(jsonObject);
                        if (contactObjs.size() > 0) {
                            // Save contacts into preference
                            DataStoreManager.saveContactsList(contactObjs);
                        }
                    }
                }

                @Override
                public void onError() {
                }
            });
        }
    }

    private void initNavigationView() {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AppUtil.hideSoftKeyboard(RestaurantActivity.this);

//                updateMenuHeader();
            }
        };
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
    }

    private void initMenuLeft() {
        imgAvatar = (CircleImageView) findViewById(R.id.img_avatar);
        tvName = (TextViewBold) findViewById(R.id.lbl_deal_name);
        tvEmail = (TextViewBold) findViewById(R.id.tv_email);
        lvMenu = (RecyclerView) findViewById(R.id.lv_menu);

        initMenuLeftHeader();

        initListMenu();

        adapter = new MenuLeftAdapte(menuLefts, self, new IOnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (indexMenu != position) {
                    menuLefts.get(indexMenu).setSelected(false);
                    indexMenu = position;
                    switchMenu(menuLefts.get(position).getId());
                    menuLefts.get(position).setSelected(true);
                } else mDrawer.closeDrawer(GravityCompat.START);

                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager manager = new LinearLayoutManager(self);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        lvMenu.setAdapter(adapter);
        lvMenu.setLayoutManager(manager);
        lvMenu.setHasFixedSize(true);
    }

    private void initMenuLeftHeader() {
        UserObj userObj = DataStoreManager.getUser();
        if (userObj != null) {
            tvName.setText(userObj.getName());
            tvEmail.setText(userObj.getEmail());
            ImageUtil.setImage(self, imgAvatar, userObj.getAvatar());
        }
    }

    private void initListMenu() {
        menuLefts = new ArrayList<>();

        menuLefts.add(new MenuLeft(Constants.MENU_ALL_DEAL, R.drawable.ic_all_deals_white,
                getResources().getString(R.string.home), true));

        menuLefts.add(new MenuLeft(Constants.MENU_CHAT, R.drawable.ic_chatbubble_,
                getResources().getString(R.string.chat), false));
        menuLefts.add(new MenuLeft(Constants.MENU_BUYER_NAMAGER, R.drawable.ic_buyer_white,
                getResources().getString(R.string.buy_deals), false));
        menuLefts.add(new MenuLeft(Constants.MENU_SELLER, R.drawable.ic_seller_white,
                getResources().getString(R.string.sell_deals), false));
        menuLefts.add(new MenuLeft(Constants.MENU_PAYMENT, R.drawable.ic_payment_white,
                getResources().getString(R.string.payment), false));
        menuLefts.add(new MenuLeft(Constants.MENU_PROFILE, R.drawable.ic_profiles_white,
                getResources().getString(R.string.profile), false));
        menuLefts.add(new MenuLeft(Constants.MENU_NEW_EVENT, R.drawable.ic_new_event,
                getResources().getString(R.string.news_and_events), false));
        menuLefts.add(new MenuLeft(Constants.MENU_SHARE, R.drawable.ic_share_white,
                getResources().getString(R.string.share), false));
        menuLefts.add(new MenuLeft(Constants.MENU_FAQ, R.drawable.ic_faq,
                getResources().getString(R.string.faq), false));
        menuLefts.add(new MenuLeft(Constants.MENU_SETTING, R.drawable.ic_setting_white,
                getResources().getString(R.string.settings), false));
        menuLefts.add(new MenuLeft(Constants.MENU_HELP, R.drawable.ic_help_white,
                getResources().getString(R.string.help), false));
        menuLefts.add(new MenuLeft(Constants.MENU_ABOUT_US, R.drawable.ic_aboutus_white,
                getResources().getString(R.string.about_us), false));
        menuLefts.add(new MenuLeft(Constants.MENU_LOGOUT, R.drawable.ic_signout_white,
                getResources().getString(R.string.log_out), false));


    }

    public void switchMenu(int idMenuLeft) {
        setbackGroundMainHaveData();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (idMenuLeft) {
            case Constants.MENU_ALL_DEAL:

                break;

            case Constants.MENU_FAVORITE:
                if (mFavoriteFragment == null) {
                    mFavoriteFragment = FragmentFavorite.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, mFavoriteFragment).commit();
                setTitle(R.string.favorite_deals);
                break;

            case Constants.MENU_CHAT:
                if (mContactFragment == null) {
                    mContactFragment = ContactFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, mContactFragment).commit();

                setTitle(R.string.chat);
                break;

            case Constants.MENU_BUYER_NAMAGER:
                if (mFrgDeal == null) {
                    mFrgDeal = DealsFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, mFrgDeal).commit();

                setTitle(R.string.buy_deals);
                break;

            case Constants.MENU_SELLER:
                if (mProManaFragment == null) {
                    mProManaFragment = ProducerManagerFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, mProManaFragment).commit();
                setTitle(R.string.sell_deals);
                break;

            case Constants.MENU_PAYMENT:
                if (mIwanaPayFragment == null) {
                    mIwanaPayFragment = IwanaPayFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, mIwanaPayFragment).commit();

                setTitle(R.string.payment);
                break;

            case Constants.MENU_PROFILE:
                if (myAccountFragment == null) {
                    myAccountFragment = MyAccountMyInfoFragment.newInstance();
                }
                fragmentTransaction.replace(R.id.frl_main, myAccountFragment).commit();

                setTitle(R.string.profile);
                break;

            case Constants.MENU_NEW_EVENT:
                Bundle bundle = new Bundle();
                bundle.putString(Args.KEY_ID_DEAL_CATE, DealCateObj.NEWS_AND_EVENTS);
                bundle.putString(Args.TYPE_OF_DEAL_NAME, getString(R.string.news_and_events));
                bundle.putString(Args.TYPE_OF_SEARCH_DEAL, Constants.SEARCH_NEARBY);

                GlobalFunctions.startActivityWithoutAnimation(self, DealsActivity.class, bundle);
                break;

            case Constants.MENU_SHARE:
                AppUtil.share(this, "http://play.google.com/store/apps/details?id=" + new PacketUtility().getPacketName());

                setTitle(R.string.share);

                break;

            case Constants.MENU_FAQ:
                showScreenFaq();
                break;

            case Constants.MENU_SETTING:
                fragmentTransaction.replace(R.id.frl_main, SettingsFragment.newInstance()).commit();
                setTitle(R.string.settings);
                break;

            case Constants.MENU_HELP:
                showScreenHelp();
                break;

            case Constants.MENU_ABOUT_US:
                showScreenAboutUs();
                break;

            case Constants.MENU_LOGOUT:
                logout();
                break;
        }

        mDrawer.closeDrawer(GravityCompat.START);

    }

    public void setbackGroundMainHaveData() {
        bgMain.setBackgroundResource(R.drawable.bg_deal);
    }

    private void showScreenFaq() {
        SettingsObj setting = DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                ModelManager.getSettingUtility(this, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        JSONObject jsonObject = (JSONObject) object;
                        ApiResponse apiResponse = new ApiResponse(jsonObject);
                        if (!apiResponse.isError()) {
                            DataStoreManager.saveSettingUtility(jsonObject.toString());
                            SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.faq), utitlityObj.getFaq());

                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.KEY_URL, utitlityObj.getFaq());
                            mWebViewFragment = WebViewFragment.newInstance(bundle);

                            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                            setTitle(getString(R.string.faq));

//                                    openWebView(getString(R.string.about_us), DataStoreManager.getSettingUtility().getAbout());
                        }

                    }

                    @Override
                    public void onError() {
                    }
                });
            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getFaq());
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

            setTitle(getString(R.string.faq));

//                    openWebView(getString(R.string.faq), DataStoreManager.getSettingUtility().getFaq());
        }
    }

    private void showScreenHelp() {
        SettingsObj setting = DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                ModelManager.getSettingUtility(this, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        JSONObject jsonObject = (JSONObject) object;
                        ApiResponse apiResponse = new ApiResponse(jsonObject);
                        if (!apiResponse.isError()) {
                            DataStoreManager.saveSettingUtility(jsonObject.toString());
                            SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.help), utitlityObj.getHelp());

                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.KEY_URL, utitlityObj.getHelp());
                            mWebViewFragment = WebViewFragment.newInstance(bundle);

                            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                            setTitle(getString(R.string.help));

                        } else {
                            AppUtil.showToast(getApplicationContext(), apiResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError() {
                        AppUtil.showToast(getApplicationContext(), "Error!");
                    }
                });
            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }

        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getHelp());
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

            setTitle(getString(R.string.help));

//                    openWebView(getString(R.string.help), DataStoreManager.getSettingUtility().getHelp());
        }
    }

    private void showScreenAboutUs() {

        SettingsObj setting = DataStoreManager.getSettingUtility();
        if (setting == null) {
            if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
                ModelManager.getSettingUtility(this, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                        JSONObject jsonObject = (JSONObject) object;
                        ApiResponse apiResponse = new ApiResponse(jsonObject);
                        if (!apiResponse.isError()) {
                            DataStoreManager.saveSettingUtility(jsonObject.toString());
                            SettingsObj utitlityObj = apiResponse.getDataObject(SettingsObj.class);
//                                    openWebView(getString(R.string.about_us), utitlityObj.getAbout());

                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.KEY_URL, utitlityObj.getAbout());
                            mWebViewFragment = WebViewFragment.newInstance(bundle);

                            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

                            setTitle(getString(R.string.about_us));
                        }
                    }

                    @Override
                    public void onError() {
                    }
                });
            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }
        } else {

            Bundle bundle = new Bundle();
            bundle.putString(Constants.KEY_URL, DataStoreManager.getSettingUtility().getAbout());
            mWebViewFragment = WebViewFragment.newInstance(bundle);

            fragmentTransaction.replace(R.id.frl_main, mWebViewFragment).commit();

            setTitle(getString(R.string.about_us));
//                    openWebView(getString(R.string.about_us), DataStoreManager.getSettingUtility().getAbout());
        }
    }

    private void logout() {
        showDialogLogout();
    }

    private void showDialogLogout() {
        DialogUtil.showAlertDialog(self, R.string.log_out, R.string.you_wanto_logout, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                requestLogout();
            }
        });
    }

    private void requestLogout() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            final MyProgressDialog progressDialog = new MyProgressDialog(self);
            progressDialog.show();

            if (QbAuthUtils.isSessionActive()) {
                ChatHelper.getInstance().logout(new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        processBeforeLoggingOut(progressDialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Log.e(TAG, "Log out - onError: " + e.getMessage());
                    }
                });
            } else {
                ChatHelper.getInstance().login(self, SharedPreferencesUtil.getQbUser(), new QBEntityCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid, Bundle bundle) {
                        processBeforeLoggingOut(progressDialog);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Toast.makeText(self, "Fail to logout: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Toast.makeText(self, R.string.msg_no_network, Toast.LENGTH_SHORT).show();
        }
    }

    private void processBeforeLoggingOut(MyProgressDialog progressDialog) {
        if (DataStoreManager.getUser() != null && DataStoreManager.getUser().getDriverData() != null) {
            if (DataStoreManager.getUser().getDriverData().isAvailable()) {
                LocationService.start(self, LocationService.STOP_REQUESTING_LOCATION);

                // Deactivate driver's mode before logging out
                ModelManager.activateDriverMode(self, Constants.OFF, 0, new ModelManagerListener() {
                    @Override
                    public void onSuccess(Object object) {
                    }

                    @Override
                    public void onError() {
                    }
                });
            }
        }

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        SubscribeService.unSubscribeFromPushes(self);
        QBChatService.getInstance().destroy();
        SharedPreferencesUtil.removeQbUser();
        QbDialogHolder.getInstance().clear();

        DataStoreManager.clearUserToken();
        AppController.getInstance().setUserUpdated(true);
        AppUtil.startActivity(self, SplashLoginActivity.class);
        finish();
    }
}
