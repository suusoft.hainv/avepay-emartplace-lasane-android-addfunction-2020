package com.aveplus.avepay_cpocket.restaurant.main.notification;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.restaurant.network.ApiResponse;
import com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.NotifiRespone;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.ViewDialog;
import com.aveplus.avepay_cpocket.restaurant.widgets.recyclerview.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_CONTENT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_IMAGE;


public class NotificationFragment extends BaseFragment implements View.OnClickListener, EndlessRecyclerViewScrollListener.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView rvNotifi;
    private List<NotificationModel> list;
    private NotificationAdapter notificationAdapter;
    private LinearLayoutManager manager;
    private LinearLayout llnoData, llnoConect;
    private EndlessRecyclerViewScrollListener onScrollListener;
    private int page = 1;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int phone;

    private static final String TAG = "NotificationFragment";

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_notification;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getData();
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        llnoData = view.findViewById(R.id.ll_no_data);
        llnoConect = view.findViewById(R.id.ll_no_connection);
        rvNotifi = view.findViewById(R.id.rv_notifi);
        manager = new LinearLayoutManager(self);
        checkNetworkshow();
        rvNotifi.setLayoutManager(manager);
        swipeRefreshLayout.setOnRefreshListener(this);
        list = new ArrayList<>();
        notificationAdapter = new NotificationAdapter(list, self, new NotificationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(NotificationModel model) {
                Bundle bundle = new Bundle();
                bundle.putString(PARAM_CONTENT, model.getContent());
                bundle.putString(PARAM_IMAGE, model.getImage());
                FragmentNotificationDetail fragment = new FragmentNotificationDetail();
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction().replace(R.id.root_layout, fragment);
                fragmentTransaction.commit();
                fragmentTransaction.addToBackStack("");

            }
        });
        rvNotifi.setAdapter(notificationAdapter);
        rvNotifi.hasFixedSize();
        onScrollListener = new EndlessRecyclerViewScrollListener(this, manager);
        rvNotifi.addOnScrollListener(onScrollListener);
        onScrollListener.setEnded(false);

    }


    @Override
    protected void getData() {
        if (NetworkUtility.isNetworkAvailable()) {
            ViewDialog dialog = new ViewDialog(getActivity());
            dialog.showDialog();
            APIService apiService = ApiUtils.getAPIService();
            apiService.getListNotifi(page, 10).enqueue(new Callback<NotifiRespone>() {
                @Override
                public void onResponse(Call<NotifiRespone> call, Response<NotifiRespone> response) {
                    if (response.isSuccessful()) {
                        ApiResponse apiResponse = new ApiResponse(response.code(), response.message());
                        list.addAll(response.body().getData());
                        String page_total = response.body().getTotal_page();
                        notificationAdapter.notifyDataSetChanged();
                        onScrollListener.onLoadMoreComplete();

                        Log.e(TAG, "onResponse: " + page_total);
                        swipeRefreshLayout.setRefreshing(false);
                        checkViewHideShow();
                        dialog.hideDialog();
                        if (list.isEmpty()) {
                            llnoData.setVisibility(View.VISIBLE);
                        } else {
                            llnoData.setVisibility(View.GONE);
                        }
                    } else {
                        Log.e(TAG, "onResponse: else");
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<NotifiRespone> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    swipeRefreshLayout.setRefreshing(false);
                    dialog.hideDialog();

                }
            });

        } else {
            AppUtil.showToast(getActivity(), R.string.msg_no_network);
        }
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onLoadMore(int page) {
        this.page = page;
        getData();
    }


    @Override
    public void onRefresh() {
        list.clear();
        notificationAdapter.notifyDataSetChanged();
        page = 1;
        onScrollListener.setEnded(false);
        onScrollListener.setCurrentPage(page);
        getData();
    }

    private void checkViewHideShow() {
        if (list.isEmpty()) {
            llnoData.setVisibility(View.VISIBLE);
        } else {
            llnoData.setVisibility(View.GONE);
        }
    }

    private void checkNetworkshow() {
        if (NetworkUtility.isNetworkAvailable()) {
            llnoConect.setVisibility(View.GONE);
        } else {
            llnoConect.setVisibility(View.VISIBLE);
        }
    }

}
