package com.aveplus.avepay_cpocket.music.main.downloaded;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListActivityBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class DownloadedFileActivity extends BaseListActivityBinding {

    DownloadedFileVM viewModel;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new DownloadedFileVM(self);
        return viewModel;
    }

    @Override
    protected void onViewCreated() {
        setToolbarTitle(R.string.song_downloaded);
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        SingleAdapter adapter = new SingleAdapter(self, R.layout.item_song,viewModel.getListData(), ItemDownloadedFileVM.class, viewModel.getActionListener());
        recyclerView.setAdapter(adapter);
    }


}
