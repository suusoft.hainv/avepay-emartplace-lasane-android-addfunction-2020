package com.aveplus.avepay_cpocket.trainingcenter.object;

public class Portfolios {

	String id;
	String title;
	String image;
	String desc;
	String content;

	public Portfolios() {
		super();
	}

	public Portfolios(String id, String title, String image, String desc,
                      String content) {
		super();
		this.id = id;
		this.title = title;
		this.image = image;
		this.desc = desc;
		this.content = content;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
