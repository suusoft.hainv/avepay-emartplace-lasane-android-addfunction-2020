package com.aveplus.avepay_cpocket.restaurant.base.model;

import com.google.gson.Gson;

/**
 * Created by Trang on 1/8/2016.
 */
public class BaseModel {

    public String toJSon(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
