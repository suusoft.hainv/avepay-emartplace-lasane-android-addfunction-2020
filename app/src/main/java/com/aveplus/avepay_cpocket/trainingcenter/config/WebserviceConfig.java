package com.aveplus.avepay_cpocket.trainingcenter.config;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

public interface WebserviceConfig {

    public static String SERVER_DOMAIN = AppController.getInstance().getString(R.string.URL_API_TRAINING_CENTER);
    public static String CORPORATE_DOMAIN = SERVER_DOMAIN + "/backend/web/index.php/api/";


    public static String URL_ABOUT = CORPORATE_DOMAIN + "about";

    public static String URL_CONTACT = CORPORATE_DOMAIN + "contact";

    public static String URL_PEOPLE = CORPORATE_DOMAIN + "employees";

    public static String URL_NEWS = CORPORATE_DOMAIN + "news";

    public static String URL_PRODUCTS = CORPORATE_DOMAIN + "products";

    public static String URL_PORTFOLIOS = CORPORATE_DOMAIN + "searchportfolio";

    public static String URL_SERVICE = CORPORATE_DOMAIN + "service";

    public static String URL_SALE = CORPORATE_DOMAIN + "saleoff";

    public static String URL_ORDER = CORPORATE_DOMAIN + "order";

    public static String URL_CUSTOMERS = CORPORATE_DOMAIN + "customer";

    public static String URL_TESTIMONIAL = CORPORATE_DOMAIN + "testimonial";
    public static String HOST_URL_EMAIL = CORPORATE_DOMAIN + "appointment";

    public static String URL_GETCATEGORY = CORPORATE_DOMAIN + "category";

    public static String URL_SETTING = CORPORATE_DOMAIN + "setting";

    public static String HOST_URL_HISTORY = CORPORATE_DOMAIN + "orderhistory";

    public static String HOST_URL_DETAIL_HISTORY = CORPORATE_DOMAIN + "orderdetail";

    public static String URL_GETCATEGORYNEWS = CORPORATE_DOMAIN + "newscategory";

    public static String URL_GETLISTPRODUCTBYCATEGORY = CORPORATE_DOMAIN + "search-product";

    public static String URL_GETLISTNEWBYCATEGORY = CORPORATE_DOMAIN + "search-news";

    public static String URL_COMMENT = CORPORATE_DOMAIN + "send-testimonial";
    // categories
    public static final String KEY_CATEGORY_ID = "id";
    public static final String KEY_CATEGORY_NAME = "name";
    public static final String KEY_CATEGORY_IMAGE = "image";
}
