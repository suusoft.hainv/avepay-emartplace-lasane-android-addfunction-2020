package com.aveplus.avepay_cpocket.music.retrofit.response;

import com.aveplus.avepay_cpocket.music.main.login.UserObj;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResponseLogin extends BaseModel {

    private UserObj data;

    public UserObj getData() {
        return data;
    }

    public void setData(UserObj data) {
        this.data = data;
    }

    @SerializedName("login_token")
    @Expose
    private String loginToken;

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }
}
