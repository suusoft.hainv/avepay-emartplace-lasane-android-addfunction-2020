package com.aveplus.avepay_cpocket.movie.movie.main.impl;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface CallBackOnPressLanguage {
    public void OnPressLanguage(int position, int langId);
}
