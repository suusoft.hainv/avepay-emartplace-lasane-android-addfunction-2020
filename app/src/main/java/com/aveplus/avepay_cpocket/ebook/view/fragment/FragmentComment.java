package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.AbstractActivity;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseListFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.model.Comment;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;
import com.aveplus.avepay_cpocket.ebook.view.adapter.CommentAdapter;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.CommentVM;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Suusoft on 9/8/2017.
 */
public class FragmentComment extends BaseListFragmentBinding {

    private CommentAdapter mAdapter;
    private CommentVM viewModel;
    private Chapter chapter;
    private Dialog dialog;

    public static FragmentComment newInstance(Chapter chapter) {
        Bundle args = new Bundle();
        args.putParcelable(Constant.KEY_CHAPTER, chapter);
        FragmentComment fragment = new FragmentComment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.comment, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_comment2:
                showDialogComment();
                return true;
        }
        return false;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        mAdapter = new CommentAdapter(self, viewModel.getListData());
        recyclerView.setLayoutManager(viewModel.getLayoutManager());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(viewModel.getOnScrollListener());
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            chapter = bundle.getParcelable(Constant.KEY_CHAPTER);
            viewModel = new CommentVM(self, chapter);
        }
        return viewModel;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (null != dialog) {
            dialog.dismiss();
        }
    }

    private void showDialogComment() {

        dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_comment);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        final TextView textView2 = (TextView) dialog.findViewById(R.id.textView2);

        final EditText txtName = (EditText) dialog.findViewById(R.id.txtName);
        final EditText txtContent = (EditText) dialog
                .findViewById(R.id.txtContent);
        txtContent.setHorizontallyScrolling(false);
        txtContent.setMaxLines(8);
        txtContent.setMinLines(4);

        TextView btnCancel = (TextView) dialog.findViewById(R.id.btnCancel);
        TextView btnAdd = (TextView) dialog.findViewById(R.id.btnAdd);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String name = txtName.getText().toString();
                String content = txtContent.getText().toString();
                if (name.isEmpty()) {
                    ((AbstractActivity) self).showToast(R.string.input_name);
                } else if (content.isEmpty()) {
                    ((AbstractActivity) self).showToast(R.string.input_comment);
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    String strDate = sdf.format(System.currentTimeMillis());

                    final Comment comment = new Comment();
                    comment.setChapterId(Integer.parseInt(chapter.getId()));
                    if (chapter.getBookId() != null) {
                        comment.setBookId(Integer.parseInt(chapter.getBookId()));
                    }
                    comment.setName(name);
                    comment.setContent(content);
                    comment.setDatetime(strDate);
                    comment.setStatus(1);
                    // request to sever
                    RequestManager.addComment(self, comment, new ApiManager.CompleteListener() {
                        @Override
                        public void onSuccess(ApiResponse response) {
                            ((AbstractActivity) self).showSnackBar(R.string.success);
                            mAdapter.create(comment);
                        }

                        @Override
                        public void onError(String message) {
                            ((AbstractActivity) self).showSnackBar(R.string.error);
                        }
                    });
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

}
