package com.aveplus.avepay_cpocket.restaurant.main.history;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseFragment;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.network.NetworkUtility;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeDetail;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.ViewDialog;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView rvHistory;
    private HistoryAdapter historyAdapter;
    private ArrayList<DetailOrder> details;
    private LinearLayout llnoData, llnoConnect;
    private LinearLayoutManager manager;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static ArrayList<Detail> arrayList;

    public static final String KEY_PRODUCT_OBJECT = "KEY_PRODUCT_OBJECT";

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_history_restaurant;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {

        llnoData = view.findViewById(R.id.ll_no_data);
        llnoConnect = view.findViewById(R.id.ll_no_connection);
        checkNetworkshow();
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        rvHistory = view.findViewById(R.id.rv_history);
        manager = new LinearLayoutManager(self);
        rvHistory.setLayoutManager(manager);
        swipeRefreshLayout.setOnRefreshListener(this);
        details = new ArrayList<>();
        arrayList = new ArrayList<>();
    }

    @Override
    protected void getData() {
        if (NetworkUtility.isNetworkAvailable()) {
            ViewDialog dialog = new ViewDialog(getActivity());
            dialog.showDialog();
            APIService apiService1 = ApiUtils.getAPIService();
            apiService1.DetailOrder(DataStoreManager.getToken(), 0).enqueue(new Callback<ResponeDetail>() {
                @Override
                public void onResponse(Call<ResponeDetail> call, Response<ResponeDetail> response) {

                    if (response.isSuccessful()) {
                        details.addAll(response.body().getData());

                        for (int i = 0; i < response.body().getData().size(); i++) {
                            arrayList.addAll(response.body().getData().get(i).getOrderDetail());
                            //Log.e("TAG", "onResponse: " + arrayList.get(i).getRestaurantInfo().getName());
                        }
                        historyAdapter = new HistoryAdapter(details, getActivity(), arrayList, new HistoryAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(DetailOrder detailOrder, int i) {
                                Intent intent = new Intent(self, DetailOrderActivity.class);
                                intent.putExtra("name", details.get(i).getBillingName());
                                intent.putExtra("address", details.get(i).getBillingAddress());
                                intent.putExtra("phone", details.get(i).getBillingPhone());
                                intent.putExtra("payment", details.get(i).getPaymentMethod());
                                intent.putExtra("date", details.get(i).getCreateDate());
                                intent.putExtra("status", details.get(i).getStatus());
                                intent.putExtra("total", String.valueOf(details.get(i).getTotal()));
                                intent.putExtra("transportFee", details.get(i).getTransportFee());
                                intent.putExtra("id", details.get(i).getId());
                                startActivity(intent);


                            }
                        });

                    }
                    rvHistory.setAdapter(historyAdapter);
                    rvHistory.hasFixedSize();
                    swipeRefreshLayout.setRefreshing(false);
                    checkViewHideShow();
                    dialog.hideDialog();

                }

                @Override
                public void onFailure(Call<ResponeDetail> call, Throwable t) {
                    Log.e("Fail", t.getMessage());
                    AppUtil.showToast(self, "Fail");
                    dialog.hideDialog();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        } else {
            AppUtil.showToast(self, getString(R.string.msg_no_network));
        }
    }

    public void getHistory() {


    }

    @Override
    public void onRefresh() {
        details.clear();
        historyAdapter.notifyDataSetChanged();
        getData();
    }

    private void checkViewHideShow() {
        if (details.isEmpty()) {
            llnoData.setVisibility(View.VISIBLE);
        } else {
            llnoData.setVisibility(View.GONE);
        }
    }

    private void checkNetworkshow() {
        if (NetworkUtility.isNetworkAvailable()) {
            llnoConnect.setVisibility(View.GONE);
        } else {
            llnoConnect.setVisibility(View.VISIBLE);
        }
    }
}

