package com.aveplus.avepay_cpocket.music.modelmanager;

public interface ModelManagerListener {

    void onSuccess(Object object);

    void onError();

}
