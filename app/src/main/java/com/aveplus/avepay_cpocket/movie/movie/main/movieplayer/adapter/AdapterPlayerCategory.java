package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onFocusRow;

import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public class AdapterPlayerCategory extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<String> genres;
    private int mResource;
    private Context mContext;
    private onFocusRow focusRow;

    public AdapterPlayerCategory(Context context, int resource, List<String> genres,  onFocusRow focusRow) {
        this.genres = genres;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
        mContext = context;
        this.focusRow = focusRow;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;
        if(view==null){
            view = layoutInflater.inflate(mResource,null);
            holder = new Holder();
            holder.tv_name = (TextView) view.findViewById(R.id.tv_name);

            //set font
            AssetManager assetManager = mContext.getAssets();
            holder.tv_name.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));

            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }
        if(genres!=null){
            holder.tv_name.setText(genres.get(position));
        }
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if(keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER ) {
                        focusRow.onFocus(position);
                        return true;
                    }
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return genres.size();
    }

    @Override
    public Object getItem(int i) {
        return genres.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private class Holder{
        TextView tv_name;
    }
}
