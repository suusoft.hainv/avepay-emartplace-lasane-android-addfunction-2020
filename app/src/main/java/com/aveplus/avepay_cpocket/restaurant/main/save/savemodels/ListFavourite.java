package com.aveplus.avepay_cpocket.restaurant.main.save.savemodels;
/**
 * Created by suusoft.com on 11/12/19.
 */
import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListFavourite extends BaseModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("object_id")
    @Expose
    private Integer objectId;
    @SerializedName("object_type")
    @Expose
    private String objectType;
    @SerializedName("coupon_info")
    @Expose
    private Object couponInfo;
    @SerializedName("restaurant_info")
    @Expose
    private RestaurantInfo restaurantInfo;

    public ListFavourite() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public Object getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(Object couponInfo) {
        this.couponInfo = couponInfo;
    }

    public RestaurantInfo getRestaurantInfo() {
        return restaurantInfo;
    }

    public void setRestaurantInfo(RestaurantInfo restaurantInfo) {
        this.restaurantInfo = restaurantInfo;
    }
}
