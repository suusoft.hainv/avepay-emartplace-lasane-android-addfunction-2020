package com.aveplus.avepay_cpocket.movie.movie.main.movies.model;

import android.net.Uri;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YTParser extends BaseYTParser {

	private final static String TAG = "YTParser";

	final static String UTF8 = "UTF-8";

	// http://en.wikipedia.org/wiki/YouTube#Quality_and_codecs
    // keep it in order hi->lo
    public enum VideoQuality {
        p3072, p2304, p1080, p720, p520, p480, p360, p270, p240, p224, p144
    }

	static final Map<Integer, VideoQuality> itagMap = new HashMap<Integer, VideoQuality>() {
		private static final long serialVersionUID = -6925194111122038477L;
		{
			put(120, VideoQuality.p720);
			put(102, VideoQuality.p720);
			put(101, VideoQuality.p360);
			put(100, VideoQuality.p360);
			put(85, VideoQuality.p520);
			put(84, VideoQuality.p720);
			put(83, VideoQuality.p240);
			put(82, VideoQuality.p360);
			put(46, VideoQuality.p1080);
			put(45, VideoQuality.p720);
			put(44, VideoQuality.p480);
			put(43, VideoQuality.p360);
			put(38, VideoQuality.p3072);
			put(37, VideoQuality.p1080);
			put(36, VideoQuality.p240);
			put(35, VideoQuality.p480);
			put(34, VideoQuality.p360);
			put(22, VideoQuality.p720);
			put(18, VideoQuality.p360);
			put(17, VideoQuality.p144);
			put(6, VideoQuality.p270);
			put(5, VideoQuality.p240);
		}
	};
	
	public static Map<String, String> getQueryMap(String qs) {
		try {
			qs = qs.trim();
			List<NameValuePair> list;
			Uri uri = Uri.parse(qs);
			list = URLEncodedUtils.parse(new URI(null, null, null, -1, null,
                    qs, null), UTF8);
			HashMap<String, String> map = new HashMap<String, String>();
			for (String paramName : uri.getQueryParameterNames()) {
				if (paramName != null) {
					String paramValue = uri.getQueryParameter(paramName);
					if (paramValue != null) {
						map.put(paramName, paramValue);
					}
				}
			}
//			for (NameValuePair p : list) {
//				map.put(p.getName(), p.getValue());
//			}
			return map;
		} catch (URISyntaxException e) {
			throw new RuntimeException(qs, e);
		}
	}

	/**
	 * allows to download age restricted videos
	 *
	 * @throws Exception
	 */
	public static List<VideoStream> extractEmbedded(String html) {
		
		List<VideoStream> sNextVideoURL = new ArrayList<VideoStream>();
		// stop
		Map<String, String> map = getQueryMap(html);
		if (!map.containsKey("status") || map.get("status").equals("fail")) {
			return null;
		}

		String url_encoded_fmt_stream_map;
		try {
			url_encoded_fmt_stream_map = URLDecoder.decode(
                    map.get("url_encoded_fmt_stream_map"), UTF8);
			extractUrlEncodedVideos(sNextVideoURL, url_encoded_fmt_stream_map);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sNextVideoURL;
	}

	public static List<VideoStream> streamCpature(String html){
		List<VideoStream> sNextVideoURL = new ArrayList<VideoStream>();

		try {
			extractHtmlInfo(sNextVideoURL, html);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sNextVideoURL;
	}
	
	
	static void extractUrlEncodedVideos(List<VideoStream> sNextVideoURL,
			String sline) throws Exception {
		String[] urlStrings = sline.split("url=");

		for (String urlString : urlStrings) {

			String urlFull = URLDecoder.decode(urlString, UTF8);
			{
				String url = null;
				{
					Pattern link = Pattern.compile("([^&,]*)[&,]");
					Matcher linkMatch = link.matcher(urlString);
					if (linkMatch.find()) {
						url = linkMatch.group(1);
						url = URLDecoder.decode(url, UTF8);
					}
				}

				String itag = null;
				{
					Pattern link = Pattern.compile("itag=(\\d+)");
					Matcher linkMatch = link.matcher(urlFull);
					if (linkMatch.find()) {
						itag = linkMatch.group(1);
					}
				}

				String sig = null;

				if (sig == null) {
					Pattern link = Pattern.compile("&signature=([^&,]*)");
					Matcher linkMatch = link.matcher(urlFull);
					if (linkMatch.find()) {
						sig = linkMatch.group(1);
					}
				}

				if (sig == null) {
					Pattern link = Pattern.compile("sig=([^&,]*)");
					Matcher linkMatch = link.matcher(urlFull);
					if (linkMatch.find()) {
						sig = linkMatch.group(1);
					}
				}

				if (sig == null) {
					Pattern link = Pattern.compile("[&,]s=([^&,]*)");
					Matcher linkMatch = link.matcher(urlFull);
					if (linkMatch.find()) {
						sig = linkMatch.group(1);
					}
				}

				System.err.println(urlString);

				if (url != null && itag != null && sig != null) {
					try {
						url += "&signature=" + sig;

						addVideo(sNextVideoURL, itag, new URL(url));
						continue;
					} catch (MalformedURLException e) {
						// ignore bad urls
					}
				}
			}
		}
	}
	
	static void extractHtmlInfo(List<VideoStream> sNextVideoURL,String html) throws Exception {
		{
			Pattern age = Pattern.compile("(verify_age)");
			Matcher ageMatch = age.matcher(html);
			if (ageMatch.find())
				throw new Exception();
		}

		{
			Pattern age = Pattern.compile("(unavailable-player)");
			Matcher ageMatch = age.matcher(html);
			if (ageMatch.find())
				throw new Exception();
		}

		{
			Pattern urlencod = Pattern
					.compile("\"url_encoded_fmt_stream_map\": \"([^\"]*)\"");
			Matcher urlencodMatch = urlencod.matcher(html);
			if (urlencodMatch.find()) {
				String url_encoded_fmt_stream_map;
				url_encoded_fmt_stream_map = urlencodMatch.group(1);

				// normal embedded video, unable to grab age restricted videos
				Pattern encod = Pattern.compile("url=(.*)");
				Matcher encodMatch = encod.matcher(url_encoded_fmt_stream_map);
				if (encodMatch.find()) {
					String sline = encodMatch.group(1);

					extractUrlEncodedVideos(sNextVideoURL, sline);
				}

				// stream video
				Pattern encodStream = Pattern.compile("stream=(.*)");
				Matcher encodStreamMatch = encodStream
						.matcher(url_encoded_fmt_stream_map);
				if (encodStreamMatch.find()) {
					String sline = encodStreamMatch.group(1);

					String[] urlStrings = sline.split("stream=");

					for (String urlString : urlStrings) {

						Pattern link = Pattern
								.compile("(sparams.*)&itag=(\\d+)&.*&conn=rtmpe(.*),");
						Matcher linkMatch = link.matcher(urlString);
						if (linkMatch.find()) {

							String sparams = linkMatch.group(1);
							String itag = linkMatch.group(2);
							String url = linkMatch.group(3);

							url = "http" + url + "?" + sparams;

							url = URLDecoder.decode(url, UTF8);

							addVideo(sNextVideoURL, itag, new URL(url));
						}
					}
				}
			}
		}

		{
			Pattern title = Pattern
					.compile("<meta name=\"title\" content=(.*)");
			Matcher titleMatch = title.matcher(html);
			if (titleMatch.find()) {
				String sline = titleMatch.group(1);
				String name = sline.replaceFirst(
						"<meta name=\"title\" content=", "").trim();
			}
		}
	}

	/**
	 * Add resolution video for specific youtube link.
	 * 
	 * @param url
	 *            download source url
	 * @throws MalformedURLException
	 */
	static void addVideo(List<VideoStream> sNextVideoURL, String itag, URL url) {
		Integer i = Integer.decode(itag);
		VideoQuality vd = itagMap.get(i);

		sNextVideoURL.add(new VideoStream(vd, url));
	}
}
