package com.aveplus.avepay_cpocket.music.listener;

import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public interface IDataChangedListener {
    void onListDataChanged(List<?> data, boolean isAppend);
}
