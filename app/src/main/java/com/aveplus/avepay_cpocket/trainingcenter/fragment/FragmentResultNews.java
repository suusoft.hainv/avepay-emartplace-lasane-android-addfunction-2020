package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.activity.HomeActivity;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.NewEventAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshBase;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshListView;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

/**
 * Created by suusoft on 06/09/2017.
 */

public class FragmentResultNews extends BaseFragment {
    private View view;
    private ArrayList<NewEvent> arraylist = new ArrayList<NewEvent>();
    private NewEventAdapter adapter = null;
    private ListView listview;
    private PullToRefreshListView lGv;
    private int currentPage = 1, totalPage;
    private EditText edtSearch;
    private TextView btnCancel;
    private boolean isPullRefresh = true;
    private HomeActivity mEducationActivity;
    private LinearLayout llParent;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof HomeActivity) {
            mEducationActivity = (HomeActivity) activity;

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_result_news, container, false);
        mEducationActivity.productOrNewsBlog = HomeActivity.NEWS_BLOG;
        mEducationActivity.hideHeader();
        initView(view);
        refreshGridView(true);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mEducationActivity.showHeader();
    }

    public void initView(View view) {
        lGv = (PullToRefreshListView) view.findViewById(R.id.lvNewEvent);
        listview = lGv.getRefreshableView();
        llParent = (LinearLayout) view.findViewById(R.id.llParent);
        edtSearch = (EditText) view.findViewById(R.id.edtSearchNews);
        closeKeyboardWhenTouchOutside(getActivity(), llParent);
        edtSearch.setSelected(true);
        edtSearch.setFocusable(true);
        edtSearch.requestFocus();
        showKeyboard(mEducationActivity,edtSearch);
        btnCancel = (TextView) view.findViewById(R.id.btnClose);
        setControl();
    }

    private void initListView() {
        adapter = new NewEventAdapter(getActivity(), R.layout.item_product, arraylist);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


            }
        });
    }

    private void setControl() {

        initListView();
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getAllNews(1, true, edtSearch.getText().toString());
                    hideSoftKeyboard(mEducationActivity);
                }
                return false;
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        lGv.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                String label = DateUtils.formatDateTime(getActivity(),
                        System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
                                | DateUtils.FORMAT_SHOW_DATE
                                | DateUtils.FORMAT_ABBREV_ALL);
                refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
                refreshGridView(true);
            }

            @Override
            public void onPullUpToRefresh(
                    PullToRefreshBase<ListView> refreshView) {
                if (currentPage < totalPage) {
                    loadMorePage();
                } else {
                    showNoMoreData();
                }
            }
        });
    }


    private void refreshGridView(boolean isPull) {
        currentPage = 1;
        getAllNews(currentPage, isPull, edtSearch.getText().toString());
    }

    private void loadMorePage() {
        currentPage++;
        getAllNews(currentPage, isPullRefresh, edtSearch.getText().toString());
    }

    private void getAllNews(final int currentPage2, boolean isPull, String key) {
        ModelManager.getListNewsByCateAndKey("", key, String.valueOf(currentPage2), new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (currentPage2 < 2) {
                    arraylist.clear();
                }
                arraylist.addAll(ParseUtility.parseListNews(response.getRootStr()));
                totalPage = ParseUtility.getAllPage(response.getRootStr());
                adapter.notifyDataSetChanged();
                closeLoadingPull();
                if (arraylist.isEmpty()) {
                    showToast(R.string.not_found_data);
                }
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self,message);
            }
        });
    }

    private void showNoMoreData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showToast(R.string.endPage);
                lGv.onRefreshComplete();
            }
        }, 1000);
    }

    protected void showToast(int idString) {
        Toast.makeText(getActivity(), idString, Toast.LENGTH_SHORT).show();
    }

    private void closeLoadingPull() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                lGv.onRefreshComplete();
            }
        }, 1000);
    }

    public void closeKeyboardWhenTouchOutside(final Activity act, View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(act);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                closeKeyboardWhenTouchOutside(act, innerView);
            }
        }
    }

    public void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public static void showKeyboard(Context ctx, EditText editText) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }
}
