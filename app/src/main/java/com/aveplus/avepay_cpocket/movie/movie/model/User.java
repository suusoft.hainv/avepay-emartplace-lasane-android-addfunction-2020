package com.aveplus.avepay_cpocket.movie.movie.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


public class User {

    private String id, firstName, lastName, name, profileLink, profilePicture, email;
    @SerializedName("avatar")
    public String image;
    public User() {
    }

    public User(String id, String firstName, String lastName, String name, String profileLink, String profilePicture, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.name = name;
        this.profileLink = profileLink;
        this.profilePicture = profilePicture;
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileLink() {
        return profileLink;
    }

    public void setProfileLink(String profileLink) {
        this.profileLink = profileLink;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toJSon(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
