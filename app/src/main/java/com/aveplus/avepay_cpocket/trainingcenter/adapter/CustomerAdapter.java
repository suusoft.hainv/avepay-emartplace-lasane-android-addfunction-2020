package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.Customers;

import java.util.ArrayList;

public class CustomerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Customers> objects;
    private LayoutInflater inflater;

    public CustomerAdapter(Context context, ArrayList<Customers> objects) {
        super();
        this.context = context;
        this.objects = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_customers, null);
            vh = new ViewHoler();
            vh.txtCustomName = (TextView) rowView
                    .findViewById(R.id.txtCustomName);
            vh.txtCustomInfo = (TextView) rowView
                    .findViewById(R.id.txtCustomInfo);
            vh.txtCustomDecs = (TextView) rowView
                    .findViewById(R.id.txtCustomDesc);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        final Customers c = objects.get(position);
        if (c != null) {
            vh.txtCustomName.setText(c.getName());
            vh.txtCustomInfo.setText(c.getDate());
            vh.txtCustomDecs.setText(c.getInformation());
        }
        return rowView;
    }

    static class ViewHoler {
        public TextView txtCustomName, txtCustomInfo, txtCustomDecs;
    }
}
