package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.FragmentDetailsChapterPdfBinding;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.configs.GlobalFunction;
import com.aveplus.avepay_cpocket.ebook.listener.IOnRefresh;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.util.StringUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.DetailsBookActivity;
import com.aveplus.avepay_cpocket.ebook.view.activity.OpenChapterActivity;
import com.aveplus.avepay_cpocket.ebook.view.adapter.ChapterAdapter;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class FragmentDetailsChapterPDF extends BaseFragmentBinding implements OnPageChangeListener, OnLoadCompleteListener {
    private static final String TAG = "PDF";
    private PDFView pdfView;
    private Chapter chapter;
    private ProgressDialog mProgressDialog;
    private DetailsChapterPDFVM viewModel;
    private FragmentDetailsChapterPdfBinding binding;
    private AlertDialog alertDialogReload;
    private AsyncTask<Void, Integer, Boolean> downloadAsyntask;
    private TextView tvIndicator;
    private boolean continueRead = false;

    private int curPage = 0;
    private IOnRefresh onRefresh;

    private DetailsBookActivity detailsBookActivity;
    private OpenChapterActivity openChapterActivity;

    private int flag;
    private boolean isDownload;

    public static FragmentDetailsChapterPDF newInstance(Chapter chapter, Book book, int flag) {
        Bundle args = new Bundle();
        args.putParcelable(Constant.KEY_CHAPTER, chapter);
        args.putParcelable(Constant.KEY_BOOK, book);
        FragmentDetailsChapterPDF fragment = new FragmentDetailsChapterPDF();
        fragment.setArguments(args);
        fragment.flag = flag;
        return fragment;
    }


    public static FragmentDetailsChapterPDF newInstance(Chapter chapter, boolean isDownload) {
        Bundle args = new Bundle();
        args.putParcelable(Constant.KEY_CHAPTER, chapter);
        args.putBoolean(Constant.KEY_IS_DOWNLOAD, isDownload);
        FragmentDetailsChapterPDF fragment = new FragmentDetailsChapterPDF();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DetailsBookActivity)
            detailsBookActivity = (DetailsBookActivity) context;
        else if (context instanceof OpenChapterActivity)
            openChapterActivity = (OpenChapterActivity) context;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_details_chapter_pdf;
    }

    @Override
    protected void initialize() {
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            chapter = bundle.getParcelable(Constant.KEY_CHAPTER);
            isDownload = bundle.getBoolean(Constant.KEY_IS_DOWNLOAD);
            GlobalFunction.addCountReadBook(self, chapter.getBookId());
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new DetailsChapterPDFVM(self, chapter);
        return viewModel;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        this.binding = (FragmentDetailsChapterPdfBinding) binding;
        this.binding.setViewModel(viewModel);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.details_chapter_epub_pdf_normal, menu);
        menu.findItem(R.id.action_bookmarks).setVisible(false);
        if (DataStoreManager.isBookMarks(chapter.getBookId(), Constant.LIST_BOOK_MARKS)) {
            menu.findItem(R.id.action_bookmarks).setIcon(R.drawable.ic_book_marks_yellow);
        } else {
            menu.findItem(R.id.action_bookmarks).setIcon(R.drawable.ic_book_marks_white);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_fullscreen:
                if (detailsBookActivity != null) detailsBookActivity.showFullScreen(true);
                if (openChapterActivity != null) openChapterActivity.showFullScreen(true);
                tvIndicator.setVisibility(View.GONE);
                return true;
            case R.id.action_bookmarks:
                if (DataStoreManager.isBookMarks(chapter.getBookId(), Constant.LIST_BOOK_MARKS)) {
                    if (detailsBookActivity != null) detailsBookActivity.deleteBookrMarks();
                    if (openChapterActivity != null) openChapterActivity.deleteBookrMarks();
                    item.setIcon(R.drawable.ic_book_marks_white);
                } else {
                    if (detailsBookActivity != null) detailsBookActivity.addBookrMarks();
                    if (openChapterActivity != null) openChapterActivity.addBookrMarks();
                    item.setIcon(R.drawable.ic_book_marks_yellow);
                }
                return true;
            case R.id.action_move:
                showDialogMove();
                return true;
            case R.id.action_comment:
                ((FragmentActivity) self).getSupportFragmentManager().beginTransaction()
                        .add(R.id.content_details, FragmentComment.newInstance(chapter), Constant.FRAGMENT_COMENT)
                        .addToBackStack(null)
                        .commit();
                return true;
        }
        return false;
    }

    @Override
    protected void initView(View view) {
        if (chapter != null) {
            Log.e(TAG, chapter.getAttachment());
            tvIndicator = (TextView) view.findViewById(R.id.tv_indicator);
            tvIndicator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialogMove();
                }
            });

            File file = new File(chapter.getSdcardURL());
            if (file.exists()) {
                if (flag == ChapterAdapter.CONTINUES)
                    readContinues();
                else
                    startRead();
            } else {
                download();
            }
            //download();
        }
        if (detailsBookActivity != null) detailsBookActivity.setFooterChapter(tvIndicator);
        if (openChapterActivity != null) openChapterActivity.setFooterChapter(tvIndicator);
    }

    private void showDialogMove() {
        AlertDialog.Builder builder = new AlertDialog.Builder(self);
        final EditText input = new EditText(self);
        input.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
        input.setHint(R.string.input_page);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});

        AlertDialog alertDialog = builder.setTitle(R.string.app_name)
                .setMessage(R.string.move_to_page)
                .setView(input)
                .setNegativeButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String page = input.getText().toString();
                                if (pdfView != null && !StringUtil.isEmpty(page)) {
                                    pdfView.fromUri(Uri.parse(chapter.getSdcardURL()))
                                            .defaultPage(Integer.parseInt(page) - 1)
                                            .onPageChange(FragmentDetailsChapterPDF.this)
                                            .enableAnnotationRendering(true)
                                            .onLoad(FragmentDetailsChapterPDF.this)
                                            .scrollHandle(new DefaultScrollHandle(self))
                                            .load();
                                }
                            }
                        })
                .setPositiveButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
        alertDialog.show();
    }

    private void showDialogContinue() {
        if (Integer.parseInt(DataStoreManager.getIndexChapter(self, this.chapter)) == -1) {
            continueRead = false;
            openPDF();
        } else {
            new AlertDialog.Builder(self)
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            continueRead = false;
                            openPDF();
                        }
                    })
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.message_continue_read)
                    .setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            continueRead = true;
                            openPDF();
                        }
                    })
                    .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            continueRead = false;
                            openPDF();
                        }
                    })
                    .create()
                    .show();
        }
    }

    private void startRead() {
        continueRead = false;
        openPDF();
    }

    private void readContinues() {
        continueRead = true;
        openPDF();
    }

    public void download() {
        downloadAsyntask = new AsyncTask<Void, Integer, Boolean>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(self);
                mProgressDialog.setMessage(self.getString(R.string.downloading));
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((FragmentActivity) self).onBackPressed();
                    }
                });


            }

            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    String downloadUrl = chapter.getAttachment();
                    String downloadPath = getSdCardUrl();

                    long downloadedLength = 0;

                    File file = new File(downloadPath);
                    URL url = new URL(downloadUrl);

                    BufferedInputStream inputStream = null;
                    BufferedOutputStream outputStream = null;

                    URLConnection connection = url.openConnection();

                    if (file.exists()) {
                        downloadedLength = file.length();
                        connection.setRequestProperty("Range", "bytes=" + downloadedLength + "-");
                        outputStream = new BufferedOutputStream(new FileOutputStream(file, true));
                    } else {
                        outputStream = new BufferedOutputStream(new FileOutputStream(file));

                    }

                    connection.connect();

                    if (connection.getContentLength() <= 0) {
                        return true;
                    } else {
                        ((Activity) self).runOnUiThread(new Runnable() {
                            public void run() {
                                if (mProgressDialog.isShowing()) {
                                    mProgressDialog.dismiss();
                                    mProgressDialog.show();
                                } else {
                                    mProgressDialog.show();
                                }

                            }
                        });
                    }
                    inputStream = new BufferedInputStream(connection.getInputStream());

                    byte[] buffer = new byte[1024 * 8];
                    int byteCount;

                    long total = 0;

                    int fileLength = connection.getContentLength();
                    Log.e("eee", "File Length: " + fileLength);
                    while ((byteCount = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, byteCount);

                        total += byteCount;
                        if (fileLength > 0) {
                            Log.e("eee", "progres: " + (int) (total * 100 / fileLength));
                            publishProgress((int) (total * 100 / fileLength));
                        }
                    }

                    inputStream.close();
                    outputStream.flush();
                    outputStream.close();
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                Log.e("eee", "" + values[0] + " %");
                mProgressDialog.setProgress(values[0]);
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                super.onPostExecute(aVoid);
                mProgressDialog.dismiss();
                if (aVoid) {
                    if (flag == ChapterAdapter.START) {
                        startRead();
                    } else if (flag == ChapterAdapter.CONTINUES) {
                        readContinues();
                    }
//                    showDialogContinue();
                    openPDF();
                } else {
                    showDialodDownloadFailed();
                }
                Log.e("eee", "End");
                System.gc();
                Runtime.getRuntime().gc();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
        if (pdfView != null && chapter != null) {
            DataStoreManager.saveIndexChapter(self, chapter, String.valueOf(pdfView.getCurrentPage()));
        }

        if (null != mProgressDialog) {
            mProgressDialog.dismiss();
        }
        if (null != alertDialogReload) {
            alertDialogReload.dismiss();
        }
        if (null != downloadAsyntask) {
            downloadAsyntask.cancel(true);
        }

    }

    public void openPDFtoPage() {
        pdfView = binding.pdfView;

        pdfView.fromUri(Uri.parse(getSdCardUrl()))
                .defaultPage(curPage)
                .onPageChange(FragmentDetailsChapterPDF.this)
                .enableAnnotationRendering(true)
                .onLoad(FragmentDetailsChapterPDF.this)
                .scrollHandle(new DefaultScrollHandle(self))
                .load();
    }

    public void openPDF() {
        String page = "0";
        pdfView = binding.pdfView;

        if (continueRead) {
            page = DataStoreManager.getIndexChapter(self, this.chapter);
        }


        pdfView.fromUri(Uri.parse(getSdCardUrl()))
                .defaultPage(Integer.parseInt(page))
                .onPageChange(FragmentDetailsChapterPDF.this)
                .enableAnnotationRendering(true)
                .onLoad(FragmentDetailsChapterPDF.this)
                .scrollHandle(new DefaultScrollHandle(self))
                .load();
    }

    //change extension
    private String getSdCardUrl() {

        Log.e("pdf_path", chapter.getSdcardURL());
        return this.chapter.getSdcardURL();// + ".gdf" ; //thêm vào khi ko muốn pdf reader đọc được
    }

    public void showDialodDownloadFailed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(self);
        builder.setTitle(R.string.app_name)
                .setCancelable(false)
                .setMessage(R.string.error_LoadBook)
                .setNegativeButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        downloadPDF();
                        download();
                    }
                })
                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((FragmentActivity) self).onBackPressed();
                    }
                });

        alertDialogReload = builder.create();
        if (this.isVisible()) {
            if (!alertDialogReload.isShowing()) {
                alertDialogReload.show();
            }
        } else {
            alertDialogReload.dismiss();
        }
    }


    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));
            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        Log.e(TAG, "title = " + meta.getTitle());
        Log.e(TAG, "author = " + meta.getAuthor());
        Log.e(TAG, "subject = " + meta.getSubject());
        Log.e(TAG, "keywords = " + meta.getKeywords());
        Log.e(TAG, "creator = " + meta.getCreator());
        Log.e(TAG, "producer = " + meta.getProducer());
        Log.e(TAG, "creationDate = " + meta.getCreationDate());
        Log.e(TAG, "modDate = " + meta.getModDate());
        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        Log.e(TAG, " " + page + "  -  " + pageCount);
        tvIndicator.setText((page + 1) + "/" + pageCount);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (onRefresh != null)
            onRefresh.refresh();
    }

    public FragmentDetailsChapterPDF setOnRefresh(IOnRefresh onRefresh) {
        this.onRefresh = onRefresh;
        return this;
    }
}