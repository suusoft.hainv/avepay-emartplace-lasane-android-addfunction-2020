package com.aveplus.avepay_cpocket.ebook.listener;

/**
 * Created by Truong on 02/04/2018.
 */

public interface IConfirmation {

    void onPositive();

    void onNegative();
}
