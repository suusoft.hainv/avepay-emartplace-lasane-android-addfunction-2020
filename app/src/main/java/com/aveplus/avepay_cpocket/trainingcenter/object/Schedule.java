package com.aveplus.avepay_cpocket.trainingcenter.object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by suusoft on 29/08/2017.
 */

public class Schedule implements Parcelable {
    private String week;
    private String startTime;
    private String endTime;

    public Schedule(String week, String startTime, String endTime) {
        this.week = week;
        this.startTime = startTime;
        this.endTime = endTime;
    }


    protected Schedule(Parcel in) {
        week = in.readString();
        startTime = in.readString();
        endTime = in.readString();
    }

    public static final Creator<Schedule> CREATOR = new Creator<Schedule>() {
        @Override
        public Schedule createFromParcel(Parcel in) {
            return new Schedule(in);
        }

        @Override
        public Schedule[] newArray(int size) {
            return new Schedule[size];
        }
    };

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(week);
        dest.writeString(startTime);
        dest.writeString(endTime);
    }
}
