package com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.confirmorder;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.ConfirmAdapter;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.MycartActivity;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.OrderSuccessActivity;
import com.aveplus.avepay_cpocket.restaurant.retrofit.APIService;
import com.aveplus.avepay_cpocket.restaurant.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeCoupon;
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.ResponeOrder;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;
import com.aveplus.avepay_cpocket.restaurant.util.StringUtil;
import com.aveplus.avepay_cpocket.restaurant.widgets.dialog.ViewDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.MycartActivity.grandTotalplus;
import static com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.MycartActivity.itemModels;
import static com.aveplus.avepay_cpocket.restaurant.main.restaurant.order.MycartActivity.temparraylist;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.ID_RESTAURANT;


public class ConfirmActivity extends BaseActivity implements View.OnClickListener {
    private TextView tvName, tvPhone, tvAddress, tvTotalPart, tvTransport, tvTotal, txtCoupon,
            tvTotalCredit, tvTotalCash, tvCoupon, tvCode, tvBtnOder, tvQuantity, tvTime, tvApply, tvleft, tvRight;
    private ImageView imgAvatar, imgSave, imgEdit, imgClose;
    private RelativeLayout rlCoupon;
    private EditText edCode, edName, edPhone, edAddress;
    private static final String TAG = ConfirmActivity.class.getSimpleName();
    private RecyclerView rcFood;
    private ConfirmAdapter adapter;
    private BottomSheetDialog bottomSheetDialog;
    private int id_restaurant;

    @Override
    protected int getLayoutInflate() {
        return R.layout.confirmorder_new;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        id_restaurant = intent.getIntExtra(ID_RESTAURANT, 0);

    }


    @Override
    protected void initView() {
        setToolbarTitle("CONFIRM ORDER");
        imgEdit = findViewById(R.id.img_edit);
        imgSave = findViewById(R.id.img_save);
        imgAvatar = findViewById(R.id.img_address);
        tvName = findViewById(R.id.tv_name);
        tvPhone = findViewById(R.id.tv_phone);
        tvAddress = findViewById(R.id.tv_address);
        tvTotalPart = findViewById(R.id.tv_total_part);
        tvQuantity = findViewById(R.id.tv_quantity);
        tvTransport = findViewById(R.id.tv_transport);
        tvTotal = findViewById(R.id.tv_total);
        tvTotalCash = findViewById(R.id.tv_total_cash);
        tvCoupon = findViewById(R.id.tv_coupon);
        tvBtnOder = findViewById(R.id.btn_confirm);
        rlCoupon = findViewById(R.id.rl_coupon);
        tvTime = findViewById(R.id.tv_time);
        tvCode = findViewById(R.id.tv_coupon_code);
        rcFood = findViewById(R.id.rc_confirm);
        tvleft = findViewById(R.id.tv_text6);
        tvRight = findViewById(R.id.tv_text7);
        edName = findViewById(R.id.ed_name);
        edPhone = findViewById(R.id.ed_phone);
        edAddress = findViewById(R.id.ed_address);
        txtCoupon = findViewById(R.id.txt_coupon);

        String timeStamp = new SimpleDateFormat("yyyy/MM/dd - HH:mm").format(Calendar.getInstance().getTime());


        adapter = new ConfirmAdapter(temparraylist, self);
        rcFood.setAdapter(adapter);
        rcFood.hasFixedSize();
        tvBtnOder.setOnClickListener(this);
        rlCoupon.setOnClickListener(this);
        imgSave.setOnClickListener(this);
        imgEdit.setOnClickListener(this);
        tvName.setText(DataStoreManager.getUserModels().getName());
        tvAddress.setText(DataStoreManager.getUserModels().getAddress());
        tvPhone.setText(DataStoreManager.getUserModels().getPhone());
        tvTime.setText(timeStamp);
        tvTotalPart.setText("$" + grandTotalplus);
        grandTotalplus = grandTotalplus + 1;
        tvTransport.setText("$" + 1);
        tvTotalCash.setText(String.valueOf(grandTotalplus));
        tvTotal.setText("$" + grandTotalplus);


        // count part quantity
        int count = 0;
        for (int i = 0; i < temparraylist.size(); i++) {
            count += temparraylist.get(i).getQuantity();
        }
        tvQuantity.setText(String.valueOf(count));


    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_coupon:
                View v = LayoutInflater.from(this).inflate(R.layout.dialog_coupon, null);
                edCode = v.findViewById(R.id.ed_code);
                tvApply = v.findViewById(R.id.btn_apply);
                imgClose = v.findViewById(R.id.img_close);

                bottomSheetDialog = new BottomSheetDialog(this);
                bottomSheetDialog.setContentView(v);
                bottomSheetDialog.show();

                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheetDialog.dismiss();
                    }
                });

                TextWatcher textWatcher = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        tvApply.setTextColor(getResources().getColor(R.color.grey_light2));
                        tvApply.setBackgroundResource(R.drawable.elip_apply);
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        tvApply.setTextColor(getResources().getColor(R.color.white));
                        tvApply.setBackgroundResource(R.drawable.elip_apply_red);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (edCode.getText().length() != 0) {
                            tvApply.setTextColor(getResources().getColor(R.color.white));
                            tvApply.setBackgroundResource(R.drawable.elip_apply_red);
                        } else {
                            tvApply.setTextColor(getResources().getColor(R.color.grey_light2));
                            tvApply.setBackgroundResource(R.drawable.elip_apply);
                        }
                    }

                };
                edCode.addTextChangedListener(textWatcher);

                tvApply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ViewDialog dialog = new ViewDialog(ConfirmActivity.this);
                        dialog.showDialog();
                        String code = edCode.getText().toString();
                        APIService apiService = ApiUtils.getAPIService();
                        apiService.Coupon(DataStoreManager.getToken(), code).enqueue(new Callback<ResponeCoupon>() {
                            @Override
                            public void onResponse(Call<ResponeCoupon> call, Response<ResponeCoupon> response) {
                                if (response.body().getData() != null) {
                                    tvCoupon.setText("- $" + response.body().getData().getDiscountAmount());
                                    tvleft.setVisibility(View.VISIBLE);
                                    tvCode.setText(response.body().getData().getCode());
                                    tvCode.setVisibility(View.VISIBLE);
                                    tvRight.setVisibility(View.VISIBLE);
                                    int total = grandTotalplus - response.body().getData().getDiscountAmount();
                                    tvTotal.setText("$" + total);
                                    tvTotalCash.setText(String.valueOf(total));
                                    bottomSheetDialog.dismiss();
                                    txtCoupon.setVisibility(View.GONE);
                                    dialog.hideDialog();
                                } else {
                                    AppUtil.showToast(self, R.string.not_coupon);
                                    bottomSheetDialog.dismiss();
                                    dialog.hideDialog();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponeCoupon> call, Throwable t) {
                                Log.e(TAG, "onFailure: " + t.getMessage());
                                dialog.hideDialog();
                            }
                        });
                    }
                });

                break;

            case R.id.btn_confirm:
                Order();
                break;

            case R.id.img_edit:
                edName.setText(tvName.getText().toString());
                edPhone.setText(tvPhone.getText().toString());
                edAddress.setText(tvAddress.getText().toString());
                edName.setVisibility(View.VISIBLE);
                edPhone.setVisibility(View.VISIBLE);
                edAddress.setVisibility(View.VISIBLE);
                tvName.setVisibility(View.GONE);
                tvPhone.setVisibility(View.GONE);
                tvAddress.setVisibility(View.GONE);
                imgSave.setVisibility(View.VISIBLE);
                imgEdit.setVisibility(View.GONE);
                break;


            case R.id.img_save:

                tvName.setText(edName.getText().toString());
                tvPhone.setText(edPhone.getText().toString());
                edAddress.setText(edAddress.getText().toString());
                tvPhone.setVisibility(View.VISIBLE);
                tvName.setVisibility(View.VISIBLE);
                tvAddress.setVisibility(View.VISIBLE);
                edAddress.setVisibility(View.GONE);
                edPhone.setVisibility(View.GONE);
                edName.setVisibility(View.GONE);
                imgSave.setVisibility(View.GONE);
                imgEdit.setVisibility(View.VISIBLE);

                break;
        }
    }

    private void Order() {
        ViewDialog dialog = new ViewDialog(this);
        dialog.showDialog();

        Gson gson = new Gson();
        String item = gson.toJson(itemModels);
        String payment = "Cash on delivery";
        int restautan_id = id_restaurant;

        String name = tvName.getText().toString();
        String address = tvAddress.getText().toString();
        String phone = tvPhone.getText().toString();
        int user_id = DataStoreManager.getUserModels().getId();
        int total = Integer.parseInt(tvTotalCash.getText().toString());
        if (StringUtil.isEmpty(name)) {
            AppUtil.showToast(self, getString(R.string.name_not_null));
            dialog.hideDialog();
            return;
        }
        if (StringUtil.isEmpty(address)) {
            AppUtil.showToast(self, getString(R.string.address_not_null));
            dialog.hideDialog();
            return;
        }
        if (StringUtil.isEmpty(phone)) {
            AppUtil.showToast(self, getString(R.string.phone_not_null));
            dialog.hideDialog();
            return;
        }

        APIService apiService = ApiUtils.getAPIService();
        apiService.Order(restautan_id, user_id, name, phone, address, total, payment, item).enqueue(new Callback<ResponeOrder>() {
            @Override
            public void onResponse(Call<ResponeOrder> call, Response<ResponeOrder> response) {
                DataStoreManager.saveOder(response.body().getData());
                String status = response.body().getStatus();
                dialog.hideDialog();
                grandTotalplus = 0;
                MycartActivity.restaurantid = 0;
                AppUtil.startActivity(ConfirmActivity.this, OrderSuccessActivity.class);
                Toast.makeText(ConfirmActivity.this, "ORDER  " + status, Toast.LENGTH_SHORT).show();
                finishAffinity();
            }

            @Override
            public void onFailure(Call<ResponeOrder> call, Throwable t) {
                AppUtil.showToast(getApplicationContext(), t.getMessage());
                Log.e("Error: ", t.getMessage());
                dialog.hideDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
