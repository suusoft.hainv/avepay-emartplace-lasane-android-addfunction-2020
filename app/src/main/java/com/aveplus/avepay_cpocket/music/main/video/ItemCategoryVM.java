package com.aveplus.avepay_cpocket.music.main.video;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.configs.Constant;


public class ItemCategoryVM extends BaseAdapterVM {
    private Category category;
    public ItemCategoryVM(Context self, Category category) {
        super(self);
        this.category = category;
    }
    public String getImage() {
        return category.getImg_thumb();
    }

    public String getName() {
        return category.getName();
    }

    @Override
    public void setData(Object object) {

    }

    public void onItemClicked(View view){
      //  Toast.makeText(self, ""+category.getName(), Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.PREF_KEY_OBJECT, category);
        Intent intent = new Intent(self, ListVideoActivity.class);
        intent.putExtra(Constant.PREF_KEY_DATA,bundle);
        self.startActivity(intent);

    }
}
