package com.aveplus.avepay_cpocket.movie.movie.configs;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

/**
 * Copyright © 2019 SUUSOFT
 *
 */
public class Apis {
    public static final int REQUEST_TIME_OUT = 15000;
    // Domain
    private static final String APP_DOMAIN_LOGIN = AppController.getInstance().getString(R.string.URL_API)+"movie/backend/web/index.php/api/";
    public static final String URL_REGISTER_DEVICE = APP_DOMAIN_LOGIN + "device/index";
    // Urls
  //  public static final String URL_REGISTER_DEVICE = APP_DOMAIN + "device";
    public static final String URL_LOGIN = APP_DOMAIN_LOGIN + "user/login";
//    public static final String URL_LOGOUT = APP_DOMAIN + "logout";
//    public static final String URL_REGISTER = APP_DOMAIN + "register";
//    public static final String URL_RESET_PASSWORD = APP_DOMAIN + "resetPassword";
//    public static final String URL_GET_ALL = APP_DOMAIN + "browseTask";

    //temp
    public static final String API_BROWTASK = "browseTask";
    //public static final String URL_TASK_DETAIL = APP_DOMAIN + "taskDetail";

}
