package com.aveplus.avepay_cpocket.taxi.retrofit;


import com.aveplus.avepay_cpocket.taxi.retrofit.response.BaseModel;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseIpn;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseTripObj;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseTypeDriver;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseUser;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_ADDRESS;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_BillMapTransactionId;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_EMAIL;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_EWPTransactionId;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_FROM;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_GCM_ID;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_IS_DRIVER;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_NAME;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_ResponseCode;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_ResponseMessage;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_TOKEN;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_TYPE;
import static com.aveplus.avepay_cpocket.taxi.retrofit.param.Param.PARAM_USER_ID;


public interface APIService {

    @POST("/posts")
    @FormUrlEncoded
    Call<Object> savePost(@Field("title") String title,
                          @Field("body") String body,
                          @Field("userId") long userId);


    @GET("transport/trip-status")
    @Headers("Cache-Control: no-cache")
        // no cache
    Call<ResponseTripObj> getTripStatus(@Query(PARAM_USER_ID) String userId,
                                        @Query(PARAM_IS_DRIVER) String is_driver,
                                        @Query(PARAM_TOKEN) String token);


    @GET("ipn/index")
    Call<ResponseIpn> ipn(@Query(PARAM_FROM) String from,
                          @Query(PARAM_TOKEN) String token,
                          @Query(PARAM_BillMapTransactionId) String billMapTransactionId,
                          @Query(PARAM_EWPTransactionId) String eWPTransactionId,
                          @Query(PARAM_ResponseCode) int responseCode,
                          @Query(PARAM_ResponseMessage) String responseMessage);


    @GET("device/index")
    @Headers("Cache-Control:no-cache")
        // no cache
    Call<BaseModel> updateGcmId(@Query(PARAM_TOKEN) String token,
                                @Query(PARAM_GCM_ID) String gcm_id,
                                @Query(PARAM_TYPE) String type);


    @Multipart
    @POST("user/update-profile")
    @Headers("Cache-Control:no-cache")
        // no cache
    Call<ResponseUser> updateProfileNormal(@Query(PARAM_TOKEN) String token,
                                           @Query(PARAM_NAME) String name,
                                           @Query(PARAM_ADDRESS) String address,
                                           @Query(PARAM_EMAIL) String email,
                                           @Part MultipartBody.Part file);


    @GET("user/driver-price")
    @Headers("Cache-Control:no-cache")
        // no cache
    Call<ResponseTypeDriver> getListTypeDriver(@Query(PARAM_TOKEN) String token);


}
