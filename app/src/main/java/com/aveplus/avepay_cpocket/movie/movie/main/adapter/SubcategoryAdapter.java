package com.aveplus.avepay_cpocket.movie.movie.main.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieGenre;

import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public class SubcategoryAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<MovieGenre> genres;
    private int mResource;
    private Context mContext;
    public SubcategoryAdapter(Context context, int resource, List<MovieGenre> genres) {
        //super(context, resource, genres);
        this.genres = genres;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;
        if(view==null){
            view = layoutInflater.inflate(mResource,null);
            holder = new Holder();
            holder.tv_name = (TextView) view.findViewById(R.id.tv_name);

            //set font
            AssetManager assetManager = mContext.getAssets();
            holder.tv_name.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));

            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }
        if(genres!=null){
            holder.tv_name.setText(genres.get(position).getName());
        }
        holder.tv_name.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                Toast.makeText(mContext,"KEY DOWN", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return genres.size();
    }


    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private class Holder{
        TextView tv_name;
    }
}
