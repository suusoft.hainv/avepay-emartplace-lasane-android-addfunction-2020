package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.interfaces.IRedeem;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewLightItalic;

import org.json.JSONObject;

/**
 * Created by SuuSoft.com on 05/12/2016.
 */

public class FidelityActivity extends com.aveplus.avepay_cpocket.base.BaseActivity implements View.OnClickListener, IRedeem {
    private static final String ACTION_REDEEM = "redeem";
    private static final String TAG = FidelityActivity.class.getName();
    private TextView tvCreditsAvailable;
    private EditText edtCredits;
    private TextViewLightItalic tvInfo;
    private TextViewBold btnRedeem;
    private EditText edtDescription;
    private TextView tvBalance;
    private UserObj user;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_fidelity;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.onPause();
    }

    @Override
    protected void initilize() {
        user = DataStoreManager.getUser();
    }

    @Override
    protected void initView() {
        setToolbarTitle(R.string.fidelity_account);
        AppController.getInstance().setiRedeem2(this);
        tvCreditsAvailable = findViewById(R.id.tv_credits);
        edtCredits = (EditText) findViewById(R.id.edt_credits);
        tvInfo = (TextViewLightItalic) findViewById(R.id.tv_info);
        btnRedeem = (TextViewBold) findViewById(R.id.btn_functions);
        edtDescription = (EditText) findViewById(R.id.edt_description);
        tvBalance = findViewById(R.id.tv_balance);

        tvBalance.setText(getString(R.string.bonus_pay));

    }

    @Override
    protected void onViewCreated() {
        btnRedeem.setOnClickListener(this);
        edtCredits.requestFocus();
//        tvCreditsAvailable.setText(AppUtil.formatCurrency(user.getBalance()));
        if (NetworkUtility.getInstance(this).isNetworkAvailable()) {
            getProfile();
        } else {
            AppUtil.showToast(this, R.string.msg_network_not_available);
        }

    }

    @Override
    public void onClick(View view) {
        if (view == btnRedeem) {
            if (NetworkUtility.getInstance(getApplicationContext()).isNetworkAvailable()) {
                if (isValid()) {
                    redeem();
                }
            } else {
                AppUtil.showToast(getApplicationContext(), R.string.msg_network_not_available);
            }
        }
    }

    private void redeem() {
        ModelManager.transaction(this, "", edtCredits.getText().toString(), ACTION_REDEEM, edtCredits.getText().toString(), "", "", new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                ApiResponse apiResponse = new ApiResponse(jsonObject);
                if (!apiResponse.isError()) {
                    AppUtil.showToast(getApplicationContext(), R.string.msg_transaction_success);
                    finish();
                } else {
                    AppUtil.showToast(getApplicationContext(), apiResponse.getMessage());
                }
            }

            @Override
            public void onError() {
                Log.e(TAG, "REDEEM: Error!");
            }
        });
    }

    private boolean isValid() {
        String amount = edtCredits.getText().toString();
        String description = edtDescription.getText().toString();
        String numCredits = tvCreditsAvailable.getText().toString().replaceAll(",", "");
//        DecimalFormat format = new DecimalFormat();
//        Number number = null;
//        try {
//            number = format.parse(numCredits);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        if (StringUtil.isEmpty(amount)) {
            AppUtil.showToast(getApplicationContext(), R.string.msg_amout_is_require);
            edtCredits.requestFocus();
            return false;
        } else {
            int amountCredit = Integer.parseInt(amount);
            if (amountCredit <= 0) {
                AppUtil.showToast(getApplicationContext(), R.string.msg_value_credits);
                edtCredits.requestFocus();
                return false;
            } else if (amountCredit > Double.parseDouble(numCredits)) {
                AppUtil.showToast(getApplicationContext(), R.string.msg_enought_credits);
                return false;
            }
        }
        if (StringUtil.isEmpty(description)) {
            AppUtil.showToast(getApplicationContext(), R.string.msg_description_is_require);
            edtDescription.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void updateBalace(final float balance) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvCreditsAvailable.setText(StringUtil.convertNumberPrice(balance));
//                tvCreditsAvailable.setText(AppUtil.formatCurrency(balance));
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().setiRedeem(null);
    }

    private void getProfile() {

        RequestManager.getProfile(self, new BaseRequest.CompleteListener() {

            @Override
            public void onSuccess(com.aveplus.avepay_cpocket.taxi.network.ApiResponse response) {
                UserObj userObj = response.getDataObject(UserObj.class);
                UserObj user = DataStoreManager.getUser();
                user.setBonus(userObj.getBonus());
                DataStoreManager.saveUser(user);
                tvCreditsAvailable.setText(StringUtil.convertNumberPrice(user.getBonus()));
            }

            @Override
            public void onError(String message) {

            }
        });
    }
}
