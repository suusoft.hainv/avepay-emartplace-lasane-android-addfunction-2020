package com.aveplus.avepay_cpocket.movie.movie.listener;

import android.view.View;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface IOnItemClickedListener {
    public void onItemClicked(View view, int position);
}
