package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aveplus.avepay_cpocket.R;

import java.util.ArrayList;

/**
 * Created by suusoft on 8/15/2017.
 */
public class TabSilderAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> listPage;
    Context mCt;

    public TabSilderAdapter(Context ct, FragmentManager fm, ArrayList<Fragment> listPage) {
        super(fm);
        this.mCt = ct;
        this.listPage = listPage;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mCt.getResources().getString(R.string.recent);
            case 1:
                return mCt.getResources().getString(R.string.popular);
            case 2:
                return mCt.getResources().getString(R.string.featured);
            default:
                return "";
        }

    }

    @Override
    public int getCount() {
        return listPage.size();
    }

    @Override
    public Fragment getItem(int position) {
        return listPage.get(position);
    }
}