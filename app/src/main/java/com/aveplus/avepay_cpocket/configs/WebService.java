package com.aveplus.avepay_cpocket.configs;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

/**
 *
 */
public class WebService {
    private static final String PROTOCOL_HTTP = "http://";
    private static final String BASE_URL = AppController.getInstance().getString(R.string.URL_API_MOVIE) + "backend/web/index.php/api/movie/movie-sub";
    private static final String BASE_URL_NEW = AppController.getInstance().getString(R.string.URL_API_MOVIE);
    //private static final String BASE_URL_NEW = "http://ktuiptv.com/tu3/";
    private static final String PROTOCOL_HTTPS = "https://";
    private static final String OMDB_API_KEY = "a2762bca";


    public static final int resultPerPage = 20;


    public static final String URL_MOVIE_INFO = PROTOCOL_HTTP + "omdbapi.com/?apikey=" + OMDB_API_KEY + "&i=%s";

    public static final String URL_MOVIE_BACKDROP = PROTOCOL_HTTP + "api.themoviedb.org/3/movie/%s?api_key=%s";
    /*Config for YT Data*/
    public static String YTURL = PROTOCOL_HTTPS + "youtube.com/watch?v=";
    public static String YTExtractURLEMBED = PROTOCOL_HTTPS + "youtube.com/get_video_info?authuser=0&video_id=%s&el=embedded&ps=default&eurl=&gl=US&hl=en";
    public static String YTExtractURLDetails = PROTOCOL_HTTPS + "youtube.com/get_video_info?authuser=0&video_id=%s&el=detailpage&ps=default&eurl=&gl=US&hl=en";
    public static String YTExtractURLVevo = PROTOCOL_HTTPS + "youtube.com/get_video_info?authuser=0&video_id=%s&el=vevo&ps=default&eurl=&gl=US&hl=en";

    //API for MOVIE TV
    public static final String URL_NEWEST_MOVIE = BASE_URL + "1stmovie";
    public static final String URL_NEW_MOVIES = BASE_URL + "newmovies";
    public static final String URL_MOVIES_BY_LANGUAGE = BASE_URL + "movies/%s";

    //public static final String URL_GENRES = BASE_URL+"genres";
    //suusoft.com/project = 192.168.100.9
    public static final String URL_GENRES = BASE_URL_NEW+"backend/web/index.php/api/movie/genre";
    public static final String URL_MOVIES = BASE_URL + "movies/%s";
    public static final String URL_MOVIES_SEARCH = BASE_URL + "movie-voya/search?title=%s";
    // public static final String URL_MOVIES_NEW= "http://192.168.100.9/movie-tuiptv/backend/web/index.php/api/movie/all?genre=";
    public static final String URL_MOVIES_NEW = BASE_URL_NEW+"backend/web/index.php/api/movie/all?&number_per_page=50" + "&token=";//+ AppController.getInstance().getToken() +"&action="+"genre="
    public static final String URL_MOVIES_NEW1 = BASE_URL_NEW+"backend/web/index.php/api/movie/all?&number_per_page=20" + "&token=";//+ AppController.getInstance().getToken() +"&action="+"genre="
    // http://suusoft-demo.com/projects/movie-tuiptv/backend/web/index.php

    public static final String URL_MOVIES_PERPAGE = BASE_URL + "moviesbycategory/%s/"
            + "%d/" + resultPerPage;
    public static final String URL_YOUTUBE_LIVE = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=%s&eventType=live&type=video&key=AIzaSyAhoQwIFJGM8QKGMXCFSgsJws0NhqZGsDk";
    public static final String URL_GOOGLE_DRIVE = "https://www.googleapis.com/drive/v3/files/%s?key=AIzaSyA0p6BC9c8XPn1v0JsSsi7H534FZ730X4w&alt=media";
}
