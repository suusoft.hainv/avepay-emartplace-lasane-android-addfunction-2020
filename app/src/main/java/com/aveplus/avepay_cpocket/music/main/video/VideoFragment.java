package com.aveplus.avepay_cpocket.music.main.video;

import android.view.View;
import android.widget.ImageView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragment;
import com.google.android.material.tabs.TabLayout;


import java.util.ArrayList;
import java.util.List;

public class VideoFragment extends BaseFragment {
    private ImageView imgBack;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerVideoAdapter adapter;
    List<Genre> list;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public static VideoFragment newInstance() {
        VideoFragment fragment = new VideoFragment();
        return fragment;
    }

    @Override

    protected int getLayoutInflate() {
        return R.layout.fragment_video;
    }

    @Override
    protected void init() {

    }


    @Override
    protected void initView(View view) {
        list = new ArrayList<>();
        list.add(new Genre(10000, "NEW"));
        list.add(new Genre(20000, "TRENDING"));
        list.add(new Genre(30000, "POPULAR"));
        list.add(new Genre(40000, "GENRE"));
        adapter = new ViewPagerVideoAdapter(self, getChildFragmentManager());
        imgBack = view.findViewById(R.id.c_btn_back);
        tabLayout = view.findViewById(R.id.video_tablayout);
        viewPager = view.findViewById(R.id.video_viewpager);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow, R.color.green, R.color.blue,R.color.black);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                adapter.setListGenre(list);
                adapter.notifyDataSetChanged();
                viewPager.setAdapter(adapter);
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        adapter.setListGenre(list);
        adapter.notifyDataSetChanged();
        viewPager.setAdapter(adapter);
       tabLayout.setupWithViewPager(viewPager);


    }

    @Override
    protected void getData() {

    }


}
