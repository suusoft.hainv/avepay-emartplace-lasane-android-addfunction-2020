package com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Review implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("author_id")
    @Expose
    private Integer authorId;
    @SerializedName("author_role")
    @Expose
    private String authorRole;
    @SerializedName("destination_id")
    @Expose
    private Integer destinationId;
    @SerializedName("destination_role")
    @Expose
    private String destinationRole;
    @SerializedName("object_id")
    @Expose
    private Integer objectId;
    @SerializedName("object_type")
    @Expose
    private String objectType;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("rate")
    @Expose
    private Integer rate;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("modified_date")
    @Expose
    private String modifiedDate;
    @SerializedName("author_avatar")
    @Expose
    private String authorAvatar;
    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("object_rate")
    @Expose
    private String objectRate;
    @SerializedName("object_rate_count")
    @Expose
    private String objectRateCount;

    protected Review(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            authorId = null;
        } else {
            authorId = in.readInt();
        }
        authorRole = in.readString();
        if (in.readByte() == 0) {
            destinationId = null;
        } else {
            destinationId = in.readInt();
        }
        destinationRole = in.readString();
        if (in.readByte() == 0) {
            objectId = null;
        } else {
            objectId = in.readInt();
        }
        objectType = in.readString();
        content = in.readString();
        if (in.readByte() == 0) {
            rate = null;
        } else {
            rate = in.readInt();
        }
        isActive = in.readString();
        createdDate = in.readString();
        modifiedDate = in.readString();
        authorAvatar = in.readString();
        authorName = in.readString();
        objectRate = in.readString();
        objectRateCount = in.readString();
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthorRole() {
        return authorRole;
    }

    public void setAuthorRole(String authorRole) {
        this.authorRole = authorRole;
    }

    public Integer getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Integer destinationId) {
        this.destinationId = destinationId;
    }

    public String getDestinationRole() {
        return destinationRole;
    }

    public void setDestinationRole(String destinationRole) {
        this.destinationRole = destinationRole;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getAuthorAvatar() {
        return authorAvatar;
    }

    public void setAuthorAvatar(String authorAvatar) {
        this.authorAvatar = authorAvatar;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getObjectRate() {
        return objectRate;
    }

    public void setObjectRate(String objectRate) {
        this.objectRate = objectRate;
    }

    public String getObjectRateCount() {
        return objectRateCount;
    }

    public void setObjectRateCount(String objectRateCount) {
        this.objectRateCount = objectRateCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (authorId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(authorId);
        }
        parcel.writeString(authorRole);
        if (destinationId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(destinationId);
        }
        parcel.writeString(destinationRole);
        if (objectId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(objectId);
        }
        parcel.writeString(objectType);
        parcel.writeString(content);
        if (rate == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(rate);
        }
        parcel.writeString(isActive);
        parcel.writeString(createdDate);
        parcel.writeString(modifiedDate);
        parcel.writeString(authorAvatar);
        parcel.writeString(authorName);
        parcel.writeString(objectRate);
        parcel.writeString(objectRateCount);
    }
}
