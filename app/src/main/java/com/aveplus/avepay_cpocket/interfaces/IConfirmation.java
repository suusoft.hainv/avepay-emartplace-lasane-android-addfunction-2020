package com.aveplus.avepay_cpocket.interfaces;

/**
 * Created by Suusoft on 10/06/2015.
 */
public interface IConfirmation {

    void onPositive();

    void onNegative();
}
