package com.aveplus.avepay_cpocket.trainingcenter.config;


import com.aveplus.avepay_cpocket.trainingcenter.object.Cart;

import java.util.ArrayList;

public class GlobalValue {

    public static String preScreen;

    public static ArrayList<Cart> arrcart;

    //public static ArrayList<Product> arrPro = new ArrayList<Product>();

    public static final String FRUITY_DROID_PREFERENCES = "FRUITY_DROID_PREFERENCES";
    public static FruitySharedPreferences pref;
    public static final String KEY_ORDER_IDS = "orderIds";

    // JSON keys
    public static final String JSON_STATUS = "status";
    public static final String JSON_MESSAGE = "message";
    public static final String JSON_DATA = "data";
    public static final String JSON_SUCCESS = "success";

}
