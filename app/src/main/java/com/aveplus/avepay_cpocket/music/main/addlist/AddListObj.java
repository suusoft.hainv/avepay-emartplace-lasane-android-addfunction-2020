package com.aveplus.avepay_cpocket.music.main.addlist;

import android.os.Parcel;
import android.os.Parcelable;


import com.aveplus.avepay_cpocket.music.base.model.BaseModel;
import com.aveplus.avepay_cpocket.music.model.Song;

import java.util.ArrayList;

public class AddListObj extends BaseModel implements Parcelable {
    private String addListName;
    private ArrayList<Song> listVideos;

    public AddListObj() {
    }

    public AddListObj(String addListName, ArrayList<Song> listVideos) {
        this.addListName = addListName;
        this.listVideos = listVideos;
    }

    protected AddListObj(Parcel in) {
        addListName = in.readString();
        listVideos = in.createTypedArrayList(Song.CREATOR);
    }

    public static final Creator<AddListObj> CREATOR = new Creator<AddListObj>() {
        @Override
        public AddListObj createFromParcel(Parcel in) {
            return new AddListObj(in);
        }

        @Override
        public AddListObj[] newArray(int size) {
            return new AddListObj[size];
        }
    };

    public String getAddListName() {
        return addListName;
    }

    public void setAddListName(String addListName) {
        this.addListName = addListName;
    }

    public ArrayList<Song> getListVideos() {
        return listVideos;
    }

    public void setListVideos(ArrayList<Song> listVideos) {
        this.listVideos = listVideos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(addListName);
        dest.writeTypedList(listVideos);
    }
}
