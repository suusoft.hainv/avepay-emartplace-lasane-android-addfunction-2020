package com.aveplus.avepay_cpocket.taxi.objects;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.taxi.objects.book.DriverObj;
import com.aveplus.avepay_cpocket.taxi.objects.book.ServiceObj;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;


public class TripObj implements Parcelable {

    public static final String TYPE_INSTANT_BOOKING  = "0";
    public static final String TYPE_NEGOCIATE_BOOKING  = "1";

    public static final String NEW = "new";
    public static final String CANCELED = "cancelled";
    public static final String FINISHED = "finished";
    public static final String ON_GOING = "processing";
    public static final String STATUS_DEAL = "deal";
    public static final String DRIVER_ACCEPT_DEAL = "accept_deal";
    public static final String STATUS_PAYMENT_PENDING = "payment-pending";
    public static final String STATUS_DRIVE_PUT_TIME = "driver-put-time";

    public static final String ACTION_CREATE = "create";
    public static final String ACTION_DENY = "deny";
    public static final String ACTION_CANCEL = "cancel";
    public static final String ACTION_DELETE = "delete";
    public static final String ACTION_FINISH = "finish";
    public static final String ACTION_DETAIL = "detail";

    public static final String PROCESSING = "processing";
    public static final String HISTORY = "history";

    private String id;
    private String price;
    private String name;
    private String driver;
    private String service;
    private String company;
    private String status;
    private String timestamp;
    private String addressFrom, addressTo;
    private String timeBookLater;

    private String start_lat;

    private String start_long;

    private String end_lat;

    private String end_long;

    @SerializedName("passenger_finished")
    private String passengerFinished;

    @SerializedName("passenger_id")
    private String passengerId;

    @SerializedName("passenger_confirm")
    private String passengerConfirm;

    @SerializedName("passenger_avatar")
    private String passengerAvatar;

    @SerializedName("passenger_name")
    private String passengerName;

    @SerializedName("passenger_rate")
    private String passengerRate;

    @SerializedName("passenger_rate_count")
    private String passengerRateCount;

    @SerializedName("passenger_phone")
    private String passengerPhone;

    @SerializedName("driver_id")
    private String driverId;

    private String driverType;

    @SerializedName("driver_name")
    private String driverName;

    private String driverEmail;

    @SerializedName("driver_phone")
    private String driverPhone;

    @SerializedName("driver_avatar")
    private String driverAvatar;

    @SerializedName("driver_finished")
    private String driverFinished;
    private String created_date;
    private String modified_date;

    @SerializedName("estimate_distance")
    private String estimateDistance;

    @SerializedName("estimate_fare")
    private String estimateFare ;

    @SerializedName("actual_fare")
    private float actualFare;

    @SerializedName("estimate_fare_driver")
    private String estimateFareDriver ;

    @SerializedName("estimate_time")
    public int estimateTime;

    @SerializedName("driver_rate")
    private String  rateOfDriver;

    @SerializedName("distance")
    private String  distance;

    private String  time;

    private int  is_active;

    @SerializedName("estimate_duration")
    private String  estimate_duration;

    @SerializedName("time_to_passenger")
    private String  time_to_passenger;

    @SerializedName("start_location")
    private String pickup;

    @SerializedName("end_location")
    private String destination;

    private String eta;

    private String paymentMethod;

    @SerializedName("payment_status")
    private String paymentStatus;

    @SerializedName("driver_type")
    private String transportType;

    @SerializedName("driver_qb_id")
    private String driverQBId;

    @SerializedName("passenger_qb_id")
    private String passengerQBId;
    private LatLng latLngPickup ,latLngDestination, latLngDriver;
    private String  routeDistance;
    private int duration, routeDuration, rateQuantity, passengerQuantity;
    private boolean isDriverIsDelivery;

    private ServiceObj serviceObj;
    private DriverObj driverObj;
    private String note;
    private String type;


    @SerializedName("driver_plate")
    private String driverPlate;

    @SerializedName("driver_brand")
    private String driverBrand;

    @SerializedName("driver_model")
    private String driverModel;

    @SerializedName("driver_color")
    private String driverColor;

    @SerializedName("driver_deal_count")
    private int driverDealCount ;

    @SerializedName("passenger_deal_count")
    private int passengerDealCount ;


    public TripObj() {
    }


    protected TripObj(Parcel in) {
        id = in.readString();
        price = in.readString();
        name = in.readString();
        driver = in.readString();
        service = in.readString();
        company = in.readString();
        status = in.readString();
        timestamp = in.readString();
        addressFrom = in.readString();
        addressTo = in.readString();
        timeBookLater = in.readString();
        start_lat = in.readString();
        start_long = in.readString();
        end_lat = in.readString();
        end_long = in.readString();
        passengerFinished = in.readString();
        passengerId = in.readString();
        passengerConfirm = in.readString();
        passengerAvatar = in.readString();
        passengerName = in.readString();
        passengerRate = in.readString();
        passengerRateCount = in.readString();
        passengerPhone = in.readString();
        driverId = in.readString();
        driverType = in.readString();
        driverName = in.readString();
        driverEmail = in.readString();
        driverPhone = in.readString();
        driverAvatar = in.readString();
        driverFinished = in.readString();
        created_date = in.readString();
        modified_date = in.readString();
        estimateDistance = in.readString();
        estimateFare = in.readString();
        estimateFareDriver = in.readString();
        estimateTime = in.readInt();
        rateOfDriver = in.readString();
        distance = in.readString();
        time = in.readString();
        is_active = in.readInt();
        estimate_duration = in.readString();
        time_to_passenger = in.readString();
        pickup = in.readString();
        destination = in.readString();
        eta = in.readString();
        paymentMethod = in.readString();
        paymentStatus = in.readString();
        transportType = in.readString();
        driverQBId = in.readString();
        passengerQBId = in.readString();
        latLngPickup = in.readParcelable(LatLng.class.getClassLoader());
        latLngDestination = in.readParcelable(LatLng.class.getClassLoader());
        latLngDriver = in.readParcelable(LatLng.class.getClassLoader());
        actualFare = in.readFloat();
        routeDistance = in.readString();
        duration = in.readInt();
        routeDuration = in.readInt();
        rateQuantity = in.readInt();
        passengerQuantity = in.readInt();
        isDriverIsDelivery = in.readByte() != 0;
        serviceObj = in.readParcelable(ServiceObj.class.getClassLoader());
        driverObj = in.readParcelable(DriverObj.class.getClassLoader());
        note = in.readString();
        type = in.readString();
        driverPlate = in.readString();
        driverBrand = in.readString();
        driverModel = in.readString();
        driverColor = in.readString();
        driverDealCount = in.readInt();
        passengerDealCount = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(price);
        dest.writeString(name);
        dest.writeString(driver);
        dest.writeString(service);
        dest.writeString(company);
        dest.writeString(status);
        dest.writeString(timestamp);
        dest.writeString(addressFrom);
        dest.writeString(addressTo);
        dest.writeString(timeBookLater);
        dest.writeString(start_lat);
        dest.writeString(start_long);
        dest.writeString(end_lat);
        dest.writeString(end_long);
        dest.writeString(passengerFinished);
        dest.writeString(passengerId);
        dest.writeString(passengerConfirm);
        dest.writeString(passengerAvatar);
        dest.writeString(passengerName);
        dest.writeString(passengerRate);
        dest.writeString(passengerRateCount);
        dest.writeString(passengerPhone);
        dest.writeString(driverId);
        dest.writeString(driverType);
        dest.writeString(driverName);
        dest.writeString(driverEmail);
        dest.writeString(driverPhone);
        dest.writeString(driverAvatar);
        dest.writeString(driverFinished);
        dest.writeString(created_date);
        dest.writeString(modified_date);
        dest.writeString(estimateDistance);
        dest.writeString(estimateFare);
        dest.writeString(estimateFareDriver);
        dest.writeInt(estimateTime);
        dest.writeString(rateOfDriver);
        dest.writeString(distance);
        dest.writeString(time);
        dest.writeInt(is_active);
        dest.writeString(estimate_duration);
        dest.writeString(time_to_passenger);
        dest.writeString(pickup);
        dest.writeString(destination);
        dest.writeString(eta);
        dest.writeString(paymentMethod);
        dest.writeString(paymentStatus);
        dest.writeString(transportType);
        dest.writeString(driverQBId);
        dest.writeString(passengerQBId);
        dest.writeParcelable(latLngPickup, flags);
        dest.writeParcelable(latLngDestination, flags);
        dest.writeParcelable(latLngDriver, flags);
        dest.writeFloat(actualFare);
        dest.writeString(routeDistance);
        dest.writeInt(duration);
        dest.writeInt(routeDuration);
        dest.writeInt(rateQuantity);
        dest.writeInt(passengerQuantity);
        dest.writeByte((byte) (isDriverIsDelivery ? 1 : 0));
        dest.writeParcelable(serviceObj, flags);
        dest.writeParcelable(driverObj, flags);
        dest.writeString(note);
        dest.writeString(type);
        dest.writeString(driverPlate);
        dest.writeString(driverBrand);
        dest.writeString(driverModel);
        dest.writeString(driverColor);
        dest.writeInt(driverDealCount);
        dest.writeInt(passengerDealCount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TripObj> CREATOR = new Creator<TripObj>() {
        @Override
        public TripObj createFromParcel(Parcel in) {
            return new TripObj(in);
        }

        @Override
        public TripObj[] newArray(int size) {
            return new TripObj[size];
        }
    };

    public LatLng getLatLngDriver() {
        return latLngDriver;
    }

    public void setLatLngDriver(LatLng latLngDriver) {
        this.latLngDriver = latLngDriver;
    }

    public String getDriverFinished() {
        return driverFinished;
    }

    public void setDriverFinished(String driverFinished) {
        this.driverFinished = driverFinished;
    }

    public String getPassengerFinished() {
        return passengerFinished;
    }

    public void setPassengerFinished(String passengerFinished) {
        this.passengerFinished = passengerFinished;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public float getRateOfDriver() {
        if (rateOfDriver.equals("") ||rateOfDriver==null )
            return 0;
        return Float.parseFloat(rateOfDriver);
    }

    public void setRateOfDriver(float rateOfDriver) {
        this.rateOfDriver = String.valueOf(rateOfDriver);
    }

    public boolean isFinished() {
        return getStatus().equalsIgnoreCase(FINISHED);
    }

    public boolean isCanceled() {
        return getStatus().equalsIgnoreCase(CANCELED);
    }

    public boolean isOnGoing() {
        return getStatus().equalsIgnoreCase(ON_GOING);
    }

    public boolean isTripDeal() {
        return getStatus().equals(STATUS_DEAL);
    }

    public boolean isPaymentPending() {
        return getStatus().equals(STATUS_PAYMENT_PENDING);
    }

    public boolean isDriverPutTime() {
        return getStatus().equals(STATUS_DRIVE_PUT_TIME);
    }

    public boolean isNew() {
        return getStatus().equals(NEW);
    }

    public boolean isDriverAccept() {
        return getStatus().equals(DRIVER_ACCEPT_DEAL);
    }

    public boolean isPaid() {
        return getPaymentStatus().equalsIgnoreCase("1");
    }

    public String getPassengerConfirm() {
        return passengerConfirm;
    }

    public void setPassengerConfirm(String passengerConfirm) {
        this.passengerConfirm = passengerConfirm;
    }

    public String getDriverType() {
        return driverType;
    }

    public void setDriverType(String driverType) {
        this.driverType = driverType;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverEmail() {
        return driverEmail;
    }

    public void setDriverEmail(String driverEmail) {
        this.driverEmail = driverEmail;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getDriverAvatar() {
        return driverAvatar;
    }

    public void setDriverAvatar(String driverAvatar) {
        this.driverAvatar = driverAvatar;
    }

    public double getDistance() {
        return Double.parseDouble(distance);
    }

    public void setDistance(double distance) {
        this.distance = String.valueOf(distance);
    }

    public long getTime() {
        return Long.parseLong(time);
    }

    public void setTime(long time) {
        this.time = String.valueOf(time);
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public double getEstimate_duration() {
        return Double.parseDouble(estimate_duration);
    }

    public void setEstimate_duration(double estimate_duration) {
        this.estimate_duration = String.valueOf(estimate_duration);
    }

    public int getTime_to_passenger() {
        if (time_to_passenger==null || time_to_passenger .equals(""))
            return 0;
        return Integer.parseInt(time_to_passenger);
    }

    public void setTime_to_passenger(int time_to_passenger) {
        this.time_to_passenger = String.valueOf(time_to_passenger);
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public String getPassengerAvatar() {
        return passengerAvatar;
    }

    public void setPassengerAvatar(String passengerAvatar) {
        this.passengerAvatar = passengerAvatar;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerRate() {
        return passengerRate;
    }

    public void setPassengerRate(String passengerRate) {
        this.passengerRate = passengerRate;
    }

    public String getPassengerRateCount() {
        return passengerRateCount;
    }

    public void setPassengerRateCount(String passengerRateCount) {
        this.passengerRateCount = passengerRateCount;
    }

    public String getPassengerPhone() {
        return passengerPhone;
    }

    public void setPassengerPhone(String passengerPhone) {
        this.passengerPhone = passengerPhone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public double getPrice() {
        if (price== null || price.equals(""))
            return 0;
        return Double.parseDouble(price);
    }

    public void setPrice(double price) {
        this.price = String.valueOf(price);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public LatLng getLatLngPickup() {
        if (latLngPickup==null)
            latLngPickup = new LatLng(Double.parseDouble(start_lat), Double.parseDouble(start_long));
        return latLngPickup;
    }

    public void setLatLngPickup(LatLng latLngFrom) {
        this.latLngPickup = latLngFrom;
    }

    public LatLng getLatLngDestination() {
        if (latLngDestination==null)
            latLngDestination = new LatLng(Double.parseDouble(end_lat), Double.parseDouble(end_long));
        return latLngDestination;
    }

    public void setLatLngDestination(LatLng latLngDestination) {
        this.latLngDestination = latLngDestination;
    }

    public DriverObj getDriverObj() {
        return driverObj;
    }

    public void setDriverObj(DriverObj driverObj) {
        this.driverObj = driverObj;
    }

    public String getAddressFrom() {
        return addressFrom;
    }

    public void setAddressFrom(String addressFrom) {
        this.addressFrom = addressFrom;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ServiceObj getServiceObj() {
        return serviceObj;
    }

    public void setServiceObj(ServiceObj serviceObj) {
        this.serviceObj = serviceObj;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public double getEstimateDistance() {
        if (estimateDistance== null || estimateDistance.equals(""))
            return 0;
        return Double.parseDouble(estimateDistance);
    }

    public void setEstimateDistance(double estimateDistance) {
        this.estimateDistance = String.valueOf(estimateDistance);
    }

    public int getEstimateFare() {
        if (estimateFare== null || estimateFare.equals(""))
        return 0;
        return (int) Double.parseDouble(estimateFare);
    }

    public void setEstimateFare(double estimateFare) {
        this.estimateFare = String.valueOf(estimateFare);
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public int getDriverQBId() {
        return Integer.parseInt(driverQBId);
    }

    public void setDriverQBId(int driverQBId) {
        this.driverQBId = String.valueOf(driverQBId);
    }

    public int getPassengerQBId() {
        return Integer.parseInt(passengerQBId);
    }

    public void setPassengerQBId(int passengerQBId) {
        this.passengerQBId = String.valueOf(passengerQBId);
    }

    public float getActualFare() {
        return actualFare;
    }

    public void setActualFare(float actualFare) {
        this.actualFare = actualFare;
    }

    public double getRouteDistance() {
        if (routeDistance== null || routeDistance.equals(""))
            return 0;
        return Double.parseDouble(routeDistance);
    }

    public void setRouteDistance(double routeDistance) {
        this.routeDistance = String.valueOf(routeDistance);
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getRouteDuration() {
        return routeDuration;
    }

    public void setRouteDuration(int routeDuration) {
        this.routeDuration = routeDuration;
    }

    public int getRateQuantity() {
        return rateQuantity;
    }

    public void setRateQuantity(int rateQuantity) {
        this.rateQuantity = rateQuantity;
    }

    public int getPassengerQuantity() {
        return passengerQuantity;
    }

    public void setPassengerQuantity(int passengerQuantity) {
        this.passengerQuantity = passengerQuantity;
    }

    public boolean driverIsDelivery() {
        return isDriverIsDelivery;
    }

    public void setDriverIsDelivery(boolean isDriverIsDelivery) {
        this.isDriverIsDelivery = isDriverIsDelivery;
    }


    public String getTimeBookLater() {
        return timeBookLater;
    }

    public void setTimeBookLater(String timeBookLater) {
        this.timeBookLater = timeBookLater;
    }

    public int getEstimateTime() {
        return estimateTime;
    }

    public void setEstimateTime(int estimateTime) {
        this.estimateTime = estimateTime;
    }

    public String getDriverPlate() {
        return driverPlate;
    }

    public void setDriverPlate(String driverPlate) {
        this.driverPlate = driverPlate;
    }

    public String getDriverBrand() {
        return driverBrand;
    }

    public void setDriverBrand(String driverBrand) {
        this.driverBrand = driverBrand;
    }

    public String getDriverModel() {
        return driverModel;
    }

    public void setDriverModel(String driverModel) {
        this.driverModel = driverModel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isInstantBooking(){
        return type.equals(TYPE_INSTANT_BOOKING);
    }

    public boolean isNegociateBooking(){
        return type.equals(TYPE_NEGOCIATE_BOOKING);
    }

    public int getDriverDealCount() {
        return driverDealCount;
    }

    public void setDriverDealCount(int driverDealCount) {
        this.driverDealCount = driverDealCount;
    }

    public int getPassengerDealCount() {
        return passengerDealCount;
    }

    public void setPassengerDealCount(int passengerDealCount) {
        this.passengerDealCount = passengerDealCount;
    }

    public boolean isDriverDealContinues(){
        return getDriverDealCount()< 3;
    }

    public String getEstimateFareDriver() {
        if (estimateFareDriver==null)
            return "";
        return estimateFareDriver;
    }

    public void setEstimateFareDriver(String estimateFareDriver) {
        this.estimateFareDriver = estimateFareDriver;
    }
}
