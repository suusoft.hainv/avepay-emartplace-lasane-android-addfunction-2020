package com.aveplus.avepay_cpocket.music.main.album;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.model.Album;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class AllAlbumByVM extends BaseViewModelList {

    int isTop, isHot, isNew, position;

    public AllAlbumByVM(Context context, Bundle bundle) {
        super(context, bundle);
        getDataBundle();
        getData(1);

    }

    private void getDataBundle() {
        position = bundle.getInt(AllAlbumByFragment.KEY_POSITION);
        isTop = position == 4 ? 1 : 0;
        isHot = position == 3 ? 1 : 0;
        isNew = position == 2 ? 1 : 0;
    }

    @Override
    public void getData(int page) {
        RequestManager.getAllAlbumBy(position, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                ArrayList<Album> list = (ArrayList<Album>) response.getDataList(Album.class);
                addListData(list);
                // checkLoadingMoreComplete(Integer.parseInt(response.getValueFromRoot(Constant.KEY_TOTAL_PAGE)));
            }

            @Override
            public void onError(String message) {
                Log.d("AllAlbumByVM", "can not get video detail");
            }
        });
    }
}
