package com.aveplus.avepay_cpocket.taxi.utils;

import android.location.Location;
import android.util.Log;

import com.aveplus.avepay_cpocket.AppController;
import com.google.android.gms.maps.model.LatLng;

public class LocationUtils {

    private static final String TAG = LocationUtils.class.getSimpleName();

    public static boolean isPointChanged(Location lOld, Location lNew) {

        if (lOld == null)
            return true;

        if (lOld.getLatitude() == 0 || lOld.getLongitude() == 0) {
            return true;
        }

        float distance = lOld.distanceTo(lNew);

        if (distance > 5)
            return true;

        return false;

    }


    public static boolean isPointChanged(Location lOld, Location lNew, String tag) {

        if (lOld == null)
            return true;


        if (lOld.getLatitude() == 0 || lOld.getLongitude() == 0) {
            Log.e(tag, "latlng == 0 ");
            return true;
        }

        float distance = lOld.distanceTo(lNew);

        // Log.e(tag, "isPointChanged: " + distance );

        if (distance > 5) {
            AppController.setMyLatLng(new LatLng(lNew.getLatitude(), lNew.getLongitude()));
            return true;
        }


        return false;

    }


    public static boolean isPointChanged(LatLng lOld, LatLng lNew, String tag) {

        Location lFrom = new Location("locaiton from");
        lFrom.setLatitude(lOld.latitude);
        lFrom.setLongitude(lOld.longitude);

        Location lTo = new Location("locaiton TO");
        lTo.setLatitude(lNew.latitude);
        lTo.setLongitude(lNew.longitude);

        if (lOld == null)
            return true;


        float distance = lFrom.distanceTo(lTo);

        // Log.e(tag, "isPointChanged: " + distance );

        if (distance > 5)
            return true;

        return false;

    }


}
