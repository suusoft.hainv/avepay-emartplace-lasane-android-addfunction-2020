package com.aveplus.avepay_cpocket.taxi.configs;

/**
 * Created by SuuSoft on 3/26/2018.
 */

public class Action {
    public final static String ACTION_PUSH_DEAL = "ACTION_PUSH_DEAL";
    public final static String ACTION_PUSH_CANCEL_DEAL = "ACTION_PUSH_CANCEL_DEAL";

    //action trip
    public static final String ACTION_DELETE = "delete";
    public static final String ACTION_DENY = "deny";
    public static final String ACTION_REJECT = "reject";
    public static final String ACTION_CANCEL = "cancel";
    public static final String ACTION_DEAL = "deal";
    public static final String ACTION_PAY = "pay";
    public static final String ACTION_CREATE = "create";
    public static final String ACTION_UPDATE = "update";
    public static final String ACTION_ACCEPT = "accept";
    public static final String ACTION_FINISH = "finish";
    public static final String ACTION_ACCEPT_DEAL = "accept_deal";
    public static final String ACTION_PAYMENT = "payment";
    public static final String ACTION_DRIVE_PUT_TIME = "driver-put-time";

    //action broadcast
    public static final String ACTION_FINISHED = "finished";
    // public static final String ACTION_CANCEL = "cancelled";


    public static final String ACTION_DRIVER_DEAL = "driver-deal";
    public static final String ACTION_PASSENGER_DEAL = "passenger-deal";

}
