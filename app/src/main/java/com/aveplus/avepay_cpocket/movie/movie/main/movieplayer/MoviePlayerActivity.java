package com.aveplus.avepay_cpocket.movie.movie.main.movieplayer;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import androidx.mediarouter.app.MediaRouteButton;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.LoadMovieCallback;
import com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.AndPlayer.AndPlayerFragment;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadOptions;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

import static com.aveplus.avepay_cpocket.movie.movie.main.movieplayer.AndPlayer.AndPlayerFragment.RQ_YOUTUBE_PLAYER;


public class MoviePlayerActivity extends Activity implements LoadMovieCallback {
    public static final String EXTRA_MOVIE = "movie";
    public static final String CATEGORY_EXTRA = "position category extra";
    public static final String SUBCATEGORY_EXTRA = "position subcategory extra";
    public static final String MOVIE_EXTRA = "position movie extra";
    private static final String TAG = MoviePlayerActivity.class.getSimpleName();

    private int mMoviePosition;
    private int mCategoryPosition;
    private int mSubcategoryPosition;

    //Current Movie and Current Movie Info
    private Movie mCurrentMovie;

    private AndPlayerFragment mPlayerFragment;
    private MovieDetailFragment mDetailFragment;
    private MoviesListFragment mMoviesListFragment;
    //private SubtitleControllerFragment mSubtitleControlFragment;
    private Animation animation;
    private LinearLayout layoutOption;
    private boolean shownOptionView;
    private CastSession mCastSession;
    private MediaInfo mSelectedMedia;
    private SuuPlayer player;
    private View loController;
    private SessionManagerListener<CastSession> mSessionManagerListener;
    private MediaRouteButton mMediaRouteButton;
    private CastContext mCastContext;

    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

    private PlaybackLocation mLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_movie_player);
        getExtraDataFromIntent();
        initView();
        PlayMovie(mCurrentMovie);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        //initCastTV();

    }

    private void initCastTV() {
        setupCastListener();
        mMediaRouteButton = findViewById(R.id.btn_cast);



        mCastContext = CastContext.getSharedInstance(this);
        CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), mMediaRouteButton);
        mCastSession = mCastContext.getSessionManager().getCurrentCastSession();

    }

    private void setupCastListener() {
        mSessionManagerListener = new SessionManagerListener<CastSession>() {

            @Override
            public void onSessionEnded(CastSession session, int error) {
                Log.e("cast", "onSessionEnded " + session + " " + error);
                onApplicationDisconnected();
            }

            @Override
            public void onSessionResumed(CastSession session, boolean wasSuspended) {
                Log.e("cast", "onSessionResumed " + wasSuspended);
                onApplicationConnected(session);
            }

            @Override
            public void onSessionResumeFailed(CastSession session, int error) {
                Log.e("cast", "onSessionResumeFailed " + session + " " + error);
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarted(CastSession session, String sessionId) {
                Log.e("cast", "onSessionStarted " + session + " " + sessionId);
                onApplicationConnected(session);
            }

            @Override
            public void onSessionStartFailed(CastSession session, int error) {
                Log.e("cast", "onSessionStartFailed " + session + " " + error);
                onApplicationDisconnected();
            }

            @Override
            public void onSessionStarting(CastSession session) {
                Log.e("cast", "onSessionStarting " + session);
            }

            @Override
            public void onSessionEnding(CastSession session) {
                Log.e("cast", "onSessionEnding " + session);
            }

            @Override
            public void onSessionResuming(CastSession session, String sessionId) {
                Log.e("cast", "onSessionResuming " + session + " " + sessionId);
            }

            @Override
            public void onSessionSuspended(CastSession session, int reason) {
                Log.e("cast", "onSessionSuspended " + session + " " + reason);
            }

            private void onApplicationConnected(CastSession castSession) {
                Log.e("cast", "onApplicationConnected");
                mCastSession = castSession;

//                if (null != mSelectedMedia) {
//
//                    if (mPlaybackState == PlaybackState.PLAYING) {
//                        mVideoView.pause();
//                        loadRemoteMedia(mSeekbar.getProgress(), true);
//                        return;
//                    } else {
//                        mPlaybackState = PlaybackState.IDLE;
//                        updatePlaybackLocation(PlaybackLocation.REMOTE);
//                    }
//                }
//                updatePlayButton(mPlaybackState);
//                invalidateOptionsMenu();
                if (null != mSelectedMedia) {

                    if (player.isPlaying()) {
                        player.pause();
                        loadRemoteMedia((int) player.getCurrentPosition(), true);
                        return;
                    } else {
                        //mPlaybackState = PlaybackState.IDLE;
                        updatePlaybackLocation(PlaybackLocation.REMOTE);
                    }
                }
                updatePlayButton();
                //invalidateOptionsMenu();
            }

            private void onApplicationDisconnected() {
                updatePlaybackLocation(PlaybackLocation.LOCAL);
                //mPlaybackState = PlaybackState.IDLE;
                mLocation = PlaybackLocation.LOCAL;
                updatePlayButton();
                //invalidateOptionsMenu();
            }
        };
    }

    private void loadRemoteMedia(int position, boolean autoPlay) {
        if (mCastSession == null) {
            return;
        }
        final RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
        if (remoteMediaClient == null) {
            return;
        }
//        remoteMediaClient.registerCallback(new RemoteMediaClient.Callback() {
//            @Override
//            public void onStatusUpdated() {
//                Intent intent = new Intent(self, ExpandedControlsActivity.class);
//                startActivity(intent);
//                remoteMediaClient.unregisterCallback(this);
//            }
//        });

        remoteMediaClient.addListener(new RemoteMediaClient.Listener() {
            @Override
            public void onStatusUpdated() {

            }

            @Override
            public void onMetadataUpdated() {

            }

            @Override
            public void onQueueStatusUpdated() {

            }

            @Override
            public void onPreloadStatusUpdated() {

            }

            @Override
            public void onSendingRemoteMediaRequest() {

            }

            @Override
            public void onAdBreakStatusUpdated() {

            }
        });

        remoteMediaClient.load(mSelectedMedia,
                new MediaLoadOptions.Builder()
                        .setAutoplay(autoPlay)
                        .setPlayPosition(position).build());
    }

    private void updatePlaybackLocation(PlaybackLocation location) {
        mLocation = location;
//        if (location == PlaybackLocation.LOCAL) {
//            if (mPlaybackState == PlaybackState.PLAYING
//                    || mPlaybackState == PlaybackState.BUFFERING) {
//                setCoverArtStatus(null);
//                startControllersTimer();
//            } else {
//                stopControllersTimer();
//                setCoverArtStatus(MediaUtils.getImageUrl(mSelectedMedia, 0));
//            }
//        } else {
//            stopControllersTimer();
//            setCoverArtStatus(MediaUtils.getImageUrl(mSelectedMedia, 0));
//            updateControllersVisibility(false);
//        }
        if (location == PlaybackLocation.LOCAL) {
//            if (player.isPlaying()) {
//                setCoverArtStatus(null);
//                startControllersTimer();
//            } else {
//                stopControllersTimer();
//                setCoverArtStatus(MediaUtils.getImageUrl(mSelectedMedia, 0));
//            }
        } else {
//            stopControllersTimer();
//            setCoverArtStatus(MediaUtils.getImageUrl(mSelectedMedia, 0));
            updateControllersVisibility(false);
        }
    }

    private void updateControllersVisibility(boolean show) {
        if (show) {
            //getSupportActionBar().show();
            loController.setVisibility(View.VISIBLE);
        } else {
//            if (!Utils.isOrientationPortrait(this)) {
//                getSupportActionBar().hide();
//            }
            loController.setVisibility(View.INVISIBLE);
        }
        //doLayout();
    }

    private void updatePlayButton() {
        Log.e(TAG, "Controls: PlayBackState: " + player.isPlaying());
        boolean isConnected = (mCastSession != null)
                && (mCastSession.isConnected() || mCastSession.isConnecting());
        loController.setVisibility(isConnected ? View.GONE : View.VISIBLE);
//        mPlayCircle.setVisibility(isConnected ? View.GONE : View.VISIBLE);
//        switch (state) {
//            case PLAYING:
//                mLoading.setVisibility(View.INVISIBLE);
//                mPlayPause.setVisibility(View.VISIBLE);
//                mPlayPause.setImageDrawable(
//                        getResources().getDrawable(R.drawable.ic_av_pause_dark));
//                mPlayCircle.setVisibility(isConnected ? View.VISIBLE : View.GONE);
//                break;
//            case IDLE:
//                mPlayCircle.setVisibility(View.VISIBLE);
//                mControllers.setVisibility(View.GONE);
//                mCoverArt.setVisibility(View.VISIBLE);
//                mVideoView.setVisibility(View.INVISIBLE);
//                break;
//            case PAUSED:
//                mLoading.setVisibility(View.INVISIBLE);
//                mPlayPause.setVisibility(View.VISIBLE);
//                mPlayPause.setImageDrawable(
//                        getResources().getDrawable(R.drawable.ic_av_play_dark));
//                mPlayCircle.setVisibility(isConnected ? View.VISIBLE : View.GONE);
//                break;
//            case BUFFERING:
//                mPlayPause.setVisibility(View.INVISIBLE);
//                mLoading.setVisibility(View.VISIBLE);
//                break;
//            default:
//                break;
//        }
    }

    private void initView() {

//
        mPlayerFragment = (AndPlayerFragment) getFragmentManager().findFragmentById(R.id.fragment_player);
        mPlayerFragment.setActivity(MoviePlayerActivity.this);
//        mPlayerFragment = AndPlayerFragment.newInstance(mCurrentMovie,MoviePlayerActivity.this);
//        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.fragment_player,mPlayerFragment);
//        fragmentTransaction.commit();


        layoutOption = (LinearLayout) findViewById(R.id.showOption);

        mPlayerFragment.setOnOptionsKeyPressListener(new AndPlayerFragment.OnOptionsKeyPressListener() {
            @Override
            public void onPressCaptionButton() {
                //controlVisibilityOfSubtitleConfigView();
                controlVisibilityOfMoviesListView();
            }

            @Override
            public void onPressInfoButton() {
                controlVisibilityOfMoviesDetailView();
            }
        });
    }

    private void getExtraDataFromIntent() {
        if (getIntent() != null) {
            mCurrentMovie = getIntent().getParcelableExtra(EXTRA_MOVIE);
            mCategoryPosition = getIntent().getIntExtra(CATEGORY_EXTRA, -1);
            mSubcategoryPosition = getIntent().getIntExtra(SUBCATEGORY_EXTRA, -1);
            mMoviePosition = getIntent().getIntExtra(MOVIE_EXTRA, -1);

        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            int keyCode = event.getKeyCode();
            switch (keyCode) {
                // display detail movie
                case KeyEvent.KEYCODE_1:
                case KeyEvent.KEYCODE_INFO:
                case KeyEvent.KEYCODE_NOTIFICATION:
                    controlVisibilityOfMoviesDetailView();
                    return true;

                // dis play movie list
                case KeyEvent.KEYCODE_0:
                case KeyEvent.KEYCODE_MENU:
                    controlVisibilityOfMoviesListView();
                    return true;

                // display subtitle config
                case KeyEvent.KEYCODE_2:
                    // controlVisibilityOfSubtitleConfigView();
                    return true;

                // Back pressed
                case KeyEvent.KEYCODE_BACK:
                    if (shownOptionView) {
                        layoutOption.setVisibility(View.GONE);
                        updateVisibilityOfOptionsView();
                        return true;
                    }
                    break;

            }
        }

        return super.dispatchKeyEvent(event);
    }

    @Override
    public void PlayMovie(Movie movie) {
        mCurrentMovie = movie;

        // reset player
        if (mPlayerFragment != null) {
            mPlayerFragment.setMovie(mCurrentMovie);
        }

        // reset detail
        if (mDetailFragment != null) {
            mDetailFragment.setInfor(mCurrentMovie);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (shownOptionView) {
            layoutOption.setVisibility(View.GONE);
            updateVisibilityOfOptionsView();
        }


    }

    public void controlVisibilityOfMoviesDetailView() {
        if (mDetailFragment == null) {
            mDetailFragment = MovieDetailFragment.newInstance(mCurrentMovie);
            //mDetailFragment = MovieDetailFragment.newInstance(mCurrentMovie);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.showOption, mDetailFragment).commit();
            layoutOption.setVisibility(View.VISIBLE);
        } else if (mDetailFragment.isVisible()) {
            int visibility = layoutOption.getVisibility() == View.GONE ? View.VISIBLE : View.GONE;
            layoutOption.setVisibility(visibility);
        } else {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.showOption, mDetailFragment).commit();
            layoutOption.setVisibility(View.VISIBLE);
        }

        // Update state of option views
        updateVisibilityOfOptionsView();
    }

    private void controlVisibilityOfMoviesListView() {
        if (mMoviesListFragment == null) {
            mMoviesListFragment = MoviesListFragment.newInstance(mCategoryPosition, mSubcategoryPosition, mMoviePosition, mCurrentMovie);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.showOption, mMoviesListFragment).commit();
            layoutOption.setVisibility(View.VISIBLE);
        }
        if (mMoviesListFragment.isVisible()) {
            int visibility = layoutOption.getVisibility() == View.GONE ? View.VISIBLE : View.GONE;
            layoutOption.setVisibility(visibility);
        } else {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.showOption, mMoviesListFragment).commit();
            layoutOption.setVisibility(View.VISIBLE);
        }

        // Update state of option views
        updateVisibilityOfOptionsView();
    }

//    public void controlVisibilityOfSubtitleConfigView() {
//        if (mSubtitleControlFragment == null)
//        {
//            mSubtitleControlFragment = SubtitleControllerFragment.newInstance(mCurrentMovie.getMovie_name());
//            mSubtitleControlFragment.setSubtitleController(mSubtitleController);
//            FragmentTransaction transaction = getFragmentManager().beginTransaction();
//            transaction.replace(R.id.showOption, mSubtitleControlFragment).commit();
//            layoutOption.setVisibility(View.VISIBLE);
//        }
//        else if (mSubtitleControlFragment.isVisible())
//        {
//            int visibility = layoutOption.getVisibility() == View.GONE ? View.VISIBLE : View.GONE;
//            if (visibility == View.VISIBLE) {
//                mSubtitleControlFragment.didShow();
//            }
//            layoutOption.setVisibility(visibility);
//        } else
//        {
//            FragmentTransaction transaction = getFragmentManager().beginTransaction();
//            transaction.replace(R.id.showOption, mSubtitleControlFragment).commit();
//            layoutOption.setVisibility(View.VISIBLE);
//        }
//
//        // Update state of option views
//        updateVisibilityOfOptionsView();
//    }

    private void updateVisibilityOfOptionsView() {
        shownOptionView = layoutOption.getVisibility() == View.GONE ? false : true;
        mPlayerFragment.setEnableMediaController(!shownOptionView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==RQ_YOUTUBE_PLAYER){
            finish();
        }
    }
}
