package com.aveplus.avepay_cpocket.music.main.player;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.util.AdsUtil;
import com.google.android.gms.ads.AdView;


import de.hdodenhof.circleimageview.CircleImageView;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class NowPlayingFragment extends BaseFragmentBinding {

    NowPlayingVM viewModel;
    private CircleImageView imageView;
    private AdView adsView;
    private TextView tvNameSong;

    public static NowPlayingFragment newInstance() {

        Bundle args = new Bundle();

        NowPlayingFragment fragment = new NowPlayingFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_now_playing;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new NowPlayingVM(self);
        return viewModel;
    }

    @Override
    protected void initView(View view) {
        tvNameSong = (TextView) view.findViewById(R.id.tv_namesong);
        adsView = (AdView) view.findViewById(R.id.adView);
        imageView = (CircleImageView) view.findViewById(R.id.img_video_cate);
    }

    public void startAnimation() {
        imageView.startAnimation(AnimationUtils.loadAnimation(imageView.getContext(), R.anim.rotation));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adsView.setVisibility(View.GONE);
                AdsUtil.loadBanner(adsView);

            }
        }, 2000);
    }

    public void stopAnimation() {
        imageView.clearAnimation();
        adsView.setVisibility(View.GONE);

    }


}
