package com.aveplus.avepay_cpocket.ebook.viewmodel.item;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.listener.IOnItemClickedListener1;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.util.StringUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.DetailsBookActivity;
import com.aveplus.avepay_cpocket.movie.movie.listener.ISetDataListener;
import com.aveplus.avepay_cpocket.movie.movie.util.DateTimeUtility;
import com.google.gson.Gson;



public class ItemBookVM extends BaseAdapterVM implements IOnItemClickedListener1,
        ISetDataListener {

    private Book book;

    public ItemBookVM(Context context, Object book, int position) {
        super(context, position);
        this.book = (Book) book;
    }

    public boolean isSelected() {
        return true;
    }

    public String getTitle() {
        return book.getTitle();
    }

    public String getImage() {
        return book.getImage();
    }

    public String getType() {
        return book.getType();
    }

    public int getBgStatus() {
        /*if (book.getStatus().equals(Constant.STATUS_BOOK_HOT)) {
            return R.drawable.status_hot;
        }
        if (book.getStatus().equals(Constant.STATUS_BOOK_NEW)) {
            return R.drawable.status_new;
        }
        if (book.getStatus().equals(Constant.STATUS_BOOK_MOST)) {
            return R.drawable.status_most;
        }*/
        return 0;
    }

    public String getStatus() {
        /*if (book.getStatus().equals(Constant.STATUS_BOOK_HOT)) {
            return self.getString(R.string.hot);
        }
        if (book.getStatus().equals(Constant.STATUS_BOOK_NEW)) {
            return self.getString(R.string.status_new);
        }
        if (book.getStatus().equals(Constant.STATUS_BOOK_MOST)) {
            return self.getString(R.string.most);
        }*/
        return "";
    }


    public String getAuthor() {
        return book.getAuthor();
    }

    public String getPublisher() {
        return book.getPublisher();
    }

    public String getDescription() {
        return book.getDescription();
    }

    public String getDateHistory() {
        if (!StringUtil.isEmpty(book.getDateHistory())) {
            return DateTimeUtility.convertTimeStampToDate(book.getDateHistory());
        } else {
            return "";
        }
    }


    @Override
    public void setData(Object object) {
        this.book = (Book) object;
        notifyChange();
    }


    @Override
    public void onItemClicked(View view) {
        Log.e("Test", "onItemClicked: test " );
        Bundle bundle = new Bundle();
        bundle.putString(Constant.KEY_BOOK, new Gson().toJson(book));
        bundle.putBoolean(Constant.KEY_OPEN_LAST_CHAPTER, true);
        AppUtil.startActivityClearTop(self, DetailsBookActivity.class, bundle);

        //((MainActivity) self).startActivityForResult(intent,69);

        book.setDateHistory(DateTimeUtility.getCurrentTimeStamp());
        DataStoreManager.addBook(book, Constant.LIST_BOOK_HISTORY);
    }
}
