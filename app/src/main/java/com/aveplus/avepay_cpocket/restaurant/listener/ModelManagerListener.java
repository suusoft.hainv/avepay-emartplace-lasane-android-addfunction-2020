package com.aveplus.avepay_cpocket.restaurant.listener;

public interface ModelManagerListener {
    void onSuccess(Object object);

    void onError();
}
