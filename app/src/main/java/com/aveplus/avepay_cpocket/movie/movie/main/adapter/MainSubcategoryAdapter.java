package com.aveplus.avepay_cpocket.movie.movie.main.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.RequestManager;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onFocusRowAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.impl.onPageChangeAdapter;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;


import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public class MainSubcategoryAdapter extends BaseAdapter {
    private final int mPagePosition = 4;
    private LayoutInflater layoutInflater;
    private List<Movie> movies;
    private int mResource;
    private Context mContext;
    private onPageChangeAdapter callback;
    private onFocusRowAdapter callbackFocus;
    private ImageLoader mImageLoader;

    public MainSubcategoryAdapter(Context context, int resource,
                                  List<Movie> movies, onPageChangeAdapter callback, onFocusRowAdapter callbackFocus) {
        
        //super(context, resource, genres);
        mContext = context;
        this.movies = movies;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mResource = resource;
        mImageLoader = RequestManager.getImageLoader();
        this.callback = callback;
        this.callbackFocus = callbackFocus;
    }

    @SuppressLint("WrongViewCast")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final Holder holder;
        if(view==null){
            view = layoutInflater.inflate(mResource,null);
             holder = new Holder();
            holder.imageView = (NetworkImageView) view.findViewById(R.id.imageView);
            holder.tv_name = (TextView) view.findViewById(R.id.tv_name);
            //set font
            AssetManager assetManager = mContext.getAssets();
            holder.tv_name.setTypeface(Typeface.createFromAsset(assetManager, "fonts/CaviarDreams_Bold.ttf"));
            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }
        if(movies!=null){
            holder.tv_name.setText(movies.get(position).getMovie_name());
            holder.imageView.setImageUrl(movies.get(position).getMovie_thumb(),mImageLoader);
        }
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onClickItemSubcate(position);
            }
        });
        view.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if(keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER) {
                        callback.onClickItemSubcate(position);
                        return true;
                    }
                    if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                        if(position != 0 && position % mPagePosition == 0 ) {
                            callback.onNextPage(position);
                            return true;
                        }if(position == movies.size()-1)
                            return true;
                    }
                    if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                        if(position != 0 && position != mPagePosition &&  position % mPagePosition == 0) {
                            callback.onPrevPage(position);
                            return true;
                        }
                    }
                }
                return false;
            }
        });
        view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                Log.i("onFocusChange", "onFocusChange: "+hasFocus);
                if(hasFocus){
                    callbackFocus.onFocusAdapter( position ,hasFocus);
                    // init paint

                }
            }
        });

        return view;
    }
    @Override
    public int getCount() {
       return movies.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class Holder{
        NetworkImageView imageView;
        TextView tv_name;
    }

}
