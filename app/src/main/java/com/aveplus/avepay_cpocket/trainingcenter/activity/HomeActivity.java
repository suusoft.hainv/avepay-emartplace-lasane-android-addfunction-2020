package com.aveplus.avepay_cpocket.trainingcenter.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.fragment.app.FragmentManager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.cart.CartManager;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.FragmentAppointment;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.FragmentCart;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.FragmentHome;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.FragmentResultNews;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.FragmentResultSearch;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.FragmentSaleOff;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.Fragment_Gallery;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.view.activities.BaseActivity;

import java.beans.PropertyChangeEvent;

public class HomeActivity extends BaseActivity implements OnClickListener,
        BaseFragment.TitleChanged, CartManager.ICartChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (CartManager.getInstance().size() > 0) {
            tvNumberOfCar.setVisibility(View.VISIBLE);
            tvNumberOfCar.setText(CartManager.getInstance().size() + "");
        } else {
            tvNumberOfCar.setVisibility(View.INVISIBLE);
        }
    }

    public interface IFragProduct {
        void imeSearch();
    }

    public static final int NEWS_BLOG = 141351413;
    public static final int PRODUCT = 1239040934;
    public int productOrNewsBlog;
    public IFragProduct mIFragProduct;
    private LinearLayout btsale, bthome, btemail, btabout;
    private RelativeLayout btcart;
    private RelativeLayout rlParent;
    ImageView imgSearch, imgcart, imgsale, imghome, imgemail, imgabout, imgBack;
    TextView tvTitle;
    private String currentTab = "";
    FragmentAppointment frmEmail;
    Fragment_Gallery frmGallery;
    private RelativeLayout rltHeader;
    private TextView tvNumberOfCar;

    public static final String TAB_CART = "cart";
    public static final String TAB_SALE = "sale";
    public static final String TAB_HOME = "home";
    public static final String TAB_EMAIL = "email";
    public static final String TAB_ABOUT = "about";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_education);
        initUI();
        initClick();
        initCurrent();
        CartManager.getInstance().addCartChangeListener(this);
    }

    @Override
    protected void inflateLayout() {

    }

    @Override
    public void onTitleChanged(String title) {
        setTitle(title);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btCart:
                changeTab(TAB_CART);

                break;

            case R.id.btSale:
                changeTab(TAB_SALE);
                break;

            case R.id.btHome:
                changeTab(TAB_HOME);
                break;

            case R.id.btEmail:
                changeTab(TAB_EMAIL);
                break;

            case R.id.btAbout:
                changeTab(TAB_ABOUT);
                break;
        }
        showHeader();
    }

    @Override
    public void onBackPressed() {
        Log.e("eee", "eee");
        super.onBackPressed();

    }

    public void changeTab(String tabName) {
        if (currentTab.equalsIgnoreCase(tabName)) {
            return;
        }
        currentTab = tabName;
        switch (tabName) {
            case TAB_CART:
                getSupportFragmentManager().popBackStack(null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right,
                                R.anim.slide_out_right)
                        .replace(R.id.layout_replace, new FragmentCart(),
                                "fragmentcart").commit();
                imgBack.setVisibility(View.INVISIBLE);
                imgcart.setImageResource(R.drawable.btn_cart_selected);
                imghome.setImageResource(R.drawable.btn_home_unselected);
                imgemail.setImageResource(R.drawable.btn_appointment_unselected);
                imgsale.setImageResource(R.drawable.btn_promotions_unselected);
                imgabout.setImageResource(R.drawable.btn_about_unselected);
                btcart.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_select));
                bthome.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btemail.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btsale.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btabout.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                break;

            case TAB_SALE:
                getSupportFragmentManager().popBackStack(null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right,
                                R.anim.slide_out_right)
                        .replace(R.id.layout_replace, new FragmentSaleOff(),
                                "fragmentsale")
                        .commit();


                imgBack.setVisibility(View.INVISIBLE);
                imgcart.setImageResource(R.drawable.btn_cart_unselected);
                imghome.setImageResource(R.drawable.btn_home_unselected);
                imgemail.setImageResource(R.drawable.btn_appointment_unselected);
                imgsale.setImageResource(R.drawable.btn_promotions_selected);
                imgabout.setImageResource(R.drawable.btn_about_unselected);

                btcart.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                bthome.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btemail.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btsale.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_select));
                btabout.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                break;

            case TAB_HOME:
                getSupportFragmentManager().popBackStack(null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_from_bottom,
                                R.anim.slide_out_to_top)
                        .replace(R.id.layout_replace, new FragmentHome(),
                                "fragmenthome").commit();
                imgBack.setVisibility(View.INVISIBLE);
                imgcart.setImageResource(R.drawable.btn_cart_unselected);
                imghome.setImageResource(R.drawable.btn_home_selected);
                imgemail.setImageResource(R.drawable.btn_appointment_unselected);
                imgsale.setImageResource(R.drawable.btn_promotions_unselected);
                imgabout.setImageResource(R.drawable.btn_about_unselected);
                imgSearch.setVisibility(View.INVISIBLE);
                btcart.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                bthome.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_select));
                btemail.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btsale.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btabout.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                break;

            case TAB_EMAIL:
                getSupportFragmentManager().popBackStack(null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left,
                                R.anim.slide_out_left)
                        .replace(R.id.layout_replace, frmEmail,
                                "fragmentemail").commit();
                imgBack.setVisibility(View.INVISIBLE);
                imgcart.setImageResource(R.drawable.btn_cart_unselected);
                imghome.setImageResource(R.drawable.btn_home_unselected);
                imgemail.setImageResource(R.drawable.btn_appointment_selected);
                imgsale.setImageResource(R.drawable.btn_promotions_unselected);
                imgabout.setImageResource(R.drawable.btn_about_unselected);
                imgSearch.setVisibility(View.INVISIBLE);
                btcart.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                bthome.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btemail.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_select));
                btsale.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btabout.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                break;

            case TAB_ABOUT:
                frmGallery = new Fragment_Gallery();
                getSupportFragmentManager().popBackStack(null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left,
                                R.anim.slide_out_left)
                        .replace(R.id.layout_replace, frmGallery,
                                "fragmentAbout").commit();
                imgBack.setVisibility(View.INVISIBLE);
                imgcart.setImageResource(R.drawable.btn_cart_unselected);
                imghome.setImageResource(R.drawable.btn_home_unselected);
                imgemail.setImageResource(R.drawable.btn_appointment_unselected);
                imgsale.setImageResource(R.drawable.btn_promotions_unselected);
                imgabout.setImageResource(R.drawable.btn_about_selected);
                imgSearch.setVisibility(View.INVISIBLE);
                btcart.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                bthome.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btemail.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btsale.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_unselect));
                btabout.setBackgroundColor(getResources().getColor(
                        R.color.color_tab_select));
                break;
        }
    }

    public void comeBackCart() {
        getSupportFragmentManager().popBackStack(null,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right,
                        R.anim.slide_out_right)
                .replace(R.id.layout_replace, new FragmentCart(),
                        "fragmentcart").commit();
        imgBack.setVisibility(View.INVISIBLE);
        imgcart.setImageResource(R.drawable.btn_cart_selected);
        imghome.setImageResource(R.drawable.btn_home_unselected);
        imgemail.setImageResource(R.drawable.btn_appointment_unselected);
        imgsale.setImageResource(R.drawable.btn_promotions_unselected);
        imgabout.setImageResource(R.drawable.btn_about_unselected);
        btcart.setBackgroundColor(getResources().getColor(
                R.color.color_tab_select));
        bthome.setBackgroundColor(getResources().getColor(
                R.color.color_tab_unselect));
        btemail.setBackgroundColor(getResources().getColor(
                R.color.color_tab_unselect));
        btsale.setBackgroundColor(getResources().getColor(
                R.color.color_tab_unselect));
        btabout.setBackgroundColor(getResources().getColor(
                R.color.color_tab_unselect));
    }

    public void setTitle(String title) {
        switch (title) {
            case "product":
                tvTitle.setText(getString(R.string.product));
                imgSearch.setVisibility(View.VISIBLE);
                imgBack.setVisibility(View.VISIBLE);

                break;

            case "service":
                tvTitle.setText(getString(R.string.faqs));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;
            case "customer":
                tvTitle.setText(getString(R.string.customer));
                imgBack.setVisibility(View.VISIBLE);

                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "newevent":
                tvTitle.setText(getString(R.string.newevent));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.VISIBLE);
                break;

            case "contact":
                tvTitle.setText(getString(R.string.contact));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "portfolios":
                tvTitle.setText(getString(R.string.port));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "home":
                tvTitle.setText(getString(R.string.project));
                imgBack.setVisibility(View.INVISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "email":
                tvTitle.setText(getString(R.string.email));

                imgBack.setVisibility(View.INVISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "detailpro":
                tvTitle.setText(getString(R.string.product_details));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "detailsale":
                tvTitle.setText(getString(R.string.sale_off_details));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "schedule":
                tvTitle.setText(getString(R.string.schedule));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;
            case "classes":
                tvTitle.setText(getString(R.string.classess));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "detailnew":
                tvTitle.setText(getString(R.string.news_event_details));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "ordercart":
                tvTitle.setText(getString(R.string.order_cart));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;
            case "ourpeople":
                tvTitle.setText(getString(R.string.our_tutor));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;
            case "detailtutor":
                tvTitle.setText(getString(R.string.detail_tutor));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "detail_order_history":
                tvTitle.setText(getString(R.string.detail_order_history));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "gallery":
                tvTitle.setText(getString(R.string.gallery));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "about":
                tvTitle.setText(getString(R.string.about_us));
                imgBack.setVisibility(View.INVISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "cart":
                tvTitle.setText(getString(R.string.my_cart));
                imgBack.setVisibility(View.INVISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "orderHistory":
                tvTitle.setText(getString(R.string.order_history));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "sale":
                tvTitle.setText(getString(R.string.sale_off));
                imgBack.setVisibility(View.INVISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;

            case "comment":
                tvTitle.setText(getString(R.string.comment));
                imgBack.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public void initCurrent() {
        changeTab(TAB_HOME);
//        FragmentManager csFragmentManager = getSupportFragmentManager();
//        csFragmentManager
//                .beginTransaction()
//                .replace(R.id.layout_replace, new FragmentHome(),
//                        "fragmenthome").commit();
    }

    public void initUI() {
        imgSearch = (ImageView) findViewById(R.id.btnClose);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        btabout = (LinearLayout) findViewById(R.id.btAbout);
        btcart = (RelativeLayout) findViewById(R.id.btCart);
        bthome = (LinearLayout) findViewById(R.id.btHome);
        btsale = (LinearLayout) findViewById(R.id.btSale);
        btemail = (LinearLayout) findViewById(R.id.btEmail);
        imgabout = (ImageView) findViewById(R.id.imgAbout);
        imgcart = (ImageView) findViewById(R.id.imgCart);
        imghome = (ImageView) findViewById(R.id.imgHome);
        imgsale = (ImageView) findViewById(R.id.imgSale);
        imgemail = (ImageView) findViewById(R.id.imgEmail);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        rlParent = (RelativeLayout) findViewById(R.id.rl_main);
        rltHeader = (RelativeLayout) findViewById(R.id.layout_header);
        tvNumberOfCar = (TextView) findViewById(R.id.tv_number_of_car);

        frmEmail = new FragmentAppointment();
        frmGallery = new Fragment_Gallery();

    }

    @Override
    protected void initControl() {

    }

    public void initClick() {
        btcart.setOnClickListener(this);
        btabout.setOnClickListener(this);
        bthome.setOnClickListener(this);
        btemail.setOnClickListener(this);
        btsale.setOnClickListener(this);

        imgSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//                startActivity(ResultSearchActivity.class);
                switch (productOrNewsBlog) {
                    case NEWS_BLOG:
                        FragmentResultNews news = new FragmentResultNews();
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_left,
                                        R.anim.slide_out_left)
                                .replace(R.id.childReplace, news, "resultsearchnew")
                                .addToBackStack("resultsearchnew").commit();
                        break;
                    case PRODUCT:
                        FragmentResultSearch fragment = new FragmentResultSearch();
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_left,
                                        R.anim.slide_out_left)
                                .replace(R.id.showPro, fragment, "resultsearch")
                                .addToBackStack("resultsearch").commit();
                        break;
                }

                hideHeader();

            }
        });
        imgBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                tvTitle.setVisibility(View.VISIBLE);
                imgSearch.setVisibility(View.INVISIBLE);
                showHeader();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                onBackPressed();
            }
        });
    }

    public void showHeader() {
        if (!rltHeader.isShown()) {
            rltHeader.setVisibility(View.VISIBLE);
        }
    }

    public void hideHeader() {
        if (rltHeader.isShown()) {
            rltHeader.setVisibility(View.GONE);
            Log.e("HIDE", "HIDE HEADER");
        }
    }

    public void onListenerSearch(IFragProduct mIFragProduct) {
        this.mIFragProduct = mIFragProduct;
    }
}
