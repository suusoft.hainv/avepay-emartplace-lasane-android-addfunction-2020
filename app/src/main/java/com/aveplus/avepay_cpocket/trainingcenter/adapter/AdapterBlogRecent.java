package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.activity.BlogDetailActivity;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.aveplus.avepay_cpocket.trainingcenter.view.IOnClick;
import com.aveplus.avepay_cpocket.trainingcenter.view.IOnClickBlogRecent;
import com.aveplus.avepay_cpocket.trainingcenter.view.IOnClickItem;
import com.aveplus.avepay_cpocket.utils.ImageUtil;

import java.util.ArrayList;

public class AdapterBlogRecent extends RecyclerView.Adapter<AdapterBlogRecent.MyViewHolder> {

    private ArrayList<NewEvent> newEvents;
    Context context;
    private IOnClickBlogRecent onClick;
    private IOnClickItem iOnClickItem;


    public AdapterBlogRecent(ArrayList<NewEvent> newEvents, Context context, IOnClickBlogRecent onClick) {
        this.newEvents = newEvents;
        this.context = context;
        this.onClick = onClick;
    }

    public void addList(ArrayList<NewEvent> newEvents) {
        this.newEvents = newEvents;
        this.notifyDataSetChanged();
    }

    public AdapterBlogRecent() {
    }


    @NonNull
    @Override
    public AdapterBlogRecent.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_event, parent, false);
        return new AdapterBlogRecent.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterBlogRecent.MyViewHolder holder, int position) {
        final NewEvent newEvent = newEvents.get(position);

        ImageUtil.setImage(holder.img, newEvent.getImage());
        holder.txtNewTitle.setText(newEvent.getTitle());
        holder.txtNewDate.setText(newEvent.getTime());
        holder.txtReadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (iOnClickItem != null) {
//                    iOnClickItem.OnClick(position);
//                }
//                onclickItem(position);
            }
        });

        holder.rltBlogRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClick != null) {
                    onClick.OnClick(position, newEvent);

                }
//                onclickItem(position);
            }
        });

    }

    private void onclickItem(int position) {
//        NewEvent item = newEvents.get(position);
//        Bundle bundle = new Bundle();
//        bundle.putString("id", item.getId());
//        bundle.putString("title", item.getTitle());
//        bundle.putString("description", item.getDescription());
//        bundle.putString("content", item.getContent());
//        bundle.putString("image", item.getImage());
//        bundle.putString("time", item.getTime());
//        bundle.putString("link", item.getLinkNews());
//        Intent intent = new Intent(context, BlogDetailActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        intent.putExtra("bundle", bundle);
//        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return newEvents == null ? 0 : newEvents.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView txtNewTitle, txtNewDate, txtReadMore;
        private RelativeLayout rltBlogRecent;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imageSaleOff);
            txtNewTitle = (TextView) itemView.findViewById(R.id.txtNewTitle);
            txtNewDate = (TextView) itemView.findViewById(R.id.txtNewDate);
            txtReadMore = (TextView) itemView.findViewById(R.id.txtReadMore);
            rltBlogRecent = (RelativeLayout) itemView.findViewById(R.id.relativeLayout1);

        }
    }
}
