package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;

import java.util.regex.Matcher;


/**
 * Created by Ha on 10/6/2017.
 */

public class FragmentComment extends BaseFragment implements View.OnClickListener {
    private EditText edtName;
    private EditText edtEmail;
    private EditText edtComment;
    private TextView btnComment;
    private TextView tvNotify;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("comment");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("classes");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment, container, false);
        init(view);
        initControl();
        return view;
    }

    private void init(View view) {
        edtName = (EditText) view.findViewById(R.id.edt_name);
        edtEmail = (EditText) view.findViewById(R.id.edt_email);
        edtComment = (EditText) view.findViewById(R.id.edt_comment);
        btnComment = (TextView) view.findViewById(R.id.btn_comment);
        tvNotify = (TextView) view.findViewById(R.id.tv_notify);

    }

    private void initControl() {
        btnComment.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_comment:
                if (edtName.getText().toString().isEmpty()) {
                    Toast.makeText(self, getString(R.string.msg_field_name), Toast.LENGTH_SHORT).show();
                    edtName.requestFocus();
                    return;
                }
                Matcher isUserMail = Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString());
                if (!isUserMail.matches()) {
                    Toast.makeText(self, getString(R.string.msg_field_email_invalid), Toast.LENGTH_SHORT).show();
                    edtEmail.requestFocus();
                    return;
                }
                if (edtComment.getText().toString().isEmpty()) {
                    Toast.makeText(self, getString(R.string.msg_field_comment), Toast.LENGTH_SHORT).show();
                    edtComment.requestFocus();
                    return;
                }
                ModelManager.sendComment(
                        edtName.getText().toString(),
                        edtEmail.getText().toString(),
                        "",
                        edtComment.getText().toString(), new BaseRequest.CompleteListener() {
                            @Override
                            public void onSuccess(ApiResponse response) {
                                edtName.setText("");
                                edtEmail.setText("");
                                edtComment.setText("");
                                tvNotify.setText(getString(R.string.success_send_testimonials));

                            }

                            @Override
                            public void onError(String message) {
                                tvNotify.setText(getString(R.string.error_send_testimonials));

                            }
                        });
                break;
        }
    }
}
