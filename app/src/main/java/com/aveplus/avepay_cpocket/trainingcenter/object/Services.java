package com.aveplus.avepay_cpocket.trainingcenter.object;

import android.os.Parcel;
import android.os.Parcelable;

public class Services implements Parcelable {

	private String id, title, description, content;
	private String image;

	public Services(String id, String title, String description,
                    String content, String image) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.content = content;
		this.image = image;
	}

	public Services() {
		// TODO Auto-generated constructor stub
	}

	protected Services(Parcel in) {
		id = in.readString();
		title = in.readString();
		description = in.readString();
		content = in.readString();
		image = in.readString();
	}

	public static final Creator<Services> CREATOR = new Creator<Services>() {
		@Override
		public Services createFromParcel(Parcel in) {
			return new Services(in);
		}

		@Override
		public Services[] newArray(int size) {
			return new Services[size];
		}
	};

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(content);
		dest.writeString(image);
	}
}
