package com.aveplus.avepay_cpocket.taxi.retrofit.response;

import android.app.Activity;
import android.content.Context;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Global;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.DialogUtil;
import com.aveplus.avepay_cpocket.taxi.view.activities.SplashLoginActivity;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;

public class BaseModel {

    public String status;
    public int code;
    public String message;

    public String toJson() {
        return new Gson().toJson(this);
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess(){
        if ( getCode() == 200 || getCode() == 202) {
            return true;
        }
        return false;

    }

    public boolean isSuccess(final Context context){
        if ( getCode() == 200 || getCode() == 202){
            return true;
        }else if (isTokenMismatch()){

            DialogUtil.showAlertDialog(context,
                    R.string.the_login_session_expires_you_must_login_again,
                    false, false, new DialogUtil.IDialogConfirm() {
                        @Override
                        public void onClickOk() {
                            DataStoreManager.removeUser();

                            AppUtil.startActivity(context, SplashLoginActivity.class);
                            if (((Activity)context)!=null){
                                ((Activity)context).finish();
                            }

                            EventBus.getDefault().post(new Global.FinishAllCurrentAct());

                        }
                    });

            return false;

        }

        return false;

    }

    public boolean isTokenMismatch(){
        return getCode() == 205;
    }

}
