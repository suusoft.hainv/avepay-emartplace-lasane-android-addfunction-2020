package com.aveplus.avepay_cpocket.music.main.video2;

import android.content.Intent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.music.retrofit.ApiUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListVideoAct extends BaseActivity {
    private RecyclerView rcvVideo;
    private ArrayList<ObjVideo> arrVideo;
    private ArrayList<ObjVideoCategory> arrVideoCate;
    private AdapterMore adapterMore;
    private TextView tvEmpty;
    private ProgressBar progress;


    public static String NAME_PLAYLIST = "NAME_PLAYLIST";
    public static String ID_CATEGORY = "ID_CATEGORY";
    public static String ID_PLAYLIST = "ID_PLAYLIST";
    public static String FULL_CATEGORY = "FULL_CATEGORY";

    private String namePlaylist, idCate, idPlaylist, fullCate;

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_list_video;
    }

    @Override
    protected void initBeforeChildView() {
        super.initBeforeChildView();
        Intent intent = getIntent();
        namePlaylist = intent.getStringExtra(NAME_PLAYLIST);
        idCate = intent.getStringExtra(ID_CATEGORY);
        idPlaylist = intent.getStringExtra(ID_PLAYLIST);
        fullCate = intent.getStringExtra(FULL_CATEGORY);
    }

    @Override
    protected void initView() {
        rcvVideo = findViewById(R.id.rcv_video);
        progress = findViewById(R.id.progress);
        tvEmpty = findViewById(R.id.tv_empty);

    }

    @Override
    protected void onViewCreated() {
        if (namePlaylist != null) {
            setToolbarTitle(namePlaylist);
        } else if (fullCate != null) {
            setToolbarTitle(fullCate);
        }
        setAdapterRCV();
        getData();
    }

    private void getData() {
        if (idCate != null && !idCate.isEmpty()) {
            getVideoByCategory(idCate);
        } else if (idPlaylist != null && !idPlaylist.isEmpty()) {
            getVideoByPlaylist(idPlaylist);
        } else if (fullCate != null && !fullCate.isEmpty()) {
            getFullCategory();
        }
    }

    private void getFullCategory() {
        ApiUtils.getAPIService().getListCategory().enqueue(new Callback<ResponseCategory>() {
            @Override
            public void onResponse(Call<ResponseCategory> call, Response<ResponseCategory> response) {
                if (response.body() != null) {
                    progress.setVisibility(View.GONE);
                    tvEmpty.setVisibility(View.GONE);
                    arrVideoCate.addAll(response.body().getData());
                    rcvVideo.setLayoutManager(new GridLayoutManager(self, 2, RecyclerView.VERTICAL, false));
                    adapterMore.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ResponseCategory> call, Throwable t) {

            }
        });
    }

    private void getVideoByCategory(String idCate) {
        ApiUtils.getAPIService().getVideoCate(idCate).enqueue(new Callback<ResponeMoreVideo>() {
            @Override
            public void onResponse(Call<ResponeMoreVideo> call, Response<ResponeMoreVideo> response) {
                if (response.body() != null) {
                    tvEmpty.setVisibility(View.GONE);
                    progress.setVisibility(View.GONE);
                    arrVideo.clear();
                    arrVideo.addAll(response.body().getData());
                    adapterMore.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<ResponeMoreVideo> call, Throwable t) {

            }
        });
    }

    private void getVideoByPlaylist(String idVideo) {
        ApiUtils.getAPIService().getVideo(idVideo).enqueue(new Callback<ResponeMoreVideo>() {
            @Override
            public void onResponse(Call<ResponeMoreVideo> call, Response<ResponeMoreVideo> response) {
                if (response.body() != null) {
                    tvEmpty.setVisibility(View.GONE);
                    progress.setVisibility(View.GONE);
                    arrVideo.clear();
                    arrVideo.addAll(response.body().getData());
                    adapterMore.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<ResponeMoreVideo> call, Throwable t) {

            }
        });
    }

    private void setAdapterRCV() {
        arrVideo = new ArrayList<>();
        arrVideoCate = new ArrayList<>();
        adapterMore = new AdapterMore(arrVideo, arrVideoCate, new AdapterMore.CategoryClickListener() {
            @Override
            public void onListener(ObjVideoCategory objVideoCategory) {
                setToolbarTitle(objVideoCategory.getName());
                arrVideoCate.clear();
                rcvVideo.setLayoutManager(new LinearLayoutManager(self, RecyclerView.VERTICAL, false));
                getVideoByCategory(objVideoCategory.getId().toString());
            }
        });
        rcvVideo.setLayoutManager(new LinearLayoutManager(self, RecyclerView.VERTICAL, false));
        rcvVideo.setAdapter(adapterMore);
    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {

    }
}
