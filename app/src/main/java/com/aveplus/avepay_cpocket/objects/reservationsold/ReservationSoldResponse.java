package com.aveplus.avepay_cpocket.objects.reservationsold;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ReservationSoldResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<ReserSoldItem> data;

	@SerializedName("total_page")
	private int totalPage;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<ReserSoldItem> data){
		this.data = data;
	}

	public List<ReserSoldItem> getData(){
		return data;
	}

	public void setTotalPage(int totalPage){
		this.totalPage = totalPage;
	}

	public int getTotalPage(){
		return totalPage;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}
