package com.aveplus.avepay_cpocket.taxi.objects.book;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;


import com.aveplus.avepay_cpocket.R;

import java.util.ArrayList;

import static com.aveplus.avepay_cpocket.taxi.utils.ResourceUtils.getString;


/**
 * Created by SuuSoft.com on 11/29/2016.
 */

public class ServiceObj implements Parcelable {

    // This constants MUST be match with server
    public static final String ALL = "all";
    public static final String TAXI = "taxi";
    public static final String VIP = "vip";
    public static final String LIFTS = "lift";
    public static final String MOTORBIKE = "moto";
    public static final String DELIVERY = "delivery";

    // This constants MUST be match with server
    public static final String SUV = "suv";
    public static final String REGULAR = "regular";
    public static final String AIR_CONDITIONED = "air_conditioned";
    public static final String VIP1 = "vip";

//    public static final String SUV = "suv";
//    public static final String REGULAR = "classic";
//    public static final String AIR_CONDITIONED = "rickshaw";
//    public static final String VIP1 = "tuk_tuk";

    private String type, name;
    private int icon;

    private String id;
    private String title;
    private String image;
    private String price;
    private String content;

    private double minimumFare;
    private double flagDownFee;
    private double priceMinute;
    private double priceKilomet;
    private int person;

    public ServiceObj(String type, String name, int icon) {
        this.type = type;
        this.name = name;
        this.icon = icon;
    }

    public ServiceObj(String type, String name) {
        this.type = type;
        this.name = name;
    }

//    public static ArrayList<ServiceObj> getListService(Context context) {
//        final ArrayList<ServiceObj> transportObjs = new ArrayList<>();
//        transportObjs.add(new ServiceObj(ServiceObj.TAXI, context.getString(R.string.taxi)));
//        transportObjs.add(new ServiceObj(ServiceObj.VIP, context.getString(R.string.vip)));
//        transportObjs.add(new ServiceObj(ServiceObj.LIFTS, context.getString(R.string.lifts)));
//        transportObjs.add(new ServiceObj(ServiceObj.MOTORBIKE, context.getString(R.string.motorbike)));
//        return transportObjs;
//    }

    protected ServiceObj(Parcel in) {
        type = in.readString();
        name = in.readString();
        icon = in.readInt();
        minimumFare = in.readDouble();
        flagDownFee = in.readDouble();
        priceMinute = in.readDouble();
        priceKilomet = in.readDouble();
        person = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(name);
        dest.writeInt(icon);
        dest.writeDouble(minimumFare);
        dest.writeDouble(flagDownFee);
        dest.writeDouble(priceMinute);
        dest.writeDouble(priceKilomet);
        dest.writeInt(person);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceObj> CREATOR = new Creator<ServiceObj>() {
        @Override
        public ServiceObj createFromParcel(Parcel in) {
            return new ServiceObj(in);
        }

        @Override
        public ServiceObj[] newArray(int size) {
            return new ServiceObj[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return super.toString();
    }



    public double getMinimumFare() {
        return minimumFare;
    }

    public void setMinimumFare(double minimumFare) {
        this.minimumFare = minimumFare;
    }

    public double getFlagDownFee() {
        return flagDownFee;
    }

    public void setFlagDownFee(double flagDownFee) {
        this.flagDownFee = flagDownFee;
    }

    public double getPriceMinute() {
        return priceMinute;
    }

    public void setPriceMinute(double priceMinute) {
        this.priceMinute = priceMinute;
    }

    public double getPriceKilomet() {
        return priceKilomet;
    }

    public void setPriceKilomet(double priceKilomet) {
        this.priceKilomet = priceKilomet;
    }

    public int getPerson() {
        return person;
    }

    public void setPerson(int person) {
        this.person = person;
    }

    public static ArrayList<ServiceObj> getListService(){
        ArrayList<ServiceObj> serviceObjs = new ArrayList<>();

        ServiceObj serviceObj = new ServiceObj(ServiceObj.REGULAR, getString(R.string.classic), R.drawable.ic_taxi_regular);
        serviceObjs.add(serviceObj);

         serviceObj = new ServiceObj(ServiceObj.AIR_CONDITIONED, getString(R.string.richshaw), R.drawable.ic_taxi_air_conditioned);
        serviceObjs.add(serviceObj);

        serviceObj = new ServiceObj(ServiceObj.VIP1, getString(R.string.khmer_tuk_tuk), R.drawable.ic_taxi_vip);
        serviceObjs.add(serviceObj);

        return serviceObjs;

    }

    public static ArrayList<ServiceObj> getListService(Context context){
        ArrayList<ServiceObj> serviceObjs = new ArrayList<>();

        ServiceObj serviceObj = new ServiceObj(ServiceObj.REGULAR, getString(R.string.classic), R.drawable.ic_4cho);
//        serviceObj.setMinimumFare(3000 );
//        serviceObj.setFlagDownFee(2000);
//        serviceObj.setPerson(2);
//        serviceObj.setPriceKilomet(2000);
//        serviceObj.setPriceMinute(200);
        serviceObjs.add(serviceObj);

        serviceObj = new ServiceObj(ServiceObj.AIR_CONDITIONED, getString(R.string.richshaw), R.drawable.ic_4cho);
//        serviceObj.setMinimumFare(4000);
//        serviceObj.setFlagDownFee(2000);
//        serviceObj.setPerson(4);
//        serviceObj.setPriceKilomet(2000);
//        serviceObj.setPriceMinute(250);
        serviceObjs.add(serviceObj);

        serviceObj = new ServiceObj(ServiceObj.VIP1, getString(R.string.khmer_tuk_tuk), R.drawable.ic_4cho);
//        serviceObj.setMinimumFare(3000);
//        serviceObj.setFlagDownFee(2000);
//        serviceObj.setPerson(7);
//        serviceObj.setPriceKilomet(1200);
//        serviceObj.setPriceMinute(200);
        serviceObjs.add(serviceObj);

        return serviceObjs;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
