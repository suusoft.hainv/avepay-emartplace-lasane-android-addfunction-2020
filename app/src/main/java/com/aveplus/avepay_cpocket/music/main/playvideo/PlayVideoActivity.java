package com.aveplus.avepay_cpocket.music.main.playvideo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListActivityBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.main.payment.BottomPaymentFragment;
import com.aveplus.avepay_cpocket.music.main.payment.IPayement;
import com.aveplus.avepay_cpocket.music.main.payment.ListSubscribeActivity;
import com.aveplus.avepay_cpocket.music.main.video2.ObjVideo;
import com.aveplus.avepay_cpocket.music.main.video2.VideoPlayVM;
import com.aveplus.avepay_cpocket.music.suuplayer.SuuPlayer;
import com.aveplus.avepay_cpocket.music.suuplayer.SuuPlayerImpl;
import com.aveplus.avepay_cpocket.music.suuplayer.SuuPlayerView;
import com.aveplus.avepay_cpocket.music.util.DateTimeUtility;
import com.aveplus.avepay_cpocket.music.video.VideoAdapter;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.esewa.android.sdk.payment.ESewaConfiguration;
import com.esewa.android.sdk.payment.ESewaPayment;
import com.esewa.android.sdk.payment.ESewaPaymentActivity;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import static com.aveplus.avepay_cpocket.music.configs.Constant.PREF_KEY_DATA_LIST;


public class PlayVideoActivity extends BaseListActivityBinding implements OnTrailerListener, SuuPlayer.EventListener, View.OnClickListener {
    private static final String TAG = PlayVideoActivity.class.getSimpleName();
    private ObjVideo item;
    private ArrayList<ObjVideo> list = new ArrayList<>();
    private VideoPlayVM viewModel;

    private VideoAdapter mAdapter;
    private int mPosition;

    private SuuPlayerView playerView;
    private SuuPlayer player;

    private SeekBar seekBar;
    private TextView tvTitle, tvStartTime, tvTotalTime;
    private ImageButton btnFavorite, btnShare, btnPlay, btnNext, btnPrev, btnFullScreen, btnForWard, btnReplay, btnAddtoList;
    private RelativeLayout contentView, rlPlayerView;
    private ProgressBar progressBar;
    private Animation animation;


    //Ewasa
    private static final String CONFIG_ENVIRONMENT = ESewaConfiguration.ENVIRONMENT_TEST;
    private static final int REQUEST_CODE_PAYMENT = 1;
    private ESewaConfiguration eSewaConfiguration;

    private static final String MERCHANT_ID = "JB0BBQ4aD0UqIThFJwAKBgAXEUkEGQUBBAwdOgABHD4DChwUAB0R";
    private static final String MERCHANT_SECRET_KEY = "BhwIWQQADhIYSxILExMcAgFXFhcOBwAKBgAXEQ==";


    // views
    private AndVideoView mVideoView;
    private RelativeLayout mMediaController;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            tvStartTime.setText(DateTimeUtility.convertSecondsToHMmSs(player.getCurrentPosition()));
            seekBar.setProgress((int) player.getCurrentPosition());
            handler.postDelayed(this, 1000);
        }
    };
    private SuuPlayerListener playerListener;

    @Override
    public void onClick(View view) {
        if (view == btnPlay) {
            tooglePlayer();
        } else if (view == btnFullScreen) {
            actionFullScreen();
        } else if (view == playerView) {
            Log.e(TAG, "onClick: playerview");
            btnPlay.startAnimation(animation);
            removeTimer();
            toolbar.setVisibility(View.VISIBLE);
        } else if (view == rlPlayerView) {
            Log.e(TAG, "onClick: rlplayerview");
            tooglePlayer();
//            btnPlay.startAnimation(animation);
//            removeTimer();
//            toolbar.setVisibility(View.VISIBLE);
        }
    }

    interface SuuPlayerListener {
        void onFullScreen(boolean isFull);
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new VideoPlayVM(self, mPosition);
        return viewModel;
    }


    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        Log.e(TAG, "setUpRecyclerView: !ListVideoActivity");
        mAdapter = new VideoAdapter(self, list);
        recyclerView.setLayoutManager(viewModel.getLayoutManager());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(viewModel.getOnScrollListener());


        tvTitle = findViewById(R.id.tv_title_video);

        btnFavorite = findViewById(R.id.btn_favorite);
        btnShare = findViewById(R.id.btn_share);
        contentView = findViewById(R.id.frm_content);
        tvStartTime = findViewById(R.id.txt_start_time);
        tvTotalTime = findViewById(R.id.txt_total_time);
        btnPlay = findViewById(R.id.btn_play);
        btnNext = findViewById(R.id.btn_next);
        btnPrev = findViewById(R.id.btn_prev);
        btnFullScreen = findViewById(R.id.btn_fullscreen1);
        rlPlayerView = findViewById(R.id.rl_playerview);
        seekBar = findViewById(R.id.srb_progress);
        btnForWard = findViewById(R.id.btn_forward);
        btnReplay = findViewById(R.id.btn_replay);
        btnAddtoList = findViewById(R.id.btn_add_to_list);
        playerView = findViewById(R.id.player_view);
        progressBar = findViewById(R.id.progress_bar);

        progressBar.setVisibility(View.VISIBLE);
        setVisibleProgressBar(true);
        initPlayer();
        initAnimationTooglePlay();
        preparing(0);
        btnPlay.setOnClickListener(this);
        btnFullScreen.setOnClickListener(this);
        playerView.setOnClickListener(this);
        rlPlayerView.setOnClickListener(this);

    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_play_video;
    }

    @Override
    protected void onViewCreated() {
//        setTitleColor (R.color.black);
        if (item != null) {
            Log.e(TAG, "onViewCreated: " + item.getName());
            tvTitle.setText(item.getName());
            setToolbarTitle("");
        }
        makeToolbarTransparent();


    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {
        Bundle bundle = getIntent().getBundleExtra(Constant.PREF_KEY_DATA);
        if (bundle != null) {
            item = bundle.getParcelable(Constant.PREF_KEY_OBJECT);
            list = bundle.getParcelableArrayList(PREF_KEY_DATA_LIST);
            Log.e(TAG, "onPrepareCreateView: " + list.size());
            mPosition = bundle.getInt("mPosition");
//            preparing(0);
        } else finish();
        eSewaConfiguration = new ESewaConfiguration()
                .clientId(MERCHANT_ID)
                .secretKey(MERCHANT_SECRET_KEY)
                .environment(CONFIG_ENVIRONMENT);
        //makePayment("10");
        Log.e(TAG, "onPrepareCreateView: " + new Gson().toJson(item) + mPosition);

    }

    @Override
    public void onCreateTrailer(String mTrailerKey) {
//        mYoutubeFragment.setTrailerId(mTrailerKey);

    }

    @Override
    public void onPlayTrailer(String mTrailerKey) {
//        mYoutubeFragment.setTrailerId(mTrailerKey);
//        mYoutubeFragment.playTrailer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.VideoItemClicked event) {
        ObjVideo video = event.type;
        setToolbarTitle("");
        tvTitle.setText(video.getName());

        AppUtil.checkPayment(DataStoreManager.getUser().getToken());
        if (video.getIsFree() == 0) {
            if (DataStoreManager.getCheckPayment()) {
                setVisibleProgressBar(true);
                player.prepare(event.type.getVideo(), SuuPlayerImpl.TYPE_OTHER);
            } else {
                BottomPaymentFragment bottomPaymentFragment = new BottomPaymentFragment(new IPayement() {
                    @Override
                    public void onPayment(String payment) {
                        Intent intent = new Intent(self, ListSubscribeActivity.class);
                        self.startActivity(intent);
                    }
                });
                bottomPaymentFragment.show(((PlayVideoActivity) self).getSupportFragmentManager(), bottomPaymentFragment.getTag());
            }
        } else {
            setVisibleProgressBar(true);
            player.prepare(event.type.getVideo(), SuuPlayerImpl.TYPE_OTHER);
        }
    }

    @Override
    protected void initView() {
        super.initView();
    }

    private void preparing(int position) {
        if (item != null) {
            Log.e(TAG, "preparing: " + item.getVideo());
            player.prepare(item.getVideo(), SuuPlayerImpl.TYPE_OTHER);
        }
    }


    private void setVisibleProgressBar(boolean isVisible) {
        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
            btnPlay.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            btnPlay.setVisibility(View.VISIBLE);
        }
    }

    private void initPlayer() {
        playerView.setUseHandControl(true);
        player = new SuuPlayerImpl(self, playerView);
        player.addEventListener(this);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                player.seekTo(seekBar.getProgress());
            }
        });
    }

    @Override
    public void onStared() {
        setVisibleProgressBar(false);
        checkPlayingItem();

    }

    @Override
    public void onPlaying() {
        btnPlay.startAnimation(animation);
        checkPlayingItem();
        toolbar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPaused() {
        btnPlay.startAnimation(animation);
        removeTimer();
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStopped() {
        removeTimer();
        btnPlay.startAnimation(animation);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.pause();
    }

    @Override
    public void onCompletion() {

    }

    @Override
    public void onFullScreen(boolean isFullScreen) {
        if (playerListener != null) {
            playerListener.onFullScreen(isFullScreen);
        }
    }

    @Override
    public void onBuffering(boolean var1) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.e("onPlayerError", "error: " + error.getMessage());
    }

    private void checkPlayingItem() {
        handler.postDelayed(runnable, 1000);
        tvTotalTime.setText(DateTimeUtility.convertSecondsToHMmSs(player.getDuration()));
        seekBar.setMax((int) player.getDuration());

    }


    private void removeTimer() {
        handler.removeCallbacks(runnable);
    }

    private void initAnimationTooglePlay() {
        animation = AnimationUtils.loadAnimation(self, R.anim.rotate);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (player != null && player.isPlaying()) {
                    btnPlay.setImageResource(R.drawable.ic_pause_black_24d_2);
                } else {
                    btnPlay.setImageResource(R.drawable.ic_play_white2);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void tooglePlayer() {
        player.togglePlay();
        btnPlay.startAnimation(animation);
    }

    private void actionFullScreen() {
        playerView.setFullScreen(true);
        makeToolbarTransparent();
    }

    private void makePayment(String amount) {
        ESewaPayment eSewaPayment = new ESewaPayment(amount, "someProductName", "someUniqueId_" + System.nanoTime(), "https://somecallbackurl.com");
        Intent intent = new Intent(PlayVideoActivity.this, ESewaPaymentActivity.class);
        intent.putExtra(ESewaConfiguration.ESEWA_CONFIGURATION, eSewaConfiguration);
        intent.putExtra(ESewaPayment.ESEWA_PAYMENT, eSewaPayment);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == RESULT_OK) {
                String s = data.getStringExtra(ESewaPayment.EXTRA_RESULT_MESSAGE);
                Log.e("Proof of Payment", s);
                Toast.makeText(this, "SUCCESSFUL PAYMENT", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Canceled By User", Toast.LENGTH_SHORT).show();
            } else if (resultCode == ESewaPayment.RESULT_EXTRAS_INVALID) {
                String s = data.getStringExtra(ESewaPayment.EXTRA_RESULT_MESSAGE);
                Log.e("Proof of Payment", s);
            }
        }
    }
}
