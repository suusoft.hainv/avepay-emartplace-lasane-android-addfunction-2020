package com.aveplus.avepay_cpocket.objects.reservationsold;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("buyer_visible")
	private int buyerVisible;

	@SerializedName("buyer_avatar")
	private String buyerAvatar;

	@SerializedName("deal")
	private Deal deal;

	@SerializedName("is_active")
	private int isActive;

	@SerializedName("seller_visible")
	private int sellerVisible;

	@SerializedName("payment_status")
	private String paymentStatus;

	@SerializedName("seller_avatar")
	private String sellerAvatar;

	@SerializedName("buyer_name")
	private String buyerName;

	@SerializedName("buyer_id")
	private int buyerId;

	@SerializedName("modified_date")
	private Object modifiedDate;

	@SerializedName("buyer_confirm")
	private int buyerConfirm;

	@SerializedName("seller_confirm")
	private int sellerConfirm;

	@SerializedName("seller_name")
	private String sellerName;

	@SerializedName("balance")
	private Object balance;

	@SerializedName("deal_name")
	private String dealName;

	@SerializedName("price")
	private int price;

	@SerializedName("id")
	private int id;

	@SerializedName("time")
	private String time;

	@SerializedName("created_date")
	private String createdDate;

	@SerializedName("deal_id")
	private int dealId;

	@SerializedName("seller_id")
	private int sellerId;

	@SerializedName("payment_method")
	private String paymentMethod;

	@SerializedName("status")
	private String status;

	public void setBuyerVisible(int buyerVisible){
		this.buyerVisible = buyerVisible;
	}

	public int getBuyerVisible(){
		return buyerVisible;
	}

	public void setBuyerAvatar(String buyerAvatar){
		this.buyerAvatar = buyerAvatar;
	}

	public String getBuyerAvatar(){
		return buyerAvatar;
	}

	public void setDeal(Deal deal){
		this.deal = deal;
	}

	public Deal getDeal(){
		return deal;
	}

	public void setIsActive(int isActive){
		this.isActive = isActive;
	}

	public int getIsActive(){
		return isActive;
	}

	public void setSellerVisible(int sellerVisible){
		this.sellerVisible = sellerVisible;
	}

	public int getSellerVisible(){
		return sellerVisible;
	}

	public void setPaymentStatus(String paymentStatus){
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentStatus(){
		return paymentStatus;
	}

	public void setSellerAvatar(String sellerAvatar){
		this.sellerAvatar = sellerAvatar;
	}

	public String getSellerAvatar(){
		return sellerAvatar;
	}

	public void setBuyerName(String buyerName){
		this.buyerName = buyerName;
	}

	public String getBuyerName(){
		return buyerName;
	}

	public void setBuyerId(int buyerId){
		this.buyerId = buyerId;
	}

	public int getBuyerId(){
		return buyerId;
	}

	public void setModifiedDate(Object modifiedDate){
		this.modifiedDate = modifiedDate;
	}

	public Object getModifiedDate(){
		return modifiedDate;
	}

	public void setBuyerConfirm(int buyerConfirm){
		this.buyerConfirm = buyerConfirm;
	}

	public int getBuyerConfirm(){
		return buyerConfirm;
	}

	public void setSellerConfirm(int sellerConfirm){
		this.sellerConfirm = sellerConfirm;
	}

	public int getSellerConfirm(){
		return sellerConfirm;
	}

	public void setSellerName(String sellerName){
		this.sellerName = sellerName;
	}

	public String getSellerName(){
		return sellerName;
	}

	public void setBalance(Object balance){
		this.balance = balance;
	}

	public Object getBalance(){
		return balance;
	}

	public void setDealName(String dealName){
		this.dealName = dealName;
	}

	public String getDealName(){
		return dealName;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setDealId(int dealId){
		this.dealId = dealId;
	}

	public int getDealId(){
		return dealId;
	}

	public void setSellerId(int sellerId){
		this.sellerId = sellerId;
	}

	public int getSellerId(){
		return sellerId;
	}

	public void setPaymentMethod(String paymentMethod){
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentMethod(){
		return paymentMethod;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}