package com.aveplus.avepay_cpocket.movie.movie.main.movies.impl;


import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;

import java.util.List;

/**
 */
public interface SearchMoviesCallback {
    public void onCompleted(List<Movie> searchResult);

    public void onFailed(Exception e);
}
