package com.aveplus.avepay_cpocket.restaurant.retrofit.param;

public interface Param {
    //intent put extra frag home
    String ID_RESTAURANT ="id_restaurant";
    String USER_LAT ="user_lat";
    String USER_LONG ="user_long";
//intent put extra frag  restaurant
    String POSITION ="position";
    String LIST_PHOTO ="listphoto";
    String NAME_RESTAURANT ="listphoto";
    // Param Nearby Restaurant
     String PARAM_BUYER_LAT = "buyer_lat";
   String PARAM_BUYER_LONG = "buyer_long";

   //Param Detail Restaurant
     String PARAM_RESTAURANT_ID = "restaurant_id";
    String PARAM_USER_LAT = "user_lat";
    String PARAM_USER_LONG = "user_long";
    //inten put extra oder
    String PARAM_NAME = "name";
    String PARAM_PRICE = "price";
    String PARAM_ATTACHMENT = "attachment";
    String PARAM_CITY = "city";
    String PARAM_RATE = "rate";

    // put order
    String PARAM_ORDER_ID = "id";
    String PARAM_BILLINGNAME = "billingName";
    String PARAM_BILLINGADDRESS = "billingAddress";
    String PARAM_BILLINGPHONE = "billingPhone";
    String PARAM_SHIPPINGADDRESS = "shippingAddress";
    String PARAM_PAYMENTMETHOD = "paymentMethod";
    String PARAM_CONTENT = "content";
    String PARAM_DESTINATION = "destination_id";
    String PARAM_TOTAL = "total";
    String PARAM_DESTINATION_ROLE = "destination_role";

    // Register and Login
    String PARAM_ID_USER    = "id";
    String PARAM_USERNAME   = "username";
    String PARAM_PASSWORD   = "password";
    String PARAM_NAME_RG    = "name";
    String PARAM_PHONE      = "phone";
    String PARAM_ADDRESS    = "address";
    String PARAM_SAVELOGIN  = "savelogin";
    String PARAM_AVATAR     = "avatar";
    String PARAM_EMAIL      = "email";

    //Detail Order
    String PARAM_TOKEN      = "token";
    String PARAM_STATUS     = "status";

    //fcm
    String PARAM_GCM        = "gcm_id";
    String PARAM_IMEI       = "ime";
    String PARAM_TYPE       = "type";

    String PARAM_KEYWORD    = "keyword";
    String PARAM_SELLLER_ID = "seller_id";

    //Notifi
    String PARAM_PAGE               = "page";
    String PARAM_NUMBER_PER_PAGE    = "number_per_page";
    String PARAM_IMAGE              = "image";

    //Code
    String PARAM_CODE       = "code";

}
