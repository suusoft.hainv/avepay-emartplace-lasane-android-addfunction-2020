package com.aveplus.avepay_cpocket.taxi.view.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseFragment;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.UserObj;
import com.aveplus.avepay_cpocket.taxi.utils.DateTimeUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.view.activities.MainActivity;


public class BonusFragment extends BaseFragment {


    private TextView tvBonus, tvNotif;

    public static BonusFragment newInstance() {

        Bundle args = new Bundle();

        BonusFragment fragment = new BonusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_bonus;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {

        tvBonus = view.findViewById(R.id.tv_credits);
        tvNotif = view.findViewById(R.id.tv_notif);

        getProfile();

    }

    @Override
    protected void getData() {

    }


    private void getProfile() {
        ((MainActivity) self).showProgress(true);
        RequestManager.getProfile(self, new BaseRequest.CompleteListener() {

            @Override
            public void onSuccess(ApiResponse response) {
                ((MainActivity) self).showProgress(false);
                UserObj userObj = response.getDataObject(UserObj.class);
                UserObj user = DataStoreManager.getUser();
                user.setBonus(userObj.getBonus());
                DataStoreManager.saveUser(user);

                tvBonus.setText(StringUtil.convertNumberPrice(userObj.getBonus()));
                tvNotif.setText(String.format(getString(R.string.you_have_accumulated_at_the_date),
                        DateTimeUtil.convertTimeStringStampToDate(user.getCreated_date(), DateTimeUtil.DATE_BONUS)));
            }

            @Override
            public void onError(String message) {
                ((MainActivity) self).showProgress(false);
            }
        });
    }
}
