package com.aveplus.avepay_cpocket.restaurant.main.notification.object;
/**
 * Created by suusoft.com on 11/12/19.
 */
import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Content extends BaseModel {
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("content")
    @Expose
    private String conten;



    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getConten() {
        return conten;
    }

    public void setConten(String conten) {
        this.conten = conten;
    }
}
