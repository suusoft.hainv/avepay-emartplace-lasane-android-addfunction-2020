package com.aveplus.avepay_cpocket.restaurant.base.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.listener.IOnFragmentNavListener;


/**
 * Created by suusoft.com on 1/22/18.
 */

public abstract class BaseFragmentNav extends Fragment implements View.OnClickListener {

    protected FrameLayout contentLayout;
    protected FrameLayout contentLayout1;
    protected ProgressBar progressBar;
    // toolbar
    protected ImageButton btnBack, btnAction;
    protected TextView tvTitle;

    protected Context self;

    protected Animation animSlideIn, animSlideOut;

    protected abstract int getLayoutInflate();
    protected abstract void init();
    protected abstract void initView(View view);
    protected abstract void getData();

    private IOnFragmentNavListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self = getActivity();
        init();
        initAnimation();
    }

    private void initAnimation() {
        animSlideIn = AnimationUtils.loadAnimation(self, R.anim.slide_in_left);
        animSlideOut = AnimationUtils.loadAnimation(self, R.anim.slide_out_left);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutInflate(),container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initViewBase(view);
        initView(view);
    }


    private void initViewBase(View view){
        contentLayout = (FrameLayout) view.findViewById(R.id.content);
        contentLayout1 = (FrameLayout) view.findViewById(R.id.content1);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        btnAction = (ImageButton) view.findViewById(R.id.c_btn_action);
        btnBack = (ImageButton) view.findViewById(R.id.c_btn_back);
        tvTitle = (TextView) view.findViewById(R.id.c_tv_title);

        btnBack.setOnClickListener(this);
        btnAction.setOnClickListener(this);
    }

    public IOnFragmentNavListener getListener() {
        return listener;
    }

    public void setListener(IOnFragmentNavListener listener) {
        this.listener = listener;
    }


    protected void showProgress(boolean isShowing){
        if (isShowing){
            progressBar.setVisibility(View.VISIBLE);
        }else {
            progressBar.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View view) {
        if (view == btnBack){
            if (listener != null){
                listener.onFragmentBack();
            }
        }else if (view == btnAction){


        }
    }

    public TextView getTitle() {
        return tvTitle;
    }

    public void setTitle(String title) {
        this.tvTitle.setText(title);
    }

    public void setTitle(int title) {
        this.tvTitle.setText(getString(title));
    }


}
