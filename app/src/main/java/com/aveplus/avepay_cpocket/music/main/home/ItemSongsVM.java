package com.aveplus.avepay_cpocket.music.main.home;

import android.app.Dialog;
import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.download.DownloadSongManager;
import com.aveplus.avepay_cpocket.music.listener.IOnClickListener;
import com.aveplus.avepay_cpocket.music.listener.IOnMenuItemClick;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.main.addlist.AddListAdapter;
import com.aveplus.avepay_cpocket.music.main.addlist.AddListObj;
import com.aveplus.avepay_cpocket.music.main.addlist.Myplaylist;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.util.AdsUtil;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ItemSongsVM extends BaseAdapterVM implements IOnClickListener {
    public static final String TAG = "ItemSongsVM";
    private static int count = 0;
    private Song song;

    public ItemSongsVM(Context self, Object song, int position) {
        super(self, position);
        this.song = (Song) song;
    }

    public String getName() {
        return song.title;
    }

    public String getImage() {
        return song.image;
    }

    public String getArtistName() {
        return song.getNameSinger();
    }

    public String getDescription() {
        return song.description;
    }

    public String getTitle() {
        return song.title;
    }

    public String getNum() {
        return position + 1 + "";
    }

    @Override
    public void setData(Object object) {
        song = (Song) object;
        notifyChange();
    }

    @Override
    public void onClick(View view) {
        ((ItemMenuActionSongListener) listener).onClickWatch(position);
        //  AdsUtil.checkShowAds(self);
        // checkShowAds(self);

    }

    public void onClickAction(View view) {
        showPopupMenu(view, R.menu.menu_action_song);
    }

    public void showPopupMenu(View view, int menu_action_song) {
        Global.showPopupMenu(self, view, menu_action_song, new IOnMenuItemClick() {
            @Override
            public void onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_add_to_favorite:
                        actionFavorite();
                        break;

                    case R.id.action_add_to_queue:
                        actionAddToQueue();
                        break;
                    case R.id.action_add_to_mylist:
                        //  Toast.makeText(self, "ADD TO LIST!", Toast.LENGTH_SHORT).show();

                        showDialogAddList();

                        break;
                    case R.id.action_share:
                        actionShare();
                        break;

                    case R.id.action_download:
                        actionDownload();
                        break;

                    case R.id.action_remove:

                        ((ItemMenuActionSongListener) listener).onClickDelete(position);

                        break;
                }
            }
        });
    }

    protected void actionAddToQueue() {
        QueueManager.getInstance().addToQueue(song);
        AppUtil.showToast(self, R.string.added_to_queue);
    }

    public void actionDownload() {
        DialogUtil.showAlertDialog(self, R.string.confirm_download, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                DownloadSongManager.getInstance(self).download(song);
            }

            @Override
            public void onClickCancel() {

            }
        });
    }

    public void actionShare() {
        if (song != null) {
            AppUtil.shareMusic(self, song.title);
        }
    }

    public void actionFavorite() {
        if (!DataStoreManager.isFavorited(song.id)) {
            DataStoreManager.saveFavorited(song);
            AppUtil.showToast(self, R.string.favorited);
        } else {
            DataStoreManager.unFavorite(song.id);
            AppUtil.showToast(self, R.string.unfavorite);
        }
    }

    public static void checkShowAds(Context context) {
        count = count + 1;

        if (count == 5) {
            AdsUtil.loadInterstitial(context);
            count = 0;
        }
//        else if (count == 10) {
//            AdsUtil.loadInterstitial(context);
//            count = 5;
//
//        }
    }

    private void showDialogAddList() {
        final Dialog dialog = DialogUtil.setDialogCustomView(self, R.layout.dialog_add_list, true);
        TextView tvCreate = dialog.findViewById(R.id.tv_createnewplaylist);
        RecyclerView recyclerView = dialog.findViewById(R.id.rcv_data);
        ArrayList<AddListObj> listObjs;
        if (DataStoreManager.getMyPlaylist() == null) {
            listObjs = new ArrayList<>();
        } else {
            listObjs = DataStoreManager.getMyPlaylist().getListObjs();
        }
        AddListAdapter adapter = new AddListAdapter(self, listObjs);
        recyclerView.setLayoutManager(new LinearLayoutManager(self, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        tvCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogCreateList();
                dialog.dismiss();
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void showDialogCreateList() {
        final Dialog dialog = DialogUtil.setDialogCustomView(self, R.layout.dialog_create_newlist, false);
        EditText edtName = dialog.findViewById(R.id.edt_newname);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
        TextView tvOk = dialog.findViewById(R.id.tv_ok);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtName.getText().toString() != null || !"".equals(edtName.getText().toString())) {
                    Toast.makeText(self, "" + edtName.getText(), Toast.LENGTH_SHORT).show();
                    ArrayList<Song> list = new ArrayList<>();
                    list.add(QueueManager.getInstance().getCurrentItem());
                    AddListObj addListObj = new AddListObj(edtName.getText().toString(), list);
                    Myplaylist myplaylist;
                    ArrayList<AddListObj> listObjs = new ArrayList<>();
                    listObjs.add(addListObj);

                    if (DataStoreManager.getMyPlaylist() == null) {
                        myplaylist = new Myplaylist(listObjs);
                    } else {
                        myplaylist = DataStoreManager.getMyPlaylist();
                        myplaylist.getListObjs().add(addListObj);
                    }
                    //Log.e(TAG, "onClick: " + new Gson().toJson(DataStoreManager.getMyPlaylist()));
                    DataStoreManager.saveMyPlaylist(myplaylist);
                    //Log.e(TAG, "onClick: " + new Gson().toJson(DataStoreManager.getMyPlaylist()));
                    dialog.dismiss();
                } else {
                    Toast.makeText(self, "New name not blank!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
