package com.aveplus.avepay_cpocket.ebook.view.adapter;

import android.content.Context;
import android.view.ViewGroup;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.ItemActorBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseAdapter;
import com.aveplus.avepay_cpocket.ebook.model.Actor;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemActorVM;

import java.util.List;


public class ActorAdapter extends BaseAdapter {

    private List<Actor> listData;


    public ActorAdapter(Context context, List<?> datas) {
        super(context);
        this.listData = (List<Actor>) datas;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ItemActorBinding binding = (ItemActorBinding) getViewBinding(parent, R.layout.item_actor);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        ((ViewHolder) holder).bindViewModel(listData.get(position));
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    @Override
    public void setDatas(List<?> data) {
        int size = listData.size();
        listData.addAll((List<Actor>) data);
        notifyItemRangeInserted(size, listData.size());
    }


    class ViewHolder extends BaseViewHolder {
        private ItemActorBinding binding;

        public ViewHolder(ItemActorBinding binding) {
            super(binding);
            this.binding = binding;
        }


        public void bindViewModel(Actor actor) {

            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemActorVM(context, actor));
            } else {
                binding.getViewModel().setData(actor);
            }
        }
    }

}
