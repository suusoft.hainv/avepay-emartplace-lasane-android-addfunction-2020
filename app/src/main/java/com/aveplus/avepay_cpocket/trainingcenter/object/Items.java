package com.aveplus.avepay_cpocket.trainingcenter.object;

import java.util.ArrayList;

/**
 * Created by SuuSoft on 05/09/2017.
 */

public class Items {
    private ArrayList<Pro> data;

    public ArrayList<Pro> getData() {
        return data;
    }

    public void setData(ArrayList<Pro> data) {
        this.data = data;
    }
}
