package com.aveplus.avepay_cpocket.music.main.category;

import android.content.Context;
import android.util.Log;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.model.Category;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class AllCategoryVM extends BaseViewModelList {


    public AllCategoryVM(Context context) {
        super(context);
        getData(1);
    }

    @Override
    public void getData(int page) {
        RequestManager.getAllCategories(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                ArrayList<Category> list = (ArrayList<Category>) response.getDataList(Category.class);
                addListData(list);
                checkLoadingMoreComplete(1);
            }

            @Override
            public void onError(String message) {
                Log.d("MusicByCategoryVM", "can not get video detail");
            }
        });
    }
}
