package com.aveplus.avepay_cpocket.movie.movie.main.impl;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface onFocusRowAdapter {
    public void onFocusAdapter(int position, boolean hasfocus);
}
