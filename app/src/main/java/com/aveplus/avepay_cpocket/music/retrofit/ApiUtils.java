package com.aveplus.avepay_cpocket.music.retrofit;


import com.aveplus.avepay_cpocket.music.configs.Config;

public class ApiUtils {

    public static final String BASE_URL = Config.URL_API;

    //public static final String BASE_URL123 = AppController.getInstance().getString(R.string.URL_SMS123);




    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }


//    public static ApiSMS123 getApiSMS123() {
//
//        return RetrofitClient.getClient123(BASE_URL123).create(ApiSMS123.class);
//    }


}
