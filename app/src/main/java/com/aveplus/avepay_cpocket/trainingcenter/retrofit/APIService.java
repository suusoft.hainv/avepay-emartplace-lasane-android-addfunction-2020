package com.aveplus.avepay_cpocket.trainingcenter.retrofit;

import com.aveplus.avepay_cpocket.music.retrofit.param.Param;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseUser;
import com.aveplus.avepay_cpocket.trainingcenter.retrofit.response.ResponseBlogRecent;
import com.aveplus.avepay_cpocket.trainingcenter.retrofit.response.ResponseListFaq;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    @GET("service")
    @Headers("Cache-Control: no-cache")
    Call<ResponseListFaq> List_Faq(
            @Query(Param.PARAM_PAGE) String page

    );

    @GET("news")
    @Headers("Cache-Control: no-cache")
    Call<ResponseBlogRecent> List_Blog_Recent(
            @Query(Param.PARAM_PAGE) String page,
            @Query(Param.PARAM_TYPE) String type
    );

    @POST("user/login")
    @Headers("Cache-Control: no-cache")
    Call<ResponseUser> login(@Query(Param.PARAM_USERNAME) String username,
                             @Query(Param.PARAM_PASSWORD) String password,
                             @Query(Param.PARAM_LOGIN_TYPE) String login_type,
                             @Query(Param.PARAM_NAME) String name
    );


}
