package com.aveplus.avepay_cpocket.movie.movie.main.impl;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface onClickMainSubCate {
    public void onClickItemSubcate(int subcatePosition);
    public void onNextPage();
}
