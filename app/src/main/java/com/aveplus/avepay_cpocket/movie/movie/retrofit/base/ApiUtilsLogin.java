package com.aveplus.avepay_cpocket.movie.movie.retrofit.base;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

public class ApiUtilsLogin {
    public static final String BASE_URL = AppController.getInstance().getString(R.string.URL_API_MOVIE) + "backend/web/index.php/api/";
//    public static final String BASE_URL = Config.URL_API;

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
