package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.music.configs.Args;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.activity.BlogDetailActivity;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.AdapterBlogRecent;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.NewEventAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.aveplus.avepay_cpocket.trainingcenter.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.trainingcenter.retrofit.response.ResponseBlogRecent;
import com.aveplus.avepay_cpocket.trainingcenter.view.IOnClickBlogRecent;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshBase;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshListView;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentBlogRecent extends BaseFragment {

    private ArrayList<NewEvent> arraylist = new ArrayList<NewEvent>();
    RecyclerView rcvRecent;
    AdapterBlogRecent adapterBlogRecent;
    private NewEventAdapter adapter = null;
    private ListView listview;
    //    private PullToRefreshListView lGv;
    private int currentPage, totalPage;
    private boolean isPullRefresh = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (arraylist == null) {
            arraylist = new ArrayList<>();
        } else {
            arraylist.clear();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blog_recent, container, false);
        initView(view);
        refreshGridView(true);
        return view;
    }

    public void initView(View view) {
        rcvRecent = view.findViewById(R.id.rcv_recent);
        loadData();
//        lGv = (PullToRefreshListView) view.findViewById(R.id.lvNewEvent);
//        listview = lGv.getRefreshableView();
//        setControl();
    }

    private void loadData() {
        dataList();

        adapterBlogRecent = new AdapterBlogRecent(arraylist, context, new IOnClickBlogRecent() {
            @Override
            public void OnClick(int position, NewEvent productObj) {
                NewEvent item = arraylist.get(position);
                Bundle bundle = new Bundle();
                bundle.putString("id", item.getId());
                bundle.putString("title", item.getTitle());
                bundle.putString("description", item.getDescription());
                bundle.putString("content", item.getContent());
                bundle.putString("image", item.getImage());
                bundle.putString("time", item.getTime());
                bundle.putString("link", item.getLinkNews());
                Intent intent = new Intent(getContext(), BlogDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }
        });
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(self, LinearLayoutManager.VERTICAL, false);
        rcvRecent.setAdapter(adapterBlogRecent);
        rcvRecent.setLayoutManager(layoutManager);
    }

    private void dataList() {
        ApiUtils.getAPIService().List_Blog_Recent("1", "recent").enqueue(new Callback<ResponseBlogRecent>() {
            @Override
            public void onResponse(Call<ResponseBlogRecent> call, Response<ResponseBlogRecent> response) {
                if (response.body().getData() != null) {
                    arraylist.addAll(response.body().getData());
                    adapterBlogRecent.addList(arraylist);
                    adapterBlogRecent.notifyDataSetChanged();

                }else {
                    Toast.makeText(self, "not data1", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBlogRecent> call, Throwable t) {

            }
        });
    }

//    private void initListView() {
//        adapter = new NewEventAdapter(getActivity(), R.layout.item_product, arraylist);
//        listview.setAdapter(adapter);
//        listview.setOnItemClickListener(new OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
////                int i = position - 1;
////                NewEvent item = arraylist.get(i);
////                Bundle bundle = new Bundle();
////                bundle.putString("id", item.getId());
////                bundle.putString("title", item.getTitle());
////                bundle.putString("description", item.getDescription());
////                bundle.putString("content", item.getContent());
////                bundle.putString("image", item.getImage());
////                bundle.putString("time", item.getTime());
////                bundle.putString("link", item.getLinkNews());
////                startActivity(bundle,BlogDetailActivity.class);
//
//            }
//        });
//    }

//    private void setControl() {
//        initListView();
//        lGv.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
//            @Override
//            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
//                String label = DateUtils.formatDateTime(getActivity(),
//                        System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
//                                | DateUtils.FORMAT_SHOW_DATE
//                                | DateUtils.FORMAT_ABBREV_ALL);
//                refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
//                refreshGridView(true);
//            }
//
//            @Override
//            public void onPullUpToRefresh(
//                    PullToRefreshBase<ListView> refreshView) {
//                if (currentPage < totalPage) {
//                    loadMorePage();
//                } else {
//                    showNoMoreData();
//                }
//            }
//        });
//    }


    private void refreshGridView(boolean isPull) {
        currentPage = 1;
        getAllNews(currentPage, isPull);
    }

    private void loadMorePage() {
        currentPage++;
        getAllNews(currentPage, isPullRefresh);
    }

    private void getAllNews(final int currentPage2, boolean isPull) {
        ModelManager.getListNewsRecent(currentPage2, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                if (currentPage2 < 2) {
                    arraylist.clear();
                }
                arraylist.addAll(ParseUtility.parseListNews(response.getRootStr()));
                totalPage = ParseUtility.getAllPage(response.getRootStr());
//                adapter.notifyDataSetChanged();
                closeLoadingPull();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

    private void showNoMoreData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showToast(R.string.endPage);
//                lGv.onRefreshComplete();
            }
        }, 1000);
    }

    protected void showToast(int idString) {
        Toast.makeText(getActivity(), idString, Toast.LENGTH_SHORT).show();
    }

    private void closeLoadingPull() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
//                lGv.onRefreshComplete();
            }
        }, 1000);
    }
}
