package com.aveplus.avepay_cpocket.restaurant.main.home.Banner;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.PagerAdapter;

import com.aveplus.avepay_cpocket.restaurant.util.ImageUtil;

import java.util.List;

public class AdapterHomeBanner extends PagerAdapter {
    private Context context;
    private List<BannerModel> banner;

    public AdapterHomeBanner(Context context, List<BannerModel> banner) {
        this.context = context;
        this.banner = banner;
    }

    @Override
    public int getCount() {
        return banner.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final AppCompatImageView imageView = new AppCompatImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        container.addView(imageView);
        // Load ảnh vào ImageView
        ImageUtil.setImage(imageView, banner.get(position).getImage());
//        Glide.with(context).load(banner.get(position).getImage()).into(imageView);
        // Return
        return imageView;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        // container chính là ViewPager, còn Object chính là return của instantiateItem ứng với position
        container.removeView((View) object);
    }
}
