package com.aveplus.avepay_cpocket.quickblox;

public interface QbSessionStateCallback {

    void onSessionCreated(boolean success);
}
