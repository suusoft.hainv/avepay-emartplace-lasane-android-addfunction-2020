package com.aveplus.avepay_cpocket.view.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aveplus.avepay_cpocket.quickblox.conversation.utils.DialogUtil;
import com.aveplus.avepay_cpocket.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.retrofit.response.ResponseUser;
import com.aveplus.avepay_cpocket.utils.FileUtil;
import com.aveplus.avepay_cpocket.widgets.edittext.CustomEditText;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.view.activities.ChangePassWordActivity;
import com.aveplus.avepay_cpocket.view.activities.MainActivity;
import com.aveplus.avepay_cpocket.view.activities.PhoneCountryListActivity;
import com.aveplus.avepay_cpocket.view.activities.ViewReviewsActivity;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.interfaces.IObserver;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.objects.DataPart;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.aveplus.avepay_cpocket.utils.StringUtil;
import com.aveplus.avepay_cpocket.utils.map.MapsUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.aveplus.avepay_cpocket.retrofit.param.Param.PARAM_AVATAR;
import static com.aveplus.avepay_cpocket.utils.ImageUtil.PICK_IMAGE_REQUEST_CODE;

/**
 * Created by Suusoft on 01/12/2016.
 */

public class MyAccountMyInfoFragment extends BaseFragment implements View.OnClickListener, IObserver {
    private static final String TAG = MyAccountMyInfoFragment.class.getName();
    private static final int RC_ADDRESS = 134;
    private static final int RQ_CHANGE_PASS = 234;
    private EditText edtBusinessName, edtPhoneNumber, edtAddress, edtEmail;
    private CustomEditText edtGender;
    private CircleImageView imgAvatar;
    private ImageView imgEditAvatar;
    private ImageView imgSymbolAccount;
    private FrameLayout frDivider;
    private TextViewRegular btnEdit;
    private TextViewRegular tvChangePassword, tvNumRate, btnViewReviews;
    private TextView tvPhoneCode;
    private RelativeLayout btnSave;
    private TextView tvFunction;
    private RatingBar rating_bar;
    private boolean isEdit = false;
    private RelativeLayout rltGender;
    private Dialog dialogGender;
    private RadioButton rbFemale, rbMale;
    private String pathImage = "";
    private MultipartBody.Part partImage1 = null;
    private RequestBody RQtoken, RQname, RQgender, RQphone, RQaddress;
    private File file1;
    private ProgressBar progressBar;

    private View.OnFocusChangeListener listenerFocusAddress = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean isFocus) {
            if (isFocus) {
                MapsUtil.getAutoCompletePlaces(MyAccountMyInfoFragment.this, RC_ADDRESS);
            }
        }
    };

    public static MyAccountMyInfoFragment newInstance() {
        Bundle args = new Bundle();
        MyAccountMyInfoFragment fragment = new MyAccountMyInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected View inflateLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account_about, container, false);
//        setData(DataStoreManager.getUser());

    }

    @Override
    protected void initUI(View view) {
        imgAvatar = (CircleImageView) view.findViewById(R.id.img_avatar);
        imgEditAvatar = (ImageView) view.findViewById(R.id.btn_edit_avatar);
        imgSymbolAccount = (ImageView) view.findViewById(R.id.img_symbol_account);
        tvNumRate = (TextViewRegular) view.findViewById(R.id.tv_num_rate);
        rating_bar = (RatingBar) view.findViewById(R.id.rating_bar);
        btnViewReviews = (TextViewRegular) view.findViewById(R.id.btn_view_reviews);

        edtBusinessName = (EditText) view.findViewById(R.id.edt_bussiness_name);
        edtPhoneNumber = (EditText) view.findViewById(R.id.edt_phone_number);
        edtAddress = (EditText) view.findViewById(R.id.edt_address);
        edtEmail = (EditText) view.findViewById(R.id.edt_email);
        edtGender = (CustomEditText) view.findViewById(R.id.edt_gender);

        progressBar = view.findViewById(R.id.progress_bar);

        rltGender = view.findViewById(R.id.rlt_gender);


        btnEdit = (TextViewRegular) view.findViewById(R.id.btn_edit_infomation);
        tvChangePassword = (TextViewRegular) view.findViewById(R.id.tv_change_password);
        btnSave = (RelativeLayout) view.findViewById(R.id.btn_functions);
        tvFunction = (TextView) view.findViewById(R.id.tv_btn);
        btnSave.setVisibility(View.GONE);
        tvFunction.setText(getString(R.string.button_save));

        frDivider = (FrameLayout) view.findViewById(R.id.fr_divider);

        tvPhoneCode = (TextView) view.findViewById(R.id.tv_phone_code);

        edtGender.setDrawableClickListener(clickListenerGenre);

//        getProfile();

        setTextProfile();
        setEnable();


    }

    private void setTextProfile() {
        if (DataStoreManager.getUser() != null) {
            ImageUtil.setImage(imgAvatar, DataStoreManager.getUser().getAvatar());

            edtEmail.setText(DataStoreManager.getUser().getEmail());
            edtBusinessName.setText(DataStoreManager.getUser().getName());

//            edtGender.setText(DataStoreManager.getUser().getGender());
            edtPhoneNumber.setText(DataStoreManager.getUser().getPhone());
            edtAddress.setText(DataStoreManager.getUser().getAddress());
            edtGender.setText(DataStoreManager.getUser().getGender());

        }
    }

    private void setEnable() {
        edtEmail.setEnabled(false);
        edtBusinessName.setEnabled(false);
        edtPhoneNumber.setEnabled(false);
        edtAddress.setEnabled(false);
        edtGender.setEnabled(false);
    }

    private CustomEditText.DrawableClickListener clickListenerGenre = new CustomEditText.DrawableClickListener() {
        @Override
        public void onClick(DrawablePosition target) {
            switch (target) {
                case RIGHT:
                    //Do something here
                    Log.e(TAG, "vào không ? ");
                    processingShowDialogGender();

                    break;

                default:
                    break;
            }
        }
    };

    private void processingShowDialogGender() {
        Log.e(TAG, "processingShowDialogGender: ");
//        if (dialogGender == null) {
        Log.e(TAG, "oncrete: ");
        Context context;
        dialogGender = new Dialog(self);
        dialogGender.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogGender.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogGender.setContentView(R.layout.dialog_choose_gender);
//
//            dialogGender = DialogUtil.setDialogCustomView(self, R.layout.dialog_choose_gender, false);
        rbMale = (RadioButton) dialogGender.findViewById(R.id.rb_male);
        rbFemale = (RadioButton) dialogGender.findViewById(R.id.rb_female);
        dialogGender.show();
//        }


        if (edtGender.getText().equals(getResources().getString(R.string.male))) {
            rbMale.setChecked(true);
        } else if (edtGender.getText().equals(getResources().getString(R.string.female)))

            dialogGender.show();
        rbMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "male ");
                edtGender.setText(getResources().getString(R.string.male));
                dialogGender.dismiss();

            }
        });

        rbFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "male ");
                edtGender.setText(getResources().getString(R.string.female));
                dialogGender.dismiss();

            }
        });
    }

    @Override
    protected void initControl() {
        enableEdit(false);

        imgEditAvatar.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
//        tvChangePassword.setOnClickListener(this);
//        btnViewReviews.setOnClickListener(this);
        btnSave.setOnClickListener(this);
//        tvPhoneCode.setOnClickListener(this);
//        edtAddress.setTag(edtAddress.getKeyListener());
//        edtAddress.setKeyListener(null);
//        edtAddress.setOnClickListener(this);
//        edtAddress.setOnFocusChangeListener(listenerFocusAddress);

        frDivider.setVisibility(View.VISIBLE);


//        setData(DataStoreManager.getUser());
//        edtEmail.setEnabled(false);
//        edtBusinessName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//                if (i == EditorInfo.IME_ACTION_NEXT) {
//                    edtEmail.setFocusable(false);
//                    edtPhoneNumber.requestFocus();
//                }
//                return false;
//            }
//        });
//        if (com.aveplus.avepay_cpocket.network1.NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
//            getProfile();
//        } else {
//            AppUtil.showToast(getActivity(), R.string.msg_network_not_available);
//        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnEdit) {
            isEdit = !isEdit;
            if (isEdit) {
//                if (DataStoreManager.getUser().getAvatar().startsWith("http://iwanadeal.com") || DataStoreManager.getUser().getAvatar().isEmpty()) {
                imgEditAvatar.setVisibility(View.VISIBLE);
//
//                } else {
//                    imgEditAvatar.setVisibility(View.GONE);
//
//                }
//                edtAddress.setOnFocusChangeListener(listenerFocusAddress);
                btnSave.setVisibility(View.VISIBLE);
                btnEdit.setBackgroundResource(R.drawable.bg_pressed_radius_accent);
                edtBusinessName.requestFocus();
            } else {
//                setData(DataStoreManager.getUser());
                imgEditAvatar.setVisibility(View.GONE);
                btnSave.setVisibility(View.GONE);
                btnEdit.setBackgroundResource(R.drawable.bg_pressed_radius_grey);
                edtAddress.setOnFocusChangeListener(null);
            }
            enableEdit(isEdit);

        } else if (view == btnSave) {
            if (com.aveplus.avepay_cpocket.network1.NetworkUtility.getInstance(getActivity()).isNetworkAvailable()) {
                if (isEdit) {
                    if (isValid()) {
//                        updateProfile();
                        updateProfileNew();
                        progressBar.setVisibility(View.VISIBLE);
                    }
                } else {
                    AppUtil.showToast(getActivity(), R.string.msg_enable_edit);
                }

            } else {
                AppUtil.showToast(getActivity(), R.string.msg_no_network);
            }

        } else if (view == tvChangePassword) {
            Intent intent = new Intent(getActivity(), ChangePassWordActivity.class);
            startActivityForResult(intent, RQ_CHANGE_PASS);
        } else if (view == imgEditAvatar) {
            AppUtil.pickImage(this, AppUtil.PICK_IMAGE_REQUEST_CODE);
        } else if (view == btnViewReviews) {
//            Bundle bundle = new Bundle();
//            bundle.putString(Args.USER_ID, "");
//            bundle.putString(Args.NAME_DRIVER,"");
//            bundle.putString(Args.I_AM, Constants.SELLER);
//            Intent intent = new Intent(getApplicationContext(), ViewReviewsActivity.class);
//            intent.putExtras(bundle);
            AppUtil.startActivity(getActivity(), ViewReviewsActivity.class);
        } else if (view == tvPhoneCode) {
            Intent intent = new Intent(getActivity(), PhoneCountryListActivity.class);
            startActivityForResult(intent, Args.RQ_GET_PHONE_CODE);
        } else if (view == edtAddress) {
//            MapsUtil.getAutoCompletePlaces(this, RC_ADDRESS);
        }
    }

    private void updateProfileNew() {
        part();
        profile();


        imgEditAvatar.setVisibility(View.GONE);
        btnSave.setVisibility(View.GONE);
        btnEdit.setBackgroundResource(R.drawable.bg_pressed_radius_grey);

        edtBusinessName.setEnabled(false);
        edtGender.setEnabled(false);
        edtPhoneNumber.setEnabled(false);
        edtAddress.setEnabled(false);

    }

    private void profile() {
        ApiUtils.getAPIService().update_profile(RQtoken, RQname, RQgender, RQphone, RQaddress, partImage1).enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                if (response.body().isSuccess(self)) {
                    if (response.body() != null) {

                        upDateProFileSuccess();
                        progressBar.setVisibility(View.GONE);

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void upDateProFileSuccess() {
        ApiUtils.getAPIService().profile(DataStoreManager.getToken()).enqueue(new Callback<ResponseUser>() {
            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {

                UserObj user = response.body().getData();
                DataStoreManager.saveUser(user);
                ImageUtil.setImage(imgAvatar, DataStoreManager.getUser().getAvatar());
                edtBusinessName.setText(DataStoreManager.getUser().getName());
                edtGender.setText(DataStoreManager.getUser().getGender());
                edtPhoneNumber.setText(DataStoreManager.getUser().getPhone());
                edtAddress.setText(DataStoreManager.getUser().getAddress());

                AppUtil.showToast(self, "Update profile success");


            }

            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {

            }
        });

    }

    private void part() {
        parseMultipart();

        String token = DataStoreManager.getToken();
        String name = edtBusinessName.getText().toString();
        String gender = edtGender.getText().toString();
        String phone = edtPhoneNumber.getText().toString();
        String address = edtAddress.getText().toString();

        RQtoken = RequestBody.create(MediaType.parse("text/plain"), token);
        RQname = RequestBody.create(MediaType.parse("text/plain"), name);
        RQgender = RequestBody.create(MediaType.parse("text/plain"), gender);
        RQphone = RequestBody.create(MediaType.parse("text/plain"), phone);
        RQaddress = RequestBody.create(MediaType.parse("text/plain"), address);
    }

    private void parseMultipart() {
        if (!pathImage.isEmpty()) {
            Log.e(TAG, "parseMultipart: " + pathImage);
            File fileImage = new File(pathImage);
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), fileImage);
            partImage1 = MultipartBody.Part.createFormData(PARAM_AVATAR, fileImage.getName(), fileReqBody);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("resultCode", "resultCode = " + resultCode);

        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {

            ImageUtil.setImageFromUri(imgAvatar, data.getData());
            pathImage = FileUtil.getPath(self, data.getData()).trim();
            Log.e(TAG, "onActivityResult: " + pathImage);
            file1 = new File(pathImage);


        }

        if (requestCode == AppUtil.PICK_IMAGE_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                AppUtil.setImageFromUri(imgAvatar, data.getData());
            }

        } else if (requestCode == Args.RQ_GET_PHONE_CODE && resultCode == RESULT_OK) {
            String countryCodeSelected = data.getExtras().getString(Args.KEY_PHONE_CODE);
            tvPhoneCode.setText(countryCodeSelected);
        }
        if (requestCode == RC_ADDRESS) {
            if (resultCode == -1) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                AppUtil.fillAddress(getActivity(), edtAddress, place);
                edtPhoneNumber.requestFocus();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(self, data);
                Log.e("DealNewFragment", status.getStatusMessage());
                edtPhoneNumber.requestFocus();
            } else if (resultCode == 0) {
                edtPhoneNumber.requestFocus();
                // The user canceled the operation.
            }
        }
        if (requestCode == RQ_CHANGE_PASS) {
            if (resultCode == ChangePassWordActivity.RC_CREATE_PASS) {
                tvChangePassword.setText(getString(R.string.change_pass));
            }
        }

    }

    private boolean isValid() {
        String bussinessName = edtBusinessName.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        String phone = edtPhoneNumber.getText().toString().trim();
        if (StringUtil.isEmpty(bussinessName)) {
            AppUtil.showToast(getActivity(), R.string.msg_fill_full_name);
            edtBusinessName.requestFocus();
            return false;
        }
        if (StringUtil.isEmpty(phone)) {
            AppUtil.showToast(getActivity(), R.string.msg_phone_is_required);
            edtPhoneNumber.requestFocus();
            return false;
        }
        if (StringUtil.isEmpty(address)) {
            AppUtil.showToast(getActivity(), R.string.msg_address_is_required);
            edtAddress.requestFocus();
            return false;
        }
        return true;
    }

    private void enableEdit(boolean isEdit) {
        edtBusinessName.setEnabled(isEdit);
        edtAddress.setEnabled(isEdit);
        edtPhoneNumber.setEnabled(isEdit);
        tvPhoneCode.setEnabled(isEdit);
    }

//    private void setData(UserObj userObj) {
//        if (userObj.getProData() == null) {
//            imgSymbolAccount.setVisibility(View.VISIBLE);
//            imgSymbolAccount.setImageResource(R.drawable.ic_member);
////            tvNumRate.setText(String.valueOf(userObj.getRate_count()));
////            rating_bar.setRating(userObj.getRate());
//        } else {
//            imgSymbolAccount.setVisibility(View.VISIBLE);
//            imgSymbolAccount.setImageResource(R.drawable.ic_pro_member);
////            tvNumRate.setText(String.valueOf(userObj.getProData().getRateCount()));
////            rating_bar.setRating(userObj.getProData().getRate());
//        }
//
//        tvNumRate.setText(String.valueOf(userObj.getTotal_rate_count()));
//        rating_bar.setRating(userObj.getAvg_rate());
//        ImageUtil.setImage(getActivity(), imgAvatar, userObj.getAvatar(), 600, 600);
//        edtBusinessName.setText(userObj.getName());
//        edtPhoneNumber.setText(userObj.getPhoneNumber());
//        if (userObj.getPhoneCode().isEmpty()) {
//            getCountryCode();
//        } else {
//            tvPhoneCode.setText(userObj.getPhoneCode());
//        }
//        edtAddress.setText(userObj.getAddress());
//        edtEmail.setText(userObj.getEmail());
//        if (userObj.isSecured()) {
//            tvChangePassword.setText(getString(R.string.change_pass));
//        } else {
//            tvChangePassword.setText(getString(R.string.create_pass));
//        }
//    }

//    private void getCountryCode() {
//        String[] rl = getResources().getStringArray(R.array.CountryCodes);
//        int curPosition = AppUtil.getCurentPositionCountryCode(getActivity());
//        String phoneCode = rl[curPosition].split(",")[0];
//        tvPhoneCode.setText(phoneCode);
//    }

    private void updateProfile() {
        String bussinessName = edtBusinessName.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        String phoneNumber = edtPhoneNumber.getText().toString().trim();
        String phoneCode = tvPhoneCode.getText().toString().trim();
        String phone = phoneCode + " " + phoneNumber;
        DataPart avatar = null;
        if (imgAvatar.getDrawable() != null) {
            avatar = new DataPart("avatar.png", AppUtil.getFileDataFromDrawable(getActivity(), imgAvatar.getDrawable()), DataPart.TYPE_IMAGE);
        }

        ModelManager.updateProfileNormal(getActivity(), avatar, bussinessName, phone, address, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                try {
                    JSONObject jsonObject = new JSONObject(object.toString());
                    ApiResponse response = new ApiResponse(jsonObject);
                    if (!response.isError()) {
                        UserObj userObj = response.getDataObject(UserObj.class);
                        userObj.setToken(DataStoreManager.getToken());
                        userObj.setRememberMe(DataStoreManager.getUser().isRememberMe());
                        DataStoreManager.saveUser(userObj);
                        AppController.getInstance().setUserUpdated(true);
                        enableEdit(false);
                        btnEdit.setBackgroundResource(R.drawable.bg_pressed_radius_grey);
                        isEdit = false;
                        btnSave.setVisibility(View.GONE);
                        imgEditAvatar.setVisibility(View.GONE);
                        AppUtil.showToast(getActivity(), R.string.msg_update_success);
//                        setData(userObj);
                        ((MainActivity) getActivity()).setTitle(userObj.getName());
                        ((MainActivity) getActivity()).updateMenuLeftHeader();
                    } else {
                        Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError() {
                Log.e(TAG, "ERROR: update profile!");
            }
        });
    }

    private void getProfile() {
        ModelManager.getProfile(getActivity(), DataStoreManager.getUser().getId(), new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                try {
                    JSONObject jsonObject = new JSONObject(object.toString());
                    ApiResponse response = new ApiResponse(jsonObject);
                    if (!response.isError()) {
                        UserObj userObj = response.getDataObject(UserObj.class);
                        userObj.setToken(DataStoreManager.getToken());
                        userObj.setRememberMe(DataStoreManager.getUser().isRememberMe());
                        DataStoreManager.saveUser(userObj);
                        AppController.getInstance().setUserUpdated(true);
//                        setData(userObj);
                    } else {
                        Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError() {

            }
        });
    }


    @Override
    public void update() {
//        setData(DataStoreManager.getUser());
    }
}
