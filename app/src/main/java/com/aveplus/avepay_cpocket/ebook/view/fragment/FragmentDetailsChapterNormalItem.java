package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.model.ChapterNomal;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;


/**
 * Created by Suusoft on 11/14/2017.
 */

public class FragmentDetailsChapterNormalItem extends Fragment {

    public static final String CHAPTER_NORMAL = "CHAPTER_NORMAL";
    private Context sefl;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.sefl = context;
    }


    public static Fragment newInstance(ChapterNomal chapterNomal) {
        FragmentDetailsChapterNormalItem fragment = new FragmentDetailsChapterNormalItem();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CHAPTER_NORMAL, chapterNomal);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details_chapter_normal_item, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            ChapterNomal chapterNomal = bundle.getParcelable(CHAPTER_NORMAL);
            ImageView imvAvatar = (ImageView) rootView.findViewById(R.id.imv_avatar);
            TextView tvChapter = (TextView) rootView.findViewById(R.id.tv_chapter);

            AppUtil.setImage(imvAvatar, chapterNomal.getImage());
            tvChapter.setText(chapterNomal.getContent());
        }
        return rootView;
    }


}
