package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.AdapterFaq;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ServicesAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.Services;
import com.aveplus.avepay_cpocket.trainingcenter.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.trainingcenter.retrofit.response.ResponseListFaq;
import com.aveplus.avepay_cpocket.trainingcenter.view.IOnClick;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshBase;
import com.aveplus.avepay_cpocket.trainingcenter.widget.pulltorefresh.PullToRefreshListView;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentFAQ extends BaseFragment {

    private ArrayList<Services> listService = new ArrayList<Services>();
    private ListView lstService;
    private ServicesAdapter mAdapter;
    private PullToRefreshListView lGv;
    private int currentPage, totalPage;
    private RecyclerView rcvFaq;
    private AdapterFaq adapterFaq;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("service");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container,
                false);
        if (listService == null) {
            listService = new ArrayList<>();
        } else {
            listService.clear();
        }

        initUI(view);
        initData();
        return view;
    }

    private void initData() {
//        loadList(0);
//        setControl();
        LoadData();
    }

    private void LoadData() {
        dataList();
        adapterFaq = new AdapterFaq(listService, context, new IOnClick() {
            @Override
            public void OnClick(int position, Services productObj) {
                int i = position ;
                DetailFAQ fragment = new DetailFAQ();
                Services item = listService.get(i);
                Bundle bundle = new Bundle();
                bundle.putParcelable("item", item);

                fragment.setArguments(bundle);

                getChildFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left,
                                R.anim.slide_out_right)
                        .replace(R.id.showService, fragment, "detailservice")
                        .addToBackStack(null).commit();
            }
        });

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(self, LinearLayoutManager.VERTICAL, false);
        rcvFaq.setAdapter(adapterFaq);
        rcvFaq.setLayoutManager(layoutManager);
    }

    private void dataList() {
        ApiUtils.getAPIService().List_Faq("1").enqueue(new Callback<ResponseListFaq>() {
            @Override
            public void onResponse(Call<ResponseListFaq> call, Response<ResponseListFaq> response) {
                if (response.body().getData() != null) {
                    listService.addAll(response.body().getData());
                    adapterFaq.addList(listService);
                    adapterFaq.notifyDataSetChanged();

                }else {
                    Toast.makeText(self, "not data1", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseListFaq> call, Throwable t) {
                Toast.makeText(self, "not data 2", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initUI(View view) {

        rcvFaq = view.findViewById(R.id.rcv);

//        lGv = (PullToRefreshListView) view.findViewById(R.id.rcv);
//        lstService = lGv.getRefreshableView();
//        setControl();

    }

//    private void setControl() {
//
//        lGv.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
//            @Override
//            public void onPullDownToRefresh(
//                    PullToRefreshBase<ListView> refreshView) {
//                String label = DateUtils.formatDateTime(getActivity(),
//                        System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
//                                | DateUtils.FORMAT_SHOW_DATE
//                                | DateUtils.FORMAT_ABBREV_ALL);
//                refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
//
//                getData(true);
//
//            }
//
//            @Override
//            public void onPullUpToRefresh(
//                    PullToRefreshBase<ListView> refreshView) {
//
//                getData(false);
//            }
//        });
//
//        lstService.setOnItemClickListener(new OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//                int i = position - 1;
//                DetailFAQ fragment = new DetailFAQ();
//                Services item = listService.get(i);
//                Bundle bundle = new Bundle();
//                bundle.putParcelable("item", item);
////                bundle.putString("id", item.getId());
////                bundle.putString("title", item.getTitle());
////                bundle.putString("description", item.getDescription());
////                bundle.putString("content", item.getContent());
////                bundle.putString("image", item.getImage());
//
//                fragment.setArguments(bundle);
//
//                getChildFragmentManager()
//                        .beginTransaction()
//                        .setCustomAnimations(R.anim.slide_in_left,
//                                R.anim.slide_out_right)
//                        .replace(R.id.showService, fragment, "detailservice")
//                        .addToBackStack(null).commit();
//
//            }
//        });
//    }

//    private void loadList(int i) {
//        ModelManager.getListService(currentPage, new BaseRequest.CompleteListener() {
//            @Override
//            public void onSuccess(ApiResponse response) {
//                listService = ParseUtility.parseListService(response.getRootStr());
//                mAdapter = new ServicesAdapter(getActivity(),
//                        listService);
//                mAdapter.notifyDataSetChanged();
//                lstService.setAdapter(mAdapter);
//                lGv.onRefreshComplete();
//            }
//
//            @Override
//            public void onError(String message) {
//                AppUtil.showToast(self, message);
//            }
//        });
//    }

    protected void getData(boolean isRefresh) {
        if (isRefresh) {
            listService.clear();
            currentPage = 1;
//            loadList(currentPage);
        } else {
            if (currentPage >= totalPage) {
                showNoMoreData();
            } else {
                currentPage++;
//                loadList(currentPage);
            }
        }
    }

    private void showNoMoreData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showToast(R.string.endPage);
                lGv.onRefreshComplete();
            }
        }, 100);
    }

    protected void showToast(int idString) {
        Toast.makeText(getActivity(), idString, Toast.LENGTH_SHORT).show();
    }
}
