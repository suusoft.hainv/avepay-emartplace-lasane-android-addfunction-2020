package com.aveplus.avepay_cpocket.trainingcenter.object;

import java.util.ArrayList;

public class DetailOrderHistory {

	private String orderId, fullName, email, paymentMethod, status, total, vat,
			createDate, payment_detail;
	private ArrayList<ProductHistory> listProHistory;

	public DetailOrderHistory() {
		super();
	}

	public DetailOrderHistory(String orderId, String fullName, String email,
                              String paymentMethod, String status, String total, String vat,
                              String createDate, ArrayList<ProductHistory> listProHistory) {
		super();
		this.orderId = orderId;
		this.fullName = fullName;
		this.email = email;
		this.paymentMethod = paymentMethod;
		this.status = status;
		this.total = total;
		this.vat = vat;
		this.createDate = createDate;
		this.listProHistory = listProHistory;
	}

	public String getPayment_detail() {
		return payment_detail;
	}

	public void setPayment_detail(String payment_detail) {
		this.payment_detail = payment_detail;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public ArrayList<ProductHistory> getListProHistory() {
		return listProHistory;
	}

	public void setListProHistory(ArrayList<ProductHistory> listProHistory) {
		this.listProHistory = listProHistory;
	}
}
