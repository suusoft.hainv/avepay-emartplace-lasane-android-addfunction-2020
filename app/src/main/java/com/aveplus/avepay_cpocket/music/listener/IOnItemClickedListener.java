package com.aveplus.avepay_cpocket.music.listener;

import android.view.View;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public interface IOnItemClickedListener extends IBaseListener {
     void onItemClicked(View view, int position);
}
