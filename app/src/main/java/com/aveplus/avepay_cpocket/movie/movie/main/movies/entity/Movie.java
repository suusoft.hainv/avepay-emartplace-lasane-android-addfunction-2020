package com.aveplus.avepay_cpocket.movie.movie.main.movies.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.movie.movie.model.listSub;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

// Builder Pattern
public class Movie implements Parcelable {

    public static final String SRC_YOUTUBE = "5";
    public static final String SRC_STREAM = "1";
    public static final String SRC_PICASA = "3";
    public static final String SRC_PICASA2 = "4";
    public static final String SRC_M3U8 = "2";
    public static final String SRC_YOUTUBE_LIVE = "9";
    public static final String SRC_WEBVIEW = "8";
    public static final String SRC_MP4 = "10";
    public static final String SRC_GOOGLEDRIVE = "17";

    private String movie_id;
    private String movie_name;
    private String movie_video;
    private String movie_thumb;
    private String movie_desc;
    private String movie_year;
    private String movie_imdb;
    private String movie_sub_title;

    @SerializedName("listSub")
    private ArrayList<listSub> listSubs;

    public ArrayList<listSub> getListSubs() {
        return listSubs;
    }

    public void setListSubs(ArrayList<listSub> listSubs) {
        this.listSubs = listSubs;
    }

    @SerializedName("source_id")
    private String movie_sourceId;
    private float movie_rating;
    private boolean movie_isFavo;
    private String movie_director;
    private String movie_writer;
    private String movie_actor;
    private String movie_lang;
    private String play_with;


    public Movie() {
    }

    public Movie(String movie_id, String movie_name, String movie_video, String movie_sub_title, String movie_thumb, String movie_desc, String movie_year, String movie_imdb, String movie_sourceId, float movie_rating,
                 boolean movie_isFavo, String movie_lang, String actor, String director, String play_with) {
        this.movie_id = movie_id;
        this.movie_name = movie_name;
        this.movie_video = movie_video;
        this.movie_sub_title = movie_sub_title;
        this.movie_thumb = movie_thumb;
        this.movie_desc = movie_desc;
        this.movie_year = movie_year;
        this.movie_imdb = movie_imdb;
        this.movie_sourceId = movie_sourceId;
        this.movie_rating = movie_rating;
        this.movie_isFavo = movie_isFavo;
        this.movie_lang = movie_lang;
        this.movie_actor = actor;
        this.movie_director = director;
        this.play_with = play_with;
    }

    protected Movie(Parcel in) {
        movie_id = in.readString();
        movie_name = in.readString();
        movie_video = in.readString();
        movie_sub_title = in.readString();
        movie_thumb = in.readString();
        movie_desc = in.readString();
        movie_year = in.readString();
        movie_imdb = in.readString();
        movie_sourceId = in.readString();
        movie_rating = in.readFloat();
        movie_isFavo = in.readByte() != 0;
        movie_director = in.readString();
        movie_writer = in.readString();
        movie_actor = in.readString();
        movie_lang = in.readString();
        play_with = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(movie_id);
        dest.writeString(movie_name);
        dest.writeString(movie_video);
        dest.writeString(movie_sub_title);
        dest.writeString(movie_thumb);
        dest.writeString(movie_desc);
        dest.writeString(movie_year);
        dest.writeString(movie_imdb);
        dest.writeString(movie_sourceId);
        dest.writeFloat(movie_rating);
        dest.writeByte((byte) (movie_isFavo ? 1 : 0));
        dest.writeString(movie_director);
        dest.writeString(movie_writer);
        dest.writeString(movie_actor);
        dest.writeString(movie_lang);
        dest.writeString(play_with);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getMovie_sub_title() {
        return movie_sub_title;
    }

    public void setMovie_sub_title(String movie_sub_title) {
        this.movie_sub_title = movie_sub_title;
    }

    public String getPlay_with() {
        return play_with;
    }

    public void setPlay_with(String play_with) {
        this.play_with = play_with;
    }

    public static Creator<Movie> getCreator() {
        return CREATOR;
    }

    public String getMovie_lang() {
        return movie_lang;
    }

    public void setMovie_lang(String movie_lang) {
        this.movie_lang = movie_lang;
    }

    public String getMovie_writer() {
        return movie_writer;
    }

    public String getMovie_actor() {
        return movie_actor;
    }

    public void setMovie_writer(String movie_writer) {
        this.movie_writer = movie_writer;
    }

    public void setMovie_actor(String actor) {
        this.movie_actor = actor;
    }

    public String getMovie_director() {
        return movie_director;
    }

    public void setMovie_director(String movie_director) {
        this.movie_director = movie_director;
    }

    public String getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(String movie_id) {
        this.movie_id = movie_id;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public void setMovie_name(String movie_name) {
        this.movie_name = movie_name;
    }

    public String getMovie_video() {
        return movie_video;
    }

    public void setMovie_video(String movie_video) {
        this.movie_video = movie_video;
    }

    public String getMovie_thumb() {
        return movie_thumb;
    }

    public void setMovie_thumb(String movie_thumb) {
        this.movie_thumb = movie_thumb;
    }

    public String getMovie_desc() {
        return movie_desc;
    }

    public void setMovie_desc(String movie_desc) {
        this.movie_desc = movie_desc;
    }

    public String getMovie_year() {
        return movie_year;
    }

    public void setMovie_year(String movie_year) {
        this.movie_year = movie_year;
    }

    public String getMovie_imdb() {
        return movie_imdb;
    }

    public void setMovie_imdb(String movie_imdb) {
        this.movie_imdb = movie_imdb;
    }

    public String getMovie_sourceId() {
        return movie_sourceId;
    }

    public void setMovie_sourceId(String movie_sourceId) {
        this.movie_sourceId = movie_sourceId;
    }

    public float getMovie_rating() {
        return movie_rating;
    }

    public void setMovie_rating(float movie_rating) {
        this.movie_rating = movie_rating;
    }

    public boolean isMovie_isFavo() {
        return movie_isFavo;
    }

    public void setMovie_isFavo(boolean movie_isFavo) {
        this.movie_isFavo = movie_isFavo;
    }

    public boolean isSourceYoutube() {
        return getMovie_sourceId().equals(SRC_YOUTUBE);
    }

    public boolean isSourceM3U8() {
        return getMovie_sourceId().equals(SRC_M3U8);
    }

    public boolean isSourceYoutobeLive() {
        return getMovie_sourceId().equals(SRC_YOUTUBE_LIVE);
    }

    public boolean isSourceWEBVIEW() {
        return getMovie_sourceId().equals(SRC_WEBVIEW);
    }

    public boolean isSourceMp4() {
        return getMovie_sourceId().equals(SRC_MP4);
    }

    public boolean isSourceGoogle() {
        return getMovie_sourceId().equals(SRC_GOOGLEDRIVE);
    }
}
