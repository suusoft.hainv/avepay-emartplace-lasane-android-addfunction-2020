package com.aveplus.avepay_cpocket.music.main.payment;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListActivityBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.esewa.android.sdk.payment.ESewaConfiguration;
import com.esewa.android.sdk.payment.ESewaPayment;
import com.esewa.android.sdk.payment.ESewaPaymentActivity;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import khalti.checkOut.api.Config;
import khalti.checkOut.api.OnCheckOutListener;
import khalti.checkOut.helper.KhaltiCheckOut;

public class ListSubscribeActivity extends BaseListActivityBinding {
    private static final String TAG = ListSubscribeActivity.class.getSimpleName();
    private SubscribeVM viewModel;
    private SubscribeAdapter mAdapter;
    private PackageSubscribe item;
    //Ewasa
    private static final String CONFIG_ENVIRONMENT = ESewaConfiguration.ENVIRONMENT_TEST;
    private static final int REQUEST_CODE_PAYMENT = 1;
    private ESewaConfiguration eSewaConfiguration;

    private static final String MERCHANT_ID = "JB0BBQ4aD0UqIThFJwAKBgAXEUkEGQUBBAwdOgABHD4DChwUAB0R";
    private static final String MERCHANT_SECRET_KEY = "BhwIWQQADhIYSxILExMcAgFXFhcOBwAKBgAXEQ==";

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_subscribe;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        mAdapter = new SubscribeAdapter(self, viewModel.getListData());
        recyclerView.setLayoutManager(viewModel.getLayoutManager());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(viewModel.getOnScrollListener());
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new SubscribeVM(self);
        return viewModel;
    }

    @Override
    protected void onViewCreated() {
        setToolbarTitle("Select a Package");
        setIvTitile(R.drawable.right);
        setTitleColor(R.color.black);
    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {
//        Bundle bundle = getIntent().getBundleExtra(Constant.PREF_KEY_DATA);
//        item = bundle.getParcelable(Constant.PREF_KEY_OBJECT);
//        Log.e(TAG, "onPrepareCreateView: " + new Gson().toJson(item));
        eSewaConfiguration = new ESewaConfiguration()
                .clientId(MERCHANT_ID)
                .secretKey(MERCHANT_SECRET_KEY)
                .environment(CONFIG_ENVIRONMENT);
    }

    private void makePayment(String amount) {
        ESewaPayment eSewaPayment = new ESewaPayment(amount, "someProductName", "someUniqueId_" + System.nanoTime(), "https://somecallbackurl.com");
        Intent intent = new Intent(ListSubscribeActivity.this, ESewaPaymentActivity.class);
        intent.putExtra(ESewaConfiguration.ESEWA_CONFIGURATION, eSewaConfiguration);
        intent.putExtra(ESewaPayment.ESEWA_PAYMENT, eSewaPayment);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == RESULT_OK) {
                String s = data.getStringExtra(ESewaPayment.EXTRA_RESULT_MESSAGE);
                Log.e("Proof of Payment", s);
                Toast.makeText(this, "SUCCESSFUL PAYMENT", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Canceled By User", Toast.LENGTH_SHORT).show();
            } else if (resultCode == ESewaPayment.RESULT_EXTRAS_INVALID) {
                String s = data.getStringExtra(ESewaPayment.EXTRA_RESULT_MESSAGE);
                Log.e("Proof of Payment", s);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.SubscribeItemClicked event) {
        if (DataStoreManager.getStatusPaymentMethod().equals("esewa")) {
            makePayment(event.type);
        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put("merchant_extra", "This is extra data");
            map.put("merchant_extra_2", "This is extra data 2");
            Log.e(TAG, "onMessageEvent: " + Long.parseLong(event.type));
            Config config = new Config(Constant.pub, "Product ID", "Product Name", "", Long.parseLong(event.type + "00"), map, new OnCheckOutListener() {

                @Override
                public void onSuccess(HashMap<String, Object> data) {

                    Log.e("Payment confirmed", data + event.type);
                }

                @Override
                public void onError(String action, String message) {
                    Log.e(action, message);
                }
            });
            KhaltiCheckOut khaltiCheckOut = new KhaltiCheckOut(this, config);
            khaltiCheckOut.show();
        }


    }

    public void getPaymentKhalti(String token, String khalti_token, String amount) {
        RequestManager.getPaymentKhalti(token, khalti_token, amount, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                Log.e(TAG, "onSuccess: Khalti Payment");
            }

            @Override
            public void onError(String message) {

            }
        });
    }
}
