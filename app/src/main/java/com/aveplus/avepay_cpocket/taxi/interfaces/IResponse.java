package com.aveplus.avepay_cpocket.taxi.interfaces;

/**
 * Created by SuuSoft.com on 09/04/2015.
 */
public interface IResponse {
    void onResponse(Object response);
}
