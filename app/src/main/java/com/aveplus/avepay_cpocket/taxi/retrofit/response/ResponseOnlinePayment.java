package com.aveplus.avepay_cpocket.taxi.retrofit.response;


import android.content.Context;

import androidx.annotation.Nullable;


import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "PaymentResponse")
public class ResponseOnlinePayment {

    @Element(name = "ResponseCode")
    public int responseCode;

    @Element(name = "ResponseMessage")
    public String responseMessage;

    @Element(name = "BillMapTransactionId")
    public String billMapTransactionId;

    @Nullable
    @Element(name = "EWPTransactionId", data=false,  required=false )
    public String eWPTransactionId;


    public ResponseOnlinePayment() {
    }

    public boolean isSuccess(Context context){
        if (responseCode == 1000)
            return true;

        AppUtil.showToast(context, responseMessage);
        return false;
    }
}
