package com.aveplus.avepay_cpocket.music.configs;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public class Constant {
    public static String pub = "test_public_key_8cc7125b6c4d43be9fd06a9bd6fe0802";
    public static final int NUMBER_OF_CLICKED = 40;

    public static final String PREF_KEY_ID = "PREF_KEY_ID";
    public static final String PREF_KEY_OBJECT = "PREF_KEY_OBJECT";
    public static final String PREF_KEY_POSITION = "PREF_KEY_POSITION";
    public static final String PREF_KEY_DATA = "PREF_KEY_DATA";
    public static final String PREF_KEY_DATA_OBJECT = "PREF_KEY_DATA_OBJECT";
    public static final String PREF_KEY_DATA_OBJECT_LIST = "PREF_KEY_DATA_OBJECT_LIST";
    public static final String PREF_KEY_DATA_PLAYLIST_ID = "PREF_KEY_DATA_PLAYLIST_ID";

    public static final String PREF_KEY_DATA_LIST = "PREF_KEY_DATA_LIST";

    public static final String PREF_KEY_ITEM_POSITION = "PREF_KEY_ITEM_POSITION";
    public static final String KEY_TOTAL_PAGE = "total_page";

    // Code pick image
    public static final int RQ_CODE_PICK_IMG = 1001;

    // broast cast for player

    public class Caching{
        public static final String KEY_REQUEST = "request";
        public static final String KEY_RESPONSE = "response";
        public static final String KEY_TIME_UPDATED = "time_updated";
        public static final String CACHING_PARAMS_TIME_REQUEST = "caching_time_request";

    }

    public static final int ID_NOTIFICATION = 290292;

    public class Player{
        public static final int PLAY = 2000;
        public static final int NEXT = 2001;
        public static final int PREVIOUS = 2002;
        public static final int PAUSE = 2003;
        public static final int RESUME = 2004;
        public static final int STOP = 2005;
        public static final int CANCEL_NOTIFICATION = 2006;
        public static final String ACTION = "ACTION";
        public static final int PREPARE = 2007;
    }


}
