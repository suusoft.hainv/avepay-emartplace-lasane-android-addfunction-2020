package com.aveplus.avepay_cpocket.trainingcenter.config;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

/**
 * Created by trangpham on 2/27/18.
 */

public class Constant {
    public static final String PAYPAL_CLIENT_APP_ID = AppController.getInstance().getString(R.string.PAYPAL_CLIENT_APP_ID);

    public class Notification {
        public static final String KEY_ID = "NOTIFICATION_ID";
        public static final String KEY_TYPE = "NOTIFICATION_TYPE";
    }
}
