package com.aveplus.avepay_cpocket.music.main.favorite;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentNavBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class FavoriteSongFragment extends BaseListFragmentNavBinding {

    FavoriteVM viewModel;

    public static FavoriteSongFragment newInstance() {

        Bundle args = new Bundle();

        FavoriteSongFragment fragment = new FavoriteSongFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new FavoriteVM(self);
        return viewModel;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        // setIvTitile(R.drawable.ic_favorited_24dp);
        setTitle(R.string.favorites);
        SingleAdapter adapter = new SingleAdapter(self, R.layout.item_song, viewModel.getListData(), ItemFavoriteVM.class, viewModel.getActionListener());
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getMenuLayout() {
        return R.menu.menu_action_mylist;
    }

    @Override
    public void onMenuItemClick(MenuItem item) {
        viewModel.onMenuItemClick(item);
    }
}
