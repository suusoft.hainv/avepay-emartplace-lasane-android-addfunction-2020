package com.aveplus.avepay_cpocket.movie.movie.main.movies.model;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;


import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Language;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieBuilder;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.MovieGenre;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MovieParser {
    private static final String TAG = MovieParser.class.getName();

    public static String TAG_ITEMS = "items";
    public static String TAG_ID = "id";
    // TAG MOVIE
    private static String TAG_MESSAGE = "message";
    private static String TAG_TOTALS_RESULT = "total_results";
    private static final String TAG_DATA = "data";
    private static String TAG_PAGES = "total_page";
    private static String TAG_MOVIES = "movies";
    private static String TAG_MOVIE_ID = "id";
    private static String TAG_MOVIE_NAME = "title";
    private static String TAG_MOVIE_VIDEO = "url";
    private static String TAG_MOVIE_THUMB = "movie_thumb";
    private static String TAG_MOVIE_DESC = "description";
    private static String TAG_MOVIE_YEAR = "created_at";
    private static String TAG_MOVIE_IMDB = "movie_imdb";
    private static String TAG_MOVIE_ACTOR = "movie_actor";
    private static String TAG_MOVIE_DIRECTOR = "movie_director";
    private static String TAG_MOVIE_RATING = "movie_rating";
    private static String TAG_MOVIE_SOURCEID = "movie_sourceId";
    private static String TAG_MOVIE_CREATEDAT = "movie_created_at";
    private static String TAG_MOVIE_LANG = "movie_language";
    // TAG GENRES
    private static final String TAG_GENRES = "data";
    private static final String TAG_GENRE_ID = "id";
    private static final String TAG_GENRE_NAME = "genre";
    private static final String TAG_GENRE_PARENT = "created_at";

    public static List<Movie> getMovies(Context mContext, JSONObject jsonObject) {

        List<Movie> mMovies = new ArrayList<Movie>();
        int pages = 0;
        int totalResults = 0;
        if (jsonObject == null)
            return null;
        Log.i(TAG, jsonObject.toString());

        try {
            totalResults = jsonObject.optInt(TAG_TOTALS_RESULT);
            pages = jsonObject.optInt(TAG_PAGES);
            JSONArray movies = jsonObject.getJSONArray(TAG_DATA);
            JSONObject movieObject;
            String movie_id, movie_name, movie_video, movie_thumb, movie_desc, movie_year, movie_imdb,
                    movie_sourceId, movie_actor, movie_director, play_with;
            Movie movie;
            for (int i = 0; i < movies.length(); i++) {
                movieObject = movies.getJSONObject(i);
                movie_sourceId = movieObject.optString(TAG_MOVIE_SOURCEID, "");
                if (movie_sourceId.equals("3"))
                    continue;
                movie_id = movieObject.optString("id");
                movie_name = movieObject.optString("title");
                movie_video = movieObject.optString("url");
                movie_thumb = movieObject.optString("image");
                movie_desc = movieObject.optString("description");
                movie_year = movieObject.optString("created_at");
                movie_imdb = movieObject.optString("modified_at");
                movie_actor = movieObject.optString("modified_at");
                movie_sourceId = movieObject.optString("source_id");
                play_with = movieObject.optString("play_with");
                Log.e(TAG, "getMoviesByGenre: " + play_with);
                if (movie_actor.equals("null"))
                    movie_actor = "";
                movie_director = "";
                float movie_rating = Float.parseFloat(movieObject
                        .optString(TAG_MOVIE_RATING, "0"));
//                boolean isFavo = MoviesManagement.getInstance().isMyMovie(
//                        mContext, movie_id);
                boolean isFavo = false;
//                Log.e(TAG, "IMAGEURL: " + movie_thumb);
//                Log.e(TAG, "IMAGEVIDEO: " + movie_video);
                if (!movie_thumb.contains("http"))
                    movie_thumb = "";
                movie = new MovieBuilder().setMovie_id(movie_id).setMovie_name(movie_name)
                        .setMovie_video(movie_video).setMovie_thumb(movie_thumb)
                        .setMovie_desc(movie_desc).setMovie_year(movie_year)
                        .setMovie_imdb(movie_imdb).setMovie_sourceId(movie_sourceId)
                        .setMovie_rating(movie_rating).setMovie_isFavo(isFavo)
                        .setMovieDirector(movie_director).setMovieActor(movie_actor).setPlayWith(play_with)
                        .createMovie();

                mMovies.add(movie);
            }
        } catch (JSONException e) {
            Log.i(TAG, e.getMessage());
        }
        return mMovies;
    }

    public static SparseArray<Object> getMoviesByGenre(Context mContext, JSONObject jsonObject) {

        List<Movie> mMovies = new ArrayList<Movie>();
        int pages = 0;
        int totalResults = 0;
        if (jsonObject == null)
            return null;
        Log.i(TAG, jsonObject.toString());

        try {
            totalResults = jsonObject.optInt(TAG_TOTALS_RESULT);
            pages = jsonObject.optInt(TAG_PAGES);
            JSONArray movies = jsonObject.getJSONArray(TAG_DATA);
            JSONObject movieObject;
            String movie_id, movie_name, movie_video, movie_sub_title, movie_thumb, movie_desc, movie_year, movie_imdb,
                    movie_sourceId, movie_actor, movie_director, play_with;
            Movie movie;
            for (int i = 0; i < movies.length(); i++) {
                movieObject = movies.getJSONObject(i);
                movie_sourceId = movieObject.optString(TAG_MOVIE_SOURCEID, "");
                if (movie_sourceId.equals("3"))
                    continue;
                movie_id = movieObject.optString("id");
                movie_name = movieObject.optString("title");
                movie_video = movieObject.optString("url");
                movie_sub_title = movieObject.optString("sub_title");
                movie_thumb = movieObject.optString("image");
                movie_desc = movieObject.optString("description");
                movie_year = movieObject.optString("created_at");
                movie_imdb = movieObject.optString("modified_at");
                movie_actor = movieObject.optString("modified_at");
                movie_sourceId = movieObject.optString("source_id");
                play_with = movieObject.optString("play_with");
                Log.e(TAG, "getMoviesByGenre: " + play_with);
                if (movie_actor.equals("null"))
                    movie_actor = "";
                movie_director = "";
                float movie_rating = Float.parseFloat(movieObject
                        .optString(TAG_MOVIE_RATING, "0"));
//                boolean isFavo = MoviesManagement.getInstance().isMyMovie(
//                        mContext, movie_id);
                boolean isFavo = false;
//                Log.e(TAG, "IMAGEURL: " + movie_thumb);
//                Log.e(TAG, "IMAGEVIDEO: " + movie_video);
                if (!movie_thumb.contains("http"))
                    movie_thumb = "";
                movie = new MovieBuilder().setMovie_id(movie_id).setMovie_name(movie_name)
                        .setMovie_video(movie_video).setMovie_sub_title(movie_sub_title).setMovie_thumb(movie_thumb)
                        .setMovie_desc(movie_desc).setMovie_year(movie_year)
                        .setMovie_imdb(movie_imdb).setMovie_sourceId(movie_sourceId)
                        .setMovie_rating(movie_rating).setMovie_isFavo(isFavo)
                        .setMovieDirector(movie_director).setMovieActor(movie_actor).setPlayWith(play_with)
                        .createMovie();

                mMovies.add(movie);
            }
        } catch (JSONException e) {
            Log.i(TAG, e.getMessage());
        }

        SparseArray<Object> result = new SparseArray<Object>();
        result.put(1, mMovies);
        result.put(2, pages);
        result.put(3, totalResults);
        return result;
    }

    public static List<MovieGenre> getMovieGenres(JSONObject jsonObject) {

        List<MovieGenre> mGenres = new ArrayList<MovieGenre>();

        if (jsonObject == null)
            return null;
        Log.i(TAG, jsonObject.toString());
        try {
            JSONArray genres = jsonObject.getJSONArray(TAG_GENRES);
            for (int i = 0; i < genres.length(); i++) {
                JSONObject genreObject = genres.getJSONObject(i);
                int genre_id = genreObject.optInt(TAG_GENRE_ID);
                String genre_name = genreObject.optString(TAG_GENRE_NAME);
                int genre_parent = 0;

                MovieGenre movieGenre = new MovieGenre(genre_id, genre_name, genre_parent);
                mGenres.add(movieGenre);
            }
        } catch (JSONException e) {
            Log.i(TAG, e.getMessage());
        }
        return mGenres;
    }

    public static boolean isExistMovie(JSONObject jsonObject) {
        if (jsonObject == null)
            return false;
        Log.i(TAG, jsonObject.toString());
        try {
            JSONArray items = jsonObject.optJSONArray(TAG_ITEMS);
            JSONObject thefirstObject = items.optJSONObject(0);
            if (thefirstObject.has(TAG_ID)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
        }
        return false;
    }

    //Parser BackDrop image of Movie
    public static String parserImageBackdrop(JSONObject object) {
        String url = object.optString("backdrop_path");
        return url;
    }

    public static Movie parserMovieDetial(JSONObject object) {
        Movie movie = new Movie();
        movie.setMovie_director(object.optString("Director"));
        movie.setMovie_writer(object.optString("Writer"));
        movie.setMovie_actor(object.optString("Actors"));

        return movie;
    }

    //Parser newest movie from api
    public static Movie parserNewestMovie(JSONArray arr) throws JSONException {
        Movie movie = new Movie();
        JSONObject object = arr.getJSONObject(0);
        movie.setMovie_id(object.getString("movie_id"));
        movie.setMovie_name(object.getString("movie_name"));
        movie.setMovie_desc(object.getString("movie_desc"));
        movie.setMovie_thumb(object.getString("movie_thumb"));
        movie.setMovie_video(object.getString("movie_link"));
        movie.setMovie_year(object.getString("movie_year"));
        movie.setMovie_rating(Float.valueOf(object.getString("movie_rating")));
        movie.setMovie_imdb(object.getString("movie_imdbid"));
        movie.setMovie_actor(object.optString(TAG_MOVIE_ACTOR, ""));
        movie.setMovie_director("");

        return movie;
    }

    //Parser newest movies with language
    public static LinkedHashMap<Language, List<Movie>> parserNewMovie(JSONArray arr) throws JSONException {
        LinkedHashMap<Language, List<Movie>> newestMovies = new LinkedHashMap<>();
        List<Movie> movieList = new ArrayList<>();

        JSONObject wrap, object;
        JSONArray movies;
        String langName;
        int langId;
        Movie movie;
        Language language;
        for (int i = 0; i < arr.length(); i++) {
            wrap = arr.getJSONObject(i);
            langId = wrap.getInt("lang_id");
            langName = wrap.getString("lang_name");
            movies = wrap.getJSONArray("movies");
            language = new Language(langId, langName);
            for (int j = 0; j < movies.length(); j++) {
                movie = new Movie();
                object = movies.getJSONObject(j);
                movie.setMovie_id(object.getString("movie_id"));
                movie.setMovie_name(object.getString("movie_name"));
                movie.setMovie_desc(object.getString("movie_desc"));
                movie.setMovie_thumb(object.getString("movie_thumb"));
                movie.setMovie_video(object.getString("movie_link"));
                movie.setMovie_year(object.getString("movie_year"));
                movie.setMovie_rating(Float.valueOf(object.getString("movie_rating")));
                movie.setMovie_imdb(object.getString("movie_imdbid"));
                movie.setMovie_actor(object.optString(TAG_MOVIE_ACTOR, ""));
                movie.setMovie_director("");
                movieList.add(movie);
            }
            newestMovies.put(language, movieList);
            movieList = new ArrayList<>();
        }
        return newestMovies;
    }

    // get movie by language
    public static List<Movie> parserMovieByLanguage(JSONArray arr) throws JSONException {
        List<Movie> movieList = new ArrayList<>();
        Movie movie;
        JSONObject object;
        for (int i = 0; i < arr.length(); i++) {
            movie = new Movie();
            object = arr.getJSONObject(i);
            movie.setMovie_id(object.getString("movie_id"));
            movie.setMovie_name(object.getString("movie_name"));
            movie.setMovie_desc(object.getString("movie_desc"));
            movie.setMovie_thumb(object.getString("movie_thumb"));
            movie.setMovie_video(object.getString("movie_link"));
            movie.setMovie_year(object.getString("movie_year"));
            movie.setMovie_rating(Float.valueOf(object.getString("movie_rating")));
            movie.setMovie_imdb(object.getString("movie_imdbid"));
            movie.setMovie_actor(object.optString(TAG_MOVIE_ACTOR, ""));
            movie.setMovie_director("");
            movieList.add(movie);
        }
        return movieList;
    }

    public static String getIdLive(JSONObject jsonObject) {


        String videoId = "";

        if (jsonObject == null)
            return null;
        Log.i(TAG, jsonObject.toString());
        try {
            JSONArray genres = jsonObject.getJSONArray("items");

            videoId = genres.getJSONObject(0).getJSONObject("id").getString("videoId");
            Log.e(TAG, "getIdLive: " + videoId);
//            JSONObject genreObject = genres.getJSONObject("id");
//            videoId = genreObject.optString("videoId");


        } catch (JSONException e) {
            Log.i(TAG, e.getMessage());
        }
        return videoId;
    }
}