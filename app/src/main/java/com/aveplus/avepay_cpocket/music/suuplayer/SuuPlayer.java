package com.aveplus.avepay_cpocket.music.suuplayer;

import com.google.android.exoplayer2.ExoPlaybackException;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public interface SuuPlayer {

    public static final int TYPE_DASH = 0;
    public static final int TYPE_SS = 1;
    public static final int TYPE_HLS = 2;
    public static final int TYPE_OTHER = 3;

    void initializePlayer();

    void prepare(String urlStream, int type);

    void setShouldAutoPlay(boolean shouldAutoPlay);

    boolean isPlaying();

    void togglePlay();

    void play();

    void pause();

    long getDuration();

    long getCurrentPosition();

    int getState();

    void seekTo(long positionMs);

    void releasePlayer() ;

    void hideSystemUi() ;

    void setFullScreen(boolean isFullScreen);

    void addEventListener(EventListener listener);

    /**
     * Listener of changes in player state.
     */
    interface EventListener {
        void onStared();

        void onPlaying();

        void onPaused();

        void onStopped();

        void onCompletion();

        void onFullScreen(boolean isFullScreen);

        void onBuffering(boolean var1);

        void onRepeatModeChanged(int repeatMode);

        void onPlayerError(ExoPlaybackException error);

    }
}
