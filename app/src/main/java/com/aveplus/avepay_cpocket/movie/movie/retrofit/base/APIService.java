package com.aveplus.avepay_cpocket.movie.movie.retrofit.base;



import androidx.annotation.Nullable;

import com.aveplus.avepay_cpocket.movie.movie.retrofit.param.Param;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface APIService {
    /**
     * method get
     *
     * @param url    root url
     * @param params params
     */

    @GET()
    @Headers("Cache-Control: no-cache")
    Call<JsonObject> get(
            @Url String url,
            @QueryMap Map<String, String> params
    );


    /**
     * method post
     *
     * @param url    root url
     * @param params params
     */

    @POST()
    @Headers("Cache-Control: no-cache")
    Call<JsonObject> post(
            @Url String url,
            @QueryMap Map<String, String> params
    );


    /**
     * method post
     *
     * @param url        root url
     * @param params     params
     * @param paramFiles paramFiles
     */

    @POST()
    @Headers("Cache-Control: no-cache")
    @Multipart
    Call<JsonObject> post(
            @Url String url,
            @QueryMap Map<String, String> params,
            @Nullable @Part ArrayList<MultipartBody.Part> paramFiles
    );


    @GET("app-code/code")
    @Headers("Cache-Control: no-cache")
    Call<BaseReponse> login_by_code(@Query(Param.PARAM_MAC_ADDRESS) String mac_address,
                                    @Query(Param.PARAM_CODE_NAME) String code,
                                    @Query(Param.PARAM_TIME) String time);


    @GET("movie/movie-sub")
    @Headers("Cache-Control: no-cache")
    Call<ResponseListSub> getListSub(@Query(Param.PARAM_ID) String id);

}
