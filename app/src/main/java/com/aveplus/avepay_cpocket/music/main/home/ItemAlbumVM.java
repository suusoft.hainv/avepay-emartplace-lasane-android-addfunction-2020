package com.aveplus.avepay_cpocket.music.main.home;

import android.app.Activity;
import android.content.Context;
import android.view.View;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.model.Album;
import com.aveplus.avepay_cpocket.music.util.AdsUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ItemAlbumVM extends BaseAdapterVM {
    public static final String TAG = "Playlist";

    private Album album;
    private int height;
    private static int count = 0;

    public ItemAlbumVM(Context self, Object album, int position) {
        super(self, position);
        this.album = (Album) album;
        height = AppUtil.getScreenWidth((Activity) self) / 2 - AppUtil.convertDpToPixel(24);
    }


    public String getName() {
        return album.name;
    }

    public String getImage() {
        return album.thumbnail;
    }

    public String getSongCount() {
        return String.format(self.getString(R.string.songs_bind), album.songs_count + "");
    }

    public String getNameSong() {
        return album.nameSong;
    }

    public String getNameSinger() {
        return album.nameSinger;
    }

    public String getTitle() {
        return album.title;
    }

    public int getHeight() {
        return height;
    }


    @Override
    public void setData(Object object) {
        album = (Album) object;
        notifyChange();
    }

    public void onItemClicked(View view) {
        EventBus.getDefault().post(new Global.ItemAlbumClicked(album));
        //  AdsUtil.checkShowAds(self);

    }

    public static void checkShowAds(Context context) {
        count = count + 1;

        if (count == 5) {
            AdsUtil.loadInterstitial(context);
            count = 0;
        }
//        else if (count == 10) {
//            AdsUtil.loadInterstitial(context);
//            count = 5;
//
//        }
    }
}
