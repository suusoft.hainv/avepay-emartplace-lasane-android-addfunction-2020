package com.aveplus.avepay_cpocket.ebook.view.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.squareup.picasso.Picasso;


import java.util.List;

public class ListChapterAdapter extends RecyclerView.Adapter<ListChapterAdapter.ViewHolder> {

    public static final int START = 1;
    public static final int CONTINUES = 2;
    List<Chapter> mDatas;
    Context mContext;
    private IOnItemClick iOnItemClick;


    public interface IOnItemClick {
        public void onClick(int position, int flag);
    }

    public ListChapterAdapter(Context context, List<Chapter> mDatas) {
        this.mDatas = mDatas;
        mContext = context;
    }

    public void setIOnItemClick(IOnItemClick IOnItemClick) {
        this.iOnItemClick = IOnItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_chapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Chapter chapter = mDatas.get(position);
        if (chapter != null) {
            if (chapter.getImage() != null && !chapter.getImage().equals("")) {
                Picasso.get()
                        .load(chapter.getImage())
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(holder.imv_avatar);
            } else {
                holder.imv_avatar.setImageResource(R.mipmap.ic_launcher);
            }
            holder.tv_title.setText(chapter.getTitle());
            holder.tv_description.setText(Html.fromHtml(chapter.getDescription()));
            switch (chapter.getType()) {
                case Chapter.TYPE_PDF:
                    holder.imv_play.setImageResource(R.drawable.ic_type_pdf_white);
                    break;
                case Chapter.TYPE_MP3:
                    holder.imv_play.setImageResource(R.drawable.ic_type_music_white);
                    break;
                case Chapter.TYPE_VIDEO:
                    holder.imv_play.setImageResource(R.drawable.ic_type_video_white);
                    break;
                case Chapter.TYPE_EPUB:
                    holder.imv_play.setImageResource(R.drawable.ic_type_epub_white);
                    break;
                case Chapter.TYPE_NORMAL:
                    break;
            }

            holder.layoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != iOnItemClick) {
                        iOnItemClick.onClick(holder.getAdapterPosition(), START);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imv_avatar, imv_play;
        TextView tv_title, tv_description;
        RelativeLayout layoutItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_description = (TextView) itemView.findViewById(R.id.tv_description);
            imv_avatar = (ImageView) itemView.findViewById(R.id.imv_avatar);
            imv_play = (ImageView) itemView.findViewById(R.id.imv_play);
            layoutItem = (RelativeLayout) itemView.findViewById(R.id.layoutItem);
        }
    }
}
