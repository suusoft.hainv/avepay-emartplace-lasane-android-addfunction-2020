package com.aveplus.avepay_cpocket.ebook.widgets.textview;

/**
 * Created by Trang on 7/4/2017.
 */
public class TextFontConfig {

    // The app is using open-sans font
    public static final String FONT_BOLT = "fonts/Font-Bold.ttf";
    public static final String FONT_BOLT_ITALIC = "fonts/Font-BoldItalic.ttf";
    public static final String FONT_ITALIC = "fonts/Font-Italic.ttf";
    public static final String FONT_LIGHT = "fonts/Font-Light.ttf";
    public static final String FONT_LIGHT_ITALIC = "fonts/Font-LightItalic.ttf";
    public static final String FONT_REGULAR = "fonts/Font-Regular.ttf";

}
