package com.aveplus.avepay_cpocket.movie.movie.main.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.listener.IOnItemClickListener;
import com.aveplus.avepay_cpocket.movie.movie.model.MenuLeft;

import java.util.ArrayList;

/**
 * Copyright © 2019 SUUSOFT
 */

public class MenuLeftAdapte extends RecyclerView.Adapter<MenuLeftAdapte.ViewHolder>{

    private ArrayList<MenuLeft> menuLefts;
    private Context context;
    private MenuLeft menuLeft;
    private IOnItemClickListener iOnItemClickListener;





    public MenuLeftAdapte(ArrayList<MenuLeft> menuLefts, Context context, IOnItemClickListener iOnItemClickListener) {
        this.menuLefts = menuLefts;
        this.context = context;
        this.iOnItemClickListener = iOnItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_left, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        menuLeft = menuLefts.get(position);


        holder.icon.setImageResource(menuLeft.getIcon());
        holder.tvItemMenu.setText(menuLeft.getName());
//        if (menuLeft.getId()==5)
//            holder.deviderFunctionMain.setVisibility(View.VISIBLE);
//        else
//            holder.deviderFunctionMain.setVisibility(View.GONE);
        if (menuLeft.isSelected()){
            holder.llItemMenu.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        }

        else
            holder.llItemMenu.setBackgroundColor(context.getResources().getColor(R.color.transparent));
//        holder.llItemMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.e("OK", "onClick: " );
//                iOnItemClickListener.onItemClick(position);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return menuLefts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout llItemMenu;
        private ImageView icon;
        private TextView tvItemMenu;
        private View deviderFunctionMain;

        public ViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.img_icon);
            tvItemMenu = (TextView) itemView.findViewById(R.id.tv_item_menu);
            deviderFunctionMain = itemView.findViewById(R.id.devider_function_main);
            llItemMenu = (LinearLayout) itemView.findViewById(R.id.ll_item_menu);

            llItemMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("OK", "onClick: " );
                    iOnItemClickListener.onItemClickMenu(getAdapterPosition());
                }
            });

        }
    }
}
