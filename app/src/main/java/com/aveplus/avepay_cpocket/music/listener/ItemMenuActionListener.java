package com.aveplus.avepay_cpocket.music.listener;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public interface ItemMenuActionListener extends IBaseListener {

    void onClickWatch(int position);
    void onClickListen(int position);
    void onClickAddToList(int position);
    void onClickDelete(int position);
}
