package com.aveplus.avepay_cpocket.restaurant.main.restaurant.review;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.Review;
import com.bumptech.glide.Glide;


import java.util.List;

public class AdapteReview extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Review> reviewList;
    private Context context;


    public AdapteReview(List<Review> reviewList, Context context) {
        this.reviewList = reviewList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_review, viewGroup, false);
        return new ItemReview(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ((ItemReview) viewHolder).Container(reviewList.get(i));
    }

    @Override
    public int getItemCount() {
        if (reviewList.size() > 0)
            return reviewList.size();
        else
            return 0;
    }

    public class ItemReview extends RecyclerView.ViewHolder {
        private ImageView imgAvatar;
        private TextView tvAuthorName;
        private TextView tvCreatedDate;
        private TextView tvContent;

        public ItemReview(@NonNull View itemView) {
            super(itemView);
            imgAvatar = itemView.findViewById(R.id.img_avatar);
            tvAuthorName = itemView.findViewById(R.id.tv_author_name);
            tvCreatedDate = itemView.findViewById(R.id.tv_created_date);
            tvContent = itemView.findViewById(R.id.tv_content);

        }

        void Container(Review review) {
            if (review.getAuthorAvatar() != null && !review.getAuthorAvatar().isEmpty()) {
                Glide.with(context).load(review.getAuthorAvatar()).into(imgAvatar);
            } else {
                Glide.with(context).load(R.drawable.profile).into(imgAvatar);
            }
            if (review.getAuthorName() != null)
                tvAuthorName.setText(review.getAuthorName());
            if (review.getContent() != null)
                tvContent.setText(review.getContent());
            if (review.getCreatedDate() != null)
                tvCreatedDate.setText(review.getCreatedDate());
        }
    }
}
