package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.content.Context;

import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;


/**
 * Created by Suusoft on 9/27/2017.
 */
public class DetailsChapterPDFVM extends BaseViewModel {

    @Override
    public void getData(int page) {

    }

    private Chapter chapter;

    public DetailsChapterPDFVM(Context self, Chapter chapter) {
        super(self);
        this.chapter = chapter;
    }


}
