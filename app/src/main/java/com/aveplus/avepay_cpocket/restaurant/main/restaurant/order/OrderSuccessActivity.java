package com.aveplus.avepay_cpocket.restaurant.main.restaurant.order;
/**
 * Created by suusoft.com on 11/12/19.
 */
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.main.mainAct.MainActivity;


public class OrderSuccessActivity extends BaseActivity implements View.OnClickListener {
    private TextView tvHome;

    @Override
    protected int getLayoutInflate() {
        return R.layout.activiti_order_succes;
    }

    @Override
    protected void initView() {
        tvHome  = findViewById(R.id.tv_gohome);
        tvHome.setOnClickListener(this);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_gohome:
                Intent intent = new Intent(self, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }
}
