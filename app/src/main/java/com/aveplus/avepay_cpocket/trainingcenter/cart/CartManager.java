package com.aveplus.avepay_cpocket.trainingcenter.cart;


import com.aveplus.avepay_cpocket.trainingcenter.object.Product;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trangpv on 8/2/2017.
 */
public class CartManager {

    public static final String ITEM_CART_PROPERTY_CHANGE = "listItemCart";
    private List<ICartChangeListener> listener = new ArrayList<>();
    private ArrayList<Product> listItemCart = new ArrayList<>();

    private static CartManager instance;

    public interface ICartChangeListener extends PropertyChangeListener {

    }

    public static CartManager getInstance() {
        if (instance == null) {
            instance = new CartManager();
        }
        return instance;
    }

    public boolean isExist(Product item) {
        for (Product temp : listItemCart) {
            if (item.getId().equals(temp.getId())) {
                return true;
            }
        }

        return false;
    }

    /**
     * add item to cart
     */
    public void addCart(Product item) {
        this.listItemCart.add(item);
        notifyListeners(this, ITEM_CART_PROPERTY_CHANGE, "", "");
    }

    public ArrayList<Product> getYourCart() {
        if (listItemCart == null)
            listItemCart = new ArrayList<>();
        return this.listItemCart;
    }

    public int size() {
        return getYourCart().size();
    }

    /**
     * remove a item of cart
     */
    public void removeCart(Product item) {
        this.listItemCart.remove(item);
        notifyListeners(this, ITEM_CART_PROPERTY_CHANGE, "", "");
    }

    /**
     * remove a item of cart
     */
    public void removeCart(int position) {
        this.listItemCart.remove(position);
        notifyListeners(this, ITEM_CART_PROPERTY_CHANGE, "", "");
    }

    /**
     * clear all item of cart
     */
    public void clearCart() {
        this.listItemCart.clear();
        notifyListeners(this, ITEM_CART_PROPERTY_CHANGE, "", "");
    }

    /**
     *  get total cart
     *
     */
   /* public int getTotal() {
        int total = 0;
        for (Product item : CartManager.getInstance().getYourCart()) {
            if (!"0".equals(item.getProduct().getPromotion_price())) {
                total += item.getQuantity()
                        * Float.parseFloat(item.getProduct().getPromotion_price());
            } else {
                total += item.getQuantity() * Float.parseFloat(item.getProduct().getPrice());
            }
        }
        return total;
    }*/

    /**
     * Notify to all listener
     */
    private void notifyListeners(Object object, String property, String oldValue, String newValue) {
        for (ICartChangeListener name : listener) {
            name.propertyChange(new PropertyChangeEvent(this, property, oldValue, newValue));
        }
    }

    /**
     * Add a listener
     *
     * @param newListener listener
     */
    public void addCartChangeListener(ICartChangeListener newListener) {
        listener.add(newListener);
    }
}
