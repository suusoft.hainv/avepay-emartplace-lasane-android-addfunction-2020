package com.aveplus.avepay_cpocket.music.main.video;

import android.os.Bundle;
import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListActivityBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.google.gson.Gson;


public class ListVideoActivity extends BaseListActivityBinding {
    private static final String TAG = ListVideoActivity.class.getSimpleName();
    private CategoryVM viewModel;
    private VideoAdapter mAdapter;
    private Category item;

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        mAdapter = new VideoAdapter(self, viewModel.getListData(), item.getId());
        recyclerView.setLayoutManager(viewModel.getLayoutManager());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(viewModel.getOnScrollListener());
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new CategoryVM(self, item.getId());
        return viewModel;
    }

    @Override
    protected void onViewCreated() {
        setToolbarTitle(item.getName());
        setTitleColor(R.color.black);
        Log.e(TAG, "onViewCreated: " + item.getName());
    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {
        Bundle bundle = getIntent().getBundleExtra(Constant.PREF_KEY_DATA);
        item = bundle.getParcelable(Constant.PREF_KEY_OBJECT);
        Log.e(TAG, "onPrepareCreateView: " + new Gson().toJson(item));
    }
}
