package com.aveplus.avepay_cpocket.music.main.video2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.main.payment.BottomPaymentFragment;
import com.aveplus.avepay_cpocket.music.main.payment.IPayement;
import com.aveplus.avepay_cpocket.music.main.payment.ListSubscribeActivity;
import com.aveplus.avepay_cpocket.music.main.playvideo.PlayVideoActivity;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.ImageUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class AdapterMore extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ObjVideo> arrVideo;
    private ArrayList<ObjVideoCategory> arrVideoCate;
    private int LIST_VIDEO = 2;
    private int LIST_CATE = 1;
    private Context context;
    private CategoryClickListener categoryClickListener;

    public interface CategoryClickListener {
        void onListener(ObjVideoCategory objVideoCategory);
    }

    public AdapterMore(ArrayList<ObjVideo> arrVideo, ArrayList<ObjVideoCategory> arrVideoCate, CategoryClickListener ClickListener) {
        this.arrVideo = arrVideo;
        this.arrVideoCate = arrVideoCate;
        this.categoryClickListener = ClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (arrVideoCate.size() > 0) {
            return LIST_CATE;
        } else {
            return LIST_VIDEO;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        Log.e("TAGGGGGGG", "onCreateViewHolder: " + viewType);
        if (viewType == LIST_CATE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_video, parent, false);
            return new ItemCate(view);
        } else {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_list, parent, false);
            return new ItemVideo(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemVideo) {
            ((ItemVideo) holder).Container(arrVideo.get(position));
            ((ItemVideo) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().post(new Global.VideoItemClicked(arrVideo.get(position)));
                    StarAct(arrVideo.get(position));
                }
            });
        } else if (holder instanceof ItemCate) {
            if (arrVideoCate.size() > 0) {
                ((ItemCate) holder).Container(arrVideoCate.get(position));
            }
        }

    }

    private void StarAct(ObjVideo video) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.PREF_KEY_OBJECT, video);
        bundle.putParcelableArrayList(Constant.PREF_KEY_DATA_LIST, arrVideo);

        AppUtil.checkPayment(DataStoreManager.getUser().getToken());
        if (video.getIsFree() == 0) {
            if (DataStoreManager.getCheckPayment()) {
                Intent intent = new Intent(context, PlayVideoActivity.class);
                intent.putExtra(Constant.PREF_KEY_DATA, bundle);
                context.startActivity(intent);
            } else {
                BottomPaymentFragment bottomPaymentFragment = new BottomPaymentFragment(new IPayement() {
                    @Override
                    public void onPayment(String payment) {
                        Intent intent = new Intent(context, ListSubscribeActivity.class);
                        context.startActivity(intent);
                    }
                });
                bottomPaymentFragment.show(((ListVideoAct) context).getSupportFragmentManager(), bottomPaymentFragment.getTag());
            }

        } else {
            Intent intent = new Intent(context, PlayVideoActivity.class);
            intent.putExtra(Constant.PREF_KEY_DATA, bundle);
            context.startActivity(intent);
        }

    }


    @Override
    public int getItemCount() {
        if (arrVideoCate.size() > 0) {
            return arrVideoCate.size();
        } else if (arrVideo.size() > 0) {
            return arrVideo.size();
        } else
            return 0;
    }

    private class ItemCate extends RecyclerView.ViewHolder {
        private ImageView imgCate;
        private TextView tv_category;

        public ItemCate(@NonNull View itemView) {
            super(itemView);
            imgCate = itemView.findViewById(R.id.img_video_cate);
            tv_category = itemView.findViewById(R.id.tv_category);
        }

        private void Container(ObjVideoCategory objVideoCategory) {
            ImageUtil.setImage(imgCate, objVideoCategory.getImgThumb());
            tv_category.setText(objVideoCategory.getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    categoryClickListener.onListener(objVideoCategory);
                }
            });

        }
    }

    private class ItemVideo extends RecyclerView.ViewHolder {
        private ImageView imgThumb;
        private ImageView imgVip;
        private TextView tvTitle;
        private TextView tvDescription;

        public ItemVideo(@NonNull View itemView) {
            super(itemView);
            imgThumb = itemView.findViewById(R.id.img_thumb);
            imgVip = itemView.findViewById(R.id.img_vip);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvDescription = itemView.findViewById(R.id.tv_description);
        }

        private void Container(ObjVideo objVideo) {
            if (objVideo.getIsFree() == 0) {
                imgVip.setVisibility(View.VISIBLE);
            } else {
                imgVip.setVisibility(View.GONE);
            }
            ImageUtil.setImage(imgThumb, objVideo.getImage());
            tvTitle.setText(objVideo.getName());
            tvDescription.setText(objVideo.getDescription());
        }

    }
}
