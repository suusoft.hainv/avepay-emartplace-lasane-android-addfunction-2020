package com.aveplus.avepay_cpocket.restaurant.modelmanager;

import android.content.Context;
import android.util.Log;


import com.aveplus.avepay_cpocket.restaurant.configs.Config;
import com.aveplus.avepay_cpocket.restaurant.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.restaurant.network.BaseRequest;
import com.aveplus.avepay_cpocket.restaurant.util.AppUtil;

import java.util.HashMap;

/**
 */
public class RequestManager extends BaseRequest {

    private static final String TAG = RequestManager.class.getSimpleName();

    // Params
    private static final String PARAM_GCM_ID = "gcm_id";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_STATUS = "status";
    private static final String PARAM_IMEI = "ime";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_FULLNAME = "fullname";
    private static final String PARAM_IMAGE = "image";
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_USER_ID = "user_id";
    private static final String PARAM_MESSAGE = "message";

    private static final String PARAM_PAGE = "page";
    private static final String PARAM_KEYWORD    = "keyword";
    private static final String PARAM_LAT    = "buyer_lat";
    private static final String PARAM_LONG    = "buyer_long";
    private static final String PARAM_TOKEN    = "token";
    private static final String PARAM_ID = "id";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_NUMBER_PER_PAGE    = "number_per_page";

    private static final String PARAM_PAGE_SIZE = "per-page";


    ///////////////////////////////////////////////////////////////////////////
    // Add functions connecting to server
    ///////////////////////////////////////////////////////////////////////////
    /**/
    //
//    public static void upload(final CompleteListener completeListener) {
//
//        String url = "http://marketplace.suusoft.com/backend/web/index.php/api/deal";
//        HashMap<String, String> params = new HashMap<>();
//        params.put("token", "88cc54a2ca0dfe816f7f513c11c192ea");
//        params.put("action", "create");
//        params.put("duration", "4");
//        params.put("is_premium", "0");// 1 is android
//        params.put("is_renew", "0");// 1 is android
//        params.put("price", "60");// 1 is android
//        params.put("description", "a trang test");// 1 is android
//        params.put("name", "Dai ca trang test");// 1 is android
//        params.put("category_id", "3");// 1 is android
//        params.put("lat", "20.983698399999994");// 1 is android
//        params.put("long", "105.78573589999999");// 1 is android
//        params.put("address", "Ho Hoan Kiem - HN");// 1 is android
//        params.put("discount_type", "percent");// 1 is android
//        params.put("discount", "0.0");// 1 is android
//
//        HashMap<String, String> paramsFile = new HashMap<>();
//        String image = Environment.getExternalStorageDirectory().getPath() + "/" + "Download/download.jpg";
//        Log.e("eeeeee","image: "+ image);
//
//        paramsFile.put("image", image);
//        multipart(url,params,paramsFile,true,completeListener);
//    }

    public static void getRestaurant(Context context, int page, int number_per_page,  final BaseRequest.CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE, page+"");
        params.put(PARAM_NUMBER_PER_PAGE, number_per_page+"");

        get(Config.Api.RESTAURANT, params,completeListener);

    }

    public static void getRestaurantNearBy(Context context,double lat, double longtilute, String token, int page, int number_per_page,  final CompleteListener completeListener) {
        Log.d(TAG, "getRestaurantNearBy: "+token);
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_LAT, lat+"");
        params.put(PARAM_LONG, longtilute+"");
        params.put(PARAM_TOKEN, token);
        params.put(PARAM_PAGE, page+"");
        params.put(PARAM_NUMBER_PER_PAGE, number_per_page+"");

        get(Config.Api.RESTAURANT_NEARBY, params,completeListener);

    }

    public static void login(Context context, String username, String password, final CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USERNAME, username);
        params.put(PARAM_PASSWORD, password);
        params.put(PARAM_IMEI, AppUtil.getIMEI(context));
        params.put(PARAM_GCM_ID, DataStoreManager.getTokenFCM());
        params.put(PARAM_TYPE, "1");
        params.put(PARAM_STATUS, "1");

        get(Config.Api.URL_LOGIN, params, completeListener);
    }

    public static void search(String key, int page, int numberpage, final CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_KEYWORD, key);
        params.put(PARAM_PAGE, page+"");
        params.put(PARAM_NUMBER_PER_PAGE, numberpage+"");
        get(Config.Api.SEARCH, params,completeListener);
    }


    public static void getHome(Context context, CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        get(Config.Api.HOME, params, true, false, completeListener);
    }

    public static void getItemByCategory(int page, String categoryId, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE, page +"");
        params.put(PARAM_ID, categoryId);
        params.put(PARAM_PAGE_SIZE, Config.MAX_RESULT +"");
        get(Config.Api.ITEM_BY_CATEGORY, params, true , false , completeListener);
    }

    public static void getItemByPlaylist(int page, String playlistId, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE, page +"");
        params.put(PARAM_ID, playlistId);
        params.put(PARAM_PAGE_SIZE, Config.MAX_RESULT +"");
        get(Config.Api.ITEM_BY_PLAYLIST, params, true , false , completeListener);
    }

    public static void getAllCategories( final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE_SIZE, "50");
        get(Config.Api.ALL_CATEGORIES, params, true , false , completeListener);
    }

    public static void getAllAlbumBy(int page, int isTop, int isHot, int isNew, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE_SIZE, Config.MAX_RESULT + "");
        params.put(PARAM_PAGE, page + "");
        params.put("top", isTop +"");
        params.put("hot", isHot + "");
        params.put("new", isNew +"");
        get(Config.Api.ALL_ALBUM_BY, params, true , false , completeListener);
    }

    public static void searchSongs(int page, String keywork, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE_SIZE, Config.MAX_RESULT + "");
        params.put(PARAM_NAME, keywork);
        params.put(PARAM_PAGE, page + "");
        get(Config.Api.SEARCH, params, true , false , completeListener);
    }

}
