package com.aveplus.avepay_cpocket.taxi.configs;

import android.app.Activity;

import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;


public class SizeScreen {

    private static SizeScreen instance;
    public static int width, height, statusBarHeight;
    private static Activity activity;

    public static SizeScreen getInstance() {
        if (instance==null)
            instance = new SizeScreen();
        return instance;
    }

    public static  void init (Activity activity) {
       getInstance(). width = AppUtil.getScreenWidth(activity);
        getInstance().  height = AppUtil.getScreenHeight(activity);
        getInstance(). activity = activity;

        int resourceId = activity. getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = activity.getResources().getDimensionPixelSize(resourceId);
        }
    }


    public static int getWidth() {
        if (width==0)
            width = AppUtil.getScreenWidth(activity);
        return width;
    }

    public static int getHeight() {
        if (height==0)
            height = AppUtil.getScreenHeight(activity);
        return height;
    }
}
