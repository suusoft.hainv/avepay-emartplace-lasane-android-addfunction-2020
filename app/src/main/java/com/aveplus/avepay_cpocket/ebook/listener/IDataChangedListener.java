package com.aveplus.avepay_cpocket.ebook.listener;

import java.util.List;

/**
 * Created by Trang on 7/11/2017.
 */
public interface IDataChangedListener {
    void onListDataChanged(List<?> data, boolean isAppend);

}
