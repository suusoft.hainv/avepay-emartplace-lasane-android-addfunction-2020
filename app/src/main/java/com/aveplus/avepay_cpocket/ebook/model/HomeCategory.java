package com.aveplus.avepay_cpocket.ebook.model;


import com.aveplus.avepay_cpocket.ebook.base.model.model.BaseModel;

import java.util.ArrayList;

/**
 * Created by Trang on 6/29/2017.
 */
public class HomeCategory extends BaseModel {
    public static final String FEATURED = "1";
    public static final String NEW = "2";
    public static final String HOT = "3 hot";
    public static final String TOP = "category top";
    public static final String HEADER = "header";
    public static final String ARTIST = "single";
    public static final String CATEGORY = "category";
    public static final String BOOKS = "BOOKS";
    public String id, title, name, description, image, type, content;
    private ArrayList<Book> listBooks;
    private ArrayList<Category> listCategory;

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Book> getListBooks() {
        if (listBooks == null)
            listBooks = new ArrayList<>();
        return listBooks;
    }

    public void setListBooks(ArrayList<Book> listBooks) {
        this.listBooks = listBooks;
    }

    public ArrayList<Category> getListCategory() {
        if (listCategory == null)
            listCategory = new ArrayList<>();
        return listCategory;
    }

    public void setListCategory(ArrayList<Category> listCategory) {
        this.listCategory = listCategory;
    }

    public void setName(String name) {
        this.name = name;
    }
}
