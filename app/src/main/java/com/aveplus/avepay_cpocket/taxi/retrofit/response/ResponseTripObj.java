package com.aveplus.avepay_cpocket.taxi.retrofit.response;


import com.aveplus.avepay_cpocket.taxi.objects.TripObj;

public class ResponseTripObj extends BaseModel {

    public TripObj data;


    public TripObj getData() {
        return data;
    }

    public void setData(TripObj data) {
        this.data = data;
    }
}
