package com.aveplus.avepay_cpocket.ebook.util;

import android.os.Environment;
import android.util.Log;


import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;

import java.io.File;
import java.util.ArrayList;

public class FileUtil {

    public static void getFilesFromFolder(String folderPath){
        String path = Environment.getExternalStorageDirectory().toString()+"/" + folderPath;
        Log.e("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();
        Log.d("Files", "Size: "+ files.length);
        for (int i = 0; i < files.length; i++)
        {
            Log.d("Files", "FileName:" + files[i].getName());
        }


    }


    public static ArrayList<Chapter> getAllFilesDownloaded(){
        String pathPdf = Environment.getExternalStorageDirectory().toString()+ Constant.FOLDER_PDF;
        String pathEpub = Environment.getExternalStorageDirectory().toString()+ Constant.FOLDER_EPUB;
        ArrayList<Chapter> chapters = new ArrayList<>();
        ArrayList<File> files = new ArrayList<>();

        Log.e("Files", "Path: " + pathPdf);
        File directory = new File(pathPdf);
        File directoryEpub = new File(pathEpub);
        File[] filePdf = directory.listFiles();
        File[] fileEpub = directoryEpub.listFiles();

        if (filePdf!=null){
            Log.e("Files", "Size: "+ filePdf.length);
            for (int i = 0; i < filePdf.length; i++)
            {
                Log.e("Files", "FileName:" + filePdf[i].getName());
                Log.e("Files", "getPath:" + filePdf[i].getPath());
                chapters.add(DataStoreManager.getInfoChapterInPath(filePdf[i].getPath()));
            }
        }

        if (fileEpub!=null){
            for (int i = 0; i < fileEpub.length; i++)
            {
                Log.e("Files", "FileName:" + fileEpub[i].getName());
                Log.e("Files", "getPath:" + fileEpub[i].getPath());
                chapters.add(DataStoreManager.getInfoChapterInPath(fileEpub[i].getPath()));
            }
        }

        Log.e("Files", "chapters:" + chapters.size());
        return chapters;
    }
}
