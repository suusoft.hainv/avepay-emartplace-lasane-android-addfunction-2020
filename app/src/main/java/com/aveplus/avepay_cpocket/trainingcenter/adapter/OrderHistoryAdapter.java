package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.OrderHistory;

import java.util.ArrayList;

public class OrderHistoryAdapter extends ArrayAdapter<OrderHistory> {

    private Context context;
    private ArrayList<OrderHistory> list;


    public OrderHistoryAdapter(Context context, int layoutID, ArrayList<OrderHistory> list) {
        super(context, layoutID);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHoler vh;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_order_history, null);
            vh = new ViewHoler();
            vh.tvOrderId = (TextView) rowView.findViewById(R.id.tvOrderId);
            vh.tvEmail = (TextView) rowView.findViewById(R.id.tvEmail);
            vh.tvDate = (TextView) rowView.findViewById(R.id.tvDate);
            vh.tvStatus = (TextView) rowView.findViewById(R.id.tvStatus);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }

        OrderHistory history = list.get(position);
        if (history != null) {
            vh.tvOrderId.setText(history.getOrderID());
            vh.tvEmail.setText(history.getEmail());
            vh.tvDate.setText(history.getDate());
            if (history.getStatus().equals(OrderHistory.ORDER_STATUS_BOOKED)) {
                vh.tvStatus.setText("BOOKED");
            } else if (history.getStatus().equals(OrderHistory.ORDER_STATUS_CANCEL)) {
                vh.tvStatus.setText("CANCEL");
            } else if (history.getStatus().equals(OrderHistory.ORDER_STATUS_REJECT)) {
                vh.tvStatus.setText("REJECT");
            } else if (history.getStatus().equals(OrderHistory.ORDER_STATUS_NEW)) {
                vh.tvStatus.setText("NEW");
            } else if (history.getStatus().equals(OrderHistory.ORDER_STATUS_IN_PROCESS)) {
                vh.tvStatus.setText("IN PROCESS");
            } else if (history.getStatus().equals(OrderHistory.ORDER_STATUS_CLOSED)) {
                vh.tvStatus.setText("CLOSED");
            }
        }
        return rowView;
    }

    class ViewHoler {
        TextView tvOrderId, tvEmail, tvDate, tvStatus;
    }
}
