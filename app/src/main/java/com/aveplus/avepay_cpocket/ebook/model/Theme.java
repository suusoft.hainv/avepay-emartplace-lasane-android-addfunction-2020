package com.aveplus.avepay_cpocket.ebook.model;

import com.google.gson.Gson;

public class Theme {

    private String name;

    private String colorPrimary;
    private String colorPrimaryDark;
    private String colorAccent;
    private String backgroundMainDark;
    private String textColorPrimaryDark;
    private String textColorSecondaryDark;
    private String backgroundMainLight;
    private String textColorPrimaryLight;
    private String textColorSecondaryLight;

    public Theme() {
    }

    public Theme(String name, String colorPrimary, String colorPrimaryDark, String colorAccent,
                 String backgroundMainDark, String textColorPrimaryDark, String textColorSecondaryDark,
                 String backgroundMainLight, String textColorPrimaryLight, String textColorSecondaryLight) {
        this.name = name;
        this.colorPrimary = colorPrimary;
        this.colorPrimaryDark = colorPrimaryDark;
        this.colorAccent = colorAccent;
        this.backgroundMainDark = backgroundMainDark;
        this.textColorPrimaryDark = textColorPrimaryDark;
        this.textColorSecondaryDark = textColorSecondaryDark;
        this.backgroundMainLight = backgroundMainLight;
        this.textColorPrimaryLight = textColorPrimaryLight;
        this.textColorSecondaryLight = textColorSecondaryLight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColorPrimary() {
        return colorPrimary;
    }

    public void setColorPrimary(String colorPrimary) {
        this.colorPrimary = colorPrimary;
    }

    public String getColorPrimaryDark() {
        return colorPrimaryDark;
    }

    public void setColorPrimaryDark(String colorPrimaryDark) {
        this.colorPrimaryDark = colorPrimaryDark;
    }

    public String getColorAccent() {
        return colorAccent;
    }

    public void setColorAccent(String colorAccent) {
        this.colorAccent = colorAccent;
    }

    public String getBackgroundMainDark() {
        return backgroundMainDark;
    }

    public void setBackgroundMainDark(String backgroundMainDark) {
        this.backgroundMainDark = backgroundMainDark;
    }

    public String getTextColorPrimaryDark() {
        return textColorPrimaryDark;
    }

    public void setTextColorPrimaryDark(String textColorPrimaryDark) {
        this.textColorPrimaryDark = textColorPrimaryDark;
    }

    public String getTextColorSecondaryDark() {
        return textColorSecondaryDark;
    }

    public void setTextColorSecondaryDark(String textColorSecondaryDark) {
        this.textColorSecondaryDark = textColorSecondaryDark;
    }

    public String getBackgroundMainLight() {
        return backgroundMainLight;
    }

    public void setBackgroundMainLight(String backgroundMainLight) {
        this.backgroundMainLight = backgroundMainLight;
    }

    public String getTextColorPrimaryLight() {
        return textColorPrimaryLight;
    }

    public void setTextColorPrimaryLight(String textColorPrimaryLight) {
        this.textColorPrimaryLight = textColorPrimaryLight;
    }

    public String getTextColorSecondaryLight() {
        return textColorSecondaryLight;
    }

    public void setTextColorSecondaryLight(String textColorSecondaryLight) {
        this.textColorSecondaryLight = textColorSecondaryLight;
    }

    public String toJSon(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
