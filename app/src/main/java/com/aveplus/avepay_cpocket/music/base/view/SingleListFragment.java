package com.aveplus.avepay_cpocket.music.base.view;

import android.content.Context;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class SingleListFragment extends BaseListFragmentBinding {
    private static final String KEY_LAYOUT_INT = "LAYOUT_INT";
    private static final String KEY_LAYOUT_LIST = "LAYOUT_LIST";
    private static final String KEY_CLASS_VM = "CLASS_VM";
    private static final String KEY_CLASS_ITEM_VM = "CLASS_ITEM_VM";
    private static final String KEY_CLASS_ADAPTER = "KEY_CLASS_ADAPTER";
    private static final String KEY_BUNDLE = "KEY_BUNDLE";

    public int layout, layoutItem, layoutList;
    public BaseViewModelList viewModel;
    private Constructor<?> cons, consAdapterList;
    private Bundle bundle;

    private Class itemViewModel;
    public BaseAdapterBinding adapter;

    /**
     * this constructor is using #link{@SingleAdapter}
     * @param viewModelClass view model class for this
     * @param layoutitem layout of adapter
     * @param itemVm view model of adapter
     */
    public static SingleListFragment newInstance(Class viewModelClass, int layoutitem, Class itemVm){
        return newInstance(viewModelClass, SingleAdapter.class, layoutitem, itemVm, 0,null);
    }

    public static SingleListFragment newInstance(Class viewModelClass, int layoutList, int layoutitem, Class itemVm){
        return newInstance(viewModelClass, SingleAdapter.class, layoutitem, itemVm, layoutList,null);
    }

    public static SingleListFragment newInstance(Class viewModelClass, int layoutitem, Class itemVm, Bundle bundle ){
        return newInstance(viewModelClass, SingleAdapter.class, layoutitem, itemVm, 0,bundle);
    }

    /**
     * this constructor is using for other adapter
     * @param viewModelClass view model class for this
     * @param adapter adapter class need to be use
     */
    public static SingleListFragment newInstance(Class viewModelClass, Class adapter){
        return newInstance(viewModelClass, adapter, 0, null, 0,null);
    }

    /**
     *  this constructor is using an adapter
     *  @param viewModelClass view model class for this
     *  @param adapter adapter class
     *  @param layoutitem layout of adapter
     *  @param itemVm view model of adapter
     */
    public static SingleListFragment newInstance(Class viewModelClass, Class adapter, int layoutitem, Class itemVm, int layoutList , Bundle bundle) {

        Bundle args = new Bundle();
        SingleListFragment fragment = new SingleListFragment();
        args.putInt(KEY_LAYOUT_INT,layoutitem);
        args.putString(KEY_CLASS_VM,viewModelClass.getName());
        args.putString(KEY_CLASS_ADAPTER,adapter.getName());
        args.putInt(KEY_LAYOUT_LIST,layoutList);

        if (bundle != null)
            args.putBundle(KEY_BUNDLE, bundle);

        if (itemVm != null)
            args.putString(KEY_CLASS_ITEM_VM,itemVm.getName());

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        if (layoutList == 0)
            return super.getLayoutInflate();
        return layoutList;
    }

    @Override
    protected void initialize() {
        try {
            Bundle bundle = getArguments();
            layoutItem = bundle.getInt(KEY_LAYOUT_INT);
            layoutList = bundle.getInt(KEY_LAYOUT_LIST);
            Class viewModelClass = Class.forName(bundle.getString(KEY_CLASS_VM));
            Class adapter = Class.forName(bundle.getString(KEY_CLASS_ADAPTER));
            if (bundle.containsKey(KEY_CLASS_ITEM_VM))
                itemViewModel = Class.forName(bundle.getString(KEY_CLASS_ITEM_VM));
            if (bundle.containsKey(KEY_BUNDLE)) {
                this.bundle = bundle.getBundle(KEY_BUNDLE);
                cons = viewModelClass.getConstructor(Context.class, Bundle.class);
            }else{
                cons = viewModelClass.getConstructor(Context.class);
            }

            if (layoutItem == 0) {
                consAdapterList = adapter.getConstructor(Context.class, List.class);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        try {
            if (bundle != null){
                viewModel = (BaseViewModelList) cons.newInstance(self, bundle);
            }else {
                viewModel = (BaseViewModelList) cons.newInstance(self);
            }
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return viewModel;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        if (layoutItem != 0) {
            adapter = new SingleAdapter(self, layoutItem , viewModel.getListData(), itemViewModel);
            adapter.setListener(baseListener != null? baseListener : viewModel.getActionListener());
        }else{
            try {
                adapter = (BaseAdapterBinding) consAdapterList.newInstance(self, viewModel.getListData());
            } catch (java.lang.InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(viewModel.getOnScrollListener());
    }

}
