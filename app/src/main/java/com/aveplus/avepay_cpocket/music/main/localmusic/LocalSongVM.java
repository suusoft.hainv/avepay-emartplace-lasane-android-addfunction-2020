package com.aveplus.avepay_cpocket.music.main.localmusic;

import android.content.Context;
import android.os.AsyncTask;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.network.ListenerLoading;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.FileUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class LocalSongVM extends BaseViewModelList implements ItemMenuActionSongListener {

    private ArrayList<Song> list;

    public LocalSongVM(Context context, ListenerLoading listenerLoading) {
        super(context);
        this.listenerLoading = listenerLoading;
    }

    @Override
    public void getData(int page) {
        listenerLoading.onLoadingIsProcessing();
        new AsyncTask<String, String, ArrayList<Song>>() {

            @Override
            protected ArrayList<Song> doInBackground(String... strings) {
                list = LocalContentWrapper.getAllSongs(self);
                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Song> list) {
                super.onPostExecute(list);
                addListData(list);
                checkLoadingMoreComplete(1);
                listenerLoading.onLoadingIsCompleted();

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().addNewQueue(list, position);//list.get(position)
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(final int position) {
        DialogUtil.showAlertDialog(self, R.string.confirm_delete, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                if (FileUtil.deleteFile(list.get(position).getSong_file())) {
                    list.remove(position);
                    mDatas.remove(position);
                    notifyDataChanged(false);
                    AppUtil.showToast(self, R.string.deleted);
                } else {
                    AppUtil.showToast(self, R.string.cannot_remove_this_song);
                }
            }

            @Override
            public void onClickCancel() {

            }
        });
    }
}
