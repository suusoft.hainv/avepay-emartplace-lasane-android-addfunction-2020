package com.aveplus.avepay_cpocket.ebook.viewmodel.item;

import android.content.Context;

import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModelAdapter;
import com.aveplus.avepay_cpocket.ebook.model.Actor;


public class ItemActorVM extends BaseViewModelAdapter {

    private Actor actor;

    public ItemActorVM(Context context, Actor actor) {
        super(context);
        this.actor = actor;
    }

    public String getName() {
        return actor.getName();
    }

    public String getImage() {
        return actor.getImage();
    }

    @Override
    public void setData(Object object) {
        this.actor = (Actor) object;
        notifyChange();
    }
}
