package com.aveplus.avepay_cpocket.restaurant.main.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHoler> {
    public List<DetailOrder> details;
    private Context context;
    private ArrayList<Detail> detailArrayList;
    private OnItemClickListener listener;

    public HistoryAdapter(List<DetailOrder> details, Context context, ArrayList<Detail> detailArrayList, OnItemClickListener listener) {
        this.details = details;
        this.context = context;
        this.detailArrayList = detailArrayList;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(DetailOrder detailOrder, int i);
    }

    @NonNull
    @Override
    public ViewHoler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_history, viewGroup, false);
        return new ViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoler viewHoler, int i) {
        final DetailOrder order = details.get(i);
        //viewHoler.tvName.setText(detailArrayList.get(i).getRestaurantInfo().getProData().getBusinessName());
        //viewHoler.tvRestaurant.setText(detailArrayList.get(i).getRestaurantInfo().getProData().getBusinessAddress());
        viewHoler.tvPrice.setText(details.get(i).getTotal() + "$");
        viewHoler.tvDate.setText(details.get(i).getCreateDate());
        Glide.with(context).load(detailArrayList.get(i).getRestaurantInfo().getAttachment()).into(viewHoler.imgFood);
        viewHoler.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onItemClick(order, i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return details == null ? 0 : details.size();
    }

    public class ViewHoler extends RecyclerView.ViewHolder {
        private TextView tvName, tvRestaurant, tvQuantity, tvPrice, tvStatus, tvDate;
        private ImageView imgFood, imgDelete;
        private LinearLayout layout;

        public ViewHoler(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_name);
            tvRestaurant = itemView.findViewById(R.id.tv_address);
            tvQuantity = itemView.findViewById(R.id.tv_amount);
            imgFood = itemView.findViewById(R.id.img_cart_item);
            tvStatus = itemView.findViewById(R.id.tv_amount);
            layout = itemView.findViewById(R.id.card_view_cart);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvPrice = itemView.findViewById(R.id.tv_price);

        }

    }

}
