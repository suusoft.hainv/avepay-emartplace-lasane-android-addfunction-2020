package com.aveplus.avepay_cpocket.movie.movie.main.impl;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface OnFocusClickMovie {
    public void onFocusMovie(int position, boolean hasfocus);
    public void onClickMovie(int position);
    public void onNextPageMovie(int position);
    public void onPrevMovie(int position);
}
