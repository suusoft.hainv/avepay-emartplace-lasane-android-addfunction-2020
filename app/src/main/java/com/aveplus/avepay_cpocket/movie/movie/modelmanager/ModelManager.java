package com.aveplus.avepay_cpocket.movie.movie.modelmanager;

import android.content.Context;
import android.net.Uri;

import com.android.volley.toolbox.Volley;
import com.aveplus.avepay_cpocket.interfaces.IResponse;
import com.aveplus.avepay_cpocket.movie.movie.configs.Apis;
import com.aveplus.avepay_cpocket.network1.VolleyGet;


public class ModelManager {
    public static void registerDevice(Context ctx, String gcmId, final ModelManagerListener listener){
        String url = Apis.URL_REGISTER_DEVICE;
        Uri.Builder builder = Uri.parse(url).buildUpon();
        builder.appendQueryParameter("gcm_id", gcmId);
        builder.appendQueryParameter("type", "1");
        builder.appendQueryParameter("status", "1");

        new VolleyGet(ctx, true, false).getJSONObject(builder, Volley.newRequestQueue(ctx), new IResponse() {
            @Override
            public void onResponse(Object response) {
                if (response != null) {
                    listener.onSuccess(response);
                } else {
                    listener.onError();
                }
            }
        });
    }
}
