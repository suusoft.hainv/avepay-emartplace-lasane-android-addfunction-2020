package com.aveplus.avepay_cpocket.trainingcenter.object;

public class Cart {
	String id;
	String nameCart;
	String image;
	String price;
	String total;
	String categoryID;
	private String productId;
	private String CARTID;

	public Cart(String id, String nameCart, String image, String price,
                String total, String productId, String cARTID, String categoryID) {
		super();
		this.id = id;
		this.nameCart = nameCart;
		this.image = image;
		this.price = price;
		this.total = total;
		this.productId = productId;
		this.categoryID = categoryID;
		CARTID = cARTID;
	}

	public String getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}

	public Cart() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNameCart() {
		return nameCart;
	}

	public void setNameCart(String nameCart) {
		this.nameCart = nameCart;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCARTID() {
		return CARTID;
	}

	public void setCARTID(String cARTID) {
		CARTID = cARTID;
	}

}
