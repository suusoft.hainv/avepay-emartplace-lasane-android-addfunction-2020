package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.Manifest;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.interfaces.IConfirmation;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.book.ServiceObj;
import com.aveplus.avepay_cpocket.taxi.retrofit.APIService;
import com.aveplus.avepay_cpocket.taxi.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.taxi.utils.ErrorUtils;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.google.android.material.snackbar.Snackbar;


public abstract class BaseActivity extends AppCompatActivity implements BaseRequest.ListenerLoading {

    private static final String TAG = BaseActivity.class.getSimpleName();

    protected BaseActivity self;
    public ProgressBar progressBar;
    protected APIService mAPIService;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAPIService = ApiUtils.getAPIServiceTypeTaxi();
        inflateLayout();
        self = this;
        progressBar = findViewById(R.id.progress);
        getExtraValues();
        BaseRequest.getInstance().setListenerLoading(this);

        initUI();
        initControl();
    }

    abstract void inflateLayout();

    abstract void initUI();

    abstract void initControl();


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * show hide progress bar on screen
     *
     * @param isShow true is show
     */
    public void showProgress(boolean isShow) {
        if (progressBar != null)
            if (isShow) {
                if (!progressBar.isShown())
                    progressBar.setVisibility(View.VISIBLE);
            } else {
                if (progressBar.isShown())
                    progressBar.setVisibility(View.GONE);
            }
    }

    protected void onSessionCreated() {
    }

    protected void getExtraValues() {
    }

    protected void showPermissionsReminder(final int reqCode, final boolean flag) {
        GlobalFunctions.showConfirmationDialog(self, getString(R.string.msg_remind_user_grants_permissions),
                getString(R.string.allow), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        GlobalFunctions.isGranted(self, new String[]{Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.ACCESS_FINE_LOCATION}, reqCode, null);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            finish();
                        }
                    }
                });
    }

    public void turnOnLocationReminder(final int reqCode, final boolean flag) {
        GlobalFunctions.showConfirmationDialog(self, getString(R.string.msg_remind_user_turn_on_location),
                getString(R.string.turn_on), getString(R.string.no_thank), false, new IConfirmation() {
                    @Override
                    public void onPositive() {
                        MapsUtil.displayLocationSettingsRequest(self, reqCode);
                    }

                    @Override
                    public void onNegative() {
                        if (flag) {
                            finish();
                        }
                    }
                });
    }

    protected int getTransportIcon(TripObj tripObj) {
        int icon = R.drawable.ic_taxi_type;
        if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.TAXI)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_taxi_delivery_type;
            } else {
                icon = R.drawable.ic_taxi_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.VIP)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_vip_delivery_type;
            } else {
                icon = R.drawable.ic_vip_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.LIFTS)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_lifts_delivery_type;
            } else {
                icon = R.drawable.ic_lifts_type;
            }
        } else if (tripObj.getTransportType().equalsIgnoreCase(ServiceObj.MOTORBIKE)) {
            if (tripObj.driverIsDelivery()) {
                icon = R.drawable.ic_moto_delivery_type;
            } else {
                icon = R.drawable.ic_moto_type;
            }
        }

        return icon;
    }

    protected Snackbar showErrorSnackbar(@StringRes int resId, View view, Exception e,
                                         View.OnClickListener clickListener) {
        return ErrorUtils.showSnackbar(view, resId, e, R.string.retry, clickListener);
    }

    @Override
    public void onLoadingIsCompleted() {
        showProgress(false);
    }

    @Override
    public void onLoadingIsProcessing() {
        showProgress(true);
    }


}
