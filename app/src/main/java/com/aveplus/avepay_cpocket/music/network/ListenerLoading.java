package com.aveplus.avepay_cpocket.music.network;


import com.aveplus.avepay_cpocket.music.listener.IBaseListener;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public interface ListenerLoading extends IBaseListener {
    void onLoadingIsProcessing();
    void onLoadingIsCompleted();
}
