package com.aveplus.avepay_cpocket.restaurant.listener;

/**
 * Created by Trang on 7/7/2016.
 */
public interface IOnClickListener {

    public void onClick(int position);
}
