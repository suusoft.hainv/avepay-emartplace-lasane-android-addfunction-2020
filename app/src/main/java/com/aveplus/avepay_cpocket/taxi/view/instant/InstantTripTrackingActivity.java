package com.aveplus.avepay_cpocket.taxi.view.instant;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.configs.Action;
import com.aveplus.avepay_cpocket.taxi.configs.SizeScreen;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.taxi.network.ApiResponse;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.InfoReceive;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.objects.orther.DirectionObject;
import com.aveplus.avepay_cpocket.taxi.objects.orther.GsonRequest;
import com.aveplus.avepay_cpocket.taxi.objects.orther.Helper;
import com.aveplus.avepay_cpocket.taxi.objects.orther.LegsObject;
import com.aveplus.avepay_cpocket.taxi.objects.orther.PolylineObject;
import com.aveplus.avepay_cpocket.taxi.objects.orther.RouteObject;
import com.aveplus.avepay_cpocket.taxi.objects.orther.StepsObject;
import com.aveplus.avepay_cpocket.taxi.objects.orther.VolleySingleton;
import com.aveplus.avepay_cpocket.taxi.utils.AppUtil;
import com.aveplus.avepay_cpocket.taxi.utils.CheckPermission;
import com.aveplus.avepay_cpocket.taxi.utils.DialogUtil;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


import java.util.ArrayList;
import java.util.List;

public class InstantTripTrackingActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, GoogleMap.OnMapClickListener {

    public static final int RQ_TRACKING = 1000;
    public static final int RS_CANCEL_TRIP = 1001;
    private static final int PERMISSION_LOCATION_REQUEST_CODE = 100;
    private String TAG = InstantTripTrackingActivity.class.getSimpleName();
    private GoogleMap mMap;

    // Use google api to get current location
    private GoogleApiClient mGoogleApiClient;
    protected Location mMyLocation;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private double latitudeValue = 0.0;
    private double longitudeValue = 0.0;

    private Marker mDriverMarker, mFromMarker, mToMarker;
    private ArrayList<LatLng> latLngList ;

    private ArrayList<MarkerOptions>  listMarkerOptions;

    public TripObj tripObj;
    public InfoReceive infoReceive;
    private TextView btnCancel, btnDetail, tvWaitingMin, tvNotif, tvInformation, btnComplete, tvEstimateFare;
    private RelativeLayout loProgressBar;
    private MarkerOptions pickUpOption, destinationOption, driverOption;

    private View frgDetailTrip;
    private Bundle bundle;

    private ReceiverTripAction receiverTripAction;
    private IntentFilter intentFilter;


    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_instant_tracking_driver;
    }

    @Override
    protected void getExtraData(Intent intent) {

        bundle = intent.getExtras();
        if (bundle!=null){
            if (bundle.containsKey(Args.KEY_TRIP_OJBECT)){
                tripObj = intent.getExtras().getParcelable(Args.KEY_TRIP_OJBECT);

                if (bundle.containsKey(Args.KEY_INFO_RECEIVE)){
                    infoReceive = intent.getExtras().getParcelable(Args.KEY_INFO_RECEIVE);
                    tripObj.setId(infoReceive.trip_id);
                    tripObj.setEstimate_duration(Integer.parseInt(infoReceive.duration));
                    tripObj.setLatLngDriver(new LatLng(Double.parseDouble(infoReceive.lat),Double.parseDouble(infoReceive.lng) ));
                }
            }

        }

       // Log.e(TAG, "trip: " + new Gson().toJson(tripObj));

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fr_detail_trip, DetailTripFragment.newInstance()).commit();

    }

    private void bindData() {
        if (tripObj!=null){
            if (tripObj.getDriverFinished()!=null){
                if (tripObj.getDriverFinished().equals("1")){
                    showViewWhileDriverFinished();
                }else
                    showViewWhileDriverProcessing();
            }


        }
    }

    @Override
    protected void initilize() {


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initLocationListener();
    }

    @Override
    protected void initView() {
        SizeScreen.init(this);
        receiverTripAction = new ReceiverTripAction();
        intentFilter = new IntentFilter(Action.ACTION_FINISHED);

        latLngList = new ArrayList<>();
        listMarkerOptions = new ArrayList<>();
        initGoogleApiClient();

        initButton();

        bindData();
    }

    private void initButton() {
        frgDetailTrip = findViewById(R.id.fragment);

        btnCancel = findViewById(R.id.btn_cancel);
        btnComplete = findViewById(R.id.btn_complete);
        btnDetail = findViewById(R.id.btn_detail);
        tvWaitingMin = findViewById(R.id.tv_waiting_min);
        tvInformation = findViewById(R.id.tv_information);
        tvEstimateFare = findViewById(R.id.tv_estimate_fare);
        tvNotif = findViewById(R.id.tv_notif);
        loProgressBar = findViewById(R.id.lo_loading);

        //tvWaitingMin.setText(tripObj.getEstimate_duration()/1000+ " min");
        //tvWaitingMin.setText(tripObj.getDriverObj().getDuration()+ " s");
        //showViewWhileDriverProcessing();
        tvInformation.setText((double) (tripObj.getEstimateDistance()/1000) + " Km"  );
        tvEstimateFare.setText( StringUtil.convertNumberToPrice(tripObj.getEstimateFare() ));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogCancel();

            }
        });

        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putParcelable(Args.KEY_TRIP_OJBECT, tripObj);
                AppUtil.startActivityForResult(InstantTripTrackingActivity.this, CompleteAndRateActivity.class, bundle, CompleteAndRateActivity.RQ_RATING);

            }
        });


        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailTrip();
            }
        });
    }

    private void showDialogCancel() {
        DialogUtil.showAlertDialog(self, getString(R.string.cancel_trip), R.string.do_you_want_to_cancel_trip, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                onCancel();
            }
        });
    }


    private void showDetailTrip() {
        frgDetailTrip.setVisibility(View.VISIBLE);
    }


    public void hideDetailTrip(){
        frgDetailTrip.setVisibility(View.GONE);
    }

    private void onCancel() {
        //request cancel to server
        loProgressBar.setVisibility(View.VISIBLE);
        tvNotif.setText(R.string.cancel_trip);

        RequestManager.instantTripAction(Action.ACTION_CANCEL, tripObj.getId(),"", new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setResult(RS_CANCEL_TRIP);
                        finish();
                    }
                }, 1000);

            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });



    }

    @Override
    protected void onViewCreated() {

    }

    private void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    // listener

    @Override
    public void onConnected(@Nullable Bundle bundle) {

//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
//        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
//        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//
//            @Override
//            public void onResult(@NonNull LocationSettingsResult result) {
//            final Status status = result.getStatus();
//            switch (status.getStatusCode()) {
//                case LocationSettingsStatusCodes.SUCCESS:
//                    Log.d(TAG, "Connection method has been called");
//
//                if (ActivityCompat.checkSelfPermission(getApplicationContext(),
//                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                        && ActivityCompat.checkSelfPermission(getApplicationContext(),
//                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//
//                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//                    assignLocationValues(mLastLocation);
//                    setDefaultMarkerOption(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
//                }else{
//                    ActivityCompat.requestPermissions(TrackingDriverActivity.this,
//                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
//                                    Manifest.permission.ACCESS_FINE_LOCATION},
//                            PERMISSION_LOCATION_REQUEST_CODE);
//                }
//                break;
//
//                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                    break;
//            }
//            }
//        });


//        if (GlobalFunctions.locationIsGranted(this, RC_LOCATION, null)) {
//            if (MapsUtil.locationIsEnable(self)) {
//                mMyLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//                initMyLocation();
//            } else {
//                MapsUtil.displayLocationSettingsRequest(this, RC_TURN_ON_LOCATION);
//            }
//        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                MapsUtil.moveCameraShowAllLatLng(mMap, tripObj.getLatLngPickup(), tripObj.getLatLngDestination());
            }
        });

        pickUpOption = new MarkerOptions();
        pickUpOption.position(tripObj.getLatLngPickup())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_from));

        destinationOption = new MarkerOptions();
        destinationOption.position(tripObj.getLatLngDestination())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_to));

        driverOption = new MarkerOptions();

        if ( tripObj.getLatLngDriver()==null)
            tripObj.setLatLngDriver(tripObj.getLatLngPickup());

        driverOption.position(tripObj.getLatLngDriver())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car));

        mFromMarker =  mMap.addMarker(pickUpOption);
        mToMarker =  mMap.addMarker(destinationOption);
        mDriverMarker =  mMap.addMarker(driverOption);

        markStartingLocationOnMap(mMap, pickUpOption.getPosition());

        drawPoline(pickUpOption.getPosition(), driverOption.getPosition(), Color.GREEN);

        drawPoline(pickUpOption.getPosition(), destinationOption.getPosition(), Color.BLUE);




        //set click in map
        //mMap.setOnMapClickListener(this);
    }


    private void updateLocationAllUser(Location location){
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

       // mFromMarker.setPosition(latLng);
        mDriverMarker.setPosition(latLng);

        //drawPoline(mDriverMarker.getPosition(), destinationOption.getPosition(), Color.BLUE);
    }



    @Override  
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) { 
        switch (requestCode) {       
            case PERMISSION_LOCATION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // permission was denied, show alert to explain permission
                    showPermissionAlert();
                }else{                     //permission is granted now start a background service

                    if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        assignLocationValues(mLastLocation);
                        setDefaultMarkerOption(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())); }

                }
            }

            //request permission location
            case  RC_PERMISSIONS:{
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        showPermissionsReminder(RC_PERMISSIONS, true);
                    } else {
                        //permission đã được cấp phép
                        sendLocationToServer();
                    }
                }
                break;
            }
        }
    }

    private void assignLocationValues(Location currentLocation){
        if (currentLocation != null) {        
            
            latitudeValue = currentLocation.getLatitude();
            longitudeValue = currentLocation.getLongitude();
        Log.d(TAG, "Latitude: " + latitudeValue + " Longitude: " + longitudeValue);
        markStartingLocationOnMap(mMap, new LatLng(latitudeValue, longitudeValue));
        addCameraToMap(new LatLng(latitudeValue, longitudeValue));
        }
    }


    private void showPermissionAlert(){     
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.permission_request_title);
        builder.setMessage(R.string.msg_remind_user_grants_permissions);
        builder.create();
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {      
            @Override   
            public void onClick(DialogInterface dialog, int which) {   
                if (ActivityCompat.checkSelfPermission(
                        self, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED                   
                        && ActivityCompat.checkSelfPermission(InstantTripTrackingActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {  
                    ActivityCompat.requestPermissions(InstantTripTrackingActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_FINE_LOCATION},
                            PERMISSION_LOCATION_REQUEST_CODE);
        }             }    
        });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {   
                @Override           
                public void onClick(DialogInterface dialog, int which) {       
                    Toast.makeText(self, R.string.permission_refused, Toast.LENGTH_LONG).show();
            }         });
        builder.show();
    }


    private void addCameraToMap(LatLng latLng){
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(16)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void markStartingLocationOnMap(GoogleMap mapObject, LatLng location){
        mapObject.addMarker(new MarkerOptions().position(location).title("Current location"));
        mapObject.moveCamera(CameraUpdateFactory.newLatLng(location));
    }

    private void refreshMap(GoogleMap mapInstance){
        mapInstance.clear();
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    private void setDefaultMarkerOption(LatLng location){
        if(pickUpOption == null){
            pickUpOption = new MarkerOptions();
        }
        pickUpOption.position(location);
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void getDirectionFromDirectionApiServer(String url){
        GsonRequest<DirectionObject> serverRequest = new GsonRequest<DirectionObject>(
                Request.Method.GET,
                url,
                DirectionObject.class,
                createRequestSuccessListener(),
                createRequestErrorListener());
        serverRequest.setRetryPolicy(new DefaultRetryPolicy(
                Helper.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(serverRequest);
    }

    private void getDirectionFromDirectionApiServer(String url, int color){
        GsonRequest<DirectionObject> serverRequest = new GsonRequest<DirectionObject>(
                Request.Method.GET,
                url,
                DirectionObject.class,
                createRequestSuccessListener(color),
                createRequestErrorListener());
        serverRequest.setRetryPolicy(new DefaultRetryPolicy(
                Helper.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(serverRequest);
    }

    private Response.Listener<DirectionObject> createRequestSuccessListener() {

        return new Response.Listener<DirectionObject>() {
            @Override
            public void onResponse(DirectionObject response) {
                try {
                    Log.d("JSON Response", response.toString());
                    if(response.getStatus().equals("OK")){
                        List<LatLng> mDirections = getDirectionPolylines(response.getRoutes());
                        drawRouteOnMap(mMap, mDirections);
                    }else{
                        //Toast.makeText(self, getString( R.string.connection_error), Toast.LENGTH_SHORT).show();
                        Log.e(TAG, getString(R.string.connection_error) );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                 }
            }
        };
    }

    private Response.Listener<DirectionObject> createRequestSuccessListener(final int color) {

        return new Response.Listener<DirectionObject>() {
            @Override
            public void onResponse(DirectionObject response) {
                try {
                    Log.d("JSON Response", response.toString());
                    if(response.getStatus().equals("OK")){
                        List<LatLng> mDirections = getDirectionPolylines(response.getRoutes());
                        drawRouteOnMap(mMap, mDirections, color);
                    }else{
                       // Toast.makeText(self, getString( R.string.connection_error), Toast.LENGTH_SHORT).show();
                        Log.e(TAG, getString(R.string.connection_error) );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private List<LatLng> getDirectionPolylines(List<RouteObject> routes){
        List<LatLng> directionList = new ArrayList<LatLng>();
        for(RouteObject route : routes){
            List<LegsObject> legs = route.getLegs();
            for(LegsObject leg : legs){
                List<StepsObject> steps = leg.getSteps();
                for(StepsObject step : steps){
                    PolylineObject polyline = step.getPolyline();
                    String points = polyline.getPoints();
                    List<LatLng> singlePolyline = decodePoly(points);
                    for (LatLng direction : singlePolyline){
                        directionList.add(direction);
                    }
                }
            }
        }

                    return directionList;
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    private void drawRouteOnMap(GoogleMap map, List<LatLng> positions){
        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
        options.addAll(positions);
        Polyline polyline = map.addPolyline(options);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(positions.get(1).latitude, positions.get(1).longitude))
                .zoom(17)
                .build();

        //animateCamera
        //map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//        listMarkerOptions.clear();
//        listMarkerOptions.add(pickUpOption);
//        listMarkerOptions.add(driverOption);
//        MapsUtil.moveCameraShowAllMarkerOptions(mMap, listMarkerOptions);

        MapsUtil.moveCameraShowAllLatLng(mMap, positions);

        Log.e("latlong", "size:" + positions.size());

    }

    private void drawRouteOnMap(GoogleMap map, List<LatLng> positions, int color){
        PolylineOptions options = new PolylineOptions().width(5).color(color).geodesic(true);
        options.addAll(positions);
        Polyline polyline = map.addPolyline(options);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(positions.get(1).latitude, positions.get(1).longitude))
                .zoom(17)
                .build();

        //animateCamera
        //map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//        listMarkerOptions.clear();
//        listMarkerOptions.add(pickUpOption);
//        listMarkerOptions.add(driverOption);
//        MapsUtil.moveCameraShowAllMarkerOptions(mMap, listMarkerOptions);
        MapsUtil.moveCameraShowAllLatLng(mMap, positions);

        Log.e("latlong", "size:" + positions.size());

    }


    //click map
    public void onMapClick(LatLng latLng) {
        if(latLngList.size() > 0){
            refreshMap(mMap);
            latLngList.clear();
        }

        latLngList.add(latLng);
        Log.d(TAG, "Marker number " + latLngList.size());

        listMarkerOptions.clear();
        listMarkerOptions.add(pickUpOption);
        driverOption = new MarkerOptions().position(latLng);

        mMap.addMarker(pickUpOption);
        mMap.addMarker(driverOption);

        LatLng defaultLocation = pickUpOption.getPosition();
        LatLng destinationLocation = latLng;
        //use Google Direction API to get the route between these Locations

        String directionApiPath = Helper.getUrl(String.valueOf(defaultLocation.latitude), String.valueOf(defaultLocation.longitude),
                String.valueOf(destinationLocation.latitude), String.valueOf(destinationLocation.longitude));
        Log.d(TAG, "Path " + directionApiPath);
        getDirectionFromDirectionApiServer(directionApiPath);
    }


    private void drawPoline(LatLng latLngFrom, LatLng latLngTo){
        String directionApiPath = Helper.getUrl(String.valueOf(latLngFrom.latitude), String.valueOf(latLngFrom.longitude),
                String.valueOf(latLngTo.latitude), String.valueOf(latLngTo.longitude));
        Log.d(TAG, "Path " + directionApiPath);
        getDirectionFromDirectionApiServer(directionApiPath);
    }

    private void drawPoline(LatLng latLngFrom, LatLng latLngTo, int color){
        String directionApiPath = Helper.getUrl(String.valueOf(latLngFrom.latitude), String.valueOf(latLngFrom.longitude),
                String.valueOf(latLngTo.latitude), String.valueOf(latLngTo.longitude));
        Log.d(TAG, "Path " + directionApiPath);
        getDirectionFromDirectionApiServer(directionApiPath, color);
    }



    /**      * Method to decode polyline points
     *  * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     *  * */


    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            LatLng p = new LatLng((((double) lat / 1E5)), (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.onResume();
        registerReceiver(receiverTripAction, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.onPause();
        unregisterReceiver(receiverTripAction);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case CompleteAndRateActivity.RQ_RATING:
                if (resultCode==CompleteAndRateActivity.RS_RATING_SUCCESS){
                    setResult(CompleteAndRateActivity.RS_RATING_SUCCESS);
                    finish();
                }


                break;
        }

    }


    /// listener location

    private final int RC_PERMISSIONS = 10;
    private void initLocationListener() {
        if (CheckPermission.isGranted(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                RC_PERMISSIONS,
                "")) {


            sendLocationToServer();
        }
    }


    private LocationManager mLocationManager;
    @SuppressLint("MissingPermission")
    private void sendLocationToServer() {
        mLocationManager = (LocationManager)getSystemService(LOCATION_SERVICE);


        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                0, mLocationListener);
    }


    private Location locationOld;
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            Log.e("location","onLocationChanged");
            if (locationOld!=null)
                Log.e("location","locationOld " + locationOld.getLatitude()  + "  " + locationOld.getLongitude() );
            Log.e("location","location " + location.getLatitude()  + "  " + location.getLongitude() );

            //if(isPointChanged(locationOld, location)){
            if(isPointChanged(locationOld, location)){
                updateLocationAllUser(location);
                Log.e("location","onLocationChanged true");
                locationOld = location;
            }


        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

            Log.e("location","onStatusChanged");
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e("location","onProviderEnabled");

        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e("location","onProviderDisabled");

        }
    };


    private boolean isPointChanged(Location lOld, Location lNew){
        boolean isChange = false;

        if (lOld==null)
            return true;

        if (isAxisChanged(lOld.getLatitude(), lNew.getLatitude())
                || isAxisChanged(lOld.getLongitude(), lNew.getLongitude())){
            isChange = true;
        }

        return isChange;

    }

    private boolean isAxisChanged(double toadoCu, double toadoMoi){
        boolean isChanged = false;

        if (getNumber(toadoCu)!= getNumber(toadoMoi))
            isChanged = true;

        Log.e("location", "toa do cu = "  + getNumber(toadoCu) + " toa do moi = " + getNumber(toadoMoi) );

        return isChanged;
    }

    private int getNumber (double d){
        int number =(int)(d*10000) ;

        return number;
    }


    public class ReceiverTripAction extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getAction()){

                case Action.ACTION_FINISHED:
                    Log.e("trip", "infoReceive: " + "ACTION_FINISH");

                    //AppUtil.showToast(self, "Looking for another driver...");

                    showViewWhileDriverFinished();

                    break;

            }
        }
    }


    private void showViewWhileDriverFinished(){
        tvWaitingMin.setVisibility(View.GONE);
        tvNotif.setText("The trip has completed.");
        btnCancel.setVisibility(View.GONE);
    }

    private void showViewWhileDriverProcessing(){

        tvWaitingMin.setText(StringUtil.converTimeToString((int) tripObj.getEstimate_duration()) /*/1000+ " min"*/);
    }
}
