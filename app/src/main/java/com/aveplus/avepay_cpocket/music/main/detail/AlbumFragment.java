package com.aveplus.avepay_cpocket.music.main.detail;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentNavBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.main.home.ItemSongsVM;
import com.aveplus.avepay_cpocket.music.model.Album;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class AlbumFragment extends BaseListFragmentNavBinding {
    private FloatingActionButton buttonPlay;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AlbumVM viewModel;
    private Album album;

    private Toolbar toolbar;

    public static AlbumFragment newInstance(Album object) {

        Bundle args = new Bundle();
        args.putParcelable(Constant.PREF_KEY_OBJECT, object);
        AlbumFragment fragment = new AlbumFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_album;

    }

    @Override
    protected void initialize() {
        album = getArguments().getParcelable(Constant.PREF_KEY_OBJECT);
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new AlbumVM(self, album);
        return viewModel;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        showHideActionMenu(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(new SingleAdapter(self, R.layout.item_song_album, viewModel.getListData(), ItemSongsVM.class, viewModel.getActionListener()));

    }

    @Override
    protected void initToolbar(View view) {
        super.initToolbar(view);
        AppUtil.hideSoftKeyboard((Activity) self);
        collapsingToolbarLayout = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(album.title);
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(self, android.R.color.transparent));
        buttonPlay = (FloatingActionButton) view.findViewById(R.id.buttonPlay);
        AppBarLayout appBarLayout = (AppBarLayout) view.findViewById(R.id.app_bar_layout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    buttonPlay.hide();
                } else {
                    buttonPlay.show();
                }
            }
        });
    }


}
