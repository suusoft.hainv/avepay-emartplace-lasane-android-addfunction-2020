package com.aveplus.avepay_cpocket.music.main.library;

import android.content.Context;
import android.view.View;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Global;

import org.greenrobot.eventbus.EventBus;

public class LibraryVM  extends BaseViewModel {
    public LibraryVM(Context self) {
        super(self);
    }
    public void onClickAlbum(View view){
        EventBus.getDefault().post(new Global.MyMusicItemClicked(Global.LocalItemTypeClick.TYPE_ARITST));

    }
    public void onClickSong(View view){
        EventBus.getDefault().post(new Global.MyMusicItemClicked(Global.LocalItemTypeClick.TYPE_SONG));

    }
    public void onClickFavorite(View view){
        EventBus.getDefault().post(new Global.MyMusicItemClicked(Global.LocalItemTypeClick.TYPE_FAVORITE));

    }
    public void onClickDownload(View view){
        EventBus.getDefault().post(new Global.MyMusicItemClicked(Global.LocalItemTypeClick.TYPE_DOWNLOADED));
    }
}
