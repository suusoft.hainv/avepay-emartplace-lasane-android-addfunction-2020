package com.aveplus.avepay_cpocket.music.main.payment;

import android.os.Parcel;
import android.os.Parcelable;

public class PackageSubscribe implements Parcelable {
    private int id, price,type,is_used,is_active;
    private String name, image,key_code,used_by,used_at,description,created_at,modified_at;

    public PackageSubscribe() {
    }

    public PackageSubscribe(int id, int price, int type, int is_used, int is_active, String name, String image, String key_code, String used_by, String used_at, String description, String created_at, String modified_at) {
        this.id = id;
        this.price = price;
        this.type = type;
        this.is_used = is_used;
        this.is_active = is_active;
        this.name = name;
        this.image = image;
        this.key_code = key_code;
        this.used_by = used_by;
        this.used_at = used_at;
        this.description = description;
        this.created_at = created_at;
        this.modified_at = modified_at;
    }

    protected PackageSubscribe(Parcel in) {
        id = in.readInt();
        price = in.readInt();
        type = in.readInt();
        is_used = in.readInt();
        is_active = in.readInt();
        name = in.readString();
        image = in.readString();
        key_code = in.readString();
        used_by = in.readString();
        used_at = in.readString();
        description = in.readString();
        created_at = in.readString();
        modified_at = in.readString();
    }

    public static final Creator<PackageSubscribe> CREATOR = new Creator<PackageSubscribe>() {
        @Override
        public PackageSubscribe createFromParcel(Parcel in) {
            return new PackageSubscribe(in);
        }

        @Override
        public PackageSubscribe[] newArray(int size) {
            return new PackageSubscribe[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIs_used() {
        return is_used;
    }

    public void setIs_used(int is_used) {
        this.is_used = is_used;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getKey_code() {
        return key_code;
    }

    public void setKey_code(String key_code) {
        this.key_code = key_code;
    }

    public String getUsed_by() {
        return used_by;
    }

    public void setUsed_by(String used_by) {
        this.used_by = used_by;
    }

    public String getUsed_at() {
        return used_at;
    }

    public void setUsed_at(String used_at) {
        this.used_at = used_at;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(price);
        parcel.writeInt(type);
        parcel.writeInt(is_used);
        parcel.writeInt(is_active);
        parcel.writeString(name);
        parcel.writeString(image);
        parcel.writeString(key_code);
        parcel.writeString(used_by);
        parcel.writeString(used_at);
        parcel.writeString(description);
        parcel.writeString(created_at);
        parcel.writeString(modified_at);
    }
}
