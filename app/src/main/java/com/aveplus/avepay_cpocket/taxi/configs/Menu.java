package com.aveplus.avepay_cpocket.taxi.configs;

public class Menu {

    public static int nav_instant_booking       = 0;
    public static int nav_booking_later         = 1;
    public static int nav_negotiated_booking    = 2;
    public static int nav_order                 = 3;
    public static int nav_order_history         = 4;
    public static int nav_chat                  = 5;
    public static int nav_pay                   = 6;
    public static int nav_become_driver         = 7;
    public static int nav_bonus                 = 8;
    public static int nav_profile               = 9;
    public static int nav_about_us              = 10;
    public static int nav_share                 = 11;
    public static int nav_faqs                  = 12;
    public static int nav_settings              = 13;
    public static int nav_help                  = 14;

}
