package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.activity.BlogDetailActivity;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;

public class NewEventAdapter extends BaseAdapter implements Filterable {
    private Activity context;
    private ArrayList<NewEvent> objects = new ArrayList<NewEvent>();
    private ArrayList<NewEvent> listFilter = new ArrayList<NewEvent>();
    private NewFilter mFilter = new NewFilter();
    private LayoutInflater inflater;
    int resource;

    public NewEventAdapter(Activity context, int resource, ArrayList<NewEvent> objects) {
        this.context = context;
        this.resource = resource;
        this.objects = objects;
        this.listFilter = objects;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public NewEvent getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {

        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_new_event, null);
            vh = new ViewHoler();
            vh.txtTitle = (TextView) rowView.findViewById(R.id.txtNewTitle);
            vh.txtDate = (TextView) rowView.findViewById(R.id.txtNewDate);
            vh.imgNew = (ImageView) rowView.findViewById(R.id.imageSaleOff);
            vh.btnReadMore = (TextView) rowView.findViewById(R.id.txtReadMore);
            vh.txtTitle.setSelected(true);
            vh.btnReadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //int i = (int) v.getTag();
                    onclickItem(position);

                }
            });
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onclickItem(position);
                }
            });
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        vh.btnReadMore.setTag(position);
        final NewEvent c = objects.get(position);
        if (c != null) {
            vh.txtTitle.setText(c.getTitle());
            vh.txtDate.setText(c.getDescription());
            Picasso.get().load(c.getImage()).into(vh.imgNew);
        }
        return rowView;
    }

    private void onclickItem(int position) {
        NewEvent item = objects.get(position);
        Bundle bundle = new Bundle();
        bundle.putString("id", item.getId());
        bundle.putString("title", item.getTitle());
        bundle.putString("description", item.getDescription());
        bundle.putString("content", item.getContent());
        bundle.putString("image", item.getImage());
        bundle.putString("time", item.getTime());
        bundle.putString("link", item.getLinkNews());
        Intent intent = new Intent(context, BlogDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("bundle", bundle);
        context.startActivity(intent);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    static class ViewHoler {
        public TextView txtTitle, txtDate;
        public ImageView imgNew;
        public TextView btnReadMore;
    }

    private class NewFilter extends Filter {

        @SuppressLint({"NewApi", "DefaultLocale"})
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            final ArrayList<NewEvent> list1 = listFilter;

            int count = list1.size();
            final ArrayList<NewEvent> nlist = new ArrayList<NewEvent>(count);
            if (!filterString.isEmpty()) {
                String filterableString;
                for (int i = 0; i < count; i++) {
                    filterableString = list1.get(i).getTitle();
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add(listFilter.get(i));
                    }
                }
                if (nlist.size() > 0) {
                    results.values = nlist;
                    results.count = nlist.size();
                } else {
                    results.values = listFilter;
                    results.count = listFilter.size();
                }
            } else {
                results.values = listFilter;
                results.count = listFilter.size();
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count == 0) {
                notifyDataSetInvalidated();
            } else {
                objects = (ArrayList<NewEvent>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}
