package com.aveplus.avepay_cpocket.restaurant.main.restaurant.order;
/**
 * Created by suusoft.com on 11/12/19.
 */
import com.aveplus.avepay_cpocket.restaurant.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderModel extends BaseModel {

    @SerializedName("type_product")
    @Expose
    private String type_product;
    @SerializedName("billingName")
    @Expose
    private String billingName;
    @SerializedName("billingAddress")
    @Expose
    private String billingAddress;
    @SerializedName("billingPhone")
    @Expose
    private String billingPhone;
    @SerializedName("billingEmail")
    @Expose
    private String billingEmail;
    @SerializedName("billingPostcode")
    @Expose
    private String billingPostcode;
    @SerializedName("shippingName")
    @Expose
    private String shippingName;
    @SerializedName("shippingAddress")
    @Expose
    private String shippingAddress;
    @SerializedName("shippingPhone")
    @Expose
    private String shippingPhone;
    @SerializedName("shippingEmail")
    @Expose
    private String shippingEmail;
    @SerializedName("vat")
    @Expose
    private String vat;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("total")
    @Expose
    private int total;
    @SerializedName("transportFee")
    @Expose
    private String transportFee;
    @SerializedName("transportDes")
    @Expose
    private String transportDes;
    @SerializedName("transportType")
    @Expose
    private String transportType;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("status_user")
    private int status_user;
    @SerializedName("seller_id")
    @Expose
    private int seller_id;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("restaurant_id")
    @Expose
    private int restaurant_id;
    @SerializedName("items")
    @Expose
    private String items;

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }


    public OrderModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingPhone() {
        return billingPhone;
    }

    public void setBillingPhone(String billingPhone) {
        this.billingPhone = billingPhone;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingPostcode() {
        return billingPostcode;
    }

    public void setBillingPostcode(String billingPostcode) {
        this.billingPostcode = billingPostcode;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getShippingEmail() {
        return shippingEmail;
    }

    public void setShippingEmail(String shippingEmail) {
        this.shippingEmail = shippingEmail;
    }


    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getType_product() {
        return type_product;
    }

    public void setType_product(String type_product) {
        this.type_product = type_product;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getTransportFee() {
        return transportFee;
    }

    public void setTransportFee(String transportFee) {
        this.transportFee = transportFee;
    }

    public String getTransportDes() {
        return transportDes;
    }

    public void setTransportDes(String transportDes) {
        this.transportDes = transportDes;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus_user() {
        return status_user;
    }

    public void setStatus_user(int status_user) {
        this.status_user = status_user;
    }

    public int getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(int seller_id) {
        this.seller_id = seller_id;
    }
}
