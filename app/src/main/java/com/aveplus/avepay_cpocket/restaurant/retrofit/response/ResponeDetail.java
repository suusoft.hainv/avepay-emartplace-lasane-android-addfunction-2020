package com.aveplus.avepay_cpocket.restaurant.retrofit.response;
/**
 * Created by suusoft.com on 11/12/19.
 */

import com.aveplus.avepay_cpocket.restaurant.main.history.DetailOrder;

import java.util.ArrayList;

public class ResponeDetail extends BaseModel {

    public ArrayList<DetailOrder> data;


    public ArrayList<DetailOrder> getData() {
        return data;
    }

    public void setData(ArrayList<DetailOrder> data) {
        this.data = data;
    }
}
