package com.aveplus.avepay_cpocket.ebook.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseActivity;
import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.configs.GlobalFunction;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentChapter;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentListChapter;
import com.google.gson.Gson;


public class DetailsBookActivity extends BaseActivity {

    private static final String TAG = "DetailsBookActivity";
    private Bundle bundle;
    private Book mBook;
    private Fragment mCurrenFragment;
    private Menu menu;
    private TextView tvFooterChapter;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_details_book;

    }

    @Override
    protected void getExtraData(Intent intent) {

        bundle = intent.getExtras();
        if (null != bundle) {
            if (Config.TYPE_ITEM_CHAPTER == 0) {
                FragmentChapter fragment = FragmentChapter.newInstance(bundle);
                fragment.setArguments(bundle);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_details, fragment, Constant.FRAGMENT_CHAPTER)
                        .commit();
            } else {
                FragmentListChapter fragment = FragmentListChapter.newInstance(bundle);
                fragment.setArguments(bundle);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_details, fragment, Constant.FRAGMENT_CHAPTER)
                        .commit();
            }
        }
    }

    @Override
    protected void initilize() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }

        });
    }

    @Override
    public void onBackPressed() {
        String tag = getSupportFragmentManager().findFragmentById(R.id.content_details).getTag();
        Log.e("EEE", "BACK");
        if (toolbar.getVisibility() == View.GONE) {
            showFullScreen(false);
            ((Activity) self).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            ((Activity) self).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } else {
            if (tag.equals(Constant.FRAGMENT_CHAPTER)) {
                finish();
            } else {
                super.onBackPressed();
            }
            System.gc();
            Runtime.getRuntime().gc();
        }

    }

    @Override
    protected void initView() {
        if (bundle != null) {
            mBook = new Gson().fromJson(bundle.getString(Constant.KEY_BOOK), Book.class);
            setToolbarTitle(mBook.getTitle());
        }
    }

    @Override
    protected void onViewCreated() {
        GlobalFunction.showInterstitialAdmob();
    }

    public void showFullScreen(boolean values) {
        TextView tvIndicator = (TextView) this.findViewById(R.id.tv_indicator);

        if (values) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            toolbar.setVisibility(View.GONE);
            if (tvFooterChapter != null)
                tvFooterChapter.setVisibility(View.GONE);
            tvIndicator.setVisibility(View.GONE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            toolbar.setVisibility(View.VISIBLE);
            if (tvFooterChapter != null)
                tvFooterChapter.setVisibility(View.VISIBLE);
            tvIndicator.setVisibility(View.VISIBLE);

        }
    }


    public void addBookrMarks() {
        setResult(RESULT_CANCELED);
        DataStoreManager.addBook(mBook, Constant.LIST_BOOK_MARKS);
        showSnackBar(R.string.message_success_add_bookmarks);
    }

    public void deleteBookrMarks() {
        setResult(RESULT_OK);
        DataStoreManager.deleteBook(mBook, Constant.LIST_BOOK_MARKS);
        showSnackBar(R.string.message_success_delete_bookmarks);
    }

    public void setFooterChapter(TextView footerChapter) {
        tvFooterChapter = footerChapter;
    }
}
