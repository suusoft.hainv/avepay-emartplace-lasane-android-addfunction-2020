package com.aveplus.avepay_cpocket.restaurant.main.notification.object;
/**
 * Created by suusoft.com on 11/12/19.
 */
import com.aveplus.avepay_cpocket.restaurant.retrofit.response.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class onResponeFCM extends BaseModel {
    @SerializedName("data")
    @Expose
    private DataFCM dataUser;


    public DataFCM getDataUser() {
        return dataUser;
    }

    public void setDataUser(DataFCM data) {
        this.dataUser = data;
    }

}
