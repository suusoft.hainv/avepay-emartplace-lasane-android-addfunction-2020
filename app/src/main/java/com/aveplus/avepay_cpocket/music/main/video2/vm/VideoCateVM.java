package com.aveplus.avepay_cpocket.music.main.video2.vm;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.main.video2.ListVideoAct;
import com.aveplus.avepay_cpocket.music.main.video2.ObjVideoCategory;

import static com.aveplus.avepay_cpocket.music.main.video2.ListVideoAct.ID_CATEGORY;
import static com.aveplus.avepay_cpocket.music.main.video2.ListVideoAct.NAME_PLAYLIST;


public class VideoCateVM extends BaseAdapterVM {
    private ObjVideoCategory videoCategory;

    public VideoCateVM(Context self, Object videoCategory, int position) {
        super (self, position);
        this.videoCategory = (ObjVideoCategory) videoCategory;
    }

    public String getName() {
        return videoCategory.getName ();
    }

    public String getDescription() {
        return videoCategory.getDescription ();
    }

    public String getImage() {
        return videoCategory.getImgThumb ();
    }


    @Override
    public void setData(Object object) {
        videoCategory = (ObjVideoCategory) object;
        notifyChange ();
    }

    public void onItemClicked(View view) {
        Intent intent = new Intent(self, ListVideoAct.class);
        intent.putExtra (NAME_PLAYLIST,videoCategory.getName ());
        intent.putExtra (ID_CATEGORY,videoCategory.getId ().toString ());
        self.startActivity (intent);
    }

}
