package com.aveplus.avepay_cpocket.interfaces;

import android.util.SparseArray;

/**
 */
public interface MovieOfPageByGenreCallback {
    public void onCompleted(SparseArray<Object> object);
    public void onFailed(Exception exception);
}
