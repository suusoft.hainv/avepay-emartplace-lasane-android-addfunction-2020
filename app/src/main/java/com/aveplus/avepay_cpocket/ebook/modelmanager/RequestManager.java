package com.aveplus.avepay_cpocket.ebook.modelmanager;

import android.content.Context;
import android.util.Log;

import com.aveplus.avepay_cpocket.configs.Apis;
import com.aveplus.avepay_cpocket.ebook.model.Comment;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.BaseRequest;

import java.util.HashMap;

//import com.pt.template.gcm.MyGcmSharedPrefrences;

/**
 *
 */
public class RequestManager extends BaseRequest {

    private static final String TAG = RequestManager.class.getSimpleName();

    // Params
    private static final String PARAM_GCM_ID = "gcm_id";
    private static final String PARAM_STATUS = "status_hot";
    private static final String PARAM_IMEI = "ime";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_FULLNAME = "fullname";
    private static final String PARAM_IMAGE = "image";
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_USER_ID = "user_id";
    private static final String PARAM_MESSAGE = "message";
    private static final String PARAM_ID = "id";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_TASK_ID = "task_id";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_BOOK_ID = "book_id";
    private static final String PARAM_CATEGORY_ID = "category_id";
    private static final String PARAM_CHAPTER_ID = "chapter_id";
    private static final String PARAM_KEYWORD = "keyword";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_FILTER = "filter";


    public static void getHomeLastChapterByBook(Context context, String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE, page);
        get(Apis.getUrlBookDashboard, params, false, completeListener);
    }

    public static void getCategory(Context context, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        get(Apis.getUrlCategory, params, true, completeListener);
    }

    /*public static void getBookByCategory(Context context, String id, String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_CATEGORY_ID, id);
        params.put(PARAM_PAGE, page);
        get(Apis.URL_BOOK_BY_CATEGORY, params, true, completeListener);
    }*/

    public static void getLastest(Context context, String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE, page);
        get(Apis.getUrlBookDashboard, params, true, completeListener);
    }

    public static void getMostView(Context context, String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_PAGE, page);
        get(Apis.getUrlBookDashboard, params, true, completeListener);
    }

    public static void getBookList(Context context, String type, String categoryId, String keyword,
                                   String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_TYPE, type);
        params.put(PARAM_CATEGORY_ID, categoryId);
        params.put(PARAM_KEYWORD, keyword);
        params.put(PARAM_PAGE, page);
        get(Apis.getUrlBookList, params, true, completeListener);
    }

    public static void getBookByCondition(Context context, int type, String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_FILTER, type + "");
        params.put(PARAM_PAGE, page);
        get(Apis.getUrlGetBookByCondition, params, true, completeListener);
    }

    public static void getChapterByBookId(Context context, String type, String keyword, String bookId, String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_TYPE, type);
        params.put(PARAM_KEYWORD, keyword);
        params.put(PARAM_BOOK_ID, bookId);
        params.put(PARAM_PAGE, page);
        get(Apis.getUrlChapterByBookId, params, true, completeListener);
    }

    /*public static void getSearchBook(Context context, String searchKey, String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_SEARCH_KEY, searchKey);
        params.put(PARAM_PAGE, page);
        get(Apis.URL_SEARCH_BOOK, params, true, completeListener);
        Log.e("eee", "getSearchBook  :   " + Apis.URL_SEARCH_BOOK + "?" + PARAM_SEARCH_KEY + "=" + searchKey + "&" + PARAM_PAGE + "=" + page);
    }*/

    public static void getChapterByChapterId(Context context, String chapterId, String page, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_CHAPTER_ID, chapterId);
        params.put(PARAM_PAGE, page);
        get(Apis.getUrlChapterByChapterId, params, true, completeListener);
        Log.e("eee", "getChapterByChapterId  :   " + Apis.getUrlChapterByChapterId + "?" + PARAM_CHAPTER_ID + "=" + chapterId + "&" + PARAM_PAGE + "=" + page);
    }

    public static void getSendFeedBack(Context context, String name, String email, String subject,
                                       String content, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("email", email);
        params.put("subject", subject);
        params.put("content", content);
        get(Apis.getUrlFeedBack, params, true, completeListener);
    }

    public static void getCommentByChapter(Context context, String chapterId, String page,
                                           final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_CHAPTER_ID, chapterId);
        params.put(PARAM_PAGE, page);
        get(Apis.getUrlGetComment, params, true, completeListener);
    }

    public static void addComment(Context context, Comment comment, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("name", comment.getName());
        params.put("content", comment.getContent());
        params.put("chapter_id", comment.getChapterId() + "");
        params.put("book_id", comment.getBookId() + "");
        params.put("date", comment.getDatetime());
        params.put("status", comment.getStatus() + "");
        get(Apis.getUrlAddComment, params, true, completeListener);
    }

    public static void countReadBook(Context context, String action, String bookId, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("action", action);
        params.put("book_id", bookId);
        get(Apis.getUrlCountReadBook, params, false, completeListener);
    }


    public static void registerDevice(String gcmId, final ApiManager.CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("gcm_id", gcmId);
        params.put("type", "0");
        get(Apis.registerDevice, params, false, completeListener);
    }
}
