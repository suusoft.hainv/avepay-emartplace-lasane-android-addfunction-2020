package com.aveplus.avepay_cpocket.music.main.payment;

import android.content.Context;
import android.view.View;


import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.configs.Global;

import org.greenrobot.eventbus.EventBus;

public class ItemSubscribeVM extends BaseAdapterVM {
    private PackageSubscribe packageSubscribe;

    public ItemSubscribeVM(Context self, Object object) {
        super(self);
        this.packageSubscribe = (PackageSubscribe) object;
    }

    @Override
    public void setData(Object object) {

    }

    public String getImage() {
        return packageSubscribe.getImage();
    }

    public String getName() {
        return packageSubscribe.getName();
    }

    public String getDescription() {
        return packageSubscribe.getDescription();
    }

    public int getPrice() {
        return packageSubscribe.getPrice();
    }

    public void onItemClicked(View view) {

        //Toast.makeText(self, "" + packageSubscribe.getName(), Toast.LENGTH_SHORT).show();

        EventBus.getDefault().post(new Global.SubscribeItemClicked(String.valueOf(packageSubscribe.getPrice())));


    }

}
