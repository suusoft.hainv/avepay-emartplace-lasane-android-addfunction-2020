package com.aveplus.avepay_cpocket.music.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.music.base.model.BaseModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Album extends BaseModel implements Parcelable {
    public static final String NORMAL = "normal";
    public static final String FEATURE = "feature";
    public static final String SINGLE = "single";
    public static final String KEY_ALBUM = "album";
    public String release_date;
    public String nameSong;
    public String nameSinger;
    @SerializedName("img_thumb")
    public String thumbnail;
    @SerializedName("img_banner")
    public String banner;
    @SerializedName("songs_count")
    public int songs_count;
    public String category_id;
    public String nameCategory;
    public boolean isPlaylist;
    public String id, title, name, description, image;
    @SerializedName("songs")
    private ArrayList<Song> listSong;

    protected Album(Parcel in) {
        release_date = in.readString();
        nameSong = in.readString();
        nameSinger = in.readString();
        thumbnail = in.readString();
        songs_count = in.readInt();
        category_id = in.readString();
        nameCategory = in.readString();
        isPlaylist = in.readByte() != 0;
        id = in.readString();
        title = in.readString();
        name = in.readString();
        description = in.readString();
        image = in.readString();
        listSong = in.createTypedArrayList(Song.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(release_date);
        dest.writeString(nameSong);
        dest.writeString(nameSinger);
        dest.writeString(thumbnail);
        dest.writeInt(songs_count);
        dest.writeString(category_id);
        dest.writeString(nameCategory);
        dest.writeByte((byte) (isPlaylist ? 1 : 0));
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeTypedList(listSong);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public int getSongs_count() {
        return songs_count;
    }

    public void setSongs_count(int songs_count) {
        this.songs_count = songs_count;
    }

    public String getNameSong() {
        return nameSong;
    }

    public void setNameSong(String nameSong) {
        this.nameSong = nameSong;
    }

    public String getNameSinger() {
        return nameSinger;
    }

    public void setNameSinger(String nameSinger) {
        this.nameSinger = nameSinger;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public boolean isPlaylist() {
        return isPlaylist;
    }

    public void setPlaylist(boolean playlist) {
        isPlaylist = playlist;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ArrayList<Song> getListSong() {
        if (listSong == null)
            listSong = new ArrayList<>();
        return listSong;
    }

    public void setListSong(ArrayList<Song> listSong) {
        this.listSong = listSong;
    }
}
