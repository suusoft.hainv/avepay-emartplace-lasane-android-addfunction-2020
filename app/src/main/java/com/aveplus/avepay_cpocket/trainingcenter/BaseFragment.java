package com.aveplus.avepay_cpocket.trainingcenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment {
    protected Activity self;
    protected Context context;
    protected static final String TAG = "BaseFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    public interface TitleChanged {
        public void onTitleChanged(String title);
    }

    protected TitleChanged onTitleChanged;

    public TitleChanged getOnTitleChanged() {
        return onTitleChanged;
    }

    public void setOnTitleChanged(TitleChanged onTitleChanged) {
        this.onTitleChanged = onTitleChanged;
    }

    protected void startActivity(Bundle bundle, Class<?> cla) {
        Intent intent = new Intent(getActivity(), cla);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("bundle", bundle);
        startActivity(intent);
    }

}
