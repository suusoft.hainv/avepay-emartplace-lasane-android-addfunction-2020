package com.aveplus.avepay_cpocket.taxi.interfaces;


import com.aveplus.avepay_cpocket.taxi.objects.book.ServiceObj;

/**
 * Created by SuuSoft.com on 11/29/2016.
 */

public interface ITransport {
    void onTransportSelected(ServiceObj transportObj);
}
