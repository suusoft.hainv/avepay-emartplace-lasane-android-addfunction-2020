package com.aveplus.avepay_cpocket.ebook.listener;

import android.view.View;

/**
 * Created by Trang on 7/7/2017.
 */
public interface IOnItemClickedListener extends IBaseListener {
     void onItemClicked(View view, int position);
}
