package com.aveplus.avepay_cpocket.movie.movie.retrofit.base;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.aveplus.avepay_cpocket.movie.movie.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.movie.movie.retrofit.param.Param;
import com.aveplus.avepay_cpocket.movie.movie.retrofit.response.ApiResponse;
import com.google.gson.JsonObject;


import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;

import static com.aveplus.avepay_cpocket.movie.movie.retrofit.param.Param.PARAM_TOKEN;


/**
 * Created by Suusoft 2020.
 */

public class BaseRequest implements Param {


    private static BaseRequest mInstance;

    protected static Context mContext;

    public static BaseRequest getInstance() {
        if (mInstance == null) {
            mInstance = new BaseRequest();
        }

        return mInstance;
    }

    /**
     * Interface listener when requestGet api
     */
    public interface CompleteListener {
        public void onSuccess(ApiResponse response);

        public void onError(String message);

    }


    //********************************************************************************
    /**
     * interface for listening progress.
     * purpose is show and hide progressbar
     */
    private ListenerLoading listenerLoading;

    public ListenerLoading getListenerLoading() {
        return listenerLoading;
    }

    public void setListenerLoading(ListenerLoading listenerLoading) {
        this.listenerLoading = listenerLoading;
    }

    /**
     * show hide progress bar
     */
    private void showProgress(boolean isOpen) {
        if (isOpen && listenerLoading != null) {
            listenerLoading.onLoadingIsProcessing();
        }
    }

    public void hideProgress(boolean isOpen) {
        if (isOpen && listenerLoading != null) {
            listenerLoading.onLoadingIsCompleted();
        }
    }

    //*******************************************************************************

    /**
     * set default pamrams
     */
    private static void addDefaultParams(HashMap<String, String> params) {
//        String userId = DataStoreManager.getUser() != null ? DataStoreManager.getUser().getId() : "";
//        params.put(PARAM_USER_ID, userId);
        params.put(PARAM_TOKEN, DataStoreManager.getToken());
    }


    public static void get(String url, HashMap<String, String> params, final BaseRequest.CompleteListener listener) {
        BaseRequest.getInstance().requestGet(url, params, false, listener, null);
    }


    public static void get(String url, HashMap<String, String> params, boolean isShowProgress, final BaseRequest.CompleteListener listener) {
        BaseRequest.getInstance().requestGet(url, params, isShowProgress, listener, null);
    }

    public static void get(String url, HashMap<String, String> params, boolean isShowProgress, final BaseRequest.CompleteListener listener, String tag) {
        BaseRequest.getInstance().requestGet(url, params, isShowProgress, listener, tag);
    }


    public static void post(String url, HashMap<String, String> params, final BaseRequest.CompleteListener listener) {
        BaseRequest.getInstance().requestPost(url, params, true, listener, null);
    }

    public static void post(String url, HashMap<String, String> params, boolean isShowProgress, final BaseRequest.CompleteListener listener) {
        BaseRequest.getInstance().requestPost(url, params, isShowProgress, listener, null);
    }

    public static void post(String url, HashMap<String, String> params, boolean isShowProgress, final BaseRequest.CompleteListener listener, String tag) {
        BaseRequest.getInstance().requestPost(url, params, isShowProgress, listener, tag);
    }

    public static void multipart(String url, HashMap<String, String> textParams, @Part ArrayList<MultipartBody.Part> fileParams, boolean isShowProgress, final BaseRequest.CompleteListener listener, String tag) {
        BaseRequest.getInstance().requestMultipart(url, textParams, fileParams, isShowProgress, listener, tag);
    }

    public static void multipart(String url, HashMap<String, String> textParams, @Part ArrayList<MultipartBody.Part> fileParams, boolean isShowProgress, final BaseRequest.CompleteListener listener) {
        BaseRequest.getInstance().requestMultipart(url, textParams, fileParams, isShowProgress, listener, null);
    }


    /**
     * checking and excute request to api.
     * 1. Checking network connection. if network is connected to api
     * else get caching data if is on or notify that not connection
     *
     * @param url            root url
     * @param params         params
     * @param isShowProgress show progressbar or not
     * @param listener       listener for request
     * @param tag            tag of request
     */

    private void requestGet(String url, HashMap<String, String> params,
                            final boolean isShowProgress, final CompleteListener listener, String tag) {

        showProgress(isShowProgress);
        addDefaultParams(params);

        ApiUtils.getAPIService().get(url, params).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                hideProgress(isShowProgress);

                checkResponseData(response, listener);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgress(isShowProgress);
                listener.onError(t.getMessage());


            }
        });

    }


    /**
     * checking and excute request to api.
     * 1. Checking network connection. if network is connected to api
     * else get caching data if is on or notify that not connection
     *
     * @param url            root url
     * @param params         params
     * @param isShowProgress show progressbar or not
     * @param listener       listener for request
     * @param tag            tag of request
     */


    private void requestPost(String url, HashMap<String, String> params,
                             final boolean isShowProgress, final CompleteListener listener, String tag) {

        showProgress(isShowProgress);
        addDefaultParams(params);

        ApiUtils.getAPIService().post(url, params).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                hideProgress(isShowProgress);

                checkResponseData(response, listener);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgress(isShowProgress);
                listener.onError(t.getMessage());


            }
        });

    }


    /**
     * checking and excute request to api.
     * 1. Checking network connection. if network is connected to api
     * else get caching data if is on or notify that not connection
     *
     * @param url            root url
     * @param params         params
     * @param isShowProgress show progressbar or not
     * @param listener       listener for request
     * @param tag            tag of request
     */

    private void requestMultipart(String url, HashMap<String, String> params, @Nullable ArrayList<MultipartBody.Part> fileParams,
                                  final boolean isShowProgress, final CompleteListener listener, String tag) {


        showProgress(isShowProgress);
        addDefaultParams(params);

        ApiUtils.getAPIService().post(url, params, fileParams).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                hideProgress(isShowProgress);

                checkResponseData(response, listener);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgress(isShowProgress);
                listener.onError(t.getMessage());


            }
        });

    }


    private void checkResponseData(Response<JsonObject> response, CompleteListener listener) {

        if (response.isSuccessful() && response.body() != null) {

            Log.e("BaseRequest", "onResponse: " + response.body().toString());

            ApiResponse apiResponse = new ApiResponse(response.body());

            listener.onSuccess(apiResponse);


        } else {

            listener.onError(response.message());
        }
    }


}
