package com.aveplus.avepay_cpocket.restaurant.main.shop;

import android.view.KeyEvent;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;


public class RegisterShopAct extends BaseActivity {

    private WebView wvShop;


    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_register_shop;
    }

    @Override
    protected void initView() {
        setToolbarTitle(R.string.your_shop);
        wvShop = findViewById(R.id.wv_shop);

    }

    @Override
    protected void onViewCreated() {
        wvShop.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wvShop.getSettings().setJavaScriptEnabled(true);
        wvShop.getSettings().setLoadWithOverviewMode(true);
        wvShop.getSettings().setUseWideViewPort(true);

        String data = "https://bazaaramadan.com/shop/backend/web/index.php";
        wvShop.loadUrl(data);
        wvShop.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                WebView wvShop = (WebView) v;
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (wvShop.canGoBack()) {
                        wvShop.goBack();
                        return true;
                    }
                }
                return false;
            }
        });
        wvShop.setWebViewClient(new WebViewClient() {

                                    @Override
                                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                        return super.shouldOverrideUrlLoading(view, url);

                                    }

                                    @Override
                                    public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
                                        super.onReceivedClientCertRequest(view, request);
                                    }
                                }
        );

    }


    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected void onPrepareCreateView() {

    }


}
