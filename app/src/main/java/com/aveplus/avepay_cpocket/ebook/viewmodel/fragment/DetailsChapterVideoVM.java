package com.aveplus.avepay_cpocket.ebook.viewmodel.fragment;

import android.content.Context;
import android.text.Html;

import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;


/**
 * Created by Suusoft on 9/27/2017.
 */
public class DetailsChapterVideoVM extends BaseViewModel {

    @Override
    public void getData(int page) {

    }
    private Book book;

    private Chapter chapter;

    public DetailsChapterVideoVM(Context self, Chapter chapter, Book book) {
        super(self);
        this.chapter = chapter;
        this.book = book;
    }

    public String getImage() {
        return chapter.getImage();
    }

    public String getTitle() {
        return chapter.getTitle();
    }

    public String getDescription() {
        return Html.fromHtml(chapter.getDescription()).toString();

    }

    public String getTime() {
        return chapter.getTime();
    }
    public String getAuthor() {
        return book.getAuthor();
    }
}
