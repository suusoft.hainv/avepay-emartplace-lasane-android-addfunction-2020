package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.activity.HomeActivity;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.SlidesAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.lib.CirclePageIndicator;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.NewEvent;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class FragmentHome extends BaseFragment implements OnClickListener,
        BaseFragment.TitleChanged {

    View view;
    LinearLayout btproduct, btcontact, btnew, btservice, btport, btcustomer;
    RelativeLayout llSlideImg;
    private ViewPager viewPager;
    private CirclePageIndicator mIndicator;
    private SlidesAdapter adapter;
    ArrayList<NewEvent> data = new ArrayList<NewEvent>();
    private Timer mTimer;
    private int i;
    private HomeActivity mEducationActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_training_center, container, false);
        mTimer = new Timer();
        getAllNews(1, false);
        initUni();
        initClick();
        initSlide();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof HomeActivity) {
            mEducationActivity = (HomeActivity) activity;
        }
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("home");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void initClick() {
        btcontact.setOnClickListener(this);
        btcustomer.setOnClickListener(this);
        btproduct.setOnClickListener(this);
        btport.setOnClickListener(this);
        btnew.setOnClickListener(this);
        btservice.setOnClickListener(this);
    }

    public void initUni() {
        btcontact = (LinearLayout) view.findViewById(R.id.btContact);
        btproduct = (LinearLayout) view.findViewById(R.id.btProduct);
        btport = (LinearLayout) view.findViewById(R.id.btPort);
        btnew = (LinearLayout) view.findViewById(R.id.btNew);
        btservice = (LinearLayout) view.findViewById(R.id.btService);
        btcustomer = (LinearLayout) view.findViewById(R.id.btCustomer);
        llSlideImg = (RelativeLayout) view.findViewById(R.id.llSlideImg);
    }

    private void initSlide() {
//        for (Galleries slide : GlobalValue.pref.getArrPicture()) {
//            data.add(slide.getUrl());
//        }
        adapter = new SlidesAdapter(getActivity(), data);
        viewPager = (ViewPager) view.findViewById(R.id.pagerPro);
        viewPager.setAdapter(adapter);


        mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        mIndicator.setViewPager(viewPager);
        mIndicator.setFillColor(Color.RED);
    }

    private void autoSlideImage() {
        final int totalImage = data.size();
        i = 1;
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (i < totalImage) {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            viewPager.setCurrentItem(i);

                        }
                    });

                    i++;
                    Log.e("EEEEEEEEEee", "EEEEEEEEEEEEEEe1111" + i);
                } else if (i == totalImage) {

                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            i = 0;
                            viewPager.setCurrentItem(i);
                            Log.e("EEEEEEEEEee", "EEEEEEEEEEEEEEe2222" + i);
                        }
                    });
                    i++;
                }

            }
        }, 3000, 6000);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btProduct:
                mEducationActivity.productOrNewsBlog = HomeActivity.PRODUCT;
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_from_bottom,
                                R.anim.slide_out_to_top)
                        .replace(R.id.show, new FragmentProducts(), "product")
                        .addToBackStack("product").commit();
                break;

            case R.id.btService:
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_from_bottom,
                                R.anim.slide_out_to_top)
                        .replace(R.id.show, new FragmentFAQ(), "service")
                        .addToBackStack(null).commit();
                break;

            case R.id.btCustomer:
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_from_bottom,
                                R.anim.slide_out_to_top)
                        .replace(R.id.show, new FragmentTestimonials(), "customer")
                        .addToBackStack(null).commit();
                break;

            case R.id.btNew:
                mEducationActivity.productOrNewsBlog = HomeActivity.NEWS_BLOG;
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_from_bottom,
                                R.anim.slide_out_to_top)
                        .replace(R.id.show, new FragmentNewEvent(), "newevent")
                        .addToBackStack(null).commit();
                break;

            case R.id.btContact:
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_from_bottom,
                                R.anim.slide_out_to_top)
                        .replace(R.id.show, new FragmentContacts(), "contact")
                        .addToBackStack(null).commit();
                break;

            case R.id.btPort:
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_from_bottom,
                                R.anim.slide_out_to_top)
                        .replace(R.id.show, new Fragment_Tutors(), "port")
                        .addToBackStack(null).commit();
                break;

        }
    }

    private void getAllNews(final int currentPage2, boolean isPull) {
        if (data.size() == 0) {
            ModelManager.getListNewsPopular(currentPage2, new BaseRequest.CompleteListener() {
                @Override
                public void onSuccess(ApiResponse response) {
                    data.addAll(ParseUtility.parseListNews(response.getRootStr()));
                    adapter.notifyDataSetChanged();
                    autoSlideImage();
                }

                @Override
                public void onError(String message) {
                    AppUtil.showToast(self, message);
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null)
            mTimer.cancel();
    }

    @Override
    public void onTitleChanged(String title) {
    }
}
