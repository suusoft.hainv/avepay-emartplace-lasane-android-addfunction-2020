package com.aveplus.avepay_cpocket.music.main.player;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.library.Lrc;
import com.aveplus.avepay_cpocket.music.library.LrcHelper;
import com.aveplus.avepay_cpocket.music.library.LrcView;
import com.aveplus.avepay_cpocket.music.player.QueueManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;


public class LyricFragment extends BaseFragmentBinding {
    private static final String TAG = LyricFragment.class.getSimpleName();
    private LyricsVM viewModel;
    LrcView lrcView;

    private MediaPlayer mMediaPlayer = new MediaPlayer();
    private Handler mHandler = new Handler();

    public static LyricFragment newInstance() {

        Bundle args = new Bundle();

        LyricFragment fragment = new LyricFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_lyrics;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new LyricsVM(self);
        return viewModel;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.NewSongSelected event) {
        parseLyric();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void initView(View view) {
        EventBus.getDefault().register(this);
//        FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.view_lyrics);
//        frameLayout.setFocusable(true);
//        mLrcView = new LrcView(self, null);
//        frameLayout.addView((View) mLrcView);
//       String lrc = getFromAssets("test.lrc");
//        ILrcBuilder builder = new DefaultLrcBuilder();
//        List<LrcRow> rows = builder.getLrcRows(lrc);
//        ((View) mLrcView).setBackgroundColor(R.color.bg);
//
//        mLrcView.setLrc(rows);
        InputStream inputStream = new ByteArrayInputStream(viewModel.item.getContent().getBytes(Charset.forName("UTF-8")));

        lrcView = (LrcView) view.findViewById(R.id.lyrics);
        //List<Lrc> lrcs = LrcHelper.parseLrcFromAssets(self,"suytnuathi.lrc");

        List<Lrc> lrcs = LrcHelper.parseLrc2(QueueManager.getInstance().getCurrentItem().getContent());
        Log.e(TAG, "initView: " + viewModel.item.getContent());

        lrcView.setLrcData(lrcs);
        lrcView.updateTime(20000);
//        lrcView.setOnPlayIndicatorLineListener(new LrcView.OnPlayIndicatorLineListener() {
//            @Override
//            public void onPlay(long time, String content) {
//                mMediaPlayer.seekTo((int) time);
//                mMediaPlayer.prepareAsync();
//                mHandler.post(mRunnable);
//            }
//        });
//

    }

    private void parseLyric() {
        List<Lrc> lrcs = LrcHelper.parseLrc2(QueueManager.getInstance().getCurrentItem().getContent());
        Log.e(TAG, "initView: " + viewModel.item.getContent());

        lrcView.setLrcData(lrcs);
        lrcView.updateTime(20000);
    }

    public void updateTime(long time) {

        lrcView.updateTime(time);
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            int currentPosition = mMediaPlayer.getCurrentPosition();
            lrcView.updateTime(currentPosition);
            mHandler.postDelayed(this, 100);
        }
    };

    public String getFromAssets(String fileName) {
        try {
            InputStreamReader inputReader = new InputStreamReader(getResources().getAssets().open(fileName));
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line = "";
            String Result = "";
            while ((line = bufReader.readLine()) != null) {
                if (line.trim().equals(""))
                    continue;
                Result += line + "\r\n";
            }
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
