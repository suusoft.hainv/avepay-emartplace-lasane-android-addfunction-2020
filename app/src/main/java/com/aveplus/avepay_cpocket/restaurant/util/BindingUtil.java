package com.aveplus.avepay_cpocket.restaurant.util;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.aveplus.avepay_cpocket.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by phamv on 7/25/2017.
 */

public class BindingUtil {


    /**
     * Set image by Picasso
     * @param imageView image destination
     * @param imageUrl image path
     *
     */
    @BindingAdapter("url")
    public static void setImage(ImageView imageView, String imageUrl) {
        if (imageUrl != null && !imageUrl.equals("")) {
            Picasso.get()
                    .load(imageUrl)
                    .error(R.drawable.placeholder)
                    .into(imageView);

        } else {
            imageView.setImageResource(R.drawable.placeholder);
        }
    }

    @BindingAdapter("img_blur")
    public static void setImageResizeQuare(final ImageView imageView, final String imageUrl) {
        if (imageUrl != null && !imageUrl.equals("")) {
            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    if (bitmap != null) {
                        imageView.setImageBitmap(ImageUtil.blur(imageView.getContext(), bitmap));
                    }
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }


                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            imageView.setTag(target);
            Picasso.get()
                    .load(imageUrl)
                    .error(R.mipmap.ic_launcher)
                    .into(target);

        } else {
            imageView.setImageResource(R.mipmap.ic_launcher);
        }
    }

    /**
     * Set image by Picasso
     */
    @BindingAdapter("url")
    public static void setImage(ImageView imageView, int res) {
        imageView.setImageResource(res);
    }
    /**
     * Set image by Picasso
     */
    @BindingAdapter("anim")
    public static void setAnimView(View imageView, String s) {
        imageView.startAnimation(AnimationUtils.loadAnimation(imageView.getContext(), R.anim.rotation));
    }

    /**
     * set height of view equal with a ratio width of view
     * @param highPercent percent of height with width
     */
    @BindingAdapter("layout_height_percent")
    public static void setLayoutHeight(View view, float highPercent) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = (int) (layoutParams.width*highPercent/100);
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("layout_height")
    public static void setLayoutHeight(View view, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }


}
