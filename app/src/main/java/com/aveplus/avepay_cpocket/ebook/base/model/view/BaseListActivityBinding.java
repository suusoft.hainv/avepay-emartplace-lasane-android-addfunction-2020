package com.aveplus.avepay_cpocket.ebook.base.model.view;

import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.listener.IDataChangedListener;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModelList;

import java.util.List;

/**
 * Created by Trang on 7/15/2017.
 */
public abstract class BaseListActivityBinding extends BaseActivityBinding implements IDataChangedListener {

    private BaseViewModelList mViewModel;
    public RecyclerView recyclerView;

    protected abstract void setUpRecyclerView(RecyclerView recyclerView);

    @Override
    protected int getLayoutInflate() {
        return R.layout._base_list;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        // set viewmodel
        mViewModel = (BaseViewModelList) viewModel;
        mViewModel.setDataListener(this);

        // set bindding
        this.binding =  binding;
        this.binding.setVariable(BR.viewModel,viewModel);

        // get recyclerView and set
        recyclerView = (RecyclerView) binding.getRoot().findViewById(R.id.rcv_data);
        recyclerView.setLayoutManager(mViewModel.getLayoutManager());
        recyclerView.addOnScrollListener(mViewModel.getOnScrollListener());
        setUpRecyclerView(recyclerView);
    }

    /**
     * listener #BaseViewmodel.notifyDataChanged()
     *  and refresh adapter
     *
     */
    @Override
    public void onListDataChanged(List<?> data, boolean isAppend) {
        BaseAdapterBinding adapter = (BaseAdapterBinding) recyclerView.getAdapter();
        adapter.setItems(data, isAppend);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.onDestroy();
    }
}
