package com.aveplus.avepay_cpocket.music.util;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public class StringUtil {

    /**
     * Check format of email
     *
     * @param target is a email need checking
     */
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Check field not empty and 6 word
     *
     * @param input text input
     */
    public static boolean isGoodField(String input) {
        if (input == null || input.isEmpty() || input.length() < 6)
            return false;
        return true;
    }

    /**
     * Check field not empty
     *
     * @param input text input
     */
    public static boolean isEmpty(String input) {
        if (input == null || input.isEmpty())
            return true;
        return false;
    }

    /**
     * get action of url request
     *
     * @param url
     */
    public static String getAction(String url) {
        return url.substring(url.lastIndexOf("/") + 1, url.length());
    }

    /**
     * bind price
     */
    public static String getPrice(String price) {
        return String.format(AppController.getInstance().getString(R.string.price_bind), price);
    }

    public static boolean isNumeric(String str) {
        try {
            int d = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isValidatePassword(String password) {
        Pattern p = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(password);
        return m.find();
    }
}
