package com.aveplus.avepay_cpocket.movie.movie.main;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.movie.movie.main.adapter.SearchGridMovieAdaper;
import com.aveplus.avepay_cpocket.movie.movie.main.detail.MovieDetailActivity;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.MoviesManagement;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;
import com.aveplus.avepay_cpocket.movie.movie.main.movies.impl.SearchMoviesCallback;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.List;

/**
 * Copyright © 2019 SUUSOFT
 */
public class SearchActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    public static final String TEXT_SEARCH = "search text";
    private String mStrSearch;

    private EditText mTxtSearch;
    private ImageButton mbtnBack;
    private ImageButton mbtnSeach;
    private GridView mGridview;

    private SearchGridMovieAdaper adaper;
    private List<Movie> mMovies;
    private boolean isTabblet;

    @Override
    public void onCreate(Bundle savedInstanceState) {
//        isTabblet = getResources().getBoolean(R.bool.isTablet);
//        if (isTabblet){
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        }else{
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_movie);
        initView();
        getExtra();
        if (mStrSearch.isEmpty()) {
            mTxtSearch.requestFocus();
            AppUtil.showKeyboard(this, mTxtSearch);
        } else {
            searchMovie(mStrSearch);
        }

        //AdsUtil.loadBanner(findViewById(R.id.adView));
    }

    EditText.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int keyCode, KeyEvent keyEvent) {
            if (keyCode == EditorInfo.IME_ACTION_SEARCH || keyCode == KeyEvent.KEYCODE_ENTER) {
                searchMovie(mTxtSearch.getText().toString());
                return true;
            }
            return false;
        }
    };

    private void initView() {
        mTxtSearch = (EditText) findViewById(R.id.txt_search);
        mbtnBack = (ImageButton) findViewById(R.id.btn_back);
        mbtnSeach = (ImageButton) findViewById(R.id.btnSearch);
        mGridview = (GridView) findViewById(R.id.grv_search_movie);
        mTxtSearch.setOnEditorActionListener(editorActionListener);

        mbtnBack.setOnClickListener(this);
        mbtnSeach.setOnClickListener(this);
        mGridview.setOnItemClickListener(this);
    }

    private void getExtra() {
        mStrSearch = getIntent().getExtras().getString(TEXT_SEARCH);
        mTxtSearch.setText(mStrSearch);
    }

    private void setAdapter() {
        adaper = new SearchGridMovieAdaper(getApplicationContext(), R.layout.adapter_search_grid, mMovies, mGridview);
        mGridview.setAdapter(adaper);
    }

    @Override
    public void onClick(View view) {
        if (view == mbtnBack)
            finish();
        else if (view == mbtnSeach)
            searchMovie(mTxtSearch.getText().toString());
    }

    private void didSelectMovieAtPosition(Movie movie, int position) {
        Intent movieDetailIntent = new Intent(getApplicationContext(), MovieDetailActivity.class);
        // Set data for bundle
        Bundle bundle = new Bundle();
        bundle.putInt(MovieDetailActivity.MOVIE_EXTRA_POSITION, position);
        bundle.putParcelable(MovieDetailActivity.MOVIE_EXTRA_OBJECT, movie);
        movieDetailIntent.putExtras(bundle);
        startActivity(movieDetailIntent);

    }

    private void searchMovie(String text) {
        MoviesManagement.getInstance().searchMoviesRequest(getApplicationContext(), text,
                new SearchMoviesCallback() {

                    @Override
                    public void onFailed(Exception e) {
                        Toast.makeText(getApplicationContext(), "Sorry, we can not find any film with this keyword", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onCompleted(List<Movie> searchResult) {
                        if (searchResult == null)
                            return;
                        mMovies = searchResult;
                        setAdapter();
                        AppUtil.hideSoftKeyboard(SearchActivity.this);
                    }
                });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        MoviesManagement.getInstance().setmMovies(mMovies);
        didSelectMovieAtPosition(mMovies.get(position), position);
    }

    @Override
    protected void onDestroy() {
        AppUtil.hideSoftKeyboard(SearchActivity.this);
        super.onDestroy();
    }
}
