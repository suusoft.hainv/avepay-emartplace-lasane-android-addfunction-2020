package com.aveplus.avepay_cpocket.restaurant.main.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class DetailOrderAdapter extends RecyclerView.Adapter<DetailOrderAdapter.ViewHoler>{
    public List<Detail> details;
    private Context context;

    public DetailOrderAdapter(List<Detail> details, Context context) {
        this.details = details;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from (context).inflate (R.layout.row_cart_confirm, parent, false);
        return new ViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoler holder, int position) {
        holder.binData(details.get(position));
    }

    @Override
    public int getItemCount() {
        return details == null ? 0 : details.size();
    }

    public class ViewHoler extends RecyclerView.ViewHolder {
        private TextView tvName,tvRestaurant, tvQuantity, tvPrice, tvStatus;
        private ImageView imgFood;

        public ViewHoler(@NonNull View itemView) {
            super (itemView);
            tvName          = itemView.findViewById(R.id.tv_namefood_cart);
            tvQuantity      = itemView.findViewById(R.id.tv_quantity);
            tvPrice         = itemView.findViewById(R.id.tv_cart_price);
            imgFood         = itemView.findViewById(R.id.img_cart_item);
        }

        public void binData(Detail detail){
            tvName.setText(detail.getProductName());
            tvPrice.setText("$"+detail.getPrice());
            tvQuantity.setText(String.valueOf(detail.getQuantity()));
            Glide.with(context).load(detail.getRestaurantInfo().getAttachment()).into(imgFood);

        }


    }
}
