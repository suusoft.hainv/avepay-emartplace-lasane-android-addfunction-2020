package com.aveplus.avepay_cpocket.trainingcenter.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseActivity;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ClassAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.fragment.DetailSchedule;
import com.aveplus.avepay_cpocket.trainingcenter.object.Class;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.squareup.picasso.Picasso;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SuuSoft on 05/09/2017.
 */

public class DetailCouresActivity extends BaseActivity {
    ImageView imgservice;
    private ArrayList<Class> mListClass;
    private ListView mLvListClass;
    private ClassAdapter mClassAdapter;
    private Bundle bundle;
    TextView txttitle, txtdesc, txtcontent;
    private ImageView btnBack;

    @Override
    protected ToolbarType getToolbarType() {
        return null;
    }

    @Override
    protected int getLayoutInflate() {
        return 0;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_courses);
        intiView();
        intiData();
        initControl();
    }

    public void intiView() {
        txttitle = (TextView) findViewById(R.id.txtTitleServicesDetail);
        imgservice = (ImageView) findViewById(R.id.imgServiceDetail);
        mLvListClass = (ListView) findViewById(R.id.lvListSchedules);
        txttitle.setSelected(true);
        btnBack = (ImageView) findViewById(R.id.imgBack);


    }

    private void initControl() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mLvListClass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DetailSchedule fragment = new DetailSchedule();
                Class aClass = mListClass.get(position);
                long startTime = parserStringDateToTimeStamp(aClass.getmStartDate() + " 23:59:59");
                if (System.currentTimeMillis() < startTime) {
                    aClass.setExpired(false);
                } else {
                    aClass.setExpired(true);
                }
                bundle.putParcelable("Class", aClass);
                Intent intent = new Intent(getApplicationContext(), DetailScheduleActivity.class);
                intent.putExtra("bundle", bundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
    }

    private long parserStringDateToTimeStamp(String stringDate) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public void intiData() {
        bundle = getIntent().getBundleExtra("bundle");
        Product product = bundle.getParcelable("item");
        mListClass = product.getmListClass();
        mClassAdapter = new ClassAdapter(mListClass);
        mLvListClass.setAdapter(mClassAdapter);
        txttitle.setText(product.getName());
        Picasso.get().load(product.getImage().get(0)).into(imgservice);
    }
}
