package com.aveplus.avepay_cpocket.taxi.view.adapters;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.interfaces.IOnItemClickListener;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;

import java.util.List;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {

    private List<Location> mDatas;
    private Context context;
    private IOnItemClickListener i;

    public PlaceAdapter(Context context, List<Location> mDatas, IOnItemClickListener i) {
        this.mDatas = mDatas;
        this.context = context;
        this.i = i;
    }

    @Override
    public PlaceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place, parent, false);
        return new PlaceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PlaceAdapter.ViewHolder holder, int position) {
        Location item = mDatas.get(position);
        if (item != null) {
            holder.bind(item);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        ImageView imgProduct;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            imgProduct = itemView.findViewById(R.id.btn_favorite);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    i.onItemClick(getAdapterPosition());
                }
            });

        }

        public void bind(Location item) {
            MapsUtil.getAddressFromLocation(tvName, item);
        }

    }
}



