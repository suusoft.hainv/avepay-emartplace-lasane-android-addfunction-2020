package com.aveplus.avepay_cpocket.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.Constants;
import com.aveplus.avepay_cpocket.objects.DealCateObj;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.view.fragments.DealListByMapFragment;
import com.aveplus.avepay_cpocket.view.fragments.DealListFragment;
import com.aveplus.avepay_cpocket.view.fragments.ReservationListFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DealsActivity extends com.aveplus.avepay_cpocket.base.BaseActivity implements DealListFragment.IListenerDealsChange {

    private ArrayList<DealObj> listData;
    private MenuItem menuMapItem;
    private Menu menu;
    private Fragment curFragment;
    private ReservationListFragment reservationListFragment;

    private String mIdDealCate;
    private String mNameCategory;
    private Bundle bundle;
    private boolean isAsc = true;
    private boolean defaultIsAsc =true;

    private DealListFragment dealListFragment;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.layout_content;
    }

    @Override
    protected void getExtraData(Intent intent) {
        bundle = intent.getExtras();
        if (bundle.containsKey(Args.KEY_ID_DEAL_CATE)) {
            mIdDealCate = bundle.getString(Args.KEY_ID_DEAL_CATE);
        }

        if (bundle.containsKey(Args.TYPE_OF_DEAL_NAME)) {
            mNameCategory = bundle.getString(Args.TYPE_OF_DEAL_NAME);
        }
        Log.d("DealsActivity", "onCreateGetIntentExtra" + bundle);
    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {
        if (bundle.containsKey(Args.KEY_KEY_WORD)) {
            setToolbarTitle(R.string.search_results);
        } else {
            setToolbarTitle(mNameCategory);
        }

        if (bundle.containsKey(Args.TYPE_OF_SEARCH_DEAL)) {
            if (bundle.get(Args.TYPE_OF_SEARCH_DEAL).equals(Constants.SOLD)) {
                reservationListFragment = ReservationListFragment.newInstance(bundle);
                replaceFragment(reservationListFragment);
                setToolbarTitle(R.string.sold);
            } else {
                dealListFragment = DealListFragment.newInstance(bundle);
                replaceFragment(dealListFragment);
            }
        }


    }

    @Override
    protected void onViewCreated() {
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (curFragment instanceof DealListByMapFragment) {
                    if (dealListFragment == null)
                        dealListFragment = DealListFragment.newInstance(bundle);
                    replaceFragment(dealListFragment);
                    menuMapItem.setIcon(R.drawable.ic_location_on_white);
                    menu.setGroupEnabled(R.id.action_group_sort, true);
                } else {
                    finish();
                }
            }
        });
    }

    private void replaceFragment(Fragment fragment) {
        curFragment = fragment;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, fragment);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_deal_filter, menu);
        MenuItem item = menu.findItem(R.id.action_view_on_map);
        if (item != null) {
            menuMapItem = item;
            if (mIdDealCate != null)
                if (mIdDealCate.equals(DealCateObj.FOOD_AND_BEVERAGES)) {
                    menuMapItem.setVisible(true);
                }
        }
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d("DealActivity", "onOptionsItemSelected: " + dealListFragment);

        if (id == R.id.action_by_rating) {
            isAsc = !isAsc;
            if (dealListFragment != null) dealListFragment.sortDealSoldList(1,isAsc);
            else if (reservationListFragment != null) reservationListFragment.sortDealSoldList(1,isAsc);
//            dealListFragment.sortData(Constants.SORT_RATE, Constants.SORT_DESC);

        } else if (id == R.id.action_by_distance) {
//            dealListFragment.sortData(Constants.SORT_DISTANCE, Constants.SORT_ASC);
            isAsc = !isAsc;
            if (dealListFragment != null) dealListFragment.sortDealSoldList(0,isAsc);
            else if (reservationListFragment != null) reservationListFragment.sortDealSoldList(0,isAsc);
        } else if (id == R.id.action_by_name) {
//            dealListFragment.sortData(Constants.SORT_NAME, Constants.SORT_ASC);
            isAsc = !isAsc;
            if (dealListFragment != null) dealListFragment.sortDealSoldList(2,isAsc);
            else if (reservationListFragment != null) reservationListFragment.sortDealSoldList(2,isAsc);
        } else if (id == R.id.action_by_price) {
//            dealListFragment.sortData(Constants.SORT_PRICE, Constants.SORT_ASC);
            isAsc = !isAsc;
            if (dealListFragment != null) dealListFragment.sortDealSoldList(3,isAsc);
            else if (reservationListFragment != null) reservationListFragment.sortDealSoldList(3,isAsc);
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_view_on_map) {
            if (curFragment instanceof DealListByMapFragment) {
                if (dealListFragment == null)
                    dealListFragment = DealListFragment.newInstance(bundle);
                replaceFragment(dealListFragment);
                menuMapItem.setIcon(R.drawable.ic_location_on_white);
                menu.setGroupEnabled(R.id.action_group_sort, true);
            } else {
                replaceFragment(DealListByMapFragment.newInstance(getListData()));
                menuMapItem.setIcon(R.drawable.ic_all_deals);
                menu.setGroupEnabled(R.id.action_group_sort, false);
            }
        }

        return super.onOptionsItemSelected(item);
    }


    public ArrayList<DealObj> getListData() {
        if (listData == null)
            listData = new ArrayList<>();
        return listData;
    }

    @Override
    public void onChanged(ArrayList<DealObj> mDealObj) {
        listData = mDealObj;
    }

    private void sortByRating() {
        Collections.sort(listData, new Comparator<DealObj>() {
            @Override
            public int compare(DealObj o1, DealObj o2) {
                return (int) (o1.getRate() - o2.getRate());
            }
        });
        dealListFragment.refreshData(listData);

    }

    private void sortByName() {
        Collections.sort(listData, new Comparator<DealObj>() {
            @Override
            public int compare(DealObj o1, DealObj o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        dealListFragment.refreshData(listData);

    }

    private void sortByPrice() {
        Collections.sort(listData, new Comparator<DealObj>() {
            @Override
            public int compare(DealObj o1, DealObj o2) {
                return (int) (o1.getPrice() - o2.getPrice());
            }
        });
        dealListFragment.refreshData(listData);
    }


}
