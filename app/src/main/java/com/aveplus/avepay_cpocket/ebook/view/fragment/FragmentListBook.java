package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseListFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.SingleAdapter;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.ListBookVM;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemBookVM;

/**
 * Created by Suusoft on 9/8/2017.
 */
public class FragmentListBook extends BaseListFragmentBinding {

    private ListBookVM viewModel;
    private static boolean mLoadMore;

    public static FragmentListBook newInstance(boolean loadMore) {
        mLoadMore = loadMore;
        Bundle args = new Bundle();
        FragmentListBook fragment = new FragmentListBook();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(self));
        SingleAdapter adapter = new SingleAdapter(self, R.layout.item_book_grid_all, viewModel.getListData(), ItemBookVM.class);
        recyclerView.setAdapter(adapter);
    }

    public void setUpListOrGrid(int type) {

    }


    @Override
    protected void initialize() {

    }


    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new ListBookVM(self, getArguments());
        return viewModel;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 69 && resultCode == Activity.RESULT_OK) {
            // getmAdapter().removeAll();
            viewModel.onRefresh();
        }
    }
}
