package com.aveplus.avepay_cpocket.view.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.aveplus.avepay_cpocket.PacketUtility;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.ApiResponse;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.modelmanager.ModelManagerListener;
import com.aveplus.avepay_cpocket.network1.MyProgressDialog;
import com.aveplus.avepay_cpocket.network1.NetworkUtility;
import com.aveplus.avepay_cpocket.objects.UserObj;
import com.aveplus.avepay_cpocket.quickblox.SharedPreferencesUtil;
import com.aveplus.avepay_cpocket.quickblox.chat.ChatHelper;
import com.aveplus.avepay_cpocket.retrofit.ApiUtils;
import com.aveplus.avepay_cpocket.retrofit.response.ResponseUser;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.aveplus.avepay_cpocket.utils.StringUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewCondensedItalic;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_GOOGLE_SIGN_IN = 9001;
    private static final int RC_PERMISSIONS = 1;
    private static final int RC_SIGN_IN = 1;
    private LinearLayout layoutScreen;
    private EditText mTxtEmail, mTxtPassword;
    private TextView mLblLogin, mLblFacebook, mLblGoogle;
    private TextViewCondensedItalic mLblForget, mLblNoAccount;
    private CheckBox mChkRememberMe;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager mCallbackManager;
    private RequestQueue mRequestQueue;
    private String mEmail = "", mPassword = ""; // This vars save credential from 'SignUpActivity.java'
    private boolean isFromSignUp = false;
    private Bundle bundle;
    AccessTokenTracker tokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if (currentAccessToken == null) {

                Toast.makeText(self, "logout", Toast.LENGTH_SHORT).show();
            } else {
                loadUserProfile(currentAccessToken);
            }
        }
    };
    private ProgressBar progressBar;
    private View mClickedView; // Keep button which was just clicked(google, facebook or login)
    //google login firebase
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequestQueue = Volley.newRequestQueue(self);
        initGoogleApiClient();


        //tich hợp login google firebase
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        callbackManager = CallbackManager.Factory.create();

    }

    private void initGoogleApiClient() {
        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(self, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .enableAutoManage(this, this)
                .build();
        // [END build_client]
    }

    @Override
    protected void inflateLayout() {
        setContentView(R.layout.activity_login_reskin);
    }

    @Override
    protected void getExtraValues() {
        super.getExtraValues();

//        bundle = getIntent().getExtras();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
//        bundle = intent.getExtras();
    }

    @Override
    protected void initUI() {
        // Hide actionbar
        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        progressBar = findViewById(R.id.progress);
        layoutScreen = (LinearLayout) findViewById(R.id.layout_screen);
        mTxtEmail = (EditText) findViewById(R.id.txt_email);
        mTxtPassword = (EditText) findViewById(R.id.txt_password);
        mLblLogin = (TextView) findViewById(R.id.lbl_login);
        mLblFacebook = (TextView) findViewById(R.id.lbl_facebook_login);
        mLblGoogle = (TextView) findViewById(R.id.lbl_google_login);
        mLblForget = (TextViewCondensedItalic) findViewById(R.id.lbl_forget_password);
        mLblNoAccount = (TextViewCondensedItalic) findViewById(R.id.lbl_no_account_yet);
        mChkRememberMe = (CheckBox) findViewById(R.id.chk_remember_me);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String email = bundle.getString("email");
            String password = bundle.getString("password");
            mTxtEmail.setText(email);
            mTxtPassword.setText(password);
        }


        layoutScreen.getLayoutParams().height = AppUtil.getScreenHeight(this) - AppUtil.getStatusBarHeight(this);
        Log.e("screen", layoutScreen.getLayoutParams().height + "px");
        Log.e("screen heightStatusBar", AppUtil.getStatusBarHeight(this) + "px");

        // Fill credential automatically
        if (isFromSignUp) {
            if (DataStoreManager.getUser() != null && DataStoreManager.getUser().isRememberMe()) {
                mTxtEmail.setText(mEmail);
                mTxtPassword.setText(mPassword);
            }
        } else {
            if (DataStoreManager.getUser() != null && DataStoreManager.getUser().isRememberMe()) {
                if (!DataStoreManager.getUser().getEmail().isEmpty()) {
                    mTxtEmail.setText(DataStoreManager.getUser().getEmail());
                    Log.e("EE", "EEE");
                }
                Log.e("EE", "EEE");
            }
        }
    }

    @Override
    protected void initControl() {
        mLblLogin.setOnClickListener(this);
        mLblFacebook.setOnClickListener(this);
        mLblGoogle.setOnClickListener(this);
        mLblForget.setOnClickListener(this);
        mLblNoAccount.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        GlobalFunctions.startActivityWithoutAnimation(self, SplashLoginActivity.class);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSIONS: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED
                            || grantResults[1] == PackageManager.PERMISSION_DENIED) {
                        showPermissionsReminder(RC_PERMISSIONS, true);
                    } else {
                        if (mClickedView == mLblGoogle) {
//                            signInGoogle();
                            Toast.makeText(this, "tét", Toast.LENGTH_SHORT).show();
//                            Log.e(TAG, "vào 1: " );
                            signInGoogleNew();
                        } else if (mClickedView == mLblFacebook) {
                            signInFacebook();
                        } else if (mClickedView == mLblLogin) {
//                            login();
                            loginMain();
                        }
                    }
                }
                break;
            }
            default:
                break;
        }
    }

    private void showProgressDialog(boolean isShow) {
        progressBar.setVisibility(!progressBar.isShown() && isShow ? View.VISIBLE : View.GONE);
    }

    private void loginMain() {
        String email = mTxtEmail.getText().toString();
        String password = mTxtPassword.getText().toString();
        if (email.isEmpty() || password.isEmpty()) {
            AppUtil.showToast(self, "Login error");
            return;
        } else {
            showProgressDialog(true);
            ApiUtils.getAPIService().login(email, password, "n", "").enqueue(new Callback<ResponseUser>() {
                @Override
                public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                    if (response.body().isSuccess(self)) {
                        if (response.body().getData() != null) {
                            UserObj userObj = response.body().getData();
                            Log.d(TAG, "onResponseLogIn: " + userObj.getEmail() + " " + userObj.getToken());
                            userObj.setEmail(email);
                            userObj.setToken(response.body().getData().getToken());
                            DataStoreManager.saveToken(response.body().getLogin_token());
                            userObj.setRememberMe(mChkRememberMe.isChecked());
                            DataStoreManager.saveUser(userObj);
                            QBUser qbUser = new QBUser();
                            qbUser.setLogin(userObj.getEmail());
                            qbUser.setPassword(getResources().getString(R.string.QB_DEFAULT_PASSWORD));
                            qbUser.setFullName(userObj.getName());
                            if (userObj.getId().equals("0")) {

                                QBUsers.signUp(qbUser).performAsync(new QBEntityCallback<QBUser>() {

                                    @Override
                                    public void onSuccess(QBUser qbUser, Bundle bundle) {
                                        ChatHelper.getInstance().login(LoginActivity.this, qbUser, new QBEntityCallback<Void>() {

                                            @Override
                                            public void onSuccess(Void aVoid, Bundle bundle) {
                                                ModelManager.updateQuickbloxId(LoginActivity.this, userObj.getId(), response.body().getLogin_token(), new ModelManagerListener() {
                                                    @Override
                                                    public void onSuccess(Object object) {
                                                        showProgressDialog(false);
                                                        SharedPreferencesUtil.saveQbUser(LoginActivity.this, qbUser);
                                                        gotoHomePage();
                                                    }

                                                    @Override
                                                    public void onError() {
                                                        showProgressDialog(false);
                                                        AppUtil.showToast(LoginActivity.this, "Have some unknown error when update QB Chat");
                                                    }
                                                });
                                            }

                                            @Override
                                            public void onError(QBResponseException e) {
                                                showProgressDialog(false);
                                                AppUtil.showToast(LoginActivity.this, e.getMessage());
                                            }
                                        });

                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        showProgressDialog(false);
                                        AppUtil.showToast(LoginActivity.this, e.getMessage());
                                    }
                                });
                            } else {
                                qbUser.setId(Integer.parseInt(userObj.getId()));
                                ChatHelper.getInstance().login(LoginActivity.this, qbUser, new QBEntityCallback<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid, Bundle bundle) {
                                        showProgressDialog(false);
                                        SharedPreferencesUtil.saveQbUser(LoginActivity.this, qbUser);
                                        gotoHomePage();
                                    }

                                    @Override
                                    public void onError(QBResponseException e) {
                                        showProgressDialog(false);
                                        Log.d(TAG, "onErrorChatLogin: "+e.getMessage()+ " "+e.getErrors());
                                    }
                                });
                            }


                        } else {
                            Toast.makeText(LoginActivity.this, "Incorrect account or password.", Toast.LENGTH_SHORT).show();
                        }
//                        progressBar.setVisibility(View.GONE);

                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, "Incorrect account or password.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseUser> call, Throwable t) {
                    showProgressDialog(false);
                    Toast.makeText(LoginActivity.this, "Incorrect account or password.", Toast.LENGTH_SHORT).show();

                    Log.e(TAG, "onResponse: " + t.getMessage());
                }
            });
        }

    }

    @Override
    public void onClick(View v) {
        if (v == mLblLogin) {
            mClickedView = mLblLogin;
            if (isValid()) {
                // Check permissions
                if (GlobalFunctions.isGranted(self, new String[]{Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION}, RC_PERMISSIONS, "")) {
//                    login();
                    loginMain();
                }
            }
        } else if (v == mLblForget) {
            showDialogForgot();
        } else if (v == mLblNoAccount) {
            GlobalFunctions.startActivityWithoutAnimation(self, SignUpActivity.class);
            finish();
        } else if (v == mLblGoogle) {
            mClickedView = mLblGoogle;

            // Check permissions
            if (GlobalFunctions.isGranted(self, new String[]{Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSIONS, "")) {
                signInGoogleNew();
                Log.e(TAG, "vào rồi ");
            }
        } else if (v == mLblFacebook) {
            mClickedView = mLblFacebook;

            // Check permissions
            if (GlobalFunctions.isGranted(self, new String[]{Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSIONS, "")) {
                signInFacebook();
//                signInFacebookNew();
                Log.e(TAG, "login FB");
            }
        }
    }

    private void signInFacebookNew() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResultNew(result);
        }


        if (mCallbackManager != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);


        }


    }

    private void loadUserProfile(AccessToken newAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                try {
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
//                    String email = object.getString("email");
                    String email = "";
                    if (object.has("email")) {
                        email = object.getString("email");
                    }
                    String id = object.getString("id");

                    String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

//                    txtName.setText(first_name + " " + last_name);
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.dontAnimate();
                    // Glide.with(self).load(image_url).into(circleImageViewInfor);

                    DataStoreManager.init(self);
                    UserObj user = new UserObj();
                    user.setId(id);
                    user.setName(first_name + " " + last_name);

                    user.setEmail(email);

                    user.setAvatar(image_url);
                    DataStoreManager.saveUser(user);
//                    AppUtil.startActivity(self, MainActivity.class);

                    ApiUtils.getAPIService().login(email, "", "s", first_name + last_name).enqueue(new Callback<ResponseUser>() {
                        @Override
                        public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                            if (response.body().isSuccess(self)) {
                                if (response.body() != null) {
                                    UserObj user = response.body().getData();
//                                user.setEmail(email);
                                    user.setAvatar(image_url);
                                    user.setToken(response.body().getData().getToken()); //Token
                                    DataStoreManager.saveUser(user);
                                    DataStoreManager.saveToken(response.body().getLogin_token());
//                                    notificationCar();
                                    gotoHomePage();

                                } else {
                                    Log.e(TAG, "error1: ");
                                }
                            } else {
                                Log.e(TAG, "error2: ");
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseUser> call, Throwable t) {
                            AppUtil.showToast(self, "Tài khoản hoặc mật khẩu không đúng");
                            Log.e(TAG, "onFailure: " + t.getMessage());
                        }
                    });

                    Toast.makeText(self, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        Bundle bundle = new Bundle();
        bundle.putString("fields", "first_name, last_name, id, email");
        request.setParameters(bundle);
        request.executeAsync();
    }


    private void signInGoogleNew() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN);
    }

    private void handleSignInResultNew(GoogleSignInResult result) {
        Log.e(TAG, "vào không ?: ");
        if (result.isSuccess()) {
//            gotoProfile();
            GoogleSignInAccount acct = result.getSignInAccount();

            String email = "", name = "", avatar = "";
            if (acct.getEmail() != null) {
                email = acct.getEmail();
            }
            if (acct.getDisplayName() != null) {
                name = acct.getDisplayName();
            }
            if (acct.getPhotoUrl() != null) {
                avatar = acct.getPhotoUrl().toString();
            }

            if (!email.equals("")) {
                DataStoreManager.init(self);
                UserObj user = new UserObj();
                user.setName(name);
                user.setEmail(email);
                user.setAvatar(avatar);
                DataStoreManager.saveUser(user);
//                AppUtil.startActivity(self, AcceuilActivity.class);

                showProgressDialog(true);
                ApiUtils.getAPIService().login(email, "", "s", name).enqueue(new Callback<ResponseUser>() {
                    @Override
                    public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                        if (response.body().isSuccess(self)) {
                            if (response.body() != null) {
                                UserObj user = response.body().getData();
                                user.setAvatar(acct.getPhotoUrl().toString());
                                user.setToken(response.body().getData().getToken()); //Token
                                DataStoreManager.saveUser(user);
                                DataStoreManager.saveToken(response.body().getLogin_token());
                                QBUser qbUser = new QBUser();
                                qbUser.setLogin(user.getEmail());
                                qbUser.setPassword(getResources().getString(R.string.QB_DEFAULT_PASSWORD));
                                qbUser.setFullName(user.getName());
                                MyProgressDialog progressDialog = new MyProgressDialog(LoginActivity.this);

                                if (user.getId().equals("0")) {

                                    QBUsers.signUp(qbUser).performAsync(new QBEntityCallback<QBUser>() {

                                        @Override
                                        public void onSuccess(QBUser qbUser, Bundle bundle) {
                                            ChatHelper.getInstance().login(LoginActivity.this, qbUser, new QBEntityCallback<Void>() {

                                                @Override
                                                public void onSuccess(Void aVoid, Bundle bundle) {
                                                    ModelManager.updateQuickbloxId(LoginActivity.this, user.getId(), response.body().getLogin_token(), new ModelManagerListener() {
                                                        @Override
                                                        public void onSuccess(Object object) {
                                                            Log.d(TAG, "onSuccessLoginChat: ");
                                                            showProgressDialog(false);
                                                            SharedPreferencesUtil.saveQbUser(LoginActivity.this, qbUser);
                                                            gotoHomePage();
                                                        }

                                                        @Override
                                                        public void onError() {
                                                            showProgressDialog(false);
                                                            AppUtil.showToast(LoginActivity.this, "Have some unknown error when update QB Chat");
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onError(QBResponseException e) {
                                                    showProgressDialog(false);
                                                    AppUtil.showToast(LoginActivity.this, e.getMessage());
                                                }
                                            });

                                        }

                                        @Override
                                        public void onError(QBResponseException e) {
                                            showProgressDialog(false);
                                            AppUtil.showToast(LoginActivity.this, e.getMessage());
                                        }
                                    });
                                } else {
                                    qbUser.setId(Integer.parseInt(user.getId()));
                                    ChatHelper.getInstance().login(LoginActivity.this, qbUser, new QBEntityCallback<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid, Bundle bundle) {
                                            Log.d(TAG, "onSuccessLoginChat: ");
                                            showProgressDialog(false);
                                            SharedPreferencesUtil.saveQbUser(LoginActivity.this, qbUser);
                                            gotoHomePage();
                                        }

                                        @Override
                                        public void onError(QBResponseException e) {
                                            showProgressDialog(false);
                                            Log.d(TAG, "onError: "+e.getMessage());
                                        }
                                    });
                                }

//                                notificationCar();
                            } else {
                                Log.e(TAG, "error1: ");
                            }
                        } else {
                            Log.e(TAG, "error2: ");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseUser> call, Throwable t) {
                        AppUtil.showToast(self, t.getMessage());
                        Log.e(TAG, "onFailure: " + t.getMessage());
                    }
                });

            } else {
                Toast.makeText(self, R.string.msg_can_not_get_email, Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(self, "Sign in cancel", Toast.LENGTH_LONG).show();
        }
    }


    private void signInFacebook() {
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            mCallbackManager = CallbackManager.Factory.create();

            if (AccessToken.getCurrentAccessToken() != null) {
                // Sign out then sign in again
                LoginManager.getInstance().logOut();
            }

            LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    String accessToken = loginResult.getAccessToken().getToken();
                    ModelManager.getFacebookInfo(self, mRequestQueue, accessToken, new ModelManagerListener() {
                        @Override
                        public void onSuccess(Object object) {
                            Log.e("Splash", "onSuccess login fb1  obj -- " + object.toString());
                            JSONObject jsonObject = (JSONObject) object;

                            String email = jsonObject.optString("email");
                            String name = jsonObject.optString("name");
                            String avatar = "";
                            try {
                                avatar = jsonObject.getJSONObject("picture").getJSONObject("data").optString("url");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (!email.equals("")) {
                                Log.e("Splash", "onSuccess login fb2");
                                login(name, email, "", avatar, UserObj.SOCIAL, PacketUtility.getImei(self));
                            } else {
                                Toast.makeText(self, R.string.msg_can_not_get_email, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError() {
                            Log.e("Splash", "onError login fb");
                            Toast.makeText(self, R.string.msg_have_some_errors, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onCancel() {
                    Log.e("Splash", "Facebook Login cancel");
                }

                @Override
                public void onError(FacebookException exception) {
                    Log.d("Splash", "Facebook Login error" + exception.getMessage() + " " + exception.getCause());

                }
            });
            //  LoginManager.getInstance().setLoginBehavior(LoginBehavior.SSO_WITH_FALLBACK);
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        } else {
            Toast.makeText(self, getString(R.string.msg_no_network), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isValid() {
        String email = mTxtEmail.getText().toString().trim();
        String password = mTxtPassword.getText().toString().trim();

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(self, R.string.msg_email_is_required, Toast.LENGTH_SHORT).show();
            mTxtEmail.requestFocus();

            return false;
        }
        if (password.isEmpty()) {
            Toast.makeText(self, R.string.msg_password_is_required, Toast.LENGTH_SHORT).show();
            mTxtPassword.requestFocus();

            return false;
        }

        return true;
    }


    private void login(String name, String email, String password, String avatar, final String loginMethod, String imei) {
        Log.e("Splash", "login");
        if (NetworkUtility.getInstance(self).isNetworkAvailable()) {
            ModelManager.login(self, mRequestQueue, email, password, name, avatar, loginMethod, imei, new ModelManagerListener() {
                @Override
                public void onSuccess(Object object) {
                    Log.e("Splash", "Login onSuccess");
                    JSONObject jsonObject = (JSONObject) object;
                    ApiResponse response = new ApiResponse(jsonObject);
                    if (!response.isError()) {
                        UserObj userObj = response.getDataObject(UserObj.class);
                        userObj.setToken(response.getValueFromRoot(Args.TOKEN));
                        // It's remember default if users login as social
                        userObj.setRememberMe(true);
                        Log.e(TAG, "user: ");
                        // Save qbId for user if it's the first time
                        if (userObj.getQb_id() == 0) {
                            if (SharedPreferencesUtil.hasQbUser()) {
                                userObj.setQb_id(SharedPreferencesUtil.getQbUser().getId());
//                                userObj.setQb_id(80813);
                            }
                        }

                        // Save user to preference
                        userObj.setRememberMe(false);
                        DataStoreManager.saveUser(userObj);

                        gotoHomePage();

                    } else {
                        Log.e("Splash", "response onError");
                        Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("PP", response.getMessage());
                    }
                }

                @Override
                public void onError() {

                    Log.e("Splash", "Login onError");
                    Toast.makeText(self, R.string.msg_have_some_errors, Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(self, getString(R.string.msg_no_network), Toast.LENGTH_SHORT).show();
        }
    }

    private void gotoHomePage() {
        if (bundle != null) {
            GlobalFunctions.startActivityWithoutAnimation(self, AcceuilActivity.class, bundle);
        } else {
            GlobalFunctions.startActivityWithoutAnimation(self, AcceuilActivity.class);
        }


        // Close this activity
        finish();
    }

    private void showDialogForgot() {
        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forgot_password);

        final EditText edtEmail = (EditText) dialog.findViewById(R.id.txt_email);
        TextView btnSubmit = (TextView) dialog.findViewById(R.id.tv_submit);
        final TextView tvError = (TextView) dialog.findViewById(R.id.tv_error);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edtEmail.getText().toString();
                if (!StringUtil.isValidEmail(email)) {
                    tvError.setVisibility(View.VISIBLE);
                    tvError.setText(getString(R.string.msg_email_is_required));
                    return;
                }
                requestForgotPasswork(email);
                dialog.dismiss();
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void requestForgotPasswork(String email) {
        ModelManager.forgotPassword(this, email, new ModelManagerListener() {
            @Override
            public void onSuccess(Object object) {
                JSONObject jsonObject = (JSONObject) object;
                ApiResponse response = new ApiResponse(jsonObject);
                if (!response.isError()) {
                    Toast.makeText(self, getString(R.string.msg_forget_password_success), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(self, response.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }


}
