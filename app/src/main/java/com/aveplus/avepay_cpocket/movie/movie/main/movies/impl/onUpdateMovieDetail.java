package com.aveplus.avepay_cpocket.movie.movie.main.movies.impl;


import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface onUpdateMovieDetail {
    public void onResponde(Movie movie);
    public void onError();
}
