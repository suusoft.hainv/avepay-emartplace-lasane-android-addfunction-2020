package com.aveplus.avepay_cpocket.movie.movie.configs;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public class Config {
    public static final boolean cho_phep_xac_thuc_sdt = true;
    public static final boolean cho_phep_kiem_tra_xac_thuc_acc = true;
    public static final boolean logout_co_nhan_push = false;


    //    public static final String URL_API = AppController.getInstance().getString(R.string.URL_API) + "backend/web/index.php/api/";
    public static final String URL_API = AppController.getInstance().getString(R.string.URL_API) + "backend/web/index.php/api/";
//    public static final String URL_API_LOCALHOST = AppController.getInstance().getString(R.string.URL_API_LOCALHOST) + "backend/web/index.php/api/";

    public static final int MAX_RESULT = 10;
    public static final String KEY_TOTAL_PAGE = "total_page";

    public static class Api {
        //register
        public static final String URL_REGISTER = Config.URL_API + "user/register";


        public static final String URL_SETTING = Config.URL_API + "utility/setting";
        public static final String URL_LOGIN = Config.URL_API + "user/login";
        public static final String URL_LOGOUT = Config.URL_API + "user/logout";
        public static final String URL_ACTIVE_ACCOUNT = Config.URL_API + "user/verify-account";
        public static final String URL_CHECK_PHONE = Config.URL_API + "user/check-phone";
        public static final String URL_FORGOT_PASSWORD = Config.URL_API + "user/forget-password";
        public static final String URL_CHANGE_PASSWORD = Config.URL_API + "user/change-password";
        public static final String URL_UPDATE_PROFILE = Config.URL_API + "user/update-profile";
        public static final String URL_UPDATE_IMAGE = Config.URL_API + "user/upload-image";

        public static final String URL_GET_BALANCE = Config.URL_API + "user/get-balance";
        public static final String URL_GET_REVIEW_DETAIL = Config.URL_API + "review/detail";
        public static final String URL_GET_SERVICE_DETAIL = Config.URL_API + "user/service-detail";
        public static final String URL_SERVICE_LIST = Config.URL_API + "user/service-list";
        public static final String URL_REGISTER_SERVICE = Config.URL_API + "user/service-add";
        public static final String URL_UPDATE_SERVICE = Config.URL_API + "user/service-update";
        public static final String URL_REGISTER_DEVICE = Config.URL_API + "device/index";
        public static final String URL_ORDER_HISTORY = Config.URL_API + "user/order-history";
        public static final String URL_SEARCH_SELLER_BY_KEY = Config.URL_API + "user/search-user";
        public static final String URL_SEARCH_SELLER_BY_SERVICE = Config.URL_API + "user/find-user-by-service";
        public static final String URL_HOME_USER = Config.URL_API + "user/home-user";
        public static final String URL_PROFILE = Config.URL_API + "user/profile";
        public static final String URL_BOOK_SERVICE = Config.URL_API + "user/order";
        public static final String URL_ORDER_OWED = Config.URL_API + "user/order-owed";
        public static final String URL_LIST_SERVICE = Config.URL_API + "user/service-by-user";
        public static final String URL_NOTIFICATION = Config.URL_API + "user/user-notification";
        public static final String URL_RATE = Config.URL_API + "review/index";
        public static final String URL_LIST_RATE = Config.URL_API + "review/list";
        public static final String URL_RESET_PASSWORD = Config.URL_API + "resetPassword";
        public static final String URL_ACCOUNT_BANK = Config.URL_API + "user/account-bank";
        public static final String URL_PAYMENT = Config.URL_API + "user/request-payment";
        public static final String URL_TRANSACTION_STATISTIC = Config.URL_API + "user/transaction-history";


    }


}
