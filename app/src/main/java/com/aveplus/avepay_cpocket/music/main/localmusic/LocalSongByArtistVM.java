package com.aveplus.avepay_cpocket.music.main.localmusic;

import android.content.Context;
import android.os.AsyncTask;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.QueueManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class LocalSongByArtistVM extends BaseViewModelList implements ItemMenuActionSongListener {

    private ArrayList<Song> list;
    private String id;

    public LocalSongByArtistVM(Context context, String id) {
        super(context);
        this.id = id;
    }

    @Override
    public void getData(int page) {
        new AsyncTask<String, String, ArrayList<Song>>() {

            @Override
            protected ArrayList<Song> doInBackground(String... strings) {
                list = LocalContentWrapper.getSongsByArtist(self, id);
                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Song> list) {
                super.onPostExecute(list);
                addListData(list);
                checkLoadingMoreComplete(1);

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().addNewQueue(list.get(position));
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {

    }
}
