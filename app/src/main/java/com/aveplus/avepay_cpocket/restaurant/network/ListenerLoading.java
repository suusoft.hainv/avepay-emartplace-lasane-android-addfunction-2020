package com.aveplus.avepay_cpocket.restaurant.network;


import com.aveplus.avepay_cpocket.restaurant.listener.IBaseListener;

/**
 * Created by suusoft.com on 1/26/18.
 */

public interface ListenerLoading extends IBaseListener {
    void onLoadingIsProcessing();
    void onLoadingIsCompleted();
}
