package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.squareup.picasso.Picasso;


public class DetailSaleOff extends BaseFragment {

    TextView tvTitle, tvContent;
    ImageView imgSale;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.detail_saleoff, container, false);
        intiView(view);
        intiData();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("detailsale");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("sale");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    public void intiView(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tvTitleDetailSale);
        tvContent = (TextView) v.findViewById(R.id.tvContentDetailSale);
        imgSale = (ImageView) v.findViewById(R.id.imageSaleOffDetail);
    }

    public void intiData() {
        Bundle bundle = this.getArguments();
        String title = bundle.getString("title");
        String content = bundle.getString("content");
        String image = bundle.getString("image");
        tvTitle.setText(title);
        tvContent.setText(content);
        Picasso.get().load(image).into(imgSale);
    }
}
