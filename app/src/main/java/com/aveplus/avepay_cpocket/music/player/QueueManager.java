package com.aveplus.avepay_cpocket.music.player;

import android.support.v4.media.MediaMetadataCompat;
import android.util.Log;


import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.model.Song;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Random;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class QueueManager {

    private static final String TAG = QueueManager.class.getSimpleName();


    public boolean isShuffle = false;
    public boolean isRepeat = false;

    // "Now playing" queue:
    private ArrayList<Song> mPlayingQueue;
    private int mCurrentIndex;

    private static QueueManager instance;

    private IQueueChangeListener listener;

    public interface IQueueChangeListener {
        void onItemSet();
    }

    public static QueueManager getInstance() {
        if (instance == null) {
            instance = new QueueManager();
        }
        return instance;
    }

    public QueueManager() {
    }


    public void nextSong() {
        if (isShuffle) {
            setPosCurrentSongRandom();
        } else {
            if (!isRepeat) {
                if (getPositionCurrentSong() < getLastIndexSong()) {
                    mCurrentIndex++;
                } else {
                    setFirstIndexSong();
                }
            }
        }

    }

    public void prevSong() {
        if (isShuffle) {
            setPosCurrentSongRandom();
        } else {
            if (!isRepeat) {
                if (getPositionCurrentSong() > 0) {
                    mCurrentIndex--;
                } else {
                    setLastIndexSong();
                }
            }
        }

    }

    public boolean remove(int index) {
        if (mPlayingQueue.size() > 1) {
            mPlayingQueue.remove(index);
            return true;
        }
        return false;
    }

    public void clear() {
        if (mPlayingQueue!=null)
            mPlayingQueue.clear();
    }

    public void addToQueue(Song item) {
        mPlayingQueue.add(item);
        notifyChange();
    }

    public void addToQueue(ArrayList<Song> items) {
        mPlayingQueue.addAll(items);
        notifyChange();
    }

    public void addNewQueue(Song item) {
        clear();
        addToQueue(item);
        setCurrentQueueIndex(0);

    }

    public void addNewQueue(ArrayList<Song> items) {
        addNewQueue(items, 0);
    }

    public void addNewQueue(ArrayList<Song> items, int position) {
        clear();
        addToQueue(items);
        setCurrentQueueIndex(position);
    }

    private void notifyChange() {
        EventBus.getDefault().post(new Global.ItemChanged());
        if (listener != null) {
            listener.onItemSet();
        }
    }


    private void setCurrentQueueIndex(int index) {
        if (index >= 0 && index < mPlayingQueue.size()) {
            mCurrentIndex = index;
        }
    }

    public boolean isShuffle() {
        return isShuffle;
    }

    public void setShuffle(boolean shuffle) {
        isShuffle = shuffle;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public void setRepeat(boolean repeat) {
        isRepeat = repeat;
    }

    // process position song.
    public int getPositionCurrentSong() {
        return mCurrentIndex;
    }

    public void setLastIndexSong() {
        mCurrentIndex = mPlayingQueue.size() - 1;
    }

    public void setFirstIndexSong() {
        mCurrentIndex = 0;
    }

    public void setPosCurrentSongRandom() {
        mCurrentIndex = new Random().nextInt(mPlayingQueue.size());
    }

    public void setPositionCurrentItem(int mCurrentIndex) {
        this.mCurrentIndex = mCurrentIndex;
    }

    public int getLastIndexSong() {
        return mPlayingQueue.size() - 1;
    }

    /****
     * @return current song is playing.
     */

    public void setPlayingQueue(ArrayList<Song> list) {
        mPlayingQueue = list;
    }

    public ArrayList<Song> getPlayingQueue() {
        if (mPlayingQueue == null)
            mPlayingQueue = new ArrayList<>();
        return mPlayingQueue;
    }

//    public Song getCurrentItem() {
//        return getPlayingQueue().get(mCurrentIndex);
//    }


    public Song getCurrentItem() {
        if (getPlayingQueue().size() > 0) {
            if (mCurrentIndex >= getPlayingQueue().size())
                mCurrentIndex = 0;

            Log.e(TAG, mCurrentIndex + "");
            return getPlayingQueue().get(mCurrentIndex);
        } else {
            return null;
        }
    }


    public MediaMetadataCompat getMetadataCurrentItem(long durations) {
        MediaMetadataCompat copy = new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, getCurrentItem().id)
                .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE, getCurrentItem().title)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, getCurrentItem().title)
                .putString(MediaMetadataCompat.METADATA_KEY_ART_URI, getCurrentItem().image)
                .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON_URI, getCurrentItem().image)
                .putString(MediaMetadataCompat.METADATA_KEY_DISPLAY_SUBTITLE, getCurrentItem().getNameSinger())
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, getCurrentItem().nameSinger)
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, durations)
                .build();

        return copy;
    }

    public String getUrlItem() {
        return mPlayingQueue.get(mCurrentIndex).song_file;
    }

    public IQueueChangeListener getListener() {
        return listener;
    }

    public void setListener(IQueueChangeListener listener) {
        this.listener = listener;
    }
}
