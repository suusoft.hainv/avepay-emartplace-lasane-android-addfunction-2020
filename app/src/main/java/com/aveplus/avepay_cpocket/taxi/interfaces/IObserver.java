package com.aveplus.avepay_cpocket.taxi.interfaces;

/**
 * Created by SuuSoft.com on 27/02/2017.
 */

public interface IObserver {
    void update();
}
