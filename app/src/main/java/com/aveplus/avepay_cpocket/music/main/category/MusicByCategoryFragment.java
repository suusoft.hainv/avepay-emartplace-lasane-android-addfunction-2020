package com.aveplus.avepay_cpocket.music.main.category;

import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentNavBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.main.home.ItemSongsVM;
import com.aveplus.avepay_cpocket.music.model.Category;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class MusicByCategoryFragment extends BaseListFragmentNavBinding {

    private static final String KEY_OBJECT = "OBJECT";
    MusicByCategoryVM viewModel;
    Category category;

    public static MusicByCategoryFragment newInstance(Category category) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_OBJECT, category);
        MusicByCategoryFragment fragment = new MusicByCategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new MusicByCategoryVM(self, category);
        return viewModel;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        setTitle(category.name);
        showHideActionMenu(false);
        recyclerView.setAdapter(new SingleAdapter(self, R.layout.item_song, viewModel.getListData(), ItemSongsVM.class, viewModel.getActionListener()));
    }

    @Override
    protected void initialize() {
        category = getArguments().getParcelable(KEY_OBJECT);
    }


}
