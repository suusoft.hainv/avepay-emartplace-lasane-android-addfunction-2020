package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.activity.HomeActivity;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.CartConfirmAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.cart.CartManager;
import com.aveplus.avepay_cpocket.trainingcenter.config.FruitySharedPreferences;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.Items;
import com.aveplus.avepay_cpocket.trainingcenter.object.ObjectInfoUser;
import com.aveplus.avepay_cpocket.trainingcenter.object.Pro;
import com.aveplus.avepay_cpocket.utils.AppUtil;
import com.google.gson.Gson;


import java.util.ArrayList;

public class FragmentOrderCartStepConfirm extends BaseFragment implements OnClickListener {
    public static final String MESSAGE_SUCCESS = "success";

    private View view;
    private CartConfirmAdapter adapter;
    private ListView lvCart;
    private TextView tvUserName, tvUserPhone, tvUserPostCode, tvUserAddress, tvUserMail, tvTotalPrice;
    private Button btnOrder;
    private Bundle mBundle;
    private ObjectInfoUser objectInfoUser;
    private double totalPrice;
    private HomeActivity mEducationActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_order_step_confirm, container, false);
        initUI(view);
        initData(view);
        getData();
        initControl();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof HomeActivity) {
            mEducationActivity = (HomeActivity) activity;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("ordercart");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("cart");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    private void getData() {
        mBundle = getArguments();
        objectInfoUser = mBundle.getParcelable("object");
        totalPrice = mBundle.getDouble("total");
    }

    private void initUI(View v) {
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvUserPhone = (TextView) findViewById(R.id.tvPhoneNumber);
        tvUserAddress = (TextView) findViewById(R.id.tvAddress);
        tvUserPostCode = (TextView) findViewById(R.id.tvPostCode);
        tvUserMail = (TextView) findViewById(R.id.tvUserEmail);
        tvTotalPrice = (TextView) findViewById(R.id.tvTotalPrice);
        btnOrder = (Button) findViewById(R.id.btn_order);
    }

    private void initControl() {
        btnOrder.setOnClickListener(this);
        tvUserName.setText(objectInfoUser.getNameUser());
        tvUserPhone.setText(objectInfoUser.getPhoneUser());
        tvUserAddress.setText(objectInfoUser.getAddressUser());
        tvUserPostCode.setText(objectInfoUser.getPostCodeUser());
        tvUserMail.setText(objectInfoUser.getMailUser());
        tvTotalPrice.setText("$" + String.format("%.2f", totalPrice));
    }

    private View findViewById(int idRes) {
        return view.findViewById(idRes);
    }


    @Override
    public void onClick(View v) {
        if (v == btnOrder) {
            processOrder();
        }
    }

    private void processOrder() {
        ArrayList<Pro> mListPro = new ArrayList<>();
        for (int i = 0; i < CartManager.getInstance().getYourCart().size(); i++) {
            Pro pro = new Pro(Integer.parseInt(CartManager.getInstance().getYourCart().get(i).getId()), CartManager.getInstance().getYourCart().get(i).getName(), String.valueOf(CartManager.getInstance().getYourCart().get(i).getNumberCart()), CartManager.getInstance().getYourCart().get(i).getPrice(), CartManager.getInstance().getYourCart().get(i).getIdClassChose());
            mListPro.add(pro);
        }
        Items item = new Items();
        item.setData(mListPro);

        Gson gson = new Gson();
        String items = gson.toJson(item);
        ModelManager.order(objectInfoUser.getNameUser(), objectInfoUser.getPhoneUser(),
                objectInfoUser.getAddressUser(), objectInfoUser.getMailUser(), objectInfoUser.getPostCodeUser(),
                objectInfoUser.getNameReceiver(), objectInfoUser.getPhoneReceiver(), objectInfoUser.getAddressReceiver(),
                objectInfoUser.getMailReceiver(), objectInfoUser.getPostCodeReceiver(), "",
                FruitySharedPreferences.getInstance(getActivity()).getStringValue("idSetting"), "1",
                String.valueOf(totalPrice), "1", items, new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        Toast.makeText(getActivity(), getString(R.string.order_sucess), Toast.LENGTH_SHORT).show();
                        CartManager.getInstance().clearCart();
                        mEducationActivity.changeTab(HomeActivity.TAB_HOME);
                    }

                    @Override
                    public void onError(String message) {
                        AppUtil.showToast(self,message);
                    }
                });
    }

    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        for (Fragment frag : fm.getFragments()) {
            if (frag != null) {
                getActivity().overridePendingTransition(R.anim.slide_in_left,
                        R.anim.slide_out_right);
                if (frag.isVisible()) {
                    FragmentManager childFm = frag.getChildFragmentManager();
                    if (childFm.getBackStackEntryCount() > 0) {
                        childFm.popBackStack(null,
                                FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        childFm.beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_left,
                                        R.anim.slide_out_right).commit();
                        return;

                    }

                }

            }
        }

        if (fm.getBackStackEntryCount() > 0) {

            Log.i("MainActivity", "popping backstack");
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            new AlertDialog.Builder(getActivity())
                    .setMessage(getString(R.string.quit_app_message))
                    .setPositiveButton(getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(
                                            R.anim.slide_in_left,
                                            R.anim.slide_out_right);
                                }
                            }).setNegativeButton(getString(R.string.no), null)
                    .show();
        }
    }

    private void initData(View v) {
        lvCart = (ListView) v.findViewById(R.id.listCartOrder);
        adapter = new CartConfirmAdapter(getActivity(), R.layout.item_cart_confirm,
                CartManager.getInstance().getYourCart(), new CartConfirmAdapter.UpdateQuantityListener() {

            public void onUpdate(boolean isPlus, int index) {
                adapter.notifyDataSetChanged();
            }
        });
        lvCart.setAdapter(adapter);
    }

}
