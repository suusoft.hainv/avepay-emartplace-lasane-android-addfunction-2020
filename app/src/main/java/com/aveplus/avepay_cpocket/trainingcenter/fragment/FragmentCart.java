package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.adapter.ListCartAdapter;
import com.aveplus.avepay_cpocket.trainingcenter.cart.CartManager;
import com.aveplus.avepay_cpocket.trainingcenter.config.FruitySharedPreferences;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ParseUtility;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;
import com.aveplus.avepay_cpocket.trainingcenter.object.Setting;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.util.ArrayList;

public class FragmentCart extends BaseFragment {
    public static ArrayList<Product> list = new ArrayList<Product>();
    private ListView lvCart;
    private ListCartAdapter adapter;

    private Button btorder;
    private TextView textTotalOrder, lblVat;
    private double totalPrice, Price, priceVAT, priceTransport;
    private String VAT, transportFee;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("cart");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        lvCart.clearFocus();
        textTotalOrder.clearComposingText();
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_carl, container, false);
        init(view);
        initControl();
        initData();
        getData();
        return view;
    }

    private void init(View view) {
        lvCart = (ListView) view.findViewById(R.id.lvCart);
        btorder = (Button) view.findViewById(R.id.btOrder);
        textTotalOrder = (TextView) view.findViewById(R.id.textTotalOrder);
        lblVat = (TextView) view.findViewById(R.id.lblVat);

    }

    private void initControl() {
        btorder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentOrderCartStepInfo order = new FragmentOrderCartStepInfo();
                if (CartManager.getInstance().size() > 0) {

                    Bundle bundle = new Bundle();
                    bundle.putDouble("total", totalPrice);
                    bundle.putDouble("vat", priceVAT);
                    bundle.putDouble("ship", priceTransport);

                    order.setArguments(bundle);

                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_left,
                                    R.anim.slide_out_left)
                            .replace(R.id.layout_replace, order, "showOrder")
                            .addToBackStack(null).commit();
                } else {
                    Toast.makeText(getActivity(),
                            R.string.message_no_item_menu, Toast.LENGTH_SHORT)
                            .show();

                }

            }
        });
    }

    private void getData() {
        ModelManager.getSetting(new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                Setting setting = new Setting();
                setting = ParseUtility.parseSetting(response.getRootStr());
                VAT = setting.getVat();
                FruitySharedPreferences.getInstance(getActivity()).putStringValue("idSetting", setting.getId());
                Log.d(VAT, "VAT:");
                transportFee = setting.getTransportFee();
                Log.d(transportFee, "transportFee:");
                priceVAT = Double.parseDouble(VAT);
                priceTransport = Double.parseDouble(transportFee);
                updateTotal();
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }

    public void updateTotal() {
        totalPrice = 0;
        Price = 0;
        if (CartManager.getInstance().size() != 0) {
            for (Product item : CartManager.getInstance().getYourCart()) {
                Price += (item.getNumberCart() * Double.parseDouble(item
                        .getPrice()));
            }
        }
        totalPrice = Price + (Price * priceVAT / 100);
        textTotalOrder.setText(self.getString(R.string.dola) + String.format("%.2f", Float.parseFloat("" + totalPrice)));
        lblVat.setText(self.getString(R.string.vat) + " :" + String.format("%.2f", Float.parseFloat(priceVAT + "")) + "%");

    }

    private void initData() {

        adapter = new ListCartAdapter(getActivity(), R.layout.item_carl,
                CartManager.getInstance().getYourCart(), new ListCartAdapter.UpdateQuantityListener() {

            @Override
            public void onUpdate(boolean isPlus, int index) {
                if (isPlus) {
                    updateTotal();
                } else {
                    if (index == 1) {
                        updateTotal();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
        lvCart.setAdapter(adapter);
    }
}
