package com.aveplus.avepay_cpocket.ebook.view.adapter;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;

import java.util.List;

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.ViewHolder> {
    public static final int START = 1;
    public static final int CONTINUES = 2;
    private Activity activity;
    private List<Chapter> mListChapter;
    private IOnItemClick iOnItemClick;


    public interface IOnItemClick {
        public void onClick(int position, int flag);
    }

    public ChapterAdapter(Activity activity, List<Chapter> listChapter) {
        this.activity = activity;
        this.mListChapter = listChapter;

    }

    public void setIOnItemClick(ChapterAdapter.IOnItemClick IOnItemClick) {
        this.iOnItemClick = IOnItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_chapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Chapter chapter = mListChapter.get(position);
        AppUtil.setImage(holder.imvAvatar, chapter.getImage());
        holder.tvTitle.setText(chapter.getTitle());
        holder.tvDescription.setText(Html.fromHtml(chapter.getDescription()));
        String type = chapter.getType();
        if (type.equals(Chapter.TYPE_EPUB) || type.equals(Chapter.TYPE_NORMAL) || type.equals(Chapter.TYPE_PDF)) {
            holder.imvPlay.setVisibility(View.GONE);
        } else {
            holder.imvAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (null != iOnItemClick) {
                        iOnItemClick.onClick(holder.getAdapterPosition(), START);
                    }
                }
            });
        }
//        holder.tvRead.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (null != iOnItemClick) {
//                    iOnItemClick.onClick(holder.getAdapterPosition(),CONTINUES);
//                }
//            }
//        });

        /*Show button continue with file epub*/
        if (mListChapter.get(position).getType().equals(Chapter.TYPE_EPUB)) {
            if (!DataStoreManager.getReadPosition(mListChapter.get(position)).equals(""))
                holder.btnContinues.setVisibility(View.VISIBLE);

            holder.btnContinues.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != iOnItemClick) {
                        iOnItemClick.onClick(holder.getAdapterPosition(), CONTINUES);
                    }
                }
            });
        } else if (!DataStoreManager.getIndexChapter(activity, mListChapter.get(position)).equals("-1")) {
            holder.btnContinues.setVisibility(View.VISIBLE);
            holder.btnContinues.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != iOnItemClick) {
                        iOnItemClick.onClick(holder.getAdapterPosition(), CONTINUES);
                    }
                }
            });


        } else {
            holder.btnContinues.setVisibility(View.GONE);
        }
        holder.btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != iOnItemClick) {
                    iOnItemClick.onClick(holder.getAdapterPosition(), START);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return null == mListChapter ? 0 : mListChapter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imvPlay;
        protected TextView tvTitle;
        protected TextView tvDescription;
        protected TextView tvRead;
        protected ImageView imvAvatar;
        private TextViewBold btnRead;
        private TextViewBold btnContinues;
        private RelativeLayout layoutItem;

        public ViewHolder(View itemView) {
            super(itemView);
            imvAvatar = (ImageView) itemView.findViewById(R.id.imv_avatar);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            tvRead = (TextView) itemView.findViewById(R.id.tv_read);
            imvPlay = (ImageView) itemView.findViewById(R.id.imv_play);
            btnRead = (TextViewBold) itemView.findViewById(R.id.tv_read_now);
            btnContinues = (TextViewBold) itemView.findViewById(R.id.tv_read_continues);
            layoutItem = (RelativeLayout) itemView.findViewById(R.id.layoutItem);
        }
    }
}
