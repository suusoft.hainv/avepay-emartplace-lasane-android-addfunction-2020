package com.aveplus.avepay_cpocket.objects.reservationsold;

import com.google.gson.annotations.SerializedName;

public class Deal{

	@SerializedName("image")
	private String image;

	@SerializedName("pro_data")
	private Object proData;

	@SerializedName("is_active")
	private int isActive;

	@SerializedName("created_user")
	private Object createdUser;

	@SerializedName("seller_avatar")
	private Object sellerAvatar;

	@SerializedName("discount")
	private Object discount;

	@SerializedName("is_favourite")
	private int isFavourite;

	@SerializedName("seller_qb_id")
	private Object sellerQbId;

	@SerializedName("modified_date")
	private Object modifiedDate;

	@SerializedName("content")
	private String content;

	@SerializedName("modified_user")
	private Object modifiedUser;

	@SerializedName("category_id")
	private Object categoryId;

	@SerializedName("balance")
	private Object balance;

	@SerializedName("price")
	private String price;

	@SerializedName("id")
	private int id;

	@SerializedName("created_date")
	private String createdDate;

	@SerializedName("status")
	private Object status;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setProData(Object proData){
		this.proData = proData;
	}

	public Object getProData(){
		return proData;
	}

	public void setIsActive(int isActive){
		this.isActive = isActive;
	}

	public int getIsActive(){
		return isActive;
	}

	public void setCreatedUser(Object createdUser){
		this.createdUser = createdUser;
	}

	public Object getCreatedUser(){
		return createdUser;
	}

	public void setSellerAvatar(Object sellerAvatar){
		this.sellerAvatar = sellerAvatar;
	}

	public Object getSellerAvatar(){
		return sellerAvatar;
	}

	public void setDiscount(Object discount){
		this.discount = discount;
	}

	public Object getDiscount(){
		return discount;
	}

	public void setIsFavourite(int isFavourite){
		this.isFavourite = isFavourite;
	}

	public int getIsFavourite(){
		return isFavourite;
	}

	public void setSellerQbId(Object sellerQbId){
		this.sellerQbId = sellerQbId;
	}

	public Object getSellerQbId(){
		return sellerQbId;
	}

	public void setModifiedDate(Object modifiedDate){
		this.modifiedDate = modifiedDate;
	}

	public Object getModifiedDate(){
		return modifiedDate;
	}

	public void setContent(String content){
		this.content = content;
	}

	public String getContent(){
		return content;
	}

	public void setModifiedUser(Object modifiedUser){
		this.modifiedUser = modifiedUser;
	}

	public Object getModifiedUser(){
		return modifiedUser;
	}

	public void setCategoryId(Object categoryId){
		this.categoryId = categoryId;
	}

	public Object getCategoryId(){
		return categoryId;
	}

	public void setBalance(Object balance){
		this.balance = balance;
	}

	public Object getBalance(){
		return balance;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setStatus(Object status){
		this.status = status;
	}

	public Object getStatus(){
		return status;
	}
}