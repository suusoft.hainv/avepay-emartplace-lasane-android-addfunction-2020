package com.aveplus.avepay_cpocket.movie.movie.util;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aveplus.avepay_cpocket.R;
import com.squareup.picasso.Picasso;

/**
 * Created by SuuSoftv on 7/25/2017.
 */

public class BindingUtil {


    /**
     * Set image by Picasso
     *
     * @param imageView image destination
     * @param imageUrl  image path
     */
    public static void setImage(ImageView imageView, String imageUrl) {
        if (imageUrl != null && !imageUrl.equals("")) {
            Picasso.get()
                    .load(imageUrl)
                    .error(R.drawable.bt_products)
                    .into(imageView);

        } else {
            imageView.setImageResource(R.drawable.bt_products);
        }
    }


    /**
     * Set image by Picasso
     */
    public static void setImage(ImageView imageView, int res) {
        imageView.setImageResource(res);
    }
    /**
     * Set image by Picasso
     */
    /**
     * set height of view equal with a ratio width of view
     *
     * @param highPercent percent of height with width
     */
    public static void setLayoutHeight(View view, float highPercent) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = (int) (layoutParams.width * highPercent / 100);
        view.setLayoutParams(layoutParams);
    }

    public static void setLayoutHeight(View view, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }


}
