package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseActivity;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.Args;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.utils.StringUtil;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewBold;
import com.aveplus.avepay_cpocket.widgets.textview.TextViewRegular;


public class TripFinishingActivity1 extends BaseActivity {

    private TripObj mTripObj;
    private int mTransactionFee = 0; // Set 1 credit as default

    private TextViewBold mLblDollar, mLblPay;
    private TextViewRegular mLblCredits, mLblMonetary, mLblTotal;
    private EditText mTxtAmount;
    private RecyclerView mRcl;
    private LinearLayout mLlNote;

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_trip_finishing1;
    }

    @Override
    protected void getExtraData(Intent intent) {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Args.KEY_TRIP_OJBECT)) {
                mTripObj = bundle.getParcelable(Args.KEY_TRIP_OJBECT);

                try {
                    mTransactionFee = Integer.parseInt(DataStoreManager.getSettingUtility().getTrip_payment_fee());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    protected void initilize() {

    }

    @Override
    protected void initView() {

        mLblDollar = (TextViewBold) findViewById(R.id.lbl_dollar);
        mLblPay = (TextViewBold) findViewById(R.id.lbl_pay);
        mLblCredits = (TextViewRegular) findViewById(R.id.lbl_credits);
        mLblMonetary = (TextViewRegular) findViewById(R.id.lbl_monetary_unit);
        mLblTotal = (TextViewRegular) findViewById(R.id.lbl_total);
        mTxtAmount = (EditText) findViewById(R.id.txt_amount);
        mLlNote = (LinearLayout) findViewById(R.id.ll_note);

        mRcl = (RecyclerView) findViewById(R.id.rcl_payment_methods);
        mRcl.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));
        mRcl.setHasFixedSize(true);

        // Should call this methods at the end of declaring UI
        initData();
//        initPaymentMethod();

    }

    @Override
    protected void onViewCreated() {

    }


    private void initData() {
        if (mTripObj != null) {
            mLblDollar.setText(StringUtil.convertNumberToPrice(mTripObj.getEstimateFare()));

            mLblCredits.setText(StringUtil.convertNumberToPrice(mTripObj.getEstimateFare()));

            String amount = StringUtil.convertNumberToPrice(mTripObj.getEstimateFare());

            mTxtAmount.setText(amount);
            mLblTotal.setText(StringUtil.convertNumberToPrice((mTripObj.getEstimateFare())));

        }
    }

}
