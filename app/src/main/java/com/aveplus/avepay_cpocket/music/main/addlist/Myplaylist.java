package com.aveplus.avepay_cpocket.music.main.addlist;

import android.os.Parcel;
import android.os.Parcelable;


import com.aveplus.avepay_cpocket.music.base.model.BaseModel;

import java.util.ArrayList;

public class Myplaylist extends BaseModel implements Parcelable {
    private ArrayList<AddListObj> listObjs;

    public Myplaylist() {
    }

    public Myplaylist(ArrayList<AddListObj> listObjs) {
        this.listObjs = listObjs;
    }

    protected Myplaylist(Parcel in) {
        listObjs = in.createTypedArrayList(AddListObj.CREATOR);
    }

    public static final Creator<Myplaylist> CREATOR = new Creator<Myplaylist>() {
        @Override
        public Myplaylist createFromParcel(Parcel in) {
            return new Myplaylist(in);
        }

        @Override
        public Myplaylist[] newArray(int size) {
            return new Myplaylist[size];
        }
    };

    public ArrayList<AddListObj> getListObjs() {
        if (listObjs == null) {
            listObjs = new ArrayList<>();
        }
        return listObjs;
    }

    public void setListObjs(ArrayList<AddListObj> listObjs) {
        this.listObjs = listObjs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(listObjs);
    }
}
