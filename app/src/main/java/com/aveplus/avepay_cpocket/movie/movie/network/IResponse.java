package com.aveplus.avepay_cpocket.movie.movie.network;

/**
 * Copyright © 2019 SUUSOFT
 */
public interface IResponse {
    void onResponse(Object response);
}
