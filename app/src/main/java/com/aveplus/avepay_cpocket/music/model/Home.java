package com.aveplus.avepay_cpocket.music.model;

import java.util.ArrayList;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class Home {
    public List<Album> playlists_feature;
    public List<Album> playlists_top;
    public List<Album> playlists_new;
    public List<Album> playlists_hot;
    public List<Album> playlist_banner;
    public ArrayList<Song> songs;
    public ArrayList<Category> categories;
    public ArrayList<Artist> artists;
    public List<ListPlayList> playlist_title;
    public int total_list;

    public List<Album> getPlaylist_banner() {
        return playlist_banner;
    }

    public void setPlaylist_banner(List<Album> playlist_banner) {
        this.playlist_banner = playlist_banner;
    }

    public int getTotal_list() {
        return total_list;
    }

    public void setTotal_list(int total_list) {
        this.total_list = total_list;
    }

    public List<Album> getPlaylist_feature() {
        if (playlists_feature == null)
            playlists_feature = new ArrayList<>();
        return playlists_feature;
    }

    public void setPlaylist_feature(List<Album> playlist_feature) {
        this.playlists_feature = playlist_feature;
    }



    public ArrayList<Song> getSongs() {
        if (songs == null)
            songs = new ArrayList<>();
        return songs;
    }

    public List<Album> getPlaylists_new() {
        if (playlists_new == null)
            playlists_new = new ArrayList<>();
        return playlists_new;
    }

    public void setPlaylists_new(List<Album> playlists_new) {
        this.playlists_new = playlists_new;
    }

    public List<Album> getPlaylist_hot() {
        if (playlists_hot == null)
            playlists_hot = new ArrayList<>();
        return playlists_hot;
    }
    public List<ListPlayList> getPlaylist_title() {
        if (playlist_title == null)
            playlist_title = new ArrayList<>();
        return playlist_title;
    }
    public List<Album> getPlaylist_top() {
        if (playlists_top == null)
            playlists_top = new ArrayList<>();
        return playlists_top;
    }

    public void setPlaylist_hot(List<Album> playlist_hot) {
        this.playlists_hot = playlist_hot;
    }

    public void setSongs(ArrayList<Song> songs) {
        this.songs = songs;
    }

    public ArrayList<Category> getCategories() {
        if (categories == null)
            categories = new ArrayList<>();
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public ArrayList<Artist> getArtists() {
        if (artists == null)
            artists = new ArrayList<>();
        return artists;
    }

    public void setArtists(ArrayList<Artist> artists) {
        this.artists = artists;
    }
}
