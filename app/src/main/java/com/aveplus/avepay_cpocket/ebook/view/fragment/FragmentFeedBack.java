package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.ViewDataBinding;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.FragmentFeedbackBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.view.activity.MainActivity;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.FeedBackVM;


/**
 * Created by Suusoft on 9/8/2017.
 */
public class FragmentFeedBack extends BaseFragmentBinding {
    private FeedBackVM viewModel;
    private FragmentFeedbackBinding binding;

    public static FragmentFeedBack newInstance() {
        Bundle args = new Bundle();
        FragmentFeedBack fragment = new FragmentFeedBack();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_feedback;
    }

    @Override
    protected void initialize() {

    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new FeedBackVM(self);
        return viewModel;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        this.binding = (FragmentFeedbackBinding) binding;
        this.binding.setViewModel(viewModel);

        this.binding.txtContent.setHorizontallyScrolling(false);
        this.binding.txtContent.setMaxLines(4);
        this.binding.txtContent.setMinLines(4);
    }


    @Override
    protected void initView(View view) {
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) self).setToolbarTitle(R.string.feedback);
        ((MainActivity) self).showIconToolbar();
    }

}
