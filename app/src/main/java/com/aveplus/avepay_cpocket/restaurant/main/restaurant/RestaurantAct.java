package com.aveplus.avepay_cpocket.restaurant.main.restaurant;
/**
 * Created by suusoft.com on 11/12/19.
 */
import android.content.Intent;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;

import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.ID_RESTAURANT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.PARAM_SELLLER_ID;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LAT;
import static com.aveplus.avepay_cpocket.restaurant.retrofit.param.Param.USER_LONG;


public class RestaurantAct extends BaseActivity {
    private Double userlat, userlong;
    private int idrestaurant;
    private ViewPager mPager;
    private int id_seller;

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_retaurant;
    }

    @Override
    protected void initView() {
        setToolbarTitle(R.string.shop);
        Intent intent   = getIntent ();
        idrestaurant    = intent.getIntExtra (ID_RESTAURANT, 0);
        userlat         = intent.getDoubleExtra (USER_LAT, 0);
        userlong        = intent.getDoubleExtra (USER_LONG, 0);
        id_seller = intent.getIntExtra(PARAM_SELLLER_ID, 0);

        Bundle bundle = new Bundle ();
        bundle.putInt (ID_RESTAURANT, idrestaurant);
        bundle.putDouble (USER_LAT, userlat);
        bundle.putDouble (USER_LONG, userlong);
        bundle.putInt(PARAM_SELLLER_ID, id_seller);
        RestaurantFrag restaurantFrag = new RestaurantFrag ();
        restaurantFrag.setArguments (bundle);
        getSupportFragmentManager ().beginTransaction ().replace (R.id.frame_restaurant, restaurantFrag).commit ();

//        Intent intent1 = getIntent();
//        id_seller = intent1.getIntExtra(PARAM_SELLLER_ID, -1);
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NAVI;
    }


    @Override
    protected void onPrepareCreateView() {

    }
}
