package com.aveplus.avepay_cpocket.music.main.video2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.main.video2.vm.VideoCateVM;
import com.aveplus.avepay_cpocket.music.main.video2.vm.VideoVM;

import java.util.ArrayList;

import static com.aveplus.avepay_cpocket.music.main.video2.ListVideoAct.FULL_CATEGORY;
import static com.aveplus.avepay_cpocket.music.main.video2.ListVideoAct.ID_PLAYLIST;
import static com.aveplus.avepay_cpocket.music.main.video2.ListVideoAct.NAME_PLAYLIST;


public class AdapterVideo extends RecyclerView.Adapter<AdapterVideo.ViewHolder> {
    private ArrayList<Video> arrVideo;
    private Context context;
    private OnItemClick onItemClick;

    public interface OnItemClick {
        void listenerr(String name, String id);
    }

    public AdapterVideo(Context context, ArrayList<Video> arrVideo) {
        this.context = context;
        this.arrVideo = arrVideo;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Video item = arrVideo.get(position);
        if (item != null) {
            holder.tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ListVideoAct.class);
                    intent.putExtra(FULL_CATEGORY, "All Category");
                    context.startActivity(intent);
                }
            });
            if (item.getName().equals(DataVideo.CATEGORY_VIDEO)) {
                holder.tvName.setText(item.getName());
                holder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                holder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_category_video, item.getArrVideoCate(), VideoCateVM.class));
            } else if (item.getName().equals(DataVideo.VIDEO)) {
                holder.tvName.setText(item.getName());
                holder.tvMore.setVisibility(View.GONE);
                holder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_video_list, item.getArrVideo(), VideoVM.class));
                holder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            } else {
                holder.tvMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String id = item.getId_playlist() + "";
                        Intent intent = new Intent(context, ListVideoAct.class);
                        intent.putExtra(ID_PLAYLIST, id);
                        intent.putExtra(NAME_PLAYLIST, item.getName());
                        context.startActivity(intent);

                    }
                });
                holder.tvName.setText(item.getName());
                holder.rcvData.setAdapter(new SingleAdapter(context, R.layout.item_video_playlist, item.getArrVideo(), VideoVM.class));
                holder.rcvData.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            }

        }
    }


    @Override

    public int getItemCount() {
        return arrVideo.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvMore;
        RecyclerView rcvData;

        ViewHolder(View itemView) {
            super(itemView);
            tvMore = itemView.findViewById(R.id.tv_more);
            tvName = itemView.findViewById(R.id.tv_name);
            rcvData = itemView.findViewById(R.id.rcv_data);
            rcvData.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        }
    }
}
