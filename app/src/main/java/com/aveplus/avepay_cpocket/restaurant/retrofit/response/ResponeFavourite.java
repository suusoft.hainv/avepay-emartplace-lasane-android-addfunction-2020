package com.aveplus.avepay_cpocket.restaurant.retrofit.response;

import com.aveplus.avepay_cpocket.restaurant.main.save.savemodels.SaveMdoels;

/**
 * Created by suusoft.com on 11/12/19.
 */

public class ResponeFavourite extends BaseModel {
    public SaveMdoels data;

    public SaveMdoels getData() {
        return data;
    }

    public void setData(SaveMdoels data) {
        this.data = data;
    }
}
