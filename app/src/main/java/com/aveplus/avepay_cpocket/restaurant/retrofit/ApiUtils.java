package com.aveplus.avepay_cpocket.restaurant.retrofit;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;

public class ApiUtils {

    public static final String BASE_URL = AppController.getInstance().getString(R.string.URL_API_RESTAURANT_PROJECT);

    public static final String TAG = ApiUtils.class.getSimpleName ();

    public static APIService getAPIService() {
        //Log.e (TAG, "getAPIService: " + RetrofitClient.getClient (BASE_URL).toString () );
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);

    }

}
