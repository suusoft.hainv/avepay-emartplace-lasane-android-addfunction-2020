package com.aveplus.avepay_cpocket.taxi.view.instant;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.base.BaseFragment;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.utils.ImageUtil;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailTripFragment extends BaseFragment {


    private TripObj data;

    private TextView  tvAddressFrom, tvAddressDestination, tvNameDriver, tvNameCar, tvTypeCar, tvPhoneNumber;
    private CircleImageView imgAvatar;
    private ImageView btnBack;

    public static DetailTripFragment newInstance() {
        Bundle args = new Bundle();
        DetailTripFragment fragment = new DetailTripFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_detail_trip;
    }

    @Override
    protected void init() {
        data=((InstantTripTrackingActivity)self).tripObj;
    }

    @Override
    protected void initView(View view) {
        btnBack = view.findViewById(R.id.btn_back);
        imgAvatar = view.findViewById(R.id.img_avatar);
        tvAddressFrom = view.findViewById(R.id.tv_address_from);
        tvAddressDestination = view.findViewById(R.id.tv_address_destination);
        tvNameDriver = view.findViewById(R.id.tv_name_driver);
        tvNameCar = view.findViewById(R.id.tv_name_car);
        tvTypeCar = view.findViewById(R.id.tv_type_car);
        tvPhoneNumber = view.findViewById(R.id.tv_phone_number);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InstantTripTrackingActivity)self).hideDetailTrip();
            }
        });

        bindData();

    }

    private void bindData() {
        ImageUtil.setImage(self, imgAvatar, data.getDriverAvatar());
        tvAddressFrom.setText(MapsUtil.getAddressFromLatLng(self, data.getLatLngPickup()) );
        tvAddressDestination.setText(MapsUtil.getAddressFromLatLng(self, data.getLatLngDestination()) );
        tvNameDriver.setText(data.getDriverName());
        tvTypeCar.setText(data.getDriverType());
        tvPhoneNumber.setText(data.getDriverPhone());
    }

    @Override
    protected void getData() {

    }
}
