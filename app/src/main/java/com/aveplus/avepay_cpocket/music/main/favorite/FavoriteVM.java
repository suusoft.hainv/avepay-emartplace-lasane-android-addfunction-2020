package com.aveplus.avepay_cpocket.music.main.favorite;

import android.content.Context;
import android.os.AsyncTask;
import android.view.MenuItem;
import android.view.View;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class FavoriteVM extends BaseViewModelList implements ItemMenuActionSongListener {
    /**
     * Constructor with fragment
     *
     *
     * @param context context
     */

    ArrayList<Song> list;

    public FavoriteVM(Context context) {
        super(context);
        getData(1);
    }

    @Override
    public void getData(int page) {
        new AsyncTask<String, String, ArrayList<Song>>(){

            @Override
            protected ArrayList<Song> doInBackground(String... strings) {
                list = DataStoreManager.getFavourites();
                return list;
            }

            @Override
            protected void onPostExecute(ArrayList<Song> list) {
                super.onPostExecute(list);
                addListData(list);
                checkLoadingMoreComplete(1);
            }
        }.execute("");
    }

    @Override
    public void onItemClicked(View view, int position) {
        onClickWatch(position);
    }

    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().addNewQueue(list);
        QueueManager.getInstance().setPositionCurrentItem(position);
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {
        if (list != null) {
            QueueManager.getInstance().addToQueue(list.get(position));
            AppUtil.showToast(self, R.string.added_to_queue);
        }
    }


    @Override
    public void onClickDelete(int position) {
        DataStoreManager.unFavorite(((Song)mDatas.get(position)).id);
        mDatas.remove(position);
        list.remove(position);
        notifyDataChanged(false);
        AppUtil.showToast(self,R.string.deleted);
    }

    public void onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_clear_all :
                clearAll();
                break;


        }
    }

    private void playAll() {
        QueueManager.getInstance().clear();
        QueueManager.getInstance().addToQueue(list);
        EventBus.getDefault().post(new Global.HomeSongItemClicked());

    }

    private void clearAll() {
        if (list != null && list.size() > 0) {
            DialogUtil.showAlertDialog(self, R.string.confirm_delete, new DialogUtil.IDialogConfirm() {
                @Override
                public void onClickOk() {
                    DataStoreManager.clearFavorites();
                    mDatas.clear();
                    list.clear();
                    notifyDataChanged(false);
                    AppUtil.showToast(self, R.string.deleted);
                }

                @Override
                public void onClickCancel() {

                }
            });

        }
    }


}
