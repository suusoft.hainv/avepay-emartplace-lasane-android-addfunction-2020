package com.aveplus.avepay_cpocket.movie.movie.main.customwidget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ListAdapter;

/**
 * Copyright © 2019 SUUSOFT
 * */
public class CenterLockHorizontalScrollview extends HorizontalScrollView {
    private Context context;
    private int prevIndex = -1;

    // next page
    private int mAllItem;
    private final int itemPage = 5; // version 1.0
    private int itemPerPage; // version 1.1

    private boolean autoScale;

    public int getItemPerPage() {
        return itemPerPage;
    }

    public void setItemPerPage(int itemPerPage) {
        this.itemPerPage = itemPerPage;
    }


    public CenterLockHorizontalScrollview(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.setSmoothScrollingEnabled(true);
 
    }
 
    public void setAdapter(Context context, ListAdapter mAdapter) {
            fillViewWithAdapter(mAdapter);
    }
 
    private void fillViewWithAdapter(ListAdapter mAdapter){
        if (getChildCount() == 0 || mAdapter == null)
            return;
        mAllItem = mAdapter.getCount();
        ViewGroup parent = (ViewGroup) getChildAt(0);
 
        parent.removeAllViews();
        int count = mAdapter.getCount();
        for (int i = 0; i < count; i++) {
            parent.addView(mAdapter.getView(i, null, parent));
        }
    }
    public void AddViewWithAdapter(ListAdapter mAdapter){
        if (getChildCount() == 0 || mAdapter == null)
            return;
        ViewGroup parent = (ViewGroup) getChildAt(0);

        int count = mAdapter.getCount();
        int startPos = mAllItem +1;
        for (int i = startPos ; i < count; i++) {
            parent.addView(mAdapter.getView(i, null, parent));
        }
        mAllItem += mAdapter.getCount();
    }
    public void setCenter(int index) {
        ViewGroup parent = (ViewGroup) getChildAt(0);
        //set background for item selected: not background
        ViewGroup view = (ViewGroup) parent.getChildAt(index);
        // set postion center
        int screenWidth = ((Activity) context).getWindowManager()
                .getDefaultDisplay().getWidth();
 
        int scrollX = (view.getLeft() - (screenWidth / 2))
                + (view.getWidth() / 2);
        this.smoothScrollTo(scrollX, 0);
        prevIndex = index;
    }
    private int getPage(){
       int page =  mAllItem % itemPage == 0 ? mAllItem/itemPage :  mAllItem/itemPage+1;
        return page;
    }
    public void nextPage(int index) {
        if(index != mAllItem-1) {
            ViewGroup parent = (ViewGroup) getChildAt(0);
            ViewGroup view = (ViewGroup) parent.getChildAt(index + 1);
            this.smoothScrollTo(view.getLeft(), 0);
            parent.getChildAt(index + 1).requestFocus();
        }

    }
    public void prevPage(int index) {
        ViewGroup parent = parent = (ViewGroup) getChildAt(0);
        if(index != mAllItem-1) {
            ViewGroup view;
            if(index - itemPage >0)
                 view = (ViewGroup) parent.getChildAt(index - 5);
            else
                 view = (ViewGroup) parent.getChildAt(1);
            this.smoothScrollTo(view.getLeft(), 0);
            parent.getChildAt(index - 1).requestFocus();
        }
    }
    public void prevPageV1(int index) {
        if(index != mAllItem-1) {
            ViewGroup parent = (ViewGroup) getChildAt(0);
            ViewGroup view;
            if(index - itemPerPage >0)
                view = (ViewGroup) parent.getChildAt(index - itemPerPage);
            else
                view = (ViewGroup) parent.getChildAt(1);
            this.smoothScrollTo(view.getLeft(), 0);
            parent.getChildAt(index - 1).requestFocus();
        }

    }

}
