package com.aveplus.avepay_cpocket.taxi.view.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseTripObj;
import com.aveplus.avepay_cpocket.taxi.trip.TripManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DirectionActivity extends BaseActivity {

    private static final String TAG = DirectionActivity.class.getSimpleName();
    private Bundle mBundle;

    private TextView tvTitle;

    @Override
    protected void getExtraValues() {
        mBundle = getIntent().getExtras();
    }

    @Override
    void inflateLayout() {
        setContentView(R.layout.activity_direction);
    }

    @Override
    void initUI() {

    }

    @Override
    void initControl() {
        tvTitle = findViewById(R.id.tv_title);

        tvTitle.setText(String.format(getString(R.string.welcome_bind_name_how_can_i_help_you), DataStoreManager.getUser().getName()));

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkTripStatus();
    }

    public void onClickInstantBooking(View view) {

        AppController.getInstance().isInstantBooking = true;
        if (mBundle == null) {
            mBundle = new Bundle();
        }

        GlobalFunctions.startActivityWithoutAnimation(self, MainActivity.class, mBundle);
        Log.e("Splash", "gotoHomePage");
        finish();

    }

    public void onClickBookingNormal(View view) {

        AppController.getInstance().isInstantBooking = false;


        if (mBundle == null) {
            mBundle = new Bundle();
        }

        GlobalFunctions.startActivityWithoutAnimation(self, MainActivity.class, mBundle);
        Log.e("Splash", "gotoHomePage");
        finish();
    }


    private void checkTripStatus() {

        showProgress(true);
        mAPIService.getTripStatus(DataStoreManager.getUser().getId(), "0", DataStoreManager.getTokenUser())
                .enqueue(new Callback<ResponseTripObj>() {
                    @Override
                    public void onResponse(Call<ResponseTripObj> call, Response<ResponseTripObj> response) {
                        showProgress(false);

                        if (response.isSuccessful()) {
                            if (response.body().isSuccess(self)) {
                                Log.e(TAG, "respone: " + response.body().toJson());

                                TripObj tripObj = response.body().getData();

                                TripManager.directionTrip(self, tripObj, TAG);
                            }
                        }


                    }

                    @Override
                    public void onFailure(Call<ResponseTripObj> call, Throwable t) {
                        showProgress(false);
                        Log.e(TAG, "onFailure: " + t.getMessage());

                    }
                });
    }

}
