package com.aveplus.avepay_cpocket.music.main.search;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;


import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.IOnItemClickedListener;
import com.aveplus.avepay_cpocket.music.listener.ItemMenuActionSongListener;
import com.aveplus.avepay_cpocket.music.model.Search;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.music.network.ApiResponse;
import com.aveplus.avepay_cpocket.music.network.BaseRequest;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class SearchVM extends BaseViewModelList implements IOnItemClickedListener, ItemMenuActionSongListener {

    private String keyword = "";

    private List<SearchCategory> listData;
    private ListenerSearch listenerSearch;

    public SearchVM(Context context, Bundle bundle, ListenerSearch listenerSearh) {
        super(context, bundle);
        keyword = bundle.getString(SearchFragment.KEY_SEARCH);
        listenerSearch = listenerSearh;
        getData(1);
    }

    public interface ListenerSearch {
        void search(List<SearchCategory> list);
    }

    @Override
    public void getData(int page) {

        RequestManager.searchSongs(page, keyword, new BaseRequest.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                listData = new ArrayList<>();
                if (!listData.isEmpty()) {
                    listData.clear();
                }
                if (!response.isError()) {
                    SearchCategory searchCategory;
                    Search search = response.getDataObject(Search.class);
                    for (int i = 0; i < 3; i++) {
                        searchCategory = new SearchCategory();
                        if (i == 0 && search.getArtists().size() > 0) {
                            searchCategory.setName(SearchCategory.ARTIST);
                            searchCategory.getListArtist().addAll(search.getArtists());
                        } else if (i == 2 && search.getPlaylists().size() > 0) {
                            searchCategory.getListAlbum().addAll(search.getPlaylists());
                        } else if (search.getSongs().size() > 0) {
                            searchCategory.setName(SearchCategory.PLAYLIST);
                            searchCategory.setName(SearchCategory.SONGS);
                            searchCategory.getListSongs().addAll(search.getSongs());
                        }
                        listData.add(searchCategory);
                    }
                    Log.e("SearchVM", "onSuccess: " + listData.size());
                    addListData(listData);
                    if (listData != null) {
                        listenerSearch.search(listData);
                    }
                    checkLoadingMoreComplete(1);

                }
            }

            @Override
            public void onError(String message) {
                AppUtil.showToast(self, message);
            }
        });
    }


    @Override
    public void onClickWatch(int position) {
        QueueManager.getInstance().addNewQueue((Song) getListData().get(position));
        EventBus.getDefault().post(new Global.HomeSongItemClicked());
    }

    @Override
    public void onClickAddToQueue(int position) {

    }

    @Override
    public void onClickDelete(int position) {

    }
}
