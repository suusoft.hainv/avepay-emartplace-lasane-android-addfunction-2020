package com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rate {

    @SerializedName("1")
    @Expose
    private Integer one;
    @SerializedName("2")
    @Expose
    private Integer two;
    @SerializedName("3")
    @Expose
    private Integer three;
    @SerializedName("4")
    @Expose
    private Integer four;
    @SerializedName("5")
    @Expose
    private Integer five;
    @SerializedName("max")
    @Expose
    private Integer max;

    public Integer getOne() {
        return one;
    }

    public Integer getTwo() {
        return two;
    }

    public Integer getThree() {
        return three;
    }

    public Integer getFour() {
        return four;
    }

    public Integer getFive() {
        return five;
    }

    public Integer getMax() {
        return max;
    }
}
