package com.aveplus.avepay_cpocket.taxi.modelmanager;

import android.content.Context;
import android.util.Log;

import com.aveplus.avepay_cpocket.AppController;
import com.aveplus.avepay_cpocket.taxi.configs.Action;
import com.aveplus.avepay_cpocket.taxi.configs.Apis;
import com.aveplus.avepay_cpocket.taxi.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.taxi.globals.GlobalFunctions;
import com.aveplus.avepay_cpocket.taxi.network.BaseRequest;
import com.aveplus.avepay_cpocket.taxi.objects.TripObj;
import com.aveplus.avepay_cpocket.taxi.utils.map.MapsUtil;
import com.google.android.gms.maps.model.LatLng;


import java.util.HashMap;

/**
 *
 */
public class RequestManager extends BaseRequest {

    private static final String TAG = RequestManager.class.getSimpleName();


    private static final String PARAM_IMEI = "ime";

    // Params
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_IS_DRIVER = "is_driver";
    private static final String PARAM_IME = "ime";
    private static final String PARAM_GCM_ID = "gcm_id";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_TYPE_TRIP = "type_trip";
    private static final String PARAM_STATUS = "status";
    private static final String PARAM_CODE = "code";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_ADDRESS = "address";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_IMAGE = "image";
    private static final String PARAM_PHONE = "phone";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_FIELDS = "fields";
    private static final String PARAM_ACCESS_TOKEN = "access_token";
    private static final String PARAM_LOGIN_METHOD = "login_type";
    // params update profile pro
    private static final String PARAM_AVATAR = "avatar";
    private static final String PARAM_USER_ID = "user_id";
    private static final String PARAM_QB_USER_ID = "qb_id";

    private static final String PARAM_BUSINESS_NAME = "business_name";
    private static final String PARAM_BUSINESS_EMAIL = "business_email";
    private static final String PARAM_BUSINESS_PHONE = "business_phone";
    private static final String PARAM_BUSINESS_ADDRESS = "business_address";
    private static final String PARAM_BRAND = "brand";
    private static final String PARAM_MODEL = "model";
    private static final String PARAM_YEAR = "year";
    private static final String PARAM_COST_YOU_CHARGE_PER_KM = "fare";
    private static final String PARAM_CAR_COLOR = "color";
    private static final String PARAM_PLATE = "plate";
    private static final String PARAM_FARE_TYPE = "fare_type";

    private static final String PARAM_TYPE_OF_TRANSPORT = "type";
    private static final String PARAM_FUEL = "fuel_type";
    private static final String PARAM_YEAR_KM = "yearly_km";
    private static final String PARAM_YEAR_INTEND = "year_intend";
    private static final String PARAM_YEAR_TAX = "yearly_tax";
    private static final String PARAM_YEAR_GARA = "yearly_gara";
    private static final String PARAM_AVERAGE_CONSUMPTION = "average_consumption";
    private static final String PARAM_FUEL_UNIT_PRICE = "fuel_unit_price";
    private static final String PARAM_YEAR_INSURANCE = "yearly_insurance";
    private static final String PARAM_YEAR_MAINTENANCE = "yearly_maintenance";
    private static final String PARAM_PRICE_4_NEW_TYRES = "price_4_new_tyres";
    private static final String PARAM_YEAR_UNEXPECTED = "yearly_unexpected";
    private static final String PARAM_SOLD_VALUE = "sold_value";
    private static final String PARAM_BOUGHT_VALUE = "bought_value";
    private static final String PARAM_CERTIFICATION = "driver_license";
    private static final String PARAM_CAR = "image";
    private static final String PARAM_DELIVERY = "is_delivery";

    private static final String PARAM_TOKEN = "token";
    private static final String PARAM_SEARCH_TYPE = "search_type";
    private static final String PARAM_IS_ONLINE = "is_online";
    private static final String PARAM_CATEGORY_ID = "category_id";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_LAT = "lat";
    private static final String PARAM_LONG = "long";
    private static final String PARAM_IS_PREMIUM = "is_premium";
    private static final String PARAM_ACTION = "action";
    private static final String PARAM_TIME_TO_PASSENGER = "time_to_passenger";
    private static final String PARAM_ACTION_CREATE = "create";
    private static final String PARAM_ACTION_UPDATE = "update";
    private static final String PARAM_ACTION_ONLINE = "online";
    private static final String PARAM_DURATION = "duration";
    private static final String PARAM_ESTIMATE_DURATION = "estimate_duration";
    private static final String PARAM_PRICE = "price";
    public static final String PARAM_DISCOUNT_TYPE = "discount_type";
    private static final String PARAM_DISCOUNT_PRICE = "discount";
    public static final String PARAM_DISCOUNT_PERCENT = "discount_rate";
    private static final String PARAM_AUTO_RENEW = "is_renew";
    private static final String PARAM_KEY_WORD = "keyword";
    private static final String PARAM_DISTANCE = "distance";
    private static final String PARAM_ESTIMATE_DISTANCE = "estimate_distance";
    private static final String PARAM_NUM_PER_PAGE = "number_per_page";
    private static final String PARAM_OBJECT_ID = "object_id";
    private static final String PARAM_OBJECT_TYPE = "object_type";
    public static final String FAVORITE_TYPE_DEAL = "deal";
    public static final String PARAM_SORT_BY = "sort_by";
    public static final String PARAM_SORT_TYPE = "sort_type";

    private static final String PARAM_DESTINATION_ID = "destination_id";
    private static final String PARAM_AUTHOR_ROLE = "author_role";
    private static final String PARAM_DESTINATION_ROLE = "destination_role";
    private static final String PARAM_CONTENT = "content";
    private static final String PARAM_RATE = "rate";
    private static final String PARAM_ATTACHED = "attachment";


    private static final String PARAM_NEW_PASS = "new_password";
    private static final String PARAM_CURRENT_PASS = "current_password";
    private static final String PARAM_START_LATITUDE = "start_lat";
    private static final String PARAM_END_LATITUDE = "end_lat";
    private static final String PARAM_START_LONGITUDE = "start_long";
    private static final String PARAM_END_LONGITUDE = "end_long";
    private static final String PARAM_DEAL_ID = "deal_id";
    private static final String PARAM_DEAL_NAME = "deal_name";
    private static final String PARAM_BUYER_ID = "buyer_id";
    private static final String PARAM_BUYER_NAME = "buyer_name";
    private static final String PARAM_RESERVATION_ID = "reservation_id";
    //    settings
    private static final String PARAM_NOTIFI = "notify";
    private static final String PARAM_NOTIFI_FAVOURITE = "notify_favourite";
    private static final String PARAM_NOTIFI_TRANSPORT = "notify_transport";
    private static final String PARAM_NOTIFI_FOOD = "notify_food";
    private static final String PARAM_NOTIFI_LABOR = "notify_labor";
    private static final String PARAM_NOTIFI_TRAVEL = "notify_travel";
    private static final String PARAM_NOTIFI_SHOPPING = "notify_shopping";
    private static final String PARAM_NOTIFI_NEW_AND_EVENT = "notify_news";
    private static final String PARAM_NOTIFI_NEARBY = "notify_nearby";
    //    Transaction
    private static final String PARAM_AMOUNT = "amount";
    private static final String PARAM_NOTE = "note";
    private static final String PARAM_METHOD_PAYMENT = "payment_method";
    private static final String PARAM_DESTINATION_EMAIL = "destination_email";
    private static final String PARAM_ID_TRANSACTION = "transaction_id";

    private static final String PARAM_TRIP_ID = "trip_id";
    private static final String PARAM_PASSENGER_ID = "passenger_id";
    private static final String PARAM_DRIVER_ID = "driver_id";
    private static final String PARAM_SEAT_COUNT = "seat_count";
    private static final String PARAM_START_LOCATION = "start_location";
    private static final String PARAM_END_LOCATION = "end_location";
    private static final String PARAM_TRANSACTION_ID = "transaction_id";
    private static final String PARAM_ACTUAL_FARE = "actual_fare";
    private static final String PARAM_ESTIMATE_FARE = "estimate_fare";
    private static final String PARAM_MODE = "mode";
    private static final String PARAM_FRIEND_ID = "friend_id";
    private static final String PARAM_LATLNG = "latlng";
    private static final String PARAM_SENSOR = "sensor";
    private static final String PARAM_RESTAURANT_ID = "restaurant_id";
    private static final String PARAM_ITEMS = "items";
    private static final String PARAM_ORDER_ID = "order_id";
    private static final String PARAM_BILLING_NAME = "billingName";
    private static final String PARAM_BILLING_ADDRESS = "billingAddress";
    private static final String PARAM_BILLING_PHONE = "billingPhone";
    private static final String PARAM_BILLING_EMAIL = "billingEmail";
    private static final String PARAM_PAYMENT_METHOD = "paymentMethod";
    private static final String PARAM_VAT = "vat";
    private static final String PARAM_TOTAL = "total";
    private static final String PARAM_ID = "id";


    ///////////////////////////////////////////////////////////////////////////
    // Add functions connecting to server
    ///////////////////////////////////////////////////////////////////////////

    public static void registerDevice(String gcm, String ime, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_IMEI, ime);
        params.put(PARAM_GCM_ID, gcm);
        params.put(PARAM_TYPE, "1");// 1 is android
        params.put(PARAM_STATUS, "1");

        get(Apis.URL_REGISTER_DEVICE, params, false, completeListener);
    }

    public static void login(Context context, String email, String phone, String type, final CompleteListener completeListener) {
        String gcm_id = GlobalFunctions.getFCMToken(context);
        Log.e(TAG, "gcm_id : login " + gcm_id);
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USERNAME, email);
        params.put(PARAM_PHONE, phone);
        params.put(PARAM_GCM_ID, gcm_id);
        params.put(PARAM_TYPE, type); //Type = 0 login by email   || Type = 1 login by phone.

        get(Apis.URL_LOGIN, params, completeListener);

    }

    public static void logout(Context self, final CompleteListener completeListener) {
        context = self;
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_TOKEN, DataStoreManager.getTokenUser());

        get(Apis.URL_LOGOUT, params, completeListener);

    }


    public static void updateProfile(String name, String address, String email, String filePath, CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_NAME, name);
        params.put(PARAM_ADDRESS, address);
        params.put(PARAM_EMAIL, email);
        HashMap<String, String> files = new HashMap<>();
        if (filePath != null) {
            files.put(PARAM_AVATAR, filePath);
            multipart(Apis.URL_UPDATE_PROFILE_NORMAL, params, files, true, completeListener);
        } else {
            get(Apis.URL_UPDATE_PROFILE_NORMAL, params, completeListener);
        }
    }


    public static void getNearbyDriversAndRequest(String typeDriver, LatLng start,
                                                  LatLng destination, String note, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_TYPE, typeDriver);
        params.put(PARAM_NOTE, note);
        params.put(PARAM_START_LATITUDE, String.valueOf(start.latitude));
        params.put(PARAM_START_LONGITUDE, String.valueOf(start.longitude));

        if (destination != null) {
            params.put(PARAM_END_LATITUDE, String.valueOf(destination.latitude));
            params.put(PARAM_END_LONGITUDE, String.valueOf(destination.longitude));
        }

        get(Apis.URL_GET_NEARBY_DRIVERS, params, completeListener);
    }

    public static void findDriver(String typeDriver, LatLng start,
                                  LatLng destination, String note, String type, final CompleteListener completeListener) {
        Log.d(TAG, "findDriver: ");
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_TYPE, typeDriver);
        params.put(PARAM_NOTE, note);
        params.put(PARAM_TYPE_TRIP, type);
        params.put(PARAM_START_LATITUDE, String.valueOf(start.latitude));
        params.put(PARAM_START_LONGITUDE, String.valueOf(start.longitude));

        params.put(PARAM_START_LOCATION, MapsUtil.getAddressFromLatLng(AppController.getInstance(), start));
        params.put(PARAM_END_LOCATION, MapsUtil.getAddressFromLatLng(AppController.getInstance(), destination));

        if (destination != null) {
            params.put(PARAM_END_LATITUDE, String.valueOf(destination.latitude));
            params.put(PARAM_END_LONGITUDE, String.valueOf(destination.longitude));
        }

        get(Apis.URL_FIND_DRIVER, params, false, completeListener);
    }

    public static void tripPrice(String typeDriver, LatLng start,
                                 LatLng destination, String note, final CompleteListener completeListener) {
        
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_TYPE, typeDriver);
        params.put(PARAM_NOTE, note);
        params.put(PARAM_START_LATITUDE, String.valueOf(start.latitude));
        params.put(PARAM_START_LONGITUDE, String.valueOf(start.longitude));

        if (destination != null) {
            params.put(PARAM_END_LATITUDE, String.valueOf(destination.latitude));
            params.put(PARAM_END_LONGITUDE, String.valueOf(destination.longitude));
        }
        Log.d(TAG, "tripPrice: ");
        get(Apis.URL_TRIP_PRICE, params, true, completeListener);
    }

    public static void instantTripAction(String action, String tripId, String timeEstimate,
                                         final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_PASSENGER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_DRIVER_ID, ""); // if user is driver
        params.put(PARAM_TRIP_ID, tripId);
        params.put(PARAM_ACTION, action);
        params.put(PARAM_TIME_TO_PASSENGER, timeEstimate);
        get(Apis.URL_TRIP_INSTANT_ACTION, params, true, false, completeListener);
    }

    public static void payTrip(Context self, TripObj obj, String paymentMethod,
                               String price, CompleteListener completeListener) {
        String api = obj.isInstantBooking() ? Apis.URL_TRIP_INSTANT_ACTION : Apis.URL_TRIP_DEAL;

        context = self;
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_TRIP_ID, obj.getId());
        params.put(PARAM_ACTION, Action.ACTION_PAYMENT);
        params.put(PARAM_METHOD_PAYMENT, paymentMethod);
        params.put(PARAM_ACTUAL_FARE, price);

        get(api, params, completeListener);
    }


    public static void finishTripInstantAndDeal(Context self, TripObj obj, String paymentMethod,
                                                String price, float rate, String comment, CompleteListener completeListener) {
        String api = obj.isInstantBooking() ? Apis.URL_TRIP_INSTANT_ACTION : Apis.URL_TRIP_DEAL;

        context = self;
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_TRIP_ID, obj.getId());
        params.put(PARAM_ACTION, Action.ACTION_FINISH);
        //params.put(PARAM_METHOD_PAYMENT, paymentMethod);
        // params.put(PARAM_ACTUAL_FARE, price);
        params.put(PARAM_RATE, String.valueOf(rate));
        params.put(PARAM_CONTENT, comment);

        get(api, params, completeListener);
    }


    public static void actionTrip(Context self, TripObj obj, String action, String paymentMethod,
                                  String price, String rate, String comment, CompleteListener completeListener) {
        String api = obj.isInstantBooking() ? Apis.URL_TRIP_INSTANT_ACTION : Apis.URL_TRIP_DEAL;

        context = self;
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_TRIP_ID, obj.getId());
        params.put(PARAM_ACTION, action);


        get(api, params, completeListener);
    }


    // hàm này sử dụng khi driver accept trip deal trong màn chat
    public static void tripDeal(String action, String tripId, String estimateFare,
                                final CompleteListener completeListener) {

        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_TRIP_ID, tripId);
        params.put(PARAM_ACTION, action);

//        if (timeEstimate!=null)
//            if (!timeEstimate.equals(""))
//            params.put(PARAM_TIME_TO_PASSENGER , timeEstimate);

        if (action.equals(Action.ACTION_PASSENGER_DEAL))
            if (estimateFare != null)
                if (!estimateFare.equals(""))
                    params.put(PARAM_ESTIMATE_FARE, estimateFare);

        get(Apis.URL_TRIP_DEAL, params, completeListener);
    }


    public static void tripStatus(final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_IS_DRIVER, "0");  //1 = Driver, 0 = Passenger
        get(Apis.URL_TRIP_STATUS, params, true, completeListener);
    }

    public static void tripStatus(boolean isProgress, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_IS_DRIVER, "0");  //1 = Driver, 0 = Passenger
        get(Apis.URL_TRIP_STATUS, params, isProgress, completeListener);
    }

    public static void tripHistory(final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_IS_DRIVER, "0");  //1 = Driver, 0 = Passenger
        get(Apis.URL_TRIP_HISTORY, params, true, completeListener);
    }


    // lấy danh sách nhà hàng gần điểm đến
    public static void findRestaurantNearDestiation(Context self, LatLng latLng, final CompleteListener completeListener) {
        context = self;
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());

        if (latLng != null) {
            params.put(PARAM_START_LATITUDE, latLng.latitude + "");
            params.put(PARAM_START_LONGITUDE, latLng.longitude + "");
        }
        get(Apis.URL_FIND_RESTAURANT, params, true, completeListener);
    }


    // lấy danh sách food trong nhà hàng
    public static void getListFoodOnRestaurant(String id, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_RESTAURANT_ID, id);
        get(Apis.URL_RESTAURANT_PRODUCT, params, true, completeListener);
    }


    // lấy list order theo trip
    public static void getOrderByTrip(String tripId, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_TRIP_ID, tripId);
        get(Apis.URL_ORDER_BY_TRIP, params, true, completeListener);
    }

    // lấy list order đã đặt
    public static void getOderHistory(final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        get(Apis.URL_ORDER_HISTORY, params, true, completeListener);
    }

    // lấy thông tin order
    public static void getOrderDetail(String orderId, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        params.put(PARAM_ORDER_ID, orderId);
        get(Apis.URL_ORDER_HISTORY_DETAIL, params, true, completeListener);
    }

    public static void tripDetail(String tripId, final CompleteListener completeListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_ID, tripId);
        get(Apis.URL_TRIP_DETAIL, params, true, completeListener);
    }


    public static void getProfile(Context self, final CompleteListener completeListener) {
        context = self;
        HashMap<String, String> params = new HashMap<>();
        params.put(PARAM_TOKEN, DataStoreManager.getTokenUser());
        params.put(PARAM_USER_ID, DataStoreManager.getUser().getId());
        get(Apis.URL_PROFILE, params, true, completeListener);
    }
}
