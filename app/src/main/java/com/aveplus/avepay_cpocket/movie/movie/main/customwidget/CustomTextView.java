package com.aveplus.avepay_cpocket.movie.movie.main.customwidget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;


public class CustomTextView extends TextView {

	public CustomTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomeTextView, 0, 0);
		try {
			String nameTypeFace = typedArray.getString(R.styleable.CustomeTextView_customTypeFace);
			if (!isInEditMode() &&!TextUtils.isEmpty(nameTypeFace)) {
				Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + nameTypeFace);
				if (typeface != null) {
					setTypeface(typeface);
					setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		} finally {
			typedArray.recycle();
		}
	}

}
