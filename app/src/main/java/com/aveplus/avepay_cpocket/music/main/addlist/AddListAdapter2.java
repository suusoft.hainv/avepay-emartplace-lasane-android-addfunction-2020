package com.aveplus.avepay_cpocket.music.main.addlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.configs.Global;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class AddListAdapter2 extends RecyclerView.Adapter<AddListAdapter2.AddListVH> {

    private static final String TAG =AddListAdapter2.class.getSimpleName() ;
    private List<AddListObj> listObjs;

    public AddListAdapter2(List<AddListObj> listObjs) {
        this.listObjs = listObjs;
    }
    @NonNull
    @Override
    public AddListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_addlist, parent, false);
        return new AddListAdapter2.AddListVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddListVH holder, int position) {
        AddListObj addListObj = listObjs.get(position);
        holder.tvName.setText(addListObj.getAddListName());
        holder.tvNumber.setText(addListObj.getListVideos().size()+" songs");
        holder.llMyPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e(TAG, "onClick: "+addListObj.getAddListName() );
                EventBus.getDefault().post(new Global.ClickFavoriteAddList(addListObj));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listObjs != null ? listObjs.size() : 0;
    }

    public class AddListVH extends RecyclerView.ViewHolder {
        TextView tvName,tvNumber;
        LinearLayout llMyPlaylist;
        public AddListVH(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvNumber = itemView.findViewById(R.id.tv_number);
            llMyPlaylist = itemView.findViewById(R.id.ll_myplaylist);
        }
    }
}
