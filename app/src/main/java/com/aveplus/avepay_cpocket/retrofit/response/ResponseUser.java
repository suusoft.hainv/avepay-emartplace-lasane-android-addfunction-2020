package com.aveplus.avepay_cpocket.retrofit.response;


import com.aveplus.avepay_cpocket.objects.UserObj;

public class ResponseUser extends BaseReponse {

    public UserObj data ;

    public UserObj getData() {
        return data;
    }

    public void setData(UserObj data) {
        this.data = data;
    }
}
