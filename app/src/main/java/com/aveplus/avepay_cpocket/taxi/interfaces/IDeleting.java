package com.aveplus.avepay_cpocket.taxi.interfaces;

/**
 * Created by SuuSoft.com on 12/08/2016.
 */

public interface IDeleting {

    void onDeleted(Object obj);

    void onCancel();
}
