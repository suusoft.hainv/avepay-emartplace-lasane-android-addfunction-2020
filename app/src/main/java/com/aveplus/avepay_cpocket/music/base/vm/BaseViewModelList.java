package com.aveplus.avepay_cpocket.music.base.vm;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.ObservableInt;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.music.listener.IBaseListener;
import com.aveplus.avepay_cpocket.music.listener.IDataChangedListener;
import com.aveplus.avepay_cpocket.music.listener.IOnItemClickedListener;
import com.aveplus.avepay_cpocket.music.widgets.recyclerview.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public abstract class BaseViewModelList extends BaseViewModel implements
        EndlessRecyclerOnScrollListener.OnLoadMoreListener, IOnItemClickedListener {

    /**
     * show hide textview  No Data
     */
    public ObservableInt isDataAvailable;

    public int isVisibility(){
        if(getListData().size()>0){
            return View.GONE;
        }
        return View.VISIBLE;
    }


    /**
     * list data
     */
    protected ArrayList<?> mDatas;

    /**
     * listener when data is changed
     */
    private IDataChangedListener dataListener;

    protected IBaseListener iBaseListener;

    protected EndlessRecyclerOnScrollListener mOnScrollListener;

    public abstract void getData(int page);

    /**
     * Constructor with fragment
     *
     * @param context context
     */
    public BaseViewModelList(Context context) {
        this(context, null);
    }

    public BaseViewModelList(Context context, Bundle bundle) {
        super(context, bundle);
        initialize();
    }

    /**
     * initialize all component
     */
    private void initialize() {
        mOnScrollListener = new EndlessRecyclerOnScrollListener(this);
        mDatas = new ArrayList<>();
        isDataAvailable = new ObservableInt();
        isDataAvailable.set(View.GONE);
    }

    public EndlessRecyclerOnScrollListener getOnScrollListener() {
        return mOnScrollListener;
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(self);
    }

    public IBaseListener getActionListener() {
        return this;
    }

    @Override
    public void onLoadMore(int page) {
        getData(page);
    }

    @Override
    public void onDestroyView() {
        dataListener = null;
        self = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDatas = null;
    }

    /**
     * Notify that the view is needed to update
     *
     * @param newData new list is added. using for add a new list to listData default
     */
    protected void notifyDataChanged(List newData, boolean isAppend) {

        if (dataListener != null) {
            dataListener.onListDataChanged(newData, isAppend);
        }
    }

    /**
     * Notify that the view is needed to update
     * this function use the listData default
     */
    protected void notifyDataChanged(boolean isAppend) {
        if (dataListener != null) {
            dataListener.onListDataChanged(mDatas, isAppend);
        }
    }


    public ArrayList<?> getListData() {
        if (mDatas == null) {
            mDatas = new ArrayList<>();
        }
        return mDatas;
    }

    /**
     * add to list data and notify that need update
     *
     * @param data list data
     */

    public void addListData(List data) {
        mDatas.addAll(data);
        notifyDataChanged(data, true);
        if (data.size() > 0) {
            isDataAvailable.set(View.GONE);
        } else{
            isDataAvailable.set(View.VISIBLE);
        }

        Log.e("BaseViewModelList", "addListData: " + isDataAvailable);
        notifyChange();
    }


    /**
     * add to list data and notify that need update
     *
     * @param data list data
     */

    public void setListData(List data) {
        getListData().clear();
        getListData().addAll(data);
        notifyDataChanged(data, false);
        if (data.size() > 0) {

            Log.e("OK", "setListData: ");
            isDataAvailable.set(View.GONE);
        } else {isDataAvailable.set(View.VISIBLE);}
        Log.e("BaseViewModelList", "setListData: " + isDataAvailable.get());
        notifyChange();
    }

    /**
     * Check loading more. Using this when call recyclerView.addOnScrollListener
     *
     * @param totalPage get in json
     */
    protected void checkLoadingMoreComplete(int totalPage) {
        // set status loadmore
        mOnScrollListener.onLoadMoreComplete();
        mOnScrollListener.setEnded(mOnScrollListener.getCurrentPage() >= totalPage);
    }

    /**
     * set is ended loading more
     */
    protected void setIsEndedLoadMore() {
        mOnScrollListener.onLoadMoreComplete();
        mOnScrollListener.setEnded(true);
    }

    public IDataChangedListener getDataListener() {
        return dataListener;
    }

    public void setDataListener(IDataChangedListener listener) {
        this.dataListener = listener;
    }

    @Override
    public void onItemClicked(View view, int position) {

    }
}
