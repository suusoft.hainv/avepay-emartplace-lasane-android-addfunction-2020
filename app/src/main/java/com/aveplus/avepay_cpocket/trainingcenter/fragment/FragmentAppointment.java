package com.aveplus.avepay_cpocket.trainingcenter.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.BaseFragment;
import com.aveplus.avepay_cpocket.trainingcenter.modelmanager.ModelManager;
import com.aveplus.avepay_cpocket.trainingcenter.network.ApiResponse;
import com.aveplus.avepay_cpocket.trainingcenter.network.BaseRequest;
import com.aveplus.avepay_cpocket.trainingcenter.until.StringUtility;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class FragmentAppointment extends BaseFragment implements OnClickListener {

    public static final String MESSAGE_SUCCESS = "success";
    private Button btnSub;
    private EditText edtName, edtPhoneNumber, edtAddress, edtEmail, etSubjects, etContent;
    private String name, email, phone, address, subjects, content;
    private EditText edtPreferedTime, edtDate;
    private TimePickerDialog mTimePickerDialog;
    private DatePickerDialog datePickerDialog;
    private TextView tvNotify;
    private ScrollView scrollView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setOnTitleChanged((TitleChanged) getActivity());
        if (onTitleChanged != null)
            onTitleChanged.onTitleChanged("email");
        else
            Log.i(TAG, "onTitleChanged is null");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_email, container, false);
        initView(view);
        initTextChange(view);
        return view;
    }

    public void initView(View v) {
        etContent = (EditText) v.findViewById(R.id.etContentEmail);
        edtName = (EditText) v.findViewById(R.id.edtUserEmail);
        edtPhoneNumber = (EditText) v.findViewById(R.id.edtPhoneNumber);
        edtAddress = (EditText) v.findViewById(R.id.edtAddress);
        edtEmail = (EditText) v.findViewById(R.id.edtEmail);
        etSubjects = (EditText) v.findViewById(R.id.etSubjectEmail);
        edtPreferedTime = (EditText) v.findViewById(R.id.etFreferTime);
        edtDate = (EditText) v.findViewById(R.id.etDate);
        tvNotify = (TextView) v.findViewById(R.id.tv_notify);
        tvNotify.setVisibility(View.GONE);
        scrollView = (ScrollView) v.findViewById(R.id.scrollView);

        btnSub = (Button) v.findViewById(R.id.btSend);
        btnSub.setOnClickListener(this);

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        int m = calendar.get(Calendar.MINUTE);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DateFormat dateFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
        String time = dateFormat.format(calendar.getTime());

        String date = day + "-" + (month + 1) + "-" + year;
        edtPreferedTime.setText(time);
        edtDate.setText(date);

        mTimePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                cal.set(Calendar.MINUTE, minute);
                DateFormat dateFormat = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
                String time = dateFormat.format(cal.getTime());
                edtPreferedTime.setText(time);
            }
        }, h, m, true);

        datePickerDialog = new DatePickerDialog(self, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);

                if (cal.getTimeInMillis() > calendar.getTimeInMillis()) {
                    edtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                }
            }
        }, year, month, day);

        edtPreferedTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mTimePickerDialog.isShowing())
                    mTimePickerDialog.show();
            }
        });

        edtDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!datePickerDialog.isShowing())
                    datePickerDialog.show();
            }
        });

    }

    @Override
    public void onClick(View v) {
        onBtnOkClick();
    }

    @SuppressLint("NewApi")
    private void onBtnOkClick() {
        String message = checkFormSend();
        if (!message.equals(MESSAGE_SUCCESS)) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        } else {
            sendDetailsOrder();
        }
    }

    @SuppressLint("NewApi")
    public String checkFormSend() {
        String message = MESSAGE_SUCCESS;
        name = edtName.getText().toString();
        email = edtEmail.getText().toString();
        phone = edtPhoneNumber.getText().toString();
        subjects = etSubjects.getText().toString();
        address = edtAddress.getText().toString();
        content = etContent.getText().toString();
        if (name.isEmpty()) {
            message = self.getResources().getString(
                    R.string.error_UserName_empty);
            return message;
        }
        // phone
        if (phone.isEmpty()) {
            message = self.getResources().getString(R.string.error_Phone);
            return message;
        }
        // topic
        if (address.isEmpty()) {
            message = self.getResources().getString(R.string.error_Topic);
            return message;
        }
        // email
        if (email.isEmpty()) {
            message = self.getResources().getString(R.string.error_Email_empty);
            return message;
        } else {
            if (!StringUtility.isEmailValid(email)) {
                message = self.getResources().getString(
                        R.string.error_Email_wrong);
                return message;
            }
        }
        // subjects
        if (subjects.isEmpty()) {
            message = self.getResources().getString(R.string.error_Subjects);
            return message;
        }
        // content
        if (content.isEmpty()) {
            message = self.getResources().getString(R.string.error_Content);
            return message;
        }
        return message;
    }

    private void sendDetailsOrder() {
        ModelManager.putDataEmail(name, phone, email, edtPreferedTime.getText().toString(),
                address, content, new BaseRequest.CompleteListener() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        edtName.setText("");
                        edtEmail.setText("");
                        edtPhoneNumber.setText("");
                        edtAddress.setText("");
                        etSubjects.setText("");
                        etContent.setText("");

                        tvNotify.setVisibility(View.VISIBLE);
                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });
                    }

                    @Override
                    public void onError(String message) {
                        AppUtil.showToast(self, message);
                    }
                });
    }

    public void initTextChange(final View v) {

        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtName.getText().toString().length() > 0) {
                    ((ImageView) v.findViewById(R.id.checkName)).setVisibility(View.VISIBLE);
                    ((ImageView) v.findViewById(R.id.checkName)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_tick_selected));
                } else {
                    ((ImageView) v.findViewById(R.id.checkName)).setVisibility(View.INVISIBLE);
                    ((ImageView) v.findViewById(R.id.checkName)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_tick_selected));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtPhoneNumber.getText().toString().length() > 0) {
                    ((ImageView) v.findViewById(R.id.checkPhone)).setVisibility(View.VISIBLE);
                    ((ImageView) v.findViewById(R.id.checkPhone)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_tick_selected));

                } else {
                    ((ImageView) v.findViewById(R.id.checkPhone)).setVisibility(View.INVISIBLE);
                    ((ImageView) v.findViewById(R.id.checkPhone)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_tick_selected));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtAddress.getText().toString().length() > 0) {
                    ((ImageView) v.findViewById(R.id.checkAddress)).setVisibility(View.VISIBLE);
                    ((ImageView) v.findViewById(R.id.checkAddress)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_tick_selected));

                } else {
                    ((ImageView) v.findViewById(R.id.checkAddress)).setVisibility(View.INVISIBLE);
                    ((ImageView) v.findViewById(R.id.checkAddress)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_tick_selected));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtEmail.getText().toString().length() > 0) {
                    ((ImageView) v.findViewById(R.id.checkEmail)).setVisibility(View.VISIBLE);
                    if (StringUtility.isEmailValid(edtEmail.getText().toString())) {
                        ((ImageView) v.findViewById(R.id.checkEmail)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_tick_selected));
                    } else {
                        ((ImageView) v.findViewById(R.id.checkEmail)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_remove_selected));
                    }

                } else {
                    ((ImageView) v.findViewById(R.id.checkEmail)).setVisibility(View.INVISIBLE);
                    ((ImageView) v.findViewById(R.id.checkEmail)).setImageDrawable(getActivity().getResources().getDrawable(R.drawable.btn_tick_selected));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
