package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.object.OurPeople;
import com.squareup.picasso.Picasso;



import java.util.ArrayList;

public class OurPeopleAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<OurPeople> objects;
    private LayoutInflater inflater;

    public OurPeopleAdapter(Context context, ArrayList<OurPeople> objects) {
        this.context = context;
        this.objects = objects;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_ourpeople, null);
            vh = new ViewHoler();
            vh.txtNamePeople = (TextView) rowView
                    .findViewById(R.id.txtNamePeople);
            vh.txtPositionPeople = (TextView) rowView
                    .findViewById(R.id.txtPositionPeople);
            vh.imgPeople = (ImageView) rowView.findViewById(R.id.imgPeople);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        final OurPeople c = objects.get(position);
        if (c != null) {
            vh.txtNamePeople.setText(c.getName());
            vh.txtPositionPeople.setText(c.getPosition());
            Picasso.get().load(c.getImage()).into(vh.imgPeople);
        }
        return rowView;
    }

    static class ViewHoler {
        public TextView txtNamePeople, txtPositionPeople;
        public ImageView imgPeople;
    }
}
