package com.aveplus.avepay_cpocket.ebook.view.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.AbstractActivity;
import com.aveplus.avepay_cpocket.databinding.FragmentDetailsChapterVideoBinding;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseFragmentBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.configs.GlobalFunction;
import com.aveplus.avepay_cpocket.ebook.model.Book;
import com.aveplus.avepay_cpocket.ebook.model.Chapter;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.DetailsBookActivity;
import com.aveplus.avepay_cpocket.ebook.view.adapter.ActorAdapter;
import com.aveplus.avepay_cpocket.ebook.viewmodel.fragment.DetailsChapterVideoVM;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoControls;
import com.devbrackets.android.exomedia.ui.widget.VideoView;


public class FragmentDetailsChapterVideo extends BaseFragmentBinding {
    private static final String TAG = "VIDEO";
    private VideoView emVideoView;
    private DetailsChapterVideoVM viewModel;
    private FragmentDetailsChapterVideoBinding binding;
    private Chapter chapter;
    private AlertDialog alertDialogReload;
    private LinearLayout llDetails;
    private TextView tvIndicator;
    private VideoControls videoControls;
    private Book book;

    public static FragmentDetailsChapterVideo newInstance(Chapter chapter, Book book) {
        Bundle args = new Bundle();
        args.putParcelable(Constant.KEY_CHAPTER, chapter);
        args.putParcelable(Constant.KEY_BOOK, book);
        FragmentDetailsChapterVideo fragment = new FragmentDetailsChapterVideo();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_details_chapter_video;
    }

    @Override
    protected void initialize() {
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            chapter = bundle.getParcelable(Constant.KEY_CHAPTER);
            book = bundle.getParcelable(Constant.KEY_BOOK);
            GlobalFunction.addCountReadBook(self, chapter.getBookId());
        }
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new DetailsChapterVideoVM(self, chapter, book);
        return viewModel;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        this.binding = (FragmentDetailsChapterVideoBinding) binding;
        this.binding.setViewModel(viewModel);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.details_chapter_video_mp3, menu);
        menu.findItem(R.id.action_bookmarks).setVisible(false);
        if (DataStoreManager.isBookMarks(chapter.getBookId(), Constant.LIST_BOOK_MARKS)) {
            menu.findItem(R.id.action_bookmarks).setIcon(R.drawable.ic_book_marks_yellow);
        } else {
            menu.findItem(R.id.action_bookmarks).setIcon(R.drawable.ic_book_marks_white);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bookmarks:
                if (DataStoreManager.isBookMarks(chapter.getBookId(), Constant.LIST_BOOK_MARKS)) {
                    ((DetailsBookActivity) self).deleteBookrMarks();
                    item.setIcon(R.drawable.ic_book_marks_white);
                } else {
                    ((DetailsBookActivity) self).addBookrMarks();
                    item.setIcon(R.drawable.ic_book_marks_yellow);
                }
                return true;
            case R.id.action_comment:
                if (emVideoView.isPlaying()) {
                    emVideoView.pause();
                }
                ((FragmentActivity) self).getSupportFragmentManager().beginTransaction()
                        .add(R.id.content_details, FragmentComment.newInstance(chapter), Constant.FRAGMENT_COMENT)
                        .addToBackStack(null)
                        .commit();
                return true;

        }
        return false;
    }

    @Override
    protected void initView(View view) {
        tvIndicator = (TextView) view.findViewById(R.id.tv_indicator);
        emVideoView = binding.videoView;
        llDetails = binding.llDetails;
        binding.tvCast.setText(Html.fromHtml("<u>" + getString(R.string.the_cast) + "</u>"));
        Log.e(TAG, chapter.getAttachment());
        AppUtil.setImage(emVideoView.getPreviewImageView(), chapter.getImage());
        emVideoView.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared() {
                emVideoView.start();
            }
        });
        emVideoView.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError(Exception e) {
                showDialodDownloadFailed();

                return false;
            }


        });
        videoControls = emVideoView.getVideoControls();
//        videoControls.btnFull.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                fullScreen(b);
//            }
//        });


        AppUtil.setRateFollowWithDevice(emVideoView, 100, 56);
        emVideoView.setVideoURI(Uri.parse(chapter.getAttachment()));

        if (chapter.getActor() != null && chapter.getActor().size() > 0) {
            ActorAdapter mAdapter = new ActorAdapter(self, chapter.getActor());
            binding.rcvActor.setLayoutManager(new LinearLayoutManager(self, LinearLayoutManager.HORIZONTAL, false));
            binding.rcvActor.setAdapter(mAdapter);
        }
    }

    private void fullScreen(boolean values) {
        if (values) {
            llDetails.setVisibility(View.GONE);
            ((Activity) self).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            ((Activity) self).getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            ((Activity) self).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            emVideoView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            ((AbstractActivity) self).getToolbar().setVisibility(View.GONE);
//            tvIndicator.setVisibility(View.GONE);
        } else {
            llDetails.setVisibility(View.VISIBLE);
            ((Activity) self).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            ((Activity) self).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            AppUtil.setRateFollowWithDevice(emVideoView, 100, 56);
            ((AbstractActivity) self).getToolbar().setVisibility(View.VISIBLE);
//            tvIndicator.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (null != alertDialogReload) {
            alertDialogReload.dismiss();
        }
        fullScreen(false);
        emVideoView.release();
    }

    public void showDialodDownloadFailed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(self);
        builder.setTitle(R.string.app_name)
                .setMessage(R.string.error_LoadBook)
                .setCancelable(false)
                .setNegativeButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        emVideoView.setVideoURI(Uri.parse(chapter.getAttachment()));
                    }
                })
                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((Activity) self).onBackPressed();
                    }
                });
        alertDialogReload = builder.create();
        alertDialogReload.show();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        switch (newConfig.orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                Log.e("eee", "LANDSCAPE");
                // videoControls.btnFull.setChecked(true);
                break;

            case Configuration.ORIENTATION_PORTRAIT:
                // videoControls.btnFull.setChecked(false);
                Log.e("eee", "PORTRAIT");
                break;
        }
    }

}