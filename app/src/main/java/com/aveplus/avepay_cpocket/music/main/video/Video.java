package com.aveplus.avepay_cpocket.music.main.video;

import android.os.Parcel;
import android.os.Parcelable;

import com.aveplus.avepay_cpocket.music.base.model.BaseModel;
import com.google.gson.annotations.SerializedName;

public class Video extends BaseModel implements Parcelable {
    private int id,is_active,is_new,is_trending,is_popular,source_id,is_free;
    private String name,image,description,created_at,modified_at,price;
    @SerializedName("video")
    private String url;
   // private List<Artist> musicArtists;

    public Video() {
    }

    protected Video(Parcel in) {
        id = in.readInt();
        is_active = in.readInt();
        is_new = in.readInt();
        is_trending = in.readInt();
        is_popular = in.readInt();
        source_id = in.readInt();
        is_free = in.readInt();
        name = in.readString();
        image = in.readString();
        url = in.readString();
        description = in.readString();
        created_at = in.readString();
        modified_at = in.readString();
        price = in.readString();
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public int getIs_new() {
        return is_new;
    }

    public void setIs_new(int is_new) {
        this.is_new = is_new;
    }

    public int getIs_trending() {
        return is_trending;
    }

    public void setIs_trending(int is_trending) {
        this.is_trending = is_trending;
    }

    public int getIs_popular() {
        return is_popular;
    }

    public void setIs_popular(int is_popular) {
        this.is_popular = is_popular;
    }

    public int getSource_id() {
        return source_id;
    }

    public void setSource_id(int source_id) {
        this.source_id = source_id;
    }

    public int getIs_free() {
        return is_free;
    }

    public void setIs_free(int is_free) {
        this.is_free = is_free;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(is_active);
        parcel.writeInt(is_new);
        parcel.writeInt(is_trending);
        parcel.writeInt(is_popular);
        parcel.writeInt(source_id);
        parcel.writeInt(is_free);
        parcel.writeString(name);
        parcel.writeString(image);
        parcel.writeString(url);
        parcel.writeString(description);
        parcel.writeString(created_at);
        parcel.writeString(modified_at);
        parcel.writeString(price);
    }
}
