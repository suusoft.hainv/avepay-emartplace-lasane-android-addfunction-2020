package com.aveplus.avepay_cpocket.ebook.viewmodel.fragment;

import android.content.Context;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.ebook.model.Category;
import com.aveplus.avepay_cpocket.ebook.modelmanager.RequestManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiManager;
import com.aveplus.avepay_cpocket.ebook.network.ApiResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Suusoft on 9/8/2017.
 */
public class CategoryVM extends BaseViewModelList {
    public CategoryVM(Context context) {
        super(context);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        if (Config.TYPE_CATEGORY_TYPE_1 == Config.getTypeCategory()) {
            return new GridLayoutManager(self, 2);
        } else {
            return new LinearLayoutManager(self);
        }
    }

    @Override
    public void getData(int page) {
        List<Category> list = new ArrayList<>();

        RequestManager.getCategory(self, new ApiManager.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
//                addListData(response.getDataList(Book.class));
                List<Category> dataList = response.getDataList(Category.class);
                addListData(dataList);
            }

            @Override
            public void onError(String message) {

            }
        });
        addListData(list);
    }
}
