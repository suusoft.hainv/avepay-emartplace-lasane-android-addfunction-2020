package com.aveplus.avepay_cpocket.taxi.interfaces;


import com.aveplus.avepay_cpocket.taxi.objects.TripObj;

/**
 * Created by SuuSoft.com on 12/05/2016.
 */

public interface ITransportDeal {

    void onCancel(TripObj obj);

    void onTracking(TripObj obj);

    void onFinish(TripObj obj);
}
