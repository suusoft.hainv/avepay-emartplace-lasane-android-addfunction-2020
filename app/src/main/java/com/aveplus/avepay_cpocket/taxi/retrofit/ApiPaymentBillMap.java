package com.aveplus.avepay_cpocket.taxi.retrofit;



import com.aveplus.avepay_cpocket.taxi.retrofit.response.ResponseOnlinePayment;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiPaymentBillMap {

    final String PARAM_CODE = "Code";
    final String PARAM_Password = "Password";
    final String PARAM_MSISDN = "MSISDN";
    final String PARAM_Reference = "Reference";
    final String PARAM_Amount = "Amount";
    final String PARAM_MetaData = "MetaData";

    @GET("WebServices/BillPayment.asmx/ProcessOnlinePayment_V1.4")
    //@Headers("Cache-Control: no-cache") // no cache
    Call<ResponseOnlinePayment> processOnlinePayment(@Query(PARAM_CODE) String code,
                                                     @Query(PARAM_Password) String password,
                                                     @Query(PARAM_MSISDN) String MSISDN,
                                                     @Query(PARAM_Reference) String reference,
                                                     @Query(PARAM_Amount) String amount,
                                                     @Query(PARAM_MetaData) String metaData);



}
