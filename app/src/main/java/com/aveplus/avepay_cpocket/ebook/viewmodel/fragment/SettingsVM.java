package com.aveplus.avepay_cpocket.ebook.viewmodel.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.databinding.Bindable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.BR;
import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.AbstractActivity;
import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.model.Language;
import com.aveplus.avepay_cpocket.ebook.model.ListType;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.util.DialogUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.SplashActivity;
import com.aveplus.avepay_cpocket.ebook.view.adapter.SelectLanguageAdapter;
import com.aveplus.avepay_cpocket.ebook.view.adapter.SelectListTypeAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Suusoft on 9/12/2017.
 */
public class SettingsVM extends BaseViewModel {
    private boolean isScreenOn;
    private int texSize;


    @Override
    public void getData(int page) {

    }

    public SettingsVM(Context self) {
        super(self);
        texSize = DataStoreManager.getSettingTextSize();
        isScreenOn = DataStoreManager.getSettingScreenOn();
    }

    public void onClickApply(View view) {
        if (isScreenOn == DataStoreManager.getSettingScreenOn() && texSize == DataStoreManager.getSettingTextSize()) {
            ((AbstractActivity) self).showSnackBar(R.string.not_change);
        } else {
            ((AbstractActivity) self).showSnackBar(R.string.saved);
            DataStoreManager.setSettingScreenOn(isScreenOn);
            DataStoreManager.setSettingTextSize(texSize);
        }
    }

    public boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    public void onClickCleanCache(View view) {
        DialogUtil.showAlertDialog(self, R.string.clear_cache, R.string.title_clear_cache, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                boolean delete = deleteDirectory(new File(Environment.getExternalStorageDirectory() + Constant.FOLDER_EBOOK));
                if (delete) {
                    ((AbstractActivity) self).showSnackBar(R.string.successfully);
                } else {
                    ((AbstractActivity) self).showSnackBar(R.string.no_data_clean);
                }
            }
        });

    }

    public void onClickAboutUs(View view) {
        AppUtil.openWebView(self, self.getString(R.string.about_link));
    }

    public void onClickChangeTheme(View view) {
        //final ArrayList<Theme> mListTheme = Config.getListTheme(self);

        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_select_theme);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        // Get view
        final RecyclerView rcvData = (RecyclerView) dialog.findViewById(R.id.rcvData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(self, RecyclerView.VERTICAL, false);
        rcvData.setLayoutManager(linearLayoutManager);
        dialog.show();
    }

    public void onClickChangeListType(View view) {
        final ArrayList<ListType> mListType = new ArrayList<>();
        mListType.add(new ListType(Config.TYPE_GRID, Config.TYPE_GRID_NAME));
        mListType.add(new ListType(Config.TYPE_LIST, Config.TYPE_LIST_NAME));

        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_select_list_type);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        // Get view
        final RecyclerView rcvData = (RecyclerView) dialog.findViewById(R.id.rcvData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(self, RecyclerView.VERTICAL, false);
        rcvData.setLayoutManager(linearLayoutManager);
        SelectListTypeAdapter selectListTypeAdapter = new SelectListTypeAdapter(self, mListType,
                new SelectListTypeAdapter.ItemClickListener() {
                    @Override
                    public void onClickItem(int position) {
                        DataStoreManager.saveListType(mListType.get(position));
                        notifyPropertyChanged(BR.listType);
                        dialog.dismiss();

                    }
                });
        rcvData.setAdapter(selectListTypeAdapter);
        dialog.show();
    }


    public void onClickChangeLanguage(View view) {
        final ArrayList<Language> mListLanguage = Config.getListLanguage(self);

        final Dialog dialog = new Dialog(self);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_select);
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        // Get view
        final TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText(self.getString(R.string.select_language));
        final RecyclerView rcvData = (RecyclerView) dialog.findViewById(R.id.rcvData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(self, RecyclerView.VERTICAL, false);
        rcvData.setLayoutManager(linearLayoutManager);
        SelectLanguageAdapter selectLanguageAdapter = new SelectLanguageAdapter(self, mListLanguage,
                new SelectLanguageAdapter.ItemClickListener() {
                    @Override
                    public void onClickItem(int position) {
                        DataStoreManager.saveLanguage(mListLanguage.get(position));
                        //notifyPropertyChanged(BR.language);
                        dialog.dismiss();
                        DialogUtil.showAlertDialog(self, R.string.app_name, R.string.msg_confirm_restart_app,
                                new DialogUtil.IDialogConfirm() {
                                    @Override
                                    public void onClickOk() {
                                        updateLanguage();
                                        reloadActivity();
                                    }
                                });
                    }
                });
        rcvData.setAdapter(selectLanguageAdapter);
        dialog.show();
    }

    public void updateLanguage() {
        Locale myLocale = new Locale(DataStoreManager.getLanguage().getCode());
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale = myLocale;
        ((Activity) self).getBaseContext().getResources().updateConfiguration(config, ((Activity) self).getBaseContext().getResources().getDisplayMetrics());
    }

    private void reloadActivity() {
        Intent intent = new Intent(self, SplashActivity.class);
        self.startActivity(intent);
        ((Activity) self).finishAffinity();
    }

    public int getImage() {
        return 0;
    }

    public String getTitle() {
        return self.getString(R.string.about_title);
    }

    @Bindable
    public String getListType() {
        if (DataStoreManager.getListType() != null) {
            return DataStoreManager.getListType().getName();
        } else {
            return "";
        }
    }

    public int getSize() {
        return texSize;
    }

    public boolean isScreenOn() {
        return isScreenOn;
    }

    public void setScreenOn(boolean values) {
        isScreenOn = values;
        DataStoreManager.setSettingScreenOn(isScreenOn);
    }

    public String getDescription() {
        return self.getString(R.string.about_content1);
    }

    public void onValueChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
        this.texSize = 10 + progresValue;
        notifyChange();
    }

    public void setProgres(int progresValue) {
        this.texSize = 10 + progresValue;
    }

    public int getProgres() {
        return texSize - 10;
    }

}
