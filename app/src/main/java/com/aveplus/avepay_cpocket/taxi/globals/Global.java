package com.aveplus.avepay_cpocket.taxi.globals;


import com.aveplus.avepay_cpocket.taxi.objects.TripObj;

public class Global {

    public static class NotifiShowToast {

        public String gcm;
        public NotifiShowToast(String obj){
            gcm = obj;

        }
    }

    public static class NotifiChangeCart {  }

    public static class NotifiShowRequestTrip {

        public TripObj tripObj;
        public NotifiShowRequestTrip(TripObj obj){
            tripObj = obj;

        }
    }

    public static class FinishScreenLogin {  }

    public static class FinishAllCurrentAct {  }

    public static class FinishAllScreenProcessingTrip {  }

    public static class FinishAllChatAct { }

    public static class FinishAllScreenTrip { }

    public static class PushDriverAccept { }

    public static class PushDriverDeal { }

    public static class PushDriverEstimateTime { }

    public static class PushCancelTrip { }

    public static class PushBookLater { }

    public static class PushBookNegociatePrice { 
        public TripObj tripObj;
        public PushBookNegociatePrice(TripObj obj){
            tripObj  = obj;
        }
    }

    public static class PushDriverAcceptDeal { }

    public static class GoToChat { 
        public TripObj tripObj;
        public GoToChat(TripObj obj){
            tripObj = obj;

        }

    }

    public static class PushClickBtnFinishTrip {  }



}
