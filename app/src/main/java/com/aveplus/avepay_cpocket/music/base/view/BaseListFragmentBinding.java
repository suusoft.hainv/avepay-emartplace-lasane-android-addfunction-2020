package com.aveplus.avepay_cpocket.music.base.view;

import android.view.View;
import android.widget.ProgressBar;

import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.listener.IBaseListener;
import com.aveplus.avepay_cpocket.music.listener.IDataChangedListener;

import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */
public abstract class BaseListFragmentBinding extends BaseFragmentBinding implements IDataChangedListener {

    private BaseViewModelList mViewModel;
    public RecyclerView recyclerView;
    protected ProgressBar progressBar;
    protected abstract void setUpRecyclerView(RecyclerView recyclerView);

    protected IBaseListener baseListener;

    @Override
    protected int getLayoutInflate() {
        return R.layout._base_list_music;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        mViewModel = (BaseViewModelList) viewModel;
        // set listener data changed from viewmodel
        mViewModel.setDataListener(this);
        // get binding
        this.binding = binding;
        // set view model
        this.binding.setVariable(BR.viewModel, mViewModel);
    }

    @Override
    protected void initView(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        recyclerView = (RecyclerView) view.findViewById(R.id.rcv_data);
        recyclerView.setLayoutManager(mViewModel.getLayoutManager());
        recyclerView.addOnScrollListener(mViewModel.getOnScrollListener());
        setUpRecyclerView(recyclerView);
    }

    @Override
    public void onListDataChanged(List<?> data, boolean isAppend) {
        ((BaseAdapterBinding) recyclerView.getAdapter()).setItems(data, isAppend);
    }

    public void setBaseListener(IBaseListener listener){
        this.baseListener = listener;
    }

    protected void showProgress(boolean isShowing){
        if (isShowing){
            progressBar.setVisibility(View.VISIBLE);
        }else {
            progressBar.setVisibility(View.GONE);
        }
    }


}
