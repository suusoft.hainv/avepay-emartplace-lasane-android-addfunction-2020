package com.aveplus.avepay_cpocket.restaurant.main.restaurant.photo;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.ColorInt;
import androidx.viewpager.widget.ViewPager;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.base.view.BaseActivity;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.ImageVideo;

import java.util.ArrayList;
import java.util.List;

public class PhotoActivity extends BaseActivity implements View.OnClickListener {
    private static List<ImageVideo> imageVideos = new ArrayList<>();
    private static int i;
    private static String nameRestaurant = "";
    private ViewPager pagerPhoto;
    private ImageView imgBack;
    private ImageSliderAdapter mImageSliderAdapter;
    private LinearLayout indicator;
    static final int COLOR_INACTIVE = Color.WHITE;
    static final int COLOR_ACTIVE = Color.RED;


    public static PhotoActivity newInstance(List<ImageVideo> list, int position, String nameRestaurantt) {
        i = position;
        imageVideos = list;
        nameRestaurant = nameRestaurantt;
        return new PhotoActivity();
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.image_restaurant;
    }

    @Override
    protected void initView() {
        pagerPhoto = findViewById(R.id.imageSlider);
        imgBack = findViewById(R.id.img_remove);
        imgBack.setOnClickListener(this);
        mImageSliderAdapter = new ImageSliderAdapter(self, imageVideos);
        pagerPhoto.setAdapter(mImageSliderAdapter);

        // Indicator:
        indicator = findViewById(R.id.indicator);
        changeColorDot();
    }

    private void changeColorDot() {

        for (int i = 0; i < imageVideos.size(); i++) {
            // vietnames
            // COLOR_ACTIVE ứng với chấm ứng với vị trí hiện tại của ViewPager,
            // COLOR_INACTIVE ứng với các chấm còn lại
            // ViewPager có vị trí mặc định là 0, vì vậy color ở vị trí i == 0 sẽ là COLOR_ACTIVE
            //English
            // COLOR_ACTIVE corresponds to the current location of ViewPager,
            // COLOR_INACTIVE for the remaining dots
            // ViewPager has a default position of 0, so the color at position i == 0 will be COLOR_ACTIVE
            View dot = createDot(indicator.getContext(), i == 0 ? COLOR_ACTIVE : COLOR_INACTIVE);
            indicator.addView(dot);
        }

        // Thay đổi màu các chấm khi ViewPager thay đổi vị trí:
        // Change the color of the dots when ViewPager changes position:
        pagerPhoto.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for (int i = 0; i < imageVideos.size(); i++) {
                    // Duyệt qua từng "chấm" trong indicator
                    // Nếu i == position, tức i đang là vị trí hiện tại của ViewPager,
                    // ta sẽ đổi màu "chấm" thành COLOR_ACTIVE, nếu không
                    // thì sẽ đổi thành màu COLOR_INACTIVE
                    //English
                    // Browse through each "dot" in the indicator
                    // If i == position, ie i is the current position of ViewPager,
                    // we will change the color "dot" to COLOR_ACTIVE, otherwise
                    // it will change to COLOR_INACTIVE color
                    indicator.getChildAt(i).getBackground().mutate().setTint(i == position ? COLOR_ACTIVE : COLOR_INACTIVE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    View createDot(Context context, @ColorInt int color) {
        View dot = new View(context);
        LinearLayout.MarginLayoutParams dotParams = new LinearLayout.MarginLayoutParams(20, 20);
        dotParams.setMargins(20, 10, 20, 10);
        dot.setLayoutParams(dotParams);
        ShapeDrawable drawable = new ShapeDrawable(new OvalShape());
        drawable.setTint(color);
        dot.setBackground(drawable);
        return dot;
    }

    @Override
    protected void onViewCreated() {

    }

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.NONE;
    }

    @Override
    protected void onPrepareCreateView() {

    }

    @Override
    public void onClick(View view) {
        if (view == imgBack) {
            finish();
        }
    }
}
