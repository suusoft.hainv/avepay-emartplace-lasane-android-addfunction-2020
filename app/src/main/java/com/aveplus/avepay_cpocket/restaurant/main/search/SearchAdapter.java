package com.aveplus.avepay_cpocket.restaurant.main.search;
/**
 * Created by suusoft.com on 11/12/19.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.restaurant.main.restaurant.restaurandetail.ObjectCustom;
import com.bumptech.glide.Glide;


import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ObjectCustom> arrayList;
    private OnItemClickListener listener;


    public SearchAdapter(Context context, ArrayList<ObjectCustom> arrayList, OnItemClickListener listener) {
        this.context = context;
        this.arrayList = arrayList;
        this.listener = listener;
    }


    public interface OnItemClickListener {
        void onItemClick(ObjectCustom object);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binData(arrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvDes, tvPrice;
        private ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_name);
            tvDes = itemView.findViewById(R.id.tv_des);
            imageView = itemView.findViewById(R.id.img_foodd);
            tvPrice = itemView.findViewById(R.id.tv_price);

        }


        public void binData(ObjectCustom object) {

            if (object.getType().equals("food")) {

                // type = food, get here
                tvName.setText(object.getName());
                tvPrice.setText(object.getPrice() + " RM");
                tvDes.setText(object.getDescription());
                Glide.with(context).load(object.getAttachment()).into(imageView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onItemClick(object);
                        notifyDataSetChanged();
                    }
                });
            } else {

                //type = restaurant, get here
                tvName.setText(object.getName());
                tvPrice.setText(object.getAddress());
                tvDes.setText(object.getDescription());
                Glide.with(context).load(object.getImage()).into(imageView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onItemClick(object);
                        notifyDataSetChanged();
                    }
                });
            }

        }

    }

}
