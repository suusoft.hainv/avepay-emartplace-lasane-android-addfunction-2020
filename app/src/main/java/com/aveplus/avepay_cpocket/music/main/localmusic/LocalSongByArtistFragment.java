package com.aveplus.avepay_cpocket.music.main.localmusic;

import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseListFragmentNavBinding;
import com.aveplus.avepay_cpocket.music.base.view.SingleAdapter;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.main.downloaded.ItemDownloadedFileVM;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class LocalSongByArtistFragment extends BaseListFragmentNavBinding {


    public static final String KEY_ID = "KEY_ID";
    public static final String KEY_NAME = "KEY_NAME";

    private LocalSongByArtistVM viewModel;
    private String id, name;

    public static LocalSongByArtistFragment newInstance(String id, String name) {

        Bundle args = new Bundle();
        args.putString(KEY_ID, id);
        args.putString(KEY_NAME, name);
        LocalSongByArtistFragment fragment = new LocalSongByArtistFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initialize() {
        id = getArguments().getString(KEY_ID);
        name = getArguments().getString(KEY_NAME);
    }

    @Override
    protected BaseViewModel getViewModel() {
        viewModel = new LocalSongByArtistVM(self, id);
        return viewModel;
    }

    @Override
    protected void setUpRecyclerView(RecyclerView recyclerView) {
        setTitle(name);
        SingleAdapter singleAdapter = new SingleAdapter(self, R.layout.item_song, viewModel.getListData(), ItemDownloadedFileVM.class, viewModel.getActionListener());
        recyclerView.setAdapter(singleAdapter);
        viewModel.getData(1);
    }
}
