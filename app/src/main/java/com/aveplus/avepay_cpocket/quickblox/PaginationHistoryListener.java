package com.aveplus.avepay_cpocket.quickblox;

public interface PaginationHistoryListener {
    void downloadMore();
}
