package com.aveplus.avepay_cpocket.trainingcenter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.trainingcenter.cart.CartManager;
import com.aveplus.avepay_cpocket.trainingcenter.object.Product;

import java.util.ArrayList;

public class CartConfirmAdapter extends ArrayAdapter<Product> {

    public interface UpdateQuantityListener {
        void onUpdate(boolean isPlus, int index);
    }

    private Context context;
    private Button btnEdit;
    private LayoutInflater inflater;
    private UpdateQuantityListener listener;

    public static String[] arrQuantity;

    public static String[] arrNewQuantity;

    public UpdateQuantityListener getListener() {
        return listener;
    }

    public void setListener(UpdateQuantityListener listener) {
        this.listener = listener;
    }

    public CartConfirmAdapter(Context context, int layoutID,
                              ArrayList<Product> list2, UpdateQuantityListener listener) {
        super(context, layoutID);
        this.context = context;
        this.listener = listener;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return CartManager.getInstance().size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ViewHoler vh;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_cart_confirm, parent, false);
            vh = new ViewHoler();
            vh.txtName = (TextView) rowView.findViewById(R.id.textTitle_Cart);
            vh.txtName.setSelected(true);
            vh.txtPrice = (TextView) rowView.findViewById(R.id.textPrice);
            vh.txtNumberQuantity = (TextView) rowView.findViewById(R.id.tvNumberQuantity);
            rowView.setTag(vh);
        } else {
            vh = (ViewHoler) convertView.getTag();
        }
        final Product p = CartManager.getInstance().getYourCart().get(position);
        String arrImg = p.getImage().get(0).toString();
        if (p != null) {
            vh.txtName.setText(p.getName());
            vh.txtPrice.setText("$" + String.format("%.2f", Float.parseFloat(p.getPrice())));
            vh.txtNumberQuantity.setText(String.valueOf(p.getNumberCart()));
        }

        return rowView;

    }

    static class ViewHoler {
        public TextView txtName, txtPrice, txtNumberQuantity;
    }
}
