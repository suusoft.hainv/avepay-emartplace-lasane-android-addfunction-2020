package com.aveplus.avepay_cpocket.movie.movie.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Copyright © 2019 SUUSOFT
 */

public class WifiReceiver extends BroadcastReceiver {

    private static final String KEY_ACTION = "ACTION_NETWORK_STATE";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("WifiReceiver","onReceive");
    }
}
