package com.aveplus.avepay_cpocket.music.main.player;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.ObservableInt;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModel;
import com.aveplus.avepay_cpocket.music.configs.Constant;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.music.download.DownloadSongManager;
import com.aveplus.avepay_cpocket.music.main.detail.SongDetailActivity;
import com.aveplus.avepay_cpocket.music.model.Song;
import com.aveplus.avepay_cpocket.music.player.QueueManager;
import com.aveplus.avepay_cpocket.music.util.DialogUtil;
import com.aveplus.avepay_cpocket.utils.AppUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public class NowPlayingVM extends BaseViewModel {

    public ObservableInt favoriteImageState = new ObservableInt();
    Song item;


    boolean isFavorited;

    public NowPlayingVM(Context context) {
        super(context);
        checkItem();
        EventBus.getDefault().register(this);
    }

    private void checkItem() {
        item = QueueManager.getInstance().getCurrentItem();
        isFavorited = DataStoreManager.isFavorited(item.id);
        checkFavoriteImage();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Global.NewSongSelected event) {
        checkItem();
        notifyChange();
    }

    public String getName() {

        return item != null ? item.getTitle() : "";
    }

    public String getImage() {
        return item != null ? item.image : "";
    }

    public String getContent() {
        return item != null ? item.getContent() : "";
    }

    public boolean isEnableButton() {
        return item != null ? !item.isSongLocal : false;
    }

    @Override
    public void getData(int page) {

    }

    private void checkFavoriteImage() {
        if (isFavorited) {
            favoriteImageState.set(R.drawable.ic_favorited_24dp);
        } else {
            favoriteImageState.set(R.drawable.ic_favorite_border_24dp);
        }

    }

    public void onClickFavorite(View view) {
        if (!isFavorited) {
            DataStoreManager.saveFavorited(item);
            isFavorited = true;
            AppUtil.showToast(self, R.string.favorited);
        } else {
            DataStoreManager.unFavorite(item.id);
            isFavorited = false;
            AppUtil.showToast(self, R.string.unfavorite);
        }
        checkFavoriteImage();
    }

    public void onClickAddTolist(View view) {

    }

    public void onClickActionMore(View view) {
    }

    public void onClickShare(View view) {
        if (item != null) {
            AppUtil.share(self, item.title);
        }
    }

    public void onClickDownload(View view) {
        DialogUtil.showAlertDialog(self, R.string.confirm_download, new DialogUtil.IDialogConfirm() {
            @Override
            public void onClickOk() {
                DownloadSongManager.getInstance(self).download(item);
            }

            @Override
            public void onClickCancel() {

            }
        });
    }

    public void onClickInfor(View view) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.PREF_KEY_OBJECT, item);
        Intent intent = new Intent(self, SongDetailActivity.class);
        intent.putExtra(Constant.PREF_KEY_DATA, bundle);
        self.startActivity(intent);
    }


}
