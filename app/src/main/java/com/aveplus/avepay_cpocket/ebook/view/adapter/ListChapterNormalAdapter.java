package com.aveplus.avepay_cpocket.ebook.view.adapter;

import android.content.Context;
import android.view.ViewGroup;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.databinding.ItemDetailsChapterNormalBinding;
import com.aveplus.avepay_cpocket.ebook.base.model.view.BaseAdapter;
import com.aveplus.avepay_cpocket.ebook.model.ChapterNomal;
import com.aveplus.avepay_cpocket.ebook.viewmodel.item.ItemChapterNormalVM;

import java.util.List;


public class ListChapterNormalAdapter extends BaseAdapter {

    private List<ChapterNomal> listData;

    public ListChapterNormalAdapter(Context context, List<?> datas) {
        super(context);
        this.listData = (List<ChapterNomal>) datas;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemDetailsChapterNormalBinding binding = (ItemDetailsChapterNormalBinding) getViewBinding(parent, R.layout.item_details_chapter_normal);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        ((ViewHolder) holder).bindViewModel(listData.get(position));
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }


    @Override
    public void setDatas(List<?> data) {
        int size = listData.size();
        listData.addAll((List<ChapterNomal>) data);
        notifyItemRangeInserted(size, listData.size());
    }


    class ViewHolder extends BaseViewHolder {
        private ItemDetailsChapterNormalBinding binding;

        public ViewHolder(ItemDetailsChapterNormalBinding binding) {
            super(binding);
            this.binding = binding;
        }

        public void bindViewModel(ChapterNomal chapterNomal) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemChapterNormalVM(context, chapterNomal));
            } else {
                binding.getViewModel().setData(chapterNomal);
            }
        }
    }

}
