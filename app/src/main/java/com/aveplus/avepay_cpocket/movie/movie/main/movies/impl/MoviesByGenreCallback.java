package com.aveplus.avepay_cpocket.movie.movie.main.movies.impl;


import com.aveplus.avepay_cpocket.movie.movie.main.movies.entity.Movie;

import java.util.List;

/**
 */
public interface MoviesByGenreCallback {
    public void onCompleted(List<Movie> movies);

    public void onFailed(Exception ex);
}
