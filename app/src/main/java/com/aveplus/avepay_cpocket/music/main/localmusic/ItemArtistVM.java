package com.aveplus.avepay_cpocket.music.main.localmusic;

import android.content.Context;
import android.view.View;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.music.listener.IOnClickListener;
import com.aveplus.avepay_cpocket.music.listener.IOnItemArtistClickListener;
import com.aveplus.avepay_cpocket.music.model.Artist;


/**
 * "Copyright © 2019 SUUSOFT"
 */

public class ItemArtistVM extends BaseAdapterVM implements IOnClickListener {
    public static final String TAG = "ItemArtistVM";

    Artist item;


    public ItemArtistVM(Context self, Object object, int position) {
        super(self, position);
        this.item = (Artist) object;
    }

    public String getName() {
        return item.name;
    }

    public String getImage() {
        return  item.thumbnail;

    }

    public String getItemCount() {
        return String.format(self.getString(R.string.bind_count),item.songs_count);
    }

    public String getNum(){
        return  position + 1 +"";
    }

    // call to LocalMusicFragment class
    @Override
    public void onClick(View view) {
        ((IOnItemArtistClickListener) listener).onArtistItemClicked(item);
    }

    @Override
    public void setData(Object object) {

    }
}
