package com.aveplus.avepay_cpocket.music.model;

import android.os.Parcel;
import android.os.Parcelable;


import com.aveplus.avepay_cpocket.music.base.model.BaseModel;

import java.util.List;

public class ListPlayList extends BaseModel implements Parcelable {
    private int id, is_active;
    private String title,image,description,created_at,modified_at;
    private List<Album> list_playlist;

    public ListPlayList() {
    }

    public ListPlayList(int id, int is_active, String title, String image, String description, String created_at, String modified_at, List<Album> list_playlist) {
        this.id = id;
        this.is_active = is_active;
        this.title = title;
        this.image = image;
        this.description = description;
        this.created_at = created_at;
        this.modified_at = modified_at;
        this.list_playlist = list_playlist;
    }

    protected ListPlayList(Parcel in) {
        id = in.readInt();
        is_active = in.readInt();
        title = in.readString();
        image = in.readString();
        description = in.readString();
        created_at = in.readString();
        modified_at = in.readString();
        list_playlist = in.createTypedArrayList(Album.CREATOR);
    }

    public static final Creator<ListPlayList> CREATOR = new Creator<ListPlayList>() {
        @Override
        public ListPlayList createFromParcel(Parcel in) {
            return new ListPlayList(in);
        }

        @Override
        public ListPlayList[] newArray(int size) {
            return new ListPlayList[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public List<Album> getList_playlist() {
        return list_playlist;
    }

    public void setList_playlist(List<Album> list_playlist) {
        this.list_playlist = list_playlist;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(is_active);
        dest.writeString(title);
        dest.writeString(image);
        dest.writeString(description);
        dest.writeString(created_at);
        dest.writeString(modified_at);
        dest.writeTypedList(list_playlist);
    }
}
