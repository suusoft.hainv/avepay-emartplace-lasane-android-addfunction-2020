package com.aveplus.avepay_cpocket.music.base.view;

import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.vm.BaseViewModelList;
import com.aveplus.avepay_cpocket.music.configs.Global;
import com.aveplus.avepay_cpocket.music.listener.IBaseListener;
import com.aveplus.avepay_cpocket.music.listener.IDataChangedListener;
import com.aveplus.avepay_cpocket.music.listener.IOnFragmentNavListener;
import com.aveplus.avepay_cpocket.music.listener.IOnMenuItemClick;
import com.aveplus.avepay_cpocket.utils.ImageUtil;

import java.util.List;

/**
 * "Copyright © 2019 SUUSOFT"
 */

public abstract class BaseListFragmentNavBinding extends BaseFragmentBinding implements View.OnClickListener, IDataChangedListener, IOnMenuItemClick {

    private BaseViewModelList mViewModel;
    public RecyclerView recyclerView;
    protected ProgressBar progressBar;

    protected abstract void setUpRecyclerView(RecyclerView recyclerView);

    protected IBaseListener baseListener;

    protected FrameLayout contentLayout;
    protected FrameLayout contentLayout1;
    // toolbar
    protected ImageButton btnBack, btnAction;
    protected TextView tvTitle;
    protected ImageView ivTitile;

    private IOnFragmentNavListener listener;
    protected Animation animSlideIn, animSlideOut;

    @Override
    protected int getLayoutInflate() {
        return R.layout._base_list_fragment_nav;
    }

    @Override
    protected void setViewDataBinding(ViewDataBinding binding) {
        mViewModel = (BaseViewModelList) viewModel;
        // set listener data changed from viewmodel
        mViewModel.setDataListener(this);
        // get binding
        this.binding = binding;
        // set view model
        this.binding.setVariable(BR.viewModel, mViewModel);
    }

    @Override
    protected void initView(View view) {
        initAnimation();

        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        recyclerView = (RecyclerView) view.findViewById(R.id.rcv_data);
        recyclerView.setLayoutManager(mViewModel.getLayoutManager());
        contentLayout = (FrameLayout) view.findViewById(R.id.content);
        contentLayout1 = (FrameLayout) view.findViewById(R.id.content1);

        initToolbar(view);
        recyclerView.setLayoutManager(mViewModel.getLayoutManager());
        recyclerView.addOnScrollListener(mViewModel.getOnScrollListener());
        setUpRecyclerView(recyclerView);
    }

    protected void initToolbar(View view) {
        btnAction = (ImageButton) view.findViewById(R.id.c_btn_action);
        btnBack = (ImageButton) view.findViewById(R.id.c_btn_back);
        tvTitle = (TextView) view.findViewById(R.id.c_tv_title);
        ivTitile = (ImageView) view.findViewById(R.id.iv_title);

        btnBack.setOnClickListener(this);
        btnAction.setOnClickListener(this);
    }

    @Override
    public void onListDataChanged(List<?> data, boolean isAppend) {
        ((BaseAdapterBinding) recyclerView.getAdapter()).setItems(data, isAppend);
    }

    public void setBaseListener(IBaseListener listener) {
        this.baseListener = listener;
    }

    protected void showProgress(boolean isShowing) {
        if (isShowing) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }


    private void initAnimation() {
        animSlideIn = AnimationUtils.loadAnimation(self, R.anim.slide_in_left);
        animSlideOut = AnimationUtils.loadAnimation(self, R.anim.slide_out_left);
    }

    public IOnFragmentNavListener getListener() {
        return listener;
    }

    public void setListener(IOnFragmentNavListener listener) {
        this.listener = listener;
    }


    @Override
    public void onClick(View view) {
        if (view == btnBack) {
            if (listener != null) {
                listener.onFragmentBack();
            }
        } else if (view == btnAction) {
            showMenuAction();
        }
    }

    protected void showMenuAction() {
        Global.showPopupMenu(self, btnAction, getMenuLayout(), this);
    }

    protected int getMenuLayout() {
        return 0;
    }

    ;

    @Override
    public void onMenuItemClick(MenuItem item) {

    }

    public void showHideActionMenu(boolean isShow) {
        if (isShow) btnAction.setVisibility(View.VISIBLE);
        else btnAction.setVisibility(View.INVISIBLE);
    }

    public void showHideBack(boolean isShow) {
        if (isShow) btnBack.setVisibility(View.VISIBLE);
        else btnBack.setVisibility(View.GONE);
    }

    public TextView getTitle() {
        return tvTitle;
    }

    public void setTitle(String title) {
        this.tvTitle.setText(title);
    }

    public void setColorTitle(int color) {
        this.tvTitle.setTextColor(color);
    }

    public void setTitle(int title) {
        this.tvTitle.setText(getString(title));
    }

    public void setIvTitile(int drawable) {
        ivTitile.setVisibility(View.VISIBLE);
        ImageUtil.setImage(this.ivTitile, drawable);
    }
}
