package com.aveplus.avepay_cpocket.movie.movie.retrofit.response;

import android.util.Log;

import com.aveplus.avepay_cpocket.movie.movie.model.codeModel;
import com.aveplus.avepay_cpocket.movie.movie.retrofit.base.BaseReponse;


public class ResponseCode extends BaseReponse {

    public codeModel data;

    public codeModel getData() {
        if(data==null){
            Log.e("TAG", "getData: dât");
            return new codeModel();
        }
        return data;
    }

    public void setData(codeModel data) {
        this.data = data;
    }
}
