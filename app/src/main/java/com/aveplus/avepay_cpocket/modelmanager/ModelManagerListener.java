package com.aveplus.avepay_cpocket.modelmanager;

public interface ModelManagerListener {

    void onSuccess(Object object);

    void onError();

}
