package com.aveplus.avepay_cpocket.ebook.viewmodel.item;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.ebook.base.model.vm.BaseAdapterVM;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.model.Category;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.view.activity.ListBookActivity;


public class ItemCategoryVM extends BaseAdapterVM {

    private Category category;

    public ItemCategoryVM(Context context, Object category, int position) {
        super(context, position);
        this.category = (Category) category;
    }

    public ItemCategoryVM(Context context, Category category) {
        super(context);
        this.category =  category;
    }

    public String getImage() {
        return category.getImage();
    }

    public String getName() {
        return category.getName();
    }

    public String getDescription() {
        return category.getDescription();
    }

    public int getBackground() {
        return R.drawable.placeholder;
    }


    public int[] getBackgroundRandom() {
       return self.getResources().getIntArray(R.array.androidcolors);
    }


    @Override
    public void setData(Object object) {

    }


    public void onItemClicked(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK, Constant.MENU_LEFT_CATEGORY);
        bundle.putString(Constant.KEY_CATEGORY, category.getId());
        bundle.putString(Constant.KEY_TITLE_TOOLBAR, category.getName());
        AppUtil.startActivityClearTop(self, ListBookActivity.class,bundle);

    }
}
