package com.aveplus.avepay_cpocket.ebook.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.base.BaseActivity;
import com.aveplus.avepay_cpocket.configs.Config;
import com.aveplus.avepay_cpocket.datastore.DataStoreManager;
import com.aveplus.avepay_cpocket.ebook.configs.Constant;
import com.aveplus.avepay_cpocket.ebook.configs.GlobalFunction;
import com.aveplus.avepay_cpocket.ebook.model.MenuLeft;
import com.aveplus.avepay_cpocket.ebook.util.AppUtil;
import com.aveplus.avepay_cpocket.ebook.util.DialogUtil;
import com.aveplus.avepay_cpocket.ebook.util.StringUtil;
import com.aveplus.avepay_cpocket.ebook.view.adapter.MenuLeftAdapter;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentAbout;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentCategory;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentFeedBack;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentListBook;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentSearch;
import com.aveplus.avepay_cpocket.ebook.view.fragment.FragmentSettings;
import com.aveplus.avepay_cpocket.ebook.view.fragment.HomeFragment;
import com.aveplus.avepay_cpocket.ebook.view.fragment.MoreAppFragment;
import com.aveplus.avepay_cpocket.ebook.view.fragment.StoreFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    private DrawerLayout drawer;
    private Fragment mCurrenFragment;
    private Bundle bundle;
    private int mIndex = 0;

    private RelativeLayout layoutMenuLeft;
    private TextView tvUsername, tvLogin;
    private ImageView imgAvatar;
    private LinearLayout layoutUser;
    private Menu menu;
    public MenuItem itemListOrGrid;
    public FragmentSearch mFragmentSearch;

    private ListView mLvMenu;
    private ArrayList<MenuLeft> mArrMenu;
    private Integer[] arrImageMenu;
    private MenuLeftAdapter mMenuLeftAdapter;
    private boolean checkSelectMenuLeft = true;
    private BottomNavigationView navigationBottom;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    switchScreen(MenuLeft.FRAGMENT_HOME);
                    showIconToolbar(R.id.action_search2);

                    return true;
                case R.id.navigation_store:
                    switchScreen(MenuLeft.FRAGMENT_STORE);
                    showIconToolbar(/*R.id.action_clear_history*/);
                    return true;
                case R.id.navigation_search:
                    includeSearchScreen();
                    return true;

                case R.id.navigation_settings:
                    switchScreen(MenuLeft.FRAGMENT_SETTING);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected ToolbarType getToolbarType() {
        return ToolbarType.EBOOK;
    }

    @Override
    protected int getLayoutInflate() {
        return R.layout.activity_main_ebook;
    }

    @Override
    protected void getExtraData(Intent intent) {

    }

    @Override
    protected void initilize() {
        //initNavigationView();
        initBottomNav();
        // initMenuLeft();
        initBackStack();
    }

    private void initBackStack() {
    }

    @Override
    protected void initView() {
//        GlobalFunction.initInterstitialAdmob(this);
    }


    @Override
    protected void onViewCreated() {
        setTheme(R.style.AppTheme2);
        bundle = new Bundle();
        bundle.putInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK, Constant.MENU_LEFT_HOME);
        bundle.putString(Constant.KEY_TITLE_TOOLBAR, getString(R.string.home));
        mCurrenFragment = HomeFragment.newInstance();
        mCurrenFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mCurrenFragment, "FRAGMENT_" + MenuLeft.FRAGMENT_HOME)
                .commit();

    }


    private void initNavigationView() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


    }

    private void initBottomNav() {
        navigationBottom = (BottomNavigationView) findViewById(R.id.navigation);
        navigationBottom.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_ebook, menu);
        this.menu = menu;
        showIconToolbar(R.id.action_search2);
        return true;
    }

    public void showIconToolbar(Integer... id) {
        Log.e("eee", "3");
        if (null != menu) {
            MenuItem itemBookMarks = menu.findItem(R.id.action_bookmarks);
            MenuItem itemSearch = menu.findItem(R.id.action_search2);
            MenuItem itemClearHistory = menu.findItem(R.id.action_clear_history);
            itemListOrGrid = menu.findItem(R.id.action_list_or_grid);

            itemBookMarks.setVisible(false);
            itemSearch.setVisible(false);
            itemClearHistory.setVisible(false);
            itemListOrGrid.setVisible(false);
//            if (Config.TYPE_GRID == DataStoreManager.getListType().getId()) {
//                itemListOrGrid.setIcon(R.drawable.ic_list);
//            } else {
//                itemListOrGrid.setIcon(R.drawable.ic_grid);
//            }
            for (int i = 0; i < id.length; i++) {
                switch (id[i]) {
                    case R.id.action_bookmarks:
                        itemBookMarks.setVisible(true);
                        break;
                    case R.id.action_search2:
                        itemSearch.setVisible(true);
                        break;
                    case R.id.action_clear_history:
                        itemClearHistory.setVisible(true);
                        break;
                    case R.id.action_list_or_grid:
                        itemListOrGrid.setVisible(true);
                        break;
                }
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search2:
                navigationBottom.setSelectedItemId(R.id.navigation_search);
                return true;
            case R.id.action_clear_history:
                EventBus.getDefault().post(new Config.MyStoreEvent());

                return true;
            case R.id.action_list_or_grid:
                clickActionListOrGrid();
                return true;
        }
        return true;
    }

    private void includeSearchScreen() {
        setToolbarTitle(R.string.search);
        mFragmentSearch = FragmentSearch.newInstance();
        ((FragmentActivity) self).getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, mFragmentSearch, Constant.FRAGMENT_SEARCH)
                .addToBackStack(null)
                .commit();
        showIconToolbar();

    }

    public void clickActionListOrGrid() {

    }

    private void switchScreen(int values) {
        bundle = new Bundle();
        String tag = "FRAGMENT_" + values;
        switch (values) {
            case MenuLeft.FRAGMENT_HOME:
                checkSelectMenuLeft = true;
                mCurrenFragment = HomeFragment.newInstance();
                setToolbarTitle(R.string.home);
                break;

            case MenuLeft.FRAGMENT_STORE:
                checkSelectMenuLeft = true;
                mCurrenFragment = StoreFragment.newInstance();
                setToolbarTitle(R.string.my_store);
                break;
            case MenuLeft.FRAGMENT_ALL_CONTENT:
                checkSelectMenuLeft = true;
                bundle.putInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK, Constant.MENU_LEFT_ALL_BOOK);
                bundle.putString(Constant.KEY_TITLE_TOOLBAR, getString(R.string.all_book));
                mCurrenFragment = FragmentListBook.newInstance(true);
                break;
            case MenuLeft.FRAGMENT_CATEGORY:
                checkSelectMenuLeft = true;
                mCurrenFragment = FragmentCategory.newInstance();
                break;
            case MenuLeft.FRAGMENT_HISTORY:
                checkSelectMenuLeft = true;
                bundle.putInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK, Constant.MENU_LEFT_HISTORY);
                bundle.putString(Constant.KEY_TITLE_TOOLBAR, getString(R.string.histories));
                mCurrenFragment = FragmentListBook.newInstance(true);
                break;
            case MenuLeft.FRAGMENT_BOOKMARK:
                checkSelectMenuLeft = true;
                bundle.putInt(Constant.OPTIONS_FRAGMENT_LIST_BOOK, Constant.MENU_LEFT_BOOKMARKS);
                bundle.putString(Constant.KEY_TITLE_TOOLBAR, getString(R.string.bookmarks));
                mCurrenFragment = FragmentListBook.newInstance(true);
                break;
            case MenuLeft.FRAGMENT_SETTING:
                setToolbarTitle(R.string.settings);
                checkSelectMenuLeft = true;
                mCurrenFragment = FragmentSettings.newInstance();
                break;
            case MenuLeft.FRAGMENT_FEEDBACK:
                checkSelectMenuLeft = true;
                mCurrenFragment = FragmentFeedBack.newInstance();
                break;
            case MenuLeft.FRAGMENT_ABOUT:
                checkSelectMenuLeft = true;
                mCurrenFragment = FragmentAbout.newInstance();
                break;
            case MenuLeft.FRAGMENT_MORE_APP:
                int typeMoreApp = 0;
                if (Config.TYPE_MORE_APP_FROM_GOOGLE_PLAY == typeMoreApp) {
                    checkSelectMenuLeft = false;
                    AppUtil.goToResultByKeySearchOnGooglePlay(this, Config.KEY_SEARCH_MORE_APP_GOOGLE_PLAY);
                } else {
                    checkSelectMenuLeft = true;
                    mCurrenFragment = MoreAppFragment.newInstance();
                }
                break;

        }
        if (checkSelectMenuLeft) {
            mCurrenFragment.setArguments(bundle);
            if (!tag.equals(getSupportFragmentManager().findFragmentById(R.id.content).getTag())) {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.translate_left_in,
                                R.anim.translate_right_out,
                                R.anim.translate_pop_in,
                                R.anim.translate_pop_out)
                        .replace(R.id.content, mCurrenFragment, tag)
                        .commit();
            }
        }
//        drawer.closeDrawer(GravityCompat.START);
        AppUtil.hideSoftKeyboard(this);

    }

//    @Override
//    public void onBackPressed() {
//
////        DialogUtil.showAlertDialog(this, R.string.app_name, R.string.msg_confirm_exit, new DialogUtil.IDialogConfirm() {
////            @Override
////            public void onClickOk() {
////                finish();
////                overridePendingTransition(0, R.anim.slide_out_left);
////            }
////        });
//
//}

    public void initMenuLeft() {
        layoutMenuLeft = (RelativeLayout) findViewById(R.id.layoutMenuLeft);
        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        layoutUser = (LinearLayout) findViewById(R.id.layoutUser);
        mLvMenu = (ListView) findViewById(R.id.lvMenu);
        initMenu();
        mLvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mIndex = position;
                switchScreen(mArrMenu.get(position).getId());
                if (checkSelectMenuLeft) setSelectedItemMenu(position);
                mMenuLeftAdapter.notifyDataSetChanged();
            }
        });
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataStoreManager.getStatus()) {
                    DataStoreManager.removeUser();
                    DataStoreManager.saveStatus(false);
                    setInformationUser();
                }
            }
        });
    }

    private void setSelectedItemMenu(int position) {
        if (mArrMenu != null && mArrMenu.size() > 0) {
            for (int i = 0; i < mArrMenu.size(); i++) {
                if (i == position) {
                    mArrMenu.get(i).setSelected(true);
                } else {
                    mArrMenu.get(i).setSelected(false);
                }
            }
        }
    }

    public void initMenu() {
        setInformationUser();

        arrImageMenu = new Integer[]{R.drawable.ic_menu_home_black, R.drawable.ic_menu_select_all_black,
                R.drawable.ic_menu_category_black,
                R.drawable.ic_menu_history_black, R.drawable.ic_menu_bookmark_black,
                R.drawable.ic_menu_settings_black, R.drawable.ic_menu_feedback_black,
                R.drawable.ic_menu_more_black, R.drawable.ic_menu_info_black};

        mArrMenu = new ArrayList<>();
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_HOME, getString(R.string.home), false));
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_ALL_CONTENT, getString(R.string.all_book), false));
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_CATEGORY, getString(R.string.category), false));
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_HISTORY, getString(R.string.histories), false));
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_BOOKMARK, getString(R.string.bookmarks), false));
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_SETTING, getString(R.string.settings), false));
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_FEEDBACK, getString(R.string.feedback), false));
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_MORE_APP, getString(R.string.more_apps), false));
        mArrMenu.add(new MenuLeft(MenuLeft.FRAGMENT_ABOUT, getString(R.string.about), false));

        mArrMenu.get(mIndex).setSelected(true);
        mMenuLeftAdapter = new MenuLeftAdapter((MainActivity) self, mArrMenu, arrImageMenu);
        mLvMenu.setAdapter(mMenuLeftAdapter);
    }

    public void initTootlbarNavi(boolean values) {
        if (values) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(GravityCompat.START);
                }
            });
            initNavigationView();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 69 && resultCode == RESULT_OK) {
            mCurrenFragment.onActivityResult(requestCode, resultCode, new Intent());
        }
    }

    private void setInformationUser() {
        if (DataStoreManager.getStatus()) {
            tvLogin.setText(getString(R.string.logout));
            if (!StringUtil.isEmpty(DataStoreManager.getUser().getProfilePicture()))
                Picasso.get().load(DataStoreManager.getUser().getProfilePicture()).error(R.drawable.ic_default_user).into(imgAvatar);
            tvUsername.setText(DataStoreManager.getUser().getName());
        } else {
            tvLogin.setText(getString(R.string.login));
            imgAvatar.setImageResource(R.drawable.ic_default_user);
            tvUsername.setText("");
        }
    }
}
