package com.aveplus.avepay_cpocket.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.globals.Args;
import com.aveplus.avepay_cpocket.objects.DealObj;
import com.aveplus.avepay_cpocket.utils.ImageUtil;
import com.aveplus.avepay_cpocket.view.activities.DealDetailActivity;

import static com.aveplus.avepay_cpocket.view.adapters.DealAdapter.RQ_UPDATE_DEAL;

/**
 * Created by SUN on 3/7/2018.
 */

public class FragmentPagerIndicatorCate extends com.aveplus.avepay_cpocket.base.BaseFragment {

    private ImageView imgDeal;
    private TextView tvNameDeal;
    private DealObj dealObj;

    public static FragmentPagerIndicatorCate newInstance(DealObj dealObj) {
        Bundle args = new Bundle();
        FragmentPagerIndicatorCate fragment = new FragmentPagerIndicatorCate();
        fragment.setArguments(args);
        fragment.dealObj = dealObj;
        return fragment;
    }


    @Override
    protected int getLayoutInflate() {
        return R.layout.fragment_pager_indicator_cate;
    }

    @Override
    protected void init() {

    }

    @Override
    protected void initView(View view) {
        imgDeal = view.findViewById(R.id.image_deal);
        tvNameDeal = view.findViewById(R.id.tv_name_deal);
        imgDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Args.KEY_DEAL_OBJECT, dealObj);
                Intent intent = new Intent(self, DealDetailActivity.class);
                intent.putExtras(bundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, RQ_UPDATE_DEAL);
            }
        });
    }

    @Override
    protected void getData() {
        if (dealObj!=null){
            ImageUtil.setImage(self, imgDeal, dealObj.getImageUrl());
            tvNameDeal.setText(dealObj.getName());
        }

    }

}
