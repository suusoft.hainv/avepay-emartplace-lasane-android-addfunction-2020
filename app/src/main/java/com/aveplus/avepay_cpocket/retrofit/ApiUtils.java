package com.aveplus.avepay_cpocket.retrofit;


import com.aveplus.avepay_cpocket.configs.Config;

public class ApiUtils {
    public static final String BASE_URL = Config.URL_API;

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
