package com.aveplus.avepay_cpocket.music.video;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;


import com.aveplus.avepay_cpocket.R;
import com.aveplus.avepay_cpocket.music.base.view.BaseAdapterBinding;
import com.aveplus.avepay_cpocket.music.main.playvideo.OnTrailerListener;
import com.aveplus.avepay_cpocket.music.main.video2.ObjVideo;
import com.aveplus.avepay_cpocket.music.main.video2.vm.ItemVideoVM;

import java.util.List;

public class VideoAdapter extends BaseAdapterBinding {
    private List<ObjVideo> listData;
    private int mPosition;
    private OnTrailerListener mListener;

    public VideoAdapter(Context context, List<?> datas) {
        super (context);
        this.listData = (List<ObjVideo>) datas;
    }

    public VideoAdapter(Context context, List<?> datas, int position) {
        super (context);
        this.listData = (List<ObjVideo>) datas;
        this.mPosition = position;
        Log.e ("VideoAdapter", "onBindViewHolder2: " + mPosition);
    }

    @Override
    public void addDatas(List<?> data) {
        int positionStart = listData.size ();
        notifyItemRangeInserted (positionStart, data.size ());
    }

    @Override
    public void setDatas(List<?> data) {
        notifyDataSetChanged ();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder (getViewBinding (parent, R.layout.item_video));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ObjVideo item = listData.get (position);
        if (item != null) {
            holder.bind (new ItemVideoVM(context, item, position, mPosition));
            Log.e ("VideoAdapter", "onBindViewHolder: " + mPosition);
        }
    }

    @Override
    public int getItemCount() {
        if (listData != null) {
            return listData.size ();
        } else {
            return 0;
        }
    }
}
